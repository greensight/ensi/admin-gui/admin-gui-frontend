import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

import { BodyScrollLockProvider } from '../src/context/bodyScrollLock';
import { ThemeProvider as AutokitsThemeProvider  } from '@greensight/gds/autokits'
import { ThemeProvider, theme, scale } from '@scripts/gds';
import i18n from './i18next.cjs';

export const parameters = {
    i18n,
    locale: 'ru',
    locales: {
        ru: 'Русский',
    },
    actions: { argTypesRegex: '^on[A-Z].*' },
    controls: {
        matchers: {
            color: /(background|color)$/i,
            date: /Date$/,
        },
        hideNoControlsWarning: true,
    },
    viewport: {
        viewports: {
            ...INITIAL_VIEWPORTS,
            'Laptop 1440px': {
                name: 'Laptop 1440px',
                styles: {
                    width: '1440px',
                    height: '1000px',
                },
                type: 'desktop',
            },
            'Desktop 1600px': {
                name: 'Desktop 1600px',
                styles: {
                    width: '1600px',
                    height: '1000px',
                },
                type: 'desktop',
            },
            'Desktop 1920px': {
                name: 'Desktop 1920px',
                styles: {
                    width: '1920px',
                    height: '1080px',
                },
                type: 'desktop',
            },
        },
    },
    paddings: {
        values: [
            { name: 'None', value: '0' },
            { name: 'Small', value: '16px' },
            { name: 'Medium', value: '32px' },
            { name: 'Large', value: '64px' },
        ],
        default: 'Medium',
    },
    backgrounds: {
        grid: { cellSize: scale(1) },
        values: theme.colors && Object.entries(theme.colors).map(([name, value]) => ({ name, value })),
    },
    options: {
        storySort: {
            order: ['Intro', 'Autokits', 'Components'],
        },
    },
};

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            staleTime: 10000,
            retry: 0,
            refetchOnWindowFocus: false,
        },
    },
});

export const decorators = [
    Story => {
        return (
            <AutokitsThemeProvider  theme={theme}>
                <ThemeProvider theme={theme}>
                    <BodyScrollLockProvider>
                        <QueryClientProvider client={queryClient}>
                            <Story />
                        </QueryClientProvider>
                    </BodyScrollLockProvider>
                </ThemeProvider>
            </AutokitsThemeProvider >
        );
    },
];
