const path = require('path');
const react = require('@vitejs/plugin-react');
const ViteRequireContext = require('@originjs/vite-plugin-require-context').default;
const tsconfigPaths = require('vite-tsconfig-paths').default;
const svgr = require('vite-plugin-svgr').default;
const {mergeConfig} = require('vite');

const resolver = p => path.resolve(__dirname, p);

module.exports = {
    stories: [
        '../src/**/intro/welcome.stories.mdx',
        '../src/**/intro/*.stories.mdx',
        '../src/**/*.stories.mdx',
        '../src/**/*.stories.tsx'
    ],

    addons: [
        {
            name: '@storybook/addon-docs',
            options: {
                configureJSX: true,
            },
        },
        '@storybook/addon-links',
        '@storybook/addon-essentials',
        '@storybook/addon-interactions',
    ],

    framework: {
        name: '@storybook/react-vite',
        options: {}
    },

    typescript: {
        reactDocgen: 'none',
    },

    features: {
        storyStoreV7: true,
        previewMdx2: true,
        emotionAlias: false,
    },

    async viteFinal(config, {configType}) {
        config.plugins = config.plugins.filter(
            (plugin) =>
                !(Array.isArray(plugin) && plugin[0]?.name.includes('vite:react')),
        );

        config.plugins.push(svgr({
            include: "**/*.svg",
            svgrOptions: {
                exportType: 'default'
            }
        }));

        config.plugins.push(
            react({
                exclude: [/\.stories\.(t|j)sx?$/, /node_modules/],
                jsxImportSource: '@emotion/react',
                babel: {
                    plugins: ['@emotion/babel-plugin'],
                },
            }),
        );

        config.plugins.push(ViteRequireContext());

        config.esbuild = {
            // Fixed: [vite] warning: Top-level "this" will be replaced with undefined since this file is an ECMAScript module
            // https://github.com/vitejs/vite/issues/8644
            logOverride: {'this-is-undefined-in-esm': 'silent'},
        };

        if (configType !== 'PRODUCTION') {
            config.define = {'process': {}, 'process.env': {}};
        }

        config.plugins.push(
            /** @see https://github.com/aleclarson/vite-tsconfig-paths */
            tsconfigPaths({
                projects: [path.resolve(__dirname, '../tsconfig.json')],
            }),
        );

        return mergeConfig(config, {
            assetsInclude: ['**/*.md'],
            resolve: {
                alias: {
                    '@components': resolver('../src/components'),
                    '@scripts': resolver('../src/scripts'),
                    '@context': resolver('../src/context'),
                    '@icons': resolver('../src/icons'),
                    '@controls': resolver('../src/components/controls'),
                    '@customTypes': resolver('../src/customTypes'),
                    '@api': resolver('../src/api'),
                },
            },
        });
    }
};
