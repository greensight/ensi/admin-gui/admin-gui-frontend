const fse = require('fs-extra');
const path = require('path');

/**
 * The script copies the tinymce library files to the public folder
 * required for tiny self-hosting (see TextEditor.stories)
 */
fse.emptyDirSync(path.join(__dirname, 'public', 'assets', 'libs', 'tinymce'));
fse.copySync(
    path.join(__dirname, 'node_modules', 'tinymce'),
    path.join(__dirname, 'public', 'assets', 'libs', 'tinymce'),
    {
        overwrite: true,
    }
);
