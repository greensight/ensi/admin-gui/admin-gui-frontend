import {
    Block,
    Form,
    FormField,
    FormFieldWrapper,
    FormReset,
    IFieldWrapperProps,
} from '@ensi-platform/core-components';
import type { FieldValues } from 'react-hook-form';

import CalendarRange from '@controls/CalendarRange';
import Select, { OptionShape } from '@controls/Select';

import { Button, Layout, scale } from '@scripts/gds';

interface FilterProps extends Partial<IFieldWrapperProps<string>> {
    className?: string;
    onSubmit: (vals: FieldValues) => void;
    onReset?: (vals: FieldValues) => void;
    emptyInitialValues: FieldValues;
    initialValues: FieldValues;
    statuses: OptionShape[];
}

const Filters = ({ className, onSubmit, onReset, emptyInitialValues, initialValues, statuses }: FilterProps) => (
    <Block className={className}>
        <Form initialValues={initialValues} onSubmit={onSubmit} onReset={onReset}>
            <Block.Body>
                <Layout cols={4}>
                    <Layout.Item col={1}>
                        <FormField name="id" label="ID" type="number" />
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <FormField name="code" label="Код" />
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <FormField name="sellerId" label="Идентификатор продавца" type="number" />
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <FormField name="discountId" label="Идентификатор скидки" type="number" />
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <FormFieldWrapper name="status" label="Статус">
                            <Select options={statuses} />
                        </FormFieldWrapper>
                    </Layout.Item>
                    <Layout.Item col={2}>
                        <CalendarRange
                            label="Введите период действия"
                            nameFrom="activePeriodDate_gte"
                            nameTo="activePeriodDate_lte"
                        />
                    </Layout.Item>

                    <Layout.Item col={4} justify="end">
                        <FormReset
                            theme="secondary"
                            css={{ marginRight: scale(2) }}
                            type="button"
                            initialValues={emptyInitialValues}
                        >
                            Очистить
                        </FormReset>
                        <Button theme="primary" type="submit">
                            Применить
                        </Button>
                    </Layout.Item>
                </Layout>
            </Block.Body>
        </Form>
    </Block>
);

export default Filters;
