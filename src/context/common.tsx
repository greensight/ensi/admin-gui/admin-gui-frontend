import { Dispatch, FC, ReactNode, SetStateAction, createContext, useContext, useMemo, useState } from 'react';

type IsOpen = boolean;

export interface CommonContextProps {
    isSidebarOpen: IsOpen;
    setIsSidebarOpen: Dispatch<SetStateAction<IsOpen>>;
    isOverlayOpen: IsOpen;
    setIsOverlayOpen: Dispatch<SetStateAction<IsOpen>>;
}

const CommonContext = createContext<CommonContextProps | null>(null);
CommonContext.displayName = 'CommonContext';

export const CommonProvider: FC<{ children: ReactNode | ReactNode[] }> = ({ children }) => {
    const [isSidebarOpen, setIsSidebarOpen] = useState(false);
    const [isOverlayOpen, setIsOverlayOpen] = useState(false);

    const commonContext = useMemo(
        () => ({ isSidebarOpen, setIsSidebarOpen, isOverlayOpen, setIsOverlayOpen }),
        [isOverlayOpen, isSidebarOpen]
    );

    return <CommonContext.Provider value={commonContext}>{children}</CommonContext.Provider>;
};

export const useCommon = () => {
    const context = useContext(CommonContext);

    if (!context) {
        throw new Error(`Hook useCommon must be used within CommonProvider`);
    }

    return context;
};
