import { Dispatch, ReactNode, ReactNodeArray, createContext, useContext, useMemo, useReducer } from 'react';

import { CommonComponentDataProps } from '@scripts/getAuthData';

export interface TokenDataProps {
    accessToken?: string;
    hasRefreshToken?: boolean;
    expiresAt?: string;
}

export interface AuthContextProps {
    tokenData: TokenDataProps;
    setTokenData: Dispatch<TokenDataProps>;
}

const AuthContext = createContext<AuthContextProps | null>(null);
AuthContext.displayName = 'AuthContext';

export const AuthProvider = ({
    children,
    state: stateProps,
}: {
    children: ReactNode | ReactNodeArray;
    state: CommonComponentDataProps;
}) => {
    const updateTokenData = (state: TokenDataProps, action: TokenDataProps) => ({
        ...state,
        ...action,
    });
    const [tokenData, setTokenData] = useReducer(updateTokenData, {
        accessToken: stateProps?.accessToken,
        hasRefreshToken: stateProps?.hasRefreshToken,
        expiresAt: stateProps?.expiresAt,
    });

    const value = useMemo(() => ({ tokenData, setTokenData }), [tokenData, setTokenData]);

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

export const useAuth = () => {
    const context = useContext(AuthContext);

    if (!context) {
        throw new Error(`Hook useAuth must be used within AuthProvider`);
    }

    return context;
};
