import { MenuItemProps } from '@components/Sidebar/types';

import menuIcons from '@scripts/menuIcons';

const menu: MenuItemProps[] = [
    {
        text: 'Товары',
        Icon: menuIcons.package,
        code: 'products',
        subMenu: [
            {
                text: 'Каталог товаров',
                link: '/products/catalog',
                code: 'products_catalog',
            },
            {
                text: 'Импорт товаров',
                link: '/products/import',
                code: 'products_imports',
            },
            {
                text: 'Категории товаров',
                link: '/products/directories/categories',
                code: 'products_categories',
            },
            {
                text: 'Атрибуты',
                link: '/products/directories/properties',
                code: 'products_attributes',
            },
            { text: 'Бренды', link: '/products/brands', code: 'products_brands' },
            {
                text: 'Товарные склейки',
                link: '/products/product-groups',
                code: 'products_product_groups',
            },
            {
                text: 'Офферы',
                link: '/products/offers',
                code: 'offers',
            },
            {
                text: 'Статусная модель',
                link: '/products/statuses',
                code: 'products_statuses',
            },
            {
                text: 'История изменений',
                link: '/products/bulk-history',
                code: 'products_bulk_history',
            },
        ],
    },
    {
        text: 'Заказы',
        Icon: menuIcons.cart,
        code: 'orders',
        subMenu: [
            { text: 'Список  заказов', link: '/orders/list', code: 'orders_orders' },
            { text: 'Возвраты', link: '/orders/refunds', code: 'orders_refunds' },
            { text: 'Настройки', link: '/orders/settings', code: 'orders_settings' },
        ],
    },
    {
        text: 'Фиды',
        Icon: menuIcons.globe,
        code: 'feeds',
        link: '/feeds',
    },
    {
        text: 'Контент',
        Icon: menuIcons.image,
        code: 'content',
        subMenu: [
            { text: 'Баннеры', link: '/content/banners', code: 'content_banners' },
            { text: 'Товарные теги', link: '/content/nameplates', code: 'content_nameplates' },
            { text: 'Страницы', link: '/content/pages', code: 'content_pages' },
            { text: 'SEO-шаблоны', link: '/content/seo', code: 'content_seo_templates' },
        ],
    },
    {
        text: 'Логистика',
        Icon: menuIcons.truck,
        code: 'logistic',
        subMenu: [
            { text: 'Стоимость доставки', link: '/logistic/delivery-prices', code: 'logistic_delivery_prices' },
            { text: 'Параметры доставки', link: '/logistic/kpi', code: 'logistic_delivery_options_settings' },
            { text: 'ПВЗ', link: '/logistic/pickup-points', code: 'logistic_pickup_points' },
        ],
    },
    { text: 'Клиенты', link: '/customers', Icon: menuIcons.users, code: 'customers' },
    {
        text: 'Продавцы',
        Icon: menuIcons.award,
        code: 'sellers',
        subMenu: [
            { text: 'Список продавцов', link: '/sellers/list', code: 'sellers_sellers' },
            { text: 'Склады', link: '/stores/list', code: 'sellers_stores' },
        ],
    },
    {
        text: 'Маркетинг',
        Icon: menuIcons.birka,
        code: 'marketing',
        subMenu: [
            { text: 'Промокоды', link: '/marketing/promocodes', code: 'marketing_promo_codes' },
            { text: 'Скидки', link: '/marketing/discounts', code: 'marketing_discounts' },
        ],
    },
    {
        text: 'Коммуникации',
        Icon: menuIcons.message,
        code: 'communications',
        subMenu: [
            {
                text: 'Сервисные уведомления',
                link: '/communications/notifications',
                code: 'communications_notifications',
            },
        ],
    },
    {
        text: 'Отзывы',
        Icon: menuIcons.message,
        code: 'reviews',
        subMenu: [
            {
                text: 'Список отзывов',
                link: '/reviews/list',
                code: 'reviews_reviews',
            },
        ],
    },
    {
        text: 'Офферы',
        code: 'offers',
        subMenu: [
            {
                text: 'Обновление данные',
                link: '/products/offers/sync',
                code: 'offersEntitiesSync',
            },
        ],
    },
    {
        text: 'Настройки',
        Icon: menuIcons.settings,
        code: 'settings',
        subMenu: [
            { text: 'Пользователи', link: '/settings/users', code: 'settings_users' },
            { text: 'Роли', link: '/settings/users/roles', code: 'settings_users_roles' },
            { text: 'Параметры поиска', link: '/settings/cloud-integration', code: 'settings_cloud_integration' },
        ],
    },
];

const enrichMenu = (menuItems: MenuItemProps[], key?: string) => {
    const enrichedMenu = menuItems.slice();
    enrichedMenu.forEach(item => {
        const id = `${key}${item.code}`;
        item.id = id;
        if (item.subMenu) enrichMenu(item.subMenu, id);
    });
    return enrichedMenu;
};

interface FlatMenuItem {
    link?: string;
    text: string;
}

export interface FlatMenuItemExtended extends FlatMenuItem {
    parent: FlatMenuItem[];
}

/** нужно для формирования хлебных крошек */
const flatMenu = (menuItems: MenuItemProps[], parent: FlatMenuItem[] = []) =>
    menuItems.reduce((acc, val) => {
        if (val.link) acc.push({ text: val.text, link: val?.link, parent });
        if (val.subMenu) acc.push(...flatMenu(val.subMenu, [...parent, { text: val.text, link: val.link }]));
        return acc;
    }, [] as FlatMenuItemExtended[]);

export const preparedFlatMenu = flatMenu(menu);

export default enrichMenu(menu);
