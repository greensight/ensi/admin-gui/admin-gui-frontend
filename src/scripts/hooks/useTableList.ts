import { useRouter } from 'next/router';
import { useCallback, useEffect, useMemo, useState } from 'react';

import { useActivePage } from './useActivePage';
import { usePrevious } from './usePrevious';
import { useSessionStorage } from './useSessionStorage';

export interface useTableListProps {
    defaultSort?: string;
    defaultPerPage?: number;
    codesToSortKeys: Record<string, string>;
    pageKey?: string;
}

/**
 * Хук для работы с таблицами - листингами
 */
export const useTableList = ({
    defaultSort: defaultSortProp,
    defaultPerPage = 10,
    codesToSortKeys,
    pageKey = 'page',
}: useTableListProps) => {
    const [sort, setSort] = useState<string | undefined>(undefined);
    const { pathname } = useRouter();
    const [itemsPerPageCount, setItemsPerPageCount] = useSessionStorage(`${pathname}ItemsPerPageCount`, defaultPerPage);

    const activePage = useActivePage(pageKey);

    const defaultSort = useMemo(() => {
        if (!defaultSortProp) return defaultSortProp;
        return defaultSortProp in codesToSortKeys ? codesToSortKeys[defaultSortProp] : defaultSortProp;
    }, [codesToSortKeys, defaultSortProp]);

    const prevDefaultSort = usePrevious(defaultSort);
    useEffect(() => {
        if (defaultSort === undefined) return;
        if (defaultSort === prevDefaultSort) return;

        setSort(old => {
            if (!old) return defaultSort;
            return old;
        });
    }, [defaultSort, prevDefaultSort]);

    const onSortChange = useCallback(
        (stateAction: (string | undefined) | ((oldSortValue: string | undefined) => string)) => {
            setSort(old => {
                const newVal = typeof stateAction === 'function' ? stateAction(old) : stateAction;
                if (!newVal) return defaultSort;

                const isDesc = newVal.startsWith('-');
                const cleanKey = isDesc ? newVal.slice(1) : newVal;

                const result = cleanKey in codesToSortKeys ? codesToSortKeys[cleanKey] : newVal;

                return isDesc ? `-${result.replace('-', '')}` : result;
            });
        },
        [codesToSortKeys, defaultSort]
    );

    return {
        sort,
        setSort: onSortChange,
        itemsPerPageCount,
        setItemsPerPageCount,
        activePage,
    };
};
