import { useRouter } from 'next/router';
import { useCallback, useMemo } from 'react';

import { ExtendedRow } from '@components/Table';

interface IQuery {
    [key: string]:
        | string
        | number
        | boolean
        | readonly string[]
        | readonly number[]
        | readonly boolean[]
        | null
        | undefined;
}

export interface IUseGoToDetailPage {
    /**
     * pathname for redirecting. Must start with /
     * @example '/products/catalog'
     */
    pathname?: string;

    /**
     * additional conditions for access to the redirect
     * @example [access.ID.view]
     */
    extraConditions?: boolean[] | boolean;

    /**
     * right addon for pathname. Must start with /
     * @example '/create-refund'
     */
    rightPathnameAddon?: string;

    /**
     * query param for useRouter push
     */
    query?: IQuery;

    /**
     * callback before useRouter push
     */
    callback?: () => void;
}

export const useGoToDetailPage = ({
    pathname,
    extraConditions = true,
    rightPathnameAddon,
    query,
    callback,
}: IUseGoToDetailPage = {}) => {
    const { pathname: routerPathname, push } = useRouter();

    const isExtraConditionsFulfilled = useMemo(() => {
        if (Array.isArray(extraConditions)) return extraConditions.every(Boolean);
        return extraConditions;
    }, [extraConditions]);

    const goToDetailPage = useCallback(
        (originalRows: ExtendedRow['original'][] | undefined) => {
            if (originalRows && originalRows.length === 1 && isExtraConditionsFulfilled) {
                if (callback) callback();

                push({
                    pathname: `${pathname ?? routerPathname}/${originalRows[0].original.id}${rightPathnameAddon ?? ''}`,
                    ...(query && { query: { ...query } }),
                });
            }
        },
        [isExtraConditionsFulfilled, callback, push, pathname, routerPathname, rightPathnameAddon, query]
    );

    return goToDetailPage;
};
