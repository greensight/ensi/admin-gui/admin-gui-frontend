import timezones from 'countries-and-timezones';

const list = Object.values(timezones.getAllTimezones());

const options = list
    .sort((a, b) => b.utcOffset - a.utcOffset)
    .map(({ name, utcOffsetStr }) => ({
        value: name,
        label: `${name} ${utcOffsetStr}`,
    }));

export const useTimezones = () => options;
