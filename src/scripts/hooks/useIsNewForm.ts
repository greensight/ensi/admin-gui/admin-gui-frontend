import { useFormContext } from '@ensi-platform/core-components';

export const useIsNewForm = () => {
    try {
        const form = useFormContext();
        return !!form;
    } catch {
        return false;
    }
};
