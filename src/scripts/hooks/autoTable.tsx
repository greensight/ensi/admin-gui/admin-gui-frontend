import { UseQueryResult, useQueries } from '@tanstack/react-query';
import { cloneDeep, get, isEmpty, set } from 'lodash';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useEffect, useMemo, useRef } from 'react';
import deepEqual from 'react-fast-compare';

import {
    CommonResponse,
    FetchError,
    FilterResponse,
    Meta,
    MetaEnumValue,
    MetaField,
    NumericFieldTypes,
} from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { TableColumnDefAny } from '@components/Table';
import { Cell as FutureCell } from '@components/Table/components/TableCell';

import { cleanPhoneValue, fromRoubleToKopecks, isEmptyArray, toISOString } from '@scripts/helpers';
import { useFiltersHelper, useLinkCSS, useSessionStorage } from '@scripts/hooks';

import { useFiltersHelperClient } from './useFiltersHelperClient';

type MetaFieldObject = Record<string, Omit<MetaField, 'code'>>;

interface ParamsProps {
    isSaveToUrl?: boolean;
}

export interface autoFilterData {
    codesToSortKeys: Record<string, string>;
    isLoading: boolean;
    metaField: MetaFieldObject | undefined;
    filtersActive: boolean;
    URLHelper: (vals: Record<string, any>, shallow?: boolean) => void;
    clearInitialValue: (name: string) => void;
    reset: () => void;
    values: Record<string, any>;
    emptyInitialValues: Record<string, any>;
    searchRequestIncludes: string[];
    searchRequestFilter: Record<string, any>;
}

// из апи приходит url с v1, а наш apiClient принимает без v1
const getRightEndpoint = (url: string) => url.split('v1/')[1];

/**
 * Хук для получения
 */
export const useAutoFilters = (meta: Meta | undefined, params?: ParamsProps): autoFilterData => {
    const isSaveToUrl = useMemo(
        () => (typeof params?.isSaveToUrl === 'undefined' ? true : params.isSaveToUrl),
        [params?.isSaveToUrl]
    );

    // формируем объект полей, где ключом является code
    const metaFields = useMemo(
        () =>
            meta?.fields.reduce((acc, cur) => {
                acc[cur?.code] = cur;
                return acc;
            }, {} as MetaFieldObject) || undefined,
        [meta?.fields]
    );

    const codesToSortKeys = useMemo(() => {
        const result = {} as Record<string, string>;

        meta?.fields.forEach(field => {
            result[field.code] = field.sort_key || field.code;
        });

        return result;
    }, [meta?.fields]);

    const { pathname } = useRouter();

    /**
     * Связь между `code` фильтра и ключами значений ему соответствующих
     *
     * Для обычных фильтров возвращает 1 элемент, для множественных 2 элемента
     */
    const metaFieldValueKeys = useMemo(
        () =>
            meta?.fields.reduce(
                (acc, cur) => {
                    if (cur.filter === 'range' && cur.filter_range_key_from && cur.filter_range_key_to) {
                        acc[cur.code!] = [cur.filter_range_key_from, cur.filter_range_key_to];
                        return acc;
                    }

                    acc[cur.code!] = [cur.filter_key || cur.code];

                    return acc;
                },
                {} as Record<string, [string] | [string, string]>
            ) || {},
        [meta?.fields]
    );

    // формируем объект initialValues в зависимости от типов полей
    const emptyInitialValues = useMemo(
        () =>
            meta?.fields.reduce(
                (acc, { filter, filter_key, code, filter_range_key_to, filter_range_key_from }) => {
                    if (!filter) return acc;

                    if (filter === 'range' && filter_range_key_from && filter_range_key_to) {
                        acc[filter_range_key_from] = '';
                        acc[filter_range_key_to] = '';
                        return acc;
                    }

                    acc[filter_key || code] = filter === 'many' ? [] : '';
                    return acc;
                },
                {} as Record<string, any[] | string>
            ) || {},
        [meta?.fields]
    );

    const [savedInitialValues, setSavedInitialValues] = useSessionStorage<Record<string, any>>(
        `${pathname}Filters`,
        emptyInitialValues
    );

    const clearInitialValue = useCallback(
        (name: string) => {
            setSavedInitialValues(old => {
                if (!(name in old)) return old;

                old[name] = '';
                return { ...old };
            });
        },
        [setSavedInitialValues]
    );

    useEffect(() => {
        setSavedInitialValues(old => {
            if (!isEmpty(old)) return old;

            return emptyInitialValues;
        });
    }, [emptyInitialValues, savedInitialValues, setSavedInitialValues]);

    // НЕ храним в SessionStorage попапы, иначе они перезапишут страницы.
    // У них же один и тот же ключ pathname + Filters
    const initialValues = isSaveToUrl ? savedInitialValues : emptyInitialValues;

    const { initialValues: urlValues, URLHelper, filtersActive: urlFiltersActive } = useFiltersHelper(initialValues);

    const {
        initialValues: valuesClient,
        pushValues: pushValuesClient,
        filtersActive: filtersActiveClient,
    } = useFiltersHelperClient(initialValues);

    const pushValues = useCallback(
        (vals: Record<string, any>, shallow = false) => {
            if (isSaveToUrl) {
                setSavedInitialValues(vals);
                URLHelper(vals, shallow);
                return;
            }

            pushValuesClient(vals);
        },
        [URLHelper, isSaveToUrl, pushValuesClient, setSavedInitialValues]
    );

    const values = isSaveToUrl ? urlValues : valuesClient;
    const stateFiltersActive = isSaveToUrl ? urlFiltersActive : filtersActiveClient;
    const sessionFiltersActive = useMemo(
        () => !deepEqual(savedInitialValues, emptyInitialValues),
        [emptyInitialValues, savedInitialValues]
    );

    const filtersActive = stateFiltersActive || sessionFiltersActive;

    const reset = useCallback(() => {
        setSavedInitialValues(emptyInitialValues);
        pushValues(emptyInitialValues, true);
    }, [emptyInitialValues, pushValues, setSavedInitialValues]);

    const searchRequestFilter = useMemo(
        () =>
            (meta?.fields || []).reduce(
                (acc, field) => {
                    const valKeys = metaFieldValueKeys[field.code!];

                    valKeys.forEach(valKey => {
                        const val = values[valKey];

                        if (val && field) {
                            switch (field.type) {
                                case 'datetime':
                                case 'date': {
                                    const isEnd = valKey === field.filter_range_key_to;
                                    const time = isEnd ? '23:59:59.999' : '00:00:00.000';
                                    acc[valKey] = `${toISOString(val)}T${time}Z`;
                                    break;
                                }
                                case 'price':
                                    acc[valKey] = fromRoubleToKopecks(val);
                                    break;
                                case 'phone':
                                    acc[valKey] = cleanPhoneValue(val);
                                    break;
                                default:
                                    acc[valKey] = val;
                                    break;
                            }
                        } else if (val && !isEmptyArray(val)) {
                            acc[valKey] = val;
                        }
                    });

                    return acc;
                },
                {} as Record<string, any>
            ),
        [meta?.fields, metaFieldValueKeys, values]
    );

    const searchRequestIncludes = useMemo(() => {
        if (!meta?.fields.length) return [];
        const includes: string[] = [];

        meta?.fields.forEach(field => {
            if (field.include && !includes.includes(field.include)) {
                includes.push(field.include);
            }
        });

        return includes;
    }, [meta?.fields]);

    const isLoading = !meta;

    return {
        codesToSortKeys,
        isLoading,
        metaField: metaFields,
        filtersActive,
        URLHelper: pushValues,
        reset,
        values,
        emptyInitialValues: savedInitialValues,
        searchRequestIncludes,
        searchRequestFilter,
        clearInitialValue,
    };
};

/**
 * Хук для получения автосгенерированных столбцов
 */
export const useAutoColumns = (meta: Meta | undefined, canViewDetailPage = true) => {
    const { pathname } = useRouter();
    const listLink = useMemo(() => pathname.split(`[id]`)[0], [pathname]);
    const linkStyles = useLinkCSS();

    return useMemo(() => {
        if (!meta?.fields || meta?.fields.length === 0) return [];

        return meta?.fields?.reduce((acc, field) => {
            if (field.list && !acc.find(item => item.accessorKey === field.code)) {
                acc.push({
                    header: field.name,
                    accessorKey: field.code,
                    enableSorting: field.sort,
                    sortDescFirst: NumericFieldTypes.includes(field.type),
                    cell: ({ getValue, row }) =>
                        field.code === 'name' && canViewDetailPage ? (
                            <Link
                                legacyBehavior
                                href={`${row.original.customListLink || listLink}/${
                                    row.original.customIdResolver
                                        ? row.original.customIdResolver(row.original)
                                        : row.original.id
                                }`}
                                passHref
                            >
                                <a css={linkStyles}>{getValue()}</a>
                            </Link>
                        ) : (
                            <FutureCell value={getValue()} type={field.type} row={row} metaField={field} />
                        ),
                });
            }

            return acc;
        }, [] as TableColumnDefAny[]);
    }, [meta?.fields, linkStyles, listLink, canViewDetailPage]);
};

const EMPTY_ARR: never[] = [];

/**
 * Хук автоматически дообогащает данными поля, в которых требовался дополнительный запрос в апи
 */
export function useAutoTableData<T extends Record<string, any>>(
    /**
     * данные, полученные от бэка. Массив строк таблицы
     * */
    dataToEnrich: Array<T> | undefined,
    /**
     * данные, полученные из хука useAutoFilters
     * */
    metaField: MetaFieldObject | undefined,
    /**
     * массив кодов из метода :meta, для которых не будут подставлять человекопонятный текст,
     * но вернется объект типа {title: string, id: string}
     * */
    codesExceptions?: string[]
): Array<T> {
    // Найдем поля, по которым нужно запрашивать данные. Объект типа {code: {endpoint: string, ids: string[]}}
    const dataForRequest = useMemo(
        () =>
            dataToEnrich?.reduce(
                (acc, tableRow) => {
                    Object.keys(tableRow).forEach(key => {
                        const isExisting = metaField && metaField[key];
                        if (!isExisting) return;

                        const value = tableRow[key];
                        const isAllowedType = ['number', 'string'].includes(typeof value);
                        if (!isAllowedType) return;

                        const { type, enum_info } = metaField[key];
                        if (type !== 'enum' || !enum_info?.endpoint) return;

                        if (!acc[key]) {
                            acc[key] = { endpoint: getRightEndpoint(enum_info.endpoint), ids: [value] };
                            return;
                        }

                        if (!acc[key].ids.includes(value)) {
                            acc[key].ids.push(value);
                        }
                    });
                    return acc;
                },
                {} as Record<string, { endpoint: string; ids: string[] }>
            ) || {},
        [dataToEnrich, metaField]
    );

    const dataForRequestKeys = useMemo(() => Object.keys(dataForRequest), [dataForRequest]);

    const apiClient = useAuthApiClient();

    const enumsFromRequest = useQueries({
        queries: dataForRequestKeys.map(key => ({
            queryKey: [key, dataForRequest[key]],
            queryFn: () =>
                apiClient.post(dataForRequest[key].endpoint, {
                    data: { filter: { id: dataForRequest[key].ids } },
                }),
        })),
    }) as unknown as UseQueryResult<CommonResponse<FilterResponse>, FetchError>[];

    // useArrayMemo необходим т.к. объект, возвращаемый useQueries, всегда представляет собой новый массив
    // иначе начинается бесконечный цикл если испльзовать enumsFromRequest в зависимостях useMemo/useEffect и тд
    function useArrayMemo<T>(array: T[]): T[] {
        const memoizedArrayRef = useRef<T[]>(EMPTY_ARR);
        const areArraysEqual =
            memoizedArrayRef.current && array.length === memoizedArrayRef.current.length
                ? array.every((element, i) => element === memoizedArrayRef.current![i])
                : false;
        useEffect(() => {
            if (!areArraysEqual) {
                memoizedArrayRef.current = array;
            }
        }, [areArraysEqual, array]);
        return areArraysEqual ? memoizedArrayRef.current : array;
    }

    const memoizedResponses = useArrayMemo(enumsFromRequest.map(result => result.data));

    const resultEnumsData = useMemo(
        () =>
            memoizedResponses.reduce(
                (acc, item, index) => {
                    acc[dataForRequestKeys[index]] = item?.data || [];
                    return acc;
                },
                {} as Record<string, any>
            ),
        [dataForRequestKeys, memoizedResponses]
    );

    // обогатим данные для таблицы данными, полученными
    // из запроса или из :meta enum_info.values
    const enrichedData = useMemo(() => {
        if (!dataToEnrich) return [];

        return dataToEnrich.map(tableRow => {
            if (!metaField) return tableRow;

            const enrichedRow = cloneDeep(tableRow);

            Object.keys(metaField).forEach(code => {
                const filter = metaField[code];
                if (!filter || filter.type !== 'enum') return;

                const saveFullObject = codesExceptions?.includes(code);
                const { enum_info } = filter;

                const value = get(enrichedRow, code);

                let foundMetaValue: MetaEnumValue | undefined;
                if (enum_info?.values) {
                    foundMetaValue = Array.isArray(value)
                        ? value.map((val: any) => enum_info!.values!.find(v => String(v.id) === String(val)))
                        : enum_info.values.find(v => String(v.id) === String(value));
                } else if (enum_info?.endpoint) {
                    foundMetaValue = resultEnumsData[code]?.find(
                        (list: { id: string; title: string }) => String(list.id) === String(value)
                    );
                }

                if (foundMetaValue) {
                    // eslint-disable-next-line no-nested-ternary
                    const valueToSave = saveFullObject
                        ? foundMetaValue
                        : Array.isArray(foundMetaValue)
                          ? foundMetaValue.map(e => e.title).join(', ')
                          : foundMetaValue.title;
                    set(enrichedRow, `${code}_original`, foundMetaValue);
                    set(enrichedRow, code, valueToSave);
                }
            });

            return enrichedRow;
        });
    }, [dataToEnrich, metaField, resultEnumsData, codesExceptions]);

    return enrichedData;
}
