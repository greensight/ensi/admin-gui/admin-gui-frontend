import { useCallback, useEffect, useMemo, useRef } from 'react';

import { useCurrentUser } from '@api/auth';

export const useAllowedFields = <T extends Record<string, any>, K extends keyof T = keyof T>(
    matrix: Record<number, K[]>,
    masterRight?: number,
    whitelist: string[] = ['id']
) => {
    const { data: userData } = useCurrentUser();

    const userRights = useMemo(() => userData?.data.rights_access || [], [userData?.data.rights_access]);

    const matrixInv = useMemo(() => {
        const result: Partial<Record<K, number[]>> = {};

        const keys = Object.keys(matrix);
        keys.forEach(key => {
            const right = +key;
            const fields = matrix[right];

            fields.forEach(field => {
                if (!result[field]) result[field] = [];

                result[field]!.push(right);
            });
        });

        return result as Record<K, number[]>;
    }, [matrix]);

    const canEditCache = useRef(new Map<K, boolean>());

    useEffect(() => {
        const cache = canEditCache.current;
        cache.clear();

        return () => {
            cache?.clear();
        };
    }, [masterRight, matrix, userRights]);

    const canEditField = useCallback(
        (field: K) => {
            if (canEditCache.current.has(field)) return canEditCache.current.get(field);

            if (masterRight && userRights.includes(masterRight)) {
                canEditCache.current.set(field, true);
                return true;
            }

            const anyRight = matrixInv[field];
            const result = !!anyRight.find(right => userRights.includes(right));

            canEditCache.current.set(field, result);

            return result;
        },
        [masterRight, matrixInv, userRights]
    );

    const extractAllowedValues = useCallback(
        (values: T) => {
            const filteredValues: Partial<T> = {};

            const canUseKey = (key: string) => {
                if (whitelist.includes(key)) return true;
                if (masterRight && userRights.includes(masterRight)) {
                    return true;
                }

                const anyRight = matrixInv[key as K];

                if (!anyRight) {
                    throw new Error(
                        `Cant find field ${key} in useAllowedFields schema. Consider adding it to whitelist or schema`
                    );
                }

                const hasRight = anyRight.some(right => userRights.includes(right));
                return hasRight;
            };

            const keys = Object.keys(values);
            keys.forEach(key => {
                if (canUseKey(key)) {
                    filteredValues[key as K] = values[key];
                }
            });

            return filteredValues as T;
        },
        [masterRight, matrixInv, userRights, whitelist]
    );

    return {
        canEditField,
        extractAllowedValues,
    };
};
