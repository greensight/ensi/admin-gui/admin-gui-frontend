import { UseQueryResult } from '@tanstack/react-query';
import { useRef } from 'react';

import { CommonResponse, FetchError } from '@api/common/types';

export const useQueriesMemo = <T extends any>(responses: UseQueryResult<CommonResponse<T>, FetchError>[]) => {
    // this holds reference to previous value
    const ref = useRef<UseQueryResult<CommonResponse<T>, FetchError>[]>();
    // check if each element of the old and new array match
    const areDataSame =
        ref.current && responses.length === ref.current.length
            ? responses.every((element, i) => element.data === ref.current?.[i].data)
            : // initially there's no old array defined/stored, so set to false
              false;

    // only update prev results if array is not deemed the same
    if (!areDataSame) {
        ref.current = responses;
    }

    return areDataSame ? ref.current! : responses;
};
