import { useMemo } from 'react';

import { ProductGroupTypeParams, useProductGroupTypes } from '@api/catalog';

export const useProductTypesSelectable = (
    props: ProductGroupTypeParams = {
        include: [],
        pagination: {
            limit: 10,
            type: 'offset',
            offset: 0,
        },
        sort: ['id'],
    }
) => {
    const { data: groupTypes } = useProductGroupTypes(props);

    const options = useMemo(
        () =>
            groupTypes?.data?.map(({ id, name }) => ({
                value: id.toString(),
                key: name,
            })) || [],
        [groupTypes?.data]
    );

    return options;
};
