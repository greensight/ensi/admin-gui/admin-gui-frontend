import { useRouter } from 'next/router';
import { useCallback, useMemo } from 'react';
import deepEqual from 'react-fast-compare';
import type { FieldValues } from 'react-hook-form';

import { getQueryObjectForPathname, isEmptyArray } from '@scripts/helpers';

const PARAMS_TO_EXCLUDE = ['page', 'tab'];

const isObject = (value: any) => typeof value === 'object' && !Array.isArray(value) && value !== null;

/** Takes query-params from url, and enrich emptyInitValues */
export const useFiltersHelper = <T extends Record<string, any>>(emptyInitValues: FieldValues) => {
    const { push, query, pathname } = useRouter();

    /** Push form fields values in router history */
    const URLHelper = useCallback(
        (vals: T, shallow = false) => {
            const newURLSearchParams = new URLSearchParams();
            const queryForPathname = getQueryObjectForPathname(pathname, query) as Record<string, string>;

            if (isObject(vals)) {
                /** add unique form values */
                Object.keys(vals).forEach(k => {
                    if (vals[k] && !isEmptyArray(vals[k])) newURLSearchParams.append(k, JSON.stringify(vals[k]));
                });

                /** add query params wich associated with pathname params (next specific) */
                Object.keys(queryForPathname).forEach(k => {
                    newURLSearchParams.append(k, queryForPathname[k]);
                });

                push({ pathname, query: newURLSearchParams.toString() }, undefined, {
                    scroll: false,
                    ...(shallow && {
                        shallow: true,
                    }),
                });
            }
        },
        [push, pathname, query]
    );

    const initialValues = useMemo(() => {
        const valuesFromUrl: FieldValues = {};
        const queryForPathname = getQueryObjectForPathname(pathname, query);

        Object.keys(query).forEach(key => {
            const value = query[key];
            if (value) {
                /** exclude params from pathname params (next specific) and PARAMS_TO_EXCLUDE exceptions */
                if (!PARAMS_TO_EXCLUDE.some(p => p === key) && !Object.keys(queryForPathname).some(k => k === key)) {
                    const parsedValue = Array.isArray(value) ? value.map(v => JSON.parse(v)) : JSON.parse(value);
                    valuesFromUrl[key] = parsedValue;
                }
            }
        });

        return {
            ...emptyInitValues,
            ...valuesFromUrl,
        } as T;
    }, [emptyInitValues, pathname, query]);

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { tab, ...initialValuesWithoutTab } = initialValues;

    const filtersActive = useMemo(
        () => !deepEqual(emptyInitValues, initialValuesWithoutTab),
        [emptyInitValues, initialValuesWithoutTab]
    );

    return { initialValues, URLHelper, filtersActive };
};
