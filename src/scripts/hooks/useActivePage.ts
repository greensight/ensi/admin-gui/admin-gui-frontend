import { useRouter } from 'next/router';

export const useActivePage = (key = 'page') => {
    const { query } = useRouter();
    return +(query[key] || 1);
};
