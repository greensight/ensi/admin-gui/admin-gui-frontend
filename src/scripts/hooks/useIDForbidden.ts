import { useRouter } from 'next/router';

import { CREATE_PARAM } from '@scripts/constants';
import { regNextQueryParamName } from '@scripts/regex';

import { useAccess } from './useAccess';

export const useIDForbidden = (accessMatrix: ReturnType<typeof useAccess>, canViewDetail: boolean = false) => {
    const { query, pathname } = useRouter();

    const queryId = pathname.match(regNextQueryParamName)?.[1];

    const isCreationPage = queryId ? query?.[queryId] === CREATE_PARAM : false;

    const isIDForbidden = isCreationPage
        ? !accessMatrix.ID.create || !accessMatrix.ID.view
        : !(accessMatrix.ID.view || canViewDetail);

    return isIDForbidden;
};
