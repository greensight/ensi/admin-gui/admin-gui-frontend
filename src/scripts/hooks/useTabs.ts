import type { TabsProps } from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { useCallback, useRef, useState } from 'react';

export interface IUseTabs {
    /**
     * Tab name for `router.query`
     *
     * Use it with the `enableQuery` param if there are multiple tabs on the page
     * @default "tab"
     */
    tabName?: string;

    /**
     * Defines `<Tabs.Tab />` visibility
     *
     * Pass an object like `{ id: isVisible }`
     *
     * @example:
     * {
     *   '0': false,
     *   '1': true,
     * }
     */
    tabsVisibility?: Record<string | number, boolean>;

    /**
     * Enables binding of tabs to `router.query`
     *
     * Use it if you want to open a specific tab using a URL
     * @default false
     */
    enableQuery?: boolean;
}

export const useTabs = ({ tabName = 'tab', tabsVisibility = {}, enableQuery = false }: IUseTabs = {}) => {
    const { push, query } = useRouter();

    const updateTabQuery = useCallback(
        (tabId: string | number) => {
            push({ query: { ...query, [tabName]: tabId } }, undefined, {
                shallow: true,
            });
        },
        [push, query, tabName]
    );

    const tabIdsRef = useRef<(string | number)[] | null>(null);

    const [selectedId, setSelectedId] = useState(() => {
        const tabIds = Object.keys(tabsVisibility);
        const initialTab = (query[tabName] as string) || tabIds.find(tabId => tabsVisibility[tabId]) || tabIds[0] || 0;

        if (tabIds.length) tabIdsRef.current = tabIds;

        if (enableQuery) updateTabQuery(initialTab);

        return initialTab;
    });

    const getTabsProps = useCallback<() => Partial<TabsProps>>(
        () => ({
            selectedId,
            onChange: (_, payload) => {
                setSelectedId(payload.selectedId);

                if (enableQuery) updateTabQuery(payload.selectedId.split('_')[1]);
            },
        }),
        [selectedId, enableQuery, updateTabQuery]
    );

    return {
        tabIds: tabIdsRef.current,
        selectedId,
        getTabsProps,
    };
};
