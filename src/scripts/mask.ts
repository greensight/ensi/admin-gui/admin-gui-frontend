import IMask from 'imask';

/** Телефонный номер (+7(000) 000-00-00) */
export const maskPhone = '+7(000) 000-00-00';

/** Одиночная дата (00.00.0000) */
export const maskDateSingle = '00.00.0000';

/** Диапазон дат (00.00.0000 — 00.00.0000) */
export const maskDateRange = '00.00.0000 — 00.00.0000';

/** ИНН (000000000099) */
export const maskInn = '0000000000[00]';

/** КПП и БИК (000000000) */
export const maskNineDigits = '000000000';

export const getMaskTime = (prefix?: string) => (prefix ? `${prefix} 00:00:00` : '00:00:00');

const iMaskPhone = IMask.createMask({ mask: maskPhone });
export const preFormatPhone = (unformatted: string) => {
    iMaskPhone.resolve(unformatted);
    return `${iMaskPhone.value}`;
};

export const TIME_WITHOUT_SECONDS_MASK = {
    mask: 'HH:MM',
    autofix: true,
    overwrite: true,
    blocks: {
        HH: new IMask.MaskedRange({
            placeholderChar: 'HH',
            from: 0,
            to: 23,
            maxLength: 2,
        }),
        MM: new IMask.MaskedRange({
            placeholderChar: 'MM',
            from: 0,
            to: 59,
            maxLength: 2,
        }),
    },
};

export const timeslotMask = {
    mask: 'HH:MM - HH:MM',
    autofix: true,
    overwrite: true,
    blocks: {
        HH: new IMask.MaskedRange({
            placeholderChar: 'HH',
            from: 0,
            to: 23,
            maxLength: 2,
        }),
        MM: new IMask.MaskedRange({
            placeholderChar: 'MM',
            from: 0,
            to: 59,
            maxLength: 2,
        }),
    },
};
