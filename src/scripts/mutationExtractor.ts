import { isEqual } from 'lodash';

export type MutationExtractor<TInitialValues extends Record<string, any>> = {
    keys: (keyof TInitialValues)[];
    mapper: (val: any) => any;
    areEqual?: (initial: any, current: any) => boolean;
};

export const extractMutation = <TInitialValues extends Record<string, any>>(
    initialValues: TInitialValues,
    vals: TInitialValues,
    extractor: MutationExtractor<TInitialValues>
) => {
    const mutatedData: Partial<Record<keyof typeof initialValues, any>> = {};
    let hasDefinedValues = false;

    extractor.keys.forEach(key => {
        const initialValue = extractor.mapper(initialValues[key]);
        const currentValue = extractor.mapper(vals[key]);

        const equal = extractor.areEqual
            ? extractor.areEqual(initialValue, currentValue)
            : isEqual(initialValue, currentValue);

        if (!equal) {
            mutatedData[key] = currentValue;

            if (currentValue !== undefined) {
                hasDefinedValues = true;
            }
        }
    });

    return {
        mutatedData: mutatedData as Required<typeof mutatedData>,
        hasDefinedValues,
    };
};

export const extractMutations = <TInitialValues extends Record<string, any>>(
    initialValues: TInitialValues,
    vals: TInitialValues,
    extractors: MutationExtractor<TInitialValues>[]
) => {
    let result = {};
    let hasDefined = false;

    extractors.forEach(extractor => {
        const { hasDefinedValues, mutatedData } = extractMutation(initialValues, vals, extractor);

        if (hasDefinedValues) {
            hasDefined = true;
        }

        result = {
            ...result,
            ...mutatedData,
        };
    });

    return {
        data: result as Record<keyof TInitialValues, any>,
        hasDefinedValues: hasDefined,
    };
};
