import { CSSObject } from '@emotion/react';
import {
    ComponentsTheme,
    Theme,
    createMediaQueries,
    createTheme,
    typography as gdsTypography,
    useTheme as useGDSTheme,
} from '@greensight/gds/emotion';

import tokens from '../../public/tokens.json';
import { Button } from './themes/button';
import { Calendar, CalendarTheme } from './themes/calendar';
import { global } from './themes/global';
import { Input, InputTheme } from './themes/input';
import { Select, SelectTheme } from './themes/select';
import { Tabs, TabsTheme } from './themes/tabs';

interface ComponentsThemeExtended extends ComponentsTheme {
    Calendar?: CalendarTheme;
    Tabs?: TabsTheme;
    Input?: InputTheme;
    Select?: SelectTheme;
}

export const {
    colors,
    shadows,
    layout: { breakpoints: Breakpoints },
    typography: { styles: typographyStyles },
} = tokens;
export type ColorsTheme = typeof colors;
export type TypographyParam = keyof typeof tokens.typography.styles;

export const MEDIA_QUERIES = createMediaQueries(tokens.layout.breakpoints);

export interface ExtendedTheme extends Omit<Theme, 'colors'> {
    components?: ComponentsThemeExtended;
    colors?: ColorsTheme;
}

const settings: ExtendedTheme = {
    global,
    components: {
        Button,
        Calendar,
        Tabs,
        Input,
        Select,
    },
};

const theme = createTheme({
    tokens,
    settings,
}) as ExtendedTheme;

const typography = (name: TypographyParam = 'bodySm') => gdsTypography(name, theme) as CSSObject;
const useTheme = () => useGDSTheme() as ExtendedTheme;

export * from '@greensight/gds/emotion';
export { typography, theme, useTheme };
