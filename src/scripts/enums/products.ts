export enum ImageTypes {
    BASE = 1,
    CATALOG,
    GALLERY,
    DESCRIPTION,
}

export enum ProductStatePopup {
    SAVED = 'saved',
    DELETE = 'delete',
    DELETE_SUCCESS = 'deleteSuccess',
    LEAVE = 'leave',
}

export enum ProductType {
    // Штучный
    PIECE = 1,
    // Весовой
    WEIGHT = 2,
    // Фасованный
    PACKED = 3,
}
