export enum ReviewStatus {
    NEW = 1,
    PUBLISHED = 2,
    DECLINED = 3
}