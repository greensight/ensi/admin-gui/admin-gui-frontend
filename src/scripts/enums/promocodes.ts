export enum PromoCodeType {
    DISCOUNT = 1,
    FREE_DELIVERY,
}

export const PromoCodeTypeValue: Record<PromoCodeType, string> = {
    [PromoCodeType.DISCOUNT]: 'Скидка',
    [PromoCodeType.FREE_DELIVERY]: 'Бесплатная доставка',
};

export enum PromoCodeStatus {
    ACTIVE = 1,
    SUSPENDED = 2,
}

export const PromoCodeStatusValues = {
    [PromoCodeStatus.ACTIVE]: 'Активна',
    [PromoCodeStatus.SUSPENDED]: 'Приостановлена',
};
