import { TokenDataProps } from '@context/auth';

import { HttpCode, MILLISECONDS_IN_SECOND, TOKEN_TIME_DIFF_SECONDS } from '@scripts/constants';

import { LoginData } from './auth/helpers';
import { CommonResponse, Config, FetchError } from './common/types';

const API_URL = '/api/api-front/';

export class APIClient {
    baseURL: string;

    token: TokenDataProps | undefined;

    static setToken: ((val: TokenDataProps) => void) | undefined;

    readonly authURL = 'auth';

    static checkRequest: Promise<LoginData> | null = null;

    constructor(baseURL: string, token?: TokenDataProps, setToken?: (val: TokenDataProps) => void) {
        this.baseURL = baseURL;
        this.token = token;
        APIClient.setToken = setToken;
    }

    static checkAuthorization(response: Response) {
        if (response.status === HttpCode.UNAUTHORIZED && APIClient.setToken) {
            fetch(`${API_URL}/clear`, {
                method: 'POST',
            });
            APIClient.setToken({ accessToken: '', hasRefreshToken: false, expiresAt: '' });
        }
    }

    static async returnJSON(response: Response) {
        APIClient.checkAuthorization(response);

        const json: CommonResponse<any> = await response.json();

        if (!response.ok) {
            let errorMessage = 'Request failed';
            let errorCode = '';
            /** we must throw errors to allow @tanstack/react-query catch them in hooks */
            if (json.errors && json.errors.length > 0) {
                errorMessage = json.errors.map(e => e.message).join(` \n`);
                errorCode = [...new Set(json.errors.map(e => e.code))].join(` & `);
            }
            throw new FetchError(errorMessage, response.status, errorCode);
        }
        return json;
    }

    static async returnBlob(response: Response) {
        APIClient.checkAuthorization(response);
        return response.blob();
    }

    protected async unauthorizedClient(
        endpoint: string,
        {
            data,
            token,
            timeout = 10000,
            headers: customHeaders = {},
            params,
            abortController = new AbortController(),
            ...customConfig
        }: Config = {}
    ) {
        const endpointWithParams = `${endpoint}${params ? `?${new URLSearchParams(params)}` : ''}`;

        const timer = setTimeout(() => abortController.abort(), timeout);

        const config = {
            method: data ? 'POST' : 'GET',
            // eslint-disable-next-line no-nested-ternary
            body: data ? (data instanceof FormData ? data : JSON.stringify(data)) : undefined,
            headers: {
                ...(data && !(data instanceof FormData) && { 'Content-Type': 'application/json' }),
                ...((token || this.token?.accessToken) && {
                    'X-Access-Token': token || this.token?.accessToken,
                }),
                ...customHeaders,
            },
            ...customConfig,
            signal: abortController.signal,
        };

        const response = await fetch(`${this.baseURL}${endpointWithParams}`, config);
        clearTimeout(timer);

        return response;
    }

    static async refreshToken(): Promise<LoginData> {
        const response = await fetch(`${API_URL}/refresh`, {
            method: 'POST',
        })
            .then(APIClient.returnJSON)
            .then(res => {
                if (APIClient.setToken)
                    APIClient.setToken({
                        accessToken: res.data.accessToken,
                        hasRefreshToken: res.data.hasRefreshToken,
                        expiresAt: res.data.expiresAt,
                    });
                return res;
            });

        return response;
    }

    protected async checkToken() {
        let token = '';

        try {
            const timeNow = Math.floor(Date.now() / MILLISECONDS_IN_SECOND);

            if (
                this.token?.expiresAt &&
                this.token?.hasRefreshToken &&
                +this.token.expiresAt < timeNow - TOKEN_TIME_DIFF_SECONDS
            ) {
                if (!APIClient.checkRequest) {
                    APIClient.checkRequest = APIClient.refreshToken();
                }

                const result = await APIClient.checkRequest;

                if (result) {
                    if (APIClient.setToken)
                        APIClient.setToken({
                            accessToken: result.data.accessToken,
                            hasRefreshToken: result.data.hasRefreshToken,
                            expiresAt: result.data.expiresAt,
                        });
                    token = result.data.accessToken;
                }

                APIClient.checkRequest = null;
            } else if (this.token?.accessToken) {
                token = this.token.accessToken;
            }
        } catch (e) {
            console.error(`Unable to check token: ${e}`);
        }

        return token;
    }

    public async request(endpoint: string, config?: Config) {
        const token = await this.checkToken();
        return this.unauthorizedClient(endpoint, { ...config, token }).then(APIClient.returnJSON);
    }

    public async get(endpoint: string, config?: Omit<Config, 'data'>) {
        return this.request(endpoint, { ...config, method: 'GET' });
    }

    public async post(endpoint: string, config?: Config) {
        return this.request(endpoint, { ...config, method: 'POST' });
    }

    public async patch(endpoint: string, config?: Config) {
        return this.request(endpoint, { ...config, method: 'PATCH' });
    }

    public async put(endpoint: string, config?: Config) {
        return this.request(endpoint, { ...config, method: 'PUT' });
    }

    public async delete(endpoint: string, config?: Config) {
        return this.request(endpoint, { ...config, method: 'DELETE' });
    }

    public async downloadFile(endpoint: string, config?: Config) {
        const token = await this.checkToken();
        return this.unauthorizedClient(endpoint, { ...config, token, method: 'POST' }).then(APIClient.returnBlob);
    }

    public async logOut() {
        return this.request(`logout`);
    }
}

export const apiFront = new APIClient(API_URL);

export { FetchError };
