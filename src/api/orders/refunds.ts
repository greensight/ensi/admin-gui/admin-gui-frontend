import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, CommonSearchParams, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { FetchError } from '../index';
import {
    Refund,
    RefundCreateData,
    RefundFile,
    RefundFilters,
    RefundMutateData,
    RefundReason,
    RefundReasonCreate,
    RefundsInclude,
} from './types';

const baseURL = 'orders/refunds';

export const useRefundsSearch = (
    data: CommonSearchParams<Partial<RefundFilters>, string, RefundsInclude>,
    enabled = true
) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Refund[]>, FetchError>({
        queryKey: ['refunds', data],
        queryFn: () => apiClient.post(`${baseURL}:search`, { data }),
        enabled,
    });
};

export const useRefundsMeta = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Meta>, FetchError>({
        queryKey: ['refunds-meta'],
        queryFn: () => apiClient.get(`${baseURL}:meta`),
        enabled,
    });
};

export const useCreateRefund = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Refund>, FetchError, RefundCreateData>({
        mutationFn: data => apiClient.post(baseURL, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: ['refunds'],
            });
        },
    });
};

export const useRefund = (id: number | string | undefined, include?: RefundsInclude, enable = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Refund>, FetchError>({
        queryKey: ['refund', id, include],
        queryFn: () => apiClient.get(`${baseURL}/${id}${include ? `?include=${include.join(',')}` : ''}`),
        enabled: isDetailIdValid(id) && enable,
    });
};

export const usePatchRefund = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Refund>, FetchError, Partial<RefundMutateData> & { id: number | string }>({
        mutationFn: ({ id, ...data }) => apiClient.patch(`${baseURL}/${id}`, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: ['refund'],
            });
            queryClient.invalidateQueries({
                queryKey: ['refunds'],
            });
        },
    });
};

export const useRefundAddFile = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<RefundFile>, FetchError, { id: number | string; file: File }>({
        mutationFn: ({ id, file }) => {
            const formData = new FormData();
            formData.append('file', file);
            return apiClient.post(`${baseURL}/${id}:attach-file`, { data: formData });
        },

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: ['refund'],
            });
            queryClient.invalidateQueries({
                queryKey: ['refunds'],
            });
        },
    });
};
export const useRefundDeleteFiles = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, { id: number | string; file_ids: number[] }>({
        mutationFn: ({ id, ...data }) => apiClient.delete(`${baseURL}/${id}:delete-files`, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: ['refund'],
            });
            queryClient.invalidateQueries({
                queryKey: ['refunds'],
            });
        },
    });
};

const reasonsURL = 'orders/refund-reasons';
export const useRefundReasons = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<RefundReason[]>, FetchError>({
        queryKey: ['refundReasons'],
        queryFn: () => apiClient.get(reasonsURL),
        enabled,
    });
};

export const useCreateRefundReasons = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<RefundReason>, FetchError, RefundReasonCreate>({
        mutationFn: data => apiClient.post(reasonsURL, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: ['refundReasons'],
            });
        },
    });
};
export const usePatchRefundReasons = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<RefundReason>, FetchError, Partial<RefundReasonCreate & { id: number | string }>>(
        {
            mutationFn: ({ id, ...rest }) => apiClient.patch(`${reasonsURL}/${id}`, { data: rest }),

            onSuccess: () => {
                queryClient.invalidateQueries({
                    queryKey: ['refundReasons'],
                });
            },
        }
    );
};
