import { useMutation, useQueryClient } from '@tanstack/react-query';

import { CommonResponse } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { FetchError } from '../index';
import { OrderShipment } from './types/order-shipment';

export const useShipmentChange = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<OrderShipment[]>, FetchError, { id: number; status: number }>({
        mutationFn: ({ id, status }) => apiClient.patch(`orders/shipments/${id}`, { data: { status } }),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: ['orders'],
            });
        },
    });
};
