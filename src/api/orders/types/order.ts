import { Customer } from '@api/customers';

import { OrderListFilterPaymentMethod, OrderListItemPaymentStatus, OrderStatus } from './oms-enums';
import { Address, OrderDelivery } from './order-delivery';

export interface OrderResponsible {
    id: number;
    full_name: string;
    short_name: string;
    created_at: string;
    updated_at: string;
    active: boolean;
    login: string;
    last_name: string | null;
    first_name: string | null;
    middle_name: string | null;
    email: string;
    phone: string;
    timezone: string;
    roles: { id: number; title: string; expires: string | null }[];
}

export interface OrderFile {
    id: number;
    order_id: number;
    original_name: string;
    file: { description: string };
    created_at: string;
    updated_at: string;
}
export interface Order {
    id: number;
    number: string;
    customer_id: number | null;
    customer_email: string;
    cost: number;
    price: number;
    spent_bonus: number;
    promo_code: string;
    added_bonus: number;
    source: number;
    status_at: string | null;
    payment_status: OrderListItemPaymentStatus;
    payment_status_at: string | null;
    payed_at: string | null;
    payment_expires_at: string | null;
    payment_method: number;
    payment_system: number;
    payment_link: string | null;
    payment_external_id: string | null;
    is_problem_at: string | null;
    is_changed: boolean;
    is_expired: boolean;
    is_expired_at: string | null;
    is_return: boolean;
    is_return_at: string | null;
    is_partial_return: boolean;
    is_partial_return_at: string | null;
    is_editable: boolean;
    created_at: string;
    updated_at: string;
    status: OrderStatus;
    responsible_id: number | null;
    responsible?: OrderResponsible;
    client_comment: string | null;
    receiver_name?: string | null;
    receiver_phone?: string | null;
    receiver_email?: string | null;
    is_problem: boolean | null;
    problem_comment: string | null;
    delivery_service: number;
    delivery_method: number;
    delivery_cost: number;
    delivery_price: number;
    delivery_tariff_id: number | null;
    delivery_point_id: number | null;
    delivery_address: Address | null;
    delivery_comment: string | null;
    deliveries?: OrderDelivery[];
    customer?: Customer;
    files: OrderFile[];
}

export type OrderKeys = keyof Order;

export type OrderSearchSort = ('number' | 'created_at' | 'price' | 'delivery_price')[];

export type OrderPrices = ('price' | 'delivery_price' | 'cost' | 'delivery_cost')[];

export interface OrderChangeData {
    id: number;
    responsible_id?: number | null;
    status?: number;
    client_comment?: string | null;
    receiver_name?: string | null;
    receiver_phone?: string | null;
    receiver_email?: string | null;
    is_problem?: boolean | null;
    problem_comment?: string | null;
}

export interface OrderSearchFilter {
    number_like?: string;
    created_at_gte?: string;
    created_at_lte?: string;
    status?: OrderStatus;
    payment_method?: OrderListFilterPaymentMethod;
    price_gte?: number;
    price_lte?: number;
    'deliveries.shipments.seller_id'?: number[];
    'deliveries.shipments.store_id'?: number[];
    delivery_service?: number[];
    is_problem?: boolean;
    manager_comment_like?: string;
    customer_id?: number;
}

export type OrderSearchInclude = (
    | 'items'
    | 'items_count'
    | 'files'
    | 'files_count'
    | 'deliveries'
    | 'deliveries_count'
    | 'deliveries.shipments'
    | 'deliveries.shipments.orderItems'
    | 'customer'
    | 'customer.user'
    | 'orderItems.product'
    | 'orderItems.product.images'
    | 'orderItems.product.category'
    | 'orderItems.product.brand'
    | 'responsible'
)[];

export interface OrderItemsAddRequestOrderItems {
    /**
     * Идентификатор товарного предолжения
     */
    offer_id: number;
    /**
     * Количество товара
     */
    qty: number;
}

/**
 * Добавление OrderItems в заказ */

export interface OrderItemsAddRequest {
    order_items: OrderItemsAddRequestOrderItems[];
}

export interface OrderItemsAddResponse {
    data: Order;
}

export interface OrderItemsDeleteRequest {
    offer_ids: number[];
}

export interface OrderItemChangeQtyRequestOrderItems {
    /**
     * Идентификатор элемента заказа
     */
    item_id: number;
    /**
     * Новое количество
     */
    qty: number;
}

/**
 * Изменение количества OrderItem */

export interface OrderItemChangeQtyRequest {
    order_items: OrderItemChangeQtyRequestOrderItems[];
}

/**
 * Информация о товаре */

export interface OrderItemPropertiesProductData {
    /**
     * Вес нетто товара (кг)
     * @example 10
     */
    weight: number;
    /**
     * Вес брутто товара (кг)
     * @example 8.5
     */
    weight_gross: number;
    /**
     * Ширина товара (мм)
     * @example 100
     */
    width: number;
    /**
     * Высота товара (мм)
     * @example 100
     */
    height: number;
    /**
     * Длина товара (мм)
     * @example 100
     */
    length: number;
    /**
     * место хранения в магазине
     * @example "5-158"
     */
    storage_address?: string;
    /**
     * артикул (EAN)
     * @example "14853"
     */
    barcode?: string;
}

export interface OrderItemProperties {
    /**
     * id элемента корзины
     */
    id: number;
    /**
     * ID заказа
     * @example 1234
     */
    order_id: number;
    /**
     * ID отгрузки
     * @example 1234
     */
    shipment_id: number;
    /**
     * ID оффера
     * @example 1234
     */
    offer_id: number;
    /**
     * Название товара
     */
    name: string;
    /**
     * Кол-во товара
     */
    qty: number;
    /**
     * Цена товара (цена * qty - скидки), коп.
     * @example 10000
     */
    price: number;
    /**
     * Цена единичного товара (в коп.)
     * @example 10000
     */
    price_per_one: number;
    /**
     * Цена товара до скидок (цена * qty), коп.
     * @example 10000
     */
    cost: number;
    /**
     * Цена единичного товара до скидок (в коп.)
     * @example 10000
     */
    cost_per_one: number;
    /**
     * Информация о товаре
     */
    product_data: OrderItemPropertiesProductData;
    /**
     * Количество возвращаемых элементов корзины в заявке
     * @example 10
     */
    refund_qty?: number;
    /**
     * дата создания
     */
    created_at: string;
    /**
     * дата обновления
     */
    updated_at: string;
}

export interface OrderItemChangeQtyResponse {
    data: OrderItemProperties[];
}
