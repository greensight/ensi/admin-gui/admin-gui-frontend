import { useMutation, useQueryClient } from '@tanstack/react-query';

import { CommonResponse } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';
import { QueryKeys as OrderQueryKeys } from '@api/orders/orders';

import { FetchError } from '../index';
import { DeliveryChangeData, Order } from './types/index';

export const useDeliveryChange = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Order>, FetchError, DeliveryChangeData>({
        mutationFn: delivery => {
            const { id, ...deliveryData } = delivery;

            return apiClient.patch(`orders/deliveries/${id}`, { data: deliveryData });
        },

        onSuccess: ({ data }) =>
            queryClient.invalidateQueries({
                queryKey: OrderQueryKeys.getOrder(data?.id),
            }),
    });
};
