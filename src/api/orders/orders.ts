import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, CommonSearchParams, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { FetchError } from '../index';
import {
    Order,
    OrderChangeData,
    OrderDeliveryChange,
    OrderFile,
    OrderItemChangeQtyRequest,
    OrderItemChangeQtyResponse,
    OrderItemsAddRequest,
    OrderItemsAddResponse,
    OrderItemsDeleteRequest,
    OrderSearchFilter,
    OrderSearchInclude,
} from './types/index';

const baseURL = 'orders/orders';

export const QueryKeys = {
    getOrder: (id?: number | string) => (id ? ['order', id] : ['order']),
    searchOrders: (data?: Record<string, any>) => (data ? ['orders', data] : ['orders']),
};

export const useOrdersSearch = (
    data: CommonSearchParams<OrderSearchFilter, string[], OrderSearchInclude>,
    enabled: boolean = true
) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Order[]>, FetchError>({
        enabled,
        queryKey: QueryKeys.searchOrders(data),
        queryFn: () => apiClient.post(`${baseURL}:search`, { data }),
    });
};

export const useOrderDetail = (id: number | string | undefined, include?: OrderSearchInclude, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Order>, FetchError>({
        enabled: isDetailIdValid(id) && enabled,
        queryKey: QueryKeys.getOrder(id),
        queryFn: () => apiClient.get(`${baseURL}/${id}${include ? `?include=${include.join(',')}` : ''}`),
    });
};

export const useOrderChange = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Order>, FetchError, OrderChangeData>({
        mutationFn: order => {
            const { id, ...orderData } = order;
            return apiClient.patch(`${baseURL}/${id}`, { data: orderData });
        },

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({
                queryKey: QueryKeys.getOrder(data?.id),
            });
            queryClient.invalidateQueries({
                queryKey: QueryKeys.searchOrders(),
            });
        },
    });
};
export const useOrderDeliveryChange = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Order>, FetchError, OrderDeliveryChange & { id: number }>({
        mutationFn: delivery => {
            const { id, ...deliveryData } = delivery;
            return apiClient.post(`${baseURL}/${id}:change-delivery`, { data: deliveryData });
        },

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({
                queryKey: QueryKeys.getOrder(data?.id),
            });
            queryClient.invalidateQueries({
                queryKey: QueryKeys.searchOrders(),
            });
        },
    });
};

export const useOrdersMeta = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<{ data: Meta }, FetchError>({
        queryKey: ['ordersMeta'],
        queryFn: () => apiClient.get(`${baseURL}:meta`),
        enabled,
    });
};

export const useOrdersAddFile = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<OrderFile>, FetchError, { id: number | string; file: File }>({
        mutationFn: ({ id, file }) => {
            const formData = new FormData();
            formData.append('file', file);
            return apiClient.post(`${baseURL}/${id}:attach-file`, { data: formData });
        },

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({
                queryKey: QueryKeys.getOrder(data?.id),
            });
            queryClient.invalidateQueries({
                queryKey: QueryKeys.searchOrders(),
            });
        },
    });
};

export const useOrdersDeleteFiles = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, { id: number | string; file_ids: number[] }>({
        mutationFn: ({ id, ...data }) => apiClient.delete(`${baseURL}/${id}:delete-files`, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: QueryKeys.getOrder(),
            });
            queryClient.invalidateQueries({
                queryKey: QueryKeys.searchOrders(),
            });
        },
    });
};

/** Добавление OrderItems в заказ */
export const useAddOrderItems = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<OrderItemsAddResponse, FetchError, { id: number | string } & OrderItemsAddRequest>({
        mutationFn: ({ id, ...data }) => apiClient.post(`${baseURL}/${id}:add-order-items`, { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({
                queryKey: QueryKeys.getOrder(data?.id),
            });
            queryClient.invalidateQueries({
                queryKey: QueryKeys.searchOrders(),
            });
        },
    });
};

/** Удаление OrderItems из заказа */
export const useDeleteOrderItems = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<OrderItemsAddResponse, FetchError, { id: number | string } & OrderItemsDeleteRequest>({
        mutationFn: ({ id, ...data }) => apiClient.post(`${baseURL}/${id}:delete-order-items`, { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({
                queryKey: QueryKeys.getOrder(data?.id),
            });
            queryClient.invalidateQueries({
                queryKey: QueryKeys.searchOrders(),
            });
        },
    });
};

/** Изменение количества OrderItem */
export const useChangeOrderItemQty = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<OrderItemChangeQtyResponse, FetchError, { id: number | string } & OrderItemChangeQtyRequest>({
        mutationFn: ({ id, ...data }) => apiClient.post(`${baseURL}/${id}:change-order-items-qty`, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: QueryKeys.getOrder(),
            });
            queryClient.invalidateQueries({
                queryKey: QueryKeys.searchOrders(),
            });
        },
    });
};
