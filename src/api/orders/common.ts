import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonOption, CommonResponse, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { FetchError } from '../index';
import { OMSSettings, OMSSettingsMutate } from './types';

export const useOrderStatuses = () => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: ['orderStatuses'],
        queryFn: () => apiClient.get('orders/order-statuses'),
    });
};

export const useOrderSources = (enabled: boolean = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: ['orderSources'],
        queryFn: () => apiClient.get('orders/order-sources'),
        enabled,
    });
};

export const usePaymentMethods = () => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: ['paymentMethods'],
        queryFn: () => apiClient.get('orders/payment-methods'),
    });
};

export const usePaymentStatuses = () => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: ['paymentStatuses'],
        queryFn: () => apiClient.get('orders/payment-statuses'),
    });
};

export const useDeliveryStatuses = () => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: ['deliveryStatuses'],
        queryFn: () => apiClient.get('orders/delivery-statuses'),
    });
};

export const useShipmentStatuses = () => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: ['shipmentStatuses'],
        queryFn: () => apiClient.get('orders/shipment-statuses'),
    });
};

export const useRefundStatuses = () => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: ['refundStatuses'],
        queryFn: () => apiClient.get('orders/refund-statuses'),
    });
};

const settingsUpdater = (
    prevSettings: CommonResponse<OMSSettings[]> | undefined,
    newSettings: CommonResponse<OMSSettings[]>
) =>
    prevSettings
        ? {
              ...prevSettings,
              data: prevSettings.data.map(setting => {
                  const newItem = newSettings.data.find(n => n.id === setting.id);
                  return newItem || setting;
              }),
          }
        : prevSettings;

const omsSettingsURL = 'orders/oms-settings';
const omsSettingsKey = 'orders-oms-settings';

export const useOmsSettings = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<OMSSettings[]>, FetchError>({
        queryKey: [omsSettingsKey],
        queryFn: () => apiClient.get(omsSettingsURL),
        enabled,
    });
};

export const useOmsSettingsMeta = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<{ data: Meta }, FetchError>({
        queryKey: [`${omsSettingsKey}-meta`],
        queryFn: () => apiClient.get(`${omsSettingsURL}:meta`),
        enabled,
    });
};

export const useUpdateOMSSettings = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<OMSSettings[]>, FetchError, OMSSettingsMutate[]>({
        mutationFn: data => apiClient.patch(omsSettingsURL, { data: { settings: data } }),

        onSuccess: newSettings => {
            queryClient.setQueryData<CommonResponse<OMSSettings[]> | undefined>([omsSettingsKey], prevSettings =>
                settingsUpdater(prevSettings, newSettings)
            );
        },
    });
};

const basketsSettingsURL = 'orders/baskets-settings';
const basketsSettingsKey = 'orders-baskets-settings';

export const useBasketsSettings = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<OMSSettings[]>, FetchError>({
        enabled,
        queryKey: [basketsSettingsKey],
        queryFn: () => apiClient.get(basketsSettingsURL),
    });
};

export const useBasketSettingsMeta = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<{ data: Meta }, FetchError>({
        queryKey: [`${basketsSettingsKey}-meta`],
        queryFn: () => apiClient.get(`${basketsSettingsURL}:meta`),
        enabled,
    });
};

export const useUpdateBasketsSettings = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<OMSSettings[]>, FetchError, OMSSettingsMutate[]>({
        mutationFn: data => apiClient.patch(basketsSettingsURL, { data: { settings: data } }),

        onSuccess: newSettings => {
            queryClient.setQueryData<CommonResponse<OMSSettings[]> | undefined>([basketsSettingsKey], prevSettings =>
                settingsUpdater(prevSettings, newSettings)
            );
        },
    });
};
