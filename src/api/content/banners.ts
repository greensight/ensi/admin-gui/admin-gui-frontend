import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, FetchError, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import {
    CreateBannerRequest,
    CreateBannerResponse,
    DeleteBannerFileRequest,
    DeleteBannerFileResponse,
    DeleteBannerRequest,
    DeleteBannerResponse,
    ReplaceBannerRequest,
    ReplaceBannerResponse,
    SearchBannerTypesRequest,
    SearchBannerTypesResponse,
    SearchBannersRequest,
    SearchBannersResponse,
    SearchOneBannerRequest,
    SearchOneBannerResponse,
    UploadBannerFileRequest,
    UploadBannerFileResponse,
} from './types/banners';

export const QueryKeys = {
    getBannersMeta: () => ['cms-get-banners-meta'],
    searchBannerTypes: (data?: Record<string, any>) =>
        data ? ['cms-post-banner-types-search', data] : ['cms-post-banner-types-search'],
    searchBanners: (data?: Record<string, any>) =>
        data ? ['cms-post-banners-search', data] : ['cms-post-banners-search'],
    searchOneBanner: (data?: Record<string, any>) =>
        data ? (['cms-post-banners-search-one', data] as const) : (['cms-post-banners-search-one'] as const),
};

/** Поиск объектов типа Banner */
export function useCmsBannersSearch(data: SearchBannersRequest, enabled = true) {
    const apiClient = useAuthApiClient();

    return useQuery<SearchBannersResponse, FetchError>({
        queryKey: QueryKeys.searchBanners(data),
        queryFn: () => apiClient.post(`cms/banners:search`, { data }),
        enabled,
    });
}

/** Поиск объекта типа Banner */
export function useCmsBannersSearchOne(data: SearchOneBannerRequest, enabled = true) {
    const apiClient = useAuthApiClient();

    return useQuery<SearchOneBannerResponse, FetchError>({
        queryKey: QueryKeys.searchOneBanner(data),
        queryFn: () => apiClient.post(`cms/banners:search-one`, { data }),
        enabled: isDetailIdValid(data.filter?.id) && enabled,
    });
}

/** Создание объекта типа Banner */
export function useCreateCmsBanner() {
    const apiClient = useAuthApiClient();
    const queryClient = useQueryClient();
    return useMutation<CreateBannerResponse, FetchError, CreateBannerRequest>({
        mutationFn: data => apiClient.post(`cms/banners`, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchBanners() });
        },
    });
}

/** Замена объекта типа Banner */
export function usePutCmsBanner() {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<ReplaceBannerResponse, FetchError, { id: number | string } & ReplaceBannerRequest>({
        mutationFn: ({ id, ...data }) => apiClient.put(`cms/banners/${id}`, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchBanners() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchOneBanner() });
        },
    });
}

/** Удаление объекта типа Banner */
export function useDeleteCmsBanner() {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<DeleteBannerResponse, FetchError, DeleteBannerRequest>({
        mutationFn: ({ id }) => apiClient.delete(`cms/banners/${id}`, {}),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchBanners() });
            queryClient.invalidateQueries({
                queryKey: QueryKeys.searchOneBanner(),
                refetchType: 'none',
            });
        },
    });
}

/** Получение списка доступных полей */
export function useCmsBannersMeta(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<Meta>, FetchError>({
        queryKey: QueryKeys.getBannersMeta(),
        queryFn: () => apiClient.get(`cms/banners:meta`, {}),
        enabled,
    });
}

/** Загрузка файла для объекта типа Banner */
export function useCmsBannerUploadFile() {
    const apiClient = useAuthApiClient();
    const queryClient = useQueryClient();

    return useMutation<UploadBannerFileResponse, FetchError, { id: number | string } & UploadBannerFileRequest>({
        mutationFn: ({ id, ...data }) => apiClient.post(`cms/banners/${id}:upload-file`, { data: data.formData }),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchBanners() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchOneBanner() });
        },
    });
}

/** Удаление файла для объекта типа Banner */
export function useCmsBannerDeleteFile() {
    const apiClient = useAuthApiClient();
    const queryClient = useQueryClient();

    return useMutation<DeleteBannerFileResponse, FetchError, { id: number | string } & DeleteBannerFileRequest>({
        mutationFn: ({ id, ...data }) => apiClient.post(`cms/banners/${id}:delete-file`, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchBanners() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchOneBanner() });
        },
    });
}

/** Поиск объектов типа BannerType */
export function useCmsBannerTypesSearch(data: SearchBannerTypesRequest, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<SearchBannerTypesResponse, FetchError>({
        queryKey: QueryKeys.searchBannerTypes(data),
        queryFn: () => apiClient.post(`cms/banner-types:search`, { data }),
        enabled,
    });
}
