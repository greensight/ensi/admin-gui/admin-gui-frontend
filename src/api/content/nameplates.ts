import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { FetchError } from '..';
import {
    CreateNameplateRequest,
    CreateNameplateResponse,
    GetNameplateResponse,
    NameplateBindProductsRequest,
    SearchNameplateProductsRequest,
    SearchNameplateProductsResponse,
    SearchNameplatesRequest,
    SearchNameplatesResponse,
    UpdateNameplateRequest,
    UpdateNameplateResponse,
} from './types/nameplates';

const API_URL = 'cms/nameplates';

const QueryKeys = {
    LIST: 'cms-nameplates',
    DETAILS: (id?: number) => (typeof id === 'number' ? ['cms-nameplate', id] : ['cms-nameplate']),
    META: 'cms-nameplates-meta',
    PRODUCT_LIST: 'cms-nameplate-products',
    PRODUCT_META: 'cms-nameplate-products-meta',
};

export const useNameplate = (id: number, include: string[] = [], enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<GetNameplateResponse, FetchError>({
        enabled: isDetailIdValid(id) && enabled,
        queryKey: QueryKeys.DETAILS(id),
        queryFn: () =>
            apiClient.get(`${API_URL}/${id}`, {
                params: {
                    include,
                },
            }),
    });
};

export const useSearchNameplates = (data: SearchNameplatesRequest, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<SearchNameplatesResponse, FetchError>({
        enabled,
        queryKey: [QueryKeys.LIST, data],
        queryFn: () => apiClient.post(`${API_URL}:search`, { data }),
    });
};

export const useNameplatesMeta = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<{ data: Meta }, FetchError>({
        enabled,
        queryKey: [QueryKeys.META],
        queryFn: () => apiClient.get(`${API_URL}:meta`),
    });
};

export const useNameplateProductsMeta = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<{ data: Meta }, FetchError>({
        enabled,
        queryKey: [QueryKeys.PRODUCT_META],
        queryFn: () => apiClient.get(`${API_URL}/nameplate-products:meta`),
    });
};

export const useSearchNameplateProducts = (
    data: SearchNameplateProductsRequest,
    includes: string[] = [],
    enabled = true
) => {
    const apiClient = useAuthApiClient();

    return useQuery<SearchNameplateProductsResponse, FetchError>({
        enabled,
        queryKey: [QueryKeys.PRODUCT_LIST, data],
        queryFn: () =>
            apiClient.post(`${API_URL}/nameplate-products:search`, {
                params: {
                    include: includes,
                },
                data,
            }),
    });
};

export const useCreateNameplate = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CreateNameplateResponse, FetchError, CreateNameplateRequest>({
        mutationFn: data => apiClient.post(`${API_URL}`, { data }),

        onSuccess: response => {
            queryClient.invalidateQueries({
                queryKey: [QueryKeys.LIST],
            });
            queryClient.invalidateQueries({
                queryKey: QueryKeys.DETAILS(response?.data?.id),
            });
        },
    });
};

export const useUpdateNameplate = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<UpdateNameplateResponse, FetchError, UpdateNameplateRequest>({
        mutationFn: ({ id, ...data }) => apiClient.patch(`${API_URL}/${id}`, { data }),

        onSuccess: response => {
            queryClient.invalidateQueries({
                queryKey: [QueryKeys.LIST],
            });
            queryClient.invalidateQueries({
                queryKey: QueryKeys.DETAILS(response?.data?.id),
            });
        },
    });
};

export const useDeleteNameplate = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<UpdateNameplateResponse, FetchError, { id: number }>({
        mutationFn: ({ id }) => apiClient.delete(`${API_URL}/${id}`),

        onSuccess: response => {
            queryClient.invalidateQueries({
                queryKey: [QueryKeys.LIST],
            });
            queryClient.invalidateQueries({
                queryKey: QueryKeys.DETAILS(response?.data?.id),
                refetchType: 'none',
            });
        },
    });
};

export const useBindNameplateProducts = (nameplateId: number) => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, NameplateBindProductsRequest>({
        mutationFn: data => apiClient.post(`${API_URL}/${nameplateId}:add-products`, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: [QueryKeys.PRODUCT_LIST],
            });
        },
    });
};

export const useUnbindNameplateProducts = (nameplateId: number) => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, NameplateBindProductsRequest>({
        mutationFn: data => apiClient.delete(`${API_URL}/${nameplateId}:delete-products`, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: [QueryKeys.PRODUCT_LIST],
            });
        },
    });
};
