export interface PageFilter {}

export interface Page {
    id: number;
    name: string;
    is_active: boolean;
    slug: string;
    active_from: string;
    active_to: string;
    content: string;
    created_at: string;
    updated_at: string;
}

export interface PageMutate {
    name?: string;
    is_active?: boolean;
    slug?: string;
    active_from?: string | Date | number | null;
    active_to?: string | Date | number | null;
    content?: string;
}

export interface PageMutateWithId extends PageMutate {
    id: number;
}
