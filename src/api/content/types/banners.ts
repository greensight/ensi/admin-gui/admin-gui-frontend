import { CommonResponse, CommonSearchParams } from '@api/common/types';

import { BannerButtonLocation, BannerButtonType, BannerCode } from '@scripts/enums';

export type DeleteBannerRequest = { id: number | string };

export type DeleteBannerResponse = CommonResponse<{}>;

type BannerFilter = Partial<{
    id?: number;
    name?: string;
    is_active?: boolean;
    type_id?: number;
    button_id?: number;
}>;

export type SearchBannersRequest = CommonSearchParams<BannerFilter, string | string[]>;

export interface NewBanner {
    /**
     * Название
     */
    name: string;

    /**
     * Активность
     */
    is_active: boolean;

    sort?: number;
    url?: string;
    type_id?: number;

    button?: {
        url: string;
        text: string;
        location: BannerButtonLocation;
        type: BannerButtonType;
    };
}

export interface BannerButton {
    url: string;
    text: string;
    location: BannerButtonLocation;
    type: BannerButtonType;
}

export interface BannerType {
    /**
     * Идентификатор
     */
    id: number;

    /**
     * Код (значение из CmsBannerTypeEnum)
     */
    code: BannerCode;
    /**
     * Активность
     */
    active: boolean;
    /**
     * Значение
     */
    name: string;
}

export interface Banner extends NewBanner {
    /**
     * Идентификатор
     */
    id: number;
    /**
     * Код баннера
     */
    code?: string;
    desktop_image?: string;
    mobile_image?: string;
    button_id?: number;
    /**
     * Дата создания
     */
    created_at?: string;
    /**
     * Дата обновления
     */
    updated_at?: string;

    /**
     * Значение сортировки
     */
    sort?: number;
    url?: string;
    type_id?: number;

    button: BannerButton;

    type: BannerType;
}

export type SearchBannersResponse = CommonResponse<Banner[]>;

export type SearchOneBannerRequest = CommonSearchParams<
    {
        id?: number;
        name?: string;
        is_active?: boolean;
        type_id?: number;
        button_id?: number;
    },
    ('id' | 'name' | 'is_active')[],
    ('type' | 'button')[]
>;

export type SearchOneBannerResponse = CommonResponse<Banner>;

export type CreateBannerRequest = NewBanner;
export type CreateBannerResponse = CommonResponse<Banner>;

export type ReplaceBannerRequest = NewBanner;
export type ReplaceBannerResponse = CommonResponse<Banner>;

export type UploadBannerFileRequest = {
    formData: FormData;
    id: number;
};

export type UploadBannerFileResponse = CommonResponse<{
    url: string;
}>;

export interface DeleteBannerFileRequest {
    /**
     * Тип изображения (значение из CmsBannerImageTypeEnum)
     */
    type?: 'desktop' | 'tablet' | 'mobile';
}

export type DeleteBannerFileResponse = CommonResponse<null>;

export type SearchBannerTypesRequest = CommonSearchParams<
    {
        id?: number;
        name?: string;
        active?: boolean;
    },
    ('id' | 'name' | 'active' | 'code')[]
>;

export type SearchBannerTypesResponse = CommonResponse<BannerType[]>;
