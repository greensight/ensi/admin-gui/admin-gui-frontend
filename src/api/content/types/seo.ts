import { CommonResponse, CommonSearchParams, OffsetPaginationQuery } from '@api/common/types';

export interface SeoTemplateTypesData {
    /**
     * ID типа из CmsSeoTemplateTypeEnum
     */
    id: number;
    /**
     * Описание типа
     */
    name: string;
}

/**
 * Получение объектов типа CmsSeoVariable */
export type GetSeoTemplateTypesResponse = CommonResponse<SeoTemplateTypesData[]>;

export interface VariablesData {
    /**
     * SEO-переменная для шаблона из CmsSeoVariableEnum
     */
    id: string;
    /**
     * Описание SEO-переменной
     */
    name: string;
}

/**
 * Получение объектов типа CmsSeoTemplateType */
export type GetVariablesResponse = CommonResponse<VariablesData[]>;

/**
 * Доступные типы шаблонов:
 *
 * `1` - шаблон на конкретный товар
 *
 * `2` - общий шаблон на товар
 */
export enum CmsSeoTemplateTypeEnum {
    PRODUCT = 1,
    PRODUCT_DEFAULT = 2,
}

type Prettify<T> = {
    [K in keyof T]: T[K];
};

type WithRequired<T, K extends keyof T> = {
    [P in keyof T]: P extends K ? NonNullable<T[P]> : T[P] | null | undefined;
};

interface IdsObject {
    ids: number[];
}

export interface SeoTemplateFillableProperties {
    /**
     * Название шаблона
     * @example "Шаблон по умолчанию"
     */
    name: string;
    /**
     * Тип шаблона из SeoTemplateTypeEnum
     * @example 1
     */
    type: CmsSeoTemplateTypeEnum;
    /**
     * Заголовок h1
     * @example "Шаблон по умолчанию"
     */
    header: string;
    /**
     * Заголовок окна браузера
     * @example "Купить {название товара} с доставкой по цене {стоимость} руб в интернет-магазине."
     */
    title?: string;
    /**
     * Описание страницы, мета-тег description
     * @example "{название товара}. Описание товара"
     */
    description?: string;
    /**
     * SEO-текст. Выводится над футером в SEO-блоке.
     * @example "{название товара}. Описание товара"
     */
    seo_text?: string;
    /**
     * Активность шаблона
     * @example true
     */
    is_active: boolean;
}

/**
 * Создание объекта типа SeoTemplate */
export type SeoTemplateForCreate = Prettify<
    WithRequired<SeoTemplateFillableProperties, 'name' | 'type' | 'header' | 'is_active'>
>;

export interface SeoTemplateReadonlyProperties {
    /**
     * Идентификатор
     */
    id: number;
    /**
     * Дата создания SEO-шаблона
     * @example "2021-12-20T18:00:10.000000Z"
     */
    created_at: string;
    /**
     * Дата обновления SEO-шаблона
     * @example "2021-12-20T18:00:10.000000Z"
     */
    updated_at: string;
}

export type SeoTemplate = Prettify<SeoTemplateReadonlyProperties & SeoTemplateFillableProperties>;
export type SeoTemplateResponse = CommonResponse<SeoTemplate>;

/**
 * Привязка продукта к SeoTemplate */

export type AddSeoTemplateProductsRequest = Prettify<IdsObject>;
/**
 * Удаления связи у продукта и SeoTemplate */
export type DeleteSeoTemplateProductsRequest = Prettify<IdsObject>;
/**
 * Обновления части полей объекта типа SeoTemplate */
export type SeoTemplateForPatch = Prettify<Partial<SeoTemplateFillableProperties>>;

/**
 * Поиск объектов типа SeoTemplate */
export type SearchSeoTemplatesRequest = CommonSearchParams<Partial<SeoTemplate>, string | string[]>;

export type SearchSeoTemplatesResponse = CommonResponse<SeoTemplate[]>;

export interface SeoTemplateProductReadonlyProperties {
    /**
     * Идентификатор
     */
    id: number;
    /**
     * Идентификатор SEO-шаблона
     */
    template_id: number;
    /**
     * Дата создания
     * @example "2021-12-20T18:00:10.000000Z"
     */
    created_at: string;
    /**
     * Дата обновления
     * @example "2021-12-20T18:00:10.000000Z"
     */
    updated_at: string;
}

export interface SeoTemplateProductFillableProperties {
    /**
     * Идентификатор товара
     * @example 2
     */
    product_id?: number;
}

export type SeoTemplateProduct = Prettify<SeoTemplateProductReadonlyProperties & SeoTemplateProductFillableProperties>;

/**
 * Получение объекта типа SeoTemplateProduct */
export type SeoTemplateProductResponse = CommonResponse<SeoTemplateProduct>;

/**
 * Поиск объектов типа SeoTemplateProduct */

export interface SearchSeoTemplateProductsRequest {
    sort?: string[];
    filter: Record<string, any>;
    include?: string[];
    pagination: OffsetPaginationQuery;
}

export type SearchSeoTemplateProductsResponse = CommonResponse<SeoTemplateProduct[]>;
