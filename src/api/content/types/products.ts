import { CommonResponse } from '@api/common/types';

export type ProductAddNameplatesRequest = { productId: number; ids: number[] };
export type ProductAddNameplatesResponse = CommonResponse<null>;

export type ProductDeleteNameplatesRequest = { productId: number; ids: number[] };
export type ProductDeleteNameplatesResponse = CommonResponse<null>;
