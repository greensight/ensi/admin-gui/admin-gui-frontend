import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { FetchError } from '../index';
import {
    MenuSearchOneParams,
    MenuSearchOneResponse,
    MenuSearchParams,
    MenuSearchResponse,
    MenuUpdateTreeParams,
    MenuUpdateTreeResponse,
} from './types/menus';

const MENU_BASE_URL = 'cms/menus';

export const useMenus = (data: MenuSearchParams) => {
    const apiClient = useAuthApiClient();

    return useQuery<MenuSearchResponse, FetchError>({
        queryKey: ['cms-menus', data],
        queryFn: () => apiClient.post(`${MENU_BASE_URL}:search`, { data }),
    });
};

export const useMenu = (data: MenuSearchOneParams) => {
    const apiClient = useAuthApiClient();

    return useQuery<MenuSearchOneResponse | undefined, FetchError>({
        enabled: isDetailIdValid(data.filter?.id),
        queryKey: [`cms-menu`, data.filter?.id],
        queryFn: () => {
            if (!data.filter?.id)
                return new Promise(resolve => {
                    resolve(undefined);
                });
            return apiClient.post(`${MENU_BASE_URL}:search-one`, { data });
        },
    });
};

export const useUpdateMenuTree = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<
        MenuUpdateTreeResponse,
        FetchError,
        MenuUpdateTreeParams & {
            id: number;
        }
    >({
        mutationFn: data => apiClient.put(`${MENU_BASE_URL}/${data.id}/trees`, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: ['cms-menus'],
            });
            queryClient.invalidateQueries({
                queryKey: ['cms-menu'],
            });
        },
    });
};
