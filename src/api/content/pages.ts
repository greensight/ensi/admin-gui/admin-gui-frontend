import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, CommonSearchParams, FetchError, Meta } from '@api/common/types';
import { Page, PageFilter, PageMutate, PageMutateWithId } from '@api/content/types/pages';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

const PAGE_URL = 'cms/pages';
const PAGE_KEY = 'page';
const LIST_KEY = `${PAGE_KEY}s`;

export const usePages = (data: CommonSearchParams<PageFilter, string>, enabled: boolean = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Page[]>, FetchError>({
        enabled,
        queryKey: [LIST_KEY, data],
        queryFn: () => apiClient.post(`${PAGE_URL}:search`, { data }),
    });
};

export const usePageDetail = (id?: number | string) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Page>, FetchError>({
        enabled: isDetailIdValid(id),
        queryKey: [PAGE_KEY, id],
        queryFn: () => apiClient.get(`${PAGE_URL}/${id}`),
    });
};

export const usePagesMeta = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<{ data: Meta }, FetchError>({
        queryKey: ['pagesMeta'],
        queryFn: () => apiClient.get(`${PAGE_URL}:meta`),
        enabled,
    });
};

export const useCreatePage = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Page>, FetchError, PageMutate>({
        mutationFn: data => apiClient.post(PAGE_URL, { data }),

        onSuccess: data => {
            queryClient.setQueryData([PAGE_KEY, `${data?.data?.id}`], data);
            queryClient.invalidateQueries({
                queryKey: [LIST_KEY],
            });
        },
    });
};

export const usePageUpdate = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Page>, FetchError, PageMutateWithId>({
        mutationFn: page => {
            const { id, ...pageData } = page;
            return apiClient.patch(`${PAGE_URL}/${id}`, { data: pageData });
        },

        onSuccess: data => {
            queryClient.setQueryData([PAGE_KEY, `${data?.data?.id}`], data);
            queryClient.invalidateQueries({
                queryKey: [LIST_KEY],
            });
        },
    });
};

export const usePageDelete = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Page>, FetchError, number>({
        mutationFn: id => apiClient.delete(`${PAGE_URL}/${id}`),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [LIST_KEY],
            }),
    });
};
