import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { FetchError } from '..';
import {
    AddSeoTemplateProductsRequest,
    DeleteSeoTemplateProductsRequest,
    GetSeoTemplateTypesResponse,
    GetVariablesResponse,
    SearchSeoTemplateProductsRequest,
    SearchSeoTemplateProductsResponse,
    SearchSeoTemplatesRequest,
    SearchSeoTemplatesResponse,
    SeoTemplateForCreate,
    SeoTemplateForPatch,
    SeoTemplateProductResponse,
    SeoTemplateResponse,
} from './types/seo';

export const QueryKeys = {
    getSeoTemplate: (id?: number | string) => (id ? ['get-seo-template', id] : ['get-seo-template']),
    searchSeoTemplates: (data?: Record<string, any>) =>
        data ? ['search-seo-templates', data] : ['search-seo-templates'],
    getSeoTemplatesMeta: () => ['get-seo-templates-meta'],
    getTemplateTypes: () => ['get-template-types'],
    getTemplateVariables: () => ['get-template-variables'],
    getSeoTemplateProduct: (id?: number | string) =>
        id ? ['get-seo-template-product', id] : ['get-seo-template-product'],
    searchSeoTemplateProducts: (data?: Record<string, any>) =>
        data ? ['search-seo-template-products', data] : ['search-seo-template-products'],
    getSeoTemplateProductsMeta: () => ['get-seo-template-products-meta'],
};

/** Создание объекта типа SeoTemplate */
export const useCreateSeoTemplate = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<SeoTemplateResponse, FetchError, SeoTemplateForCreate>({
        mutationFn: data => apiClient.post('cms/seo/templates', { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchSeoTemplates() });
        },
    });
};

/** Привязка продукта к SeoTemplate */
export const useAddSeoTemplateProducts = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<CommonResponse<any>, FetchError, { id: number | string } & AddSeoTemplateProductsRequest>({
        mutationFn: ({ id, ...data }) => apiClient.post(`cms/seo/templates/${id}:add-products`, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchSeoTemplates() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchSeoTemplateProducts() });
        },
    });
};

/** Удаления связи у продукта и SeoTemplate */
export const useDeleteSeoTemplateProducts = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<CommonResponse<any>, FetchError, { id: number | string } & DeleteSeoTemplateProductsRequest>({
        mutationFn: ({ id, ...data }) => apiClient.delete(`cms/seo/templates/${id}:delete-products`, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchSeoTemplates() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchSeoTemplateProducts() });
        },
    });
};

/** Получение объекта типа SeoTemplate */
export function useSeoTemplate({ id }: { id: number | string }, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<SeoTemplateResponse, FetchError>({
        queryKey: QueryKeys.getSeoTemplate(id),
        queryFn: () => apiClient.get(`cms/seo/templates/${id}`, {}),
        enabled: isDetailIdValid(id) && enabled,
    });
}

/** Обновления части полей объекта типа SeoTemplate */
export const usePatchSeoTemplate = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<SeoTemplateResponse, FetchError, { id: number | string } & SeoTemplateForPatch>({
        mutationFn: ({ id, ...data }) => apiClient.patch(`cms/seo/templates/${id}`, { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.getSeoTemplate(data?.id) });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchSeoTemplates() });
        },
    });
};

/** Удаление объекта типа SeoTemplate */
export const useDeleteSeoTemplate = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<CommonResponse<any>, FetchError, { id: number | string }>({
        mutationFn: ({ id }) => apiClient.delete(`cms/seo/templates/${id}`, {}),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchSeoTemplates() });
        },
    });
};

/** Поиск объектов типа SeoTemplate */
export function useSearchSeoTemplates(data: SearchSeoTemplatesRequest, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<SearchSeoTemplatesResponse, FetchError>({
        queryKey: QueryKeys.searchSeoTemplates(data),
        queryFn: () => apiClient.post('cms/seo/templates:search', { data }),
        enabled,
    });
}

/** Получение списка доступных полей */
export function useSeoTemplatesMeta(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<Meta>, FetchError>({
        queryKey: QueryKeys.getSeoTemplatesMeta(),
        queryFn: () => apiClient.get('cms/seo/templates:meta', {}),
        enabled,
    });
}

/** Получение объектов типа CmsSeoVariable */
export function useSeoTemplateTypes(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<GetSeoTemplateTypesResponse, FetchError>({
        queryKey: QueryKeys.getTemplateTypes(),
        queryFn: () => apiClient.get('cms/seo/template-types', {}),
        enabled,
    });
}

/** Получение объектов типа CmsSeoTemplateType */
export function useSeoTemplateVariables(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<GetVariablesResponse, FetchError>({
        queryKey: QueryKeys.getTemplateVariables(),
        queryFn: () => apiClient.get('cms/seo/template-variables', {}),
        enabled,
    });
}

/** Получение объекта типа SeoTemplateProduct */
export function useSeoTemplateProduct({ id }: { id: number | string }, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<SeoTemplateProductResponse, FetchError>({
        queryKey: QueryKeys.getSeoTemplateProduct(id),
        queryFn: () => apiClient.get(`cms/seo/template-products/${id}`, {}),
        enabled,
    });
}

/** Поиск объектов типа SeoTemplateProduct */
export function useSearchSeoTemplateProducts(data: SearchSeoTemplateProductsRequest, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<SearchSeoTemplateProductsResponse, FetchError>({
        queryKey: QueryKeys.searchSeoTemplateProducts(data),
        queryFn: () => apiClient.post('cms/seo/template-products:search', { data }),
        enabled,
    });
}

/** Получение списка доступных полей */
export function useSeoTemplateProductsMeta(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<Meta>, FetchError>({
        queryKey: QueryKeys.getSeoTemplateProductsMeta(),
        queryFn: () => apiClient.get('cms/seo/template-products:meta', {}),
        enabled,
    });
}
