import { useMutation, useQueryClient } from '@tanstack/react-query';

import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { FetchError } from '..';
import {
    ProductAddNameplatesRequest,
    ProductAddNameplatesResponse,
    ProductDeleteNameplatesRequest,
    ProductDeleteNameplatesResponse,
} from './types/products';

const API_URL = 'cms/products';

export const useProductAddNameplates = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<ProductAddNameplatesResponse, FetchError, ProductAddNameplatesRequest>({
        mutationFn: data => apiClient.post(`${API_URL}/${data.productId}:add-nameplates`, { data }),
        onSuccess: (data, variables) => {
            queryClient.invalidateQueries({
                queryKey: variables.productId
                    ? ['cms-nameplates', { filter: { product_id: variables.productId } }]
                    : ['cms-nameplates'],
            });
        },
    });
};

export const useProductDeleteNameplates = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<ProductDeleteNameplatesResponse, FetchError, ProductDeleteNameplatesRequest>({
        mutationFn: data => apiClient.delete(`${API_URL}/${data.productId}:delete-nameplates`, { data }),
        onSuccess: (data, variables) => {
            queryClient.invalidateQueries({
                queryKey: variables.productId
                    ? ['cms-nameplates', { filter: { product_id: variables.productId } }]
                    : ['cms-nameplates'],
            });
        },
    });
};
