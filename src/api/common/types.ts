export interface CursorPagination {
    cursor: string;
    limit: number;
    next_cursor: string;
    previous_cursor: string;
    type: 'cursor';
}

export interface OffsetPaginationQuery {
    limit: number;
    type: 'offset';
    offset: number;
}

export interface OffsetPagination extends OffsetPaginationQuery {
    total: number;
}
export interface CommonOption {
    id: number;
    name: string;
}

export interface ApiError {
    code: string;
    message: string;
    meta?: {
        description: string;
    };
}

export interface FileType {
    /** relative path */
    path: string;
    /** file name */
    name: string;
    /** full url */
    url: string;
}

export interface CommonResponse<T> {
    data: T;
    meta: { pagination?: OffsetPagination };
    errors?: ApiError[];
}

export interface FileUpload {
    id: number;
    file: FormData;
}

export interface CommonSearchParams<F, S = string[], I = string[]> {
    filter?: F;
    sort?: S;
    include?: I;
    pagination?: OffsetPaginationQuery;
}

export type ImmutableFields = 'id' | 'created_at' | 'updated_at';

export class FetchError extends Error {
    constructor(
        public message: string,
        public status: number,
        public code: string
    ) {
        super(message);
        this.status = status;
        this.code = code;
    }
}

export interface Config {
    token?: string;
    data?: any;
    headers?: Record<string, string>;
    params?: any;
    method?: 'GET' | 'POST' | 'PATCH' | 'DELETE' | 'PUT';
    timeout?: number;
    abortController?: AbortController;
}

export type Client = (endpoint: string, config?: Config) => Promise<any>;

export type FieldTypeEnum =
    | 'string'
    | 'email'
    | 'phone'
    | 'datetime'
    | 'date'
    | 'url'
    | 'int'
    | 'price'
    | 'float'
    | 'enum'
    | 'bool'
    | 'photo'
    | 'plural_numeric';

export const NumericFieldTypes: FieldTypeEnum[] = ['int', 'float'];

export type FieldFilterTypeEnum = 'default' | 'like' | 'many' | 'range';

export type MetaEnumValue = { id: string | number; title: string };

export type MetaField = {
    /**
     * Символьный код поля
     * @example id
     * @example product.name
     */
    code: string;

    /**
     * Тип фильтра по данному полю из @see FieldFilterTypeEnum
     * @example default
     */
    filter: FieldFilterTypeEnum | null;

    /**
     * @deprecated
     */
    filter_name?: string;

    /**
     * Если фильтр обычный (default), то это поле -
     * ключ, под которым необходимо передавать значение
     */
    filter_key?: string;

    /**
     * Доступно ли поле для вывода в таблицу
     */
    is_object: boolean;
    include?: string | null;
    list: boolean;

    /**
     * Название поля на русском языке
     * @example Название
     */
    name: string;

    /**
     * Доступно ли поле для сортировки
     */
    sort: boolean;

    /**
     * Ключ, который необходимо передавать для сортировки по данному полю
     */
    sort_key?: string;

    /**
     * Тип поля из FieldTypeEnum
     */
    type: FieldTypeEnum;

    /**
     * Поле для директорий
     */
    enum_info?: {
        endpoint?: string;
        values?: MetaEnumValue[];
    };

    /**
     * Если тип фильтра по диапазону (range) -
     * ключ, под которым необходимо передавать значение ОТ
     */
    filter_range_key_from?: string;

    /**
     * Если тип фильтра по диапазону (range) -
     * ключ, под которым необходимо передавать значение ДО
     */
    filter_range_key_to?: string;
    // Для типа plural_numeric
    value_types?: {
        /** Лейбл */
        name: string;
        /** Как рендерить компонент */
        type: FieldTypeEnum;
        /**
         * Ключ в массиве данных
         */
        field: string;
        /** Значение */
        field_value: string;
    }[];
};

export type Meta = {
    fields: MetaField[];
    detail_link: string;
    default_sort: string;
    default_list: string[];
    default_filter: string[];
};

export type FilteredItem = { id: string; title: string };

export type FilterResponse = FilteredItem[];

export interface FilterSearchParam {
    filter: {
        query: string;
        id: (number | string)[];
    };
}

export type UniqueItems<T extends string, U extends string[] = []> =
    | U
    | {
          [K in T]: UniqueItems<Exclude<T, K>, [...U, K]>;
      }[T];

export type Prettify<T> = {
    [K in keyof T]: T[K];
};

export type RequireKeys<T extends object, K extends keyof T> = Required<Pick<T, K>> & Omit<T, K>;

export type PartialNullable<T> = {
    [P in keyof T]?: T[P] | null;
};
