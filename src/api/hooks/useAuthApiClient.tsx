import { useMemo } from 'react';

import { useAuth } from '@context/auth';

import { APIClient } from '..';

export const useAuthApiClient = (url: string = '/api/v1/') => {
    const { tokenData, setTokenData } = useAuth();

    return useMemo(() => new APIClient(url, tokenData, setTokenData), [url, tokenData, setTokenData]);
};
