import { CommonOption } from '@api/common/types';

import { CustomerGender, CustomerStatus } from '@scripts/enums';

import { Address } from './addresses';
import { User } from './users';

export interface CustomersFilter {
    id?: number | null;
    user_id?: number | null;
    status?: CustomerStatus | null;
}

export interface CustomerMutate {
    user_id: number;
    status_id?: CustomerStatus | null;
    manager_id?: number | null;
    yandex_metric_id?: string | null;
    google_analytics_id?: string | null;
    attribute_ids?: number[] | null;
    active?: boolean;
    email: string;
    phone: string;
    last_name?: string | null;
    first_name?: string | null;
    middle_name?: string | null;
    gender?: CustomerGender | null;
    create_by_admin: boolean;
    city?: string | null;
    birthday?: string;
    last_visit_date?: string | null;
    comment_status?: string | null;
    timezone: string;
}
export type CustomerForPatch = Partial<CustomerMutate> & {
    id: number;
};
export interface Customer extends CustomerMutate {
    id: number;
    avatar?: string;
    addresses?: Address;
    user?: User;
    status: CommonOption[];
    attributes: CommonOption[];

    created_at: string;
    updated_at: string;

    is_deleted: boolean;

    // Дата удаления, ISO timestamp
    delete_request: string;
}
