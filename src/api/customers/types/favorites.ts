import { Product } from '@api/catalog';

export interface FavoriteMutate {
    product_ids: number[];
    customer_id: number;
}

export interface Favorite extends FavoriteMutate {
    id: number;
    product: Product;
}

export interface FavoriteFilter {
    product_id?: number;
    customer_id?: number;
}
