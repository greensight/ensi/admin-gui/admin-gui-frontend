import { useQuery } from '@tanstack/react-query';

import { FetchError, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

export const useCustomerOrdersMeta = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<{ data: Meta }, FetchError>({
        queryKey: ['customerOrdersMeta'],
        queryFn: () => apiClient.get('customers/orders:meta'),
        enabled,
    });
};
