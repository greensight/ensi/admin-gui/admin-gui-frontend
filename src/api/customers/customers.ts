import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, CommonSearchParams, FileUpload, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { FetchError } from '..';
import { Customer, CustomerForPatch, CustomerMutate, CustomersFilter } from './types/customers';

const CUSTOMER_URL = 'customers/customers';

export const useCustomers = (data: CommonSearchParams<CustomersFilter, string | string[]> = {}, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Customer[]>, FetchError>({
        queryKey: ['customers', data],
        queryFn: () => apiClient.post(`${CUSTOMER_URL}:search`, { data }),
        enabled,
    });
};

export const useCustomersMeta = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Meta>, FetchError>({
        queryKey: ['customers-meta'],
        queryFn: () => apiClient.get(`${CUSTOMER_URL}:meta`),
        enabled,
    });
};

export const useCustomer = (id: string | number | undefined | null, enabled: boolean = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Customer>, FetchError>({
        queryKey: ['customer', id],
        queryFn: () => apiClient.get(`${CUSTOMER_URL}/${id}`),
        enabled: isDetailIdValid(id || undefined) && enabled,
    });
};

export const useCreateCustomer = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Customer>, FetchError, CustomerMutate>({
        mutationFn: data => apiClient.post(CUSTOMER_URL, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: ['customers'],
            });
        },
    });
};

export const usePatchCustomer = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Customer>, FetchError, CustomerForPatch>({
        mutationFn: ({ id, ...data }) => apiClient.patch(`${CUSTOMER_URL}/${id}`, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: ['customers'],
            });
            queryClient.invalidateQueries({
                queryKey: ['customer'],
            });
        },
    });
};

export const useChangeCustomerStatus = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Customer>, FetchError, number>({
        mutationFn: id => apiClient.post(`${CUSTOMER_URL}/${id}:change-status-if-profile-filled`),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: ['customers'],
            });
            queryClient.invalidateQueries({
                queryKey: ['customer'],
            });
        },
    });
};

export const useUploadCustomerAvatar = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Customer>, FetchError, FileUpload>({
        mutationFn: ({ id, file }) => apiClient.post(`${CUSTOMER_URL}/${id}:upload-avatar`, { data: file }),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: ['customers'],
            });
            queryClient.invalidateQueries({
                queryKey: ['customer'],
            });
        },
    });
};

export const useDeleteCustomerAvatar = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Customer>, FetchError, number>({
        mutationFn: id => apiClient.post(`${CUSTOMER_URL}/${id}:delete-avatar`),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: ['customers'],
            });
            queryClient.invalidateQueries({
                queryKey: ['customer'],
            });
        },
    });
};

export const useCustomerDeletePersonalData = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<any>, FetchError, number>({
        mutationFn: id => apiClient.post(`${CUSTOMER_URL}/${id}:delete-personal-data`),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: ['customers'],
            });
            queryClient.invalidateQueries({
                queryKey: ['customer'],
            });
        },
    });
};
