import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, CommonSearchParams } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { FetchError } from '..';
import { Favorite, FavoriteFilter, FavoriteMutate } from './types';

const FAVORITE_URL = 'customers/favorites';
const QUERY_KEY = 'favorites';

export const useFavorites = (data: CommonSearchParams<FavoriteFilter> & { enabled?: boolean } = { enabled: true }) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Favorite[]>, FetchError>({
        queryKey: [QUERY_KEY, data],
        queryFn: () => apiClient.post(`${FAVORITE_URL}:search`, { data }),
        enabled: data.enabled,
    });
};

/**
 * Search for products added to favorites (for validation)
 */
export const useSearchFavorites = () => {
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Favorite[]>, FetchError, CommonSearchParams<FavoriteFilter>>({
        mutationFn: data => apiClient.post(`${FAVORITE_URL}:search`, { data }),
    });
};

export const useCreateMassFavorite = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Favorite>, FetchError, FavoriteMutate>({
        mutationFn: data => apiClient.post(FAVORITE_URL, { data }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY],
            }),
    });
};

export const useDeleteMassFavorite = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, FavoriteMutate>({
        mutationFn: data => apiClient.post(`${FAVORITE_URL}:delete-product`, { data }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY],
            }),
    });
};

export const useDeleteAllFavorites = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, { customer_id: number }>({
        mutationFn: data => apiClient.post(`${FAVORITE_URL}:clear`, { data }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY],
            }),
    });
};
