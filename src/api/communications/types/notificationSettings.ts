import { CommonResponse, CommonSearchParams, Prettify, RequireKeys } from '@api/common/types';

export interface NotificationSettingFillableProperties {
    /**
     * Название уведомления
     * @example "Создание заказа"
     */
    name?: string;
    /**
     * Идентификатор события уведомления из NotificationEventEnum
     * @example 101
     */
    event?: number;
    channels?: number[];
    /**
     * Тема уведомлени
     * @example "Заказ №{order_number} создан!"
     */
    theme?: string;
    /**
     * Текст уведомления
     * @example "Заказ доставять по адресу {order_delivery_address}. Дата доставки {order_delivery_timeslot} {order_delivery_date}"
     */
    text?: string;
}

/**
 * Создание объекта типа NotificationSetting
 */
export type CreateNotificationSettingRequest = Prettify<
    RequireKeys<NotificationSettingFillableProperties, 'name' | 'event' | 'channels' | 'theme' | 'text'>
>;

export interface NotificationSettingReadonlyProperties {
    /**
     * Идентификатор настроек уведомления
     * @example 1
     */
    id: number;
    /**
     * Дата создания
     * @example "2021-12-20T18:00:10.000000Z"
     */
    created_at: string;
    /**
     * Дата обновления
     * @example "2021-12-20T18:00:10.000000Z"
     */
    updated_at: string;
}

export type NotificationSetting = Prettify<
    RequireKeys<
        NotificationSettingReadonlyProperties & NotificationSettingFillableProperties,
        'name' | 'event' | 'channels' | 'theme' | 'text'
    >
>;
export type NotificationSettingResponse = CommonResponse<NotificationSetting>;

/**
 * Изменение объекта типа типа NotificationSetting
 */
export type PatchNotificationSettingRequest = Prettify<NotificationSettingFillableProperties>;
export type SearchNotificationSettingsRequestFilter = Record<string, any>;

/**
 * Поиск объектов типа NotificationSetting
 */
export type SearchNotificationSettingsRequest = CommonSearchParams<SearchNotificationSettingsRequestFilter, string>;
export type SearchNotificationSettingsResponse = CommonResponse<NotificationSetting[]>;

export interface VariablesResponseVariables {
    /**
     * Переменная из NotificationVariableEnum
     * @example "{product_name}"
     */
    id: string;
    /**
     * Описание переменной
     * @example "Название товара"
     */
    title: string;
    items: VariablesResponseVariables[];
}

export interface VariablesResponseData {
    /**
     * Тип события из NotificationEventEnum
     * @example 101
     */
    event: number;
    variables: VariablesResponseVariables[];
}

/**
 * Получение справочника доступных переменных по типу события
 */
export type VariablesResponse = CommonResponse<VariablesResponseData[]>;
