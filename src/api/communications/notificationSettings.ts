import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonOption, CommonResponse, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { FetchError } from '..';
import {
    CreateNotificationSettingRequest,
    NotificationSettingResponse,
    PatchNotificationSettingRequest,
    SearchNotificationSettingsRequest,
    SearchNotificationSettingsResponse,
    VariablesResponse,
} from './types/notificationSettings';

export * from './types/notificationSettings';

export const QueryKeys = {
    getNotificationSetting: (id?: number | string) =>
        id ? ['get-notification-setting', id] : ['get-notification-setting'],
    searchNotificationSettings: (data?: Record<string, any>) =>
        data ? ['search-notification-settings', data] : ['search-notification-settings'],
    getNotificationSettingsMeta: () => ['get-notification-settings-meta'],
    getNotificationSettingChannels: () => ['get-notification-setting-channels'],
    getNotificationSettingEvents: () => ['get-notification-setting-events'],
    getNotificationSettingVariables: () => ['get-notification-setting-variables'],
};

/** Создание объекта типа NotificationSetting */
export const useCreateNotificationSetting = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();
    return useMutation<NotificationSettingResponse, FetchError, CreateNotificationSettingRequest>({
        mutationFn: data => apiClient.post('communication/notification-settings', { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.getNotificationSetting(data?.id) });

            queryClient.invalidateQueries({ queryKey: QueryKeys.searchNotificationSettings() });
        },
    });
};

/** Получение объекта типа NotificationSetting */
export function useNotificationSetting({ id }: { id: number | string }, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<NotificationSettingResponse, FetchError>({
        queryKey: QueryKeys.getNotificationSetting(id),
        queryFn: () => apiClient.get(`communication/notification-settings/${id}`, {}),
        enabled: enabled && isDetailIdValid(id),
    });
}

/** Изменение объекта типа типа NotificationSetting */
export const usePatchNotificationSetting = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<
        NotificationSettingResponse,
        FetchError,
        { id: number | string } & PatchNotificationSettingRequest
    >({
        mutationFn: ({ id, ...data }) => apiClient.patch(`communication/notification-settings/${id}`, { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.getNotificationSetting(data?.id) });

            queryClient.invalidateQueries({ queryKey: QueryKeys.searchNotificationSettings() });
        },
    });
};

/** Удаление объекта типа NotificationSetting */
export const useDeleteNotificationSetting = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<CommonResponse<null>, FetchError, { id: number | string }>({
        mutationFn: ({ id }) => apiClient.delete(`communication/notification-settings/${id}`, {}),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchNotificationSettings() });
        },
    });
};

/** Поиск объектов типа NotificationSetting */
export function useSearchNotificationSettings(data: SearchNotificationSettingsRequest, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<SearchNotificationSettingsResponse, FetchError>({
        queryKey: QueryKeys.searchNotificationSettings(data),
        queryFn: () =>
            apiClient.post('communication/notification-settings:search', {
                data,
            }),
        enabled,
    });
}

/** Получение списка доступных полей */
export function useNotificationSettingsMeta(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<Meta>, FetchError>({
        queryKey: QueryKeys.getNotificationSettingsMeta(),
        queryFn: () => apiClient.get('communication/notification-settings:meta', {}),
        enabled,
    });
}

/** Получение объектов типа NotificationChannel */
export function useNotificationSettingChannels(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: QueryKeys.getNotificationSettingChannels(),
        queryFn: () => apiClient.get('communication/notification-setting-channels', {}),
        enabled,
    });
}

/** Получение объектов типа NotificationEvent */
export function useNotificationSettingEvents(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: QueryKeys.getNotificationSettingEvents(),
        queryFn: () => apiClient.get('communication/notification-setting-events', {}),
        enabled,
    });
}

/** Получение справочника доступных переменных по типу события */
export function useNotificationSettingVariables(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<VariablesResponse, FetchError>({
        queryKey: QueryKeys.getNotificationSettingVariables(),
        queryFn: () => apiClient.get('communication/notification-setting-variables', {}),
        enabled,
    });
}
