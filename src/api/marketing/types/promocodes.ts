import { CommonOption, CommonResponse, CommonSearchParams, ImmutableFields, Meta } from '@api/common/types';

import { PromoCodeStatus } from '@scripts/enums';

import { Discount } from './discounts';

export type DeletePromoCodeRequest = { id: number | string };

export type DeletePromoCodeResponse = CommonResponse<any>;

export type GetPromoCodesMetaResponse = CommonResponse<Meta>;

export type GetPromoCodeStatusesResponse = CommonResponse<CommonOption[]>;

export interface NewPromoCode {
    /**
     * статус промокода из PromoCodeStatus
     */
    status: PromoCodeStatus;
    /**
     * Название промокода
     */
    name: string;
    /**
     * Код
     */
    code: string;
    /**
     * Ограничение на количество использований промокода
     */
    counter: number | null;
    /**
     * Дата начала действия промокода
     */
    start_date: string | null;
    /**
     * Дата окончания действия промокода
     */
    end_date: string | null;
    /**
     * ID скидки
     */
    discount_id: number;
}

export interface PromoCode extends NewPromoCode {
    /**
     * Идентификатор
     */
    id: number;

    /**
     * Количество использований промокода
     */
    current_counter: number | null;

    /**
     * Дата создания
     */
    created_at: string;

    /**
     * Дата обновления
     */
    updated_at: string;

    /**
     * Скидка, если передан include discount
     */
    discount?: Discount;
}

export type CreatePromoCodeRequest = NewPromoCode;
export type CreatePromoCodeResponse = CommonResponse<PromoCode>;
export type GetPromoCodeResponse = CommonResponse<PromoCode>;

export type PatchPromoCodeRequest = Partial<Omit<PromoCode, ImmutableFields | 'discount'>>;
export type PatchPromoCodeResponse = CommonResponse<PromoCode>;

export type SearchPromoCodesRequest = CommonSearchParams<
    object,
    string | string[],
    ['discount', 'discount.products', 'discount.products.product']
>;

export type SearchPromoCodesResponse = CommonResponse<PromoCode[]>;
