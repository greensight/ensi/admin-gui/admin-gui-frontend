import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import type { CommonResponse, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { FetchError } from '../index';
import {
    Discount,
    DiscountMassBindProductsParams,
    DiscountMassUnbindProductsParams,
    DiscountProduct,
    DiscountStatus,
    DiscountType,
    DiscountValueType,
    DiscountsStatusFormData,
    NewDiscount,
    SearchDiscountProductsRequest,
    SearchDiscountsRequest,
    UpdateDiscount,
} from './types';

export const DISCOUNTS_BASE_URL = 'marketing/discounts';
export const DISCOUNT_BASE_URL = 'marketing/discount';

export const useDiscounts = (data: SearchDiscountsRequest = {}, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Discount[]>, FetchError>({
        queryKey: ['discounts', data],
        queryFn: () => apiClient.post(`${DISCOUNTS_BASE_URL}:search`, { data }),
        enabled,
    });
};

export const useDiscountsAutocomplete = () => {
    const apiClient = useAuthApiClient();

    const discountsSearchFn = async (query: string) => {
        const res = (await apiClient.post(`${DISCOUNTS_BASE_URL}:search`, {
            data: {
                filter: {
                    name_like: query,
                },
            },
        })) as CommonResponse<Discount[]>;
        return {
            options: res.data.map(e => ({
                label: e.name,
                value: e.id,
            })),
            hasMore: false,
        };
    };

    const discountsOptionsByValuesFn = async (
        vals: number[] | { value: number }[],
        abortController: AbortController
    ) => {
        const actualValues = vals.map(v => {
            if (typeof v === 'object' && 'value' in v) return Number(v.value);
            return Number(v);
        });

        const res = (await apiClient.post(`${DISCOUNTS_BASE_URL}:search`, {
            data: {
                filter: { id: actualValues },
            },
            abortController,
        })) as CommonResponse<Discount[]>;

        return res.data.map(e => ({
            label: e.name,
            value: e.id,
        }));
    };

    return { discountsSearchFn, discountsOptionsByValuesFn };
};

export const useDiscountsMeta = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<{ data: Meta }, FetchError>({
        queryKey: ['discounts-meta'],
        queryFn: () => apiClient.get(`${DISCOUNTS_BASE_URL}:meta`),
        enabled,
    });
};

export const useDiscountsStatusChange = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, DiscountsStatusFormData>({
        mutationFn: data => apiClient.post(`${DISCOUNTS_BASE_URL}:mass-status-update`, { data }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: ['discounts'],
            }),
    });
};

export const useDiscount = (id?: number | null, include?: string, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Discount>, FetchError>({
        queryKey: ['discount', id],
        queryFn: () => apiClient.get(`${DISCOUNTS_BASE_URL}/${id}${include ? `?include=${include}` : ''}`),
        enabled: isDetailIdValid(id) && enabled,
    });
};

export const useDiscountCreate = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Discount>, FetchError, NewDiscount>({
        mutationFn: data => apiClient.post(`${DISCOUNTS_BASE_URL}`, { data }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: ['discounts'],
            }),
    });
};

export const useDiscountEdit = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Discount>, FetchError, UpdateDiscount>({
        mutationFn: data => apiClient.patch(`${DISCOUNTS_BASE_URL}/${data.id}`, { data }),

        onSuccess: res => {
            queryClient.invalidateQueries({
                queryKey: ['discounts'],
            });
            queryClient.invalidateQueries({
                queryKey: ['discount', res.data?.id],
            });
        },
    });
};

export const useDiscountBindProducts = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, DiscountMassBindProductsParams>({
        mutationFn: ({ discount_id: id, ...data }) =>
            apiClient.post(`${DISCOUNTS_BASE_URL}/${id}/products`, { data, timeout: 30000 }),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: ['discounts'],
            });
            queryClient.invalidateQueries({
                queryKey: ['discount'],
            });
            queryClient.invalidateQueries({
                queryKey: ['discounts-products'],
            });
        },
    });
};

export const useDiscountUnbindProducts = (discountId: number) => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, DiscountMassUnbindProductsParams>({
        mutationFn: data => apiClient.delete(`${DISCOUNTS_BASE_URL}/${discountId}/products`, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: ['discounts'],
            });
            queryClient.invalidateQueries({
                queryKey: ['discount'],
            });
            queryClient.invalidateQueries({
                queryKey: ['discounts-products'],
            });
        },
    });
};

export const useDeleteDiscount = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, number | string>({
        mutationFn: id => apiClient.delete(`${DISCOUNTS_BASE_URL}/${id}`),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: ['discounts'],
            });
        },
    });
};

export const useDiscountTypes = () => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<DiscountType[]>, FetchError>({
        queryKey: ['discount-types'],
        queryFn: () => apiClient.get(`${DISCOUNT_BASE_URL}-types`),
    });
};

export const useDiscountStatuses = () => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<DiscountStatus[]>, FetchError>({
        queryKey: ['discount-statuses'],
        queryFn: () => apiClient.get(`${DISCOUNT_BASE_URL}-statuses`),
    });
};

export const useDiscountValueTypes = () => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<DiscountValueType[]>, FetchError>({
        queryKey: ['discount-value-types'],
        queryFn: () => apiClient.get(`${DISCOUNT_BASE_URL}-value-types`),
    });
};

export const useDiscountProductsMeta = () => {
    const apiClient = useAuthApiClient();

    return useQuery<{ data: Meta }, FetchError>({
        queryKey: ['discounts-products-meta'],
        queryFn: () => apiClient.get(`${DISCOUNTS_BASE_URL}/discount-products:meta`),
    });
};

export const useDiscountProducts = (data: SearchDiscountProductsRequest, include: string[] = [], enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<DiscountProduct[]>, FetchError>({
        queryKey: ['discounts-products', data],
        queryFn: () =>
            apiClient.post(`${DISCOUNTS_BASE_URL}/discount-products:search`, {
                data,
                params: {
                    include,
                },
            }),
        enabled,
    });
};
