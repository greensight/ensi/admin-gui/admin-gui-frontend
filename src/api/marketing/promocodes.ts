import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { FetchError } from '../index';
import {
    CreatePromoCodeRequest,
    CreatePromoCodeResponse,
    DeletePromoCodeRequest,
    DeletePromoCodeResponse,
    GetPromoCodeResponse,
    GetPromoCodeStatusesResponse,
    GetPromoCodesMetaResponse,
    PatchPromoCodeRequest,
    PatchPromoCodeResponse,
    SearchPromoCodesRequest,
    SearchPromoCodesResponse,
} from './types';

export const QueryKeys = {
    getPromoCode: (id?: number | string) => (id ? ['get-promo-code', id] : ['get-promo-code']),
    searchPromoCodes: (data?: Record<string, any>) => (data ? ['search-promo-codes', data] : ['search-promo-codes']),
    getPromoCodesMeta: () => ['get-promo-codes-meta'],
    getPromoCodeStatuses: () => ['get-promo-code-statuses'],
};

/** Создание объекта типа PromoCode */
export function useCreatePromoCode() {
    const apiClient = useAuthApiClient();
    const queryClient = useQueryClient();
    return useMutation<CreatePromoCodeResponse, FetchError, CreatePromoCodeRequest>({
        mutationFn: data => apiClient.post(`marketing/promo-codes`, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchPromoCodes() });
        },
    });
}

/** Получение объекта типа PromoCode */
export function usePromoCode({ id }: { id: number | string }, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<GetPromoCodeResponse, FetchError>({
        queryKey: QueryKeys.getPromoCode(id),
        queryFn: () => apiClient.get(`marketing/promo-codes/${id}`),
        enabled: isDetailIdValid(id) && enabled,
    });
}

/** Обновления части полей объекта типа PromoCode */
export function usePatchPromoCode() {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<PatchPromoCodeResponse, FetchError, { id: number | string } & PatchPromoCodeRequest>({
        mutationFn: ({ id, ...data }) => apiClient.patch(`marketing/promo-codes/${id}`, { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.getPromoCode(data?.id) });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchPromoCodes() });
        },
    });
}

/** Удаление объекта типа PromoCode */
export function useDeletePromoCode() {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<DeletePromoCodeResponse, FetchError, DeletePromoCodeRequest>({
        mutationFn: ({ id }) => apiClient.delete(`marketing/promo-codes/${id}`, {}),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({
                queryKey: QueryKeys.getPromoCode(data?.id),
                refetchType: 'none',
            });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchPromoCodes() });
        },
    });
}

/** Поиск объектов типа PromoCode */
export function useSearchPromoCodes(data: SearchPromoCodesRequest, enabled = true) {
    const apiClient = useAuthApiClient();

    return useQuery<SearchPromoCodesResponse, FetchError>({
        queryKey: QueryKeys.searchPromoCodes(data),
        queryFn: () => apiClient.post(`marketing/promo-codes:search`, { data }),
        enabled,
    });
}

/** Получение списка доступных полей */
export function usePromoCodesMeta(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<GetPromoCodesMetaResponse, FetchError>({
        queryKey: QueryKeys.getPromoCodesMeta(),
        queryFn: () => apiClient.get(`marketing/promo-codes:meta`, {}),
        enabled,
    });
}

/** Получение объектов типа PromoCodeStatus */
export function usePromoCodeStatuses(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<GetPromoCodeStatusesResponse, FetchError>({
        queryKey: QueryKeys.getPromoCodeStatuses(),
        queryFn: () => apiClient.get(`marketing/promo-code-statuses`, {}),
        enabled,
    });
}
