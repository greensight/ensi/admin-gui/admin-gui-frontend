import { useQuery } from '@tanstack/react-query';

import { CommonResponse, FetchError } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

export interface CurrentUser {
    id: number;
    full_name: string;
    short_name: string;
    active: boolean;
    login: string;
    last_name: string;
    first_name: string;
    middle_name: string;
    email: string;
    phone: string;
    roles: number[];
    rights_access: number[];
}

export const useCurrentUser = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<CurrentUser>, FetchError>({
        queryKey: ['currentUser'],
        queryFn: () => apiClient.get(`auth/current-user`),
        enabled,
    });
};
