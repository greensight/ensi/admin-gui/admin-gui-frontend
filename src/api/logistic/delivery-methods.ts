import { useQuery } from '@tanstack/react-query';

import { CommonOption, CommonResponse, FetchError } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

const QueryKeys = {
    getDeliveryMethods: () => ['get-delivery-methods'],
};

/** Получение объектов типа DeliveryMethod */
export function useDeliveryMethods(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: QueryKeys.getDeliveryMethods(),
        queryFn: () => apiClient.get('logistic/delivery-methods', {}),
        enabled,
    });
}
