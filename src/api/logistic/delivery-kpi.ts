import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { FetchError } from '../index';
import {
    DeliveryKpiCt,
    DeliveryKpiCtData,
    DeliveryKpiCtFormData,
    DeliveryKpiDataAndMeta,
    DeliveryKpiFormData,
    DeliveryKpiPpt,
    DeliveryKpiPptData,
    DeliveryKpiPptFormData,
} from './types';

export const useDeliveryKpi = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<DeliveryKpiDataAndMeta, FetchError>({
        enabled,
        queryKey: ['delivery-kpi'],
        queryFn: () => apiClient.get('logistic/delivery-kpi'),
    });
};

export const useDeliveryKpiChange = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<DeliveryKpiDataAndMeta, FetchError, DeliveryKpiFormData>({
        mutationFn: data => apiClient.patch(`logistic/delivery-kpi`, { data }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: ['delivery-kpi'],
            }),
    });
};

export const useDeliveryKpiCt = (data: DeliveryKpiCtData = {}) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<DeliveryKpiCt[]>, FetchError>({
        queryKey: ['delivery-kpi-ct', data],
        queryFn: () => apiClient.post('logistic/delivery-kpi-ct:search', { data }),
    });
};

export const useDeliveryKpiCtCreate = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<DeliveryKpiCt>, FetchError, DeliveryKpiCtFormData>({
        mutationFn: deliveryKpiCt => {
            const { seller_id, ...deliveryKpiCtData } = deliveryKpiCt;
            return apiClient.post(`logistic/delivery-kpi-ct/${seller_id}`, { data: deliveryKpiCtData });
        },

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: ['delivery-kpi-ct'],
            }),
    });
};

export const useDeliveryKpiCtEdit = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<DeliveryKpiCt>, FetchError, DeliveryKpiCtFormData>({
        mutationFn: deliveryKpiCt => {
            const { seller_id, ...deliveryKpiCtData } = deliveryKpiCt;
            return apiClient.put(`logistic/delivery-kpi-ct/${seller_id}`, { data: deliveryKpiCtData });
        },

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: ['delivery-kpi-ct'],
            }),
    });
};

export const useDeliveryKpiCtDelete = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, number>({
        mutationFn: seller_id => apiClient.delete(`logistic/delivery-kpi-ct/${seller_id}`),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: ['delivery-kpi-ct'],
            }),
    });
};

export const useDeliveryKpiPpt = (data: DeliveryKpiPptData = {}) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<DeliveryKpiPpt[]>, FetchError>({
        queryKey: ['delivery-kpi-ppt', data],
        queryFn: () => apiClient.post('logistic/delivery-kpi-ppt:search', { data }),
    });
};

export const useDeliveryKpiPptCreate = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<DeliveryKpiPpt>, FetchError, DeliveryKpiPptFormData>({
        mutationFn: deliveryKpiPpt => {
            const { seller_id, ...deliveryKpiPptData } = deliveryKpiPpt;
            return apiClient.post(`logistic/delivery-kpi-ppt/${seller_id}`, { data: deliveryKpiPptData });
        },

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: ['delivery-kpi-ppt'],
            }),
    });
};

export const useDeliveryKpiPptEdit = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<DeliveryKpiPpt>, FetchError, DeliveryKpiPptFormData>({
        mutationFn: deliveryKpiPpt => {
            const { seller_id, ...deliveryKpiPptData } = deliveryKpiPpt;
            return apiClient.put(`logistic/delivery-kpi-ppt/${seller_id}`, { data: deliveryKpiPptData });
        },

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: ['delivery-kpi-ppt'],
            }),
    });
};

export const useDeliveryKpiPptDelete = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, number>({
        mutationFn: seller_id => apiClient.delete(`logistic/delivery-kpi-ppt/${seller_id}`),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: ['delivery-kpi-ppt'],
            }),
    });
};
