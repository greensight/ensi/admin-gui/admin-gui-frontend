import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, FetchError, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import {
    CreateDeliveryPriceRequest,
    DeliveryPriceResponse,
    PatchDeliveryPriceRequest,
    ReplaceDeliveryPriceRequest,
    SearchDeliveryPricesRequest,
    SearchDeliveryPricesResponse,
} from './types/delivery-prices';

const QueryKeys = {
    getDeliveryPrice: (id?: number | string) => (id ? ['get-delivery-price', id] : ['get-delivery-price']),
    searchOneDeliveryPrice: (data?: Record<string, any>) =>
        data ? ['search-one-delivery-price', data] : ['search-one-delivery-price'],
    searchDeliveryPrices: (data?: Record<string, any>) =>
        data ? ['search-delivery-prices', data] : ['search-delivery-prices'],
    getDeliveryPriceMeta: () => ['get-delivery-price-meta'],
};

/** Получение списка доступных полей */
export function useDeliveryPriceMeta(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<Meta>, FetchError>({
        queryKey: QueryKeys.getDeliveryPriceMeta(),
        queryFn: () => apiClient.get('logistic/delivery-prices:meta', {}),
        enabled,
    });
}

/** Создание объекта типа DeliveryPrice */
export const useCreateDeliveryPrice = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<DeliveryPriceResponse, FetchError, CreateDeliveryPriceRequest>({
        mutationFn: data => apiClient.post('logistic/delivery-prices', { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.getDeliveryPrice() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchDeliveryPrices() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchOneDeliveryPrice() });
        },
    });
};

/** Получение объекта типа DeliveryPrice */
export function useDeliveryPrice({ id }: { id: number | string }, params: Record<string, any> = {}, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<DeliveryPriceResponse, FetchError>({
        queryKey: QueryKeys.getDeliveryPrice(id),
        queryFn: () => apiClient.get(`logistic/delivery-prices/${id}`, { params }),
        enabled: isDetailIdValid(id) && enabled,
    });
}

/** Замена объекта типа DeliveryPrice */
export const useReplaceDeliveryPrice = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<DeliveryPriceResponse, FetchError, { id: number | string } & ReplaceDeliveryPriceRequest>({
        mutationFn: ({ id, ...data }) => apiClient.put(`logistic/delivery-prices/${id}`, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.getDeliveryPrice() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchDeliveryPrices() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchOneDeliveryPrice() });
        },
    });
};

/** Обновления части полей объекта типа DeliveryPrice */
export const usePatchDeliveryPrice = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<DeliveryPriceResponse, FetchError, { id: number | string } & PatchDeliveryPriceRequest>({
        mutationFn: ({ id, ...data }) => apiClient.patch(`logistic/delivery-prices/${id}`, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.getDeliveryPrice() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchDeliveryPrices() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchOneDeliveryPrice() });
        },
    });
};

/** Удаление объекта типа DeliveryPrice */
export const useDeleteDeliveryPrice = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<CommonResponse<any>, FetchError, { id: number | string }>({
        mutationFn: ({ id }) => apiClient.delete(`logistic/delivery-prices/${id}`, {}),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchDeliveryPrices() });
        },
    });
};

/** Поиск объектов типа DeliveryPrice */
export function useSearchDeliveryPrices(data: SearchDeliveryPricesRequest, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<SearchDeliveryPricesResponse, FetchError>({
        queryKey: QueryKeys.searchDeliveryPrices(data),
        queryFn: () => apiClient.post('logistic/delivery-prices:search', { data }),
        enabled,
    });
}

/** Поиск объектов типа DeliveryPrice */
export function useSearchOneDeliveryPrice(data: SearchDeliveryPricesRequest, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<DeliveryPriceResponse, FetchError>({
        queryKey: QueryKeys.searchOneDeliveryPrice(data),
        queryFn: () => apiClient.post('logistic/delivery-prices:search-one', { data }),
        enabled,
    });
}
