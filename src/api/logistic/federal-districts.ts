import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { FetchError } from '..';
import {
    CreateFederalDistrictRequest,
    FederalDistrict,
    FederalDistrictResponse,
    PatchFederalDistrictRequest,
    ReplaceFederalDistrictRequest,
    SearchFederalDistrictsRequest,
    SearchFederalDistrictsResponse,
} from './types/federal-districts';

const QueryKeys = {
    getFederalDistrict: (id?: number | string) => (id ? ['get-federal-district', id] : ['get-federal-district']),
    searchFederalDistricts: (data?: Record<string, any>) =>
        data ? ['search-federal-districts', data] : ['search-federal-districts'],
    searchOneFederalDistrict: (data?: Record<string, any>) =>
        data ? ['search-one-federal-district', data] : ['search-one-federal-district'],
};

/** Создание объекта типа FederalDistrict */
export const useCreateFederalDistrict = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<FederalDistrictResponse, FetchError, CreateFederalDistrictRequest>({
        mutationFn: data => apiClient.post('logistic/federal-districts', { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.getFederalDistrict(data?.id) });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchFederalDistricts() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchOneFederalDistrict(data) });
        },
    });
};

/** Получение объекта типа FederalDistrict */
export function useFederalDistrict({ id }: { id: number | string }, params: Record<string, any> = {}, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<FederalDistrictResponse, FetchError>({
        queryKey: QueryKeys.getFederalDistrict(id),
        queryFn: () => apiClient.get(`logistic/federal-districts/${id}`, { params }),
        enabled,
    });
}

/** Замена объекта типа FederalDistrict */
export const useReplaceFederalDistrict = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<FederalDistrictResponse, FetchError, { id: number | string } & ReplaceFederalDistrictRequest>({
        mutationFn: ({ id, ...data }) => apiClient.put(`logistic/federal-districts/${id}`, { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.getFederalDistrict(data?.id) });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchFederalDistricts() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchOneFederalDistrict(data) });
        },
    });
};

/** Обновления части полей объекта типа FederalDistrict */
export const usePatchFederalDistrict = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<FederalDistrictResponse, FetchError, { id: number | string } & PatchFederalDistrictRequest>({
        mutationFn: ({ id, ...data }) => apiClient.patch(`logistic/federal-districts/${id}`, { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.getFederalDistrict(data?.id) });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchFederalDistricts() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchOneFederalDistrict(data) });
        },
    });
};

/** Удаление объекта типа FederalDistrict */
export const useDeleteFederalDistrict = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<CommonResponse<any>, FetchError, { id: number | string }>({
        mutationFn: ({ id }) => apiClient.delete(`logistic/federal-districts/${id}`, {}),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.getFederalDistrict(data?.id) });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchFederalDistricts() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchOneFederalDistrict(data) });
        },
    });
};

/** Поиск объектов типа FederalDistrict */
export function useSearchFederalDistricts(data: SearchFederalDistrictsRequest, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<SearchFederalDistrictsResponse, FetchError>({
        queryKey: QueryKeys.searchFederalDistricts(data),
        queryFn: () => apiClient.post('logistic/federal-districts:search', { data }),
        enabled,
    });
}

/** Поиск объекта типа FederalDistrict */
export function useSearchOneFederalDistrict(data: SearchFederalDistrictsRequest, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<FederalDistrictResponse, FetchError>({
        queryKey: QueryKeys.searchOneFederalDistrict(data),
        queryFn: () => apiClient.post('logistic/federal-districts:search-one', { data }),
        enabled,
    });
}

export const useFederalDistrictAutocomplete = () => {
    const apiClient = useAuthApiClient();

    const federalDistrictSearchFn = async (query: string) => {
        const res = (await apiClient.post(`logistic/federal-districts:search`, {
            data: {
                filter: {
                    name_like: query,
                },
            },
        })) as CommonResponse<FederalDistrict[]>;
        return {
            options: res.data.map(e => ({
                label: e.name,
                value: e.id,
            })),
            hasMore: false,
        };
    };

    const federalDistrictOptionsByValuesFn = async (
        vals: number[] | { value: number }[],
        abortController: AbortController
    ) => {
        const actualValues = vals.map(v => {
            if (typeof v === 'object' && 'value' in v) return Number(v.value);
            return Number(v);
        });

        const res = (await apiClient.post(`logistic/federal-districts:search`, {
            data: {
                filter: { id: actualValues },
            },
            abortController,
        })) as CommonResponse<FederalDistrict[]>;

        return res.data.map(e => ({
            label: e.name,
            value: e.id,
        }));
    };

    return { federalDistrictSearchFn, federalDistrictOptionsByValuesFn };
};
