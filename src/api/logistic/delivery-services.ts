import { useMutation, useQuery } from '@tanstack/react-query';

import { CommonResponse } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { FetchError } from '../index';
import {
    DeliveryService,
    DeliveryServiceDetail,
    DeliveryServicePaymentMethod,
    DeliveryServicePaymentMethods,
    DeliveryServiceStatus,
    DeliveryServicesData,
} from './types';

const LOGISTIC_URL = 'logistic/delivery-services';

export const useDeliveryServices = (data: DeliveryServicesData = {}) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<DeliveryService[]>, FetchError>({
        queryKey: ['delivery-services', data],
        queryFn: () => apiClient.post(`${LOGISTIC_URL}:search`, { data }),
    });
};

export const useDeliveryStatuses = () => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<DeliveryServiceStatus[]>, FetchError>({
        queryKey: ['delivery-service-statuses'],
        queryFn: () => apiClient.get('logistic/delivery-service-statuses'),
    });
};

export const useDeliveryService = (id: number, include?: string) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<DeliveryServiceDetail>, FetchError>({
        queryKey: ['delivery-services', id],
        queryFn: () => apiClient.get(`${LOGISTIC_URL}/${id}${include ? `?include=${include}` : ''}`),
        enabled: !!id,
    });
};

export const useDeliveryServiceChange = () => {
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<DeliveryServiceDetail>, FetchError, Partial<DeliveryServiceDetail>>({
        mutationFn: deliveryService => {
            const { id, ...deliveryServiceData } = deliveryService;
            return apiClient.patch(`${LOGISTIC_URL}/${id}`, { data: deliveryServiceData });
        },
    });
};

export const useDeliveryServiceAddPaymentMethods = () => {
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, DeliveryServicePaymentMethods>({
        mutationFn: data =>
            apiClient.post(`${LOGISTIC_URL}/${data.id}:add-payment-methods`, {
                data,
            }),
    });
};

export const useDeliveryServiceDeletePaymentMethod = () => {
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, DeliveryServicePaymentMethod>({
        mutationFn: data =>
            apiClient.post(`${LOGISTIC_URL}/${data.id}:delete-payment-method`, {
                data,
            }),
    });
};

export const useDeliveryServiceAutocomplete = () => {
    const apiClient = useAuthApiClient();

    const deliveryServiceSearchFn = async (query: string) => {
        const res = (await apiClient.post(`${LOGISTIC_URL}:search`, {
            data: {
                filter: {
                    name_like: query,
                },
            },
        })) as CommonResponse<DeliveryService[]>;
        return {
            options: res.data.map(e => ({
                label: e.name,
                value: e.id,
            })),
            hasMore: false,
        };
    };

    const deliveryServiceOptionsByValuesFn = async (
        vals: number[] | { value: number }[],
        abortController: AbortController
    ) => {
        const actualValues = vals.map(v => {
            if (typeof v === 'object' && 'value' in v) return Number(v.value);
            return Number(v);
        });

        const res = (await apiClient.post(`${LOGISTIC_URL}:search`, {
            data: {
                filter: { id: actualValues },
            },
            abortController,
        })) as CommonResponse<DeliveryService[]>;

        return res.data.map(e => ({
            label: e.name,
            value: e.id,
        }));
    };

    return { deliveryServiceSearchFn, deliveryServiceOptionsByValuesFn };
};
