import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, FetchError, FilterResponse, FilterSearchParam, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { PatchPointRequest, PointResponse, SearchPointsRequest, SearchPointsResponse } from './types';

export const QueryKeys = {
    getPoint: (id?: number | string) => (id ? ['get-point', id] : ['get-point']),
    searchPoints: (data?: Record<string, any>) => (data ? ['search-points', data] : ['search-points']),
    getPointMeta: () => ['get-point-meta'],
    searchPointEnumValues: (data?: Record<string, any>) =>
        data ? ['search-point-enum-values', data] : ['search-point-enum-values'],
};

/** Получение объекта типа Point */
export function usePoint({ id }: { id: number | string }, params: Record<string, any> = {}, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<PointResponse, FetchError>({
        queryKey: QueryKeys.getPoint(id),
        queryFn: () => apiClient.get(`logistic/points/${id}`, { params }),
        enabled: isDetailIdValid(id) && enabled,
    });
}

/** Обновления части полей объекта типа Point */
export const usePatchPoint = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<PointResponse, FetchError, { id: number | string } & PatchPointRequest>({
        mutationFn: ({ id, ...data }) => apiClient.patch(`logistic/points/${id}`, { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.getPoint(data?.id) });

            queryClient.invalidateQueries({ queryKey: QueryKeys.searchPoints() });
        },
    });
};

/** Поиск пунктов приема/выдачи товара */
export function useSearchPoints(data: SearchPointsRequest, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<SearchPointsResponse, FetchError>({
        queryKey: QueryKeys.searchPoints(data),
        queryFn: () => apiClient.post('logistic/points:search', { data }),
        enabled,
    });
}

/** Получение списка доступных полей */
export function usePointsMeta(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<Meta>, FetchError>({
        queryKey: QueryKeys.getPointMeta(),
        queryFn: () => apiClient.get('logistic/points:meta', {}),
        enabled,
    });
}

/** Поиск пунктов приема/выдачи товара для справочника */
export function useSearchPointEnumValues(data: FilterSearchParam, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<FilterResponse>, FetchError>({
        queryKey: QueryKeys.searchPointEnumValues(data),
        queryFn: () => apiClient.post('logistic/point-enum-values:search', { data }),
        enabled,
    });
}
