import { CommonResponse, CommonSearchParams, Prettify, RequireKeys } from '@api/common/types';

import { Address } from './address';

export interface PointReadonlyProperties {
    /**
     * Идентификатор пункта
     * @example 1
     */
    id: number;
    /**
     * Id пункта у службы доставки
     * @example "1234"
     */
    external_id: string;
    /**
     * Название пункта
     * @example "ПВЗ на Наволокской"
     */
    name: string;
    /**
     * Описание проезда
     * @example "ПВЗ на Наволокской"
     */
    description?: string;
    /**
     * Телефон
     * @example "+794933155105"
     */
    phone?: string;
    /**
     * Cокращенный адрес пвз
     */
    address_reduce?: string;
    /**
     * Cтанция метро
     * @example "Ховрино"
     */
    metro_station?: string;
    /**
     * Выдача только полностью оплаченных посылок
     */
    only_online_payment?: boolean;
    /**
     * Возможна ли оплата картой
     */
    has_payment_card?: boolean;
    /**
     * Осуществляется ли курьерская доставка
     */
    has_courier?: boolean;
    /**
     * Отделение является постаматом
     */
    is_postamat?: boolean;
    /**
     * Ограничение объема (куб.метры)
     */
    max_value?: string;
    /**
     * Ограничение веса (кг)
     */
    max_weight?: number;
    /**
     * Дата создания
     * @example "2021-01-29T12:36:13.000000Z"
     */
    created_at: string;
    /**
     * Дата обновления
     * @example "2021-01-29T12:36:13.000000Z"
     */
    updated_at: string;
}

export interface PointFillableProperties {
    /**
     * Идентификатор службы доставки из DeliveryServiceEnum
     * @example 1
     */
    delivery_service_id?: number | null;
    /**
     * Активность
     */
    is_active?: boolean;
    /**
     * ФИАС id населенного пункта
     * @example "df284e77-a45c-411d-8901-94c91f4a92e5"
     */
    city_guid?: string | null;
    /**
     * адрес
     */
    address?: Address;
    /**
     * Широта
     * @example "55.013548"
     */
    geo_lat?: string | null;
    /**
     * Долгота
     * @example "36.115588"
     */
    geo_lon?: string | null;
    /**
     * Часовой пояс
     * @example "Moscow"
     */
    timezone?: string | null;
}

export interface PointIncludesPointWorkings {
    /**
     * Идентификатор
     * @example 1
     */
    id: number;
    /**
     * Идентификатор пункта
     * @example 1
     */
    point_id: number;
    /**
     * Активность
     */
    is_active: boolean;
    /**
     * День недели (1-7)
     * @example 1
     */
    day: number;
    /**
     * Время начала работы склада
     * @example "10:00"
     */
    working_start_time: string;
    /**
     * Время конца работы склада
     * @example "19:00"
     */
    working_end_time: string;
    /**
     * Дата создания
     * @example "2021-01-29T12:36:13.000000Z"
     */
    created_at: string;
    /**
     * Дата обновления
     * @example "2021-01-29T12:36:13.000000Z"
     */
    updated_at: string;
}

export interface PointIncludes {
    /**
     * Время работы
     */
    point_workings?: PointIncludesPointWorkings[];
}

/**
 * Пункт выдачи
 */
export type Point = Prettify<
    RequireKeys<
        PointReadonlyProperties & PointFillableProperties & PointIncludes,
        'delivery_service_id' | 'is_active' | 'city_guid' | 'address' | 'geo_lat' | 'geo_lon' | 'timezone'
    >
>;

/**
 * Получение объекта типа Point
 */
export type PointResponse = CommonResponse<Point>;

/**
 * Обновления части полей объекта типа Point
 */
export type PatchPointRequest = Prettify<PointFillableProperties>;
export type SearchPointsRequestFilter = Record<string, any>;

/**
 * Подгружаемые связанные сущности
 */
export enum SearchPointsRequestInclude {
    POINT_WORKINGS = 'point_workings',
}

/**
 * Поиск пунктов приема/выдачи товара
 */
export type SearchPointsRequest = CommonSearchParams<SearchPointsRequestFilter, string | string[]>;

export type SearchPointsResponse = CommonResponse<Point[]>;
