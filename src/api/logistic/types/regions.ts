import { CommonResponse, OffsetPaginationQuery } from "@api/common/types";

type Prettify<T> = {
    [K in keyof T]: T[K];
};

export interface RegionFillableProperties {
    /**
     * Id федерального округа
     * @example 1
     */
    federal_district_id: number;
    /**
     * Название
     * @example "Московская обл"
     */
    name: string;
    /**
     * Id ФИАС
     * @example "29251dcf-00a1-4e34-98d4-5c47484a36d4"
     */
    guid: string;
}

/**
 * Создание объекта типа Region */

export type CreateRegionRequest = Prettify<RegionFillableProperties>;

export interface RegionReadonlyProperties {
    /**
     * Идентификатор региона
     * @example 1
     */
    id: number;
    /**
     * Дата создания
     * @example "2021-01-29T12:36:13.000000Z"
     */
    created_at: string;
    /**
     * Дата обновления
     * @example "2021-01-29T12:36:13.000000Z"
     */
    updated_at: string;
}

export type Region = Prettify<RegionReadonlyProperties & RegionFillableProperties>;
export type RegionResponse = CommonResponse<Region>;

/**
 * Замена объекта типа Region */
export type ReplaceRegionRequest = Prettify<RegionFillableProperties>;

/**
 * Обновления части полей объекта типа Region */
export interface PatchRegionRequest {
    /**
     * Id федерального округа
     * @example 1
     */
    federal_district_id?: number;
    /**
     * Название
     * @example "Московская обл"
     */
    name?: string;
    /**
     * Id ФИАС
     * @example "29251dcf-00a1-4e34-98d4-5c47484a36d4"
     */
    guid?: string;
}

/**
 * Поиск объектов типа Region */
export interface SearchRegionsRequest {
    sort?: string;
    filter?: Record<string, any>;
    pagination: OffsetPaginationQuery;
}

export type SearchRegionsResponse = CommonResponse<Region[]>;
