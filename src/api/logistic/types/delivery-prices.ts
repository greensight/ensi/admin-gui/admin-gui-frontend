import { CommonResponse, CommonSearchParams } from '@api/common/types';

/**
 * Службы доставки. Расшифровка значений:
 * `1` - B2Cpl
 * `2` - Boxberry
 * `3` - CDEK
 * `4` - Dostavista
 * `5` - DPD
 * `6` - IML
 * `7` - MaxiPost;
 * `8` - PickPoint;
 * `9` - PONY EXPRESS;
 * `10`- Почта России;
 */
export enum LogisticDeliveryServiceEnum {
    B2CPL = 1,
    BOXBERRY = 2,
    CDEK = 3,
    DOSTAVISTA = 4,
    DPD = 5,
    IML = 6,
    MAXIPOST = 7,
    PICKPOINT = 8,
    PONTEXPRESS = 9,
    RUPOST = 10,
}

export enum LogisticDeliveryMethodEnum {
    /** Курьерская доставка */
    DELIVERY = 1,
    /** Самовывоз */
    PICKUP = 2,
}

type Prettify<T> = {
    [K in keyof T]: T[K];
};

export interface DeliveryPriceFillableProperties {
    /**
     * Id федерального округа
     * @example 1
     */
    federal_district_id: number;
    /**
     * Id региона
     * @example 1
     */
    region_id?: number;
    /**
     * Id ФИАС региона
     * @example "29251dcf-00a1-4e34-98d4-5c47484a36d4"
     */
    region_guid?: string | null;
    delivery_service?: LogisticDeliveryServiceEnum;
    delivery_method: LogisticDeliveryMethodEnum;
    /**
     * Цена
     * @example 100
     */
    price: number;
}

/**
 * Создание объекта типа DeliveryPrice */

export type CreateDeliveryPriceRequest = Prettify<DeliveryPriceFillableProperties>;

export interface DeliveryPriceReadonlyProperties {
    /**
     * Идентификатор цены доставки
     * @example 1
     */
    id: number;
    /**
     * Дата создания
     * @example "2021-01-29T12:36:13.000000Z"
     */
    created_at: string;
    /**
     * Дата обновления
     * @example "2021-01-29T12:36:13.000000Z"
     */
    updated_at: string;
}

export type DeliveryPrice = Prettify<DeliveryPriceReadonlyProperties & DeliveryPriceFillableProperties>;
export type DeliveryPriceResponse = CommonResponse<DeliveryPrice>;

/**
 * Замена объекта типа DeliveryPrice */

export type ReplaceDeliveryPriceRequest = Prettify<DeliveryPriceFillableProperties>;

/**
 * Обновления части полей объекта типа DeliveryPrice */

export interface PatchDeliveryPriceRequest {
    /**
     * Id федерального округа
     * @example 1
     */
    federal_district_id: number;
    /**
     * Id региона
     * @example 1
     */
    region_id?: number;
    /**
     * Id ФИАС региона
     * @example "29251dcf-00a1-4e34-98d4-5c47484a36d4"
     */
    region_guid?: string | null;
    delivery_service?: LogisticDeliveryServiceEnum;
    delivery_method: LogisticDeliveryMethodEnum;
    /**
     * Цена
     * @example 100
     */
    price: LogisticDeliveryServiceEnum;
}

/**
 * Поиск объектов типа DeliveryPrice
 */
export type SearchDeliveryPricesRequest = CommonSearchParams<DeliveryPrice, string | string[]>;

export type SearchDeliveryPricesResponse = CommonResponse<DeliveryPrice[]>;
