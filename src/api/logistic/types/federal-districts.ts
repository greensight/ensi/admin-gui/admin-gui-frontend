import { CommonResponse, OffsetPaginationQuery } from '@api/common/types';

type Prettify<T> = {
    [K in keyof T]: T[K];
};

export interface FederalDistrictFillableProperties {
    /**
     * Название
     * @example "Центральный федеральный округ"
     */
    name: string;
}

/**
 * Создание объекта типа FederalDistrict */

export type CreateFederalDistrictRequest = Prettify<FederalDistrictFillableProperties>;

export interface FederalDistrictReadonlyProperties {
    /**
     * Идентификатор федерального округа
     * @example 1
     */
    id: number;
    /**
     * Дата создания
     * @example "2021-01-29T12:36:13.000000Z"
     */
    created_at: string;
    /**
     * Дата обновления
     * @example "2021-01-29T12:36:13.000000Z"
     */
    updated_at: string;
}

export type FederalDistrict = Prettify<FederalDistrictReadonlyProperties & FederalDistrictFillableProperties>;

export type FederalDistrictResponse  = CommonResponse<FederalDistrict>;

/**
 * Замена объекта типа FederalDistrict */
export type ReplaceFederalDistrictRequest = Prettify<FederalDistrictFillableProperties>;

/**
 * Обновления части полей объекта типа FederalDistrict */
export interface PatchFederalDistrictRequest {
    /**
     * Название
     * @example "Центральный федеральный округ"
     */
    name?: string;
}

/**
 * Поиск объектов типа FederalDistrict */
export interface SearchFederalDistrictsRequest {
    sort?: string;
    filter?: Record<string, any>;
    include?: string[];
    pagination: OffsetPaginationQuery;
}

export type SearchFederalDistrictsResponse = CommonResponse<FederalDistrict[]>;
