import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { APIClient, FetchError } from '..';
import {
    CreateRegionRequest,
    PatchRegionRequest,
    Region,
    RegionResponse,
    ReplaceRegionRequest,
    SearchRegionsRequest,
    SearchRegionsResponse,
} from './types/regions';

const QueryKeys = {
    getRegion: (id?: number | string) => (id ? ['get-region', id] : ['get-region']),
    searchRegions: (data?: Record<string, any>) => (data ? ['search-regions', data] : ['search-regions']),
    searchOneRegion: (data?: Record<string, any>) => (data ? ['search-one-region', data] : ['search-one-region']),
};

/** Создание объекта типа Region */
export const useCreateRegion = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<RegionResponse, FetchError, CreateRegionRequest>({
        mutationFn: data => apiClient.post('logistic/regions', { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchRegions() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchOneRegion() });
        },
    });
};

export const getRegion = (apiClient: APIClient, id: number) =>
    apiClient.get(`logistic/regions/${id}`) as Promise<RegionResponse>;

/** Получение объекта типа Region */
export function useRegion({ id }: { id: number }, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<RegionResponse, FetchError>({
        queryKey: QueryKeys.getRegion(id),
        queryFn: () => getRegion(apiClient, id),
        enabled,
    });
}

/** Замена объекта типа Region */
export const useReplaceRegion = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<RegionResponse, FetchError, { id: number | string } & ReplaceRegionRequest>({
        mutationFn: ({ id, ...data }) => apiClient.put(`logistic/regions/${id}`, { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.getRegion(data?.id) });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchRegions() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchOneRegion(data) });
        },
    });
};

/** Обновления части полей объекта типа Region */
export const usePatchRegion = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<RegionResponse, FetchError, { id: number | string } & PatchRegionRequest>({
        mutationFn: ({ id, ...data }) => apiClient.patch(`logistic/regions/${id}`, { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.getRegion(data?.id) });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchRegions() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchOneRegion(data) });
        },
    });
};

/** Удаление объекта типа Region */
export const useDeleteRegion = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<CommonResponse<any>, FetchError, { id: number | string }>({
        mutationFn: ({ id }) => apiClient.delete(`logistic/regions/${id}`, {}),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.getRegion(data?.id) });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchRegions() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchOneRegion(data) });
        },
    });
};

/** Поиск объектов типа Region */
export function useSearchRegions(data: SearchRegionsRequest, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<SearchRegionsResponse, FetchError>({
        queryKey: QueryKeys.searchRegions(data),
        queryFn: () => apiClient.post('logistic/regions:search', { data }),
        enabled,
    });
}

export const searchOneRegion = (apiClient: APIClient, data: SearchRegionsRequest) =>
    apiClient.post('logistic/regions:search-one', { data }) as Promise<RegionResponse>;

/** Поиск объектов типа Region */
export function useSearchOneRegion(data: SearchRegionsRequest, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<RegionResponse, FetchError>({
        queryKey: QueryKeys.searchOneRegion(data),
        queryFn: () => searchOneRegion(apiClient, data),
        enabled,
    });
}

export const useRegionAutocomplete = ({ federalDistrictId }: { federalDistrictId?: number } = {}) => {
    const apiClient = useAuthApiClient();

    const regionSearchFn = async (query: string, offset: number, limit: number, abortController?: AbortController) => {
        const res = (await apiClient.post(`logistic/regions:search`, {
            abortController,
            data: {
                filter: {
                    name_like: query,
                    ...(federalDistrictId && {
                        federal_district_id: federalDistrictId,
                    }),
                },
                pagination: {
                    offset,
                    limit,
                },
            },
        })) as CommonResponse<Region[]>;
        return {
            options: res.data.map(e => ({
                label: e.name,
                value: e.id,
            })),
            hasMore: false,
        };
    };

    const regionSearchNameFn = async (
        query: string,
        offset?: number | undefined,
        limit?: number | undefined,
        abortController?: AbortController
    ) => {
        const res = (await apiClient.post(`logistic/regions:search`, {
            abortController,
            data: {
                filter: {
                    name_like: query,
                    ...(federalDistrictId && {
                        federal_district_id: federalDistrictId,
                    }),
                },
                pagination: {
                    offset,
                    limit,
                },
            },
        })) as CommonResponse<Region[]>;
        return {
            options: res.data.map(e => ({
                label: e.name,
                value: e.name,
            })),
            hasMore: false,
        };
    };

    const regionOptionsByValuesFn = async (vals: number[] | { value: number }[], abortController: AbortController) => {
        const actualValues = vals.map(v => {
            if (typeof v === 'object' && 'value' in v) return Number(v.value);
            return Number(v);
        });

        const res = (await apiClient.post(`logistic/regions:search`, {
            data: {
                filter: { id: actualValues },
            },
            abortController,
        })) as CommonResponse<Region[]>;

        return res.data.map(e => ({
            label: e.name,
            value: e.id,
        }));
    };

    const regionOptionsByNameFn = async (label: string[], abortController: AbortController) =>
        (await regionSearchNameFn(label[0], 0, -1, abortController)).options;

    return { regionSearchFn, regionOptionsByValuesFn, regionSearchNameFn, regionOptionsByNameFn };
};
