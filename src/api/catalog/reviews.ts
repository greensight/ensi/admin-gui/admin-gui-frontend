import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonOption, CommonResponse } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { FetchError } from '..';
import { GetReviewResponse, PatchReviewRequest, SearchReviewsRequest, SearchReviewsResponse } from './types/reviews';

const API_URL = 'catalog/reviews';

export const useReviews = (data: SearchReviewsRequest, enabled = true) => {
    const apiClient = useAuthApiClient();
    return useQuery<SearchReviewsResponse, FetchError>({
        queryKey: ['reviews', data],
        queryFn: () => apiClient.post(`${API_URL}:search`, { data }),
        enabled,
    });
};

export const useReview = (id: number, enabled = true) => {
    const apiClient = useAuthApiClient();
    return useQuery<GetReviewResponse, FetchError>({
        queryKey: ['review', id],
        queryFn: () => apiClient.get(`${API_URL}/${id}?include=products,customers`),
        enabled: isDetailIdValid(id) && enabled,
    });
};

export const useReviewsMeta = (enabled = true) => {
    const apiClient = useAuthApiClient();
    return useQuery<any, FetchError>({
        queryKey: ['reviews-meta'],
        queryFn: () => apiClient.get(`${API_URL}:meta`),
        enabled,
    });
};

export const useReviewsStatuses = () => {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: ['reviews-statuses'],
        queryFn: () => apiClient.get(`${API_URL}/review-statuses`),
    });
};

export const usePatchReview = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<any, FetchError, PatchReviewRequest>({
        mutationFn: ({ id, ...data }) => apiClient.patch(`${API_URL}/${id}`, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: ['reviews'],
            });
            queryClient.invalidateQueries({
                queryKey: ['review'],
            });
        },
    });
};

export const useDeleteReview = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<any, FetchError, { id: number }>({
        mutationFn: data => apiClient.delete(`${API_URL}/${data.id}`),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: ['reviews'],
            });
        },
    });
};

export const useMassDeleteReviews = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<any, FetchError, { ids: number[] }>({
        mutationFn: data => apiClient.post(`${API_URL}:mass-delete`, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: ['reviews'],
            });
        },
    });
};
