import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, CommonSearchParams } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { FetchError } from '../index';
import { Manufacturer, ManufacturerFormData, ManufacturerItemFormData } from './types';

const API_URL = 'catalog/manufacturers';
const QUERY_KEY = 'manufacturers';

export const useManufacturers = (data: CommonSearchParams<undefined>) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Manufacturer[]>, FetchError>({
        queryKey: [QUERY_KEY, data],
        queryFn: () => apiClient.post(`${API_URL}:search`, { data }),
    });
};

export const useManufacturerCreate = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Manufacturer>, FetchError, ManufacturerFormData>({
        mutationFn: manufacturer => apiClient.post(API_URL, { data: manufacturer }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY],
            }),
    });
};

export const useManufacturerChange = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Manufacturer>, FetchError, ManufacturerItemFormData>({
        mutationFn: manufacturer => {
            const { id, ...manufacturerData } = manufacturer;
            return apiClient.put(`${API_URL}/${id}`, { data: manufacturerData });
        },

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY],
            }),
    });
};

export const useManufacturerRemove = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation({
        mutationFn: id => apiClient.delete(`${API_URL}/${id}`),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY],
            }),
    });
};
