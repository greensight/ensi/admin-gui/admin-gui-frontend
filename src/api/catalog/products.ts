import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { ApiError, CommonResponse, CommonSearchParams, Config, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { FetchError } from '../index';
import {
    Product,
    ProductAttributeValue,
    ProductCreateParams,
    ProductDetail,
    ProductFilter,
    ProductGroupCreateParams,
    ProductGroupCreateResponse,
    ProductGroupFilterParams,
    ProductGroupFilterResponse,
    ProductGroupSearchOneResponse,
    ProductGroupSearchParams,
    ProductGroupSearchResponse,
    ProductGroupTypeParams,
    ProductGroupTypeResponse,
    ProductImage,
    ProductMassPatchByQueryRequest,
    ProductMassPatchRequest,
    ProductMassPatchResponse,
    ProductPreloadImage,
    ProductsTypes,
    Property,
} from './types';

const API_URL = 'catalog/products';
export const COMMON_ATTRIBUTES_KEY = 'products-common-attributes';

const QueryKeys = {
    getProduct: (id?: number | string) => (id ? ['get-product', id] : ['get-product']),
    searchProducts: (data?: Record<string, any>) => (data ? ['search-products', data] : ['search-products']),
};

export const useProducts = (data: CommonSearchParams<Partial<ProductFilter>, string | string[]>, isEnabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Product[]>, FetchError>({
        enabled: isEnabled,
        queryKey: QueryKeys.searchProducts(data),
        queryFn: () =>
            apiClient.post(`${API_URL}/drafts:search`, {
                data,
            }),
    });
};

export const useProductsCommonAttributes = (data: CommonSearchParams<Partial<ProductFilter>>, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Property[]>, FetchError>({
        queryKey: [COMMON_ATTRIBUTES_KEY, data],
        queryFn: () => apiClient.post(`${API_URL}:common-attributes`, { data }),
        gcTime: 10000,
        enabled,
    });
};

export const useProductsMeta = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<{ data: Meta }, FetchError>({
        queryKey: ['products-meta'],
        queryFn: () => apiClient.get(`${API_URL}/drafts:meta`),
        enabled,
    });
};

export const useProblemProducts = (
    data: CommonSearchParams<Partial<ProductFilter>, string | string[]>,
    isEnabled = true
) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Product[]>, FetchError>({
        enabled: isEnabled,
        queryKey: ['problem-products'],
        queryFn: () => apiClient.post(`${API_URL}/drafts:search`, { data }),
    });
};

export const useProductGroupFilters = (data: ProductGroupFilterParams) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<ProductGroupFilterResponse[]>, FetchError>({
        enabled: typeof data.category === 'number',
        queryKey: ['product-group-filters', data],
        queryFn: () => apiClient.post(`cms/product-group-filters:search`, { data }),
    });
};

/** TODO методы cms должны лежать в соответствующей папочке api */
export const useProductGroupTypes = (data: ProductGroupTypeParams) => {
    const apiClient = useAuthApiClient();

    return useQuery<ProductGroupTypeResponse, FetchError>({
        queryKey: ['product-group-types', data],
        queryFn: () => apiClient.post(`cms/product-group-types:search`, { data }),
    });
};

export const useProductGroups = (data: ProductGroupSearchParams) => {
    const apiClient = useAuthApiClient();

    return useQuery<ProductGroupSearchResponse, FetchError>({
        queryKey: ['product-groups', data],
        queryFn: () => apiClient.post(`cms/product-groups:search`, { data }),
    });
};

export const useProductGroup = (data: ProductGroupSearchParams) => {
    const apiClient = useAuthApiClient();

    return useQuery<ProductGroupSearchOneResponse | undefined, FetchError>({
        enabled: typeof data.filter?.id === 'number',
        queryKey: [`product-group-${data.filter?.id || -1}`, data.filter?.id],
        queryFn: () => {
            if (!data.filter?.id)
                return new Promise(resolve => {
                    resolve(undefined);
                });
            return apiClient.post(`cms/product-groups:search-one`, { data });
        },
    });
};

export const useCreateProductGroup = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<ProductGroupCreateResponse, FetchError, ProductGroupCreateParams>({
        mutationFn: data => apiClient.post('cms/product-groups', { data }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: ['product-groups'],
            }),
    });
};

export const useMassPatchProducts = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<ProductMassPatchResponse, FetchError, ProductMassPatchRequest>({
        mutationFn: data => apiClient.post(`catalog/products:mass-patch`, { data, timeout: 30000 }),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchProducts() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.getProduct() });
        },
    });
};

export const useMassPatchProductsByQuery = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, ProductMassPatchByQueryRequest>({
        mutationFn: data => apiClient.post(`catalog/products:mass-patch-by-query`, { data, timeout: 30000 }),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchProducts() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.getProduct() });
        },
    });
};

export const useUpdateProductGroup = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<
        ProductGroupSearchOneResponse,
        FetchError,
        ProductGroupCreateParams & {
            id: number;
        }
    >({
        mutationFn: data => apiClient.put(`cms/product-groups/${data.id}`, { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({
                queryKey: ['product-groups'],
            });
            queryClient.invalidateQueries({
                queryKey: [`product-group-${data.id}`],
            });
        },
    });
};

export const useProductDetail = ({ id, include }: { id: string; include?: string | string[] }, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<ProductDetail>, FetchError>({
        queryKey: QueryKeys.getProduct(id),
        queryFn: () => apiClient.get(`${API_URL}/drafts/${id}${include ? `?include=${include}` : ''}`),
        enabled: isDetailIdValid(id) && enabled,
    });
};

type PartialNullable<T extends Record<string, any>> = {
    [K in keyof T]?: T[K] | null;
};

export const useCreateProduct = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<ProductDetail>, FetchError, PartialNullable<ProductCreateParams>>({
        mutationFn: data => apiClient.post(`${API_URL}`, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchProducts() });
        },
    });
};

export const useProductDetailUpdate = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<
        CommonResponse<ProductDetail>,
        FetchError,
        { id: number; data: PartialNullable<ProductCreateParams> }
    >({
        mutationFn: productData => {
            const { id, ...data } = productData;
            return apiClient.put(`${API_URL}/${id}`, { ...data });
        },

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchProducts() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.getProduct() });
        },
    });
};

export const useProductDetailPartUpdate = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<
        ProductMassPatchResponse,
        FetchError,
        { id: number; data: PartialNullable<ProductCreateParams> }
    >({
        mutationFn: productData => {
            const { id, ...data } = productData;
            return apiClient.patch(`${API_URL}/${id}`, { ...data });
        },

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchProducts() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.getProduct() });
        },
    });
};

export const useProductDetailDelete = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<ProductDetail>, FetchError, number>({
        mutationFn: id => apiClient.delete(`${API_URL}/${id}`),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchProducts() });
        },
    });
};

export const useUpdateProductAttributes = () => {
    const apiClient = useAuthApiClient();
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<ProductDetail>, FetchError, { id: number; attributes: ProductAttributeValue[] }>({
        mutationFn: data => {
            const { id, attributes } = data;
            return apiClient.patch(`${API_URL}/${id}/attributes`, { data: { attributes } });
        },

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.getProduct() });
        },
    });
};

export const useDeleteProductAttributes = () => {
    const apiClient = useAuthApiClient();
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<ProductDetail>, FetchError, { id: number; property_ids: number[] }>({
        mutationFn: ({ id, ...data }) => apiClient.delete(`${API_URL}/${id}/attributes`, { data }),

        onSuccess: () => {
            queryClient.removeQueries({ queryKey: QueryKeys.getProduct() });
        },
    });
};

export const useProductPreloadImage = (defaultConfig?: Config) => {
    const apiClient = useAuthApiClient();

    return useMutation<
        CommonResponse<ProductPreloadImage>,
        FetchError,
        { formData: FormData; propertyId?: number | string; config?: Config }
    >({
        mutationFn: async ({ formData, propertyId, config = { timeout: 30000 } }) => {
            const res = await apiClient.post(`${API_URL}:preload-image`, {
                data: formData,
                ...(config || defaultConfig),
            });

            return { ...res, ...(propertyId && { propertyId }) };
        },
    });
};

export const useProductUploadImage = (defaultConfig?: Config) => {
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<ProductImage>, FetchError, { id: number; data: ProductImage; config?: Config }>({
        mutationFn: ({ id, data, config = { timeout: 30000 } }) =>
            apiClient.post(`${API_URL}/${id}:upload-image`, { data, ...(config || defaultConfig) }),
    });
};

export const useProductUpdateAllImages = (defaultConfig?: Config) => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<
        CommonResponse<ProductImage[]>,
        FetchError,
        {
            id: number;
            imgs: {
                id?: number;
                preload_file_id?: number;
                name: string;
                sort: number;
            }[];
            config?: Config;
        }
    >({
        mutationFn: ({ id, imgs, config }) =>
            apiClient.put(`${API_URL}/${id}/images`, { data: { images: imgs }, ...(config || defaultConfig) }),
        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchProducts() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.getProduct() });
        },
    });
};

export const useProductDeleteImage = () => {
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, { id: number; file_id: number }>({
        mutationFn: dt => {
            const { id, file_id } = dt;
            return apiClient.post(`${API_URL}/${id}:delete-image`, { data: { file_id } });
        },
    });
};

/**
 Добавление новых и обновление только заданных картинок. Не указанные в запросе не изменяются.
 */
export const useProductUpdateSeveralImages = () => {
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<ProductImage[]>, FetchError, { id: number; imgs: ProductImage[] }>({
        mutationFn: dt => {
            const { id, imgs } = dt;
            return apiClient.patch(`${API_URL}/${id}/images`, { data: { images: imgs } });
        },
    });
};

export const useDeleteProductGroupFile = () => {
    const apiClient = useAuthApiClient();

    return useMutation<
        CommonResponse<null>,
        FetchError,
        {
            id: number;
        }
    >({
        mutationFn: ({ id }) => apiClient.post(`cms/product-groups/${id}:delete-file`),
    });
};

export const useDeleteProductGroup = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<
        {
            data: null;
            errors?: ApiError[];
        },
        FetchError,
        {
            id: number;
        }
    >({
        mutationFn: ({ id }) => apiClient.delete(`cms/product-groups/${id}`),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: ['product-groups'],
            });
        },
    });
};

export const useProductsTypes = (isEnabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<ProductsTypes[]>, FetchError>({
        enabled: isEnabled,
        queryKey: ['productsTypes'],
        queryFn: () => apiClient.get(`${API_URL}/product-types`),
    });
};
