import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, CommonSearchParams } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { FetchError } from '../index';
import { Country, CountryData, CountryDataWithId, CountryFilters } from './types';

const COUNTRIES_BASE_URL = 'catalog/countries';
const COUNTRIES_KEY = 'countries';

const useGetCountries = (data: CommonSearchParams<CountryFilters>) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Country[]>, FetchError>({
        queryKey: [COUNTRIES_KEY, data],
        queryFn: () => apiClient.post(`${COUNTRIES_BASE_URL}:search`, { data }),
    });
};

const usePostCountry = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Country>, FetchError, CountryData>({
        mutationFn: data => apiClient.post(COUNTRIES_BASE_URL, { data }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [COUNTRIES_KEY],
            }),
    });
};

const usePutCountry = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Country>, FetchError, CountryDataWithId>({
        mutationFn: data => apiClient.put(`${COUNTRIES_BASE_URL}/${data.id}`, { data }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [COUNTRIES_KEY],
            }),
    });
};

const useRemoveCountry = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, number | string>({
        mutationFn: id => apiClient.delete(`${COUNTRIES_BASE_URL}/${id}`),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [COUNTRIES_KEY],
            }),
    });
};

export { useGetCountries, usePostCountry, usePutCountry, useRemoveCountry };
