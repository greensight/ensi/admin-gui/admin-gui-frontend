import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, CommonSearchParams, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { FetchError } from '../index';
import { GetOfferResponse, Offer, PatchOfferRequest } from './types';

const OFFERS_BASE_URL = 'catalog/offers';
const QueryKeys = {
    getOffer: (id?: number | string) => (id ? ['get-offer', id] : ['get-offer']),
    searchOffers: (data?: any) => (data ? ['search-offers', data] : ['search-offers']),
    getOffersMeta: () => ['get-offers-meta'],
};

/** Запуск обновления данных сущностей сервиса offers из мастер систем */
export const useSyncEntities = () => {
    const apiClient = useAuthApiClient();
    const queryClient = useQueryClient();

    return useMutation<CommonResponse<any>, FetchError, null | undefined>({
        mutationFn: () => apiClient.post(`${OFFERS_BASE_URL}/entities:sync`, {}),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchOffers() });
        },
    });
};

//* Запуск синхронизации состояния офферов относительно PIM и складов */
export const useOffersSynchronization = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError>({
        mutationFn: () => apiClient.post(`${OFFERS_BASE_URL}:migrate`),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchOffers() });
        },
    });
};

export const useSearchOffers = (data: CommonSearchParams<any>, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Offer[]>, FetchError>({
        queryKey: QueryKeys.searchOffers(data),
        queryFn: () => apiClient.post(`${OFFERS_BASE_URL}:search`, { data }),
        enabled,
    });
};

export const useOffersMeta = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Meta>, FetchError>({
        queryKey: QueryKeys.getOffersMeta(),
        queryFn: () => apiClient.get(`${OFFERS_BASE_URL}:meta`),
        enabled,
    });
};

/** Запрос на получение оффера */
export function useOffer({ id }: { id: number | string }, params: Record<string, any> = {}, enabled = true) {
    const apiClient = useAuthApiClient();

    return useQuery<GetOfferResponse, FetchError>({
        queryKey: QueryKeys.getOffer(id),
        queryFn: () => apiClient.get(`${OFFERS_BASE_URL}/${id}`, { params }),
        enabled: isDetailIdValid(id) && enabled,
    });
}

/** Запрос на обновление отдельных полей оффера */
export const usePatchOffer = () => {
    const apiClient = useAuthApiClient();
    const queryClient = useQueryClient();

    return useMutation<GetOfferResponse, FetchError, { id: number | string } & PatchOfferRequest>({
        mutationFn: ({ id, ...data }) => apiClient.patch(`${OFFERS_BASE_URL}/${id}`, { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.getOffer(data?.id) });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchOffers() });
        },
    });
};
