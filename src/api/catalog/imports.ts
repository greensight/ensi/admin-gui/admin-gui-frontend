import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { FetchError } from '..';
import {
    CreateProductImportRequest,
    PreloadFileResponse,
    ProductImportResponse,
    SearchProductImportWarningsRequest,
    SearchProductImportWarningsResponse,
    SearchProductImportsRequest,
    SearchProductImportsResponse,
} from './types/imports';

export const QueryKeys = {
    searchProductImports: (data?: Record<string, any>) =>
        data ? ['search-product-imports', data] : ['search-product-imports'],
    getProductImportsMeta: () => ['get-product-imports-meta'],
    searchProductImportWarnings: (data?: Record<string, any>) =>
        data ? ['search-product-import-warnings', data] : ['search-product-import-warnings'],
    getProductImportWarningsMeta: () => ['get-product-import-warnings-meta'],
};

/** Запрос на регистрацию нового импорта товаров */
export const useCreateProductImport = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<ProductImportResponse, FetchError, CreateProductImportRequest>({
        mutationFn: data => apiClient.post('catalog/product-imports', { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: QueryKeys.searchProductImports(),
            });
        },
    });
};

/** Загрузка файла для последущего импорта. */
export const usePreloadImportFile = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<PreloadFileResponse, FetchError, { formData: FormData }>({
        mutationFn: data =>
            apiClient.post('catalog/product-imports:preload-file', {
                data: data.formData,
            }),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: QueryKeys.searchProductImports(),
            });
        },
    });
};

/** Поиск записей журнала импорта данных по товарам */
export function useSearchProductImports(data: SearchProductImportsRequest, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<SearchProductImportsResponse, FetchError>({
        queryKey: QueryKeys.searchProductImports(data),
        queryFn: () => apiClient.post('catalog/product-imports:search', { data }),
        enabled,
    });
}

/** Получение списка доступных полей */
export function useProductImportsMeta(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<Meta>, FetchError>({
        queryKey: QueryKeys.getProductImportsMeta(),
        queryFn: () => apiClient.get('catalog/product-imports:meta', {}),
        enabled,
    });
}

/** Поиск сообщений об ошибках в журнале импорта данных по товарам */
export function useSearchProductImportWarnings(data: SearchProductImportWarningsRequest, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<SearchProductImportWarningsResponse, FetchError>({
        queryKey: QueryKeys.searchProductImportWarnings(data),
        queryFn: () => apiClient.post('catalog/product-import-warnings:search', { data }),
        enabled,
    });
}

/** Получение списка доступных полей */
export function useProductImportWarningsMeta(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<Meta>, FetchError>({
        queryKey: QueryKeys.getProductImportWarningsMeta(),
        queryFn: () => apiClient.get('catalog/product-import-warnings:meta', {}),
        enabled,
    });
}
