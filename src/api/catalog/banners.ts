import { useMutation } from '@tanstack/react-query';

import { CommonResponse } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { FetchError } from '..';
import { BannerCreateImageParams, BannerCreateParams, BannerCreateResponse } from './types/banners';

const BASE_URL = 'cms/banners';

export const useCreateProductGroupBanner = () => {
    const apiClient = useAuthApiClient();

    return useMutation<BannerCreateResponse, FetchError, BannerCreateParams>({
        mutationFn: data => apiClient.post(`${BASE_URL}`, { data }),
    });
};

export const useUpdateProductGroupBanner = () => {
    const apiClient = useAuthApiClient();

    return useMutation<
        BannerCreateResponse,
        FetchError,
        BannerCreateParams & {
            id: number;
        }
    >({
        mutationFn: data => apiClient.put(`${BASE_URL}/${data.id}`, { data }),
    });
};

export const useUploadProductGroupBannerFile = () => {
    const apiClient = useAuthApiClient();

    return useMutation<{ data: { url: string } }, FetchError, BannerCreateImageParams>({
        mutationFn: data =>
            apiClient.post(`${BASE_URL}/${data.id}:upload-file`, {
                data: data.formData,
            }),
    });
};

export const useDeleteProductGroupBannerFile = () => {
    const apiClient = useAuthApiClient();

    return useMutation<
        BannerCreateResponse,
        FetchError,
        {
            id: number;
            /**
             * @example
             * const formData = new FormData()
             * formData.append('type', 'desktop' | 'mobile')
             */
            formData: FormData;
        }
    >({
        mutationFn: data =>
            apiClient.post(`${BASE_URL}/${data.id}:delete-file`, {
                data: data.formData,
            }),
    });
};

export const useDeleteProductGroupBanner = () => {
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, number | string>({
        mutationFn: id => apiClient.delete(`${BASE_URL}/${id}`),
    });
};
