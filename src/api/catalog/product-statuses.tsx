import { Badge, SelectItem } from '@ensi-platform/core-components';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonOption, CommonResponse, FetchError, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import {
    CreateStatusSettingRequest,
    PatchStatusSettingRequest,
    SearchEnumValuesBody,
    SearchEnumValuesResponse,
    SearchStatusSettingsRequest,
    SetPreviousStatusRequest,
    StatusSettingProperties,
    StatusSettingResponse,
    StatusSettingsResponse,
    StatusesResponse,
} from './types/product-statuses';

export const QueryKeys = {
    searchProductStatusEnumValues: (data?: Record<string, any>) =>
        data ? ['search-product-status-enum-values', data] : ['search-product-status-enum-values'],
    searchProductStatuses: (data?: Record<string, any>) =>
        data ? ['search-product-statuses', data] : ['search-product-statuses'],
    searchNextProductStatuses: (id?: number | string) =>
        id ? ['search-next-product-statuses', id] : ['search-next-product-statuses'],
    getProductStatusesMeta: () => ['get-product-statuses-meta'],
    getProductStatus: (id?: number | string) => (id ? ['get-product-status', id] : ['get-product-status']),
    getProductStatusType: () => ['get-product-status-type'],
    getProductEvents: () => ['get-product-events'],
    getProductEventOperations: () => ['get-product-event-operations'],
};

/** Получение доступных статусов из текущего */
export function useNextProductStatuses({ id }: { id: number | string | undefined }, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<StatusesResponse, FetchError>({
        queryKey: QueryKeys.searchNextProductStatuses(id),
        queryFn: () =>
            apiClient.post(`catalog/product-statuses:next`, {
                data: {
                    id,
                },
            }),
        enabled,
        gcTime: 5000,
    });
}

/** Установка статусов из которых можно перейти в текущий */
export const useSetPreviousProductStatuses = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<StatusSettingResponse, FetchError, { id: number | string } & SetPreviousStatusRequest>({
        mutationFn: ({ id, ...data }) => apiClient.post(`catalog/product-statuses/${id}:set-previous`, { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.getProductStatus(data?.id) });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchNextProductStatuses() });
        },
    });
};

/** Поиск статусов продукта для справочника */
export function useSearchProductStatusEnumValues(data: SearchEnumValuesBody, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<SearchEnumValuesResponse, FetchError>({
        queryKey: QueryKeys.searchProductStatusEnumValues(data),
        queryFn: () => apiClient.post('catalog/product-status-enum-values:search', { data }),
        enabled,
    });
}

/** Поиск объектов типа ProductStatusSetting */
export function useSearchProductStatuses(data: SearchStatusSettingsRequest, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<StatusSettingsResponse, FetchError>({
        queryKey: QueryKeys.searchProductStatuses(data),
        queryFn: () =>
            apiClient.post('catalog/product-statuses:search', {
                data,
                params: {
                    include: 'previous_statuses,next_statuses',
                },
            }),
        enabled,
    });
}

/** Получение списка доступных полей */
export function useProductStatusesMeta(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<Meta>, FetchError>({
        queryKey: QueryKeys.getProductStatusesMeta(),
        queryFn: () => apiClient.get('catalog/product-statuses:meta', {}),
        enabled,
    });
}

/** Создание объекта типа ProductStatusSetting */
export const useCreateProductStatus = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<StatusSettingResponse, FetchError, CreateStatusSettingRequest>({
        mutationFn: data => apiClient.post('catalog/product-statuses', { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchProductStatuses() });

            queryClient.invalidateQueries({ queryKey: QueryKeys.getProductStatus(data?.id) });
        },
    });
};

/** Получение объекта типа ProductStatusSetting по идентификатору */
export function useProductStatus({ id }: { id: number | string }, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<StatusSettingResponse, FetchError>({
        queryKey: QueryKeys.getProductStatus(id),
        queryFn: () =>
            apiClient.get(`catalog/product-statuses/${id}`, {
                params: {
                    include: 'previous_statuses,next_statuses',
                },
            }),
        enabled: isDetailIdValid(id) && enabled,
    });
}

/** Изменение объекта типа ProductStatusSetting */
export const usePatchProductStatus = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<StatusSettingResponse, FetchError, { id: number | string } & PatchStatusSettingRequest>({
        mutationFn: ({ id, ...data }) => apiClient.patch(`catalog/product-statuses/${id}`, { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchProductStatuses() });

            queryClient.invalidateQueries({ queryKey: QueryKeys.getProductStatus(data?.id) });
        },
    });
};

/** Удаление объекта типа ProductStatusSetting */
export const useDeleteProductStatus = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<CommonResponse<null>, FetchError, { id: number | string }>({
        mutationFn: ({ id }) => apiClient.delete(`catalog/product-statuses/${id}`, {}),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchProductStatuses() });
        },
    });
};

/** Получение объектов типа CatalogProductStatusTypeEnum */
export function useProductStatusType(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: QueryKeys.getProductStatusType(),
        queryFn: () => apiClient.get('catalog/product-status-types', {}),
        enabled,
    });
}

/** Получение объектов типа CatalogProductEventEnum */
export function useProductEvents(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: QueryKeys.getProductEvents(),
        queryFn: () => apiClient.get('catalog/product-events', {}),
        enabled,
    });
}

/** Получение объектов типа CatalogEventOperationEnum */
export function useProductEventOperations(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: QueryKeys.getProductEventOperations(),
        queryFn: () => apiClient.get('catalog/product-event-operations', {}),
        enabled,
    });
}

export const useProductStatusAutocomplete = () => {
    const apiClient = useAuthApiClient();

    const statusSearchFn = async (query: string) => {
        const res = (await apiClient.post(`catalog/product-statuses:search`, {
            data: {
                filter: {
                    name_like: query,
                },
            },
        })) as CommonResponse<StatusSettingProperties[]>;
        return {
            options: res.data.map(e => ({
                label: e.name,
                value: e.id,
                content: <Badge bgColor={e.color} text={e.name} />,
            })) as SelectItem[],
            hasMore: false,
        };
    };

    const statusOptionsByValuesFn = async (vals: number[] | { value: number }[], abortController: AbortController) => {
        const actualValues = vals.map(v => {
            if (typeof v === 'object' && 'value' in v) return Number(v.value);
            return Number(v);
        });

        const res = (await apiClient.post(`catalog/product-statuses:search`, {
            data: {
                filter: { id: actualValues },
            },
            abortController,
        })) as CommonResponse<StatusSettingProperties[]>;

        return res.data.map(e => ({
            label: e.name,
            value: e.id,
            content: <Badge bgColor={e.color} text={e.name} />,
        }));
    };

    return { statusSearchFn, statusOptionsByValuesFn };
};
