import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, CommonSearchParams, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { FetchError } from '../index';
import {
    ProductGroupsBindData,
    ProductGroupsData,
    ProductGroupsFields,
    ProductGroupsFilter,
    ProductGroupsUpdate,
    ProductGroupsUpdateProducts,
} from './types';

const baseURL = 'catalog/product-groups';

export const useProductsGroupsMeta = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<{ data: Meta }, FetchError>({
        queryKey: ['product-groups-meta'],
        queryFn: () => apiClient.get(`${baseURL}:meta`),
        enabled,
    });
};

export const useProductsGroups = (data: CommonSearchParams<ProductGroupsFilter, string>, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<ProductGroupsData[]>, FetchError>({
        queryKey: ['product-groups', data],
        queryFn: () => apiClient.post(`${baseURL}:search`, { data }),
        enabled,
    });
};

export const useProductsGroupsOne = (data: CommonSearchParams<ProductGroupsFilter, string>, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<ProductGroupsData>, FetchError>({
        queryKey: ['product-groups-one', data],
        queryFn: () => apiClient.post(`${baseURL}:search-one?include=products`, { data: JSON.stringify(data) }),
        enabled,
    });
};

export const useProductsGroupsCreate = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<ProductGroupsData>, FetchError, ProductGroupsFields>({
        mutationFn: productsGroup => apiClient.post(`${baseURL}`, { data: productsGroup }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: ['product-groups'],
            }),
    });
};

export const useProductsGroupsUpdate = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<ProductGroupsData>, FetchError, ProductGroupsUpdate>({
        mutationFn: productsGroup => {
            const { id, data } = productsGroup;
            return apiClient.patch(`${baseURL}/${id}`, { data });
        },

        onSuccess: data => {
            queryClient.invalidateQueries({
                queryKey: [`product-groups-${data?.data?.id}`],
            });
            queryClient.invalidateQueries({
                queryKey: [`product-groups`],
            });
        },
    });
};

export const useProductsGroupDetail = (id?: number | string, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<ProductGroupsData>, FetchError>({
        queryKey: [`product-groups-${id}`],
        queryFn: () => apiClient.get(`${baseURL}/${id}?include=category,products`),
        enabled: isDetailIdValid(id) && enabled,
    });
};

export const useProductsGroupsUpdateProducts = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<ProductGroupsBindData>, FetchError, ProductGroupsUpdateProducts>({
        mutationFn: productsGroup => {
            const { id, ...data } = productsGroup;
            return apiClient.post(`${baseURL}/${id}:bind-products`, { data });
        },

        onSuccess: data => {
            queryClient.invalidateQueries({
                queryKey: [`product-groups-${data?.data?.product_group?.id}`],
            });
            queryClient.invalidateQueries({
                queryKey: [`product-groups`],
            });
        },
    });
};

export const useProductsGroupsDelete = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<any>, FetchError, number>({
        mutationFn: id => apiClient.delete(`${baseURL}/${id}`),

        onSuccess: data => {
            queryClient.invalidateQueries({
                queryKey: [`product-groups-${data?.data?.id}`],
            });
            queryClient.invalidateQueries({
                queryKey: [`product-groups`],
            });
        },
    });
};
