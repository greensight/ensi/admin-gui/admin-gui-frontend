import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonOption, CommonResponse, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { FetchError } from '../index';
import {
    FeedResponse,
    FeedSettingsForCreate,
    FeedSettingsForPatch,
    FeedSettingsResponse,
    SearchFeedSettingsRequest,
    SearchFeedSettingsResponse,
    SearchFeedsRequest,
    SearchFeedsResponse,
} from './types/feeds';

const QueryKeys = {
    getFeed: (id?: number | string) => (id ? ['get-feed', id] : ['get-feed']),
    searchFeeds: (data?: Record<string, any>) => (data ? ['search-feeds', data] : ['search-feeds']),
    getFeedsMeta: () => ['get-feeds-meta'],
    getFeedTypes: () => ['get-feed-types'],
    getFeedPlatforms: () => ['get-feed-platforms'],
    getFeedSettings: (id?: number | string) => (id ? ['get-feed-settings', id] : ['get-feed-settings']),
    searchFeedSettings: (data?: Record<string, any>) =>
        data ? ['search-feed-settings', data] : ['search-feed-settings'],
    getFeedSettingsMeta: () => ['get-feed-settings-meta'],
};

/** Получение объекта типа Feed */
export function useFeed({ id }: { id: number | string }, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<FeedResponse, FetchError>({
        queryKey: QueryKeys.getFeed(id),
        queryFn: () => apiClient.get(`catalog/feeds/${id}`, {}),
        enabled,
    });
}

/** Поиск объектов типа Feed */
export function useSearchFeeds(data: SearchFeedsRequest, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<SearchFeedsResponse, FetchError>({
        queryKey: QueryKeys.searchFeeds(data),
        queryFn: () => apiClient.post('catalog/feeds:search', { data }),
        enabled,
    });
}

/** Получение списка доступных полей */
export function useFeedsMeta(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<Meta>, FetchError>({
        queryKey: QueryKeys.getFeedsMeta(),
        queryFn: () => apiClient.get('catalog/feeds:meta', {}),
        enabled,
    });
}

/** Получение объектов типа CatalogFeedTypeEnum */
export function useFeedTypes(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: QueryKeys.getFeedTypes(),
        queryFn: () => apiClient.get('catalog/feed-types', {}),
        enabled,
    });
}

/** Получение объектов типа CatalogFeedPlatformEnum */
export function useFeedPlatforms(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: QueryKeys.getFeedPlatforms(),
        queryFn: () => apiClient.get('catalog/feed-platforms', {}),
        enabled,
    });
}

/** Создание объекта типа FeedSettings */
export const useCreateFeedSettings = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<FeedSettingsResponse, FetchError, FeedSettingsForCreate>({
        mutationFn: data => apiClient.post('catalog/feed-settings', { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({
                queryKey: QueryKeys.getFeedSettings(data?.id),
            });

            queryClient.invalidateQueries({
                queryKey: QueryKeys.searchFeedSettings(),
            });
        },
    });
};

/** Получение объекта типа FeedSettings */
export function useFeedSettings({ id }: { id: number | string }, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<FeedSettingsResponse, FetchError>({
        queryKey: QueryKeys.getFeedSettings(id),
        queryFn: () => apiClient.get(`catalog/feed-settings/${id}`, {}),
        enabled: isDetailIdValid(id) && enabled,
    });
}

/** Обновления части полей объекта типа FeedSettings */
export const usePatchFeedSettings = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<FeedSettingsResponse, FetchError, { id: number | string } & FeedSettingsForPatch>({
        mutationFn: ({ id, ...data }) => apiClient.patch(`catalog/feed-settings/${id}`, { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({
                queryKey: QueryKeys.getFeedSettings(data?.id),
            });

            queryClient.invalidateQueries({
                queryKey: QueryKeys.searchFeedSettings(),
            });
        },
    });
};

/** Поиск объектов типа FeedSettings */
export function useSearchFeedSettings(data: SearchFeedSettingsRequest, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<SearchFeedSettingsResponse, FetchError>({
        queryKey: QueryKeys.searchFeedSettings(data),
        queryFn: () => apiClient.post('catalog/feed-settings:search', { data }),
        enabled,
    });
}

/** Получение списка доступных полей */
export function useFeedSettingsMeta(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<Meta>, FetchError>({
        queryKey: QueryKeys.getFeedSettingsMeta(),
        queryFn: () => apiClient.get('catalog/feed-settings:meta', {}),
        enabled,
    });
}

/** Запуск миграции данных для сервиса Feeds */

export const useFeedsMigration = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError>({
        mutationFn: () => apiClient.post('catalog/feeds/entities:migrate'),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchFeedSettings() });
        },
    });
};
