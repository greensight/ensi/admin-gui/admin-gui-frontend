import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, CommonSearchParams, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { FetchError } from '../index';
import { Brand, BrandBaseWithId, BrandPreloadImage, BrandsFilter, BrandsImageMutateParams } from './types';

const BRANDS_BASE_URL = 'catalog/brands';
const BRANDS_KEY = 'brands';

export const useBrands = (data?: CommonSearchParams<BrandsFilter, string>, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Brand[]>, FetchError>({
        queryKey: [BRANDS_KEY, data],
        queryFn: () => apiClient.post(`${BRANDS_BASE_URL}:search`, { data }),
        enabled,
    });
};

export const useBrandsMeta = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<{ data: Meta }, FetchError>({
        queryKey: [`${BRANDS_KEY}-meta`],
        queryFn: () => apiClient.get(`${BRANDS_BASE_URL}:meta`),
        enabled,
    });
};

export const useBrand = (id?: number | string, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Brand>, FetchError>({
        queryKey: [BRANDS_KEY, id],
        queryFn: () => apiClient.get(`${BRANDS_BASE_URL}/${id}`),
        enabled: isDetailIdValid(id) && enabled,
    });
};

export const useCreateBrand = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Brand>, FetchError, Omit<Brand, 'id'>>({
        mutationFn: data => apiClient.post(BRANDS_BASE_URL, { data }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [BRANDS_KEY],
            }),
    });
};

export const useEditBrand = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Brand>, FetchError, BrandBaseWithId>({
        mutationFn: data => apiClient.patch(`${BRANDS_BASE_URL}/${data.id}`, { data }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [BRANDS_KEY],
            }),
    });
};

export const useDeleteBrand = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, number>({
        mutationFn: id => apiClient.delete(`${BRANDS_BASE_URL}/${id}`),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [BRANDS_KEY],
            }),
    });
};

export const useBrandPreloadImage = () => {
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<BrandPreloadImage>, FetchError, { formData: FormData }>({
        mutationFn: data => apiClient.post(`${BRANDS_BASE_URL}:preload-image`, { data: data.formData }),
    });
};

export const useMutateBrandImage = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Brand>, FetchError, BrandsImageMutateParams>({
        mutationFn: ({ id, file }) => apiClient.post(`${BRANDS_BASE_URL}/${id}:upload-image`, { data: file }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [BRANDS_KEY],
            }),
    });
};

export const useDeleteBrandImage = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Brand>, FetchError, number>({
        mutationFn: id => apiClient.post(`${BRANDS_BASE_URL}/${id}:delete-image`),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [BRANDS_KEY],
            }),
    });
};
