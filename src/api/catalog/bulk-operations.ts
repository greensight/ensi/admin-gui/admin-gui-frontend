import { useQuery } from '@tanstack/react-query';

import { CommonResponse, CommonSearchParams, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { FetchError } from '../index';
import { CatalogBulkOperation, CatalogBulkOperationsFilter } from './types/bulk-operations';

const BASE_URL = 'catalog/bulk-operations';
const BASE_KEY = 'catalog-bulk';

export const useCatalogBulkOperations = (data?: CommonSearchParams<CatalogBulkOperationsFilter>, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<CatalogBulkOperation[]>, FetchError>({
        queryKey: [BASE_KEY, data],
        queryFn: () =>
            apiClient.post(`${BASE_URL}:search`, {
                data,
                params: {
                    include: 'errors',
                },
            }),
        enabled: !Array.isArray(data?.filter?.id) ? isDetailIdValid(data?.filter?.id) && enabled : enabled,
    });
};

export const useCatalogBulkOperationsMeta = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<{ data: Meta }, FetchError>({
        queryKey: [`${BASE_KEY}-meta`],
        queryFn: () => apiClient.get(`${BASE_URL}:meta`),
        enabled,
    });
};
