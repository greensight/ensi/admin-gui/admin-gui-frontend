import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, CommonSearchParams, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { FetchError } from '../index';
import { COMMON_ATTRIBUTES_KEY } from './products';
import {
    CategoriesFilter,
    Category,
    CategoryBindPropertyData,
    CategoryFormData,
    CategoryItemFormData,
    CategoryTreeItem,
    Product,
} from './types';

const API_URL = 'catalog/categories';
const API_CATEGORY_URL = 'catalog/category';

const QUERY_KEY = 'categories';

export const useCategoriesMeta = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<{ data: Meta }, FetchError>({
        queryKey: ['categories-meta'],
        queryFn: () => apiClient.get(`${API_URL}:meta`),
        enabled,
    });
};

export const useCategoryPropertiesMeta = () => {
    const apiClient = useAuthApiClient();

    return useQuery<{ data: Meta }, FetchError>({
        queryKey: ['category-properties-meta'],
        queryFn: () => apiClient.get(`catalog/category-properties:meta`),
    });
};

export const useCategories = (data: CommonSearchParams<CategoriesFilter>, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Category[]>, FetchError>({
        queryKey: [QUERY_KEY, data],
        queryFn: () => apiClient.post(`${API_URL}:search`, { data }),
        enabled,
    });
};

export const useCategoriesTree = (data?: CommonSearchParams<CategoriesFilter>, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<CategoryTreeItem[]>, FetchError>({
        queryKey: [`${QUERY_KEY}-tree`, data],
        queryFn: () => apiClient.post(`${API_URL}:tree`, { data }),
        enabled,
    });
};

export const useCategoryDetail = (id?: number | string, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Category>, FetchError>({
        queryKey: [`category-${id}`],
        queryFn: () => apiClient.get(`${API_URL}/${id}?include=properties,hidden_properties`),
        enabled: isDetailIdValid(id) && enabled,
    });
};

export const useCategoryCreate = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Category>, FetchError, CategoryFormData>({
        mutationFn: category => apiClient.post(API_URL, { data: category }),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY],
            });
            queryClient.invalidateQueries({
                queryKey: [`${QUERY_KEY}-tree`],
            });
        },
    });
};

export const useCategoryUpdate = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Category>, FetchError, CategoryItemFormData>({
        mutationFn: category => {
            const { id, ...categoryData } = category;
            return apiClient.put(`${API_URL}/${id}`, { data: categoryData });
        },

        onSuccess: data => {
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY],
            });
            queryClient.invalidateQueries({
                queryKey: [`category-${data?.data?.id}`],
            });
            queryClient.invalidateQueries({
                queryKey: ['products'],
            });
            queryClient.invalidateQueries({
                queryKey: [COMMON_ATTRIBUTES_KEY],
            });
        },
    });
};

export const useCategoryDelete = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, number>({
        mutationFn: id => apiClient.delete(`${API_URL}/${id}`),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY],
            });
            queryClient.invalidateQueries({
                queryKey: [`${QUERY_KEY}-tree`],
            });
            queryClient.invalidateQueries({
                queryKey: [COMMON_ATTRIBUTES_KEY],
            });
        },
    });
};

export const useCategoryBindProperties = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Category>, FetchError, CategoryBindPropertyData>({
        mutationFn: properties => {
            const { id, ...bindProperties } = properties;

            return apiClient.post(`${API_URL}/${id}:bind-properties`, { data: bindProperties });
        },

        onSuccess: (data, variables) => {
            queryClient.invalidateQueries({
                queryKey: [`category-${variables.id}`],
            });
            queryClient.invalidateQueries({
                queryKey: ['product'],
            });
            queryClient.invalidateQueries({
                queryKey: ['products'],
            });
        },
    });
};

export const useCategoryEnumValuesSearchFn = () => {
    const apiClient = useAuthApiClient();

    const categorySearchFn = async (query: string) => {
        const res = await apiClient.post(`${API_CATEGORY_URL}-enum-values:search`, {
            data: { filter: { query } },
        });
        return {
            options: res.data.map((i: { title: string; id: string }) => ({
                label: i.title,
                value: i.id,
            })),
            hasMore: false,
        };
    };

    return { categorySearchFn };
};

export const useCategoryEnumOptionsByValuesFn = () => {
    const apiClient = useAuthApiClient();

    const categoryOptionsByValuesFn = async (vals: string[]) => {
        const res = await apiClient.post(`${API_CATEGORY_URL}-enum-values:search`, {
            data: { filter: { query: '', id: vals } },
        });
        return res.data.map((i: { title: string; id: string }) => ({
            label: i.title,
            value: i.id,
        }));
    };

    return { categoryOptionsByValuesFn };
};

export const useProductsSearch = () => {
    const apiClient = useAuthApiClient();
    const productsSearchFn = async (value: string) => {
        const filter = {
            name: value || '',
            has_product_groups: false,
            category_has_is_gluing: true,
        };

        const res: CommonResponse<Product[]> = await apiClient.post('catalog/products:search', {
            data: { filter },
        });

        return {
            options: res.data.map(p => ({
                label: p.name,
                value: p as any,
            })),
            hasMore: false,
        };
    };
    return { productsSearchFn };
};

export const useProductOptionsByValuesFn = () => {
    const productOptionsByValuesFn = async (vals: Product[]) => {
        if (!vals?.[0]) return [];

        return [
            {
                label: vals[0].name,
                value: vals[0] as any,
            },
        ];
    };

    return { productOptionsByValuesFn };
};
