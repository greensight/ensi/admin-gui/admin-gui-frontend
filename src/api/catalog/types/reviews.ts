import { CommonResponse, CommonSearchParams } from '@api/common/types';
import { Customer } from '@api/customers/types';

import { Product } from '.';

export interface Review {
    id: number;
    comment: string;
    grade: number;
    status_id: number;
    created_at: string;
    updated_at: string;

    product?: Product;
    customer?: Customer;
}

export type PatchReviewRequest = Partial<Omit<Review, 'id' | 'updated_at' | 'created_at'>> & { id: number };

export type ReviewFilters = Partial<Review>;

export type SearchReviewsRequest = CommonSearchParams<ReviewFilters, string | string[]>;
export type SearchReviewsResponse = CommonResponse<Review[]>;

export type GetReviewResponse = CommonResponse<Review>;
