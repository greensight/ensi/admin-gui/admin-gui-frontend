import { CommonResponse, CommonSearchParams, OffsetPaginationQuery } from '@api/common/types';

type Prettify<T> = {
    [K in keyof T]: T[K];
};

export interface FeedSettingsFillableProperties {
    /**
     * Название фида
     * @example "Фид для Яндекс.Маркета"
     */
    name: string;
    /**
     * Код
     * @example "yandex.xml"
     */
    code: string;
    /**
     * Активность фида
     */
    active: boolean;
    /**
     * Тип фида из FeedTypeEnum
     * @example 1
     */
    type: number;
    /**
     * Платформа фида из FeedPlatformEnum
     * @example 1
     */
    platform: number;
    /**
     * Добавлять в фид только активные товары
     */
    active_product: boolean;
    /**
     * Добавлять в фид только активные категории
     */
    active_category: boolean;
    /**
     * Название магазина
     * @example "Shop ENSI"
     */
    shop_name: string;
    /**
     * Сайт магазина
     * @example "https://example.com"
     */
    shop_url: string;
    /**
     * Название организации
     * @example "OOO ENSI"
     */
    shop_company: string;
    /**
     * Частота обновления (в часах)
     * @example 4
     */
    update_time: number;
    /**
     * Время хранения старых версий фидов (в днях)
     * @example 1
     */
    delete_time: number;
}

/**
 * Создание объекта типа FeedSettings */

export type FeedSettingsForCreate = Prettify<FeedSettingsFillableProperties>;

export interface FeedSettingsReadonlyProperties {
    /**
     * Идентификатор
     */
    id: number;
    /**
     * Дата создания
     * @example "2021-12-20T18:00:10.000000Z"
     */
    created_at: string;
    /**
     * Дата обновления
     * @example "2021-12-20T18:00:10.000000Z"
     */
    updated_at: string;
}

export type FeedSettings = Prettify<FeedSettingsReadonlyProperties & FeedSettingsFillableProperties>;

export type FeedSettingsResponse = CommonResponse<FeedSettings>;

/**
 * Обновления части полей объекта типа FeedSettings */

export type FeedSettingsForPatch = Prettify<Partial<FeedSettingsFillableProperties>>;
/**
 * Поиск объектов типа FeedSettings
 */
export type SearchFeedSettingsRequest = CommonSearchParams<FeedSettings, string | string[]>;

export type SearchFeedSettingsResponse = CommonResponse<FeedSettings[]>;

export interface FeedReadonlyProperties {
    /**
     * Идентификатор
     */
    id: number;
    /**
     * Код фида
     * @example "yandex"
     */
    code: string;
    /**
     * Ссылка
     * @example "https://es.ensi-dev.greensight.ru/catalog/feeds/39/4c/1_desktop_19XRlBz795cFngxd6qwP.xml"
     */
    file_url: string;
    /**
     * плановаяя дата удаления фида
     * @example "2021-12-20T18:00:10.000000Z"
     */
    planned_delete_at: string;
    /**
     * Дата создания
     * @example "2021-12-20T18:00:10.000000Z"
     */
    created_at: string;
    /**
     * Дата обновления
     * @example "2021-12-20T18:00:10.000000Z"
     */
    updated_at: string;
}

export type Feed = Prettify<FeedReadonlyProperties> & {
    feed_settings?: FeedSettings;
};

/**
 * Получение объекта типа Feed */

export type FeedResponse = CommonResponse<Feed>;

/**
 * Поиск объектов типа Feed */

export interface SearchFeedsRequest {
    sort?: string | string[];
    filter: Record<string, any>;
    include?: string[];
    pagination: OffsetPaginationQuery;
}

export type SearchFeedsResponse = CommonResponse<Feed[]>;
