import { CommonResponse, CommonSearchParams } from '@api/common/types';

type Prettify<T> = {
    [K in keyof T]: T[K];
};

/**
 * Тип импорта:
 *
 * 1 - Свойства товара
 */
export enum CatalogProductImportTypeEnum {
    PRODUCT = 1,
}

/**
 * Тип импорта:
 *
 * 1 - Создан, ожидает обработки
 *
 * 2 - Обрабатывается
 *
 * 3 - Успешно завершён
 *
 * 4 - Завершён с ошибкой
 */
export enum CatalogProductImportStatusEnum {
    NEW = 1,
    IN_PROCESS = 2,
    DONE = 3,
    FAILED = 4,
}

/**
 * Запрос на регистрацию нового импорта товаров */
export interface CreateProductImportRequest {
    /**
     * ID ранее загруженного файла
     */
    preload_file_id: number;
    type: CatalogProductImportTypeEnum;
}

export interface ProductImportReadonlyProperties {
    /**
     * ID импорта
     * @example 100
     */
    id: number;
    /**
     * Дата создания
     * @example "2021-07-03T11:57:28.000000Z"
     */
    created_at: string;
    /**
     * Дата обновления
     * @example "2021-07-03T11:57:28.000000Z"
     */
    updated_at: string;
    status: CatalogProductImportStatusEnum;
    /**
     * Ссылка на файл
     * @example "https://www.example.com/dir/file.csv"
     */
    file: CatalogProductImportStatusEnum;
    /**
     * Имя файла
     * @example "file.csv"
     */
    file_name: CatalogProductImportStatusEnum;
    /**
     * Количество запланированных подзадач импорта
     * @example 10
     */
    chunks_count?: CatalogProductImportStatusEnum;
    /**
     * Количество завершившихся подзадач импорта
     * @example 2
     */
    chunks_finished: CatalogProductImportStatusEnum;
}

export type ProductImport = Prettify<
    ProductImportReadonlyProperties &
        ProductImportReadonlyProperties &
        ProductImportReadonlyProperties &
        ProductImportReadonlyProperties
>;
export interface ProductImportResponse {
    data: ProductImport;
}

/**
 * Поиск записей журнала импорта данных по товарам */
export type SearchProductImportsRequest = CommonSearchParams<any>;

export type ProductImportData = Prettify<
    ProductImportReadonlyProperties &
        ProductImportReadonlyProperties &
        ProductImportReadonlyProperties &
        ProductImportReadonlyProperties
>;
export type SearchProductImportsResponse = CommonResponse<ProductImportData[]>;

/**
 * Поиск сообщений об ошибках в журнале импорта данных по товарам */

export type SearchProductImportWarningsRequest = CommonSearchParams<any>;

export interface ProductImportWarningReadonlyProperties {
    /**
     * ID сообщения
     * @example 101
     */
    id: number;
    /**
     * Дата создания
     * @example "2021-07-03T11:57:28.000000Z"
     */
    created_at: string;
    /**
     * Дата обновления
     * @example "2021-07-03T11:57:28.000000Z"
     */
    updated_at: string;
    /**
     * ID импорта, которому принадлежит это сообщение
     * @example 101
     */
    import_id: number;
    /**
     * Артикул проблемного CC/SKU
     * @example "GJN026494-3"
     */
    vendor_code?: string;
    import_type: CatalogProductImportTypeEnum;
    /**
     * Сообщение
     * @example "Vendor code не найден"
     */
    message: CatalogProductImportTypeEnum;
}

export type ProductImportWarningData = Prettify<
    ProductImportWarningReadonlyProperties & ProductImportWarningReadonlyProperties
>;

export type SearchProductImportWarningsResponse = CommonResponse<ProductImportWarningData[]>;

export type PreloadFileResponse = CommonResponse<{
    /**
     * Идентификатор загруженного файла
     * @example 2032
     */
    preload_file_id: number;
    /**
     * Временный URL файла
     * @example "https://files.ensi.ru/catalog/12/6d/image_001.png"
     */
    url: string;
}>;
