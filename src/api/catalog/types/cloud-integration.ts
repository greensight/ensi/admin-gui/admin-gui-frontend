import { Prettify, RequireKeys } from '@api/common/types';

export interface CloudIntegrationReadonlyProperties {
    /**
     * Время создания
     * @example "2021-01-29T12:36:13.000000Z"
     */
    created_at: string;
    /**
     * Время обновления
     * @example "2021-01-29T12:36:13.000000Z"
     */
    updated_at: string;
}

export interface CloudIntegrationFillableProperties {
    /**
     * Приватный API-ключ
     */
    private_api_key?: string;
    /**
     * Публичный API-ключ
     */
    public_api_key?: string;
    /**
     * Интеграция с сервисом Ensi Cloud
     */
    integration?: boolean;
}

export type CloudIntegrationData = Prettify<
    RequireKeys<
        CloudIntegrationReadonlyProperties & CloudIntegrationFillableProperties,
        'private_api_key' | 'public_api_key' | 'integration'
    >
>;

/**
 * Получение объекта типа CloudIntegration
 */
export interface CloudIntegrationResponse {
    data: CloudIntegrationData;
}

/**
 * Создание объекта типа CloudIntegration
 */
export type CreateCloudIntegrationRequest = Prettify<
    RequireKeys<CloudIntegrationFillableProperties, 'private_api_key' | 'public_api_key' | 'integration'>
>;

/**
 * Изменение объектов типа CloudIntegration
 */
export type PatchCloudIntegrationRequest = Prettify<CloudIntegrationFillableProperties>;
