import { CommonResponse, CommonSearchParams, Prettify, RequireKeys } from '@api/common/types';

import { StockFillableProperties, StockReadonlyProperties } from './stocks';

export interface OfferReadonlyProperties {
    /**
     * Идентификатор оффера
     * @example 202
     */
    id: number;
    /**
     * Идентификатор товара, для которого создается оффер
     * @example 512
     */
    product_id: number;
    /**
     * Активность оффера (Вычисляется по активности товара, наличию стоков и т.п.)
     */
    is_active: boolean;
    /**
     * Дата создания оффера
     * @example "2021-06-11T11:27:10.000000Z"
     */
    created_at: string;
    /**
     * Дата обновления оффера
     * @example "2021-06-11T11:27:10.000000Z"
     */
    updated_at: string;
}

export interface OfferFillableProperties {
    /**
     * Признак активности для витрины
     */
    allow_publish?: boolean;
    /**
     * Цена предложения в коп.
     * @example 12500
     */
    price?: number;
}

export type OfferIncludesStocks = Prettify<RequireKeys<StockReadonlyProperties & StockFillableProperties, 'qty'>>;

export interface OfferIncludes {
    stocks?: OfferIncludesStocks[];
}

export type Offer = Prettify<
    RequireKeys<OfferReadonlyProperties & OfferFillableProperties & OfferIncludes, 'allow_publish' | 'price'>
>;

/**
 * Запрос на получение оффера
 */
export type GetOfferResponse = CommonResponse<Offer>;

/**
 * Запрос на обновление отдельных полей оффера
 */
export type PatchOfferRequest = Prettify<OfferFillableProperties>;
export type SearchOffersRequestFilter = Record<string, any>;

/**
 * Поиск офферов, удовлетворяющих фильтру
 */
export type SearchOffersRequest = CommonSearchParams<SearchOffersRequestFilter>;

export type SearchOffersResponse = CommonResponse<Offer[]>;
