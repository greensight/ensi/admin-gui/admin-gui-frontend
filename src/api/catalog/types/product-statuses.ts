import type { CommonResponse, OffsetPaginationQuery, Prettify, RequireKeys } from '@api/common/types';

export interface EnumValueInteger {
    /**
     * Числовой идентификатор значения
     */
    id?: number;
    /**
     * Человеко-понятное значение
     */
    title?: string;
}

/**
 * Поиск пунктов приема/выдачи товара для справочника
 */
export interface SearchEnumValuesResponse {
    data?: EnumValueInteger[];
}

/**
 * Значения
 */
export type SearchEnumValuesBodyId = Record<string, any>;
export interface SearchEnumValuesBodyFilter {
    /**
     * Текст, вводимый пользователем для фильтрации списка
     */
    query?: string;
    /**
     * Отфильтровать значения по идентификатору
     * @example 1,uuid
     */
    id?: SearchEnumValuesBodyId[];
}

/**
 * Поиск пунктов приема/выдачи товара для справочника
 */
export interface SearchEnumValuesBody {
    filter?: SearchEnumValuesBodyFilter;
}

/**
 * Условия для перехода в статус
 */
export interface EventConditionsEvents {
    /**
     * Операция из EventOperationEnum
     * @example 1
     */
    operation?: number | null;
    /**
     * Список событий
     * @example 1,3
     */
    events: number[];
}

export interface StatusSettingReadonlyProperties {
    /**
     * Идентификатор статуса
     * @example 1
     */
    id: number;
    /**
     * Время создания
     * @example "2021-01-29T12:36:13.000000Z"
     */
    created_at: string;
    /**
     * Время обновления
     * @example "2021-01-29T12:36:13.000000Z"
     */
    updated_at: string;
}

export enum ProductStatusTypeEnum {
    AUTO = 1,
    MANUAL
}

export interface StatusSettingFillableProperties {
    /**
     * Код статуса
     * @example "NEW"
     */
    code?: string;
    /**
     * Название статуса
     * @example "Новый"
     */
    name?: string;
    /**
     * Цвет статуса
     * @example "#FFFF00"
     */
    color?: string;
    /**
     * Тип статуса из ProductStatusTypeEnum
     * @example 1
     */
    type?: number;
    /**
     * Признак активности статуса
     */
    is_active?: boolean;
    /**
     * Флаг по которому у товара проставляется признак активности для витрины
     */
    is_publication?: boolean;
    /**
     * Условия для перехода в статус
     */
    events?: EventConditionsEvents;
}

export type StatusSettingProperties = Prettify<
    RequireKeys<
        StatusSettingReadonlyProperties & StatusSettingFillableProperties,
        'code' | 'name' | 'color' | 'type' | 'is_active' | 'is_publication' | 'events'
    >
>;
export interface StatusSettingIncludes {
    next_statuses?: StatusSettingProperties[];
    /**
     * Список названий следующих статусов через запятую, возвращается при возврате next_statuses
     * @example "Новый, Опубликован"
     */
    next_status_ids?: string;
    previous_statuses?: StatusSettingProperties[];
    /**
     * Список названий предыдущих статусов через запятую, возвращается при возврате previous_statuses
     * @example "Новый, Опубликован"
     */
    previous_status_ids?: string;
}

export type StatusSetting = Prettify<StatusSettingProperties & StatusSettingIncludes>;

/**
 * Установка статусов из которых можно перейти в текущий
 */
export type StatusSettingResponse = CommonResponse<StatusSetting>;
export type SearchStatusSettingsRequestFilter = Record<string, any>;

/**
 * Поиск объектов типа ProductStatusSetting
 */
export interface SearchStatusSettingsRequest {
    sort?: string[];
    filter?: SearchStatusSettingsRequestFilter;
    include?: string[];
    pagination?: OffsetPaginationQuery;
}

export type ProductStatusSettings = Prettify<StatusSettingProperties & StatusSettingIncludes>;

export type StatusSettingsResponse = CommonResponse<ProductStatusSettings[]>;

/**
 * Создание объекта типа ProductStatusSetting
 */
export type CreateStatusSettingRequest = Prettify<
    RequireKeys<
        StatusSettingFillableProperties,
        'code' | 'name' | 'color' | 'type' | 'is_active' | 'is_publication'
    >
>;
export type CreateStatusSettingResponse = CommonResponse<ProductStatusSettings>;

/**
 * Изменение объекта типа ProductStatusSetting
 */
export type PatchStatusSettingRequest = Prettify<StatusSettingFillableProperties>;

export interface StatusesResponseData {
    /**
     * Cтатус продукта
     * @example 1
     */
    id: number;
    /**
     * Название статуса
     * @example "Новый"
     */
    name: string;
    /**
     * Доступность из текущего статуса
     */
    available: boolean;
}

/**
 * Получение доступных статусов из текущего
 */
export interface StatusesResponse {
    data: StatusesResponseData[];
}

/**
 * Установка статусов из которых можно перейти в текущий
 */
export interface SetPreviousStatusRequest {
    ids: number[];
}
