import { CommonResponse, CommonSearchParams, OffsetPagination, OffsetPaginationQuery } from '@api/common/types';

import { CatalogPropertyType } from './attributes';
import { Banner } from './banners';
import { Brand } from './brands';
import { Category } from './categories';
import { ProductImage } from './product-image';
import { PropertyDirectoryItem } from './properties';

interface ProductAttribute {
    name: string;
    property_id: number;
    type: string;
    value: number;
}

export interface ProductFilter {
    id: number | number[];
    name: string;
    code: string;
    barcode: string;
    vendor_code: string;
    type: number;
    is_adult: boolean;
    allow_publish: boolean | null;
    category_id: number;
    created_at_lte: Date | null;
    created_at_gte: Date | null;
    updated_at_lte: Date;
    updated_at_gte: Date;
    brand_id: number[];
    has_product_groups: boolean;
    has_no_filled_required_attributes: boolean;
    ignore_id: number[];
}

export interface ProductCreateParams {
    external_id: string | null;
    category_id?: number | string | null;
    brand_id?: number;
    name: string;
    code?: string;
    description?: string;
    type: number;
    allow_publish?: boolean;
    vendor_code: string;
    barcode?: string;
    weight?: number;
    weight_gross?: number;
    length?: number;
    width?: number;
    height?: number;
    is_adult?: boolean;
    images?: {
        id?: number;
        name?: string;
        sort: number;
        preload_file_id: number;
        url?: string;
    }[];
}

export interface Product {
    id: number;
    allow_publish: boolean;
    barcode: null;
    brand_id: null;
    category_ids: number[];
    code: string;
    created_at: string;
    description: string;
    external_id: null;
    height: number;
    is_adult: false;
    main_product?: boolean;
    /** Единица измерения из ProductUomEnum */
    uom?: number;
    /** Единица тарификации из ProductTariffingVolumeEnum */
    tariffing_volume?: number;
    /** Шаг пикера, пример 0.7 */
    order_step?: number;
    /** Минимальное количество для заказа, пример 1.4 */
    order_minvol?: number;
    /** Граница отклонения в комплектации в процентах, пример 0.35 */
    picking_weight_deviation?: number;
    length: number;
    name: string;
    type: number;
    updated_at: string;
    vendor_code: string;
    weight: number;
    weight_gross: number;
    width: number;
    main_image_url: string;
    main_image_file: string;
    images?: {
        id: number;
        is_external: boolean;
        name: string;
        sort: number;
        url: string;
    }[];
    attributes?: ProductAttribute[];
}

export interface ProductProp {
    data: Product[];
    meta: {
        pagination: OffsetPagination;
    };
}

export interface ProductAttributeValue {
    name: string;
    property_id: number;
    value: string | number;
    directory_value_id?: number;
    preload_file_id?: number;
}

export interface ProductAttributeItem extends ProductAttributeValue {
    directory?: PropertyDirectoryItem[];
    type?: string;
    url?: string;
}

export interface ProductDetailBase {
    archive?: number;
    barcode?: string;
    brand_id?: number;
    category_ids?: number[];
    code: string;
    country_id?: number;
    description?: string;
    external_id: string;
    height?: number;
    id: number;
    ingredients?: string;
    is_adult: boolean;
    is_new?: boolean;
    length?: number;
    manufacturer_id?: number;
    name: string;
    product_type_id?: number;
    type: number;
    weight_gross?: number;
    weight?: number;
    width?: number;
    /** Единица измерения из ProductUomEnum */
    uom?: number;
    /** Единица тарификации из ProductTariffingVolumeEnum */
    tariffing_volume?: number;
    /** Шаг пикера, пример 0.7 */
    order_step?: number;
    /** Минимальное количество для заказа, пример 1.4 */
    order_minvol?: number;
    /** Граница отклонения в комплектации в процентах, пример 0.35 */
    picking_weight_deviation?: number;
}

export interface ProductGroupsData {
    id: number;
    created_at: string;
    updated_at: string;
    category_ids: number[];
    main_product_id: number;
    name?: string;
    category?: Category;
    products?: Product[];
    main_product?: Product;
    main_product_image?: string;
    is_active: boolean;
}

export interface ProductGroupsBindData {
    product_errors: number[];
    product_group: ProductGroupsData;
}

export interface ProductDetail extends ProductDetailBase {
    allow_publish: boolean;
    status_id?: number;
    attributes?: ProductAttributeItem[];
    brand?: Brand;
    categories?: Category[];
    cost?: number;
    created_at?: string;
    images?: ProductImage[];
    price?: number;
    sale_active?: boolean;
    updated_at?: string;
    vendor_code: string;
    product_groups: ProductGroupsData[];
}

export interface ProductsImageMutateParams {
    id: number;
    file: FormData;
}

export interface ProductGroupTypeFilter {
    id: number[];
    name: string;
    code: string[];
}

export interface ProductGroupFilterResponse {
    id: number;
    name: string;
    display_name: string;
    code: string;
    type: CatalogPropertyType;
    is_multiple: true;
    is_filterable: true;
    is_color: true;
    directory: {
        id: number;
        name: string;
        code: string;
    }[];
}

export interface ProductGroupTypeParams {
    sort: string[];
    filter?: ProductGroupTypeFilter;
    include: string[];
    pagination: OffsetPaginationQuery;
}

export interface ProductsTypes {
    id: number;
    name: string;
}

export enum ProductGroupCode {
    promo = 'promo',
    sets = 'sets',
    brands = 'brands',
}

export interface ProductGroupType {
    id: number;
    code: ProductGroupCode;
    name: string;
}

export type ProductGroupTypeResponse = CommonResponse<ProductGroupType[]>;

export interface ProductGroupSearchFilter {
    id?: number;
    name?: string;
    code?: string;
    active?: boolean;
    is_shown?: boolean;
    type_id?: number;
    banner_id?: number;
    category_code?: string;
    'products.product_id'?: number;
}

export interface ProductGroupFilter {
    code: string;
    value: string;
    id: number;
    product_group_id: number;
}

export type ProductGroupFilterParams = CommonSearchParams<
    Partial<{
        type: CatalogPropertyType;
        is_filterable: boolean;
    }>
> & {
    category: number | null;
};

export interface ProductGroupBase {
    name: string;
    code: string;
    active: boolean;
    is_shown: boolean;
    type_id: number;
    banner_id: number | null;
    category_code: string;
}

export interface ProductGroupProduct {
    sort: number;
    product_id: number;
}

export type ProductGroupCreateParams = ProductGroupBase & {
    products: ProductGroupProduct[];
    filters: {
        code: string;
        value: string;
    }[];
};

export type ProductGroup = ProductGroupBase & {
    id: number;
    preview_photo: string | null;
    filters?: ProductGroupFilter[];
    products?: {
        sort: number;
        product_id: number;
        id: number;
        product_group_id: number;
    }[];
    type?: ProductGroupType;
    banner?: Banner;
};

export interface ProductGroupSearchParams {
    sort: (keyof ProductGroupSearchFilter)[];
    filter?: ProductGroupSearchFilter;
    include: ('filters' | 'products' | 'type' | 'banner' | 'banner.button')[];
    pagination: OffsetPaginationQuery;
}

export type ProductGroupSearchResponse = CommonResponse<ProductGroup[]>;
export type ProductGroupSearchOneResponse = CommonResponse<ProductGroup>;
export type ProductGroupCreateResponse = CommonResponse<ProductGroup[]>;

interface ProcessResult {
    processed: number[];
    errors: { id: number; message: string }[];
}

export type ProductMassPatchResponse = CommonResponse<{
    products_result: ProcessResult;
    offers_result: ProcessResult;
}>;

export interface ProductMassPatchAttribute {
    property_id: number;
    mark_to_delete?: boolean;
    value?: string | number | boolean | File;
    name?: string;
    directory_value_id?: number;
    preload_file_id?: number;
}

export type ProductMassPatchFields = Partial<Product>;

export interface ProductMassPatchRequest {
    ids: number[];
    fields: ProductMassPatchFields;
    attributes: ProductMassPatchAttribute[];
}

export interface ProductMassPatchByQueryRequest {
    filter: Record<string, any>;
    fields: ProductMassPatchFields;
    attributes: ProductMassPatchAttribute[];
}
