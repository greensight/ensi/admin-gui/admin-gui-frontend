import { CommonResponse, CommonSearchParams, Prettify, RequireKeys } from '@api/common/types';

import { OfferIncludesStocks } from './offers';

export interface StockReadonlyProperties {
    /**
     * Идентификатор стока
     * @example 202
     */
    id: number;
    /**
     * ID склада
     * @example 17
     */
    store_id: number;
    /**
     * ID товарного предложения
     * @example 15856
     */
    offer_id: number;
    /**
     * Дата создания остатка
     * @example "2021-06-11T11:27:10.000000Z"
     */
    created_at?: string;
    /**
     * Дата обновления остатка
     * @example "2021-06-11T11:27:10.000000Z"
     */
    updated_at?: string;
}

export interface StockFillableProperties {
    /**
     * Количество товара для резервирования
     * @example 1
     */
    qty?: number;
}

export type StockStock = Prettify<RequireKeys<StockReadonlyProperties & StockFillableProperties, 'qty'>>;
export type Stock = Prettify<RequireKeys<StockReadonlyProperties & StockFillableProperties, 'qty'>>;

/**
 * Запрос стока по ID
 */
export type StockResponse = CommonResponse<Stock>;

/**
 * Запрос на обновление отдельных полей стока
 */
export type PatchStockRequest = Prettify<StockFillableProperties>;
export type GetStockResponse = CommonResponse<OfferIncludesStocks>;
export type SearchStocksRequestFilter = Record<string, any>;

/**
 * Поиск остатков, удовлетворяющих фильтру
 */
export type SearchStocksRequest = CommonSearchParams<SearchStocksRequestFilter>;

export type SearchStocksResponse = CommonResponse<OfferIncludesStocks[]>;
