import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { Variant, VariantMutate, VariantsFilter } from '@api/catalog/types';
import { CommonResponse, CommonSearchParams } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';
import { FetchError } from '@api/index';

const API_URL = `catalog/variants`;
enum VariantKeys {
    VARIANTS = 'variants',
    VARIANT = 'variant',
}

export const useVariants = (data: CommonSearchParams<VariantsFilter>) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Variant[]>, FetchError>({
        queryKey: [VariantKeys.VARIANTS, data],
        queryFn: () => apiClient.post(`${API_URL}:search`, { data }),
    });
};

export const useVariant = (id: number | string, include?: string) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Variant>, FetchError>({
        queryKey: [VariantKeys.VARIANT, id, include],
        queryFn: () => apiClient.get(`${API_URL}/${id}`, include ? { params: { include } } : {}),
        enabled: !!id,
    });
};

export const useCreateVariant = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Variant>, FetchError, VariantMutate>({
        mutationFn: data => apiClient.post(API_URL, { data }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [VariantKeys.VARIANTS],
            }),
    });
};

export const useUpdateVariant = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Variant>, FetchError, VariantMutate & { id: number | string }>({
        mutationFn: data => apiClient.put(`${API_URL}/${data.id}`, { data }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [VariantKeys.VARIANTS],
            }),
    });
};

export const useDeleteVariant = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, number | string>({
        mutationFn: id => apiClient.delete(`${API_URL}/${id}`),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: [VariantKeys.VARIANTS],
            });
            queryClient.invalidateQueries({
                queryKey: [VariantKeys.VARIANT],
            });
        },
    });
};
