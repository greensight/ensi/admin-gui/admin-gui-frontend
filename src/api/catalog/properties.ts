import { QueryClient, useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, CommonSearchParams, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { FetchError } from '../index';
import {
    ChangeDirectoryData,
    DirectoryData,
    DirectoryPreloadFile,
    DirectoryPreloadImage,
    PropertiesFilters,
    Property,
    PropertyData,
    PropertyDirectoryFilters,
    PropertyDirectoryItem,
    PropertyFields,
    QUERY_KEYS,
} from './types';

const baseURL = 'catalog/properties';

const updateProperties = (queryClient: QueryClient) => {
    queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.PROPERTIES],
    });
    queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.PROPERTY],
    });
};

export const useProperties = (data: CommonSearchParams<PropertiesFilters>, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Property[]>, FetchError>({
        queryKey: [QUERY_KEYS.PROPERTIES, data],
        queryFn: () => apiClient.post(`${baseURL}:search`, { data }),
        enabled,
    });
};

export const usePropertiesMeta = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<{ data: Meta }, FetchError>({
        queryKey: ['propertiesMeta'],
        queryFn: () => apiClient.get(`${baseURL}:meta`),
        enabled,
    });
};

export const usePropertiesTypes = () => {
    const apiClient = useAuthApiClient();

    return useQuery<{ data: { id: string; name: string }[] }, FetchError>({
        queryKey: ['propertiesTypes'],
        queryFn: () => apiClient.get(`${baseURL}/properties-types`),
    });
};

export const useProperty = (id?: number | string, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Property>, FetchError>({
        queryKey: [QUERY_KEYS.PROPERTY, id],
        queryFn: () => apiClient.get(`${baseURL}/${id}`, { params: { include: 'directory' } }),
        enabled: isDetailIdValid(id) && enabled,
    });
};

export const usePropertyCreate = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Property>, FetchError, PropertyFields>({
        mutationFn: productType => apiClient.post(`${baseURL}`, { data: productType }),
        onSuccess: () => updateProperties(queryClient),
    });
};

export const usePropertyChange = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Property>, FetchError, PropertyData>({
        mutationFn: property => {
            const { id, ...propertyData } = property;
            return apiClient.patch(`${baseURL}/${id}`, { data: propertyData });
        },

        onSuccess: () => updateProperties(queryClient),
    });
};

export const usePropertyRemove = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, number>({
        mutationFn: id => apiClient.delete(`${baseURL}/${id}`),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEYS.PROPERTIES],
            });
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEYS.PROPERTY],
                refetchType: 'none',
            });
        },
    });
};

export const usePropertyDirectoryCreate = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<PropertyDirectoryItem>, FetchError, DirectoryData>({
        mutationFn: directory => {
            const { propertyId, ...directoryData } = directory;
            return apiClient.post(`${baseURL}/${propertyId}:add-directory`, { data: directoryData });
        },

        onSuccess: () => updateProperties(queryClient),
    });
};

export const usePropertyDirectoryMassCreate = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<
        CommonResponse<PropertyDirectoryItem[]>,
        FetchError,
        { propertyId: number; items: DirectoryData[] }
    >({
        mutationFn: data => {
            const { propertyId, items } = data;
            return apiClient.post(`${baseURL}/${propertyId}:mass-add-directory`, { data: { items } });
        },

        onSuccess: () => updateProperties(queryClient),
    });
};

export const usePropertyDirectories = (
    data: CommonSearchParams<PropertyDirectoryFilters>,
    enabled = true,
    customKey = ''
) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<PropertyDirectoryItem[]>, FetchError>({
        queryKey: [customKey.length ? customKey : 'properties', data],
        queryFn: () => apiClient.post(`${baseURL}/directory:search`, { data }),
        enabled,
    });
};

export const usePropertyDirectoryChange = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<PropertyDirectoryItem>, FetchError, ChangeDirectoryData>({
        mutationFn: directory => {
            const { directoryId, ...directoryData } = directory;
            return apiClient.put(`${baseURL}/directory/${directoryId}`, { data: directoryData });
        },

        onSuccess: () => updateProperties(queryClient),
    });
};

export const usePropertyDirectoryRemove = () => {
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<PropertyDirectoryItem>, FetchError, number>({
        mutationFn: id => apiClient.delete(`${baseURL}/directory/${id}`),
    });
};

// добавил propertyId, что бы можно было замапать id атрибутов к которому грузилось изображение
export const useDirectoryPreloadImage = () => {
    const apiClient = useAuthApiClient();

    return useMutation<
        CommonResponse<DirectoryPreloadImage>,
        FetchError,
        { formData: FormData; propertyId?: number | string }
    >({
        mutationFn: async data => {
            const res = await apiClient.post(`${baseURL}/directory:preload-image`, {
                data: data.formData,
                timeout: 30000,
            });
            return { ...res, ...(data?.propertyId && { propertyId: data?.propertyId }) };
        },
    });
};

export const useDirectoryPreloadFile = () => {
    const apiClient = useAuthApiClient();

    return useMutation<
        CommonResponse<DirectoryPreloadFile>,
        FetchError,
        { formData: FormData; propertyId?: number | string }
    >({
        mutationFn: async data => {
            const res = await apiClient.post(`${baseURL}/directory:preload-file`, {
                data: data.formData,
                timeout: 30000,
            });
            return { ...res, ...(data?.propertyId && { propertyId: data?.propertyId }) };
        },
    });
};
