import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, CommonSearchParams, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { FetchError } from '../index';
import { Stock } from './types';
import { GetStockResponse, PatchStockRequest } from './types/stocks';

const QueryKeys = {
    getStock: (id?: number | string) => (id ? ['get-stock', id] : ['get-stock']),
    searchStocks: (data?: any) => (data ? ['search-stocks', data] : ['search-stocks']),
    getStocksMeta: () => ['get-stocks-meta'],
};

export const useSearchStocks = (data: CommonSearchParams<any, string | string[]>, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Stock[]>, FetchError>({
        queryKey: QueryKeys.searchStocks(data),
        queryFn: () => apiClient.post(`catalog/stocks:search`, { data }),
        enabled,
    });
};

export const useStocksMeta = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Meta>, FetchError>({
        queryKey: QueryKeys.getStocksMeta(),
        queryFn: () => apiClient.get(`catalog/stocks:meta`),
        enabled,
    });
};

/** Запрос стока по ID */
export function useStock({ id }: { id: number | string }, params: Record<string, any> = {}, enabled = true) {
    const apiClient = useAuthApiClient();

    return useQuery<GetStockResponse, FetchError>({
        queryKey: QueryKeys.getStock(id),
        queryFn: () => apiClient.get(`catalog/stocks/${id}`, { params }),
        enabled,
    });
}

/** Запрос на обновление отдельных полей стока */
export const usePatchStock = () => {
    const apiClient = useAuthApiClient();
    const queryClient = useQueryClient();

    return useMutation<GetStockResponse, FetchError, { id: number | string } & PatchStockRequest>({
        mutationFn: ({ id, ...data }) => apiClient.patch(`catalog/stocks/${id}`, { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.getStock(data?.id) });
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchStocks() });
        },
    });
};
