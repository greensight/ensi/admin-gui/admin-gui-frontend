import { useMutation, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, FetchError } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

const API_URL = 'catalog/catalog-cache';

const QueryKeys = {
    searchCatalogCache: (data?: any) => (data ? ['search-catalog-cache', data] : ['search-catalog-cache']),
};

/** Запуск миграции данных для сервиса Catalog Cache */
export const useCatalogCacheMigration = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError>({
        mutationFn: () => apiClient.post(`${API_URL}/entities:migrate`),

        onSuccess: () => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchCatalogCache() });
        },
    });
};
