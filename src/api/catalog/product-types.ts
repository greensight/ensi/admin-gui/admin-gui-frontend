import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, CommonSearchParams } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { FetchError } from '../index';
import { ProductType, ProductTypesFormData, ProductTypesItemFormData } from './types';

const QUERY_KEY = 'product-types';
const API_URL = 'catalog/product-types';

export const useProductTypes = (data: CommonSearchParams<undefined>) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<ProductType[]>, FetchError>({
        queryKey: [QUERY_KEY, data],
        queryFn: () => apiClient.post(`${API_URL}:search`, { data }),
    });
};

export const useProductTypeCreate = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<ProductType>, FetchError, ProductTypesFormData>({
        mutationFn: productType => apiClient.post(API_URL, { data: productType }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY],
            }),
    });
};

export const useProductTypeUpdate = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<ProductType>, FetchError, ProductTypesItemFormData>({
        mutationFn: productType => {
            const { id, ...productTypesData } = productType;
            return apiClient.put(`${API_URL}/${id}`, { data: productTypesData });
        },

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY],
            }),
    });
};

export const useProductTypeDelete = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, number>({
        mutationFn: id => apiClient.delete(`${API_URL}/${id}`),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY],
            }),
    });
};
