import { useQuery } from '@tanstack/react-query';

import { CommonOption, CommonResponse } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { FetchError } from '../index';

const QUERY_KEY = 'product-tariffing-volumes';
const API_URL = 'catalog/products/product-tariffing-volumes';

export const useProductTariffingVolumes = () => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<CommonOption[]>, FetchError>({
        queryKey: [QUERY_KEY],
        queryFn: () => apiClient.get(API_URL),
    });
};
