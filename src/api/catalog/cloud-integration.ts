import { useMutation, useQuery } from '@tanstack/react-query';

import { FetchError } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import {
    CloudIntegrationResponse,
    CreateCloudIntegrationRequest,
    PatchCloudIntegrationRequest,
} from './types/cloud-integration';

const QueryKeys = {
    getCloudIntegration: () => ['get-cloud-integration'],
};

/** Получение объекта типа CloudIntegration */
export function useCloudIntegration(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CloudIntegrationResponse, FetchError>({
        queryKey: QueryKeys.getCloudIntegration(),
        queryFn: () => apiClient.get('catalog/cloud-integrations', {}),
        enabled,
    });
}

/** Создание объекта типа CloudIntegration */
export const useCreateCloudIntegration = () => {
    const apiClient = useAuthApiClient();
    return useMutation<CloudIntegrationResponse, FetchError, CreateCloudIntegrationRequest>({
        mutationFn: data => apiClient.post('catalog/cloud-integrations', { data }),
    });
};

/** Изменение объектов типа CloudIntegration */
export const usePatchCloudIntegration = () => {
    const apiClient = useAuthApiClient();
    return useMutation<CloudIntegrationResponse, FetchError, PatchCloudIntegrationRequest>({
        mutationFn: data => apiClient.patch('catalog/cloud-integrations', { data }),
    });
};
