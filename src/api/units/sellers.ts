import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, Meta, MetaEnumValue } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { FetchError } from '..';
import {
    EnumSearch,
    EnumValue,
    GetSellerStatusesResponse,
    SearchSellersRequest,
    SearchSellersResponse,
    SellerForCreate,
    SellerForPatch,
    SellerResponse,
} from './types';

export const QueryKeys = {
    searchSellers: (data?: Record<string, any>) => (data ? ['search-sellers', data] : ['search-sellers']),
    getSellerMeta: () => ['get-seller-meta'],
    getSellerDetail: (id?: number | string) => (id ? ['get-seller-detail', id] : ['get-seller-detail']),
    getSellerStatuses: () => ['get-seller-statuses'],
    searchSellerEnumValues: (data?: Record<string, any>) =>
        data ? ['search-seller-enum-values', data] : ['search-seller-enum-values'],
};

/** Получение объектов типа Seller */
export function useSearchSellers(data: SearchSellersRequest, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<SearchSellersResponse, FetchError>({
        queryKey: QueryKeys.searchSellers(data),
        queryFn: () => apiClient.post('units/sellers:search', { data }),
        enabled,
    });
}

/** Получение списка доступных полей */
export function useSellersMeta(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<Meta>, FetchError>({
        queryKey: QueryKeys.getSellerMeta(),
        queryFn: () => apiClient.get('units/sellers:meta', {}),
        enabled,
    });
}

/** Создание продавца */
export const useCreateSeller = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<SellerResponse, FetchError, SellerForCreate>({
        mutationFn: data => apiClient.post('units/sellers', { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchSellers() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.getSellerDetail(data.id) });
        },
    });
};

/** Получение объекта типа Seller */
export function useSellerDetail({ id }: { id: number | string }, params: Record<string, any> = {}, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<SellerResponse, FetchError>({
        queryKey: QueryKeys.getSellerDetail(id),
        queryFn: () => apiClient.get(`units/sellers/${id}`, { params }),
        enabled: isDetailIdValid(id) && enabled,
    });
}

/** Частичное изменение объекта типа Seller */
export const usePatchSeller = () => {
    const queryClient = useQueryClient();

    const apiClient = useAuthApiClient();
    return useMutation<SellerResponse, FetchError, { id: number | string } & SellerForPatch>({
        mutationFn: ({ id, ...data }) => apiClient.patch(`units/sellers/${id}`, { data }),

        onSuccess: ({ data }) => {
            queryClient.invalidateQueries({ queryKey: QueryKeys.searchSellers() });
            queryClient.invalidateQueries({ queryKey: QueryKeys.getSellerDetail(data.id) });
        },
    });
};

/** Получение объектов типа SellerStatus */
export function useSellerStatuses(enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<GetSellerStatusesResponse, FetchError>({
        queryKey: QueryKeys.getSellerStatuses(),
        queryFn: () => apiClient.get('units/sellers/seller-statuses', {}),
        enabled,
    });
}

/** Поиск продавцов для справочника */
export function useSearchSellerEnumValues(data: EnumSearch, enabled = true) {
    const apiClient = useAuthApiClient();
    return useQuery<CommonResponse<EnumValue[]>, FetchError>({
        queryKey: QueryKeys.searchSellerEnumValues(data),
        queryFn: () => apiClient.post('units/seller-enum-values:search', { data }),
        enabled,
    });
}

export const useSellerAutocomplete = () => {
    const apiClient = useAuthApiClient();

    const asyncSearchFn = async (query: string) => {
        const res = (await apiClient.post(`units/seller-enum-values:search`, {
            data: {
                filter: {
                    query,
                },
            },
        })) as CommonResponse<MetaEnumValue[]>;
        console.log('res asyncSearchFn', res);

        return {
            options: res.data.map(e => ({
                label: e.title,
                value: e.id,
            })),
            hasMore: false,
        };
    };

    const asyncOptionsByValuesFn = async (vals: number[] | { value: number }[], abortController: AbortController) => {
        const actualValues = vals.map(v => {
            if (typeof v === 'object' && 'value' in v) return Number(v.value);
            return Number(v);
        });

        const res = (await apiClient.post(`units/seller-enum-values:search`, {
            data: {
                filter: { id: actualValues },
            },
            abortController,
        })) as CommonResponse<MetaEnumValue[]>;
        console.log('res asyncOptionsByValuesFn', res);

        return res.data.map(e => ({
            label: e.title,
            value: e.id,
        }));
    };

    return {
        asyncOptionsByValuesFn,
        asyncSearchFn,
    };
};
