import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { FetchError } from '../index';
import { AdminRoleAddFields, AdminRoleDeleteFields, AdminUser, AdminUserSearch } from './types';

const ADMIN_USERS_BASE_URL = 'units/admin-users';
const ADMIN_USER_BASE_URL = 'units/admin-user';
const QUERY_KEY_ADMINS = 'admin-users';
const QUERY_KEY_ADMIN = 'admin-user';

export const useAdminUsers = (data: AdminUserSearch, enabled: boolean = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<AdminUser[]>, FetchError>({
        queryKey: [QUERY_KEY_ADMINS, data],
        queryFn: () => apiClient.post(`${ADMIN_USERS_BASE_URL}:search`, { data }),
        enabled,
    });
};

export const useAdminUsersMeta = (enabled: boolean = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<{ data: Meta }, FetchError>({
        enabled,
        queryKey: [`${QUERY_KEY_ADMINS}-meta`],
        queryFn: () => apiClient.get(`${ADMIN_USERS_BASE_URL}:meta`),
    });
};

export const useAdminUser = (id?: string, include?: string | string[], enabled: boolean = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<AdminUser>, FetchError>({
        queryKey: [QUERY_KEY_ADMIN, id],
        queryFn: () => apiClient.get(`${ADMIN_USERS_BASE_URL}/${id}${include ? `?include=${include}` : ''}`),
        enabled: isDetailIdValid(id) && enabled,
    });
};

export const useCreateAdminUser = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<AdminUser>, FetchError, Partial<AdminUser>>({
        mutationFn: data => apiClient.post(ADMIN_USERS_BASE_URL, { data }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY_ADMINS],
            }),
    });
};

export const useUpdateAdminUser = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<AdminUser>, FetchError, Partial<AdminUser>>({
        mutationFn: ({ id, ...data }) => apiClient.patch(`${ADMIN_USERS_BASE_URL}/${id}`, { data }),

        onSuccess: (data, variables) => {
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY_ADMINS],
            });
            if (variables?.disableInvalidate === false)
                queryClient.invalidateQueries({
                    queryKey: [QUERY_KEY_ADMIN],
                });
        },
    });
};

export const useAddAdminUserRole = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<AdminUser>, FetchError, AdminRoleAddFields>({
        mutationFn: ({ id, ...data }) => apiClient.post(`${ADMIN_USERS_BASE_URL}/${id}:add-roles`, { data }),

        onSuccess: (data, variables) => {
            if (variables?.disableInvalidate === false)
                queryClient.invalidateQueries({
                    queryKey: [QUERY_KEY_ADMIN],
                });
        },
    });
};

export const useDeleteAdminUserRole = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<AdminUser>, FetchError, AdminRoleDeleteFields>({
        mutationFn: ({ id, ...data }) => apiClient.post(`${ADMIN_USERS_BASE_URL}/${id}:delete-role`, { data }),

        onSuccess: (data, variables) => {
            if (variables?.disableInvalidate === false)
                queryClient.invalidateQueries({
                    queryKey: [QUERY_KEY_ADMIN],
                });
        },
    });
};

export const useRefreshPasswordTokenAdminUser = () => {
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, number>({
        mutationFn: id => apiClient.post(`${ADMIN_USERS_BASE_URL}/${id}:refresh-password-token`),
    });
};

export const useDeleteAdminUser = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, number>({
        mutationFn: id => apiClient.delete(`${ADMIN_USERS_BASE_URL}/${id}`),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY_ADMINS],
            }),
    });
};

export const useGetUserById = () => {
    const apiClient = useAuthApiClient();

    const getUserById = async (id: number) => {
        try {
            const res = await apiClient.post(`${ADMIN_USER_BASE_URL}-enum-values:search`, {
                data: { filter: { query: '', id: [id] } },
            });
            return res.data;
        } catch {
            return [];
        }
    };

    return { getUserById };
};

export const useGetAdminUsersSearchFn = () => {
    const apiClient = useAuthApiClient();

    const adminUsersSearchFn = async (query: string) => {
        const res = await apiClient.post(`${ADMIN_USER_BASE_URL}-enum-values:search`, {
            data: { filter: { query } },
        });
        return {
            options: res.data.map((i: { title: string; id: string }) => ({
                label: i.title,
                value: i.id,
            })),
            hasMore: false,
        };
    };

    return { adminUsersSearchFn };
};

export const useGetAdminUsersOptionsByValuesFn = () => {
    const apiClient = useAuthApiClient();

    const adminUsersOptionsByValuesFn = async (vals: string[]) => {
        const res = await apiClient.post(`${ADMIN_USER_BASE_URL}-enum-values:search`, {
            data: { filter: { id: vals } },
        });
        return res.data.map((i: { title: string; id: string }) => ({
            label: i.title,
            value: i.id,
        }));
    };

    return { adminUsersOptionsByValuesFn };
};
