import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, CommonSearchParams, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { isDetailIdValid } from '@scripts/helpers';

import { FetchError } from '../index';
import { CreateRoleData, RightAccessData, Role } from './types';

const BASE_URL = 'units/admin-user-roles';
const QUERY_KEY = 'admin-user-roles';

const RIGHT_ACCESS_URL = 'units/admin-users/right-access';
const RIGHT_ACCESS_KEY = 'admin-roles';

export const useAdminRoles = (
    data: CommonSearchParams<{ id?: number; title?: string; active?: boolean }, string>,
    enabled = true
) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Role[]>, FetchError>({
        queryKey: [QUERY_KEY, data],
        queryFn: () => apiClient.post(`${BASE_URL}:search`, { data }),
        enabled,
    });
};

export const useAdminRolesMeta = (enabled: boolean = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<{ data: Meta }, FetchError>({
        enabled,
        queryKey: [`${QUERY_KEY}-meta`],
        queryFn: () => apiClient.get(`${BASE_URL}:meta`),
    });
};

export const useRolesRightAccess = (enabled: boolean = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<{ data: RightAccessData[] }, FetchError>({
        enabled,
        queryKey: [`${RIGHT_ACCESS_KEY}-meta`],
        queryFn: () => apiClient.get(`${RIGHT_ACCESS_URL}`),
    });
};

export const useAdminRole = (id?: string | number, enabled: boolean = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Role>, FetchError>({
        enabled: isDetailIdValid(id) && enabled,
        queryKey: [`${QUERY_KEY}-${id}`],
        queryFn: () => apiClient.get(`${BASE_URL}/${id}`),
    });
};

export const useCreateAdminRole = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<CreateRoleData>, FetchError, CreateRoleData>({
        mutationFn: data => apiClient.post(`${BASE_URL}`, { data }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY],
            }),
    });
};

export const useUpdateAdminRole = (id?: string | number) => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<CreateRoleData>, FetchError, CreateRoleData>({
        mutationFn: data => apiClient.patch(`${BASE_URL}/${id}`, { data }),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY],
            });
            queryClient.invalidateQueries({
                queryKey: [`${QUERY_KEY}-${id}`],
            });
        },
    });
};

export const useDeleteAdminRole = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<CreateRoleData>, FetchError, number | string | undefined>({
        mutationFn: id => apiClient.delete(`${BASE_URL}/${id}`),

        onSuccess: () => {
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY],
            });
        },
    });
};
