import { CommonResponse, CommonSearchParams, PartialNullable, Prettify } from '@api/common/types';

import { StoreContactFillableProperties, StoreContactReadonlyProperties } from './storeContacts';
import { StorePickupTimeFillableProperties, StorePickupTimeReadonlyProperties } from './storePickupTimes';
import { StoreWorkingFillableProperties, StoreWorkingReadonlyProperties } from './storeWorkings';

export interface StoreReadonlyProperties {
    /**
     * Идентификатор склада
     * @example 1
     */
    id: number;
    /**
     * Время создания склада
     * @example "2021-01-15T14:55:35.000000Z"
     */
    created_at: string;
    /**
     * Время обновления склада
     * @example "2021-01-15T14:55:35.000000Z"
     */
    updated_at: string;
}

/**
 * Адрес */

export interface StoreAddress {
    /**
     * Адрес одной строкой
     * @example "107140, г Москва, Красносельский р-н, ул Красносельская Верхн., д 3А"
     */
    address_string: string;
    /**
     * Код страны
     * @example "RU"
     */
    country_code?: string;
    /**
     * Почтовый индекс
     * @example "107140"
     */
    post_index?: string;
    /**
     * Регион
     * @example "г Москва"
     */
    region?: string;
    /**
     * ФИАС ID региона
     * @example "0c5b2444-70a0-4932-980c-b4dc0d3f02b5"
     */
    region_guid?: string;
    /**
     * Район в регионе
     * @example "Центральный"
     */
    area?: string;
    /**
     * ФИАС ID района в регионе
     */
    area_guid?: string;
    /**
     * Город/населенный пункт
     * @example "г Москва"
     */
    city?: string;
    /**
     * ФИАС ID города/населенного пункта
     * @example "0c5b2444-70a0-4932-980c-b4dc0d3f02b5"
     */
    city_guid?: string;
    /**
     * Улица
     * @example "ул Красносельская Верхн."
     */
    street?: string;
    /**
     * Дом
     * @example "д 3А"
     */
    house?: string;
    /**
     * Строение/корпус
     */
    block?: string;
    /**
     * Квартира/офис
     */
    flat?: string;
    /**
     * Подъезд
     */
    porch?: string;
    /**
     * Этаж
     */
    floor?: string;
    /**
     * Домофон
     */
    intercom?: string;
    /**
     * Комментарий к адресу
     */
    comment?: string;
    /**
     * координаты: широта
     * @example "55.785513"
     */
    geo_lat?: string;
    /**
     * координаты: долгота
     * @example "37.665408"
     */
    geo_lon?: string;
}

export interface StoreFillableProperties {
    /**
     * ID продавца
     * @example 2
     */
    seller_id: number | null;
    /**
     * ID склада в системе продавца
     * @example "015"
     */
    xml_id: string | null;
    /**
     * Флаг активности склада
     * @example true
     */
    active: boolean;
    /**
     * Название
     * @example "Сокольники"
     */
    name: string;
    address: StoreAddress;
    /**
     * Часовой пояс
     * @example "Europe/Moscow"
     */
    timezone: string;
}

export type StoreContact = StoreContactReadonlyProperties & StoreContactFillableProperties;
export type StoreWorking = StoreWorkingReadonlyProperties & StoreWorkingFillableProperties;
export type StorePickupTime = StorePickupTimeReadonlyProperties & StorePickupTimeFillableProperties;

export interface StoreIncludes {
    workings: StoreWorking[];
    contacts: StoreContact[];
    contact: StoreContact;
    pickup_times: StorePickupTime[];
}

export enum StoresSearchRequestSort {
    ID = 'id',
    NAME = 'name',
    ACTIVE = 'active',
    SELLER_ID = 'seller_id',
    XML_ID = 'xml_id',
    TIMEZONE = 'timezone',
    CREATED_AT = 'created_at',
    UPDATED_AT = 'updated_at',
}

export interface StoresSearchRequestFilter {
    /**
     * ID склада
     * @example 1
     */
    id: number;
    /**
     * ID продавца
     * @example 1
     */
    seller_id: number;
    /**
     * ID склада в системе продавца
     * @example "015"
     */
    xml_id: string;
    /**
     * Флаг активности склада
     * @example true
     */
    active: boolean;
    /**
     * Название склада
     * @example "Третий склад"
     */
    name: string;
    /**
     * Часовой пояс
     * @example "Europe/Moscow"
     */
    timezone: string;
    /**
     * Адрес склада
     * @example "124482, г Москва, г Зеленоград, р-н Савелки, к 305"
     */
    address_string: string;
    /**
     * ФИО контактного лица
     * @example "Данилов Яков Евгеньевич"
     */
    contact_name: string;
    /**
     * Телефон контактного лица
     * @example "+79996663322"
     */
    contact_phone: string;
    /**
     * Дата создания меньше или равна
     * @example "2021-07-09T04:25:11.000000Z"
     */
    created_at_from: string;
    /**
     * Дата создания больше или равна
     * @example "2021-07-03T11:57:28.000000Z"
     */
    created_at_to: string;
    /**
     * Дата обновления меньше или равна
     * @example "2021-07-09T04:25:11.000000Z"
     */
    updated_at_from: string;
    /**
     * Дата обновления больше или равна
     * @example "2021-07-03T11:57:28.000000Z"
     */
    updated_at_to: string;
}

export enum StoresSearchRequestInclude {
    WORKINGS = 'workings',
    CONTACTS = 'contacts',
    CONTACT = 'contact',
    PICKUP_TIMES = 'pickup_times',
}

export type Store = StoreReadonlyProperties & StoreFillableProperties & StoreIncludes;

/**
 * Получение объектов типа Store
 */
export type StoresSearchRequest = CommonSearchParams<Partial<Store>, string | string[]>;

export type StoresSearchResponse = CommonResponse<Store[]>;

export type StoreRequiredProperties = Record<string, any>;

/**
 * Создание склада продавца */
export type StoreForCreate = Prettify<StoreFillableProperties> & StoreRequiredProperties;
export type StoreResponse = CommonResponse<Store>;

/**
 * Частичное изменение объекта типа Store */
export type StoreForPatch = Prettify<PartialNullable<StoreFillableProperties>>;
/**
 * Замена объекта типа Store */

export type StoreForReplace = Prettify<StoreFillableProperties & StoreRequiredProperties>;
