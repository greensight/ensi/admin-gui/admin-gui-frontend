import { CommonResponse, Prettify } from '@api/common/types';

export interface StorePickupTimeFillableProperties {
    /**
     * ID склада
     * @example 17
     */
    store_id: number;
    /**
     * День недели (1-7)
     * @example 3
     */
    day: number;
    /**
     * Код времени отгрузки у службы доставки
     * @example "12-14"
     */
    pickup_time_code: string;
    /**
     * Время начала отгрузки
     * @example "14:00:00"
     */
    pickup_time_start: string;
    /**
     * Время окончания отгрузки
     * @example "18:00:00"
     */
    pickup_time_end: string;
    /**
     * Служба доставки (если указана, то данная информация переопределяет данные дня недели без службы доставки)
     * @example 17
     */
    delivery_service?: number | null;
}

export interface StorePickupTimeReadonlyProperties {
    /**
     * Идентификатор записи о времени отгрузки
     * @example 1
     */
    id: number;
    /**
     * Время создания записи о времени отгрузки
     * @example "2021-01-15T14:55:35.000000Z"
     */
    created_at: string;
    /**
     * Время обновления записи о времени отгрузки
     * @example "2021-01-15T14:55:35.000000Z"
     */
    updated_at: string;
}

/**
 * Создание времени отгрузки со склада */
export type StorePickupTimeForCreate = Prettify<StorePickupTimeFillableProperties & StorePickupTimeFillableProperties>;

export type StorePickupTime = Prettify<StorePickupTimeReadonlyProperties & StorePickupTimeReadonlyProperties>;

export type StorePickupTimeResponse = CommonResponse<StorePickupTime>;

/**
 * Частичное изменение объекта типа StorePickupTime
 */
export type StorePickupTimeForPatch = Partial<Prettify<StorePickupTimeFillableProperties>>;

/**
 * Замена объекта типа StorePickupTime
 */
export type StorePickupTimeForReplace = Prettify<StorePickupTimeFillableProperties & StorePickupTimeFillableProperties>;
