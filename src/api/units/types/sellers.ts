import { CommonResponse, CommonSearchParams } from '@api/common/types';

import { Address } from './address';

type Prettify<T> = {
    [K in keyof T]: T[K];
};

/**
 * Получение объектов типа Seller */

export type SearchSellersRequest = CommonSearchParams<Record<string, any>>;

export interface SellerReadonlyProperties {
    /**
     * Идентификатор продавца
     * @example 1
     */
    id: number;
    /**
     * Время изменения статуса продавца
     * @example "2021-01-15T14:55:35.000000Z"
     */
    status_at: string;
    /**
     * Время создания продавца
     * @example "2021-01-15T14:55:35.000000Z"
     */
    created_at: string;
    /**
     * Время обновления продавца
     * @example "2021-01-15T14:55:35.000000Z"
     */
    updated_at: string;
}

export interface SellerFillableProperties {
    /**
     * ID менеджера
     * @example 1
     */
    manager_id?: number;
    /**
     * Юридическое наименование организации
     * @example "Сокольники"
     */
    legal_name: string;
    legal_address: Address | null;
    fact_address: Address | null;
    /**
     * ИНН
     */
    inn?: string;
    /**
     * КПП
     */
    kpp?: string;
    /**
     * Номер банковского счета
     */
    payment_account?: string;
    /**
     * Номер корреспондентского счета банка
     */
    correspondent_account?: string;
    /**
     * Наименование банка
     */
    bank?: string;
    bank_address: Address | null;
    /**
     * БИК банка
     */
    bank_bik?: string;
    /**
     * Сайт компании
     */
    site?: string;
    /**
     * Бренды и товары, которыми торгует продавец
     */
    info?: string;

    /**
     * Статус продавца
     */
    status: number;
}

export type SellerData = Prettify<SellerReadonlyProperties & SellerFillableProperties>;
export type SearchSellersResponse = CommonResponse<SellerData[]>;

/**
 * Создание продавца */

export type SellerForCreate = Prettify<SellerFillableProperties>;
export type Seller = Prettify<SellerReadonlyProperties & SellerFillableProperties>;
export type SellerResponse = CommonResponse<Seller>;

/**
 * Частичное изменение объекта типа Seller */

export type SellerForPatch = Prettify<Partial<SellerReadonlyProperties & SellerFillableProperties>>;

export interface SellerStatusData {
    /**
     * ID статуса из UnitsSellerStatusEnum
     */
    id: number;
    /**
     * Название статуса
     */
    name: string;
}

/**
 * Получение объектов типа SellerStatus */
export interface GetSellerStatusesResponse {
    data: SellerStatusData[];
}

export interface EnumValue {
    /**
     * Числовой идентификатор значения
     */
    id: number;
    /**
     * Человеко-понятное значение
     */
    title: string;
}

export type EnumSearch = CommonSearchParams<{
    /**
     * Текст, вводимый пользователем для фильтрации списка
     */
    query: string;
    /**
     * Отфильтровать значения по идентификатору
     * @example 1,uuid
     */
    id: (number | string)[];
}>;
