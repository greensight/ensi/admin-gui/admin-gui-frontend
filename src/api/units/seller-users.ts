import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';

import { CommonResponse, Meta } from '@api/common/types';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { FetchError } from '../index';
import {
    Role,
    SellerRoleAddFields,
    SellerRoleDeleteFields,
    SellerUser,
    SellerUserMutate,
    SellerUserMutateWithId,
    SellerUserSearch,
} from './types';

const SELLER_USERS_BASE_URL = 'units/seller-users';
const QUERY_KEY_SELLERS = 'sellers-users';
const QUERY_KEY_SELLER = 'seller-user';

export const useSellerUsersSearch = (data: SellerUserSearch = {}, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<SellerUser[]>, FetchError>({
        queryKey: [QUERY_KEY_SELLERS, data],
        queryFn: () => apiClient.post(`${SELLER_USERS_BASE_URL}:search`, { data }),
        enabled,
    });
};

export const useSellerUsersMeta = (enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<{ data: Meta }, FetchError>({
        queryKey: [QUERY_KEY_SELLERS],
        queryFn: () => apiClient.get(`${SELLER_USERS_BASE_URL}:meta`),
        enabled,
    });
};

export const useSellerUser = (id?: number | string, enabled = true) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<SellerUser>, FetchError>({
        queryKey: [QUERY_KEY_SELLER, id],
        queryFn: () => apiClient.get(`${SELLER_USERS_BASE_URL}/${id}`),
        enabled: !!id && enabled,
    });
};

export const useCreateSellerUser = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<SellerUserMutateWithId>, FetchError, SellerUserMutate>({
        mutationFn: data => apiClient.post(SELLER_USERS_BASE_URL, { data }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY_SELLERS],
            }),
    });
};

export const useUpdateSellerUser = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<SellerUser>, FetchError, SellerUserMutateWithId>({
        mutationFn: ({ id, ...data }) => apiClient.patch(`${SELLER_USERS_BASE_URL}/${id}`, { data }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY_SELLER],
            }),
    });
};

export const useDeleteSellerUser = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<null>, FetchError, number>({
        mutationFn: id => apiClient.delete(`${SELLER_USERS_BASE_URL}/${id}`),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY_SELLERS],
            }),
    });
};

export const useSellersRoles = (isOpen: boolean) => {
    const apiClient = useAuthApiClient();

    return useQuery<CommonResponse<Role[]>, FetchError>({
        queryKey: ['sellers-roles'],
        queryFn: () => apiClient.get(`seller-user-roles:search`),
        enabled: isOpen,
    });
};

export const useAddRoleSellerUser = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Role>, FetchError, SellerRoleAddFields>({
        mutationFn: ({ id, ...data }) => apiClient.post(`${SELLER_USERS_BASE_URL}/${id}:add-roles`, { data }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY_SELLER],
            }),
    });
};

export const useDeleteAdminRoleSellerUser = () => {
    const queryClient = useQueryClient();
    const apiClient = useAuthApiClient();

    return useMutation<CommonResponse<Role>, FetchError, SellerRoleDeleteFields>({
        mutationFn: ({ id, ...data }) => apiClient.post(`${SELLER_USERS_BASE_URL}/${id}:delete-role`, { data }),

        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [QUERY_KEY_SELLER],
            }),
    });
};
