import { CSSObject } from '@emotion/react';
import { Popup, PopupContent, PopupHeader } from '@ensi-platform/core-components';
import Link from 'next/link';
import { FC, useCallback, useState } from 'react';
import Media from 'react-media';

import { CurrentUser } from '@api/auth';

import { useCommon } from '@context/common';

import Breadcrumbs from '@controls/Breadcrumbs';
import Tooltip, { ContentBtn } from '@controls/Tooltip';

import { FlatMenuItemExtended } from '@scripts/data/menu';
import { Button, scale, typography, useTheme } from '@scripts/gds';

import EnsiLogo from '@icons/logo.svg';
import BellIcon from '@icons/small/bell.svg';
import LogoutIcon from '@icons/small/logOut.svg';
import BurgerIcon from '@icons/small/menu.svg';
import UserIcon from '@icons/small/user.svg';

export interface HeaderProps {
    /** on logout btn click method */
    onLogout: () => void;
    /** breadcrumb */
    breadcrumb?: FlatMenuItemExtended;
    /** On menu click */
    onMenuClick?: () => void;
    user?: CurrentUser | undefined;
}

const btnStyles: CSSObject = {
    padding: `${scale(1)}px !important`,
};

const Header: FC<HeaderProps> = ({ onLogout, breadcrumb, onMenuClick, user }) => {
    const { colors, shadows, layout } = useTheme();
    const [isUserOpen, setIsUserOpen] = useState(false);
    const { isSidebarOpen } = useCommon();
    const [isNotificationOpen, setIsNotificationOpen] = useState(false);

    const getUserContent = useCallback(
        () => (
            <>
                {user && (
                    <p css={{ padding: `${scale(1, true)}px ${scale(1)}px` }}>
                        <UserIcon css={{ marginRight: scale(1, true), marginBottom: scale(-1, true, 2) }} />
                        {user?.full_name}
                    </p>
                )}
                <ContentBtn Icon={LogoutIcon} onClick={onLogout}>
                    Выйти
                </ContentBtn>
            </>
        ),
        [onLogout, user]
    );

    const getNotificationsContent = useCallback(
        () => <p css={{ padding: `${scale(1, true)}px ${scale(1)}px` }}>Уведомлений пока нет</p>,
        []
    );

    return layout ? (
        <Media query={{ minWidth: layout.breakpoints.md }}>
            {isDesktop => (
                <>
                    <header
                        css={{
                            position: 'relative',
                            zIndex: 2,
                            width: '100%',
                            height: scale(7),
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            paddingLeft: scale(3),
                            paddingRight: scale(3),
                            borderBottom: `1px solid ${colors?.grey300}`,
                            backgroundColor: colors?.white,
                            boxShadow: shadows?.big,
                            ...typography('bodySm'),
                        }}
                    >
                        <div css={{ display: 'flex', alignItems: 'center' }}>
                            <Link legacyBehavior href="/" passHref>
                                <a css={{ marginRight: scale(1) }}>
                                    <EnsiLogo />
                                </a>
                            </Link>
                            {breadcrumb ? (
                                <Breadcrumbs>
                                    {breadcrumb.parent.map(b => (
                                        <Breadcrumbs.Item link={b.link} key={b.text}>
                                            {b.text}
                                        </Breadcrumbs.Item>
                                    ))}
                                    <Breadcrumbs.Item link={breadcrumb.link}>{breadcrumb.text}</Breadcrumbs.Item>
                                </Breadcrumbs>
                            ) : null}
                        </div>
                        {isDesktop && (
                            <div css={{ display: 'flex' }}>
                                <Tooltip
                                    placement="bottom-end"
                                    theme="light"
                                    content={getUserContent()}
                                    minWidth={scale(25)}
                                    arrow
                                >
                                    <Button type="button" theme="ghost" hidden Icon={UserIcon}>
                                        Пользователь
                                    </Button>
                                </Tooltip>

                                <Tooltip
                                    placement="bottom-end"
                                    theme="light"
                                    content={getNotificationsContent()}
                                    minWidth={scale(25)}
                                    arrow
                                >
                                    <Button type="button" theme="ghost" hidden Icon={BellIcon}>
                                        Уведомления
                                    </Button>
                                </Tooltip>
                            </div>
                        )}
                    </header>
                    {!isDesktop && (
                        <>
                            <div
                                css={{
                                    position: 'fixed',
                                    zIndex: 2,
                                    bottom: 0,
                                    left: 0,
                                    right: 0,
                                    display: 'flex',
                                    justifyContent: 'space-between',
                                    background: colors?.white,
                                    padding: `${scale(1)}px ${scale(3)}px`,
                                    boxShadow: shadows?.small,
                                    ...(isSidebarOpen && {
                                        display: 'none',
                                    }),
                                }}
                            >
                                <Button
                                    type="button"
                                    size="sm"
                                    theme="ghost"
                                    hidden
                                    Icon={BurgerIcon}
                                    css={btnStyles}
                                    onClick={onMenuClick}
                                >
                                    Меню
                                </Button>

                                <Button
                                    type="button"
                                    size="sm"
                                    theme="ghost"
                                    hidden
                                    Icon={UserIcon}
                                    css={btnStyles}
                                    onClick={() => setIsUserOpen(true)}
                                >
                                    Пользоваетль
                                </Button>

                                <Button
                                    type="button"
                                    size="sm"
                                    theme="ghost"
                                    hidden
                                    Icon={BellIcon}
                                    css={btnStyles}
                                    onClick={() => setIsNotificationOpen(true)}
                                >
                                    Уведомления
                                </Button>
                            </div>

                            <Popup open={isUserOpen} onClose={() => setIsUserOpen(false)} size="fullscreen">
                                <PopupHeader title="Администратор" />
                                <PopupContent>{getUserContent()}</PopupContent>
                            </Popup>

                            <Popup
                                open={isNotificationOpen}
                                onClose={() => setIsNotificationOpen(false)}
                                size="fullscreen"
                            >
                                <PopupHeader title="Уведомления" />
                                <PopupContent>{getNotificationsContent()}</PopupContent>
                            </Popup>
                        </>
                    )}
                </>
            )}
        </Media>
    ) : null;
};

export default Header;
