import { Form, FormFieldWrapper, FormReset, IFieldWrapperProps, useWatch } from '@ensi-platform/core-components';
import { FC } from 'react';

import { Button, scale, useTheme } from '@scripts/gds';
import { useFieldCSS } from '@scripts/hooks';

import CrossIcon from '@icons/small/closed.svg';
import SearchIcon from '@icons/small/search.svg';

export type SearchProps = IFieldWrapperProps<any> & {
    onSearch: (search: string) => void;
    placeholder?: string;
    className?: string;
};

const Input: FC<any> = ({ field, ...props }) => {
    delete props.meta;
    delete props.helpers;
    const { basicFieldCSS } = useFieldCSS();

    return <input css={{ ...basicFieldCSS, paddingRight: scale(8), width: scale(36) }} {...field} {...props} />;
};

const SearchInner = (props: IFieldWrapperProps<any>) => {
    const { colors } = useTheme();
    const values = useWatch();

    return (
        <>
            <FormFieldWrapper name="search" {...props}>
                <Input />
            </FormFieldWrapper>
            <div
                css={{
                    position: 'absolute',
                    top: 4,
                    right: 2,
                    display: 'flex',
                    alignItems: 'center',
                }}
            >
                {values?.search?.length > 0 && (
                    <>
                        <FormReset
                            type="button"
                            theme="ghost"
                            size="sm"
                            hidden
                            Icon={CrossIcon}
                            css={{ ':hover': { background: 'transparent !inherit' } }}
                        >
                            Очистить
                        </FormReset>
                        <span
                            css={{
                                height: scale(2),
                                width: 1,
                                background: colors?.grey400,
                            }}
                        />
                    </>
                )}
                <Button
                    type="submit"
                    theme="ghost"
                    size="sm"
                    hidden
                    Icon={SearchIcon}
                    css={{ ':hover': { background: 'transparent !inherit' } }}
                >
                    Поиск
                </Button>
            </div>
        </>
    );
};

export const Search = ({ onSearch, ...props }: SearchProps) => (
    <Form
        initialValues={{ search: '' }}
        onSubmit={vals => onSearch(vals.search)}
        css={{ position: 'relative', marginRight: scale(1) }}
    >
        <SearchInner {...props} />
    </Form>
);
