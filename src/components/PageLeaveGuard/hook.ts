import { useRouter } from 'next/router';
import { useCallback, useEffect, useRef } from 'react';

const errorMessage = 'Please ignore this error.';
const confirmationMessage = 'Вы не сохранили изменения';

const throwFakeErrorToFoolNextRouter = () => {
    // Throwing an actual error class trips the Next.JS 500 Page, this string literal does not.
    // eslint-disable-next-line no-throw-literal
    throw errorMessage;
};

const rejectionHandler = (event: PromiseRejectionEvent) => {
    if (event.reason === errorMessage) {
        event.preventDefault();
    }
};
interface Props {
    shouldStopNavigation: boolean;
    onNavigate: () => void;
}

// Remove ?tab=<any_string> pattern
// Remove &tab=<any_string> pattern
const sanitizeUrlFromTab = (url: string) => url.replace(/\?tab=[a-zA-Z0-9]+/, '').replace(/&tab=[a-zA-Z0-9]+/g, '');

const useNavigationObserver = ({ shouldStopNavigation, onNavigate }: Props) => {
    const router = useRouter();
    const currentPath = sanitizeUrlFromTab(router.asPath);
    const nextPath = useRef('');
    const navigationConfirmed = useRef(false);

    const killRouterEvent = useCallback(() => {
        router.events.emit('routeChangeError', '', '', { shallow: false });
        throwFakeErrorToFoolNextRouter();
    }, [router]);

    const shouldStopNavigationRef = useRef(false);
    shouldStopNavigationRef.current = shouldStopNavigation;

    const beforeUnloadHandler = useCallback((e: BeforeUnloadEvent) => {
        if (!shouldStopNavigationRef.current) return;

        (e || window.event).returnValue = confirmationMessage;
        return confirmationMessage; // Gecko + Webkit, Safari, Chrome etc.
    }, []);

    useEffect(() => {
        if (shouldStopNavigation) {
            window.addEventListener('beforeunload', beforeUnloadHandler);
        } else {
            window.removeEventListener('beforeunload', beforeUnloadHandler);
        }

        return () => window.removeEventListener('beforeunload', beforeUnloadHandler);
    }, [beforeUnloadHandler, shouldStopNavigation]);

    useEffect(() => {
        navigationConfirmed.current = false;

        const onRouteChange = (url: string) => {
            const urlClean = sanitizeUrlFromTab(url);
            const currentPathClean = sanitizeUrlFromTab(currentPath);

            if (shouldStopNavigation && urlClean !== currentPathClean && !navigationConfirmed.current) {
                // removing the basePath from the url as it will be added by the router
                nextPath.current = sanitizeUrlFromTab(url.replace(router.basePath, ''));
                onNavigate();
                killRouterEvent();
            }
        };

        router.events.on('routeChangeStart', onRouteChange);
        window.addEventListener('unhandledrejection', rejectionHandler);

        return () => {
            router.events.off('routeChangeStart', onRouteChange);
            window.removeEventListener('unhandledrejection', rejectionHandler);
        };
    }, [currentPath, killRouterEvent, onNavigate, router.basePath, router.events, shouldStopNavigation]);

    const confirmNavigation = () => {
        navigationConfirmed.current = true;
        router.push(nextPath.current);
    };

    const cancelNavigation = () => {
        navigationConfirmed.current = false;
    };

    return {
        confirmNavigation,
        cancelNavigation,
    };
};

export { useNavigationObserver };
