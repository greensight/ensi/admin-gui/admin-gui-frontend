import { ActionPopup, ActionPopupContent, ActionState, useForm, useFormContext } from '@ensi-platform/core-components';
import type { ParsedUrlQuery } from 'querystring';
import { useRef } from 'react';
import type { FieldPath, FieldValues, UseFormReturn } from 'react-hook-form';

import { useModalsContext } from '@context/modal';

import { ActionType } from '@scripts/enums';
import { usePopupState } from '@scripts/hooks';

import { useNavigationObserver } from './hook';

type ValueOrPromiseType<T> = T | Promise<T>;
type UrlOrStringType = { pathname: string; query: ParsedUrlQuery } | string;
type SubmitUrlType = void | UrlOrStringType | boolean;

interface IPageLeaveGuardProps<T extends FieldValues> {
    isTrackParameters?: boolean;
    onSubmit?: (values: T, formProps: UseFormReturn<T, unknown>) => ValueOrPromiseType<SubmitUrlType>;
}

const useIsomorphicForm = <T extends FieldValues>() => {
    const formContext = useFormContext<T>();
    const form = useForm<T>()!;

    if (!formContext) {
        throw new Error('PageLeaveGuard can only exist as a descendant of react-hook-form');
    }

    return {
        isSubmitting: formContext.formState.isSubmitting,
        dirty: formContext.formState.isDirty && Object.keys(formContext.formState.dirtyFields).length > 0,
        resetForm: formContext.reset,
        submitForm: () =>
            new Promise(resolve => {
                if (form) form.onSubmit(formContext.getValues(), formContext);
                resolve(formContext.getValues());
            }),
        validateForm: async () => formContext.trigger().then(() => formContext?.formState.errors),
        getValues: formContext.getValues,
        markErrorFields: () =>
            Object.keys(formContext.getValues()).forEach(fieldName =>
                formContext.trigger(fieldName as never as FieldPath<T>)
            ),
        context: formContext,
    };
};

const PageLeaveGuard = <T extends FieldValues>({
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    isTrackParameters = false,
    onSubmit,
}: IPageLeaveGuardProps<T>) => {
    const [popupState, popupDispatch] = usePopupState<Partial<ActionState & { isLoading: boolean }>>({
        open: false,
        isLoading: false,
    });

    const { dirty, submitForm, validateForm, resetForm, getValues, markErrorFields, context, isSubmitting } =
        useIsomorphicForm<T>();

    const dirtyRef = useRef<boolean>();
    const submittingRef = useRef<boolean>();
    dirtyRef.current = dirty;
    submittingRef.current = isSubmitting;

    const shouldStopNavigation = dirty && !isSubmitting;

    const { cancelNavigation, confirmNavigation } = useNavigationObserver({
        shouldStopNavigation,
        onNavigate() {
            popupDispatch({ type: ActionType.Add });
        },
    });

    const { appendModal } = useModalsContext();

    return (
        <ActionPopup
            open={!!popupState.open}
            onAction={async () => {
                markErrorFields();
                const errors = await validateForm();
                if (Object.keys(errors).length) {
                    appendModal({
                        title: 'Невозможно сохранить',
                        message: 'Устраните ошибки в форме',
                        theme: 'error',
                    });
                    popupDispatch({ type: ActionType.PreClose });
                    cancelNavigation();
                    return;
                }

                try {
                    popupDispatch({ type: ActionType.Edit, payload: { ...popupState, isLoading: true } });
                    if (onSubmit) {
                        await onSubmit(getValues(), context);
                    } else {
                        await submitForm();
                    }
                } catch (e) {
                    console.error(e);
                    return;
                } finally {
                    popupDispatch({ type: ActionType.Edit, payload: { ...popupState, isLoading: false } });
                }

                resetForm();
                popupDispatch({ type: ActionType.PreClose });
                setTimeout(() => {
                    confirmNavigation();
                }, 0);
            }}
            onBackdropClick={() => {
                popupDispatch({ type: ActionType.PreClose });
                cancelNavigation();
            }}
            onClose={() => {
                resetForm(getValues());
                popupDispatch({ type: ActionType.PreClose });
                confirmNavigation();
            }}
            onUnmount={() => {
                popupDispatch({ type: ActionType.Close });
            }}
            disableAction={popupState.isLoading}
            disableClose={popupState.isLoading}
            leftAddonIconTheme="warning"
            title="Сохранить изменения?"
            blockButtons
        >
            <ActionPopupContent>
                <p>Вы внесли изменения на странице, но не сохранили их</p>
            </ActionPopupContent>
        </ActionPopup>
    );
};

export default PageLeaveGuard;
