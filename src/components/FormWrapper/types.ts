import { type IFormProps } from '@ensi-platform/core-components';
import type { FieldValues } from 'react-hook-form';

import { RedirectMethods } from '@scripts/enums';

interface ISubmitResProps<T extends FieldValues> {
    redirectPath: string;
    method: RedirectMethods;
    valuesToReset?: T;
    query?: Record<string, string>;
}

export interface IFormWrapperProps<T extends FieldValues> extends Omit<IFormProps<T>, 'onSubmit'> {
    onSubmit: (...args: Parameters<IFormProps<T>['onSubmit']>) => ISubmitResProps<T> | Promise<ISubmitResProps<T>>;
}
