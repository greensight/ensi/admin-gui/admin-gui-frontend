import { Form } from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { useCallback } from 'react';
import type { FieldValues, UseFormReturn } from 'react-hook-form';

import PageLeaveGuard from '@components/PageLeaveGuard';

import { RedirectMethods } from '@scripts/enums';

import { IFormWrapperProps } from './types';

const FormWrapper = <T extends FieldValues>({ children, onSubmit, ...props }: IFormWrapperProps<T>) => {
    const { push, replace } = useRouter();

    const submitForm = useCallback(
        async (values: T, formProps: UseFormReturn<T, unknown>) => {
            const res = await onSubmit(values, formProps);
            formProps.reset(res.valuesToReset || values);
            setTimeout(() => {
                if (res.redirectPath && res.method === RedirectMethods.push) push({ pathname: res.redirectPath });
                if (res.redirectPath && res.method === RedirectMethods.replace) {
                    const queryParam = res.query ? res.query : {};
                    replace({ pathname: res.redirectPath, query: queryParam });
                }
            }, 0);
        },
        [onSubmit, push, replace]
    );

    return (
        <Form {...props} onSubmit={submitForm}>
            {typeof children === 'function' ? (
                params => (
                    <>
                        <PageLeaveGuard onSubmit={submitForm} />
                        {children(params)}
                    </>
                )
            ) : (
                <>
                    <PageLeaveGuard onSubmit={submitForm} />
                    {children}
                </>
            )}
        </Form>
    );
};

export default FormWrapper;
