import { FormFieldWrapper, useFormContext } from '@ensi-platform/core-components';
import { FC, useCallback, useMemo, useRef } from 'react';

import CalendarInput from '@controls/CalendarInput';

import { Layout, scale } from '@scripts/gds';
import { getValueFromObject } from '@scripts/helpers';
import { useMedia } from '@scripts/hooks';

interface CalendarRangeProps {
    label?: string;
    nameFrom: string;
    nameTo: string;
    disabled?: boolean;
}

const useIsomorphicValues = ({ nameFrom, nameTo }: Pick<CalendarRangeProps, 'nameFrom' | 'nameTo'>) => {
    const oldConsoleWarning = console.warn;
    console.warn = () => {};
    console.warn = oldConsoleWarning;
    const formContext = useFormContext();

    if (!formContext) {
        throw new Error('CalendarRange can only exist as a descendant of react-hook-form');
    }

    const [valueFrom, valueTo] = formContext.watch([nameFrom, nameTo]);
    return {
        values: {
            [nameFrom]: valueFrom,
            [nameTo]: valueTo,
        },
        form: 'rhf',
    };
};

const CalendarRange: FC<CalendarRangeProps> = ({ nameFrom, nameTo, label, disabled }) => {
    const { values, form } = useIsomorphicValues({ nameFrom, nameTo });
    const startDate = useMemo(() => (values ? +getValueFromObject(nameFrom, values, NaN) : NaN), [nameFrom, values]);
    const endDate = useMemo(() => (values ? +getValueFromObject(nameTo, values, NaN) : NaN), [nameTo, values]);
    const { md, xxl, xl, xs } = useMedia();

    const endRef = useRef<HTMLInputElement>(null);

    const onCalendarInputChange = useCallback(() => {
        setTimeout(() => {
            if (endRef?.current && !endDate) {
                endRef?.current?.click();
                endRef?.current?.focus();
            }
        }, 0);
    }, [endDate]);

    return (
        <Layout cols={{ xxxl: 2, xxs: 1 }}>
            <Layout.Item col={1}>
                <FormFieldWrapper name={nameFrom} disabled={disabled}>
                    <CalendarInput
                        label={label}
                        placeholder="От"
                        maxDate={endDate}
                        calendarProps={{
                            selectedFrom: startDate,
                            selectedTo: endDate,
                        }}
                        onChange={onCalendarInputChange}
                    />
                </FormFieldWrapper>
            </Layout.Item>
            <Layout.Item
                col={1}
                align="end"
                css={{
                    position: 'relative',
                    '&::before': {
                        content: "'–'",
                        position: 'absolute',
                        left: -15,
                        bottom: scale(1),
                        [xxl]: {
                            left: -scale(3, true),
                        },
                        [xl]: {
                            left: -15,
                        },
                        [md]: {
                            left: -scale(3, true),
                        },
                        [xs]: {
                            content: 'none',
                        },
                    },
                }}
            >
                <FormFieldWrapper name={nameTo} {...(form === 'rhf' && { ref: endRef })} disabled={disabled}>
                    <CalendarInput
                        placeholder="До"
                        minDate={startDate}
                        calendarProps={{
                            selectedFrom: startDate,
                            selectedTo: endDate,
                        }}
                        placement="bottom-end"
                        ref={endRef}
                    />
                </FormFieldWrapper>
            </Layout.Item>
        </Layout>
    );
};
export default CalendarRange;
