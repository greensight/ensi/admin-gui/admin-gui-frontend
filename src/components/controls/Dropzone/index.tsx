import { IFieldWrapperProps } from '@ensi-platform/core-components';
import { DragDropContext, DropResult, Droppable } from '@hello-pangea/dnd';
import { FC, useCallback, useMemo, useRef, useState } from 'react';
import { FileRejection, DropzoneProps as UseDropzoneProps, useDropzone } from 'react-dropzone';

import { scale, typography, useTheme } from '@scripts/gds';
import { useDeferredLoading } from '@scripts/hooks/useDeferredLoading';

import Legend from '../Legend';
import LoadingSkeleton from '../LoadingSkeleton';
import DropzoneArea from './DropzoneArea';
import { DraggableDropzoneFile, DropzoneFileProps, FileType } from './DropzoneFile';
import { ErrorCodes, ImagePreview } from './constants';
import { canPreviewImages, getFileSize, makeMatrixArray, removeItemFromArray } from './utils';

interface IFileWithReorderName extends File {
    reorderName: string;
}

export interface IDropzoneProps extends UseDropzoneProps, Partial<IFieldWrapperProps<FileType[]>> {
    label?: string;

    /** On files change callback */
    onFilesChange?: (files: FileType[]) => void;

    /** On file remove callback. You may need it for remove already uploaded file from Database */
    onFileRemove?: DropzoneFileProps['onRemoveClick'];

    /** On file click callback. You may need it for downloading file */
    onFileClick?: DropzoneFileProps['onFileClick'];

    /** Enable file click callback. */
    enableFileClick?: DropzoneFileProps['enableFileClick'];

    /** Disable dragging */
    isDragDisabled?: boolean;

    /** Disable delete button */
    isDisableRemove?: boolean;

    /** Button-like view */
    simple?: boolean;

    maxFileNameLength?: number;

    isLoading?: boolean;

    onBlur?: () => void;
}

const Dropzone: FC<IDropzoneProps> = ({
    accept,
    maxFiles,
    maxSize,
    maxFileNameLength,
    field,
    setFieldValue,
    onFilesChange,
    onFileRemove: onFileRemoveFromProps,
    isDragDisabled,
    isDisableRemove,
    disabled,
    simple,
    isLoading = false,
    onFileClick,
    enableFileClick = false,
    label,
    error,
    onBlur,
    ...props
}) => {
    const { components } = useTheme();
    const IT = components?.Input;
    const imagePreview = canPreviewImages(accept);

    /** checks is our Dropzone controlled by RHF or not  */
    const isControlled = typeof field?.value !== 'undefined';
    const [filesState, setFilesState] = useState<File[]>([]);
    const files = useMemo(
        () => (isControlled ? field?.value || [] : filesState),
        [field?.value, filesState, isControlled]
    );

    const setFiles = useCallback(
        (newFiles: FileType[]) => {
            if (isControlled) {
                setFieldValue?.(newFiles);
            } else {
                setFilesState(newFiles);
            }
            if (onFilesChange) onFilesChange(newFiles);
        },
        [setFieldValue, isControlled, onFilesChange]
    );

    const rejectedFilesMap = useRef<Map<string, FileRejection['errors']>>(new Map());

    const fileValidator = (file: FileType) => {
        if (maxSize && file.size > maxSize)
            return { code: ErrorCodes.TOO_BIG_FILE, message: `Максимальный размер файла ${getFileSize(maxSize)}` };
        if (maxFileNameLength && file.name.length > maxFileNameLength)
            return {
                code: ErrorCodes.TOO_LONG_FILE_NAME,
                message: `Максимальная длина имени файла ${maxFileNameLength} символов`,
            };
        return null;
    };

    const filterRepeatedFiles = useCallback(
        (newFiles: FileType[]) => newFiles.filter(f => !files.find(af => af.name === f.name)),
        [files]
    );

    const onDropAccepted = useCallback(
        (acceptedFiles: FileType[]) => {
            setFiles([...files, ...filterRepeatedFiles(acceptedFiles)]);
        },
        [files, filterRepeatedFiles, setFiles]
    );

    const onDropRejected = useCallback(
        (rejectedFiles: FileRejection[]) => {
            const filesToUpload = rejectedFiles.map(rejectedFile => rejectedFile.file);
            setFiles([...files, ...filterRepeatedFiles(filesToUpload)]);

            rejectedFiles.forEach(({ file, errors }) => {
                rejectedFilesMap.current.set(file.name, errors);
            });
        },
        [files, filterRepeatedFiles, setFiles]
    );

    const onFileRemove = useCallback(
        (removedFileIndex: number, removedFile: FileType) => {
            setFiles(removeItemFromArray<FileType>(files, removedFileIndex));
            rejectedFilesMap.current.delete(removedFile.name);
            onFileRemoveFromProps?.(removedFileIndex, removedFile);
        },
        [files, onFileRemoveFromProps, setFiles]
    );

    const { getRootProps, getInputProps, rootRef } = useDropzone({
        onDropAccepted,
        onDropRejected,
        accept,
        maxFiles,
        validator: fileValidator,
        disabled,
        ...props,
    });

    const containerWidth = useMemo(
        () => (imagePreview ? rootRef.current?.offsetWidth || 0 : 0),
        [imagePreview, rootRef]
    );
    const itemsInRow = useMemo(
        () => (imagePreview ? Math.floor(containerWidth / (ImagePreview.width + scale(2))) || 0 : 0),
        [containerWidth, imagePreview]
    );
    const filesMatrix = useMemo(() => {
        if (imagePreview && itemsInRow) {
            return makeMatrixArray<FileType>(files, itemsInRow);
        }
        return [files];
    }, [files, imagePreview, itemsInRow]);

    /** react beautiful dnd callbacks */
    const reorderItems = useCallback(
        (startIndex: number, endIndex: number) => {
            const newFiles = files.slice();
            const [movedItem] = newFiles.splice(startIndex, 1);

            // reorderName field is necessary for correct work of RHF's deepEqual with Files
            (movedItem as IFileWithReorderName).reorderName = movedItem.name;

            newFiles.splice(endIndex, 0, movedItem);
            setFiles(newFiles);
        },
        [files, setFiles]
    );

    const onDragEnd = useCallback(
        ({ source, destination }: DropResult) => {
            if (!destination || (destination.index === source.index && destination.droppableId === source.droppableId))
                return;
            reorderItems(
                source.index + +source.droppableId * itemsInRow,
                destination.index + +destination.droppableId * itemsInRow
            );
        },
        [reorderItems, itemsInRow]
    );

    const deferredIsLoading = useDeferredLoading(isLoading, 1000);

    return (
        <div css={typography(IT?.typography)}>
            <Legend label={label} error={error} />
            <DropzoneArea
                {...getRootProps()}
                inputFieldProps={{
                    ...getInputProps(),
                    onBlur,
                }}
                disabled={disabled || isDragDisabled}
                simple={simple}
                error={error}
            />
            {deferredIsLoading ? (
                <LoadingSkeleton height={scale(28)} width={scale(23)} />
            ) : (
                <div css={imagePreview && { display: 'flex', flexWrap: 'wrap' }}>
                    <DragDropContext onDragEnd={onDragEnd}>
                        {filesMatrix.map((filesArr, idx) => (
                            <Droppable
                                droppableId={`${idx}`}
                                direction={imagePreview ? 'horizontal' : 'vertical'}
                                isCombineEnabled={imagePreview}
                                key={idx}
                            >
                                {provided => (
                                    <ul
                                        css={{
                                            marginTop: scale(1),
                                            ...(imagePreview && {
                                                marginRight: scale(2),
                                                overflow: 'hidden',
                                                width: '100%',
                                            }),
                                        }}
                                        ref={provided.innerRef}
                                        {...provided.droppableProps}
                                    >
                                        {filesArr?.map((file, index) => (
                                            <DraggableDropzoneFile
                                                key={`${file?.name}${file?.lastModified}`}
                                                file={file}
                                                index={index}
                                                onRemoveClick={onFileRemove}
                                                isDisableRemove={isDisableRemove}
                                                imagePreview={imagePreview}
                                                isDragDisabled={disabled || isDragDisabled}
                                                disabled={disabled}
                                                errors={rejectedFilesMap.current.get(file?.name)}
                                                onFileClick={onFileClick}
                                                enableFileClick={enableFileClick}
                                            />
                                        ))}
                                        {imagePreview ? null : provided.placeholder}
                                    </ul>
                                )}
                            </Droppable>
                        ))}
                    </DragDropContext>
                </div>
            )}
        </div>
    );
};

export default Dropzone;
