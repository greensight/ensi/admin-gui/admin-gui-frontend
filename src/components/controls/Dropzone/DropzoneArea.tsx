import { IFieldWrapperProps } from '@ensi-platform/core-components';
import { HTMLAttributes, forwardRef } from 'react';

import { Button, scale, useTheme } from '@scripts/gds';

import ImportIcon from '@icons/small/import.svg';

import { FileType } from './DropzoneFile';

interface DropzoneAreaProps extends HTMLAttributes<HTMLDivElement>, Partial<IFieldWrapperProps<FileType[]>> {
    inputFieldProps: HTMLAttributes<HTMLInputElement>;
    disabled?: boolean;
    /** Button-like view */
    simple?: boolean;
}

const DropzoneArea = forwardRef<HTMLDivElement, DropzoneAreaProps>(
    ({ disabled, inputFieldProps, simple, error, ...props }, ref) => {
        const { colors, components } = useTheme();
        const IT = components?.Input;

        return simple ? (
            <div
                {...props}
                ref={ref}
                onBlur={(...args: [any]) => {
                    if (typeof props.onBlur === 'function') props.onBlur(...args);
                    if (typeof inputFieldProps.onBlur === 'function') {
                        inputFieldProps.onBlur(...args);
                    }
                }}
            >
                <input {...inputFieldProps} disabled={disabled} />
                <Button Icon={ImportIcon} type="button" disabled={disabled} theme="secondary">
                    Загрузить
                </Button>
            </div>
        ) : (
            <div
                {...props}
                onBlur={(...args: [any]) => {
                    if (typeof props.onBlur === 'function') props.onBlur(...args);
                    if (typeof inputFieldProps.onBlur === 'function') {
                        inputFieldProps.onBlur(...args);
                    }
                }}
                css={{
                    display: 'grid',
                    placeItems: 'center',
                    border: error ? `1px solid ${colors?.danger}` : `1px dashed ${IT?.borderColor}`,
                    borderRadius: IT?.borderRadius,
                    background: IT?.bg,
                    padding: scale(2),
                    textAlign: 'center',
                    transition: 'background 200ms ease-out',
                    cursor: disabled ? 'not-allowed' : 'pointer',
                    ...(!disabled && {
                        ':hover': { background: colors?.infoBg },
                    }),
                }}
                ref={ref}
            >
                <input {...inputFieldProps} disabled={disabled} />
                <ImportIcon width={scale(4)} height={scale(4)} css={{ marginBottom: scale(1) }} />
                <p>
                    Нажмите для загрузки файла <br />
                    или перетащите его в&nbsp;эту область
                </p>
            </div>
        );
    }
);

export default DropzoneArea;
