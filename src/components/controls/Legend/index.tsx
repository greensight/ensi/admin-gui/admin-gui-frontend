import { CSSObject, jsx } from '@emotion/react';
import { FormMessage, FormMessageProps, IFieldWrapperProps } from '@ensi-platform/core-components';
import React, { ReactNode } from 'react';

import { scale, typography, useTheme } from '@scripts/gds';
import { MergeElementProps } from '@scripts/utils';

export interface LegendBaseProps extends Partial<IFieldWrapperProps<any>> {
    /** Name for form (inner) */
    name?: string;
    /** Label for Legend */
    label?: string | ReactNode | null;
    /** Flag required for forms */
    required?: boolean;
    /** Hint for legend */
    hint?: string;
    /** Type for form */
    type?: string;
    /** Success text */
    success?: string;
    /** Show message flag */
    showMessage?: boolean;
    /** Custom message text */
    messageText?: string;
    /** Message type */
    messageType?: FormMessageProps['type'];
}

export type LegendProps<P extends keyof React.ReactHTML = 'label'> = {
    /** Use your own React component for render. */
    as?: P;
} & MergeElementProps<P, LegendBaseProps>;

export const Legend = <T extends keyof React.ReactHTML = 'label'>({
    as,
    label,
    required = true,
    hint,
    error,
    name,
    showMessage,
    messageType = 'warning',
    messageText = 'Есть изменения',
    ...props
}: LegendProps<T>) => {
    const { colors } = useTheme();
    delete props.id;
    delete props.field;

    return jsx(
        as || 'label',
        {
            htmlFor: name,
            ...props,
            css: { display: 'block', position: 'relative', paddingBottom: scale(1) } as CSSObject,
        },
        <>
            <div
                css={{
                    ...(error && {
                        color: colors?.danger,
                    }),
                }}
            >
                {label && typeof label === 'string' ? <span css={typography('bodySmBold')}>{label}</span> : label}
                {!required && (
                    <span css={{ ...typography('small'), marginLeft: scale(1, true) }}>(необязательное)</span>
                )}
            </div>
            {hint && (
                <div css={{ ...typography('bodySm'), color: colors?.grey800, marginTop: scale(1, true) }}>{hint}</div>
            )}
            {error && <FormMessage message={error} css={{ marginTop: scale(1, true) }} />}
            {!error && showMessage ? (
                <FormMessage message={messageText} type={messageType} css={{ marginTop: scale(1, true) }} />
            ) : null}
        </>
    );
};

export default Legend;
