import { useMemo } from 'react';

import { MEDIA_QUERIES, colors } from '@scripts/gds';

import StarIcon from '@icons/star.svg';

import { CSSObject } from '@emotion/react';
import { RatingStarProps, VISUALLY_HIDDEN_CSS } from './types';

export const RatingStar = ({
    id: ratingId,
    name,
    itemValue,
    ratingValue,
    actualRatingValue,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    isActive,
    hoveredValue,
    focusedValue,
    isReadonly,
    labelCSS,
    iconCSS,
    className,
    disabled,
    showReadOnlyEmptyStar,
    onChange,
    onBlur,
    onFocus,
    onClick,
    getLabelText,
    onMouseOver,
    onMouseOut,
    ...props
}: RatingStarProps) => {
    delete props.css;
    const id = `${ratingId}-${itemValue}`;

    const isChecked = itemValue === actualRatingValue;
    const isFilled = itemValue <= ratingValue;
    const isHovered = itemValue <= hoveredValue;
    const isFocusVisible = itemValue === focusedValue;

    const totalCSS = useMemo(
        () => ({
            ...labelCSS,
            ...(isFocusVisible && {
                outline: `2px solid ${colors.grey600}`,
            }),
            [MEDIA_QUERIES.smMin]: {
                ...((labelCSS[MEDIA_QUERIES.smMin] as CSSObject) || {}),
                ...(isHovered && {
                    transform: 'scale(1.1)',
                }),
            },
            ...(isFilled
                ? {
                      fill: colors.warning,
                      color: colors.warning,
                  }
                : {
                      fill: colors.grey600,
                      color: colors.grey600,
                  }),
        }),
        [isFilled, isFocusVisible, isHovered, labelCSS]
    );

    if (isReadonly) {
        return isFilled || showReadOnlyEmptyStar ? (
            <span css={totalCSS} className={className}>
                <StarIcon css={iconCSS} />
            </span>
        ) : null;
    }

    const changeHandler = () => {
        if (!onChange || isReadonly) {
            return;
        }
        onChange(itemValue);
    };

    return (
        <>
            <label htmlFor={id} css={totalCSS} className={className} {...props}>
                <StarIcon css={iconCSS} onMouseOver={onMouseOver} onMouseOut={onMouseOut} />
                {getLabelText && <span css={VISUALLY_HIDDEN_CSS}>{getLabelText(itemValue)}</span>}
            </label>
            <input
                type="radio"
                value={itemValue}
                checked={isChecked}
                disabled={disabled}
                name={name}
                id={id}
                css={VISUALLY_HIDDEN_CSS}
                onChange={changeHandler}
                onInput={changeHandler}
                onFocus={onFocus}
                onBlur={onBlur}
                onClick={onClick}
            />
        </>
    );
};
