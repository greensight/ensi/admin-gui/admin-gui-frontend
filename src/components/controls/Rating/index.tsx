import { nanoid } from 'nanoid';
import { useCallback, useMemo, useState } from 'react';

import { colors } from '@scripts/gds';

import { RatingStar } from './star';
import { RatingProps, VISUALLY_HIDDEN_CSS } from './types';

const Rating = ({
    value: propValue,
    onChange,
    getLabelText = val => `${val} звезд.`,
    showReadOnlyEmptyStar,
    field,
    setFieldValue,
    disabled,
    readOnly,
    className,
    name: propName,
}: RatingProps) => {
    const controlled = propValue !== undefined && (onChange || readOnly || disabled);

    if (!onChange && !readOnly) {
        console.error('Rating component in non-readonly mode requires onChange');
    }

    const innerValue = (controlled ? propValue : +`${field?.value}`) || 0;
    const name = controlled ? propName! : field!.name;

    const id = useMemo(() => field?.name || nanoid(4), [field?.name]);

    const [{ hoveredValue, focusedValue }, setState] = useState({
        hoveredValue: -1,
        focusedValue: -1,
    });

    const handleChange = useCallback(
        (val: number) => {
            let newValue = val;
            // Mouse priority over keyboard
            if (hoveredValue !== -1) {
                newValue = hoveredValue;
            }

            if (controlled && onChange) {
                onChange(newValue);
            } else if (setFieldValue) setFieldValue(newValue);
        },
        [controlled, setFieldValue, hoveredValue, onChange]
    );

    let value = innerValue || 0;
    if (hoveredValue !== -1) value = hoveredValue;
    if (focusedValue !== -1) value = focusedValue;

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [emptyValueFocused, setEmptyValueFocused] = useState(false);

    const [isFocused, setFocused] = useState(false);
    const isContainerFocused = isFocused && focusedValue === -1;

    return (
        <div
            className={className}
            css={{
                display: 'inline-flex',
                ...(isContainerFocused && {
                    outline: `2px solid ${colors.grey600}`,
                }),
            }}
            onFocus={() => setFocused(true)}
            onBlur={() => setFocused(false)}
            {...(readOnly && { role: 'img', 'aria-label': getLabelText(value || 0) })}
        >
            {Array(5)
                .fill(undefined)
                .map((_, i) => {
                    const itemValue = i + 1;
                    const isActive = itemValue === value && hoveredValue !== -1 && focusedValue !== -1;

                    return (
                        <RatingStar
                            id={id}
                            showReadOnlyEmptyStar={showReadOnlyEmptyStar}
                            name={name}
                            key={itemValue}
                            isActive={isActive}
                            isReadonly={readOnly}
                            disabled={disabled}
                            ratingValue={value}
                            actualRatingValue={innerValue}
                            hoveredValue={hoveredValue}
                            focusedValue={focusedValue}
                            itemValue={itemValue}
                            labelCSS={{
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'center',
                                cursor: 'pointer',
                                ...(readOnly && {
                                    cursor: 'default',
                                }),
                                ...(disabled && {
                                    cursor: 'not-allowed',
                                    opacity: 0.5,
                                    svg: {
                                        fill: colors.grey400,
                                        color: colors.grey400,
                                    },
                                }),
                            }}
                            iconCSS={{
                                transition: 'transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
                                willChange: 'transform',
                            }}
                            onChange={handleChange}
                            getLabelText={getLabelText}
                            onBlur={() => {
                                if (readOnly || disabled) return;

                                if (hoveredValue !== -1) return;

                                setState(prev => ({
                                    hoveredValue: prev.hoveredValue,
                                    focusedValue: -1,
                                }));
                            }}
                            onFocus={() => {
                                if (readOnly || disabled) return;

                                setFocused(false);

                                setState(prev => ({
                                    hoveredValue: prev.hoveredValue,
                                    focusedValue: itemValue,
                                }));
                            }}
                            onClick={event => {
                                // Ignore keyboard events
                                // https://github.com/facebook/react/issues/7407
                                if (event.clientX === 0 && event.clientY === 0) {
                                    return;
                                }

                                setState({
                                    hoveredValue: -1,
                                    focusedValue: -1,
                                });

                                if (innerValue === itemValue) {
                                    handleChange(0);
                                }
                            }}
                            onMouseOver={() => {
                                if (readOnly || disabled) return;
                                setState(prev =>
                                    prev.hoveredValue === itemValue && prev.focusedValue === itemValue
                                        ? prev
                                        : {
                                              hoveredValue: itemValue,
                                              focusedValue: prev.focusedValue,
                                          }
                                );
                            }}
                            onMouseOut={() => {
                                if (readOnly || disabled) return;
                                setState({
                                    hoveredValue: -1,
                                    focusedValue: -1,
                                });
                            }}
                        />
                    );
                })}
            {!readOnly && !disabled && (
                <label>
                    <input
                        type="radio"
                        css={VISUALLY_HIDDEN_CSS}
                        value="0"
                        name={name}
                        id={`${id}-empty`}
                        checked={innerValue === 0}
                        onFocus={() => setEmptyValueFocused(true)}
                        onBlur={() => setEmptyValueFocused(false)}
                        onChange={e => handleChange(e.target.value === '' ? 0 : +e.target.value)}
                    />
                    <span css={VISUALLY_HIDDEN_CSS}>{getLabelText(0)}</span>
                </label>
            )}
        </div>
    );
};

export default Rating;
