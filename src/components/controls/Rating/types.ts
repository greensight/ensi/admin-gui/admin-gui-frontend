import { CSSObject } from '@emotion/react';
import { HTMLProps } from 'react';
import type { ControllerFieldState, ControllerRenderProps } from 'react-hook-form';

export interface RatingProps {
    disabled?: boolean;
    value?: number;
    name?: string;
    /** Параметры RHF */
    field?: ControllerRenderProps;
    fieldState?: ControllerFieldState;
    setFieldValue?: (value: any) => void;
    className?: string;
    onChange?: (value: number) => void;
    readOnly?: boolean;
    getLabelText?: (val: number) => string;
    emptyLabelText?: string;
    showReadOnlyEmptyStar?: boolean;
}

export interface RatingState {
    isDisabled: boolean;
    isReadonly: boolean;
    isFocused: boolean;

    isIconEmpty: boolean;
    isIconFilled: boolean;
    isIconHover: boolean;
    isLabelFocused: boolean;
    isIconActive: boolean;
    isLabelEmptyValueActive: boolean;
}

export interface RatingStarProps extends Omit<HTMLProps<HTMLLabelElement>, 'onChange' | 'name'> {
    itemValue: number;
    ratingValue: number;
    actualRatingValue: number;
    hoveredValue: number;
    focusedValue: number;
    isActive: boolean;
    disabled?: boolean;
    labelCSS: CSSObject;
    iconCSS: CSSObject;
    isReadonly?: boolean;
    id: string;
    name: string;
    onChange: RatingProps['onChange'];
    onBlur: () => void;
    onFocus: () => void;
    onClick: (event: any) => void;
    getLabelText: RatingProps['getLabelText'];
    showReadOnlyEmptyStar: RatingProps['showReadOnlyEmptyStar'];
}

export const VISUALLY_HIDDEN_CSS: CSSObject = {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    whiteSpace: 'nowrap',
    width: 1,
};
