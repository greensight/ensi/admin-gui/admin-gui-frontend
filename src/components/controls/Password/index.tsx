import { IFieldWrapperProps, Input, InputProps } from '@ensi-platform/core-components';
import { useState } from 'react';

import { colors } from '@scripts/gds';

import EyeIcon from '@icons/small/eye.svg';
import EyeOffIcon from '@icons/small/eyeOff.svg';

export interface PasswordProps extends InputProps, Partial<IFieldWrapperProps<string>> {}

const Password = ({ field, error, autoComplete = 'off', ...props }: PasswordProps) => {
    const [isVisible, setIsVisible] = useState(false);
    const inputProps = { ...field, ...props };

    return (
        <>
            <Input
                {...props}
                label={props.label}
                value={field?.value || ''}
                onChange={field?.onChange}
                error={error}
                autoComplete={autoComplete}
                type={isVisible ? 'text' : 'password'}
                rightAddons={
                    <button
                        type="button"
                        onClick={() => setIsVisible(!isVisible)}
                        css={{
                            width: '100%',
                            height: '100%',
                            color: isVisible ? colors.black : colors.grey200,
                            transition: 'fill ease 300ms',
                            ':focus': { outlineOffset: 0, outlineColor: colors.primary, outlineWidth: 2 },
                        }}
                    >
                        {isVisible ? <EyeOffIcon title="Показать пароль" /> : <EyeIcon title="Скрыть пароль" />}
                    </button>
                }
            />
            {autoComplete === 'off' && <input name={inputProps.name} type="password" style={{ display: 'none' }} />}
        </>
    );
};

export default Password;
