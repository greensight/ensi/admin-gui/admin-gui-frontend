import { IFieldWrapperProps } from '@ensi-platform/core-components';
import { IMaskInput } from 'react-imask';

import { useFieldCSS } from '@scripts/hooks';

import Legend from '../Legend';

export interface MaskProps extends Partial<IFieldWrapperProps<string>> {
    /** Mask for input */
    mask: RegExp | string | { mask: string; definitions: Record<string, RegExp> };
    /** Placeholder for mask */
    placeholderChar?: string;
    /** Is show placeholder */
    lazy?: boolean;
    label?: string;
    className?: string;
    blocks?: Record<string, any>;
    format?: (input: any) => string;
    unmask?: (val: string) => any;
    autofix?: boolean;
    overwrite?: boolean;
}

const Mask = ({
    mask,
    label,
    error,
    field,
    setFieldValue,
    placeholderChar = '_',
    lazy = true,
    className,
    ...props
}: MaskProps) => {
    const value = field?.value || '';
    const { basicFieldCSS } = useFieldCSS(!!error);

    return (
        <div className={className}>
            <Legend label={label} error={error} />
            <IMaskInput
                mask={mask}
                value={value}
                {...props}
                css={basicFieldCSS}
                lazy={lazy}
                placeholderChar={placeholderChar}
                onAccept={(val: string) => {
                    if (val === value) return;

                    setFieldValue?.(val);
                }}
            />
        </div>
    );
};

export default Mask;
