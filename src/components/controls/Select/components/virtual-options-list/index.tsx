import { CSSObject } from '@emotion/react';
import { UIEventHandler, forwardRef, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { mergeRefs } from 'react-merge-refs';
import { VirtualItem, useVirtual } from 'react-virtual';

import { colors, scale } from '@scripts/gds';

import { useSelectTheme } from '../../context';
import { GroupShape, OptionShape, OptionsListProps } from '../../types';
import { isGroup, useVisibleOptions } from '../../utils';
import { Optgroup as DefaultOptgroup } from '../optgroup';

const createCounter = () => {
    let count = 0;
    // eslint-disable-next-line no-plusplus
    return () => count++;
};

export const VirtualOptionsList = forwardRef(
    (
        {
            className,
            optionGroupClassName,
            Option,
            getOptionProps,
            options = [],
            Optgroup = DefaultOptgroup,
            emptyPlaceholder,
            visibleOptions = 4,
            onScroll,
            open,
            header,
            footer,
            highlightedIndex,
        }: OptionsListProps,
        ref
    ) => {
        const renderOption = useCallback(
            (option: OptionShape, index: number, virtualOption?: VirtualItem) => (
                <Option
                    key={option.key}
                    {...getOptionProps(option, index)}
                    css={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        width: '100%',
                        ...(virtualOption && { transform: `translateY(${virtualOption.start}px)` }),
                    }}
                    // @ts-ignore
                    ref={virtualOption?.measureRef}
                />
            ),
            [Option, getOptionProps]
        );

        const listRef = useRef<HTMLDivElement>(null);
        const parentRef = useRef<HTMLDivElement>(null);
        const counter = createCounter();
        const renderGroup = useCallback(
            (group: GroupShape) => (
                <Optgroup className={optionGroupClassName} label={group.label} key={group.label}>
                    {group.options.map(option => renderOption(option, counter()))}
                </Optgroup>
            ),
            [Optgroup, optionGroupClassName, renderOption, counter]
        );

        const rowVirtualizer = useVirtual({
            size: options.length,
            parentRef,
            overscan: visibleOptions,
        });

        const [visibleOptionsInvalidateKey, setVisibleOptionsInvalidateKey] = useState(0);

        useEffect(() => {
            setVisibleOptionsInvalidateKey(
                /**
                 * react-virtual может несколько раз отрендерить список с одним элементом,
                 * поэтому нужно еще раз пересчитать высоту, когда список ВИДИМЫХ пунктов будет отрендерен полностью
                 * Также, высоту нужно пересчитывать при изменении ОБЩЕГО кол-ва пунктов меню
                 */
                rowVirtualizer.virtualItems.length > 1 ? options.length : 1
            );
        }, [rowVirtualizer.virtualItems.length, options.length]);

        const { getCSS } = useSelectTheme();

        useEffect(() => {
            if (open && highlightedIndex !== undefined) {
                rowVirtualizer.scrollToIndex(highlightedIndex, { align: 'end' });
            }
        }, [highlightedIndex, open, rowVirtualizer]);

        const virtualItems = useMemo(() => rowVirtualizer.virtualItems, [rowVirtualizer]);

        useVisibleOptions({
            visibleOptions,
            invalidate: visibleOptionsInvalidateKey,
            listRef,
            styleTargetRef: parentRef,
            open,
        });

        if (options.length === 0 && !emptyPlaceholder) {
            return null;
        }

        const renderListItems = () => (
            <>
                {virtualItems.map(virtualOption => {
                    const option = options[virtualOption.index];

                    if (isGroup(option)) {
                        return renderGroup(option);
                    }

                    return renderOption(option, counter(), virtualOption);
                })}

                {emptyPlaceholder && options.length === 0 && (
                    <div
                        css={{
                            color: colors.grey400,
                            padding: scale(1),
                        }}
                    >
                        {emptyPlaceholder}
                    </div>
                )}
            </>
        );

        const renderWithNativeScrollbar = () => (
            <div
                ref={parentRef}
                css={{
                    position: 'relative',
                    overflow: 'auto',
                    width: '100%',
                    ...(getCSS('optionListWrapper') as CSSObject),
                }}
                onScroll={onScroll as UIEventHandler}
            >
                <ul
                    className="option-list"
                    css={{
                        width: '100%',
                        height: `${rowVirtualizer.totalSize}px`,
                    }}
                    ref={mergeRefs([listRef, ref])}
                >
                    {renderListItems()}
                </ul>
            </div>
        );

        return (
            <div
                className={className}
                css={{
                    width: '100%',
                    position: 'relative',
                    outline: 'none',
                    ...getCSS('optionList'),
                }}
            >
                {header}
                {renderWithNativeScrollbar()}
                {footer}
            </div>
        );
    }
);
