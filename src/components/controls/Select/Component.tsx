import { forwardRef, useMemo } from 'react';

import { Arrow as DefaultArrow } from './components/arrow';
import { BaseSelect } from './components/base-select';
import { Field as DefaultField } from './components/field';
import { Optgroup as DefaultOptgroup } from './components/optgroup';
import { Option as DefaultOption } from './components/option';
import { OptionsList as DefaultOptionsList } from './components/options-list';
import useSelectClear from './presets/useSelectClear';
import { BaseSelectProps, OptionShape } from './types';

export type SelectProps = BaseSelectProps & {
    hideClearButton?: boolean;
    onClear?: () => void;
};

export const SimpleSelect = forwardRef<HTMLDivElement, SelectProps>(
    (
        {
            Arrow = DefaultArrow,
            Field = DefaultField,
            OptionsList = DefaultOptionsList,
            Optgroup = DefaultOptgroup,
            Option = DefaultOption,
            ...restProps
        },
        ref
    ) => {
        const props = useMemo(
            () => ({
                ref,
                Option,
                Field,
                Optgroup,
                OptionsList,
                Arrow,
                ...restProps,
            }),
            [Arrow, Field, Optgroup, Option, OptionsList, ref, restProps]
        );

        return <BaseSelect {...props} />;
    }
);

SimpleSelect.displayName = 'SimpleSelect';

const getValue = (option: string | OptionShape) =>
    typeof option === 'object' && 'key' in option && 'value' in option ? option.value : option;

export const Select = forwardRef<HTMLDivElement, SelectProps>(
    (
        {
            name,
            multiple,
            field,
            setFieldValue,
            options,
            error,
            onChange,
            onBlur,
            selected,
            hideClearButton = false,
            onClear,
            ...props
        },
        ref
    ) => {
        const clearProps = useSelectClear({
            onClearClick:
                onClear ||
                (() => {
                    setTimeout(() => {
                        setFieldValue?.(null);
                    }, 0);
                }),
            disabled: props.disabled,
        });

        const isValueArray = Array.isArray(field?.value);

        const selectedValues = useMemo(() => {
            const selectedProps =
                !Array.isArray(selected) && selected
                    ? [getValue(selected)]
                    : (selected as Array<string | OptionShape>)?.map(getValue) || [];

            if (multiple) return Array.isArray(field?.value) ? field?.value || [] : selectedProps;
            if (field?.value === undefined) return selectedProps;

            return Array.isArray(field?.value) ? field.value : [field?.value];
        }, [field?.value, multiple, selected]);

        const selectedOptions = useMemo(
            () =>
                options.filter(e => {
                    if ('value' in e) {
                        return selectedValues.includes(e.value);
                    }
                    return false;
                }) as OptionShape[],
            [options, selectedValues]
        );

        return (
            <SimpleSelect
                ref={ref}
                name={name}
                options={options}
                {...(!hideClearButton && { ...clearProps })}
                {...props}
                multiple={multiple}
                error={error}
                selected={selectedOptions}
                onChange={payload => {
                    onChange?.(payload);

                    if (typeof setFieldValue === 'function') {
                        const value =
                            multiple || isValueArray
                                ? payload.selectedMultiple.map(e => e.value)
                                : payload.selected?.value;

                        setFieldValue(value);
                    }
                }}
                onBlur={onBlur}
                fieldProps={
                    {
                        // fieldState,
                    }
                }
            />
        );
    }
);

Select.displayName = 'Select';
