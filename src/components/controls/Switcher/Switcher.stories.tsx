import { Form } from '@ensi-platform/core-components';
import { action } from '@storybook/addon-actions';
import type { Meta, StoryObj } from '@storybook/react';
import { ComponentProps } from 'react';
import * as Yup from 'yup';

import { Button, scale } from '@scripts/gds';

import Switcher from '.';
import README from './README.md';

export default {
    title: 'Controls / Form / Switcher',
    parameters: {
        docs: {
            description: {
                component: README,
            },
        },
    },
    component: Switcher,
} as Meta<typeof Switcher>;

export const Basic: StoryObj<
    ComponentProps<typeof Switcher> & {
        maxItems: number;
    }
> = {
    args: {
        maxItems: 3,
    },
    argTypes: {},
    render: args => {
        const initialValues = {
            checkboxKnobs: [],
        };

        const validationSchema = Yup.object().shape({
            checkboxKnobs: Yup.array().max(args.maxItems, 'Превышен лимит').required('Обязательное поле'),
        });

        return (
            <Form
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={action('Submit')}
                css={{ width: '25vw', minWidth: scale(40) }}
            >
                {/* CheckboxGroup was removed in ensitech-168
                <FormFieldWrapper name="checkboxKnobs">
                    <CheckboxGroup label="Выберите вариант" hint="Подсказка">
                        <Switcher value="first">Вариант 1</Switcher>
                        <Switcher value="second">Вариант 2</Switcher>
                        <Switcher value="third">Вариант 3</Switcher>
                        <Switcher value="fourth" disabled>
                            Вариант 4
                        </Switcher>
                    </CheckboxGroup>
                </FormFieldWrapper> */}
                <Switcher name="checkboxLink">
                    Я прочитал и принимаю{' '}
                    <a href="/" style={{ textDecoration: 'underline' }}>
                        Пользовательское соглашение
                    </a>{' '}
                    и{' '}
                    <a href="/" style={{ textDecoration: 'underline' }}>
                        Согласие на обработку персональных данных
                    </a>
                </Switcher>
                <br />
                <Button type="submit">Submit</Button>
            </Form>
        );
    },
};
