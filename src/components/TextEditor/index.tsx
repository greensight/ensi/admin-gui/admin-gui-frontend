import { IFieldWrapperProps } from '@ensi-platform/core-components';
import { Editor } from '@tinymce/tinymce-react';
import { useRouter } from 'next/router';
import { useState } from 'react';

import Legend from '@controls/Legend';
import LoadingSkeleton from '@controls/LoadingSkeleton';

import { colors, scale } from '@scripts/gds';

type TextEditorProps = {
    label?: string;
    name?: string;
    disabled?: boolean;
} & Partial<IFieldWrapperProps<string>>;

export const TextEditor = ({ name, field, setFieldValue, disabled = false, error, label }: TextEditorProps) => {
    const router = useRouter();
    const shouldRenderLegend = label;

    const textValue = field?.value;
    const onChange = field ? (newValue: string) => setFieldValue?.(newValue) : undefined;
    const [isLoaded, setLoaded] = useState(false);
    return (
        <>
            {shouldRenderLegend && <Legend label={label} error={error} />}
            {!isLoaded && <LoadingSkeleton height={scale(36)} />}
            <div
                css={{
                    ...(!isLoaded && {
                        display: 'none',
                    }),
                    [`textarea[name="${field?.name || name}"] ~ div`]: {
                        borderWidth: 1,
                        ...(error && {
                            borderColor: colors.danger,
                        }),
                    },
                }}
            >
                <Editor
                    // https://www.tiny.cloud/docs/tinymce/latest/react-pm-host/
                    tinymceScriptSrc={`${router.basePath}/assets/libs/tinymce/tinymce.min.js`}
                    licenseKey="gpl"
                    onEditorChange={onChange}
                    value={textValue}
                    onLoadContent={() => setLoaded(true)}
                    init={{
                        promotion: false,
                        menubar: false,
                        plugins: 'link image code anchor lists',
                        toolbar:
                            'undo redo | h1 h2 h3 h4 | bold italic | bullist numlist| forecolor backcolor | alignleft aligncenter alignright alignjustify | link anchor | image | code',
                    }}
                    textareaName={field?.name || name}
                    disabled={disabled}
                />
            </div>
        </>
    );
};

export default TextEditor;
