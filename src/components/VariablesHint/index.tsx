import { CopyButton } from '@ensi-platform/core-components';

import { VariablesResponseVariables } from '@api/communications/notificationSettings';

import Tooltip from '@controls/Tooltip';

import { Button, Layout, scale, typography } from '@scripts/gds';

import HelpIcon from '@icons/20/info.svg';

export interface VariablesHintProps {
    label: string;
    items: VariablesResponseVariables[];
}

const VariableItem = ({ id, title, items = [], isRoot }: VariablesResponseVariables & { isRoot?: boolean }) => (
    <Layout type="grid" cols={2} gap={[scale(1), scale(1, true)]}>
        <Layout.Item
            css={{
                fontWeight: isRoot || items.length ? 700 : 400,
            }}
        >
            {title}
        </Layout.Item>
        <Layout.Item>
            <CopyButton
                css={{
                    ...typography('smallBold'),
                    fontStyle: 'italic',
                    textAlign: 'left',
                    whiteSpace: 'nowrap',
                }}
            >
                {id}
            </CopyButton>
        </Layout.Item>
        <Layout.Item
            col={2}
            css={{
                paddingLeft: scale(2),
            }}
        >
            {(items || []).map(item => (
                <VariableItem key={item.title} id={item.id} title={item.title} items={item.items} />
            ))}
        </Layout.Item>
    </Layout>
);

const VariablesHint = ({ label, items }: VariablesHintProps) => (
    <Tooltip
        minWidth={scale(50)}
        theme="light"
        content={
            <div
                css={{
                    display: 'flex',
                    flexDirection: 'column',
                    paddingLeft: scale(1),
                    paddingRight: scale(2),
                }}
            >
                <p css={{ ...typography('caption'), marginBottom: scale(2) }}>{label}</p>
                <div
                    css={{
                        overflowY: 'auto',
                        maxHeight: scale(40),
                    }}
                >
                    {items.map(item => (
                        <VariableItem key={item.id} {...item} isRoot />
                    ))}
                </div>
            </div>
        }
    >
        <Button size="sm" Icon={HelpIcon} hidden theme="ghost" css={{ width: 'fit-content' }} block={false}>
            помощь
        </Button>
    </Tooltip>
);

export default VariablesHint;
