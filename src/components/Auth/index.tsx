import { Form, FormField, FormFieldWrapper } from '@ensi-platform/core-components';
import { useMemo } from 'react';
import * as Yup from 'yup';

import Password from '@controls/Password';

import { ErrorMessages } from '@scripts/constants';
import { Button, scale, typography, useTheme } from '@scripts/gds';

interface AuthProps {
    logIn: (values: { login: string; password: string }) => void;
    isLoading?: boolean;
}

const Auth = ({ logIn, isLoading }: AuthProps) => {
    const { shadows, colors } = useTheme();

    const numberValidationWithLength = useMemo(() => Yup.string().required(ErrorMessages.REQUIRED), []);

    return (
        <div
            css={{
                minHeight: '100vh',
                display: 'grid',
                placeItems: 'center',
            }}
        >
            <main
                css={{
                    width: scale(45),
                    boxShadow: shadows?.big,
                    padding: `${scale(3)}px ${scale(4)}px`,
                    backgroundColor: colors?.white,
                }}
            >
                <h1 css={{ ...typography('h1'), textAlign: 'center', marginTop: 0 }}>Авторизация</h1>
                <Form
                    onSubmit={({ login, password }) => logIn({ login, password })}
                    initialValues={{ login: '', password: '' }}
                    validationSchema={Yup.object().shape({
                        login: numberValidationWithLength,
                        password: Yup.string().required(ErrorMessages.REQUIRED).min(2, ErrorMessages.MIN_SYMBOLS(2)),
                    })}
                >
                    <FormField name="login" label="Логин" css={{ marginBottom: scale(2) }} type="email" />
                    <FormFieldWrapper
                        name="password"
                        label="Пароль"
                        css={{ marginBottom: scale(4) }}
                        autoComplete="off"
                    >
                        <Password autoComplete="password" />
                    </FormFieldWrapper>
                    <Button type="submit" block disabled={isLoading}>
                        Войти
                    </Button>
                </Form>
            </main>
        </div>
    );
};

export default Auth;
