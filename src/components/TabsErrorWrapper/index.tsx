import { useFormContext } from '@ensi-platform/core-components';
import { ReactNode, cloneElement, isValidElement } from 'react';

import { useTabsErrors } from './hooks/useTabsErrors';

const TabsErrorWrapper = ({
    fieldNamesByTabs,
    children,
}: {
    fieldNamesByTabs: { id: string; fields: string[] }[];
    children: ReactNode;
}) => {
    const rhfContext = useFormContext();

    const countErrorsFhf = useTabsErrors(rhfContext?.formState?.errors, fieldNamesByTabs);

    if (isValidElement(children)) {
        return cloneElement(children, {
            ...children?.props,
            countErrors: countErrorsFhf,
        });
    }
};

export default TabsErrorWrapper;
