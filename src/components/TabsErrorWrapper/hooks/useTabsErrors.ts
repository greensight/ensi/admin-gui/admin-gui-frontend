import { useMemo } from 'react';

export const useTabsErrors = (errors: Record<string, any>, fieldNames?: { id: string; fields: string[] }[]) =>
    useMemo(() => {
        if (!errors || !fieldNames) return [];
        const errorKeys = Object.keys(errors);

        return fieldNames.map(item => ({
            id: item.id,
            count: errorKeys.reduce((acc, cur) => {
                function checkValue(el: any): number {
                    if (Array.isArray(el)) {
                        return el
                            .map((inner: any) => checkValue(inner))
                            .reduce((inner_acc, inner_cur) => inner_acc + inner_cur, 0);
                    }
                    if (typeof el === 'object') {
                        const elKeys = Object.keys(el);

                        return elKeys
                            .map(key => checkValue(el[key]))
                            .reduce((inner_acc, inner_cur) => inner_acc + inner_cur, 0);
                    }
                    return 1;
                }

                if (item.fields.includes(cur)) {
                    return acc + checkValue(errors[cur]);
                }

                return acc;
            }, 0),
        }));
    }, [errors, fieldNames]);
