import { useWatch } from '@ensi-platform/core-components';
import { Dispatch, FC, SetStateAction, useEffect, useMemo } from 'react';

interface FilterOrderHelperProps {
    setFilterOrder: Dispatch<SetStateAction<string[]>>;
}

export const FilterOrderHelper: FC<FilterOrderHelperProps> = ({ setFilterOrder }) => {
    const values = useWatch();

    const filtersFromValues = useMemo(() => Object.keys(values).filter(key => values[key]), [values]);

    useEffect(() => {
        setFilterOrder(prevOrder => {
            const prevOrderFiltered = prevOrder.filter(i => filtersFromValues.includes(i));
            const nextOrderFiltered = filtersFromValues.filter(i => !prevOrderFiltered.includes(i));
            return [...prevOrderFiltered, ...nextOrderFiltered];
        });
    }, [filtersFromValues, setFilterOrder]);
    return null;
};
