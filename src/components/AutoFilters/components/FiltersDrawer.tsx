import {
    Drawer,
    DrawerContent,
    DrawerFooter,
    DrawerHeader,
    Form,
    FormCheckbox,
    FormFieldWrapper,
    Tabs,
} from '@ensi-platform/core-components';
import { DragDropContext, Draggable, Droppable } from '@hello-pangea/dnd';

import { Button, Layout, scale } from '@scripts/gds';

import DragIcon from '@icons/small/dragAndDrop.svg';

import { decodeFieldName, encodeFieldName } from '../helper';
import { useFiltersDrawerLocalHelper } from '../hooks';
import { IFiltersDrawerProps } from '../types';
import { FilterOrderHelper } from './FilterOrderHelper';

const FiltersDrawer = ({
    isOpen,
    closeHandler,
    filtersSettings,
    setFiltersSettings,
    filters,
    filtersObject,
}: IFiltersDrawerProps) => {
    const { formStyles, ulStyles, liStyles, initialValues, filterOrder, setFilterOrder, onDragEnd } =
        useFiltersDrawerLocalHelper({
            filtersSettings,
            filters,
        });

    return (
        <Drawer open={isOpen} onClose={closeHandler} placement="left">
            <DrawerHeader title="Настройка фильтров" onClose={closeHandler} hasCloseButton />

            <Form
                initialValues={initialValues}
                enableReinitialize
                onSubmit={() => {
                    const decodedFilters = filterOrder.map(decodeFieldName);
                    setFiltersSettings(decodedFilters);
                }}
                css={formStyles}
            >
                <DrawerContent>
                    <FilterOrderHelper setFilterOrder={setFilterOrder} />

                    <Tabs>
                        <Tabs.Tab title="Показывать" id="0">
                            <Layout cols={1} gap={scale(2)}>
                                {filters?.map(f => (
                                    <Layout.Item key={f.code}>
                                        <FormFieldWrapper name={encodeFieldName(f.code)}>
                                            <FormCheckbox>{f.name}</FormCheckbox>
                                        </FormFieldWrapper>
                                    </Layout.Item>
                                ))}
                            </Layout>
                        </Tabs.Tab>

                        <Tabs.Tab title="Сортировать" id="1">
                            <DragDropContext onDragEnd={onDragEnd}>
                                <Droppable droppableId="column-order">
                                    {droppableProps => (
                                        <ul
                                            ref={droppableProps.innerRef}
                                            {...droppableProps.droppableProps}
                                            css={ulStyles}
                                        >
                                            {filterOrder.map((filter, index) => (
                                                <Draggable key={filter} draggableId={filter} index={index}>
                                                    {(provided, snapshot) => (
                                                        <li
                                                            ref={provided.innerRef}
                                                            {...provided.draggableProps}
                                                            {...provided.dragHandleProps}
                                                            css={{
                                                                ...liStyles,
                                                                ...(snapshot.isDragging && {
                                                                    svg: { opacity: 1 },
                                                                }),
                                                            }}
                                                        >
                                                            <DragIcon />
                                                            {filtersObject[decodeFieldName(filter)]}
                                                        </li>
                                                    )}
                                                </Draggable>
                                            ))}

                                            {droppableProps.placeholder}
                                        </ul>
                                    )}
                                </Droppable>
                            </DragDropContext>
                        </Tabs.Tab>
                    </Tabs>
                </DrawerContent>

                <DrawerFooter>
                    <Button type="button" theme="fill" block onClick={closeHandler}>
                        Отменить
                    </Button>

                    <Button type="submit" block onClick={closeHandler}>
                        Сохранить
                    </Button>
                </DrawerFooter>
            </Form>
        </Drawer>
    );
};

export default FiltersDrawer;
