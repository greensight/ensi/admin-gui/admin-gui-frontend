import { Block } from '@ensi-platform/core-components';

import LoadingSkeleton from '@controls/LoadingSkeleton';

import { Button, Layout, scale } from '@scripts/gds';

import FilterIcon from '@icons/small/sliders.svg';

const SkeletonBlock = ({ className }: { className?: string }) => (
    <Block className={className}>
        <Block.Body css={{ borderRadius: 0 }}>
            <Layout cols={{ xxxl: 4, md: 3, sm: 2, xs: 1 }}>
                {[1, 2, 3, 4].map(e => (
                    <Layout.Item key={e}>
                        <LoadingSkeleton css={{ width: '100%' }} height={scale(4, false, 15)} />
                    </Layout.Item>
                ))}
            </Layout>
        </Block.Body>

        <Block.Footer>
            <Button theme="secondary" Icon={FilterIcon} disabled>
                Настройка фильтров
            </Button>

            <Button theme="primary" css={{ marginLeft: scale(2) }} type="submit" disabled>
                Применить
            </Button>
        </Block.Footer>
    </Block>
);

export default SkeletonBlock;
