import { DropResult } from '@hello-pangea/dnd';
import { useCallback, useMemo, useState } from 'react';
import type { FieldValues } from 'react-hook-form';

import { MetaField } from '@api/common/types';

import { encodeFieldName } from '../helper';
import { FiltersDrawerLocalHelper } from '../types';
import { useDrawerCSS } from './useDrawerCSS';

export const useFiltersDrawerLocalHelper = ({ filtersSettings, filters }: FiltersDrawerLocalHelper) => {
    const { formStyles, ulStyles, liStyles } = useDrawerCSS();

    const [filterOrder, setFilterOrder] = useState<string[]>(filtersSettings);

    /** react beautiful dnd callbacks */
    const reorderItems = useCallback(
        (startIndex: number, endIndex: number) => {
            const newFiltersOrder = filterOrder.slice();
            const [movedItem] = newFiltersOrder.splice(startIndex, 1);
            newFiltersOrder.splice(endIndex, 0, movedItem);
            setFilterOrder(newFiltersOrder);
        },
        [filterOrder]
    );

    const onDragEnd = useCallback(
        ({ source, destination }: DropResult) => {
            if (!destination || (destination.index === source.index && destination.droppableId === source.droppableId))
                return;
            reorderItems(source.index, destination.index);
        },
        [reorderItems]
    );

    const initialValues = useMemo(
        () =>
            filters?.reduce((acc, f) => {
                acc[encodeFieldName(f.code)] = Boolean(filtersSettings?.includes(f.code));
                return acc;
            }, {} as FieldValues) || {},
        [filters, filtersSettings]
    );

    const filtersOrdered = useMemo(() => {
        if (!filters) return [];
        const [activeFilters, inactiveFilters] = filters.reduce(
            (acc, cur) => {
                if (filterOrder.includes(encodeFieldName(cur.code))) {
                    acc[0].push(cur);
                } else {
                    acc[1].push(cur);
                }
                return acc;
            },
            [[], []] as MetaField[][]
        );
        return [
            ...activeFilters.sort(
                (a, b) => filterOrder.indexOf(encodeFieldName(a.code)) - filterOrder.indexOf(encodeFieldName(b.code))
            ),
            ...inactiveFilters,
        ];
    }, [filters, filterOrder]);

    return { initialValues, filterOrder, filtersOrdered, setFilterOrder, onDragEnd, formStyles, ulStyles, liStyles };
};
