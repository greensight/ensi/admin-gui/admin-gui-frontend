import { ReactNode } from 'react';

// eslint-disable-next-line import/no-cycle
import PageWrapper from '@components/PageWrapper';

import { typography } from '@scripts/gds';

const ForbiddenPage = ({
    title = 'Недостаточно прав',
    message = 'У вас недостаточно прав для просмотра страницы',
}: {
    message?: ReactNode | string;
    title?: string;
}) => (
    <PageWrapper title={title} h1="">
        <h1 css={{ ...typography('h1'), textAlign: 'center' }}>{message}</h1>
    </PageWrapper>
);

export default ForbiddenPage;
