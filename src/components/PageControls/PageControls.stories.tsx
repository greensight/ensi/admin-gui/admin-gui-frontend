/* eslint-disable react-hooks/rules-of-hooks */
import { Form, FormField } from '@ensi-platform/core-components';
import { action } from '@storybook/addon-actions';
import type { Meta, StoryObj } from '@storybook/react';
import { ComponentProps } from 'react';

import README from './README.md';
import PageControls from './index';

export default {
    title: 'Components / PageControls',
    component: PageControls,
    parameters: {
        docs: {
            description: {
                component: README,
            },
        },
    },
} as Meta<typeof PageControls>;

export const Basic: StoryObj<ComponentProps<typeof PageControls> & {}> = {
    args: {
        access: {
            LIST: {
                view: true,
            },
            ID: {
                create: true,
                delete: true,
                edit: true,
                view: true,
            },
        },
    },
    parameters: {
        docs: {
            description: {
                story: 'PageControls',
            },
        },
    },
    render: args => (
        <Form initialValues={{ someField: '' }} onSubmit={action('submit')}>
            <PageControls gap={8} {...args}>
                <PageControls.Delete onClick={action('delete')} />
                <PageControls.Close onClick={action('close')} />
                <PageControls.Apply onClick={action('stay')} />
                <PageControls.Save onClick={action('return back')} />
            </PageControls>

            <FormField name="someField" label="Поле" />
        </Form>
    ),
};
