import { PageControlsButton, PageControlsButtonProps } from './Button';
import { PageControlChildProps } from './types';

export interface PageControlsApplyProps
    extends Omit<PageControlsButtonProps, 'type' | 'dirtyOnly' | 'children'>,
        PageControlChildProps {}

export const PageControlsApply = ({
    onClick,
    theme = 'secondary',
    iconAfter = true,
    ...props
}: PageControlsApplyProps) => (
    <PageControlsButton theme={theme} iconAfter={iconAfter} onClick={onClick} {...props} dirtyOnly type="submit">
        Применить
    </PageControlsButton>
);
