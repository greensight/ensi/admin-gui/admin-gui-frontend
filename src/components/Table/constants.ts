import { TooltipItem } from './types';

export const TABLE_STORAGE_KEYS = {
    HIDDEN_COLS: '_HiddenColumns',
    ORDER_COLS: '_OrderColumns',
};

export const DEFAULT_COLUMNS_TO_IGNORE = ['settings', 'select'];
export const DEFAULT_VISIBLE_COLUMNS: string[] = [];
export const DEFAULT_COLUMNS_TO_DISABLE: string[] = ['id'];
export const DEFAULT_TOOLTIP_CONTENT: TooltipItem[] = [];
