/* eslint-disable react-hooks/rules-of-hooks */
import { Drawer, DrawerContent, DrawerFooter, DrawerHeader, Form } from '@ensi-platform/core-components';
import { useMemo, useState } from 'react';
import { followCursor } from 'tippy.js';

import Tooltip, { ContentBtn } from '@controls/Tooltip';

import { encodeFieldName } from '@components/AutoFilters/helper';

import { Button, Layout, scale } from '@scripts/gds';
import { useLocalStorage } from '@scripts/hooks';

import KebabIcon from '@icons/small/kebab.svg';
import SettingsIcon from '@icons/small/settings.svg';
import TipIcon from '@icons/small/status/tip.svg';

import SettingsForm from '../components/SettingsForm';
import {
    DEFAULT_COLUMNS_TO_DISABLE,
    DEFAULT_COLUMNS_TO_IGNORE,
    DEFAULT_TOOLTIP_CONTENT,
    DEFAULT_VISIBLE_COLUMNS,
    TABLE_STORAGE_KEYS,
} from '../constants';
import { IColumnSettings, TableColumnDefAny, TooltipItem } from '../types';

export interface IGetSettingsColumnProps {
    /** Need to show settings button flag */
    hasHeader?: boolean;

    /** Show cell */
    hasCell?: boolean;

    /** On settings click callback */
    onSettingsClick?: () => void;

    /** Columns to not show in popup. Array of column's ids */
    columnsToIgnore?: string[];

    /** Columns to show in table */
    visibleColumns?: string[];

    /** Columns to disable switch off in popup. Array of column's ids */
    columnsToDisable?: string[];

    /** Items that are displayed in each row on kebab button click */
    tooltipContent?: TooltipItem[];
}

export const getSettingsColumn = ({
    hasHeader = true,
    hasCell = true,
    onSettingsClick,
    columnsToIgnore = DEFAULT_COLUMNS_TO_IGNORE,
    visibleColumns = DEFAULT_VISIBLE_COLUMNS,
    columnsToDisable = DEFAULT_COLUMNS_TO_DISABLE,
    tooltipContent = DEFAULT_TOOLTIP_CONTENT,
}: IGetSettingsColumnProps): TableColumnDefAny => ({
    id: 'settings',

    header: ({ table }) => {
        const tableKey = table.options.meta?.tableKey;

        const [isOpen, setIsOpen] = useState(false);
        const onClose = () => setIsOpen(false);

        const [, setColumnsToHide] = useLocalStorage<Omit<IColumnSettings, 'isActive'>[]>(
            `${tableKey}${TABLE_STORAGE_KEYS.HIDDEN_COLS}`,
            []
        );

        const [, setColumnsOrderByUser] = useLocalStorage<string[]>(`${tableKey}${TABLE_STORAGE_KEYS.ORDER_COLS}`, []);

        const allColumns = table.getAllLeafColumns();

        const initialValues = useMemo(
            () =>
                allColumns.reduce(
                    (acc, column) => {
                        acc[encodeFieldName(column.id)] = column.getIsVisible();
                        return acc;
                    },
                    {} as Record<string, boolean>
                ),
            [allColumns]
        );

        const [columns, setColumns] = useState<IColumnSettings[]>(() =>
            allColumns
                .filter(c => !columnsToIgnore.includes(c.id))
                .map((c, i) => ({ order: i, id: c.id, isActive: c.getIsVisible() }))
        );

        return hasHeader ? (
            <div css={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center', height: '100%' }}>
                <Button
                    type="button"
                    theme="ghost"
                    hidden
                    size="sm"
                    Icon={SettingsIcon}
                    onClick={() => {
                        if (onSettingsClick) onSettingsClick();
                        setIsOpen(true);
                    }}
                    css={{
                        background: 'inherit !important',
                        paddingTop: '0 !important',
                        paddingBottom: '0 !important',
                    }}
                >
                    Управлять количеством столбцов
                </Button>

                <Drawer open={isOpen} onClose={onClose}>
                    <DrawerHeader title="Настройка столбцов" onClose={onClose} hasCloseButton />
                    <Form
                        initialValues={initialValues}
                        onChange={values => {
                            setColumns(columns.map(c => ({ ...c, isActive: values[c.id] })));
                        }}
                        onSubmit={values => {
                            setColumnsToHide(
                                columns
                                    .filter(c => !c.isActive)
                                    .map(c => ({
                                        id: c.id,
                                        order: c.order,
                                    }))
                            );

                            setColumnsOrderByUser(columns.map(c => c.id));

                            table.setColumnOrder(() => ['select', ...columns.map(c => c.id), 'settings']);

                            allColumns.forEach(c => {
                                c.toggleVisibility(values[c.id]);
                            });

                            onClose();
                        }}
                        css={{ display: 'flex', flexDirection: 'column', height: '100%' }}
                    >
                        <DrawerContent>
                            <SettingsForm
                                columnsToIgnore={columnsToIgnore}
                                visibleColumns={visibleColumns}
                                allColumns={allColumns}
                                columnsToDisable={columnsToDisable}
                                table={table}
                                columns={columns}
                                setColumns={setColumns}
                            />
                        </DrawerContent>
                        <DrawerFooter>
                            <Button theme="fill" block onClick={onClose} type="button">
                                Отменить
                            </Button>
                            <Button type="submit" block>
                                Сохранить
                            </Button>
                        </DrawerFooter>
                    </Form>
                </Drawer>
            </div>
        ) : null;
    },
    cell: ({ cell }) => {
        if (!hasCell) return null;
        const [visible, setVisible] = useState(false);
        return (
            <div css={{ display: 'flex', justifyContent: 'flex-end' }}>
                <Tooltip
                    content={
                        <ul>
                            {tooltipContent.map(t => {
                                const isDisabled =
                                    typeof t?.isDisable === 'function'
                                        ? t.isDisable(cell.row ? [cell.row] : undefined)
                                        : t?.isDisable;
                                return (
                                    <li key={t.text}>
                                        <ContentBtn
                                            type={t.type}
                                            onClick={async e => {
                                                e.stopPropagation();

                                                await Promise.resolve(t.action(cell.row ? [cell.row] : undefined));
                                                setVisible(false);
                                            }}
                                            css={{
                                                display: 'inline-flex',
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                            }}
                                            disabled={isDisabled}
                                        >
                                            <Layout
                                                cols={isDisabled && t.disabledHint ? [1, `${scale(2)}px`] : 0}
                                                gap={isDisabled && t.disabledHint ? scale(2) : 0}
                                                align="center"
                                                css={{ width: '100%' }}
                                            >
                                                <Layout.Item>{t.text}</Layout.Item>
                                                {isDisabled && t.disabledHint && (
                                                    <Layout.Item
                                                        align="center"
                                                        justify="end"
                                                        css={{ width: scale(2), height: scale(2) }}
                                                    >
                                                        <Tooltip content={t.disabledHint} arrow>
                                                            <button
                                                                type="button"
                                                                css={{
                                                                    verticalAlign: 'middle',
                                                                    paddingBottom: scale(1, true),
                                                                }}
                                                            >
                                                                <TipIcon />
                                                            </button>
                                                        </Tooltip>
                                                    </Layout.Item>
                                                )}
                                            </Layout>
                                        </ContentBtn>
                                    </li>
                                );
                            })}
                        </ul>
                    }
                    plugins={[followCursor]}
                    followCursor="initial"
                    arrow
                    theme="light"
                    placement="bottom"
                    minWidth={scale(36)}
                    disabled={tooltipContent.length === 0}
                    appendTo={() => document.body}
                    visible={visible}
                    onClickOutside={() => setVisible(false)}
                >
                    <Button
                        type="button"
                        theme="ghost"
                        hidden
                        size="sm"
                        onClick={() => {
                            setVisible(true);
                        }}
                        Icon={KebabIcon}
                        css={{ ':hover': { background: 'inherit !important' } }}
                    >
                        Вызвать контекстное меню
                    </Button>
                </Tooltip>
            </div>
        );
    },
});
