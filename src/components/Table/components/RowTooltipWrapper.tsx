import { Row } from '@tanstack/react-table';
import { FC, MouseEventHandler, ReactNode, useEffect, useState } from 'react';
import { followCursor } from 'tippy.js';

import Tooltip from '@controls/Tooltip';
import { ContentBtn } from '@controls/Tooltip/ContentBtn';

import { Layout, scale } from '@scripts/gds';

import TipIcon from '@icons/small/status/tip.svg';

import { TooltipItem, TrProps } from '../types';
import { DefaultTr } from './utils';

const RowTooltipWrapper = ({
    Tr = DefaultTr,
    row,
    getTooltipForRow,
    children,
    onDoubleClick,
}: {
    row?: Row<any>;
    children: ReactNode | ReactNode[];
    getTooltipForRow: (row: Row<any>) => TooltipItem[];
    onDoubleClick?: MouseEventHandler<HTMLTableRowElement>;
    Tr?: FC<TrProps<any>>;
}) => {
    const [visible, setVisible] = useState(false);

    useEffect(() => {
        const callback = (e: KeyboardEvent) => {
            if (e.key === 'Escape') setVisible(false);
        };
        if (visible) {
            document.addEventListener('keydown', callback);
        }
        return () => {
            document.removeEventListener('keydown', callback);
        };
    }, [setVisible, visible]);

    const tooltipContent = getTooltipForRow(row!);

    return (
        <Tooltip
            content={
                <ul>
                    {tooltipContent.map(t => (
                        <li key={t.text}>
                            <ContentBtn
                                type={t.type}
                                onClick={async e => {
                                    e.stopPropagation();

                                    await Promise.resolve(t.action(row ? [row] : undefined));
                                    setVisible(false);
                                }}
                                css={{ display: 'inline-flex' }}
                                disabled={
                                    typeof t?.isDisable === 'function'
                                        ? t.isDisable(row ? [row] : undefined)
                                        : t?.isDisable
                                }
                            >
                                <Layout
                                    cols={t.isDisable && t.disabledHint ? [1, `${scale(2)}px`] : 0}
                                    gap={t.isDisable && t.disabledHint ? 0 : scale(2)}
                                    align="center"
                                    css={{ width: '100%' }}
                                >
                                    <Layout.Item>{t.text}</Layout.Item>
                                    {t.isDisable && t.disabledHint && (
                                        <Layout.Item
                                            align="end"
                                            justify="end"
                                            css={{ width: scale(2), height: scale(2) }}
                                        >
                                            <Tooltip content={t.disabledHint} arrow>
                                                <button
                                                    type="button"
                                                    css={{
                                                        verticalAlign: 'middle',
                                                        paddingBottom: scale(1, true),
                                                    }}
                                                >
                                                    <TipIcon />
                                                </button>
                                            </Tooltip>
                                        </Layout.Item>
                                    )}
                                </Layout>
                            </ContentBtn>
                        </li>
                    ))}
                </ul>
            }
            plugins={[followCursor]}
            followCursor="initial"
            arrow
            theme="light"
            placement="bottom"
            minWidth={scale(36)}
            disabled={tooltipContent.length === 0}
            appendTo={() => document.body}
            visible={visible}
            onClickOutside={() => setVisible(false)}
        >
            <Tr
                row={row}
                onContextMenu={e => {
                    e.preventDefault();
                    setVisible(true);
                }}
                onDoubleClick={onDoubleClick}
            >
                {children}
            </Tr>
        </Tooltip>
    );
};

export default RowTooltipWrapper;
