import { FormCheckbox, FormFieldWrapper, Tabs } from '@ensi-platform/core-components';
import { DragDropContext, Draggable, DropResult, Droppable } from '@hello-pangea/dnd';
import { Column, Header, Table, flexRender } from '@tanstack/react-table';
import { Dispatch, ReactNode, SetStateAction, useCallback, useMemo } from 'react';

import { decodeFieldName, encodeFieldName } from '@components/AutoFilters/helper';

import { colors, scale } from '@scripts/gds';

import DragIcon from '@icons/small/dragAndDrop.svg';

import { IColumnSettings } from '../types';

const SettingsForm = ({
    table,
    columnsToIgnore,
    visibleColumns,
    allColumns,
    columnsToDisable,
    columns,
    setColumns,
}: {
    table: Table<any>;
    columnsToIgnore: string[];
    visibleColumns: string[];
    allColumns: Column<any>[];
    columnsToDisable: string[];
    columns: IColumnSettings[];
    setColumns: Dispatch<SetStateAction<IColumnSettings[]>>;
}) => {
    const editableColumns = useMemo(
        () => allColumns.filter(c => !columnsToIgnore.includes(c.id) || visibleColumns.includes(c.id)),
        [allColumns, columnsToIgnore, visibleColumns]
    );

    const colsToText = useMemo(
        () =>
            allColumns.reduce(
                (acc, column) => {
                    acc[column.id] =
                        flexRender(column.columnDef.header, {
                            column: { ...column, columnDef: { ...column.columnDef, enableSorting: false } },
                            table,
                            header: null as unknown as Header<any, unknown>,
                        }) || column.id;
                    return acc;
                },
                {} as Record<string, ReactNode>
            ),
        [allColumns, table]
    );

    /** react beautiful dnd callbacks */
    const reorderItems = useCallback(
        (startIndex: number, endIndex: number) => {
            const newColumnOrder = [...columns];
            const [movedItem] = newColumnOrder.splice(startIndex, 1);
            newColumnOrder.splice(endIndex, 0, movedItem);
            setColumns(newColumnOrder.map((c, i) => ({ ...c, order: i })));
        },
        [setColumns, columns]
    );

    const onDragEnd = useCallback(
        ({ source, destination }: DropResult) => {
            if (!destination || (destination.index === source.index && destination.droppableId === source.droppableId))
                return;
            reorderItems(source.index, destination.index);
        },
        [reorderItems]
    );

    return (
        <Tabs>
            <Tabs.Tab title="Показывать" id="show">
                <ul
                    css={{
                        li: {
                            ':not(:last-of-type)': { marginBottom: scale(2) },
                        },
                        paddingTop: scale(3, true),
                    }}
                >
                    {editableColumns.map(column => (
                        <li key={encodeFieldName(column.id)}>
                            <FormFieldWrapper name={encodeFieldName(column.id)}>
                                <FormCheckbox disabled={columnsToDisable.includes(column.id)}>
                                    {flexRender(column.columnDef.header, {
                                        column: {
                                            ...column,
                                            columnDef: {
                                                ...column.columnDef,
                                                enableSorting: false,
                                            },
                                        },
                                        header: null as unknown as Header<any, unknown>,
                                        table,
                                    })}
                                </FormCheckbox>
                            </FormFieldWrapper>
                        </li>
                    ))}
                </ul>
            </Tabs.Tab>
            <Tabs.Tab title="Сортировать" id="sort">
                <DragDropContext onDragEnd={onDragEnd}>
                    <Droppable droppableId="column-order">
                        {droppableProps => (
                            <ul
                                ref={droppableProps.innerRef}
                                {...droppableProps.droppableProps}
                                css={{ position: 'relative' }}
                            >
                                {columns?.map(column => (
                                    <Draggable key={column.id} draggableId={column.id} index={column.order}>
                                        {(provided, snapshot) => (
                                            <li
                                                ref={provided.innerRef}
                                                {...provided.draggableProps}
                                                {...provided.dragHandleProps}
                                                css={{
                                                    display: 'flex',
                                                    alignItems: 'center',
                                                    gap: scale(1),
                                                    padding: `${scale(1)}px 0`,
                                                    svg: { opacity: 0 },
                                                    '&:hover': { svg: { opacity: 1 } },
                                                    backgroundColor: colors.white,
                                                    left: `${scale(3)}px !important`,
                                                    ...(snapshot.isDragging && {
                                                        svg: { opacity: 1 },
                                                    }),
                                                }}
                                            >
                                                <DragIcon />
                                                {colsToText[decodeFieldName(column.id)]}
                                            </li>
                                        )}
                                    </Draggable>
                                ))}

                                {droppableProps.placeholder}
                            </ul>
                        )}
                    </Droppable>
                </DragDropContext>
            </Tabs.Tab>
        </Tabs>
    );
};

export default SettingsForm;
