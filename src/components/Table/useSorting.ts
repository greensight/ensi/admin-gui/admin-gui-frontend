import { ColumnSort, RowData, SortingState, getSortedRowModel } from '@tanstack/react-table';
import { SetStateAction, useCallback, useEffect, useMemo, useState } from 'react';
import deepEqual from 'react-fast-compare';

import { MetaField } from '@api/common/types';

import { usePrevious, useSessionStorage } from '@scripts/hooks';

import { TablePlugin } from './types';

export const useSorting = <TData extends RowData>(
    key: string,
    metaFields: MetaField[] | [] | undefined,
    initialSort?: ColumnSort,
    manualSorting = true,
    reinitialize = false
) => {
    const [savedSort, setSavedSort] = useSessionStorage(`${key}Sort`, () => (initialSort ? [initialSort] : []));
    const [sorting, setSorting] = useState<SortingState>(savedSort);

    const prevInitialSort = usePrevious(initialSort);
    useEffect(() => {
        if (reinitialize && initialSort && !deepEqual(prevInitialSort, initialSort)) {
            setSorting([initialSort]);
        }
    }, [initialSort, prevInitialSort, reinitialize]);

    const getSortKey = useCallback(
        (id: string) => metaFields?.find(el => el.code === id)?.sort_key || id,
        [metaFields]
    );

    const backendSorting = useMemo(
        () => sorting.map(e => (e.desc ? `-${getSortKey(e.id)}` : `${getSortKey(e.id)}`)),
        [sorting, getSortKey]
    );

    const onSortingChange = useCallback(
        (newSort: SetStateAction<SortingState>) => {
            setSavedSort(newSort);
            setSorting(newSort);
        },
        [setSavedSort]
    );

    const invertedSorting = useMemo(
        () =>
            sorting.map(e => ({
                ...e,
                desc: !e.desc,
            })),
        [sorting]
    );

    const backendSortingResult = useCallback(() => {
        if (!metaFields) return undefined;

        if (backendSorting.length) return backendSorting;

        if (initialSort?.id) return [initialSort.id];

        return [];
    }, [backendSorting, initialSort?.id, metaFields]);

    return [
        {
            sorting,
            invertedSorting,
            backendSorting: backendSortingResult(),
        },
        {
            state: { sorting },
            root: {
                enableSorting: true,
                onSortingChange,
                getSortedRowModel: getSortedRowModel(),
                manualSorting,
            },
        } as TablePlugin<TData>,
    ] as const;
};
