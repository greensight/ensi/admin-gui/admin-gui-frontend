/* eslint-disable react-hooks/rules-of-hooks */
import { action } from '@storybook/addon-actions';
import type { Meta, StoryObj } from '@storybook/react';
import { useQuery } from '@tanstack/react-query';
import { ColumnSort, Row, createColumnHelper } from '@tanstack/react-table';
import { ComponentProps, useCallback, useMemo } from 'react';

import { Product, ProductFilter } from '@api/catalog/types';
import { CommonResponse, CommonSearchParams, FetchError } from '@api/common/types';

import README from './README.md';
import { getSelectColumn, getSettingsColumn } from './columns';
import RowTooltipWrapper from './components/RowTooltipWrapper';
import { Cell } from './components/TableCell';
import { TableFooter } from './components/TableFooter';
import Table, { TooltipItem, TrProps, useSorting, useTable } from './index';

export default {
    title: 'Components / Table',
    component: Table,
    parameters: {
        docs: {
            description: {
                component: README,
            },
        },
        backgrounds: {
            default: 'grey200',
        },
    },
} as Meta<typeof Table>;

const initialSort: ColumnSort = {
    id: 'id',
    desc: false,
};

const columnHelper = createColumnHelper<Product>();

const columns = [
    getSelectColumn<Product>(undefined, 'Example'),

    columnHelper.accessor('id', {
        header: 'ID',
        cell: props => props.getValue(),
        enableHiding: true,
    }),

    columnHelper.accessor('name', {
        header: 'Наименование',
        cell: props => props.getValue(),
        enableHiding: true,
    }),
    columnHelper.accessor('vendor_code', {
        header: 'Поставщик',
        cell: props => props.getValue(),
        enableHiding: true,
        enableSorting: false,
    }),
    columnHelper.accessor('brand_id', {
        header: 'Бренд',
        cell: props => props.getValue(),
        enableHiding: true,
        enableSorting: false,
    }),
    columnHelper.accessor('category_ids', {
        header: 'Категория',
        cell: props => props.getValue(),
        enableHiding: true,
        enableSorting: false,
    }),
    columnHelper.accessor('description', {
        header: 'Описание',
        cell: props => props.getValue(),
        enableHiding: true,
        enableSorting: false,
    }),
    columnHelper.accessor('main_image_url', {
        header: 'Изображение',
        cell: props => <Cell type="photo" value={props.getValue()} {...props} />,
        enableHiding: true,
        enableSorting: false,
    }),
    getSettingsColumn({
        tooltipContent: [
            {
                action() {},
                isDisable: false,
                text: 'Действие доступное из меню в каждой строке',
                type: 'copy',
            },
        ],
    }),
];

const useProducts = (data: CommonSearchParams<Partial<ProductFilter>, (string | undefined)[]>, isEnabled = true) =>
    useQuery<CommonResponse<Product[]>, FetchError>({
        enabled: isEnabled,
        queryKey: ['products', data],
        queryFn: () =>
            Promise.resolve({
                data: [
                    {
                        id: 46,
                        external_id: null,
                        category_ids: [18],
                        brand_id: null,
                        code: 'kuku-koksoval',
                        name: 'Гав гав гав гаввв',
                        description: '',
                        type: 1,
                        allow_publish: true,
                        vendor_code: 'увы',
                        barcode: null,
                        weight: 0.2,
                        weight_gross: 0.333,
                        length: 15,
                        height: 15,
                        width: 150,
                        created_at: '2023-01-01T12:00:00Z',
                        is_adult: false,
                        updated_at: '2024-05-16T12:00:00Z',
                        main_image_url: 'https://example.com/path/to/image.jpg',
                        main_image_file: 'path/to/image.jpg',
                    },
                    {
                        id: 47,
                        external_id: null,
                        category_ids: [24],
                        brand_id: null,
                        code: 'dfsdfsfs',
                        name: 'Гав гав гав гаввв',
                        description: null,
                        type: 2,
                        allow_publish: true,
                        vendor_code: '343',
                        barcode: null,
                        weight: 0.2,
                        weight_gross: 0,
                        length: 0,
                        height: 0,
                        width: 150,
                        created_at: '2023-01-01T12:00:00Z',
                        is_adult: false,
                        updated_at: '2024-05-16T12:00:00Z',
                        main_image_url: 'https://example.com/path/to/default.jpg',
                        main_image_file: 'path/to/default.jpg',
                    },
                    {
                        id: 77,
                        external_id: null,
                        category_ids: [18],
                        brand_id: null,
                        code: 'asdasdasd',
                        name: 'Гав гав гав гаввв',
                        description: '',
                        type: 1,
                        allow_publish: true,
                        vendor_code: '2',
                        barcode: null,
                        weight: 0,
                        weight_gross: 0.333,
                        length: 0,
                        height: 0,
                        width: 150,
                        created_at: '2023-01-01T12:00:00Z',
                        is_adult: false,
                        updated_at: '2024-05-16T12:00:00Z',
                        main_image_url: 'https://example.com/path/to/default.jpg',
                        main_image_file: 'path/to/default.jpg',
                    },
                    {
                        id: 78,
                        external_id: null,
                        category_ids: [1],
                        brand_id: 85,
                        code: 'pro-2',
                        name: 'Гав гав гав гаввв',
                        description: '',
                        type: 1,
                        allow_publish: true,
                        vendor_code: 'апорт123',
                        barcode: null,
                        weight: 0,
                        weight_gross: 887,
                        length: 78,
                        height: 787,
                        width: 150,
                        created_at: '2023-01-01T12:00:00Z',
                        is_adult: false,
                        updated_at: '2024-05-16T12:00:00Z',
                        main_image_url: 'https://example.com/path/to/default.jpg',
                        main_image_file: 'path/to/default.jpg',
                    },
                    {
                        id: 79,
                        external_id: null,
                        category_ids: [1],
                        brand_id: null,
                        code: 'yuvyv',
                        name: 'Гав гав гав гаввв',
                        description: '',
                        type: 1,
                        allow_publish: true,
                        vendor_code: 'выв',
                        barcode: null,
                        weight: 0.2,
                        weight_gross: 0,
                        length: 0,
                        height: 0,
                        width: 150,
                        created_at: '2023-01-01T12:00:00Z',
                        is_adult: false,
                        updated_at: '2024-05-16T12:00:00Z',
                        main_image_url: 'https://example.com/path/to/default.jpg',
                        main_image_file: 'path/to/default.jpg',
                    },
                    {
                        id: 81,
                        external_id: null,
                        category_ids: [12],
                        brand_id: null,
                        code: '43',
                        name: 'Гав гав гав гаввв',
                        description: '',
                        type: 1,
                        allow_publish: true,
                        vendor_code: '233',
                        barcode: null,
                        weight: 0.2,
                        weight_gross: 0.333,
                        length: 0,
                        height: 0,
                        width: 150,
                        created_at: '2023-01-01T12:00:00Z',
                        is_adult: false,
                        updated_at: '2024-05-16T12:00:00Z',
                        main_image_url: 'https://example.com/path/to/default.jpg',
                        main_image_file: 'path/to/default.jpg',
                    },
                    {
                        id: 82,
                        external_id: null,
                        category_ids: [35],
                        brand_id: null,
                        code: '234',
                        name: 'Гав гав гав гаввв',
                        description: 'Тестики массовости',
                        type: 1,
                        allow_publish: true,
                        vendor_code: '2342',
                        barcode: null,
                        weight: 0.2,
                        weight_gross: null,
                        length: 0,
                        height: 0,
                        width: 150,
                        created_at: '2023-01-01T12:00:00Z',
                        is_adult: false,
                        updated_at: '2024-05-16T12:00:00Z',
                        main_image_url: 'https://example.com/path/to/default.jpg',
                        main_image_file: 'path/to/default.jpg',
                    },
                    {
                        id: 83,
                        external_id: null,
                        category_ids: [43],
                        brand_id: null,
                        name: 'Рис',
                        description: 'Рис вкусный',
                        vendor_code: '00000100',
                        barcode: null,
                        main_image_url: 'https://es-dev.ensi.tech/catalog/products/97/31/eWh3h8uxPdNspeY8Bcix.jpg',
                        main_image_file: 'catalog/products/97/31/eWh3h8uxPdNspeY8Bcix.jpg',
                        type: 1,
                        allow_publish: true,
                        created_at: '2023-01-01T12:00:00Z',
                        is_adult: false,
                        updated_at: '2024-05-16T12:00:00Z',
                        weight: 0.2,
                        weight_gross: 0,
                        length: 0,
                        height: 0,
                        width: 150,
                    },
                ] as Product[],
                meta: {},
            }),
    });

export const Basic: StoryObj<ComponentProps<typeof Table> & {}> = {
    args: {
        block: true,
        stickyHeader: false,
    },
    argTypes: {
        block: {
            description: 'Растягивать таблицу на всю ширину контейнера. По-умолчанию `true`',
        },
        stickyHeader: {
            description: 'Включить умное липкое поведение шапки таблицы. По-умолчанию `false`.',
        },
    },
    parameters: {
        docs: {
            description: {
                story: 'Story description',
            },
        },
    },
    render: args => {
        const [{ backendSorting }, sortingPlugin] = useSorting<Product>('storybook_FirstTable', [], initialSort);

        const { data } = useProducts({
            filter: {},
            sort: backendSorting,
            pagination: { type: 'offset', offset: 0, limit: 20 },
        });

        const products = useMemo(() => data?.data || [], [data?.data]);

        const table = useTable(
            {
                data: products,
                columns,
                meta: {
                    tableKey: `storybook_FirstTable`,
                },
            },
            [sortingPlugin]
        );

        const getTooltipForRow = useCallback(
            (row: Row<any>): TooltipItem[] => {
                if (table.getSelectedRowModel().flatRows.length) {
                    return [
                        {
                            text: `Операция над выделенными: ${table.getSelectedRowModel().flatRows.length} строками`,
                            type: 'delete',
                            action() {
                                alert('удалить');
                            },
                            isDisable: false,
                        },
                    ];
                }

                return [
                    {
                        text: `Перейти в деталку ${row.id}`,
                        action() {
                            // eslint-disable-next-line no-template-curly-in-string
                            alert('push(`/path/to/entities/${row.id}`)');
                        },
                        isDisable: false,
                        type: 'edit',
                    },
                    {
                        text: `Удалить #${row.id}`,
                        action() {},
                        isDisable: (+row.id as number) % 2 === 1,
                        type: 'delete',
                        disabledHint: 'У вас нет прав на совершение данного действия',
                    },
                ];
            },
            [table]
        );

        const renderRow = useCallback(
            ({ children, ...props }: TrProps<any>) => (
                <RowTooltipWrapper {...props} getTooltipForRow={getTooltipForRow}>
                    {children}
                </RowTooltipWrapper>
            ),
            [getTooltipForRow]
        );

        return (
            <div>
                <p>Выделено: {table.getSelectedRowModel().flatRows.length}</p>
                <Table {...args} instance={table} Tr={renderRow} />
                <TableFooter itemsPerPageCount={10} pages={14} setItemsPerPageCount={action('set items')} />
            </div>
        );
    },
};
