import { UseQueryResult } from '@tanstack/react-query';
import { ReactNode } from 'react';

import { CommonResponse, FetchError, Meta } from '@api/common/types';

import { TableColumnDefAny, TooltipItem } from '@components/Table';

import { AccessMatrix, ReturnedMatrix } from '@scripts/hooks';
import { autoFilterData } from '@scripts/hooks/autoTable';

export interface IListBuilderProps<EntityType> {
    /**
     * List name
     * @example 'Products'
     */
    title: string;

    /**
     * ListBuilder content
     *
     * It should be used for popups, additional components, etc
     */
    children?: ReactNode;

    /**
     * Access matrix
     * @example
     * {
     *     LIST: {
     *         view: true,
     *     },
     *     ID: {
     *         view: true,
     *         delete: true,
     *         edit: true,
     *         create: true,
     *     },
     * }
     */
    access: ReturnedMatrix<AccessMatrix>;

    /**
     * Hook for the endpoint that searches for an entity for the list
     * @example useReviews
     */
    searchHook: (
        data: Record<string, any>,
        enabled?: boolean
    ) => UseQueryResult<CommonResponse<EntityType[]>, FetchError>;

    /**
     * Hook for enriching the search data for the entity, e.g., middleware for fields, etc.
     */
    enrichSearchHookRequest?: (data: autoFilterData) => Record<string, any>;

    /**
     * Hook for the endpoint that retrieves metadata for the entity
     * @example useReviewsMeta
     */
    metaHook: (enabled?: boolean) => UseQueryResult<
        {
            data: Meta;
        },
        FetchError
    >;

    /**
     * Hook for enriching metadata of the entity
     */
    enrichMetaData?: (metaData?: Meta) => Partial<Meta>;

    /**
     * Flag for loading additional operations: delete, edit, etc.
     * @example true
     */
    isLoading?: boolean;

    /**
     * Additional columns
     * @example [columnHelper.accessor('name', {
     *              header: 'Наименование',
     *              cell: props => props.getValue(),
     *              enableHiding: true,
     *          })]
     */
    extraColumns?: TableColumnDefAny[];

    /**
     * Tooltip for rows in the list
     * @example [{
     *              type: 'edit',
     *              text: 'Задать признаки',
     *              action: tooltipAction,
     *          }]
     */
    tooltipItems?: TooltipItem[];

    /**
     * Additional components in the header for batch operations
     */
    headerInner?: (table?: any) => JSX.Element | null;

    /**
     * Hide row selection checkboxes
     */
    hiddenRowSelect?: boolean;

    /**
     * Tooltips for batch operations/actions
     * @example [{
     *              type: 'edit',
     *              text: 'Задать признаки',
     *              action: tooltipAction,
     *          }]
     */
    tooltipForAdditionalActions?: TooltipItem[];

    /**
     * Allow to go to detail page by row double click
     *
     * Has a higher priority than `access.ID.view`.
     * @default access.ID.view
     */
    canViewDetailPage?: boolean;
}
