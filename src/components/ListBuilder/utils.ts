import { TableColumnDefAny } from '@components/Table';

export const mergeColumns = (columns: TableColumnDefAny[], extraColumns?: TableColumnDefAny[]): TableColumnDefAny[] => {
    if (!extraColumns) return columns;

    const result = [...columns];

    extraColumns.forEach(extraColumn => {
        const index = columns.findIndex(column => column.accessorKey === extraColumn.accessorKey);
        if (index !== -1) {
            result[index] = extraColumn;
        }
    });

    return result;
};
