import { Block } from '@ensi-platform/core-components';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useMemo } from 'react';

import { useError } from '@context/modal';

import { initialSort } from '@views/logistic/pickup-points/[id]/scripts/constants';

import Tooltip, { ContentBtn, hideOnEsc } from '@controls/Tooltip';

import AutoFilters from '@components/AutoFilters';
import PageWrapper from '@components/PageWrapper';
import Table, { TableColumnDefAny, TrProps, useSorting, useTable } from '@components/Table';
import { getSelectColumn, getSettingsColumn } from '@components/Table/columns';
import { RowTooltipWrapper, TableEmpty, TableFooter, TableHeader } from '@components/Table/components';

import { ITEMS_PER_PRODUCTS_PAGE } from '@scripts/constants';
import { Button, scale } from '@scripts/gds';
import { declOfNum, getPagination, getTotal, getTotalPages } from '@scripts/helpers';
import { useRedirectToNotEmptyPage, useTableList } from '@scripts/hooks';
import { useAutoColumns, useAutoFilters, useAutoTableData } from '@scripts/hooks/autoTable';

import PlusIcon from '@icons/plus.svg';
import KebabIcon from '@icons/small/kebab.svg';

import { IListBuilderProps } from './types';
import { mergeColumns } from './utils';

const ListBuilder = <EntityType extends Record<string, any>>({
    title,
    children: childrenProp,
    access,
    searchHook,
    enrichSearchHookRequest,
    metaHook,
    enrichMetaData,
    isLoading,
    extraColumns,
    tooltipItems,
    headerInner,
    hiddenRowSelect = false,
    tooltipForAdditionalActions,
    canViewDetailPage: canViewDetailPageProp,
}: IListBuilderProps<EntityType>) => {
    const { pathname, push } = useRouter();

    const canViewDetailPage = canViewDetailPageProp === undefined ? access.ID.view : canViewDetailPageProp;

    const { data: metaData, error: metaError, isFetching: isMetaLoading } = metaHook(!!access.LIST.view);
    useError(metaError);
    const meta = useMemo(
        () => (metaData && { ...metaData?.data, ...(enrichMetaData && enrichMetaData(metaData.data)) }) || undefined,
        [enrichMetaData, metaData]
    );

    const autoFiltersData = useAutoFilters(meta);
    const {
        metaField,
        values,
        filtersActive,
        URLHelper,
        reset,
        searchRequestFilter,
        emptyInitialValues,
        searchRequestIncludes,
        codesToSortKeys,
        clearInitialValue,
    } = autoFiltersData;

    const [{ backendSorting }, sortingPlugin] = useSorting<EntityType>(pathname, meta?.fields, initialSort);

    const { activePage, itemsPerPageCount, setItemsPerPageCount } = useTableList({
        defaultSort: meta?.default_sort,
        defaultPerPage: ITEMS_PER_PRODUCTS_PAGE,
        codesToSortKeys,
    });

    const {
        data,
        isFetching: isSearchEntityLoading,
        error,
    } = searchHook(
        {
            include: searchRequestIncludes,
            sort: backendSorting,
            filter: searchRequestFilter,
            pagination: getPagination(activePage, itemsPerPageCount),
            ...(enrichSearchHookRequest && enrichSearchHookRequest(autoFiltersData)),
        },
        Boolean(metaData) && !!backendSorting && (access.LIST.view as boolean)
    );
    useError(error);

    const total = getTotal(data);
    const totalPages = getTotalPages(data, itemsPerPageCount);

    useRedirectToNotEmptyPage({ activePage, itemsPerPageCount, total });

    const columns = useAutoColumns(meta, Boolean(access.ID.view));
    const rows = useAutoTableData<EntityType>(data?.data, metaField);

    const preparedColumns = useMemo<TableColumnDefAny[]>(
        () => [
            ...(headerInner && !hiddenRowSelect && getSelectColumn(undefined, pathname)
                ? [getSelectColumn(undefined, pathname)]
                : []),
            ...mergeColumns(columns, extraColumns),
            getSettingsColumn({
                visibleColumns: meta?.default_list,
                tooltipContent: tooltipItems,
            }),
        ],
        [headerInner, tooltipItems, columns]
    );

    const table = useTable<EntityType>(
        {
            data: rows,
            columns: preparedColumns,
            meta: {
                tableKey: pathname,
            },
        },
        [sortingPlugin]
    );

    const getTooltipForRow = useCallback(() => tooltipItems || [], [tooltipItems]);
    const renderRow = useCallback(
        ({ children, ...props }: TrProps<any>) => (
            <RowTooltipWrapper
                {...props}
                getTooltipForRow={getTooltipForRow}
                onDoubleClick={() => {
                    if (canViewDetailPage) push(`${pathname}/${props.row?.original[meta?.detail_link || 'id']}`);
                }}
            >
                {children}
            </RowTooltipWrapper>
        ),
        [getTooltipForRow]
    );

    const isLoadingCommon = isLoading || isMetaLoading || isSearchEntityLoading;

    return (
        <PageWrapper h1={title} isLoading={isLoadingCommon} isForbidden={!access.LIST.view}>
            <AutoFilters
                initialValues={values}
                emptyInitialValues={emptyInitialValues}
                onSubmit={URLHelper}
                filtersActive={filtersActive}
                css={{ marginBottom: scale(2) }}
                onResetFilters={reset}
                meta={meta}
                clearInitialValue={clearInitialValue}
            />
            <Block>
                <Block.Body>
                    <TableHeader css={{ justifyContent: 'space-between' }}>
                        {table.getSelectedRowModel().flatRows.length > 0 ? (
                            <p>
                                Выделено: <b>{table.getSelectedRowModel().flatRows.length}</b>
                            </p>
                        ) : (
                            <p>Найдено {`${total} ${declOfNum(total, ['запись', 'записи', 'записей'])}`}</p>
                        )}
                        <div css={{ display: 'flex' }}>
                            {headerInner && headerInner(table)}
                            {tooltipForAdditionalActions && (
                                <Tooltip
                                    content={
                                        <>
                                            {tooltipForAdditionalActions.map(t => (
                                                <ContentBtn
                                                    key={t.text}
                                                    type={t.type}
                                                    onClick={e => {
                                                        e.stopPropagation();
                                                        t.action(table.getSelectedRowModel().flatRows);
                                                    }}
                                                    disabled={
                                                        typeof t?.isDisable === 'function'
                                                            ? t.isDisable()
                                                            : t?.isDisable
                                                    }
                                                >
                                                    {t.text}
                                                </ContentBtn>
                                            ))}
                                        </>
                                    }
                                    plugins={[hideOnEsc]}
                                    trigger="click"
                                    arrow
                                    theme="light"
                                    placement="bottom"
                                    minWidth={scale(36)}
                                    disabled={tooltipForAdditionalActions.length === 0}
                                    appendTo={() => document.body}
                                >
                                    <Button
                                        theme="outline"
                                        Icon={KebabIcon}
                                        css={{ marginLeft: scale(2), marginRight: scale(2) }}
                                        iconAfter
                                    >
                                        Действия
                                    </Button>
                                </Tooltip>
                            )}
                            {access.ID.create && (
                                <Link href={`${pathname}/create`} passHref>
                                    <Button Icon={PlusIcon} as="a">
                                        Создать
                                    </Button>
                                </Link>
                            )}
                        </div>
                    </TableHeader>
                    <Table instance={table} {...(tooltipItems && { Tr: renderRow })} />
                    {rows.length === 0 ? (
                        <TableEmpty
                            onResetFilters={reset}
                            filtersActive={filtersActive}
                            titleWithFilters="Записи не найдены"
                            titleWithoutFilters="Нет записей"
                            addItems={() => push(`${pathname}/create`)}
                        />
                    ) : (
                        <TableFooter
                            pages={totalPages}
                            itemsPerPageCount={itemsPerPageCount}
                            setItemsPerPageCount={setItemsPerPageCount}
                        />
                    )}
                </Block.Body>
            </Block>
            {childrenProp}
        </PageWrapper>
    );
};

export * from './types';
export default ListBuilder;
