import Head from 'next/head';
import { ReactNode } from 'react';

import NotFoundPage from '@views/404';

import LoadWrapper from '@controls/LoadWrapper';

// eslint-disable-next-line import/no-cycle
import ForbiddenPage from '@components/ForbiddenPage';

import { VisuallyHidden, scale, typography, useTheme } from '@scripts/gds';

type PageWrapperProps = {
    h1?: string;
    title?: string;
    error?: string;
    isLoading?: boolean;
    children: ReactNode;
    className?: string;
    isForbidden?: boolean;
    isNotFound?: boolean;
};

const PageWrapper = ({
    h1,
    title,
    isLoading = false,
    error,
    children,
    className,
    isForbidden = false,
    isNotFound = false,
}: PageWrapperProps) => {
    const { colors } = useTheme();

    const renderErrorPage = () => {
        if (isForbidden) return <ForbiddenPage />;
        if (isNotFound) return <NotFoundPage />;
        return children;
    };

    return (
        <>
            <Head>
                <title>{title || h1}</title>
            </Head>
            <LoadWrapper isLoading={isLoading} error={error}>
                <main
                    css={{
                        padding: `${scale(2)}px ${scale(3)}px`,
                        background: colors?.grey200,
                        height: '100%',
                    }}
                    className={className}
                >
                    {h1 ? (
                        <h1 css={{ ...typography('h1'), marginBottom: scale(2), marginTop: 0 }}>{h1}</h1>
                    ) : (
                        <VisuallyHidden>
                            <h1>{title}</h1>
                        </VisuallyHidden>
                    )}

                    {renderErrorPage()}
                </main>
            </LoadWrapper>
        </>
    );
};

export default PageWrapper;
