import { useAccess } from '@scripts/hooks';

const useCloudAccess = () =>
    useAccess({
        LIST: {
            view: 2602,
        },
        ID: {
            view: 2602,
            edit: 2601,
            create: 2601,
            activity_edit: 2603,
            delete: -1,
        },
    });

export default useCloudAccess;
