import {
    DescriptionList,
    DescriptionListItem,
    FormCheckbox,
    FormField,
    FormFieldWrapper,
    FormReset,
} from '@ensi-platform/core-components';
import { useMemo } from 'react';
import * as Yup from 'yup';

import {
    useCloudIntegration,
    useCreateCloudIntegration,
    usePatchCloudIntegration,
} from '@api/catalog/cloud-integration';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { ErrorMessages } from '@scripts/constants';
import { RedirectMethods } from '@scripts/enums';
import { Layout } from '@scripts/gds';

import useCloudAccess from './useCloudAccess';

const CloudIntegration = () => {
    const access = useCloudAccess();

    const { data, isFetching: isLoading } = useCloudIntegration(access.LIST.view);

    const isCreatePage = !data?.data.created_at && !isLoading;

    const createIntegration = useCreateCloudIntegration();
    const patchIntegration = usePatchCloudIntegration();

    const initialValues = useMemo(
        () => ({
            integration: data?.data.integration || false,
            public_api_key: data?.data.public_api_key || '',
            private_api_key: data?.data.private_api_key || '',
        }),
        [data?.data.integration, data?.data.private_api_key, data?.data.public_api_key]
    );

    return (
        <PageWrapper
            title="Параметры поиска"
            isLoading={isLoading || createIntegration.isPending || patchIntegration.isPending}
            isForbidden={!access.LIST.view}
        >
            <FormWrapper
                disabled={!access.ID.edit}
                validationSchema={Yup.object().shape({
                    integration: Yup.boolean(),
                    public_api_key: Yup.string().when('integration', ([integration], schema) =>
                        integration ? schema.required(ErrorMessages.REQUIRED) : schema
                    ),
                    private_api_key: Yup.string().when('integration', ([integration], schema) =>
                        integration ? schema.required(ErrorMessages.REQUIRED) : schema
                    ),
                })}
                onSubmit={async values => {
                    if (isCreatePage) {
                        await createIntegration.mutateAsync(values);

                        return {
                            method: RedirectMethods.replace,
                            redirectPath: `/settings/cloud-integration`,
                        };
                    }

                    await patchIntegration.mutateAsync(values);

                    return {
                        method: RedirectMethods.replace,
                        redirectPath: `/settings/cloud-integration`,
                    };
                }}
                initialValues={initialValues}
                enableReinitialize
                css={{ height: '100%' }}
            >
                <PageTemplate
                    h1="Параметры поиска"
                    controls={
                        <PageControls access={access}>
                            <Layout.Item>
                                <FormReset theme="outline">Сбросить</FormReset>
                            </Layout.Item>
                            <PageControls.Save />
                        </PageControls>
                    }
                    aside={
                        <DescriptionList>
                            <DescriptionListItem name="Дата создания" value={data?.data.created_at} type="date" />
                            <DescriptionListItem name="Дата обновления" value={data?.data.updated_at} type="date" />
                        </DescriptionList>
                    }
                >
                    <Layout cols={1}>
                        <FormField name="public_api_key" label="Публичный API-ключ" />
                        <FormField name="private_api_key" label="Приватный API-ключ" />
                        <FormFieldWrapper name="integration" disabled={!access.ID.activity_edit}>
                            <FormCheckbox>Интеграция с сервисом EnsiCloudSearch</FormCheckbox>
                        </FormFieldWrapper>
                    </Layout>
                </PageTemplate>
            </FormWrapper>
        </PageWrapper>
    );
};

export default CloudIntegration;
