import { CopyButton, FormFieldWrapper } from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { ReactNode, useMemo, useState } from 'react';
import * as Yup from 'yup';

import {
    useAddAdminUserRole,
    useAdminUser,
    useCreateAdminUser,
    useDeleteAdminUserRole,
    useUpdateAdminUser,
} from '@api/units';

import { useError, useSuccess } from '@context/modal';

import Switcher from '@controls/Switcher';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { CREATE_PARAM, DateFormatters, ErrorMessages, ModalMessages } from '@scripts/constants';
import { RedirectMethods } from '@scripts/enums';
import { scale, typography } from '@scripts/gds';
import { cleanPhoneValue, formatDate, isPageNotFound, prepareTelValue } from '@scripts/helpers';
import { regNameRu, regOneDigit, regOneLetter } from '@scripts/regex';

import { useUsersAccess } from '../useUsersAccess';
import FormFields from './components/FormFields';
import { useQueries } from './scripts/hooks';

const SideBarItem = ({ name, children }: { name: string; children?: ReactNode }) => (
    <div css={{ ...typography('bodySm'), ':not(:last-of-type)': { marginBottom: scale(1) } }}>
        <span css={{ marginRight: scale(1, true) }}>{name}:</span>
        {children || '-'}
    </div>
);

const LISTING_URL = '/settings/users';

export default function RoleModelDetail() {
    const [isNeedRedirect, setIsNeedRedirect] = useState(false);
    const { push, query, pathname } = useRouter();
    const listLink = pathname.split(`[id]`)[0];
    const id = Array.isArray(query.id) ? query.id[0] : query.id;
    const isCreation = id === CREATE_PARAM;

    const { access, isIDForbidden } = useUsersAccess();
    const {
        ID: {
            create: canCreate,
            edit: canEdit,
            ACTIVE: { edit: canEditUserActiveExisting },
        },
    } = access;

    const canEditUserActive = isCreation ? canCreate : canEditUserActiveExisting || canEdit;
    const { isAdminRolesLoading, isAdminRolesError } = useQueries();

    useError(isAdminRolesError);

    const { data: adminUserDetail, isFetching: isDetailLoading, error: isDetailError } = useAdminUser(id, ['roles']);

    useError(isDetailError);

    const adminUserDetailData = useMemo(() => adminUserDetail?.data, [adminUserDetail]);

    const pageTitle = isCreation ? 'Новый пользователь' : `Пользователь: ${adminUserDetailData?.login}`;

    const {
        mutateAsync: onAddRole,
        isPending: isRoleAddingLoading,
        error: addingRoleError,
        isSuccess: isAddingRolesSuccess,
    } = useAddAdminUserRole();
    useError(addingRoleError);

    const {
        mutateAsync: onDeleteRole,
        isPending: isRoleDeleteLoading,
        error: deletingRoleError,
        isSuccess: isDeletingRoleSuccess,
    } = useDeleteAdminUserRole();
    useError(deletingRoleError);

    const {
        mutateAsync: createAdminUser,
        isPending: isCreateLoading,
        error: isCreateError,
        isSuccess: isCreateSuccess,
    } = useCreateAdminUser();

    useError(isCreateError);

    useSuccess(isCreateSuccess || (isCreateSuccess && isAddingRolesSuccess) ? ModalMessages.SUCCESS_CREATE : '');

    const {
        mutateAsync: updateAdminUser,
        isPending: isUpdateLoading,
        error: isUpdateError,
        isSuccess: isUpdateSuccess,
    } = useUpdateAdminUser();

    useError(isUpdateError);

    useSuccess(isUpdateSuccess && (isAddingRolesSuccess || isDeletingRoleSuccess) ? ModalMessages.SUCCESS_UPDATE : '');

    const formInitialValues = useMemo(
        () => ({
            active: adminUserDetailData?.active || false,
            cause_deactivation: '',
            email: adminUserDetailData?.email || '',
            first_name: adminUserDetailData?.first_name || '',
            last_name: adminUserDetailData?.last_name || '',
            login: adminUserDetailData?.login || '',
            middle_name: adminUserDetailData?.middle_name || '',
            password: '',
            phone: adminUserDetailData?.phone ? prepareTelValue(adminUserDetailData?.phone) : '',
            role_id: adminUserDetailData?.roles?.map(({ id: roleId }) => Number(roleId)) || [],
            // @TODO заглушка по причине несостыковок таймзон фронт/бек
            timezone: 'Europe/Moscow',
        }),
        [adminUserDetailData]
    );

    const formValidationSchema = useMemo(
        () => ({
            active: Yup.boolean(),

            cause_deactivation: formInitialValues?.active
                ? Yup.string().when('active', ([active], schema) =>
                      !active ? schema.required('Обязательное поле') : schema
                  )
                : Yup.string(),
            email: Yup.string().email(ErrorMessages.EMAIL).required(ErrorMessages.REQUIRED),
            first_name: Yup.string()
                .matches(regNameRu, 'Используйте кириллицу для заполнения')
                .required(ErrorMessages.REQUIRED),
            last_name: Yup.string()
                .matches(regNameRu, 'Используйте кириллицу для заполнения')
                .required(ErrorMessages.REQUIRED),
            login: Yup.string().required(ErrorMessages.REQUIRED),
            middle_name: Yup.string()
                .matches(regNameRu, 'Используйте кириллицу для заполнения')
                .required(ErrorMessages.REQUIRED),
            password: Yup.string(),
            ...(isCreation && {
                password: Yup.string()
                    .matches(regOneLetter, 'Пароль должен содержать хотя бы 1 латинскую букву')
                    .matches(regOneDigit, 'Пароль должен содержать хотя бы 1 цифру')
                    .min(8, 'Пароль должен быть не менее 8 символов')
                    .required(ErrorMessages.REQUIRED),
            }),

            phone: Yup.string().required(ErrorMessages.REQUIRED),
            role_id: Yup.array().min(1, ErrorMessages.MIN_ITEMS(1)),
        }),
        [formInitialValues?.active, isCreation]
    );

    const isNotFound = isPageNotFound({ id, error: isDetailError, isCreation });

    return (
        <PageWrapper
            title={pageTitle}
            isLoading={
                isDetailLoading ||
                isAdminRolesLoading ||
                isCreateLoading ||
                isUpdateLoading ||
                isRoleAddingLoading ||
                isRoleDeleteLoading
                // isDeleteLoading
            }
            isNotFound={isNotFound}
            isForbidden={isIDForbidden}
        >
            <FormWrapper
                autoComplete="off"
                onSubmit={async values => {
                    if (isCreation) {
                        const { data: createdAdminUserData } = await createAdminUser({
                            ...values,
                            phone: cleanPhoneValue(values?.phone),
                        });

                        await onAddRole({
                            id: Number(createdAdminUserData?.id),
                            roles: values?.role_id,
                            expires: null,
                        });

                        if (isNeedRedirect) {
                            return {
                                method: RedirectMethods.push,
                                redirectPath: listLink,
                            };
                        }

                        return {
                            method: RedirectMethods.replace,
                            redirectPath: `${listLink}${createdAdminUserData?.id}`,
                        };
                    }

                    const changedValues = Object.keys(values).reduce(
                        (acc, cur) => {
                            const key = cur as keyof typeof values;
                            if (JSON.stringify(values[key]) !== JSON.stringify(formInitialValues[key]))
                                return { ...acc, [key]: values[key] };
                            return acc;
                        },
                        {} as typeof values
                    );

                    if (Object.keys(changedValues)?.length)
                        await updateAdminUser({
                            id: Number(id),
                            ...changedValues,
                            ...(changedValues?.active === false && {
                                cause_deactivation: changedValues?.cause_deactivation,
                            }),
                            ...(changedValues?.phone && {
                                phone: cleanPhoneValue(changedValues?.phone),
                            }),

                            // инвалидировать только если нет ролей для изменения
                            disableInvalidate: Boolean(changedValues?.role_id),
                        });

                    if (values?.role_id) {
                        const newRoles = values?.role_id?.filter(
                            (roleItem: number) => !formInitialValues?.role_id?.includes(roleItem)
                        );

                        const deletedRoles = formInitialValues?.role_id?.filter(
                            (roleItem: number) => !values?.role_id?.includes(roleItem)
                        );

                        if (newRoles?.length) {
                            await onAddRole({
                                id: Number(id),
                                roles: newRoles,
                                expires: null,
                                // инвалидировать только если нет ролей на удаление
                                disableInvalidate: Boolean(deletedRoles?.length),
                            });
                        }

                        if (deletedRoles?.length) {
                            await Promise.all(
                                deletedRoles?.map((roleForDelete: number) =>
                                    onDeleteRole({
                                        id: Number(id),
                                        role_id: roleForDelete,
                                        // инвалидировать только в последнем запросе
                                        disableInvalidate: Boolean(
                                            roleForDelete !== deletedRoles[deletedRoles.length - 1]
                                        ),
                                    })
                                )
                            );
                        }
                    }

                    if (isNeedRedirect) {
                        return {
                            method: RedirectMethods.push,
                            redirectPath: listLink,
                        };
                    }

                    return {
                        method: RedirectMethods.replace,
                        redirectPath: `${listLink}${id}`,
                    };
                }}
                initialValues={formInitialValues}
                enableReinitialize
                css={{ height: '100%' }}
                validationSchema={Yup.object().shape(formValidationSchema)}
            >
                <PageTemplate
                    h1={pageTitle}
                    controls={
                        <PageControls access={access}>
                            <PageControls.Close
                                onClick={() => {
                                    push(LISTING_URL);
                                }}
                            />
                            <PageControls.Apply
                                onClick={() => {
                                    setIsNeedRedirect(false);
                                }}
                            />
                            <PageControls.Save
                                onClick={() => {
                                    setIsNeedRedirect(true);
                                }}
                            />
                        </PageControls>
                    }
                    backlink={{ text: 'Назад', href: '/settings/users' }}
                    aside={
                        <>
                            <FormFieldWrapper name="active" css={{ marginBottom: scale(3) }}>
                                <Switcher disabled={!canEditUserActive}>Активность</Switcher>
                            </FormFieldWrapper>

                            {!isCreation ? (
                                <SideBarItem name="ID">{id ? <CopyButton>{String(id)}</CopyButton> : null}</SideBarItem>
                            ) : null}

                            <SideBarItem name="Изменено">
                                {!isCreation && adminUserDetailData?.updated_at
                                    ? formatDate(
                                          new Date(adminUserDetailData?.updated_at),
                                          DateFormatters.DATE_AND_TIME
                                      )
                                    : '-'}
                            </SideBarItem>
                            <SideBarItem name="Создано">
                                {!isCreation && adminUserDetailData?.created_at
                                    ? formatDate(
                                          new Date(adminUserDetailData?.created_at),
                                          DateFormatters.DATE_AND_TIME
                                      )
                                    : '-'}
                            </SideBarItem>
                        </>
                    }
                >
                    <FormFields initialValues={formInitialValues} isCreationPage={isCreation} />
                </PageTemplate>
            </FormWrapper>
        </PageWrapper>
    );
}
