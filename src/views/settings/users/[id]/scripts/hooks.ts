import { useAdminRoles } from '@api/units';

export const useQueries = () => {
    const {
        data: adminRoles,
        isFetching: isAdminRolesLoading,
        error: isAdminRolesError,
    } = useAdminRoles({
        filter: { active: true },
        pagination: { type: 'offset', limit: -1, offset: 0 },
    });

    return { adminRoles, isAdminRolesLoading, isAdminRolesError };
};
