import { FormField, FormFieldWrapper, SelectWithTags, Textarea, useFormContext } from '@ensi-platform/core-components';
import { useMemo } from 'react';

import Mask from '@controls/Mask';
import Password from '@controls/Password';

import { Layout, scale } from '@scripts/gds';
import { maskPhone } from '@scripts/mask';

import { useUsersAccess } from '../../useUsersAccess';
import { useQueries } from '../scripts/hooks';

const FormFields = ({
    initialValues,
    isCreationPage,
}: {
    initialValues?: { active: boolean };
    isCreationPage: boolean;
}) => {
    const { watch } = useFormContext();
    const active = watch('active');

    const { access } = useUsersAccess();
    const {
        ID: {
            create: canCreate,
            edit: canEdit,
            ROLES: { edit: canEditExistingUserRoles },
            ACTIVE: { edit: canEditExistingUserActive },
            PASSWORD: { edit: canEditExistingPassword },
        },
    } = access;

    const { adminRoles } = useQueries();

    const rolesOptions = useMemo(
        () => adminRoles?.data?.map(item => ({ value: item.id, label: item.title })) || [],
        [adminRoles?.data]
    );

    const canEditPassword = isCreationPage ? canCreate : canEditExistingPassword || canEdit;
    const canEditUserRoles = isCreationPage ? canCreate : canEditExistingUserRoles || canEdit;
    const canEditUserActive = isCreationPage ? canCreate : canEditExistingUserActive || canEdit;
    const canEditFormField = isCreationPage ? canCreate : canEdit;

    return (
        <Layout
            cols={{
                xxxl: 2,
                xs: 1,
            }}
            css={{ marginBottom: scale(4) }}
        >
            <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                <FormField name="last_name" label="Фамилия" disabled={!canEditFormField} />
            </Layout.Item>

            <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                <FormField name="first_name" label="Имя" disabled={!canEditFormField} />
            </Layout.Item>

            <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                <FormField name="middle_name" label="Отчество" disabled={!canEditFormField} />
            </Layout.Item>

            <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                <FormFieldWrapper name="phone" label="Телефон" disabled={!canEditFormField}>
                    <Mask mask={maskPhone} />
                </FormFieldWrapper>
            </Layout.Item>

            <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                <FormField name="login" label="Логин" autoComplete="off" disabled={!canEditFormField} />
            </Layout.Item>

            <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                <FormField name="email" label="Email" autoComplete="off" disabled={!canEditFormField} />
            </Layout.Item>

            <Layout.Item
                col={{
                    xxxl: 2,
                    xs: 1,
                }}
            >
                <hr />
            </Layout.Item>

            <Layout.Item
                col={{
                    xxxl: 2,
                    xs: 2,
                }}
            >
                <FormFieldWrapper name="password" disabled={!canEditPassword}>
                    <Password
                        label="Пароль"
                        hint="Пароль должен быть не менее 8 символов и содержать минимум 1 латинский символ"
                        autoComplete="new-password"
                    />
                </FormFieldWrapper>
            </Layout.Item>

            <Layout.Item
                col={{
                    xxxl: 2,
                    xs: 2,
                }}
            >
                <FormFieldWrapper name="role_id">
                    <SelectWithTags options={rolesOptions} label="Роли" disabled={!canEditUserRoles} />
                </FormFieldWrapper>
            </Layout.Item>

            {initialValues?.active && !active ? (
                <Layout.Item
                    col={{
                        xxxl: 2,
                        xs: 1,
                    }}
                >
                    <FormFieldWrapper
                        name="cause_deactivation"
                        label="Причина деактивации"
                        disabled={!canEditUserActive}
                    >
                        <Textarea />
                    </FormFieldWrapper>
                </Layout.Item>
            ) : null}
        </Layout>
    );
};

export default FormFields;
