import { useMemo } from 'react';

import { useCurrentUser } from '@api/auth';

import { useAccess } from '@scripts/hooks';
import { useIDForbidden } from '@scripts/hooks/useIDForbidden';

export const accessMatrix = {
    LIST: {
        view: 801,
    },
    ID: {
        view: 802,
        delete: 806,
        edit: 805,
        create: 803,
    },
};

export const useRolesAccess = () => {
    const { data: userData } = useCurrentUser();
    const access = useAccess(accessMatrix);

    const isIDForbidden = useIDForbidden(access);

    const viewListingAndFilters = 801;
    const viewDetailPage = 802;
    const createRoles = 803;
    const editRolesWithoutActiveAndDelete = 804;
    const editRoleActive = 805;
    const editDeleteRole = 806;

    const canViewListingAndFilters = useMemo(
        () => !!userData?.data.rights_access.includes(viewListingAndFilters),
        [userData?.data.rights_access]
    );
    const canViewDetailPage = useMemo(
        () => !!userData?.data.rights_access.includes(viewDetailPage),
        [userData?.data.rights_access]
    );
    const canCreateRoles = useMemo(
        () => !!userData?.data.rights_access.includes(createRoles),
        [userData?.data.rights_access]
    );
    const canEditRolesWithoutActiveAndDelete = useMemo(
        () => !!userData?.data.rights_access.includes(editRolesWithoutActiveAndDelete),
        [userData?.data.rights_access]
    );
    const canEditRoleActive = useMemo(
        () => !!userData?.data.rights_access.includes(editRoleActive),
        [userData?.data.rights_access]
    );
    const canEditDeleteRole = useMemo(
        () => !!userData?.data.rights_access.includes(editDeleteRole),
        [userData?.data.rights_access]
    );

    return {
        access,
        canViewListingAndFilters,
        canViewDetailPage,
        canCreateRoles,
        canEditRolesWithoutActiveAndDelete,
        canEditRoleActive,
        canEditDeleteRole,
        isIDForbidden,
    };
};
