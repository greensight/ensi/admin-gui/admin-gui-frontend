import {
    CopyButton,
    FormCheckbox,
    FormFieldWrapper,
    Input,
    SelectItem,
    SimpleSelect,
    useActionPopup,
} from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { ReactNode, useCallback, useEffect, useMemo, useState } from 'react';
import * as Yup from 'yup';

import {
    useAdminRole,
    useCreateAdminRole,
    useDeleteAdminRole,
    useRolesRightAccess,
    useUpdateAdminRole,
} from '@api/units';

import { useError, useSuccess } from '@context/modal';

import Switcher from '@controls/Switcher';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { CREATE_PARAM, DateFormatters, ErrorMessages, ModalMessages } from '@scripts/constants';
import { RedirectMethods } from '@scripts/enums';
import { Layout, colors, scale, typography } from '@scripts/gds';
import { formatDate, isPageNotFound } from '@scripts/helpers';
import { onlyRussianLettersDigits } from '@scripts/regex';

import { useRolesAccess } from './useRolesAccess';

const SideBarItem = ({ name, children }: { name: string; children?: ReactNode }) => (
    <div css={{ ...typography('bodySm'), ':not(:last-of-type)': { marginBottom: scale(1) } }}>
        <span css={{ marginRight: scale(1, true) }}>{name}:</span>
        {children || '-'}
    </div>
);

const RoleModelDetail = () => {
    const [isNeedRedirect, setIsNeedRedirect] = useState(false);
    const { query, pathname, replace } = useRouter();
    const listLink = pathname.split(`[id]`)[0];
    const id = Array.isArray(query.id) ? query.id[0] : query.id;
    const isCreation = id === CREATE_PARAM;

    const {
        canEditRolesWithoutActiveAndDelete,
        canEditRoleActive: canEditRole,
        canEditDeleteRole,
        canCreateRoles,
        isIDForbidden,
    } = useRolesAccess();

    const canEdit = isCreation ? canCreateRoles : canEditRolesWithoutActiveAndDelete;
    const canEditRoleActive = isCreation ? canCreateRoles : canEditRole || canEdit;

    const [activeGroups, setActiveGroups] = useState<SelectItem>({ value: '', label: '' });

    const { data: adminUserRoleDetail, isFetching: isDetailLoading, error: isDetailError } = useAdminRole(id);

    useError(isDetailError);

    const adminUserRoleDetailData = useMemo(() => adminUserRoleDetail?.data, [adminUserRoleDetail]);

    const pageTitle = isCreation ? 'Новая роль' : `Роль: ${adminUserRoleDetailData?.title}`;

    const {
        mutateAsync: createAdminRole,
        isPending: isCreateLoading,
        error: isCreateError,
        isSuccess: isCreateSuccess,
    } = useCreateAdminRole();

    useError(isCreateError);
    useSuccess(isCreateSuccess ? ModalMessages.SUCCESS_CREATE : '');

    const {
        data: rightAccessData,
        isFetching: isRightAccessLoading,
        error: isRightAccessError,
    } = useRolesRightAccess();

    const { popupState, popupDispatch, ActionPopup, ActionEnum, ActionType } = useActionPopup();

    useError(isRightAccessError);

    const {
        mutateAsync: updateAdminRole,
        isPending: isUpdateLoading,
        error: isUpdateError,
        isSuccess: isUpdateSuccess,
    } = useUpdateAdminRole(id);

    useError(isUpdateError);
    useSuccess(isUpdateSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const {
        mutateAsync: onAdminRoleDelete,
        isPending: isDeleteLoading,
        error: isDeleteError,
        isSuccess: isDeleteSuccess,
    } = useDeleteAdminRole();

    useError(isDeleteError);
    useSuccess(isDeleteSuccess ? ModalMessages.SUCCESS_DELETE : '');

    const rightAccessSelectItems = useMemo(
        () =>
            rightAccessData?.data?.map(item => ({
                value: item?.section,
                label: item?.section,
            })) || [],
        [rightAccessData?.data]
    );

    useEffect(() => {
        if (rightAccessSelectItems[0]) setActiveGroups(rightAccessSelectItems[0]);
    }, [rightAccessSelectItems]);

    const initialValues = useMemo(
        () => ({
            title: adminUserRoleDetailData?.title || '',
            active: adminUserRoleDetailData?.active || false,
            groups: rightAccessData?.data[0].section || '',
            rights_access: (adminUserRoleDetailData?.rights_access || [])
                .map(el => String(el))
                .sort((a, b) => {
                    if (a < b) return -1;
                    if (a > b) return 1;

                    return 0;
                }),
        }),
        [
            adminUserRoleDetailData?.active,
            adminUserRoleDetailData?.rights_access,
            adminUserRoleDetailData?.title,
            rightAccessData?.data,
        ]
    );

    const isNotFound = isPageNotFound({ id, error: isDetailError, isCreation });

    const [selectedRights, setSelectedRights] = useState<string[]>([]);

    const onChangeRightAccess = (rightId: string, isChecked: boolean) => {
        setSelectedRights(prev => {
            let updatedSection = prev;
            if (isChecked) {
                updatedSection = [...updatedSection, rightId];
            } else {
                updatedSection = updatedSection.filter(item => item !== rightId);
            }
            return updatedSection;
        });
    };

    const highlightOptions = useCallback(
        (option: SelectItem, selected: boolean, values: string[]) => {
            const sectionItems = rightAccessData?.data.find(ra => ra.section === option.value)?.items || [];

            const hasSelectedItems = sectionItems.some(item => values.includes(String(item.id)));

            return { ...(!selected && hasSelectedItems && { backgroundColor: colors?.lightBlue }) };
        },
        [rightAccessData?.data]
    );

    useEffect(() => {
        if (adminUserRoleDetailData) {
            const rightsArray = adminUserRoleDetailData.rights_access.map(String);
            setSelectedRights(rightsArray);
        }
    }, [adminUserRoleDetailData, adminUserRoleDetailData?.rights_access]);

    return (
        <PageWrapper
            title={pageTitle}
            isLoading={isDetailLoading || isCreateLoading || isUpdateLoading || isDeleteLoading || isRightAccessLoading}
            isNotFound={isNotFound}
            isForbidden={isIDForbidden}
        >
            <FormWrapper
                css={{ height: '100%' }}
                initialValues={initialValues}
                validationSchema={Yup.object().shape({
                    title: Yup.string()
                        .transform(e => {
                            if (!e) return null;
                            return e;
                        })
                        .matches(onlyRussianLettersDigits, 'Используйте кириллицу и цифры для заполнения')
                        .required(ErrorMessages.REQUIRED),
                })}
                disabled={!canEdit}
                enableReinitialize
                onSubmit={async values => {
                    if (isCreation) {
                        const { data: createdAdminRoleData } = await createAdminRole({
                            title: values.title,
                            active: values.active,
                            rights_access: values.rights_access.map((el: string) => Number(el)),
                        });

                        return {
                            method: isNeedRedirect ? RedirectMethods.push : RedirectMethods.replace,
                            redirectPath: isNeedRedirect ? listLink : `${listLink}${createdAdminRoleData?.id}`,
                        };
                    }

                    await updateAdminRole({
                        title: values.title,
                        active: values.active,
                        rights_access: values.rights_access.map((el: string) => Number(el)),
                    });

                    return {
                        method: isNeedRedirect ? RedirectMethods.push : RedirectMethods.replace,
                        redirectPath: isNeedRedirect ? listLink : `${listLink}${id}`,
                    };
                }}
            >
                {({ reset: resetForm }) => (
                    <PageTemplate
                        h1={pageTitle}
                        controls={
                            <PageControls
                                access={{
                                    ID: {
                                        create: true,
                                        edit: true,
                                        view: true,
                                        delete: canEditDeleteRole,
                                    },
                                    LIST: {
                                        view: true,
                                    },
                                }}
                            >
                                <PageControls.Delete
                                    onClick={() => {
                                        popupDispatch({
                                            type: ActionType.Delete,
                                            payload: {
                                                title: 'Вы уверены, что хотите удалить роль?',
                                                popupAction: ActionEnum.DELETE,
                                                onAction: async () => {
                                                    resetForm();
                                                    await onAdminRoleDelete(id);
                                                    replace({ pathname: listLink });
                                                },
                                                children: <p>Роль будет удалена из системы безвозвратно.</p>,
                                            },
                                        });
                                    }}
                                />
                                <PageControls.Close onClick={() => replace(listLink)} />
                                <PageControls.Apply
                                    onClick={() => {
                                        setIsNeedRedirect(false);
                                    }}
                                />
                                <PageControls.Save
                                    onClick={() => {
                                        setIsNeedRedirect(true);
                                    }}
                                />
                            </PageControls>
                        }
                        backlink={{ text: 'Назад', href: '/settings/users/roles' }}
                        aside={
                            <PageTemplate.FormPanel>
                                <FormFieldWrapper name="active" css={{ marginBottom: scale(3) }}>
                                    <Switcher disabled={!canEditRoleActive}>Активность</Switcher>
                                </FormFieldWrapper>

                                {!isCreation ? (
                                    <SideBarItem name="ID">
                                        {id ? <CopyButton>{String(id)}</CopyButton> : null}
                                    </SideBarItem>
                                ) : null}

                                <SideBarItem name="Изменено">
                                    {!isCreation && adminUserRoleDetailData?.updated_at
                                        ? formatDate(
                                              new Date(adminUserRoleDetailData?.updated_at),
                                              DateFormatters.DATE_AND_TIME
                                          )
                                        : '-'}
                                </SideBarItem>
                                <SideBarItem name="Создано">
                                    {!isCreation && adminUserRoleDetailData?.created_at
                                        ? formatDate(
                                              new Date(adminUserRoleDetailData?.created_at),
                                              DateFormatters.DATE_AND_TIME
                                          )
                                        : '-'}
                                </SideBarItem>
                            </PageTemplate.FormPanel>
                        }
                    >
                        <Layout cols={3} css={{ marginBottom: scale(4) }}>
                            <Layout.Item col={3}>
                                <FormFieldWrapper name="title" label="Название" disabled={!canEdit}>
                                    <Input />
                                </FormFieldWrapper>
                            </Layout.Item>
                            <Layout.Item col={3}>
                                <hr />
                            </Layout.Item>
                            <Layout.Item>
                                <SimpleSelect
                                    label="Раздел сайта"
                                    optionCSS={(option, selected) => highlightOptions(option, selected, selectedRights)}
                                    options={rightAccessSelectItems}
                                    selected={activeGroups}
                                    hideClearButton
                                    onChange={(evt, payLoad) =>
                                        setActiveGroups(
                                            payLoad?.actionItem || {
                                                value: '',
                                                label: '',
                                            }
                                        )
                                    }
                                />
                            </Layout.Item>
                            <Layout.Item col={2}>
                                {rightAccessData?.data?.map(item => (
                                    <div
                                        key={item?.section}
                                        css={{
                                            ...(activeGroups?.value !== item?.section && { display: 'none' }),
                                        }}
                                    >
                                        <fieldset>
                                            <legend css={{ marginBottom: scale(1), ...typography('bodySmBold') }}>
                                                Функции/вкладки
                                            </legend>

                                            {item?.items?.map(subItem => (
                                                <FormFieldWrapper
                                                    name="rights_access"
                                                    key={`right_access_check_${subItem?.id}`}
                                                >
                                                    <FormCheckbox
                                                        value={String(subItem?.id)}
                                                        disabled={!canEdit}
                                                        onChange={evt =>
                                                            onChangeRightAccess(
                                                                String(subItem.id),
                                                                evt.currentTarget.checked
                                                            )
                                                        }
                                                    >
                                                        {subItem?.title}
                                                    </FormCheckbox>
                                                </FormFieldWrapper>
                                            ))}
                                        </fieldset>
                                    </div>
                                ))}
                            </Layout.Item>
                        </Layout>
                        {canEditDeleteRole && <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />}
                    </PageTemplate>
                )}
            </FormWrapper>
            {canEditDeleteRole && <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />}
        </PageWrapper>
    );
};

export default RoleModelDetail;
