import { useActionPopup } from '@ensi-platform/core-components';
import { CellContext, createColumnHelper } from '@tanstack/react-table';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useMemo } from 'react';

import { useAdminRoles, useAdminRolesMeta, useDeleteAdminRole } from '@api/units';
import { Role } from '@api/units/types';

import { useError, useSuccess } from '@context/modal';

import ListBuilder from '@components/ListBuilder';
import { ExtendedRow, TableColumnDefAny, TooltipItem } from '@components/Table';

import { ModalMessages } from '@scripts/constants';
import { useLinkCSS } from '@scripts/hooks';
import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import { useRolesAccess } from './useRolesAccess';

export default function RoleList() {
    const { pathname } = useRouter();
    const { access } = useRolesAccess();
    const linkStyles = useLinkCSS();

    const { popupState, popupDispatch, ActionPopup, ActionEnum, ActionType } = useActionPopup();

    const {
        mutateAsync: onAdminRoleDelete,
        isPending: isDeleteLoading,
        error: isDeleteError,
        isSuccess: isDeleteSuccess,
    } = useDeleteAdminRole();

    useError(isDeleteError);
    useSuccess(isDeleteSuccess ? ModalMessages.SUCCESS_DELETE : '');

    const goToDetailPage = useGoToDetailPage({
        extraConditions: access.ID.view,
    });

    const onDelete = useCallback(
        async (originalRows: ExtendedRow['original'][] | undefined) => {
            if (originalRows) {
                popupDispatch({
                    type: ActionType.Delete,
                    payload: {
                        title: `Удалить роль?`,
                        popupAction: ActionEnum.DELETE,
                        onAction: async () => {
                            await onAdminRoleDelete(originalRows[0].original.id);
                        },
                        children: <p>Ролевая модель будет удалена вместе со всеми данными и настройками, удалить?</p>,
                    },
                });
            }
        },
        [ActionEnum.DELETE, ActionType.Delete, onAdminRoleDelete, popupDispatch]
    );

    const tooltipContent: TooltipItem[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Редактировать роль',
                action: goToDetailPage,
                isDisable: !access.ID.edit,
            },
            {
                type: 'delete',
                text: 'Удалить роль',
                action: onDelete,
                isDisable: !access.ID.delete,
            },
        ],
        [goToDetailPage, onDelete, access.ID.edit, access.ID.delete]
    );

    const TitleCell = useCallback(
        ({ row }: CellContext<Role, any>) =>
            access.ID.view ? (
                <Link legacyBehavior href={`${pathname}/${row.original.id}`} passHref>
                    <a css={linkStyles}>{row.original.title}</a>
                </Link>
            ) : (
                <span>{row.original.title}</span>
            ),
        [access.ID.view, linkStyles, pathname]
    );

    const columnHelper = createColumnHelper<Role>();
    const extraColumns: TableColumnDefAny[] = [
        columnHelper.accessor('id', {
            header: 'ID',
            cell: props => props.getValue(),
            enableHiding: true,
        }),
        columnHelper.accessor('title', {
            header: 'Наименование роли',
            cell: TitleCell,
            enableHiding: true,
        }),
    ];

    const isExtraLoading = isDeleteLoading;

    return (
        <ListBuilder<Role>
            access={access}
            searchHook={useAdminRoles}
            metaHook={useAdminRolesMeta}
            isLoading={isExtraLoading}
            extraColumns={extraColumns}
            tooltipItems={tooltipContent}
            title="Список ролевых моделей"
        >
            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </ListBuilder>
    );
}
