import { useMemo } from 'react';

import { Meta, MetaField } from '@api/common/types';
import { AdminUser, useAdminUsers, useAdminUsersMeta } from '@api/units';

import { useUsersAccess } from '@views/settings/users/useUsersAccess';

import ListBuilder from '@components/ListBuilder';
import { TooltipItem } from '@components/Table';

import { autoFilterData } from '@hooks/autoTable';

import { cleanPhoneValue } from '@scripts/helpers';
import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

export default function UsersList() {
    const { access } = useUsersAccess();

    const goToDetailPage = useGoToDetailPage({
        extraConditions: access.ID.view,
    });

    const tooltipContent: TooltipItem[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Редактировать пользователя',
                action: goToDetailPage,
                isDisable: !access.ID.view,
            },
        ],
        [access.ID.view, goToDetailPage]
    );

    const enrichUsersMetaData = (usersMeta?: Meta): Partial<Meta> => ({
        ...(usersMeta as Meta),
        fields:
            (usersMeta?.fields?.map((item: Partial<MetaField>) => ({
                ...item,
                ...(item?.code === 'phone' && { type: 'phone' }),
            })) as MetaField[]) || [],
    });

    const enrichUsersSearchRequest = ({ searchRequestFilter }: Partial<autoFilterData>) => ({
        filter: {
            ...searchRequestFilter,
            ...(searchRequestFilter?.phone_like && {
                phone_like: cleanPhoneValue(searchRequestFilter?.phone_like),
            }),
            ...(searchRequestFilter?.phone && {
                phone: cleanPhoneValue(searchRequestFilter?.phone),
            }),
        },
    });

    return (
        <ListBuilder<AdminUser>
            access={access}
            searchHook={useAdminUsers}
            enrichSearchHookRequest={enrichUsersSearchRequest}
            metaHook={useAdminUsersMeta}
            enrichMetaData={enrichUsersMetaData}
            tooltipItems={tooltipContent}
            title="Список пользователей"
        />
    );
}
