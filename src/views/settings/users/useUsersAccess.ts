import { defineAccessMatrix, useAccess } from '@scripts/hooks';
import { useIDForbidden } from '@scripts/hooks/useIDForbidden';

export const accessMatrix = defineAccessMatrix({
    LIST: {
        create: 707,
        view: 701,
        edit: 703,
        delete: 0,
    },
    ID: {
        view: 702,
        edit: 703,
        create: 707,
        delete: 0,
        ACTIVE: {
            edit: 704,
        },
        PASSWORD: {
            edit: 705,
        },
        ROLES: {
            edit: 706,
        },
    },
});

export const useUsersAccess = () => {
    const access = useAccess(accessMatrix);
    const isIDForbidden = useIDForbidden(access);

    return { access, isIDForbidden };
};
