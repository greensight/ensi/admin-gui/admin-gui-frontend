import {
    CopyButton,
    DescriptionList,
    DescriptionListItem,
    DescriptionListItemType,
    Tabs,
    getDefaultDates,
} from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { useMemo, useRef } from 'react';
import * as Yup from 'yup';

import { useCreateSeller, usePatchSeller, useSellerDetail } from '@api/units';

import { useError, useSuccess } from '@context/modal';

import UsersTable from '@views/sellers/list/[id]/UsersTable';
import { useSellerUsersAccess } from '@views/sellers/list/useSellerUsersAccess';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { CREATE_PARAM, ErrorMessages, ModalMessages } from '@scripts/constants';
import { RedirectMethods } from '@scripts/enums';
import { Layout, scale } from '@scripts/gds';
import { isPageNotFound } from '@scripts/helpers';
import { useTabs } from '@scripts/hooks/useTabs';
import validateInn from '@scripts/validateInn';

import { useSellersAccess } from '../useSellersAccess';
import Information from './Information';
import Stores from './Stores';

function SellerDetail() {
    const {
        query: { id },
        push,
    } = useRouter();

    const isCreation = id === CREATE_PARAM;
    const sellerId = +`${id}`;

    const {
        data: apiSeller,
        isFetching: isLoading,
        error,
    } = useSellerDetail(
        { id: sellerId },
        {
            include: 'operators',
        }
    );

    useError(error);

    const seller = apiSeller?.data;

    const { access, isIDForbidden } = useSellersAccess();
    const { access: usersAccess } = useSellerUsersAccess();

    const title = isCreation
        ? 'Создание продавца'
        : `${access.ID.edit ? 'Редактирование' : 'Просмотр'} продавца ${seller?.legal_name}`;

    const { getTabsProps } = useTabs();
    const redirectAfterSave = useRef(false);

    const initialValues = useMemo(
        () => ({
            legal_name: seller?.legal_name || '',
            inn: seller?.inn || '',
            kpp: seller?.kpp || '',
            payment_account: seller?.payment_account || '',
            correspondent_account: seller?.correspondent_account || '',
            bank_bik: seller?.bank_bik || '',
            bank: seller?.bank || '',
            status: seller?.status || '',
            manager_id: seller?.manager_id || '',
            legal_address: seller?.legal_address?.address_string || '',
            bank_address: seller?.bank_address?.address_string || '',
            fact_address: seller?.fact_address?.address_string || '',
            site: seller?.site || '',
            info: seller?.info || '',
        }),
        [seller]
    );

    const patchSeller = usePatchSeller();
    useError(patchSeller.error);
    useSuccess(patchSeller.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const createSeller = useCreateSeller();
    useError(createSeller.error);
    useSuccess(createSeller.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const isNotFound = isPageNotFound({ id: sellerId, error, isCreation });

    return (
        <PageWrapper
            title={title}
            isLoading={isLoading || patchSeller.isPending}
            isForbidden={isIDForbidden}
            isNotFound={isNotFound}
        >
            <FormWrapper
                disabled={isCreation ? !access.ID.create : !access.ID.view}
                initialValues={initialValues}
                onSubmit={async vals => {
                    const data = {
                        ...vals,
                        payment_account: `${vals.payment_account}`,
                        correspondent_account: `${vals.correspondent_account}`,
                        manager_id: +vals.manager_id,
                        legal_address: { address_string: vals.legal_address! },
                        fact_address: { address_string: vals.fact_address! },
                        bank_address: { address_string: vals.bank_address! },
                        status: +vals.status,
                    };
                    if (isCreation) {
                        const response = await createSeller.mutateAsync(data);

                        if (redirectAfterSave.current) {
                            return {
                                method: RedirectMethods.push,
                                redirectPath: '/sellers/list',
                            };
                        }

                        return {
                            method: RedirectMethods.replace,
                            redirectPath: `/sellers/list/${response.data.id}`,
                        };
                    }

                    await patchSeller.mutateAsync({
                        id: sellerId,
                        ...data,
                        // TODO: addresses
                    });

                    if (redirectAfterSave.current) {
                        return {
                            method: RedirectMethods.push,
                            redirectPath: '/sellers/list',
                        };
                    }

                    return {
                        method: RedirectMethods.replace,
                        redirectPath: `/sellers/list/${sellerId}`,
                    };
                }}
                validationSchema={Yup.object().shape({
                    legal_name: Yup.string().required(ErrorMessages.REQUIRED),
                    status: Yup.number()
                        .transform(e => (Number.isNaN(e) ? undefined : e))
                        .required(ErrorMessages.REQUIRED),
                    inn: Yup.string()
                        .test('inn', 'Проверьте формат ИНН', value => validateInn(value))
                        .test('inn-length', 'Введите ИНН юридического лица', value => value?.length !== 12),
                    bank_bik: Yup.string()
                        .test(
                            'bank_bik',
                            'БИК всегда состоит из 9 цифр',
                            value => !`${value}`.length || `${value}`.length === 9
                        )
                        .nullable()
                        .optional(),
                    kpp: Yup.string()
                        .test(
                            'kpp',
                            'КПП всегда состоит из 9 цифр',
                            value => !`${value}`.length || `${value}`.length === 9
                        )
                        .nullable()
                        .optional(),
                })}
                enableReinitialize
            >
                <PageTemplate
                    backlink={{
                        href: '/sellers/list',
                        text: 'Назад',
                    }}
                    h1={title}
                    customChildren
                    controls={
                        <PageControls access={access} gap={scale(1)}>
                            <PageControls.Close
                                onClick={() => {
                                    push('/sellers/list');
                                }}
                            />
                            <PageControls.Apply
                                onClick={() => {
                                    redirectAfterSave.current = false;
                                }}
                            />
                            <PageControls.Save
                                onClick={() => {
                                    redirectAfterSave.current = true;
                                }}
                            />
                        </PageControls>
                    }
                    aside={
                        <DescriptionList>
                            <DescriptionListItem
                                name="ID:"
                                value={!isCreation ? <CopyButton>{`${id}`}</CopyButton> : '-'}
                            />
                            {(getDefaultDates(seller || {}) as DescriptionListItemType[]).map(item => (
                                <DescriptionListItem key={item.name} {...item} />
                            ))}
                        </DescriptionList>
                    }
                >
                    <Layout cols={1} css={{ flexGrow: 2 }}>
                        <Tabs {...getTabsProps()}>
                            <Tabs.Tab title="Основные данные" id="0">
                                <Information />
                            </Tabs.Tab>
                            {!isCreation && (
                                <Tabs.Tab title="Склады" id="1">
                                    <Stores seller={seller} />
                                </Tabs.Tab>
                            )}
                            {!isCreation && (
                                <Tabs.Tab disabled={!usersAccess.LIST.view} title="Пользователи" id="2">
                                    <UsersTable seller={seller} />
                                </Tabs.Tab>
                            )}
                        </Tabs>
                    </Layout>
                </PageTemplate>
            </FormWrapper>
        </PageWrapper>
    );
}

export default SellerDetail;
