import { Block } from '@ensi-platform/core-components';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useMemo } from 'react';

import { Seller, SellerUser, Store, useSellerUsersMeta, useSellerUsersSearch } from '@api/units';

import { useError } from '@context/modal';

import { useSellerUsersAccess } from '@views/sellers/list/useSellerUsersAccess';

import ForbiddenPage from '@components/ForbiddenPage';
import Table, { TooltipItem, TrProps, useSorting, useTable } from '@components/Table';
import { getSettingsColumn } from '@components/Table/columns';
import { TableEmpty, TableFooter, TableHeader } from '@components/Table/components';
import RowTooltipWrapper from '@components/Table/components/RowTooltipWrapper';

import { useAutoColumns, useAutoFilters, useAutoTableData } from '@hooks/autoTable';

import { ITEMS_PER_PRODUCTS_PAGE } from '@scripts/constants';
import { Button, scale } from '@scripts/gds';
import { declOfNum, getPagination, getTotal, getTotalPages } from '@scripts/helpers';
import { useRedirectToNotEmptyPage, useTableList } from '@scripts/hooks';
import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import PlusIcon from '@icons/plus.svg';

const UsersTable = ({ seller }: { seller?: Seller }) => {
    const { access } = useSellerUsersAccess();
    const { push, pathname, query } = useRouter();

    const pageKey = 'seller-users-page';

    const { id } = query;

    const { data: metaData, error: metaError } = useSellerUsersMeta(access.LIST.view);
    useError(metaError);
    const meta = useMemo(() => metaData?.data, [metaData]);

    const { metaField, codesToSortKeys } = useAutoFilters(meta);

    const [{ backendSorting }, sortingPlugin] = useSorting<Store>(pathname, meta?.fields, {
        id: meta?.default_sort || 'id',
        desc: meta?.default_sort !== '-id',
    });

    const { activePage, itemsPerPageCount, setItemsPerPageCount } = useTableList({
        defaultPerPage: ITEMS_PER_PRODUCTS_PAGE,
        codesToSortKeys,
        pageKey,
    });

    const columns = useAutoColumns(metaData?.data, access.LIST.view);

    const { data: sellerUsers, error } = useSellerUsersSearch(
        {
            filter: { seller_id: Number(id) },
            sort: backendSorting,
            pagination: getPagination(activePage, itemsPerPageCount),
        },
        access.LIST.view && !!backendSorting
    );
    useError(error);

    const goToDetailPage = useGoToDetailPage({
        pathname: '/sellers/list/seller-user',
        extraConditions: access.ID.view,
        query: { seller_id: seller?.id },
    });

    const tooltipItems: TooltipItem[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Редактировать пользователя',
                action: goToDetailPage,
                isDisable: !access.ID.view,
            },
        ],
        [access.ID.view, goToDetailPage]
    );

    const total = getTotal(sellerUsers);
    const totalPages = getTotalPages(sellerUsers, itemsPerPageCount);
    const rows = useAutoTableData<SellerUser>(sellerUsers?.data, metaField);

    const table = useTable(
        {
            data: rows,
            columns: [
                ...columns,
                getSettingsColumn({
                    visibleColumns: meta?.default_list,
                    tooltipContent: tooltipItems,
                }),
            ],
            meta: {
                tableKey: `seller_users`,
            },
        },
        [sortingPlugin]
    );

    useRedirectToNotEmptyPage({ activePage, itemsPerPageCount, total, shallow: true, pageKey });

    const getTooltipForRow = useCallback(() => tooltipItems, [tooltipItems]);

    const renderRow = useCallback(
        ({ children, ...props }: TrProps<any>) => (
            <RowTooltipWrapper
                {...props}
                getTooltipForRow={getTooltipForRow}
                onDoubleClick={() => {
                    push(`/sellers/list/seller-user/${props.row?.original.id}?seller_id=${seller?.id}`);
                }}
            >
                {children}
            </RowTooltipWrapper>
        ),
        [getTooltipForRow, push]
    );

    return (
        <Block css={{ marginBottom: scale(3) }}>
            {!access.LIST.view ? (
                <ForbiddenPage />
            ) : (
                <Block.Body>
                    <TableHeader css={{ justifyContent: 'space-between' }}>
                        <p>
                            Найдено {`${total} ${declOfNum(total, ['пользователь', 'пользователя', 'пользователей'])}`}
                        </p>
                        <div css={{ display: 'flex' }}>
                            {access.ID.create && (
                                <Link href={`/sellers/list/seller-user/create?seller_id=${seller?.id}`} passHref>
                                    <Button Icon={PlusIcon} as="a">
                                        Создать нового пользователя
                                    </Button>
                                </Link>
                            )}
                        </div>
                    </TableHeader>

                    <Table instance={table} Tr={renderRow} />
                    {rows.length === 0 ? (
                        <TableEmpty
                            filtersActive={false}
                            titleWithFilters="Пользователи не найдены"
                            titleWithoutFilters="Нет пользователей"
                            addItems={() => push(`${pathname}/create`)}
                        />
                    ) : (
                        <TableFooter
                            pages={totalPages}
                            itemsPerPageCount={itemsPerPageCount}
                            setItemsPerPageCount={setItemsPerPageCount}
                            pageKey={pageKey}
                        />
                    )}
                </Block.Body>
            )}
        </Block>
    );
};

export default UsersTable;
