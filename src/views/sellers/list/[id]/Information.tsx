import { Block, FormField, FormFieldWrapper, FormTypedField, Textarea } from '@ensi-platform/core-components';
import { useMemo } from 'react';

import { useSellerStatuses } from '@api/units';

import Mask from '@controls/Mask';
import Select from '@controls/Select';

import { Layout } from '@scripts/gds';
import { toFutureSelectItems } from '@scripts/helpers';
import { maskInn, maskNineDigits } from '@scripts/mask';

import { ManagerField } from '../components/ManagerField';

function Information() {
    const { data: apiStatuses } = useSellerStatuses();
    const statusOptions = useMemo(() => toFutureSelectItems(apiStatuses?.data), [apiStatuses?.data]);

    return (
        <Block css={{ width: '100%' }}>
            <Block.Body>
                <Layout cols={2}>
                    <Layout.Item>
                        <FormField name="legal_name" label="Юридическое наименование" />
                    </Layout.Item>
                    <Layout.Item>
                        <FormField name="legal_address" label="Юридический адрес" />
                    </Layout.Item>
                    <Layout.Item>
                        <FormFieldWrapper name="inn" label="ИНН">
                            <Mask mask={maskInn} />
                        </FormFieldWrapper>
                    </Layout.Item>
                    <Layout.Item>
                        <FormFieldWrapper name="kpp" label="КПП">
                            <Mask mask={maskNineDigits} />
                        </FormFieldWrapper>
                    </Layout.Item>
                    <Layout.Item>
                        <FormTypedField
                            fieldType="positiveInt"
                            dataType="string"
                            name="payment_account"
                            label="Расчетный счет"
                        />
                    </Layout.Item>
                    <Layout.Item>
                        <FormTypedField
                            fieldType="positiveInt"
                            dataType="string"
                            name="correspondent_account"
                            label="Корреспондентский счет"
                        />
                    </Layout.Item>
                    <Layout.Item>
                        <FormFieldWrapper name="bank_bik" label="БИК">
                            <Mask mask={maskNineDigits} />
                        </FormFieldWrapper>
                    </Layout.Item>
                    <Layout.Item>
                        <FormField name="bank" label="Юридическое наименование банка" />
                    </Layout.Item>
                    <Layout.Item>
                        <FormField name="bank_address" label="Адрес банка" />
                    </Layout.Item>
                    <Layout.Item>
                        <FormFieldWrapper name="status" label="Статус">
                            <Select options={statusOptions} hideClearButton />
                        </FormFieldWrapper>
                    </Layout.Item>
                    <Layout.Item>
                        <ManagerField />
                    </Layout.Item>
                    <Layout.Item>
                        <FormField name="fact_address" label="Фактический адрес" />
                    </Layout.Item>
                    <Layout.Item>
                        <FormField name="site" label="Сайт продавца" />
                    </Layout.Item>
                    <Layout.Item col={2}>
                        <FormFieldWrapper name="info" label="Информация о продавце">
                            <Textarea minRows={5} />
                        </FormFieldWrapper>
                    </Layout.Item>
                </Layout>
            </Block.Body>
        </Block>
    );
}

export default Information;
