/* eslint-disable react/no-unstable-nested-components */
import { Block } from '@ensi-platform/core-components';
import { ColumnSort, createColumnHelper } from '@tanstack/react-table';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useMemo } from 'react';

import { Seller, Store, useSearchStores } from '@api/units';

import { useError } from '@context/modal';

import Table, { TooltipItem, TrProps, useSorting, useTable } from '@components/Table';
import { Cell, RowTooltipWrapper } from '@components/Table/components';

import { scale } from '@scripts/gds';
import { useLinkCSS } from '@scripts/hooks';

const initialSort: ColumnSort = {
    id: 'id',
    desc: false,
};

const columnHelper = createColumnHelper<Store>();

const Stores = ({ seller }: { seller?: Seller }) => {
    const { push } = useRouter();
    const [{ sorting }, sortingPlugin] = useSorting<Store>('seller_stores', [], initialSort, false);

    const linkStyles = useLinkCSS();

    const columns = useMemo(
        () => [
            columnHelper.accessor('id', {
                header: 'ID',
                cell: ({ getValue }) => (
                    <Link legacyBehavior href={`/stores/list/${getValue()}`} passHref>
                        <a css={linkStyles}>{getValue()}</a>
                    </Link>
                ),
                enableHiding: true,
            }),
            columnHelper.accessor('name', {
                header: 'Наименование',
                cell: props => props.getValue(),
                enableHiding: true,
            }),
            columnHelper.accessor('created_at', {
                header: 'Дата создания',
                cell: ({ getValue, ...props }) => <Cell type="datetime" value={getValue()} {...props} />,
                enableHiding: true,
                enableSorting: false,
            }),
            columnHelper.accessor('updated_at', {
                header: 'Дата обновления',
                cell: ({ getValue, ...props }) => <Cell type="datetime" value={getValue()} {...props} />,
                enableHiding: true,
                enableSorting: false,
            }),
        ],
        [linkStyles]
    );

    const { data: apiStores, error } = useSearchStores(
        {
            ...(seller?.id && {
                filter: {
                    seller_id: seller?.id,
                },
            }),
            pagination: { limit: -1, offset: 0, type: 'offset' },
        },
        !Number.isNaN(seller?.id)
    );
    useError(error);

    const stores = useMemo(
        () =>
            (apiStores?.data || []).sort((a, b) => {
                if (sorting.length === 0) return 0;
                const sorted = sorting[0];
                if (sorted.id === 'name') {
                    if (a.name < b.name) return sorted.desc ? -1 : 1;
                    if (a.name > b.name) return sorted.desc ? 1 : -1;
                    return 0;
                }

                if (sorted.id !== 'id') return 0;

                const idDiff = b.id - a.id;
                return sorted.desc ? idDiff : -idDiff;
            }) || [],
        [apiStores?.data, sorting]
    );

    const table = useTable(
        {
            data: stores,
            columns,
            meta: {
                tableKey: `seller_stores`,
            },
        },
        [sortingPlugin]
    );

    const tooltipItems = useMemo(
        (): TooltipItem[] => [
            {
                text: `Перейти в склад`,
                type: 'edit',
                action(row) {
                    if (row) push(`/stores/list/${row[0].original.id}`);
                },
                isDisable: false,
            },
        ],
        [push]
    );

    const getTooltipForRow = useCallback(() => tooltipItems, [tooltipItems]);

    const renderRow = useCallback(
        ({ children, ...props }: TrProps<any>) => (
            <RowTooltipWrapper
                {...props}
                getTooltipForRow={getTooltipForRow}
                onDoubleClick={() => {
                    push(`/stores/list/${props.row?.original.id}`);
                }}
            >
                {children}
            </RowTooltipWrapper>
        ),
        [getTooltipForRow, push]
    );

    return (
        <Block css={{ marginBottom: scale(3) }}>
            <Block.Body>
                <Table instance={table} Tr={renderRow} />
            </Block.Body>
        </Block>
    );
};

export default Stores;
