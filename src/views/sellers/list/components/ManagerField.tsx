import { AutocompleteAsync, FormFieldWrapper } from '@ensi-platform/core-components';

import { useGetAdminUsersOptionsByValuesFn, useGetAdminUsersSearchFn } from '@api/units';

export const ManagerField = () => {
    const { adminUsersSearchFn } = useGetAdminUsersSearchFn();
    const { adminUsersOptionsByValuesFn } = useGetAdminUsersOptionsByValuesFn();

    return (
        <FormFieldWrapper name="manager_id" label="Персональный менеджер продавца">
            <AutocompleteAsync
                moveInputToNewLine={false}
                asyncSearchFn={adminUsersSearchFn}
                asyncOptionsByValuesFn={adminUsersOptionsByValuesFn}
            />
        </FormFieldWrapper>
    );
};
