import { useRouter } from 'next/router';
import { useMemo } from 'react';

import { SellerData, useSearchSellers, useSellersMeta } from '@api/units';

import { useSellerUsersAccess } from '@views/sellers/list/useSellerUsersAccess';

import ListBuilder from '@components/ListBuilder';
import { TooltipItem } from '@components/Table';

import { useSellersAccess } from './useSellersAccess';

const SellersList = () => {
    const { access } = useSellersAccess();
    const { access: usersAccess } = useSellerUsersAccess();

    const { pathname, push } = useRouter();

    const tooltipItems = useMemo(
        (): TooltipItem[] => [
            {
                text: access.ID.edit ? 'Редактировать продавца' : 'Просмотреть продавца',
                type: 'edit',
                action(row) {
                    if (row) push(`${pathname}/${row[0].original.id}`);
                },
                isDisable: !access.ID.view,
            },
        ],
        [access.ID.edit, access.ID.view, pathname, push]
    );

    return (
        <ListBuilder<SellerData>
            access={{
                ...access,
                LIST: {
                    view: access.LIST.view,
                    create: usersAccess.ID.create,
                },
            }}
            searchHook={useSearchSellers}
            metaHook={useSellersMeta}
            tooltipItems={tooltipItems}
            title="Список продавцов"
        />
    );
};

export default SellersList;
