import { defineAccessMatrix, useAccess } from '@scripts/hooks';
import { useIDForbidden } from '@scripts/hooks/useIDForbidden';

export const accessMatrix = defineAccessMatrix({
    LIST: {
        /**
         * Пользователю доступен табличный список пользователей продавца
         */
        view: 3001,
    },
    ID: {
        /**
         * Пользователю доступна детальная страница пользователя продавца
         */
        view: 3002,
        /**
         * Пользователю доступно редактирование пользователя продавца
         */
        edit: 3003,
        /**
         * Пользователю доступно создание пользователя продавца
         */
        create: 3004,
        /**
         * Пользователю доступно изменение активности пользователя продавца
         */
        activityEdit: 3005,
        /**
         * Пользователю доступно редактирование пароля пользователя продавца
         */
        passwordEdit: 3006,
    },
});

export const useSellerUsersAccess = () => {
    const access = useAccess(accessMatrix);
    const isIDForbidden = useIDForbidden(access);

    return { access, isIDForbidden };
};
