import {
    Block,
    CopyButton,
    DescriptionList,
    DescriptionListItem,
    DescriptionListItemType,
    FormField,
    FormFieldWrapper,
    getDefaultDates,
} from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { useMemo, useRef } from 'react';
import * as Yup from 'yup';

import { SellerUserMutate, useCreateSellerUser, useSellerUser, useUpdateSellerUser } from '@api/units';

import { useError, useSuccess } from '@context/modal';

import { useSellerUsersAccess } from '@views/sellers/list/useSellerUsersAccess';

import Mask from '@controls/Mask';
import Password from '@controls/Password';
import Switcher from '@controls/Switcher';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { CREATE_PARAM, ErrorMessages, ModalMessages } from '@scripts/constants';
import { RedirectMethods } from '@scripts/enums';
import { Layout, scale } from '@scripts/gds';
import { cleanPhoneValue, prepareTelValue } from '@scripts/helpers';
import { maskPhone } from '@scripts/mask';
import { regNameRu, regOneDigit, regOneLetter, regPhone } from '@scripts/regex';

function SellerUserDetail() {
    const {
        query: { id, seller_id },
        push,
    } = useRouter();
    const { access, isIDForbidden } = useSellerUsersAccess();

    const isCreation = id === CREATE_PARAM;
    const userId = +`${id}`;

    const { data: apiSellerUser, isFetching: isLoading } = useSellerUser(userId, !Number.isNaN(userId));
    const sellerUser = apiSellerUser?.data;

    const title = isCreation
        ? 'Создание пользователя продавца'
        : `${access.ID.edit ? 'Редактирование' : 'Просмотр'} пользователя продавца ${apiSellerUser?.data.seller_id}`;

    const redirectAfterSave = useRef(false);

    const initialValues = useMemo(
        () => ({
            seller_id: sellerUser?.seller_id || '',
            active: sellerUser?.active || false,
            login: sellerUser?.login || '',
            last_name: sellerUser?.last_name || '',
            first_name: sellerUser?.first_name || '',
            middle_name: sellerUser?.middle_name || '',
            email: sellerUser?.email || '',
            phone: sellerUser?.phone ? prepareTelValue(sellerUser.phone) : '',
            password: null,
        }),
        [sellerUser]
    );

    const updateSellerUser = useUpdateSellerUser();
    useError(updateSellerUser.error);
    useSuccess(updateSellerUser.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const createSellerUser = useCreateSellerUser();
    useError(createSellerUser.error);
    useSuccess(createSellerUser.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    return (
        <PageWrapper title={title} isLoading={isLoading || updateSellerUser.isPending} isForbidden={isIDForbidden}>
            <FormWrapper
                disabled={isCreation ? !access.ID.create : !access.ID.edit}
                initialValues={initialValues}
                onSubmit={async values => {
                    if (isCreation) {
                        const data = {
                            ...values,
                            phone: cleanPhoneValue(values?.phone),
                            seller_id: Number(seller_id),
                        };

                        const response = await createSellerUser.mutateAsync(data);

                        if (redirectAfterSave.current) {
                            return {
                                method: RedirectMethods.push,
                                redirectPath: `/sellers/list/${seller_id}`,
                            };
                        }

                        return {
                            method: RedirectMethods.replace,
                            redirectPath: `/sellers/list/seller-user/${response.data.id}`,
                            ...(seller_id &&
                                typeof seller_id === 'string' && {
                                    query: { seller_id },
                                }),
                        };
                    }
                    const clearValues = {
                        ...values,
                        ...(values?.phone !== initialValues?.phone && {
                            phone: cleanPhoneValue(values?.phone),
                        }),
                    };

                    const changedValues = Object.keys(clearValues).reduce(
                        (acc, cur) => {
                            const key = cur as keyof typeof values;
                            if (clearValues[key] !== initialValues[key]) return { ...acc, [key]: values[key] };
                            return acc;
                        },
                        {} as typeof values
                    );

                    const enrichedData = {
                        ...changedValues,
                        ...(changedValues?.phone && {
                            phone: cleanPhoneValue(changedValues?.phone),
                        }),
                    } as SellerUserMutate;

                    if (Object.keys(changedValues)?.length) {
                        await updateSellerUser.mutateAsync({
                            id: userId,
                            ...enrichedData,
                        });
                    }

                    if (redirectAfterSave.current) {
                        return {
                            method: RedirectMethods.push,
                            redirectPath: `/sellers/list/${seller_id}`,
                        };
                    }

                    return {
                        method: RedirectMethods.replace,
                        redirectPath: `/sellers/list/seller-user/${userId}`,
                        ...(seller_id &&
                            typeof seller_id === 'string' && {
                                query: { seller_id },
                            }),
                    };
                }}
                validationSchema={Yup.object().shape({
                    active: Yup.boolean(),
                    login: Yup.string().required(ErrorMessages.REQUIRED),
                    first_name: Yup.string()
                        .matches(regNameRu, 'Используйте кириллицу для заполнения')
                        .required(ErrorMessages.REQUIRED),
                    last_name: Yup.string()
                        .matches(regNameRu, 'Используйте кириллицу для заполнения')
                        .required(ErrorMessages.REQUIRED),
                    middle_name: Yup.string()
                        .matches(regNameRu, 'Используйте кириллицу для заполнения')
                        .required(ErrorMessages.REQUIRED),
                    email: Yup.string().email(ErrorMessages.EMAIL).required(ErrorMessages.REQUIRED),
                    phone: Yup.string().matches(regPhone, 'Проверьте телефонный формат').required('Обязательное поле'),
                    password: Yup.string()
                        .nullable()
                        .matches(regOneLetter, 'Пароль должен содержать хотя бы 1 латинскую букву')
                        .matches(regOneDigit, 'Пароль должен содержать хотя бы 1 цифру')
                        .min(8, 'Пароль должен быть не менее 8 символов')
                        .when('$isCreation', (_, schema) =>
                            isCreation ? schema.required(ErrorMessages.REQUIRED) : schema.nullable()
                        ),
                })}
                enableReinitialize
            >
                <PageTemplate
                    backlink={{
                        href: `/sellers/list/${seller_id}?tab=2`,
                        text: 'Назад',
                    }}
                    h1={title}
                    customChildren
                    controls={
                        <PageControls access={access} gap={scale(1)}>
                            <PageControls.Close
                                onClick={() => {
                                    push(`/sellers/list/${seller_id}?tab=2`);
                                }}
                            />
                            <PageControls.Apply
                                onClick={() => {
                                    redirectAfterSave.current = false;
                                }}
                            />
                            <PageControls.Save
                                onClick={() => {
                                    redirectAfterSave.current = true;
                                }}
                            />
                        </PageControls>
                    }
                    aside={
                        <DescriptionList>
                            <FormFieldWrapper
                                name="active"
                                label="Активность"
                                disabled={
                                    !(access.ID.activityEdit || access.ID.edit || (isCreation && access.ID.create))
                                }
                                css={{ marginBottom: scale(2) }}
                            >
                                <Switcher>Активность</Switcher>
                            </FormFieldWrapper>
                            <DescriptionListItem
                                name="ID:"
                                value={!isCreation ? <CopyButton>{`${id}`}</CopyButton> : '-'}
                            />
                            {(getDefaultDates(sellerUser || {}) as DescriptionListItemType[]).map(item => (
                                <DescriptionListItem key={item.name} {...item} />
                            ))}
                        </DescriptionList>
                    }
                >
                    <Block css={{ width: '100%' }}>
                        <Block.Body>
                            <Layout cols={2}>
                                <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                    <FormField name="login" label="Логин" autoComplete="off" />
                                </Layout.Item>
                                <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                    <FormField name="last_name" label="Фамилия" />
                                </Layout.Item>
                                <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                    <FormField name="first_name" label="Имя" />
                                </Layout.Item>
                                <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                    <FormField name="middle_name" label="Отчество" />
                                </Layout.Item>
                                <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                    <FormField name="email" label="Email" autoComplete="off" />
                                </Layout.Item>
                                <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                    <FormFieldWrapper name="phone" label="Телефон">
                                        <Mask mask={maskPhone} />
                                    </FormFieldWrapper>
                                </Layout.Item>
                                <Layout.Item
                                    col={{
                                        xxxl: 2,
                                        xs: 2,
                                    }}
                                >
                                    <FormFieldWrapper
                                        name="password"
                                        disabled={
                                            !(
                                                access.ID.passwordEdit ||
                                                access.ID.edit ||
                                                (isCreation && access.ID.create)
                                            )
                                        }
                                    >
                                        <Password
                                            label="Пароль"
                                            hint="Пароль должен быть не менее 8 символов и содержать минимум 1 латинский символ"
                                            autoComplete="new-password"
                                        />
                                    </FormFieldWrapper>
                                </Layout.Item>
                            </Layout>
                        </Block.Body>
                    </Block>
                </PageTemplate>
            </FormWrapper>
        </PageWrapper>
    );
}

export default SellerUserDetail;
