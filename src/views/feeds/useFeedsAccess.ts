import { defineAccessMatrix, useAccess } from '@scripts/hooks';
import { useIDForbidden } from '@scripts/hooks/useIDForbidden';

export const accessMatrix = defineAccessMatrix({
    LIST: {
        create: 2003,
        view: 2001,
        delete: -1,
        edit: 2004,
    },
    ID: {
        delete: -1,
        view: 2002,
        create: 2003,
        edit: 2004,
    },
});

export const useFeedsAccess = () => {
    const access = useAccess(accessMatrix);
    const isIDForbidden = useIDForbidden(access);

    return { access, isIDForbidden };
};
