import { useMemo } from 'react';

import { useFeedSettingsMeta, useSearchFeedSettings } from '@api/catalog/feeds';
import { FeedSettings } from '@api/catalog/types/feeds';

import ListBuilder from '@components/ListBuilder';
import { TooltipItem } from '@components/Table';

import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import { useFeedsAccess } from './useFeedsAccess';

export default function FeedsList() {
    const { access } = useFeedsAccess();
    const goToDetailPage = useGoToDetailPage();

    const tooltipContent = useMemo<TooltipItem[]>(
        () => [
            {
                type: 'edit',
                text: 'Редактировать шаблон фида',
                action: goToDetailPage,
                isDisable: !access.ID.view,
            },
        ],
        [access.ID.view, goToDetailPage]
    );

    return (
        <ListBuilder<FeedSettings>
            access={access}
            searchHook={useSearchFeedSettings}
            metaHook={useFeedSettingsMeta}
            tooltipItems={tooltipContent}
            title="Шаблоны фидов"
        />
    );
}
