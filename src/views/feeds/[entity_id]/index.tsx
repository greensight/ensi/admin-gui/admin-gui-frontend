/* eslint-disable @next/next/no-img-element */
import {
    DescriptionList,
    DescriptionListItem,
    FormField,
    FormFieldWrapper,
    FormTypedField,
    Select,
    getDefaultDates,
} from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { useMemo, useRef } from 'react';
import * as Yup from 'yup';

import {
    useCreateFeedSettings,
    useFeedPlatforms,
    useFeedSettings,
    useFeedTypes,
    usePatchFeedSettings,
    useSearchFeeds,
} from '@api/catalog/feeds';
import { FeedSettingsForCreate, FeedSettingsForPatch } from '@api/catalog/types/feeds';

import { useError, useSuccess } from '@context/modal';

import Mask from '@controls/Mask';
import Switcher from '@controls/Switcher';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { CREATE_PARAM, ErrorMessages, ModalMessages } from '@scripts/constants';
import { RedirectMethods } from '@scripts/enums';
import { Button, Layout, scale } from '@scripts/gds';
import { isPageNotFound, toSelectItems } from '@scripts/helpers';

import { useFeedsAccess } from '../useFeedsAccess';

const LISTING_URL = '/feeds';

const useOptions = () => {
    const { data: apiTypes } = useFeedTypes();
    const { data: apiPlatforms } = useFeedPlatforms();

    const types = useMemo(() => toSelectItems(apiTypes?.data), [apiTypes?.data]);
    const platforms = useMemo(() => toSelectItems(apiPlatforms?.data), [apiPlatforms?.data]);

    return { types, platforms };
};

const useMostRecentFeed = (feedSettingsId?: number) => {
    const { data: apiFeeds } = useSearchFeeds(
        {
            filter: {
                feed_settings_id: feedSettingsId,
            },
            sort: '-id',
            pagination: {
                limit: 1,
                offset: 0,
                type: 'offset',
            },
        },
        !Number.isNaN(feedSettingsId)
    );

    return useMemo(() => apiFeeds?.data?.at(0) || undefined, [apiFeeds?.data]);
};

export default function Feed() {
    const { access, isIDForbidden } = useFeedsAccess();

    const { query, push } = useRouter();
    const isCreation = query.entity_id === CREATE_PARAM;
    const entityId = +`${query?.entity_id}`;

    const { data: apiReview, isFetching: isLoading, error } = useFeedSettings({ id: entityId }, access.ID.view);
    useError(error);
    const feedSettings = apiReview?.data;

    const title = isCreation ? 'Создание шаблона фидов' : `Шаблон фидов #${entityId}`;
    const redirectAfterSave = useRef(false);

    const initialValues = useMemo(
        () => ({
            name: feedSettings?.name || '',
            code: feedSettings?.code || '',
            active: feedSettings?.active || false,
            type: feedSettings?.type || '',
            platform: feedSettings?.platform || '',
            active_product: feedSettings?.active_product || false,
            active_category: feedSettings?.active_category || false,
            shop_name: feedSettings?.shop_name || '',
            shop_url: feedSettings?.shop_url || '',
            shop_company: feedSettings?.shop_company || '',
            update_time: feedSettings?.update_time || '',
            delete_time: feedSettings?.delete_time || '',
        }),
        [feedSettings]
    );

    const createFeedSettings = useCreateFeedSettings();
    useError(createFeedSettings.error);
    useSuccess(createFeedSettings.isSuccess && ModalMessages.SUCCESS_CREATE);

    const patchFeedSettings = usePatchFeedSettings();
    useError(patchFeedSettings.error);
    useSuccess(patchFeedSettings.isSuccess && ModalMessages.SUCCESS_UPDATE);

    const { types, platforms } = useOptions();
    const recentFeed = useMostRecentFeed(entityId);

    const canEdit = isCreation ? access.ID.create : access.ID.edit;

    const isNotFound = isPageNotFound({ id: entityId, error, isCreation });

    return (
        <PageWrapper
            title={title}
            isLoading={isLoading || patchFeedSettings.isPending || createFeedSettings.isPending}
            isNotFound={isNotFound}
            isForbidden={isIDForbidden}
        >
            <FormWrapper
                disabled={!canEdit}
                initialValues={initialValues}
                enableReinitialize
                validationSchema={Yup.object().shape({
                    name: Yup.string().required(ErrorMessages.REQUIRED),
                    code: Yup.string().required(ErrorMessages.REQUIRED),
                    type: Yup.number()
                        .transform(value => (Number.isNaN(+value) ? undefined : value))
                        .required(ErrorMessages.REQUIRED),
                    platform: Yup.number()
                        .transform(value => (Number.isNaN(+value) ? undefined : value))
                        .required(ErrorMessages.REQUIRED),
                    shop_name: Yup.string().required(ErrorMessages.REQUIRED),
                    shop_url: Yup.string().required(ErrorMessages.REQUIRED),
                    shop_company: Yup.string().required(ErrorMessages.REQUIRED),
                    update_time: Yup.number()
                        .transform(value => (Number.isNaN(+value) ? undefined : value))
                        .required(ErrorMessages.REQUIRED),
                    delete_time: Yup.number()
                        .transform(value => (Number.isNaN(+value) ? undefined : value))
                        .required(ErrorMessages.REQUIRED),
                })}
                onSubmit={async vals => {
                    if (isCreation) {
                        const result = await createFeedSettings.mutateAsync(vals as FeedSettingsForCreate);

                        return {
                            redirectPath: redirectAfterSave.current ? LISTING_URL : `${LISTING_URL}/${result.data.id}`,
                            method: RedirectMethods.push,
                        };
                    }

                    await patchFeedSettings.mutateAsync({
                        id: entityId,
                        ...(vals as FeedSettingsForPatch),
                    });

                    return {
                        redirectPath: redirectAfterSave.current ? LISTING_URL : `${LISTING_URL}/${entityId}`,
                        method: RedirectMethods.replace,
                    };
                }}
            >
                <PageTemplate
                    backlink={{
                        text: 'Назад',
                        href: LISTING_URL,
                    }}
                    aside={
                        <DescriptionList>
                            <DescriptionListItem
                                name="Активность"
                                value={
                                    <FormFieldWrapper name="active" label="Активность">
                                        <Switcher>Активность</Switcher>
                                    </FormFieldWrapper>
                                }
                            />
                            <DescriptionListItem name="ID" value={entityId} />
                            {recentFeed?.file_url && (
                                <DescriptionListItem
                                    name="Итоговый фид"
                                    value={
                                        <Button as="a" href={recentFeed?.file_url}>
                                            Скачать
                                        </Button>
                                    }
                                />
                            )}
                            {getDefaultDates({ ...feedSettings }).map(item => (
                                <DescriptionListItem {...item} key={item.name} />
                            ))}
                        </DescriptionList>
                    }
                    controls={
                        <PageControls access={access} gap={scale(1)}>
                            <PageControls.Close
                                onClick={() => {
                                    push(LISTING_URL);
                                }}
                            />
                            <PageControls.Apply
                                onClick={() => {
                                    redirectAfterSave.current = false;
                                }}
                            />
                            <PageControls.Save
                                onClick={() => {
                                    redirectAfterSave.current = true;
                                }}
                            />
                        </PageControls>
                    }
                    h1={title}
                >
                    <Layout cols={2} gap={[scale(2), scale(4)]}>
                        <Layout.Item>
                            <FormField name="name" label="Наименование" />
                        </Layout.Item>
                        <Layout.Item>
                            <FormFieldWrapper name="code" label="Код шаблона">
                                <Mask mask={/^[a-z\d/.-_\\-]+$/i} />
                            </FormFieldWrapper>
                        </Layout.Item>
                        <Layout.Item>
                            <FormTypedField
                                name="update_time"
                                label="Частота обновления, в часах"
                                fieldType="positiveInt"
                            />
                        </Layout.Item>
                        <Layout.Item>
                            <FormTypedField
                                name="delete_time"
                                label="Срок хранения старых версий, в днях"
                                fieldType="positiveInt"
                            />
                        </Layout.Item>
                        <Layout.Item>
                            <FormFieldWrapper name="active_product">
                                <Switcher>Только активные товары</Switcher>
                            </FormFieldWrapper>
                        </Layout.Item>
                        <Layout.Item>
                            <FormFieldWrapper name="active_category">
                                <Switcher>Только активные категории</Switcher>
                            </FormFieldWrapper>
                        </Layout.Item>
                        <Layout.Item>
                            <FormFieldWrapper name="platform" label="Платформа">
                                <Select options={platforms} hideClearButton />
                            </FormFieldWrapper>
                        </Layout.Item>
                        <Layout.Item>
                            <FormFieldWrapper name="type" label="Тип">
                                <Select options={types} hideClearButton />
                            </FormFieldWrapper>
                        </Layout.Item>
                        <Layout.Item>
                            <FormField name="shop_name" label="Название магазина" />
                        </Layout.Item>
                        <Layout.Item>
                            <FormField name="shop_url" label="Сайт магазина" />
                        </Layout.Item>
                        <Layout.Item>
                            <FormField name="shop_company" label="Название организации" />
                        </Layout.Item>
                    </Layout>
                </PageTemplate>
            </FormWrapper>
        </PageWrapper>
    );
}
