/* eslint-disable @next/next/no-img-element */
import { FormFieldWrapper, Popup, PopupFooter, PopupHeader, Textarea } from '@ensi-platform/core-components';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useMemo, useRef, useState } from 'react';

import { useDeleteReview, usePatchReview, useReview } from '@api/catalog/reviews';

import { useError, useSuccess } from '@context/modal';

import Legend from '@controls/Legend';
import LoadWrapper from '@controls/LoadWrapper';
import Rating from '@controls/Rating';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { ModalMessages } from '@scripts/constants';
import { RedirectMethods } from '@scripts/enums';
import { ReviewStatus } from '@scripts/enums/reviews';
import { Button, Layout, scale } from '@scripts/gds';
import { isPageNotFound } from '@scripts/helpers';
import { useLinkCSS } from '@scripts/hooks';

import { useReviewsAccess } from '../useReviewsAccess';
import { Aside } from './Aside';

const LISTING_URL = '/reviews/list';

export default function Review() {
    const { access, isIDForbidden } = useReviewsAccess();

    const { query, push } = useRouter();
    const entityId = (query && query.entity_id && +query.entity_id) || 0;

    const [isDeleting, setDeleting] = useState(false);

    const { data: apiReview, isFetching: isLoading, error } = useReview(entityId, access.ID.view);
    useError(error);

    const review = apiReview?.data;

    const deleteReview = useDeleteReview();
    useError(deleteReview.error);

    const onDelete = useCallback(async () => {
        await deleteReview.mutateAsync({
            id: entityId,
        });

        push({
            pathname: LISTING_URL,
        });
    }, [deleteReview, entityId, push]);

    const title = `Отзыв #${entityId}`;
    const redirectAfterSave = useRef(false);

    const initialValues = useMemo(
        () => ({
            customer_id: review?.customer?.id || '',
            product_id: review?.product?.id || '',
            grade: review?.grade || 0,
            comment: review?.comment || '',
        }),
        [review]
    );

    const patchReview = usePatchReview();

    useError(patchReview.error);
    useSuccess(patchReview.isSuccess && ModalMessages.SUCCESS_UPDATE);

    const onChangeStatus = useCallback(
        async (status: ReviewStatus) => {
            await patchReview.mutateAsync({
                id: entityId,
                status_id: status,
            });
        },
        [entityId, patchReview]
    );

    const linkCSS = useLinkCSS('blue');

    const canEdit = access.ID.edit;

    const isNotFound = isPageNotFound({ id: entityId, error });

    return (
        <PageWrapper
            title={title}
            isLoading={isLoading || patchReview.isPending}
            isForbidden={isIDForbidden}
            isNotFound={isNotFound}
        >
            <FormWrapper
                initialValues={initialValues}
                enableReinitialize
                disabled={!canEdit}
                onSubmit={async (vals, { reset }) => {
                    await patchReview.mutateAsync({
                        id: entityId,
                        comment: vals.comment,
                        grade: +vals.grade,
                    });

                    if (redirectAfterSave.current) {
                        reset(vals);
                        return {
                            method: RedirectMethods.push,
                            redirectPath: LISTING_URL,
                        };
                    }

                    return {
                        method: RedirectMethods.replace,
                        redirectPath: `${LISTING_URL}/${entityId}`,
                    };
                }}
            >
                <PageTemplate
                    backlink={{
                        text: 'Назад',
                        href: LISTING_URL,
                    }}
                    aside={<Aside review={review} />}
                    controls={
                        <PageControls access={access} gap={scale(1)}>
                            <PageControls.Delete onClick={() => setDeleting(true)} />
                            <PageControls.Apply
                                onClick={() => {
                                    redirectAfterSave.current = false;
                                }}
                            />
                            <PageControls.Save
                                onClick={() => {
                                    redirectAfterSave.current = true;
                                }}
                            />
                        </PageControls>
                    }
                    h1={title}
                >
                    <Layout cols={2} gap={[scale(1), scale(2)]}>
                        <Layout.Item>
                            <Legend label="Товар" />
                            <Link legacyBehavior passHref href={`/products/catalog/${review?.product?.id}`}>
                                <a css={linkCSS}>{review?.product?.name || '...'}</a>
                            </Link>
                        </Layout.Item>
                        <Layout.Item>
                            <Legend label="Клиент" />
                            <Link legacyBehavior passHref href={`/customers/${review?.customer?.id}`}>
                                <a
                                    css={{
                                        display: 'flex',
                                        alignItems: 'center',
                                        gap: scale(1),
                                        ':hover': {
                                            opacity: 0.7,
                                        },
                                    }}
                                >
                                    <img
                                        src={review?.customer?.avatar || '/noimage.png'}
                                        alt="Фото клиента"
                                        width={scale(8)}
                                        height={scale(8)}
                                    />
                                    <span css={linkCSS}>
                                        {[
                                            review?.customer?.last_name,
                                            review?.customer?.first_name,
                                            review?.customer?.middle_name,
                                        ].join(' ')}
                                    </span>
                                </a>
                            </Link>
                        </Layout.Item>
                        <Layout.Item>
                            <Legend label="Оценка" />
                            <Rating readOnly value={initialValues.grade} showReadOnlyEmptyStar />
                        </Layout.Item>
                        <Layout.Item col={2}>
                            <FormFieldWrapper name="comment" label="Комментарий">
                                <Textarea minRows={5} />
                            </FormFieldWrapper>
                        </Layout.Item>
                        {review?.status_id === ReviewStatus.NEW && (
                            <Layout.Item col={2}>
                                <Layout type="flex" justify="end">
                                    <Layout.Item>
                                        <Button
                                            theme="dangerous"
                                            onClick={() => onChangeStatus(ReviewStatus.DECLINED)}
                                            disabled={!canEdit}
                                        >
                                            Отклонить
                                        </Button>
                                    </Layout.Item>

                                    <Layout.Item>
                                        <Button
                                            onClick={() => onChangeStatus(ReviewStatus.PUBLISHED)}
                                            disabled={!canEdit}
                                        >
                                            Опубликовать
                                        </Button>
                                    </Layout.Item>
                                </Layout>
                            </Layout.Item>
                        )}
                    </Layout>
                </PageTemplate>
                <Popup open={isDeleting} onClose={() => setDeleting(false)}>
                    <PopupHeader title="Вы уверены, что хотите удалить отзыв?" />
                    <LoadWrapper isLoading={deleteReview.isPending}>
                        <PopupFooter>
                            <Button type="button" onClick={() => setDeleting(false)} theme="secondary">
                                Не удалять
                            </Button>
                            <Button type="button" onClick={() => onDelete()} theme="dangerous">
                                Удалить
                            </Button>
                        </PopupFooter>
                    </LoadWrapper>
                </Popup>
            </FormWrapper>
        </PageWrapper>
    );
}
