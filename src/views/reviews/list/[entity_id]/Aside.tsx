import { CopyButton } from '@ensi-platform/core-components';
import { FC, useMemo } from 'react';

import { useReviewsStatuses } from '@api/catalog/reviews';
import type { Review } from '@api/catalog/types/reviews';

import { scale } from '@scripts/gds';
import { formatDate } from '@scripts/helpers';

export const Aside: FC<{ review: Review | undefined; className?: string }> = ({ review, className }) => {
    const { data: apiStatuses } = useReviewsStatuses();

    const statusName = useMemo(
        () => apiStatuses?.data.find(e => e.id === review?.status_id)?.name || '',
        [apiStatuses?.data, review?.status_id]
    );

    return (
        <div className={className}>
            <ul css={{ li: { marginBottom: scale(1) } }}>
                <li>
                    <span>Статус: </span>
                    <b>{statusName}</b>
                </li>
                <li>
                    ID: <CopyButton>{`${review?.id || ''}`}</CopyButton>
                </li>
                <li>
                    Создано: {review?.created_at && formatDate(new Date(review?.created_at), 'dd MMMM yyyy, в HH:mm')}
                </li>
                <li>
                    Изменено: {review?.updated_at && formatDate(new Date(review?.updated_at), 'dd MMMM yyyy, в HH:mm')}
                </li>
            </ul>
        </div>
    );
};
