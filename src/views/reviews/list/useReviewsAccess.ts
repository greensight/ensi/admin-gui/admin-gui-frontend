import { defineAccessMatrix, useAccess } from '@scripts/hooks';
import { useIDForbidden } from '@scripts/hooks/useIDForbidden';

const accessMatrix = defineAccessMatrix({
    LIST: {
        // Пользователю доступен табличный список с отзывами с фильтрами и поиском
        view: 1501,
    },
    ID: {
        // Пользователю доступна детальная страница отзывами со всеми данными для просмотра
        view: 1502,
        // Пользователю доступно удаление отзывов
        delete: 1503,
        // Пользователю доступно изменение статуса отзыва
        edit: 1504,
        create: 0,
    },
});

export const useReviewsAccess = () => {
    const access = useAccess(accessMatrix);
    const isIDForbidden = useIDForbidden(access);

    return { access, isIDForbidden };
};
