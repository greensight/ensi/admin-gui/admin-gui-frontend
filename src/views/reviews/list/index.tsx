import { useActionPopup } from '@ensi-platform/core-components';
import { Table } from '@tanstack/react-table';
import { useCallback, useMemo } from 'react';

import { useMassDeleteReviews, useReviews, useReviewsMeta } from '@api/catalog/reviews';
import { Review } from '@api/catalog/types/reviews';

import { useError } from '@context/modal';

import ListBuilder from '@components/ListBuilder';
import { TooltipItem } from '@components/Table';

import { ActionType } from '@scripts/enums';
import { Button, Layout, scale } from '@scripts/gds';
import { declOfNum } from '@scripts/helpers';
import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import { useReviewsAccess } from './useReviewsAccess';

export default function ReviewsList() {
    const { access } = useReviewsAccess();
    const { popupState, popupDispatch, ActionPopup, ActionEnum } = useActionPopup();

    const massDeleteReviews = useMassDeleteReviews();

    useError(massDeleteReviews.error);

    const goToDetailPage = useGoToDetailPage({ extraConditions: access.ID.view });

    const deleteRow = useCallback(
        (ids: number[], method?: () => void) => {
            popupDispatch({
                type: ActionType.Delete,
                payload: {
                    title: `Вы уверены, что хотите удалить ${ids.length} ${declOfNum(ids.length, [
                        'отзыв',
                        'отзыва',
                        'отзывов',
                    ])}`,
                    popupAction: ActionEnum.DELETE,
                    onAction: async () => {
                        try {
                            massDeleteReviews.mutate({ ids });
                            if (method) method();
                        } catch (err) {
                            console.error(err);
                        }
                    },
                },
            });
        },
        [ActionEnum.DELETE, ActionType.Delete, popupDispatch]
    );

    const tooltipContent = useMemo<TooltipItem[]>(
        () => [
            {
                type: 'edit',
                text: 'Редактировать отзыв',
                action: goToDetailPage,
                isDisable: !access.ID.view,
            },
            {
                type: 'delete',
                text: 'Удалить отзыв',
                action: row => {
                    if (row) {
                        deleteRow([row[0].original.id]);
                    }
                },
                isDisable: !access.ID.delete,
            },
        ],
        [access.ID.delete, access.ID.view, goToDetailPage, deleteRow]
    );

    const headerInner = (table: Table<Review>) => (
        <Layout type="flex" gap={scale(2)} css={{ marginRight: scale(2) }}>
            <Layout.Item>
                <Button
                    theme="dangerous"
                    type="button"
                    onClick={() => {
                        deleteRow(
                            table.getSelectedRowModel().flatRows.map(row => row.original.id),
                            () => table.toggleAllRowsSelected(false)
                        );
                    }}
                    disabled={table.getSelectedRowModel().flatRows.length === 0}
                >
                    Удалить выбранные отзывы
                </Button>
            </Layout.Item>
            <Layout.Item>
                <Button
                    type="button"
                    onClick={() => {
                        table.resetRowSelection();
                    }}
                    disabled={table.getSelectedRowModel().flatRows.length === 0}
                >
                    Снять выделение
                </Button>
            </Layout.Item>
        </Layout>
    );

    return (
        <ListBuilder<Review>
            access={access}
            searchHook={useReviews}
            metaHook={useReviewsMeta}
            isLoading={massDeleteReviews.isPending}
            tooltipItems={tooltipContent}
            headerInner={headerInner}
            title="Список отзывов"
        >
            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </ListBuilder>
    );
}
