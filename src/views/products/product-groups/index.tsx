import { createColumnHelper } from '@tanstack/react-table';
import Link from 'next/link';
import { useMemo } from 'react';

import { ProductGroupsData, useProductsGroups, useProductsGroupsMeta } from '@api/catalog';

import ListBuilder from '@components/ListBuilder';
import { TableColumnDefAny, TooltipItem } from '@components/Table';

import { useLinkCSS } from '@scripts/hooks';
import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import { useProductGroupsAccess } from './useProductGroupsAccess';

export default function ProductGroups() {
    const linkStyles = useLinkCSS();
    const { access } = useProductGroupsAccess();

    const goToDetailPage = useGoToDetailPage();

    const tooltipContent: TooltipItem[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Редактировать склейку',
                action: goToDetailPage,
                isDisable: !access.ID.view,
            },
        ],
        [goToDetailPage, access.ID.view]
    );

    const columnHelper = createColumnHelper<ProductGroupsData>();
    const extraColumns: TableColumnDefAny[] = [
        columnHelper.accessor('name', {
            header: 'Название',
            // eslint-disable-next-line react/no-unstable-nested-components
            cell: ({ getValue, row }) =>
                access.ID.view ? (
                    <Link legacyBehavior href={`/products/product-groups/${row.original.id.toString()}`} passHref>
                        <a css={linkStyles}>{getValue()}</a>
                    </Link>
                ) : (
                    getValue()
                ),
            enableHiding: true,
        }),
    ];

    return (
        <ListBuilder<ProductGroupsData>
            access={access}
            searchHook={useProductsGroups}
            extraColumns={extraColumns}
            metaHook={useProductsGroupsMeta}
            tooltipItems={tooltipContent}
            title="Товарные склейки"
        />
    );
}
