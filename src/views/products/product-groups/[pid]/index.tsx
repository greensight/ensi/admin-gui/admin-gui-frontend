import {
    AutocompleteAsync,
    Block,
    DescriptionList,
    DescriptionListItem,
    DescriptionListItemType,
    FormField,
    FormFieldWrapper,
    SelectItem,
    getDefaultDates,
    useActionPopup,
    useFormContext,
} from '@ensi-platform/core-components';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect, useMemo, useRef, useState } from 'react';
import * as Yup from 'yup';

import {
    Product,
    useCategoryEnumOptionsByValuesFn,
    useCategoryEnumValuesSearchFn,
    useProductOptionsByValuesFn,
    useProductsSearch,
} from '@api/catalog';
import {
    useProductsGroupDetail,
    useProductsGroupsCreate,
    useProductsGroupsDelete,
    useProductsGroupsUpdate,
    useProductsGroupsUpdateProducts,
} from '@api/catalog/product-groups';

import { useError, useModalsContext, useSuccess } from '@context/modal';

import Switcher from '@controls/Switcher';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { CREATE_PARAM, ErrorMessages, ModalMessages } from '@scripts/constants';
import { RedirectMethods } from '@scripts/enums';
import { Button, Layout, scale } from '@scripts/gds';
import { isPageNotFound } from '@scripts/helpers';
import { useLinkCSS } from '@scripts/hooks';

import { useProductGroupsAccess } from '../useProductGroupsAccess';
import AddProductGroupItemsPopup from './AddProductGroupItemsPopup';
import { ProductGroupItems } from './ProductGroupItems';

const SaleProduct = ({ product }: { product: Product }) => {
    const linkStyles = useLinkCSS();
    return (
        <div css={{ display: 'flex', alignItems: 'end', gap: scale(2) }}>
            <div css={{ position: 'relative', width: scale(8), height: scale(8) }}>
                <Image
                    alt="Изображение товара"
                    src={product?.main_image_file || product?.main_image_url || '/noimage.png'}
                    unoptimized
                    layout="fill"
                    objectFit="contain"
                    objectPosition="bottom center"
                />
            </div>

            <Link legacyBehavior href={`/products/catalog/${product?.id}`} passHref>
                <a css={linkStyles} target="_blank">
                    {product?.name}
                </a>
            </Link>
        </div>
    );
};

const FormInner = ({
    isCreation,
    onSpecify,
    errorProducts,
}: {
    errorProducts: number[];
    isCreation: boolean;
    onSpecify: () => void;
}) => {
    const { watch, setValue } = useFormContext();
    const main_product = watch('main_product');

    const { categorySearchFn } = useCategoryEnumValuesSearchFn();
    const { categoryOptionsByValuesFn } = useCategoryEnumOptionsByValuesFn();
    const { productsSearchFn } = useProductsSearch();
    const { productOptionsByValuesFn } = useProductOptionsByValuesFn();

    return (
        <Block>
            <Block.Body>
                <Layout cols={2}>
                    <Layout.Item>
                        <FormField name="name" label="Название" />
                    </Layout.Item>
                    <Layout.Item>&nbsp;</Layout.Item>
                    <Layout.Item col={2} css={{ width: `calc(50% - ${scale(3, true)}px)` }}>
                        <FormFieldWrapper name="category_id" label="Категория" disabled>
                            <AutocompleteAsync
                                asyncSearchFn={categorySearchFn}
                                asyncOptionsByValuesFn={categoryOptionsByValuesFn}
                                hideClearButton
                            />
                        </FormFieldWrapper>
                    </Layout.Item>
                    {isCreation && (
                        <>
                            <Layout.Item
                                css={{
                                    display: 'flex',
                                    alignItems: 'end',
                                    gap: scale(2),
                                }}
                            >
                                <FormFieldWrapper name="main_product" label="Главный товар">
                                    <AutocompleteAsync
                                        asyncSearchFn={productsSearchFn}
                                        asyncOptionsByValuesFn={productOptionsByValuesFn}
                                        onChange={(payload: {
                                            selected: SelectItem[] | null;
                                            actionItem?: SelectItem | null;
                                            name?: string;
                                        }) => {
                                            // @ts-ignore
                                            setValue('category_id', payload.actionItem?.value.category_id || undefined);
                                        }}
                                    />
                                </FormFieldWrapper>
                                <Button onClick={onSpecify}>Уточнить</Button>
                            </Layout.Item>

                            <Layout.Item
                                css={{
                                    display: 'flex',
                                }}
                                align="end"
                            >
                                {main_product && <SaleProduct product={main_product} />}
                            </Layout.Item>
                        </>
                    )}

                    <Layout.Item col={2}>
                        <ProductGroupItems isCreationPage={isCreation} errorProducts={errorProducts} />
                    </Layout.Item>
                </Layout>
            </Block.Body>
        </Block>
    );
};

export default function ProductGroup() {
    const { query, push, pathname } = useRouter();
    const { access, isIDForbidden } = useProductGroupsAccess();
    const { appendModal } = useModalsContext();
    const groupId: string = query?.pid?.toString() || '';

    const isCreation = groupId === CREATE_PARAM;
    const listLink = pathname.split(`[pid]`)[0];

    const [isSpecifyTableOpen, setIsSpecifyTableOpen] = useState(false);
    const [errorProducts, setErrorProducts] = useState([] as number[]);

    const { popupState, popupDispatch, ActionPopup, ActionEnum, ActionType } = useActionPopup();

    const {
        data: productsGroup,
        isFetching: isDetailLoading,
        error: isDetailError,
    } = useProductsGroupDetail(groupId, access.ID.view);

    useError(isDetailError);

    const productsGroupData = useMemo(() => productsGroup?.data, [productsGroup]);

    const pageTitle = useMemo(
        () => (isCreation ? 'Создание склейки товаров' : `Склейка товаров: ${productsGroupData?.id}`),
        [isCreation, productsGroupData?.id]
    );

    const {
        mutateAsync: productGroupsCreate,
        isPending: isProductGroupsCreationLoading,
        error: isProductGroupsCreationError,
        isSuccess: isProductGroupsCreationSuccess,
    } = useProductsGroupsCreate();

    useSuccess(isProductGroupsCreationSuccess ? ModalMessages.SUCCESS_SAVE : '');
    useError(isProductGroupsCreationError);

    const {
        mutateAsync: updateProductGroup,
        isPending: isProductGroupsUpdateLoading,
        error: isProductGroupsUpdateError,
        isSuccess: isProductGroupsUpdateSuccess,
    } = useProductsGroupsUpdate();

    useSuccess(isProductGroupsUpdateSuccess ? ModalMessages.SUCCESS_UPDATE : '');
    useError(isProductGroupsUpdateError);

    const {
        mutateAsync: updateProductsInGroup,
        isPending: isPGUpdateProductsLoading,
        error: isPGUpdateProductsError,
        isSuccess: isPGUpdateProductsSuccess,
    } = useProductsGroupsUpdateProducts();

    useSuccess(isPGUpdateProductsSuccess ? ModalMessages.SUCCESS_UPDATE : '');
    useError(isPGUpdateProductsError);

    const {
        mutateAsync: deleteProductGroup,
        isPending: isProductGroupsDeleteLoading,
        error: isProductGroupsDeleteError,
        isSuccess: isProductGroupsDeleteSuccess,
    } = useProductsGroupsDelete();

    useSuccess(isProductGroupsDeleteSuccess ? ModalMessages.SUCCESS_DELETE : '');
    useError(isProductGroupsDeleteError);

    const formInitValues = useMemo(
        () => ({
            name: productsGroupData?.name || '',
            main_product: productsGroupData?.main_product || null,
            category_ids: productsGroupData?.main_product?.category_ids || null,
            products: productsGroupData?.products || [],
            is_active: productsGroupData?.is_active || false,
        }),
        [productsGroupData]
    );

    useEffect(() => {
        if (errorProducts.length) {
            appendModal({
                theme: 'error',
                title: `Не удалось добавить товары [${errorProducts.map(
                    id => id
                )}], т.к. у них не заполнен атрибут(ы) склеивания`,
            });
        }
    }, [errorProducts, appendModal]);

    const shouldReturnBack = useRef(false);

    const canEditForm = isCreation ? access.ID.create : access.ID.edit || access.ID.editMainInfo;
    const canEditActivity = isCreation ? access.ID.create : access.ID.edit || access.ID.edit_activity;

    const isNotFound = isPageNotFound({ id: groupId, error: isDetailError, isCreation });

    return (
        <PageWrapper
            title={pageTitle}
            isLoading={
                isDetailLoading ||
                isProductGroupsCreationLoading ||
                isProductGroupsUpdateLoading ||
                isPGUpdateProductsLoading ||
                isProductGroupsDeleteLoading
            }
            isNotFound={isNotFound}
            isForbidden={isIDForbidden}
        >
            <FormWrapper
                disabled={!canEditForm}
                initialValues={formInitValues}
                validationSchema={Yup.object().shape({
                    name: Yup.string().required(ErrorMessages.REQUIRED),
                    main_product: Yup.object().required(ErrorMessages.REQUIRED),
                })}
                enableReinitialize
                onSubmit={async (values, form) => {
                    if (isCreation) {
                        const {
                            data: { id: createdId },
                        } = await productGroupsCreate({
                            category_ids: values.main_product?.category_ids!,
                            main_product_id: values.main_product?.id!,
                            name: values.name,
                            is_active: values.is_active,
                        });

                        if (values.products.length > 0 && createdId) {
                            const products = values.products.map(el => ({
                                id: el.id,
                            }));

                            if (
                                products.filter(item => item.id === values.main_product?.id).length === 0 &&
                                values.main_product?.id
                            ) {
                                products.push({ id: values.main_product.id });
                            }

                            const { data } = await updateProductsInGroup({
                                id: createdId,
                                replace: true,
                                products,
                            });

                            if (data?.product_errors?.length) {
                                setErrorProducts(data.product_errors);
                                shouldReturnBack.current = false;
                                form.setValue('products', []);
                            }
                        }

                        return {
                            redirectPath: shouldReturnBack.current ? listLink : `${listLink}${createdId}`,
                            method: RedirectMethods.push,
                        };
                    }

                    await updateProductGroup({
                        id: Number(groupId),
                        data: {
                            ...(canEditForm && {
                                category_ids: values.main_product?.category_ids!,
                                main_product_id: values.main_product?.id || 0,
                                name: values.name,
                            }),
                            ...(canEditActivity && { is_active: values.is_active }),
                        },
                    });

                    if (values.products.length > 0 && canEditForm) {
                        const { data } = await updateProductsInGroup({
                            id: Number(groupId),
                            replace: isCreation,
                            products: values.products.map(el => ({
                                id: el.id,
                            })),
                        });

                        if (data?.product_errors?.length) {
                            setErrorProducts(data.product_errors);
                            shouldReturnBack.current = false;

                            form.setValue(
                                'products',
                                form.getValues('products').filter(e => !data.product_errors.includes(e.id))
                            );
                        }
                    }

                    return {
                        redirectPath: shouldReturnBack.current ? listLink : `${listLink}${groupId}`,
                        method: RedirectMethods.replace,
                    };
                }}
            >
                {({ reset: resetForm }) => (
                    <PageTemplate
                        h1={pageTitle}
                        backlink={{ text: 'Назад', href: '/products/product-groups' }}
                        controls={
                            <PageControls
                                access={{
                                    ID: {
                                        create: true,
                                        edit: true,
                                        view: true,
                                        delete: access.ID.delete,
                                    },
                                    LIST: {
                                        view: true,
                                    },
                                }}
                            >
                                <PageControls.Delete
                                    onClick={() => {
                                        popupDispatch({
                                            type: ActionType.Delete,
                                            payload: {
                                                title: 'Вы уверены, что хотите удалить товарную склейку?',
                                                popupAction: ActionEnum.DELETE,
                                                onAction: async () => {
                                                    resetForm();
                                                    await deleteProductGroup(Number(groupId));

                                                    push(listLink);
                                                },
                                            },
                                        });
                                    }}
                                />
                                <PageControls.Close onClick={() => push(listLink)} />
                                <PageControls.Apply
                                    onClick={() => {
                                        shouldReturnBack.current = false;
                                    }}
                                />
                                <PageControls.Save
                                    onClick={() => {
                                        shouldReturnBack.current = true;
                                    }}
                                />
                            </PageControls>
                        }
                        aside={
                            <>
                                <FormFieldWrapper
                                    name="is_active"
                                    css={{ marginBottom: scale(3) }}
                                    disabled={!canEditActivity}
                                >
                                    <Switcher>Активность</Switcher>
                                </FormFieldWrapper>
                                <DescriptionList css={{ marginBottom: scale(5) }}>
                                    {(
                                        [
                                            {
                                                name: 'Кол-во характеристик:',
                                                value: productsGroupData?.main_product?.attributes?.length,
                                            },
                                            {
                                                name: 'Кол-во товаров:',
                                                value: productsGroupData?.products?.length,
                                            },
                                            ...getDefaultDates({ ...productsGroupData }),
                                        ] as DescriptionListItemType[]
                                    ).map(item => (
                                        <DescriptionListItem {...item} key={item.name} />
                                    ))}
                                </DescriptionList>
                            </>
                        }
                        customChildren
                    >
                        <FormInner
                            isCreation={isCreation}
                            onSpecify={() => {
                                setIsSpecifyTableOpen(true);
                            }}
                            errorProducts={errorProducts}
                        />
                        <AddProductGroupItemsPopup
                            isOpen={isSpecifyTableOpen}
                            onRequestClose={() => setIsSpecifyTableOpen(false)}
                            maxRowSelect={1}
                            isMainProduct
                        />
                    </PageTemplate>
                )}
            </FormWrapper>

            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </PageWrapper>
    );
}
