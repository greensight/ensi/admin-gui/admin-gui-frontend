/* eslint-disable react/no-unstable-nested-components */
import { Checkbox, useFormContext } from '@ensi-platform/core-components';
import { createColumnHelper } from '@tanstack/react-table';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useEffect, useMemo, useState } from 'react';

import { Product, ProductGroupsData, useProductsGroupsUpdateProducts } from '@api/catalog';

import { useError, useSuccess } from '@context/modal';

import LoadWrapper from '@controls/LoadWrapper';

import Table, { ExtendedRow, TooltipItem, useSorting, useTable } from '@components/Table';
import { getSettingsColumn } from '@components/Table/columns';
import { TableEmpty, TableHeader } from '@components/Table/components';

import { ModalMessages } from '@scripts/constants';
import { Button, scale, typography } from '@scripts/gds';
import { declOfNum } from '@scripts/helpers';
import { useLinkCSS } from '@scripts/hooks';

import PlusIcon from '@icons/plus.svg';

import { useProductGroupsAccess } from '../useProductGroupsAccess';
import AddProductGroupItemsPopup from './AddProductGroupItemsPopup';

export const ProductGroupItems = ({
    isCreationPage,
    errorProducts,
}: {
    isCreationPage: boolean;
    errorProducts: number[];
}) => {
    const { query } = useRouter();

    const { access } = useProductGroupsAccess();
    const canEdit = access.ID.edit || access.ID.editMainInfo;

    const groupId: string = query?.pid?.toString() || '';

    const [isAddingTableOpen, setIsAddingTableOpen] = useState(false);

    const { watch, setValue } = useFormContext<{
        main_product: Product | null;
        products: Product[];
    }>();

    const [main_product, products] = watch(['main_product', 'products']);

    const convertProductGroupData = useMemo(
        () =>
            products
                .filter((p: Product) => !errorProducts.includes(p.id))
                .map((product: Product) => ({
                    id: product.id,
                    name: product.name,
                    main_product: !!(product.id === main_product?.id),
                })),
        [main_product, products, errorProducts]
    );

    const linkStyles = useLinkCSS();

    const pGUpdateProducts = useProductsGroupsUpdateProducts();

    useSuccess(pGUpdateProducts.status === 'success' ? ModalMessages.SUCCESS_SAVE : '');
    useError(pGUpdateProducts.error);

    useEffect(() => {
        if (main_product && isCreationPage) {
            setValue('products', []);
        }
    }, [isCreationPage, main_product, setValue]);

    const renderHeader = useCallback(
        () => (
            <TableHeader css={{ paddingLeft: 0, paddingRight: 0 }}>
                <div css={{ display: 'flex', justifyContent: 'space-between', width: '100%', padding: 0 }}>
                    <p css={{ lineHeight: '32px', ...typography('bodySm') }}>
                        {`${convertProductGroupData?.length} ${declOfNum(convertProductGroupData?.length, [
                            'товар',
                            'товара',
                            'товаров',
                        ])}`}
                    </p>

                    {canEdit && (
                        <Button
                            Icon={PlusIcon}
                            onClick={() => setIsAddingTableOpen(true)}
                            disabled={!main_product || isCreationPage}
                        >
                            Добавить товар
                        </Button>
                    )}
                </div>
            </TableHeader>
        ),
        [main_product, convertProductGroupData?.length, canEdit, isCreationPage]
    );

    const deleteProduct = useCallback(
        async (originalRows: ExtendedRow['original'][] | undefined) => {
            if (originalRows) {
                const id = Number(originalRows[0].original.id);
                const isClient = originalRows[0].original.isClient as boolean;
                const filteredProducts = convertProductGroupData.filter(item => item.id !== id);

                if (isClient) {
                    setValue('products', filteredProducts as Product[]);
                    return;
                }

                await pGUpdateProducts?.mutateAsync({
                    id: Number(groupId),
                    replace: true,
                    products: filteredProducts.map(el => ({
                        id: el.id,
                    })),
                });
            }
        },
        [convertProductGroupData, pGUpdateProducts, groupId, setValue]
    );

    const disableHint = canEdit ? 'Главный товар удалить нельзя' : 'Недостаточно прав для редактирования';

    const tooltipContent: TooltipItem[] = useMemo(
        () => [
            {
                type: 'delete',
                text: 'Удалить из склейки',
                action: deleteProduct,
                isDisable: (rows?: ExtendedRow['original'][] | undefined) => {
                    if (!canEdit) return true;

                    if (rows && rows[0].original.main_product) {
                        return rows[0].original.main_product;
                    }
                },
                disabledHint: disableHint,
            },
        ],
        [deleteProduct, main_product, canEdit]
    );

    const [{ sorting }, sortingPlugin] = useSorting<ProductGroupsData>('product_group', [], undefined, false);

    const columnHelper = createColumnHelper<ProductGroupsData>();
    const columns = useMemo(
        () =>
            isCreationPage
                ? [
                      columnHelper.accessor('name', {
                          header: 'Наименование',
                          cell: ({ row }) => (
                              <p css={{ marginBottom: scale(1) }}>
                                  <Link legacyBehavior passHref href={`/products/catalog/${row?.original.id}`}>
                                      <a css={linkStyles}>{row?.original.name}</a>
                                  </Link>
                              </p>
                          ),
                          enableHiding: true,
                      }),
                      getSettingsColumn({ tooltipContent }),
                  ]
                : [
                      columnHelper.accessor('main_product', {
                          header: 'Главный товар',
                          cell: ({ row, getValue }) => (
                              <Checkbox
                                  id={`${row.original.id}`}
                                  name={`main-product-${row.original.id}`}
                                  checked={Boolean(getValue())}
                                  onClick={() => {
                                      if (!getValue())
                                          setValue(
                                              'main_product',
                                              convertProductGroupData.filter(
                                                  item => item.id === row.original.id
                                              )[0] as Product,
                                              {
                                                  shouldDirty: true,
                                              }
                                          );
                                  }}
                                  disabled={!canEdit}
                              />
                          ),
                          enableHiding: true,
                      }),
                      columnHelper.accessor('name', {
                          header: 'Наименование',
                          cell: ({ row }) => (
                              <p css={{ marginBottom: scale(1) }}>
                                  <Link legacyBehavior passHref href={`/products/catalog/${row?.original.id}`}>
                                      <a css={linkStyles}>{row?.original.name}</a>
                                  </Link>
                              </p>
                          ),
                          enableHiding: true,
                      }),

                      getSettingsColumn({ tooltipContent }),
                  ],
        [convertProductGroupData]
    );

    const table = useTable(
        {
            data: convertProductGroupData,
            columns,
            meta: {
                tableKey: `product_group_detail`,
            },
            state: {
                sorting,
            },
        },
        [sortingPlugin]
    );

    return (
        <LoadWrapper isLoading={false}>
            {renderHeader()}
            <Table instance={table} />
            {convertProductGroupData?.length === 0 ? (
                <TableEmpty
                    filtersActive={false}
                    titleWithFilters="Товары с данными фильтрами не найдены"
                    titleWithoutFilters="Товары отсутствуют"
                />
            ) : null}

            <AddProductGroupItemsPopup isOpen={isAddingTableOpen} onRequestClose={() => setIsAddingTableOpen(false)} />
        </LoadWrapper>
    );
};
