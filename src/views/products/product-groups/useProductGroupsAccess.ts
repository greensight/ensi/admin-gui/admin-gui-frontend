import { useAccess } from '@scripts/hooks';
import { useIDForbidden } from '@scripts/hooks/useIDForbidden';

export const accessMatrix = {
    LIST: {
        view: 1101,
    },
    ID: {
        view: 1102,
        // Пользователю доступно редактирование склейки (кроме изменения активности и удаления)
        editMainInfo: 1103,
        // Пользователю доступно управление чек-боксом "Активность"
        edit_activity: 1104,
        create: 1105,
        delete: 1106,
        edit: 1107,
    },
};

export const useProductGroupsAccess = () => {
    const access = useAccess(accessMatrix);
    const isIDForbidden = useIDForbidden(access);

    return { access, isIDForbidden };
};
