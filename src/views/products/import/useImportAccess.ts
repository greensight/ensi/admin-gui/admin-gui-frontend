import { useAccess } from '@scripts/hooks';

export const useImportAccess = () =>
    useAccess({
        LIST: {
            view: 1801,
            create: 1801,
        },
        ID: {
            create: 1801,
            view: 1801,
            logsView: 1802,
            errorsView: 1803,
        },
    });
