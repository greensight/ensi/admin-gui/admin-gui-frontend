import { Form, FormFieldWrapper, FormReset, useFormContext } from '@ensi-platform/core-components';
import { useState } from 'react';
import * as Yup from 'yup';

import { useCreateProductImport, usePreloadImportFile } from '@api/catalog/imports';
import { CatalogProductImportTypeEnum, PreloadFileResponse } from '@api/catalog/types/imports';

import { useError, useSuccess } from '@context/modal';

import Dropzone from '@controls/Dropzone';

import { CatalogAcceptTypes, ErrorMessages, FileSizes } from '@scripts/constants';
import { Button, Layout, colors, scale } from '@scripts/gds';

import FileIcon from '@icons/small/folders.svg';
import ImportIcon from '@icons/small/import.svg';

import { IMPORT_TEMPLATE_LINK } from '../scripts/constants';

interface FormData {
    file: File[];
    preloadFile?: PreloadFileResponse['data'];
}

const FileComponent = ({ importId, fileId }: { fileId: number; importId?: number }) => (
    <Layout type="flex" align="center" gap={scale(1)}>
        <Layout.Item>
            {importId ? (
                <ImportIcon width={scale(5)} height={scale(5)} />
            ) : (
                <FileIcon width={scale(5)} height={scale(5)} />
            )}
        </Layout.Item>
        <Layout.Item>
            <span
                css={{
                    background: importId ? colors.success : colors.black,
                    color: colors.white,
                    padding: `${scale(1, true)}px ${scale(2, true)}px`,
                    borderRadius: 8,
                }}
            >
                {importId ? `Импорт #${importId} запущен!` : `Файл #${fileId}`}
            </span>
        </Layout.Item>
    </Layout>
);

const ImportFormInner = ({ importId, isLoading }: { importId?: number; isLoading: boolean }) => {
    const { watch } = useFormContext<FormData>();

    const preloadFile = watch('preloadFile');

    if (preloadFile)
        return (
            <Layout cols={3}>
                <Layout.Item col={2}>
                    <FileComponent fileId={preloadFile.preload_file_id} importId={importId} />
                    <Layout type="flex" css={{ marginTop: scale(1) }}>
                        <Layout.Item>
                            <FormReset theme="secondary">{importId ? 'Загрузить еще' : 'Удалить'}</FormReset>
                        </Layout.Item>
                        <Layout.Item>
                            <Button type="submit" disabled={isLoading || !!importId}>
                                Импортировать
                            </Button>
                        </Layout.Item>
                    </Layout>
                </Layout.Item>
            </Layout>
        );

    return (
        <Layout cols={3}>
            <Layout.Item col={2}>
                <FormFieldWrapper name="file" label="Загрузка файла" disabled={isLoading}>
                    <Dropzone accept={CatalogAcceptTypes} maxSize={FileSizes.MB10} maxFileNameLength={32} />
                </FormFieldWrapper>
                <Layout type="flex" gap={scale(1)}>
                    <Layout.Item>
                        <Button as="a" href={IMPORT_TEMPLATE_LINK} download theme="outline">
                            Скачать шаблон
                        </Button>
                    </Layout.Item>
                    <Layout.Item>
                        <Button type="submit" disabled={isLoading}>
                            Загрузить
                        </Button>
                    </Layout.Item>
                </Layout>
            </Layout.Item>
        </Layout>
    );
};

const initialValues: FormData = {
    file: [],
    preloadFile: undefined,
};

const ImportForm = () => {
    const preloadImportFile = usePreloadImportFile();
    useError(preloadImportFile.error);
    useSuccess(preloadImportFile.isSuccess ? 'Файл успешно загружен' : '');

    const createProductImport = useCreateProductImport();
    useError(createProductImport.error);
    useSuccess(createProductImport.isSuccess ? `Запущен импорт #${createProductImport.data.data.id}` : '');

    const [importId, setImportId] = useState<number>();

    return (
        <Form
            mode="onSubmit"
            initialValues={initialValues}
            validationSchema={Yup.object().shape({
                file: Yup.array().of(Yup.mixed()).max(1, ErrorMessages.MAX_FILES(1)).min(1, ErrorMessages.REQUIRED),
            })}
            onReset={() => {
                setImportId(undefined);
            }}
            onSubmit={async (values, helpers) => {
                if (!values.preloadFile) {
                    const formData = new FormData();
                    formData.append('file', values.file[0]);

                    try {
                        const response = await preloadImportFile.mutateAsync({
                            formData,
                        });

                        helpers.setValue('preloadFile', response.data);
                    } catch (err) {
                        console.error(err);
                    }

                    return;
                }

                try {
                    const response = await createProductImport.mutateAsync({
                        type: CatalogProductImportTypeEnum.PRODUCT,
                        preload_file_id: values.preloadFile.preload_file_id,
                    });

                    setImportId(response.data.id);
                } catch (err) {
                    console.error(err);
                }
            }}
        >
            <ImportFormInner isLoading={preloadImportFile.isPending} importId={importId} />
        </Form>
    );
};

export default ImportForm;
