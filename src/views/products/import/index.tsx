import Link from 'next/link';

import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { Button, Layout } from '@scripts/gds';

import ImportForm from './components/ImportForm';
import { useImportAccess } from './useImportAccess';

const ProductsImport = () => {
    const access = useImportAccess();

    const isAnyAccess = access.ID.errorsView || access.ID.logsView;

    return (
        <PageWrapper title="Импорт товаров" h1="" isForbidden={!access.LIST.view}>
            <PageTemplate
                h1="Импорт товаров"
                controls={
                    isAnyAccess && (
                        <Layout type="flex" css={{ marginLeft: 'auto' }}>
                            {access.ID.logsView && (
                                <Layout.Item>
                                    <Link legacyBehavior href="/products/import/logs" passHref>
                                        <Button as="a">Журнал импортов</Button>
                                    </Link>
                                </Layout.Item>
                            )}
                            {access.ID.errorsView && (
                                <Layout.Item>
                                    <Link legacyBehavior href="/products/import/errors" passHref>
                                        <Button as="a" theme="dangerous">
                                            Журнал ошибок
                                        </Button>
                                    </Link>
                                </Layout.Item>
                            )}
                        </Layout>
                    )
                }
            >
                <ImportForm />
            </PageTemplate>
        </PageWrapper>
    );
};

export default ProductsImport;
