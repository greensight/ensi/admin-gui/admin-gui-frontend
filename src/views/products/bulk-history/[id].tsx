import { Block } from '@ensi-platform/core-components';
import { createColumnHelper } from '@tanstack/react-table';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useMemo } from 'react';

import { useCatalogBulkOperations } from '@api/catalog';

import { useError } from '@context/modal';

import PageWrapper from '@components/PageWrapper';
import Table, { useSorting, useTable } from '@components/Table';

import { scale } from '@scripts/gds';
import { formatDate, isPageNotFound } from '@scripts/helpers';
import { useLinkCSS } from '@scripts/hooks';

export enum CatalogBulkOperationStatus {
    NEW = 1,
    PROCESSING = 2,
    FINISHED = 3,
    ERRORED = 4,
}

const statusNames: Record<CatalogBulkOperationStatus, string> = {
    [CatalogBulkOperationStatus.NEW]: 'Новый',
    [CatalogBulkOperationStatus.FINISHED]: 'Завершен',
    [CatalogBulkOperationStatus.PROCESSING]: 'В обработке',
    [CatalogBulkOperationStatus.ERRORED]: 'Ошибка',
};

const columnHelper = createColumnHelper<any>();

const MassEditHistory = () => {
    const { query } = useRouter();
    const id = +`${query.id}`;

    const [{ backendSorting }, sortingPlugin] = useSorting<any>('mass_edit_history', []);

    const {
        data,
        isFetching: isLoading,
        isInitialLoading: isIdle,
        error,
    } = useCatalogBulkOperations({
        filter: {
            id,
        },
        sort: backendSorting,
    });

    const bulkOperation = data?.data?.[0];

    useError(error);

    const linkCSS = useLinkCSS('blue');

    const tableData = useMemo(
        () =>
            bulkOperation?.errors?.map((e, i) => ({
                ...e,
                id: i,
            })) || [],
        [bulkOperation?.errors]
    );

    const columns = useMemo(
        () => [
            columnHelper.accessor('operation_id', {
                header: 'ID',
                cell: props => props.getValue(),
            }),
            columnHelper.accessor('entity_id', {
                header: 'ID сущности',
                cell: props => props.getValue(),
            }),
            columnHelper.accessor('message', {
                header: 'Текст',
                cell: props => props.getValue(),
            }),
        ],
        []
    );

    const table = useTable(
        {
            data: tableData,
            columns,
            meta: {
                tableKey: `mass_edit_history`,
            },
        },
        [sortingPlugin]
    );

    const isNotFound = isPageNotFound({ id, error });

    return (
        <PageWrapper h1={`Массовое изменение #${id}`} isLoading={isLoading || isIdle} isNotFound={isNotFound}>
            <Block>
                <Block.Body>
                    <div
                        css={{
                            display: 'grid',
                            gridTemplateColumns: `auto 1fr`,
                            rowGap: scale(3),
                            columnGap: scale(2),
                        }}
                    >
                        <strong>Статус: </strong>
                        <div>{bulkOperation?.status ? statusNames[bulkOperation.status] : '-'}</div>
                        <strong>Тип действия: </strong>
                        <div>{bulkOperation?.action}</div>
                        <strong>Измененные товары: </strong>
                        <div css={{ display: 'flex', gap: scale(1), flexWrap: 'wrap', maxWidth: '100%' }}>
                            {bulkOperation?.ids.map(e => (
                                <Link legacyBehavior key={e} href={`/products/catalog/${e}`} passHref>
                                    <a
                                        css={{
                                            display: 'flex',
                                            ...linkCSS,
                                        }}
                                    >
                                        {e}
                                    </a>
                                </Link>
                            ))}
                        </div>
                        <strong>Дата запуска: </strong>
                        <div>{bulkOperation?.created_at ? formatDate(new Date(bulkOperation.created_at)) : '-'}</div>
                        <strong>Дата запуска: </strong>
                        <div>{bulkOperation?.created_at ? formatDate(new Date(bulkOperation.created_at)) : '-'}</div>
                    </div>
                    {!!bulkOperation?.errors?.length && (
                        <div>
                            <hr css={{ marginTop: scale(2), marginBottom: scale(2) }} />
                            <strong>Список ошибок:</strong>
                            <Table instance={table} />
                        </div>
                    )}
                </Block.Body>
            </Block>
        </PageWrapper>
    );
};

export default MassEditHistory;
