import { useMemo } from 'react';

import { useBrands, useBrandsMeta } from '@api/catalog';
import { Brand } from '@api/catalog/types';

import ListBuilder from '@components/ListBuilder';
import { TooltipItem } from '@components/Table';

import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import { useBrandsAccess } from './useBrandsAccess';

const Brands = () => {
    const { access } = useBrandsAccess();
    const goToDetailPage = useGoToDetailPage();

    const tooltipContent = useMemo<TooltipItem[]>(
        () => [
            {
                type: 'edit',
                text: 'Редактировать бренд',
                action: goToDetailPage,
                isDisable: !access.ID.view || !access.ID.edit,
            },
        ],
        [goToDetailPage, access]
    );

    return (
        <ListBuilder<Brand>
            access={access}
            searchHook={useBrands}
            metaHook={useBrandsMeta}
            tooltipItems={tooltipContent}
            title="Бренды"
        />
    );
};

export default Brands;
