import { useAccess } from '@scripts/hooks';
import { useIDForbidden } from '@scripts/hooks/useIDForbidden';

export const AccessMatrix = {
    LIST: {
        view: 1001,
    },
    ID: {
        view: 1002,
        // Пользователю доступно редактирование бренда (кроме изменения активности и удаления)
        edit: 1003,
        // Пользователю доступно управление чек-боксом "Активность"
        edit_activity: 1004,
        create: 1005,
        delete: 1006,
    },
};

export const useBrandsAccess = () => {
    const access = useAccess(AccessMatrix);
    const isIDForbidden = useIDForbidden(access);

    return { access, isIDForbidden };
};
