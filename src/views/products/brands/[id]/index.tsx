import {
    CopyButton,
    DescriptionList,
    DescriptionListItem,
    FormField,
    FormFieldWrapper,
    Textarea,
    getDefaultDates,
    useActionPopup,
} from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import * as Yup from 'yup';

import {
    useBrand,
    useBrandPreloadImage,
    useCreateBrand,
    useDeleteBrand,
    useEditBrand,
    useMutateBrandImage,
} from '@api/catalog';

import { useError, useSuccess } from '@context/modal';

import Dropzone from '@controls/Dropzone';
import { downloadFile } from '@controls/Dropzone/utils';
import Switcher from '@controls/Switcher';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { CREATE_PARAM, ErrorMessages, FileSizes, ImageAcceptTypes, ModalMessages } from '@scripts/constants';
import { ButtonNameEnum, RedirectMethods } from '@scripts/enums';
import { Layout, scale } from '@scripts/gds';
import { isPageNotFound } from '@scripts/helpers';

import { useBrandsAccess } from '../useBrandsAccess';

const BrandDetail = () => {
    const { push, replace, pathname, query } = useRouter();
    const id = Array.isArray(query?.id) ? query.id[0] : query.id;
    const isCreation = useMemo(() => id === CREATE_PARAM, [id]);
    const listLink = pathname.split(`[id]`)[0];

    const buttonNameRef = useRef<ButtonNameEnum | null>(null);

    const {
        mutateAsync: onDeleteBrand,
        isPending: isDeleteLoading,
        error: isDeleteError,
        isSuccess: isDeleteSuccess,
    } = useDeleteBrand();

    useError(isDeleteError);
    useSuccess(isDeleteSuccess ? ModalMessages.SUCCESS_DELETE : '');

    const { popupState, popupDispatch, ActionPopup, ActionEnum, ActionType } = useActionPopup();

    const {
        data: brand,
        isFetching: isDetailLoading,
        error: isDetailError,
    } = useBrand(id, !isDeleteSuccess && !isDeleteLoading);

    useError(isDetailError);

    const brandData = useMemo(() => brand?.data, [brand]);

    const pageTitle = useMemo(
        () => (isCreation ? 'Новый бренд' : `Бренд: ${brandData?.name}`),
        [brandData?.name, isCreation]
    );

    const [imagesState, setImagesState] = useState<File[]>([]);

    const initialLogoName = useMemo(() => brandData?.image_url?.split('/')?.slice(-1)[0] || '', [brandData?.image_url]);

    const getLogo = useCallback(async () => {
        if (brandData?.image_url)
            downloadFile(brandData?.image_url, initialLogoName).then(res => {
                if (res) setImagesState([res]);
            });
    }, [brandData, initialLogoName]);

    useEffect(() => {
        getLogo();
    }, [getLogo]);

    const formInitialValues = useMemo(
        () => ({
            name: brandData?.name || '',
            code: brandData?.code || '',
            is_active: brandData ? brandData?.is_active : false,
            image_url: imagesState,
            description: brandData?.description || '',
        }),
        [brandData, imagesState]
    );

    const {
        mutateAsync: onCreateBrand,
        isPending: isCreateLoading,
        error: isCreateError,
        isSuccess: isCreateSuccess,
    } = useCreateBrand();

    useError(isCreateError);
    useSuccess(isCreateSuccess ? ModalMessages.SUCCESS_SAVE : '');

    const {
        mutateAsync: onEditBrand,
        isPending: isEditLoading,
        error: isEditError,
        isSuccess: isEditSuccess,
    } = useEditBrand();

    useError(isEditError);
    useSuccess(isEditSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const {
        mutateAsync: onBrandPreloadImage,
        isPending: isPreloadImageLoading,
        error: isPreloadImageError,
    } = useBrandPreloadImage();

    useError(isPreloadImageError);

    const {
        mutateAsync: onMutateBrandImage,
        isPending: isMutateImageLoading,
        error: isMutateImageError,
    } = useMutateBrandImage();

    useError(isMutateImageError);

    const { access, isIDForbidden } = useBrandsAccess();

    const canEdit = isCreation ? access.ID.create : access.ID.edit;
    const canEditActivity = access.ID.edit || access.ID.edit_activity;
    const isNotFound = isPageNotFound({ id, error: isDetailError, isCreation });

    return (
        <PageWrapper
            isForbidden={isIDForbidden}
            title={pageTitle}
            isLoading={
                isDetailLoading ||
                isDeleteLoading ||
                isCreateLoading ||
                isPreloadImageLoading ||
                isEditLoading ||
                isMutateImageLoading
            }
            isNotFound={isNotFound}
        >
            <FormWrapper
                initialValues={formInitialValues}
                disabled={!canEdit}
                onSubmit={async values => {
                    const { name, is_active, code, description, image_url } = values;
                    const formData = new FormData();
                    formData.append('file', image_url[0]);

                    if (isCreation) {
                        const imageData = await onBrandPreloadImage({ formData });

                        const { data: createdBrandData } = await onCreateBrand({
                            name,
                            is_active,
                            code,
                            description,
                            preload_file_id: imageData?.data?.preload_file_id,
                        });

                        if (buttonNameRef.current === ButtonNameEnum.APPLY) {
                            return {
                                method: RedirectMethods.replace,
                                redirectPath: `${listLink}${createdBrandData.id}`,
                            };
                        }

                        return { method: RedirectMethods.push, redirectPath: listLink };
                    }

                    if (image_url[0]?.name !== initialLogoName) {
                        await onMutateBrandImage({ id: Number(id), file: formData });
                    }

                    await onEditBrand({
                        id: Number(id),
                        ...(formInitialValues.name !== name && { name }),
                        ...(formInitialValues.is_active !== is_active && { is_active }),
                        ...(formInitialValues.code !== code && { code }),
                        ...(formInitialValues.description !== description && { description }),
                    });

                    if (buttonNameRef.current === ButtonNameEnum.APPLY) {
                        return {
                            method: RedirectMethods.replace,
                            redirectPath: `${listLink}${id}`,
                        };
                    }

                    return { method: RedirectMethods.push, redirectPath: listLink };
                }}
                validationSchema={Yup.object().shape({
                    name: Yup.string().required(ErrorMessages.REQUIRED),
                    code: Yup.string().required(ErrorMessages.REQUIRED),
                    is_active: Yup.boolean().required(ErrorMessages.REQUIRED),
                    image_url: Yup.array().min(1, ErrorMessages.MIN_FILES(1)).max(1, ErrorMessages.MAX_FILES(1)),
                })}
                enableReinitialize
            >
                {({ reset: resetForm }) => (
                    <PageTemplate
                        h1={pageTitle}
                        backlink={{ text: 'Назад', href: listLink }}
                        controls={
                            <PageControls access={access}>
                                <PageControls.Delete
                                    onClick={() => {
                                        popupDispatch({
                                            type: ActionType.Delete,
                                            payload: {
                                                title: `Вы уверены, что хотите удалить бренд ${brandData?.name}?`,
                                                popupAction: ActionEnum.DELETE,
                                                onAction: async () => {
                                                    if (!id) return;
                                                    resetForm();

                                                    await onDeleteBrand(Number(id));

                                                    replace({ pathname: listLink });
                                                },
                                            },
                                        });
                                    }}
                                />
                                <PageControls.Close
                                    onClick={() => {
                                        push(listLink);
                                    }}
                                />
                                <PageControls.Apply
                                    onClick={() => {
                                        buttonNameRef.current = ButtonNameEnum.APPLY;
                                    }}
                                />
                                <PageControls.Save
                                    onClick={() => {
                                        buttonNameRef.current = ButtonNameEnum.SAVE;
                                    }}
                                />
                            </PageControls>
                        }
                        aside={
                            <>
                                <FormFieldWrapper name="is_active" css={{ marginBottom: scale(3) }}>
                                    <Switcher disabled={!canEditActivity}>Активность</Switcher>
                                </FormFieldWrapper>
                                <DescriptionList css={{ marginBottom: scale(5) }}>
                                    <DescriptionListItem
                                        name="ID:"
                                        value={!isCreation ? <CopyButton>{`${id}`}</CopyButton> : '-'}
                                    />
                                    {getDefaultDates({ ...brandData }).map(item => (
                                        <DescriptionListItem {...item} key={item.name} />
                                    ))}
                                </DescriptionList>
                            </>
                        }
                    >
                        <Layout cols={2} gap={scale(3)} align="end">
                            <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                <FormField name="name" label="Наименование бренда*" />
                            </Layout.Item>
                            <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                <FormField name="code" label="Код бренда*" />
                            </Layout.Item>
                            <Layout.Item col={2}>
                                <FormFieldWrapper name="description" label="Описание">
                                    <Textarea />
                                </FormFieldWrapper>
                            </Layout.Item>
                            <Layout.Item col={2}>
                                <FormFieldWrapper name="image_url" label="Логотип*">
                                    <Dropzone
                                        accept={ImageAcceptTypes}
                                        maxFiles={1}
                                        maxSize={FileSizes.MB1}
                                        multiple={false}
                                    />
                                </FormFieldWrapper>
                            </Layout.Item>
                        </Layout>
                    </PageTemplate>
                )}
            </FormWrapper>

            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </PageWrapper>
    );
};

export default BrandDetail;
