/* eslint-disable react/no-unstable-nested-components */
import { useRouter } from 'next/router';
import { useMemo } from 'react';

import { Product, ProductGroupsData } from '@api/catalog';

import ProductGroupTable from '@views/products/catalog/[id]/components/ProductGroupTable';

import { TableEmpty } from '@components/Table/components';

import { Layout } from '@scripts/gds';

interface ProductGroupsProps {
    productGroups?: ProductGroupsData[];
}

const ProductGroups = ({ productGroups }: ProductGroupsProps) => {
    const { query } = useRouter();
    const productId = Array.isArray(query.id) ? query.id[0] : query.id || '';

    const productGroupData = useMemo(() => productGroups || [], [productGroups]);

    const convertProductGroupData = productGroupData?.map((group: ProductGroupsData) => ({
        ...group,
        products: group?.products?.map((product: Product) => ({
            ...product,
            main_product: !!(group.main_product_id === product.id),
        })),
    }));

    return (
        <Layout cols={1}>
            {convertProductGroupData?.map((group: ProductGroupsData) => (
                <Layout.Item>
                    <ProductGroupTable group={group} productId={productId} />
                </Layout.Item>
            ))}
            {convertProductGroupData.length === 0 ? (
                <Layout.Item>
                    <TableEmpty
                        filtersActive={false}
                        titleWithFilters="Склейки не найдены"
                        titleWithoutFilters="Склеек нет"
                    />
                </Layout.Item>
            ) : null}
        </Layout>
    );
};

export default ProductGroups;
