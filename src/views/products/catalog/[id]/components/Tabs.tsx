import { Block, Tabs, TabsProps, useFormContext } from '@ensi-platform/core-components';
import { Dispatch, FC, SetStateAction } from 'react';

import { ProductGroupsData, PropertyFields } from '@api/catalog';

import Circle from '@components/Circle';
import LoadingSkeleton from '@components/controls/LoadingSkeleton';

import { colors, scale } from '@scripts/gds';
import { useTabs } from '@scripts/hooks/useTabs';

import { ProductAttributesList } from '../../components/ProductAttributeList';
import { useCatalogAccess } from '../../scripts/hooks';
import { MASS_EDITABLE_PRODUCT_FIELDS, PRODUCT_CONTENT_FIELDS } from '../scripts/settings';
import Content from './Content';
import MainData from './MainData';
import ProductGroups from './ProductGroups';

const hasErrors = (attrErrors: any) => Array.isArray(attrErrors) && attrErrors.some(e => e !== null);

const hasErrorsByKeys = (errors: any, keys: string[]) => keys.some(e => !!errors[e]);

const tabLeftAddon = <Circle css={{ background: colors?.danger, marginRight: scale(1) }} />;

const getTabTitle = (title: string, isLoading: boolean = false) => (isLoading ? '' : title);

interface ITabsContentProps extends Partial<TabsProps> {
    id: number;
    isNewProduct: boolean;
    productGroups?: ProductGroupsData[];
    productProperties?: PropertyFields[];
    removedImages: string[];
    setRemovedImages: Dispatch<SetStateAction<string[]>>;
    isImageLoading: boolean;
    isLoading?: boolean;
}

const LoadingTab: FC<Pick<ITabsContentProps, 'isLoading'>> = ({ isLoading = false }) => {
    if (isLoading) return <LoadingSkeleton height={scale(4)} width={scale(14)} />;
    return null;
};

const TabsContent: FC<ITabsContentProps> = ({
    id,
    isNewProduct: isCreationPage,
    productGroups,
    productProperties,
    removedImages,
    setRemovedImages,
    isImageLoading,
    isLoading,
}) => {
    const {
        formState: { errors },
    } = useFormContext();

    const {
        ID: {
            create: canCreateProduct,
            mainTab: { view: canViewMain },
            contentTab: { view: canViewContent },
            propertiesTab: { view: canViewProperty },
            productGroupsTab: { view: canViewGroups },
        },
    } = useCatalogAccess();

    const canViewMainTab = isCreationPage ? canCreateProduct : canViewMain;
    const canViewContentTab = isCreationPage ? canCreateProduct : canViewContent;
    const canViewPropertyTab = !isCreationPage && canViewProperty;
    const canViewGroupsTab = !isCreationPage && canViewGroups;

    const hasActiveProperties = !!productProperties?.length && productProperties.every(p => p.is_active);

    const { tabIds, getTabsProps } = useTabs({
        tabsVisibility: {
            '0': canViewMainTab,
            '1': canViewContentTab,
            '2': canViewPropertyTab && hasActiveProperties,
            '3': canViewGroupsTab,
        },
    });

    return (
        <Tabs {...getTabsProps()} css={{ width: '100%' }}>
            <Tabs.Tab
                hidden={!canViewMainTab}
                leftAddons={hasErrorsByKeys(errors, MASS_EDITABLE_PRODUCT_FIELDS) ? tabLeftAddon : null}
                title="Основные данные"
                id={tabIds![0]}
            >
                <Block css={{ padding: scale(3), borderTopLeftRadius: 0 }}>
                    <MainData isCreationPage={isCreationPage} />
                </Block>
            </Tabs.Tab>

            <Tabs.Tab
                hidden={!canViewContentTab && !isLoading}
                leftAddons={
                    hasErrorsByKeys(errors, PRODUCT_CONTENT_FIELDS) ? (
                        tabLeftAddon
                    ) : (
                        <LoadingTab isLoading={isLoading} />
                    )
                }
                title={getTabTitle('Контент', isLoading)}
                disabled={isLoading}
                id={tabIds![1]}
            >
                <Block css={{ padding: scale(3), borderTopLeftRadius: 0 }}>
                    <Content
                        isCreationPage={isCreationPage}
                        removedImages={removedImages}
                        setRemovedImages={setRemovedImages}
                        isImageLoading={isImageLoading}
                    />
                </Block>
            </Tabs.Tab>

            <Tabs.Tab
                hidden={!isLoading && !(canViewPropertyTab && hasActiveProperties)}
                leftAddons={hasErrors(errors?.attributes) ? tabLeftAddon : <LoadingTab isLoading={isLoading} />}
                title={getTabTitle('Характеристики', isLoading)}
                disabled={isLoading}
                id={tabIds![2]}
            >
                <Block css={{ padding: scale(3), borderTopLeftRadius: 0 }}>
                    <ProductAttributesList id={id} isCreationPage={isCreationPage} />
                </Block>
            </Tabs.Tab>

            <Tabs.Tab
                hidden={!canViewGroupsTab}
                leftAddons={<LoadingTab isLoading={isLoading} />}
                title={getTabTitle('Товарные склейки', isLoading)}
                disabled={isLoading}
                id={tabIds![3]}
            >
                <Block css={{ padding: scale(3), borderTopLeftRadius: 0 }}>
                    <ProductGroups productGroups={productGroups} />
                </Block>
            </Tabs.Tab>
        </Tabs>
    );
};

export default TabsContent;
