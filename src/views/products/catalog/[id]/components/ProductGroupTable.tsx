/* eslint-disable react/no-unstable-nested-components */
import { Checkbox } from '@ensi-platform/core-components';
import { createColumnHelper } from '@tanstack/react-table';
import Link from 'next/link';
import { useMemo } from 'react';

import { Product, ProductGroupsData } from '@api/catalog';

import Table, { useSorting, useTable } from '@components/Table';
import { TableHeader } from '@components/Table/components';

import { useLinkCSS } from '@scripts/hooks';

const ProductGroupTable = ({ group, productId }: { group: ProductGroupsData; productId: string }) => {
    const linkStyles = useLinkCSS();

    const [{ sorting }, sortingPlugin] = useSorting<Product>(`product-group_${group.id}`, [], undefined, false);

    const columnHelper = createColumnHelper<Product>();
    const columns = useMemo(
        () => [
            columnHelper.accessor('main_product', {
                header: 'Главный товар',
                cell: ({ getValue }) => <Checkbox checked={Boolean(getValue())} readOnly />,
                enableHiding: true,
            }),
            columnHelper.accessor('name', {
                header: 'Названия товаров',
                cell: ({
                    getValue,
                    row: {
                        original: { id },
                    },
                }) =>
                    +productId === +id ? (
                        <p>{getValue()}</p>
                    ) : (
                        <Link legacyBehavior href={`/products/catalog/${id}`} passHref>
                            <a css={{ ...linkStyles }}>{getValue()}</a>
                        </Link>
                    ),
                enableHiding: true,
            }),
        ],
        []
    );

    const table = useTable(
        {
            data: group.products || [],
            columns,
            meta: {
                tableKey: `product-group_${group.id}`,
            },
            state: {
                sorting,
            },
        },
        [sortingPlugin]
    );

    return (
        <>
            <TableHeader>
                <Link legacyBehavior href={`/products/product-groups/${group?.id}`} passHref>
                    <a css={linkStyles}>{group?.name}</a>
                </Link>
            </TableHeader>
            <Table instance={table} />
        </>
    );
};

export default ProductGroupTable;
