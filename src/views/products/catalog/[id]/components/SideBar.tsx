import { CopyButton, FormFieldWrapper } from '@ensi-platform/core-components';
import { format } from 'date-fns';
import { ru } from 'date-fns/locale';
import { ReactNode } from 'react';
import type { FieldValues } from 'react-hook-form';

import Switcher from '@controls/Switcher';

import { Layout, scale, typography } from '@scripts/gds';

import { useCatalogAccess, useProductsAllowedFields } from '../../scripts/hooks';
import { StatusWithChange } from './StatusWithChange';

const SideBarItem = ({ name, children }: { name: string; children?: ReactNode }) => (
    <div css={{ ...typography('bodySm'), ':not(:last-of-type)': { marginBottom: scale(1) } }}>
        <span css={{ marginRight: scale(1, true) }}>{name}:</span>
        {children || '-'}
    </div>
);

const SideBar = ({
    id,
    initialValues,
    isCreation,
    isLoading,
}: {
    id: number;
    initialValues: FieldValues;
    isCreation: boolean;
    isLoading: boolean;
}) => {
    const {
        ID: { create: canCreate },
    } = useCatalogAccess();

    const { canEditField } = useProductsAllowedFields();

    const canEditPublish = isCreation ? canCreate : canEditField('allow_publish');

    return (
        <Layout cols={1} gap={scale(3)}>
            <Layout.Item>
                <FormFieldWrapper name="allow_publish">
                    <Switcher disabled={!canEditPublish}>Активность</Switcher>
                </FormFieldWrapper>
            </Layout.Item>
            <Layout.Item>
                <StatusWithChange isLoading={isLoading} />
            </Layout.Item>
            <Layout.Item>
                <SideBarItem name="ID">{id ? <CopyButton>{String(id)}</CopyButton> : null}</SideBarItem>
                <SideBarItem name="Код">
                    {initialValues.code ? <CopyButton>{initialValues.code}</CopyButton> : null}
                </SideBarItem>
            </Layout.Item>
            <Layout.Item>
                <SideBarItem name="Изменен">
                    {initialValues.updated_at
                        ? format(new Date(initialValues.updated_at), 'dd.MM.yyyy p', {
                              locale: ru,
                          })
                        : null}
                </SideBarItem>
                <SideBarItem name="Создан">
                    {initialValues.created_at
                        ? format(new Date(initialValues.created_at), 'dd.MM.yyyy p', {
                              locale: ru,
                          })
                        : null}
                </SideBarItem>
            </Layout.Item>
        </Layout>
    );
};

export default SideBar;
