import {
    Badge,
    Popup,
    PopupContent,
    PopupFooter,
    PopupHeader,
    SelectItem,
    SimpleSelect,
    useFormContext,
} from '@ensi-platform/core-components';
import { useMemo, useState } from 'react';

import { useNextProductStatuses, useProductStatus } from '@api/catalog/product-statuses';

import Legend from '@controls/Legend';
import LoadingSkeleton from '@controls/LoadingSkeleton';

import { Button, Layout, scale } from '@scripts/gds';

const ChangeStatusPopup = ({
    statusId: initialStatusId,
    isOpen,
    onClose,
    onSubmit,
    isLoading,
}: {
    statusId: number | null | '';
    isOpen: boolean;
    onClose: () => void;
    onSubmit: (status: number) => void;
    isLoading: boolean;
}) => {
    const { data: apiStatuses } = useNextProductStatuses(
        {
            id: initialStatusId || undefined,
        },
        !isLoading
    );

    const options = useMemo<SelectItem[]>(
        () =>
            apiStatuses?.data.map(e => ({
                label: e.name,
                disabled: !e.available,
                value: e.id,
            })) || [],
        [apiStatuses?.data]
    );

    const [status, setStatus] = useState<SelectItem | null>();

    return (
        <Popup open={isOpen} onClose={onClose}>
            <PopupHeader title="Изменение статуса товара" />

            <PopupContent>
                <SimpleSelect
                    zIndexPopover={101}
                    options={options}
                    hideClearButton
                    label="Новый статус"
                    selected={status}
                    onChange={(_, payload) => {
                        setStatus(payload?.actionItem);
                    }}
                />
            </PopupContent>
            <PopupFooter>
                <Button theme="outline" onClick={onClose}>
                    Не сохранять
                </Button>
                <Button type="button" onClick={() => onSubmit(+(status?.value || 0))} disabled={!status}>
                    Сохранить
                </Button>
            </PopupFooter>
        </Popup>
    );
};

export const StatusWithChange = ({
    name = 'status_id',
    className,
    isLoading: isFormLoading,
}: {
    name?: string;
    className?: string;
    isLoading: boolean;
}) => {
    const [isOpen, setOpen] = useState(false);

    const { watch, setValue } = useFormContext();
    const value = watch(name);

    const { data: apiStatus, isLoading } = useProductStatus(
        {
            id: value,
        },
        value !== ''
    );

    const status = apiStatus?.data;

    return (
        <div className={className}>
            <Layout type="flex" justify="space-between" align="center" gap={0}>
                <Layout.Item>
                    <Legend label="Статус" />
                </Layout.Item>
                <Layout.Item>
                    <Button onClick={() => setOpen(true)}>Изменить статус</Button>
                </Layout.Item>
            </Layout>
            {/* eslint-disable-next-line no-nested-ternary */}
            {isFormLoading || isLoading ? (
                <LoadingSkeleton width={scale(18)} height={scale(3)} />
            ) : status ? (
                <Badge bgColor={status?.color} text={status?.name} />
            ) : (
                'Не задан'
            )}
            <ChangeStatusPopup
                isOpen={isOpen}
                onClose={() => setOpen(false)}
                onSubmit={newStatus => {
                    setOpen(false);
                    setValue(name, newStatus, {
                        shouldDirty: true,
                    });
                }}
                statusId={value}
                isLoading={isFormLoading}
            />
        </div>
    );
};
