import { ActionEnum, ActionPopup, ActionPopupContent, ActionState, ThemesEnum } from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { Dispatch, FC } from 'react';

import { ActionType, ProductStatePopup } from '@scripts/enums';
import { typography } from '@scripts/gds';
import { Action } from '@scripts/hooks';

import { POPUP_DATA } from '../scripts/settings';

export interface IProductPopupState extends Partial<ActionState> {
    productAction?: ProductStatePopup;
    description?: string;
    iconTheme?: `${ThemesEnum}`;
}

export interface IProductStatusPopupProps {
    state: IProductPopupState;
    dispatch: Dispatch<Action<IProductPopupState>>;
    onDelete: () => void;
}

const ProductStatusPopup: FC<IProductStatusPopupProps> = ({ state, dispatch, onDelete }) => {
    const { push, pathname } = useRouter();

    const listLink = pathname.split(`[id]`)[0];

    const popupData = state.productAction && POPUP_DATA.find(d => d.action === state.productAction);

    return (
        <ActionPopup
            open={!!state.open}
            action={ActionEnum.DELETE}
            onAction={onDelete}
            onClose={() => {
                dispatch({ type: ActionType.PreClose });
                push(listLink);
            }}
            onUnmount={() => {
                dispatch({ type: ActionType.Close });
            }}
            id="product-state-popup"
            title={popupData?.title}
            leftAddonIconTheme={popupData?.status}
            disableFooter={state.productAction !== ProductStatePopup.DELETE}
        >
            {popupData?.description && (
                <ActionPopupContent>
                    <p css={{ ...typography('bodyMd') }}>{popupData.description}</p>
                </ActionPopupContent>
            )}
        </ActionPopup>
    );
};
export default ProductStatusPopup;
