import { FormFieldWrapper, Textarea } from '@ensi-platform/core-components';
import { useCallback } from 'react';

import Dropzone from '@controls/Dropzone';

import { FileSizes, ImageAcceptTypes } from '@scripts/constants';
import { Layout, scale, typography, useTheme } from '@scripts/gds';

import { useCatalogAccess, useProductsAllowedFields } from '../../scripts/hooks';
import { ProductFormData } from '../../scripts/hooks/types';

interface ContentProps {
    removedImages: string[];
    setRemovedImages: (urls: string[]) => void;
    isCreationPage: boolean;
    isImageLoading: boolean;
}

const Content = ({ isImageLoading, removedImages, isCreationPage, setRemovedImages }: ContentProps) => {
    const { colors } = useTheme();

    const access = useCatalogAccess();
    const { canEditField } = useProductsAllowedFields();

    const isFieldDisabled = useCallback(
        (field: keyof ProductFormData) => {
            if (isCreationPage) return !access.ID.create;

            return !canEditField(field);
        },
        [access.ID.create, canEditField, isCreationPage]
    );

    return (
        <>
            <FormFieldWrapper name="description" label="Описание">
                <Textarea disabled={isFieldDisabled('description')} />
            </FormFieldWrapper>
            <div
                css={{
                    marginTop: scale(2),
                    marginBottom: scale(2),
                    height: '1px',
                    background: colors?.grey300,
                }}
            />
            <Layout cols={12}>
                <Layout.Item col={{ xxxl: 9, xxs: 12 }}>
                    <FormFieldWrapper label="Изображения" name="images" disabled={isFieldDisabled('images')}>
                        <Dropzone
                            accept={ImageAcceptTypes}
                            maxSize={FileSizes.MB10}
                            onFileRemove={(_, file) => {
                                if (removedImages.indexOf(file.name) === -1) {
                                    setRemovedImages([...removedImages, file.name]);
                                }
                            }}
                            isLoading={isImageLoading}
                        />
                    </FormFieldWrapper>
                </Layout.Item>
                <Layout.Item col={{ xxxl: 3, xxs: 12 }} css={{ minWidth: '225px' }}>
                    <h5
                        css={{
                            ...typography('bodySmBold'),
                            color: colors?.grey700,
                            marginTop: scale(2),
                            marginBottom: scale(1),
                        }}
                    >
                        Требования к файлу:
                    </h5>
                    <ul
                        css={{
                            ...typography('bodySm'),
                            color: colors?.grey800,
                        }}
                    >
                        <li>1. Максимальное кол-во &#8212; 20шт;</li>
                        <li>2. Формат &#8212; JPEG или PNG;</li>
                        <li>3. Размер &#8212; не больше 10 МБ;</li>
                    </ul>
                </Layout.Item>
            </Layout>
        </>
    );
};
export default Content;
