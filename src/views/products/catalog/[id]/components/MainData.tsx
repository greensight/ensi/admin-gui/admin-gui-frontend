import { FormFieldWrapper, FormTypedField, Input, useFormContext } from '@ensi-platform/core-components';
import { useCallback, useMemo } from 'react';

import { useProductTariffingVolumes } from '@api/catalog/product-tariffing-volumes';
import { useProductUom } from '@api/catalog/product-uom';

import Legend from '@controls/Legend';
import Select from '@controls/Select';

import { ProductType } from '@scripts/enums';
import { Layout, colors, typography } from '@scripts/gds';

import ProductFields from '../../components/ProductFields';
import { useCatalogAccess, useProductsAllowedFields } from '../../scripts/hooks';
import { ProductFormData } from '../../scripts/hooks/types';

const UomField = ({ disabled }: { disabled: boolean }) => {
    const { data: apiUoms } = useProductUom();

    const options = useMemo(
        () =>
            apiUoms?.data.map(e => ({
                key: e.name,
                value: e.id,
            })) || [],
        [apiUoms?.data]
    );

    return (
        <FormFieldWrapper name="uom" label="Единица измерения" disabled={disabled}>
            <Select options={options} hideClearButton />
        </FormFieldWrapper>
    );
};

const TariffField = () => {
    const { data } = useProductTariffingVolumes();

    const { watch } = useFormContext();
    const val = watch('tariffing_volume');

    const label = useMemo(() => data?.data.find(e => +e.id === +val)?.name, [data?.data, val]);

    return (
        <div>
            <Legend label="Единица тарификации" />
            {label}
        </div>
    );
};

const MainData = ({ isCreationPage }: { isCreationPage: boolean }) => {
    const { watch } = useFormContext<{
        type: ProductType;
    }>();

    const currentType = watch('type');

    const access = useCatalogAccess();
    const { canEditField } = useProductsAllowedFields();

    const isFieldDisabled = useCallback(
        (field: keyof ProductFormData) => {
            if (isCreationPage) return !access.ID.create;

            return !canEditField(field);
        },
        [access.ID.create, canEditField, isCreationPage]
    );

    const commonDisabled = isCreationPage ? !access.ID.create : !access.ID.mainTab.edit;

    return (
        <Layout cols={{ xxxl: 6 }}>
            <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                <FormFieldWrapper disabled={isFieldDisabled('name')} name="name" label="Название товара*">
                    <Input />
                </FormFieldWrapper>
            </Layout.Item>

            <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                <FormFieldWrapper disabled={isFieldDisabled('vendor_code')} name="vendor_code" label="Артикул*">
                    <Input />
                </FormFieldWrapper>
            </Layout.Item>

            <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                <FormTypedField
                    disabled={isFieldDisabled('barcode')}
                    name="barcode"
                    label="Штрихкод*"
                    fieldType="positiveInt"
                    dataType="string"
                />
            </Layout.Item>

            <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                <ProductFields.Type name="type" disabled={isFieldDisabled('type')} />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                <ProductFields.Category name="category_ids" disabled={isFieldDisabled('category_ids')} />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                <ProductFields.Nameplates disabled={commonDisabled} />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                <ProductFields.Brand disabled={isFieldDisabled('brand_id')} />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 3 }}>
                <ProductFields.Adult disabled={isFieldDisabled('is_adult')} />
            </Layout.Item>
            {currentType === ProductType.WEIGHT && (
                <>
                    <Layout.Item col={{ xxxl: 6 }}>
                        <hr />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 6 }}>
                        <h4
                            css={{
                                ...typography('bodyMdBold'),
                                color: colors?.grey700,
                            }}
                        >
                            Настройки весового товара
                        </h4>
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                        <UomField disabled={commonDisabled} />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                        <TariffField />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                        <FormTypedField
                            disabled={isFieldDisabled('order_step')}
                            name="order_step"
                            label="Шаг пикера"
                            fieldType="positiveFloat"
                        />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                        <FormTypedField
                            disabled={isFieldDisabled('order_minvol')}
                            name="order_minvol"
                            label="Минимальное количество для заказа"
                            fieldType="positiveFloat"
                        />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                        <FormTypedField
                            disabled={isFieldDisabled('picking_weight_deviation')}
                            name="picking_weight_deviation"
                            label="Граница отклонения в комплектации, %"
                            fieldType="positiveFloat"
                        />
                    </Layout.Item>
                </>
            )}
            <Layout.Item col={{ xxxl: 6 }}>
                <hr />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 6 }}>
                <h4
                    css={{
                        ...typography('bodyMdBold'),
                        color: colors?.grey700,
                    }}
                >
                    Габариты
                </h4>
            </Layout.Item>
            <Layout.Item col={{ xxxl: 2, xxs: 6 }}>
                <FormTypedField
                    disabled={isFieldDisabled('width')}
                    name="width"
                    label="Ширина, мм"
                    fieldType="positiveFloat"
                />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 2, xxs: 6 }}>
                <FormTypedField
                    disabled={isFieldDisabled('height')}
                    name="height"
                    label="Высота, мм"
                    fieldType="positiveFloat"
                />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 2, xxs: 6 }}>
                <FormTypedField
                    disabled={isFieldDisabled('length')}
                    name="length"
                    label="Длина, мм"
                    fieldType="positiveFloat"
                />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                <FormTypedField
                    disabled={isFieldDisabled('weight_gross')}
                    name="weight_gross"
                    label="Масса, кг"
                    fieldType="positiveFloat"
                />
            </Layout.Item>
            <Layout.Item col={{ xxxl: 3, xxs: 6 }}>
                <FormTypedField
                    disabled={isFieldDisabled('weight')}
                    name="weight"
                    label="Масса нетто, кг"
                    fieldType="positiveFloat"
                />
            </Layout.Item>
        </Layout>
    );
};

export default MainData;
