import { Form, FormField, FormFieldWrapper, FormReset, useFormContext } from '@ensi-platform/core-components';

import Switcher from '@controls/Switcher';

import { Button, scale } from '@scripts/gds';

const FormChildren = () => {
    const {
        formState: { isDirty: dirty },
        watch,
    } = useFormContext<{ globalSettings: boolean }>();

    const globalSettings = watch('globalSettings');

    return (
        <>
            <FormFieldWrapper name="globalSettings" css={{ marginBottom: scale(2) }}>
                <Switcher>Использовать глобальные настройки</Switcher>
            </FormFieldWrapper>
            <FormField
                name="maxPercent"
                label="Максимальный процент от единицы товара, который можно оплатить бонусами"
                type="number"
                disabled={globalSettings}
                css={{ maxWidth: '50%', marginBottom: scale(2) }}
            />
            <div css={{ display: 'flex' }}>
                <FormReset theme="outline" css={{ marginRight: scale(2) }}>
                    Отменить
                </FormReset>
                <Button type="submit" theme="primary" disabled={!dirty}>
                    Сохранить
                </Button>
            </div>
        </>
    );
};

const Marketing = () => (
    <Form initialValues={{ globalSettings: true, maxPercent: '' }} onSubmit={values => console.log(values)}>
        <FormChildren />
    </Form>
);

export default Marketing;
