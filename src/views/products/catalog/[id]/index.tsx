import { isEmpty, isEqual } from 'lodash';
import { useRouter } from 'next/router';
import { useMemo, useState } from 'react';

import { ProductAttributeValue, ProductImage, Property } from '@api/catalog';
import {
    useCreateProduct,
    useDeleteProductAttributes,
    useProductDeleteImage,
    useProductDetail,
    useProductDetailDelete,
    useProductDetailPartUpdate,
    useProductUpdateAllImages,
    useProductUploadImage,
    useUpdateProductAttributes,
} from '@api/catalog/products';
import { useProductAddNameplates, useProductDeleteNameplates } from '@api/content/products';

import { useModalsContext, useSuccess } from '@context/modal';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { DetailedModalMessages, ModalMessages } from '@scripts/constants';
import { ActionType, ProductStatePopup, RedirectMethods } from '@scripts/enums';
import { calcOptionsDifference, isPageNotFound, isSameIndices } from '@scripts/helpers';
import { usePopupState } from '@scripts/hooks';

import { ImgData, useCatalogAccess, useFileUpload, useProductForm } from '../scripts/hooks';
import { useDetailedError } from '../scripts/hooks/useDetailedError';
import ProductStatusPopup, { IProductPopupState } from './components/ProductStatusPopup';
import SideBar from './components/SideBar';
import TabsContent from './components/Tabs';
import { INCLUDE_LIST } from './scripts/settings';

const Product = () => {
    const { query, push, replace, pathname } = useRouter();

    const listLink = pathname.split(`[id]`)[0];

    const [isNeedRedirect, setIsNeedRedirect] = useState(false);
    const [isUpdating, setUpdating] = useState(false);

    const {
        ID: { delete: canDeleteProducts, create: canCreate, edit: canEdit },
        canViewAnyDetail: canViewDetailPage,
        isIDForbidden,
    } = useCatalogAccess();

    const [popupState, popupDispatch] = usePopupState<IProductPopupState>({
        open: false,
    });

    const { id: queryId } = query;

    const id = useMemo(() => Number(queryId), [queryId]);

    const isNewProduct = useMemo(() => queryId === 'create', [queryId]);

    const productUpdate = useProductDetailPartUpdate();
    const productCreate = useCreateProduct();
    const productDelete = useProductDetailDelete();

    const addNameplates = useProductAddNameplates();
    const deleteNameplates = useProductDeleteNameplates();

    const productImagesPut = useProductUpdateAllImages({ timeout: 50000 });
    const productImageDelete = useProductDeleteImage();
    const productImageUpload = useProductUploadImage({ timeout: 50000 });

    const {
        data: product,
        isFetching: isDetailLoading,
        error: isDetailError,
    } = useProductDetail(
        {
            id: String(id),
            include: INCLUDE_LIST,
        },
        canViewDetailPage
    );

    const productData = product?.data;
    const pageTitle = isNewProduct ? 'Новый товар' : `Товар: ${productData?.name}`;

    const {
        filesState,
        imagesState,
        removedImages,
        attributesImagesState,
        setRemovedImages,
        setImagesState,
        uploadImageFiles,
        updateAttributesImages,
        updateAttributesFiles,
        isLoading: isFilesLoading,
    } = useFileUpload(productData?.attributes, productData?.images);

    const { initialValues, validationSchema, getFormValues } = useProductForm({
        productData,
        images: imagesState,
        attributeImages: attributesImagesState,
        attributeFiles: filesState,
    });

    const { appendModal } = useModalsContext();

    useDetailedError(productUpdate?.error, DetailedModalMessages.ERROR_UPDATE('данных товара'));
    useDetailedError(productCreate?.error, DetailedModalMessages.ERROR_CREATE('товара'));
    useDetailedError(productDelete?.error, DetailedModalMessages.ERROR_DELETE('товара'));
    useDetailedError(productImageDelete?.error, DetailedModalMessages.ERROR_DELETE('изображения товара'));
    useDetailedError(productImageUpload?.error, DetailedModalMessages.ERROR_UPLOAD('изображения товара'));
    useDetailedError(addNameplates.error, DetailedModalMessages.ERROR_UPLOAD('тега товара'));
    useDetailedError(deleteNameplates.error, DetailedModalMessages.ERROR_DELETE('тега товара'));

    useSuccess(productCreate.isSuccess ? ModalMessages.SUCCESS_SAVE : '');
    useSuccess(productDelete.isSuccess ? ModalMessages.SUCCESS_DELETE : '');
    useSuccess(productUpdate.isSuccess ? DetailedModalMessages.SUCCESS_UPDATE('товара') : '');

    const addProductImgs = async (values: typeof initialValues, images: File[]) => {
        const imgs = await uploadImageFiles(images, { timeout: 50000 });
        const startSortValue = values.images.length - imgs.length;
        return imgs
            .map((img, index) => (img ? { ...img, sort: startSortValue + index } : undefined))
            .filter(Boolean) as (ImgData & { sort: number })[];
    };

    const resolveImageDeltas = (values: typeof initialValues) => {
        const initialImages = (productData?.images || []).filter(e => e.name) as (ProductImage & { name: string })[];

        const upload = values.images.filter(
            newImageFile =>
                // Not found in previous => upload
                !initialImages.find(e => e.name === newImageFile.name)
        );

        const remove = initialImages.filter(
            initialImage =>
                // Not found in current => remove
                !values.images.find(e => e.name === initialImage.name)
        );

        const removeIds = remove.map(e => +e.id!).filter(Boolean);

        const isSortingSaved = isSameIndices(
            initialImages.map(e => e.name!),
            values.images.map(e => e.name)
        );

        const remainingImages = initialImages.filter(initialImage =>
            // Found in current => keep
            values.images.find(e => e.name === initialImage.name)
        );

        return { upload, remove, removeIds, isSortingSaved, remainingImages };
    };

    const updateProductImgs = async (values: typeof initialValues, productId: number = id) => {
        // Если картинки пустые то обновлять нечего, кроме разве что удаления
        if (!values.images.length) {
            if (initialValues.images.length) {
                await productImagesPut.mutateAsync({ id: productId, imgs: [] });
                setRemovedImages([]);
            }
            return;
        }

        const { isSortingSaved, upload, removeIds, remainingImages } = resolveImageDeltas(values);

        const finalImagesDict = new Map<string, ProductImage>();

        remainingImages.forEach(image => {
            finalImagesDict.set(image.name, image);
        });

        await Promise.all(
            removeIds.map(removedId => productImageDelete.mutateAsync({ id: productId, file_id: removedId }))
        );

        if (upload.length) {
            const uploadedImageFiles = await uploadImageFiles(upload);
            const totalImages = [...uploadedImageFiles, ...remainingImages];

            const parsedImages = values.images.reduce((acc, currentFile) => {
                if (totalImages.length) {
                    const img = totalImages.find(file => file.name === currentFile.name);
                    if (img) {
                        acc.push({
                            id: img.id,
                            preload_file_id: img.preload_file_id,
                            name: img.name,
                            sort: acc.length,
                            url: img.url,
                        });
                    }
                }
                return acc;
            }, [] as ProductImage[]);

            const results = await Promise.all(
                parsedImages.map(async img => {
                    if (!img.id) return productImageUpload.mutateAsync({ id: productId, data: img });
                })
            );

            results.forEach(response => {
                if (!response) return;

                finalImagesDict.set(response.data.name!, response.data);
            });
        }

        if (!isSortingSaved) {
            const initialImagesSorted = values.images.map((file, index) => {
                const data = finalImagesDict.get(file.name)!;

                return {
                    ...data,
                    name: data.name!,
                    sort: index,
                    url: undefined,
                };
            });

            await productImagesPut.mutateAsync({
                id: productId,
                imgs: initialImagesSorted,
            });

            // Сбрасываем список картинок, чтобы была возможность перезаписать картинки с другой сортировкой
            setImagesState([]);
        }

        setRemovedImages([]);
    };

    const createProduct = async (values: typeof initialValues) => {
        const imgs = await addProductImgs(values, values.images);

        const totalValues = {
            ...getFormValues(values),
            images: imgs,
        };

        const createdProduct = await productCreate.mutateAsync(totalValues);

        if (!isEmpty(values?.nameplates)) {
            await addNameplates.mutateAsync({
                productId: createdProduct.data.id,
                ids: values?.nameplates,
            });
        }

        popupDispatch({ type: ActionType.Edit, payload: { open: true, productAction: ProductStatePopup.SAVED } });

        return createdProduct;
    };

    const updateAttributes = useUpdateProductAttributes();
    const deleteAttributes = useDeleteProductAttributes();

    const updateProduct = async (values: typeof initialValues) => {
        const categoriesProperties = productData?.categories?.flatMap(category => category.properties);
        await updateProductImgs(values);

        const newAttrs = Object.keys(values.attributes).reduce(
            (acc, key) => {
                if (!isEqual(values.attributes[key], initialValues.attributes[key])) {
                    acc[key] = values.attributes[key];
                }

                return acc;
            },
            {} as typeof values.attributes
        );

        const [imagesAttributesValues, filesAttributesValues] = await Promise.all([
            updateAttributesImages(newAttrs, categoriesProperties),
            updateAttributesFiles(newAttrs, categoriesProperties),
        ]);

        const imagesPreloadData = imagesAttributesValues.map(({ data, propertyId }) => ({
            property_id: propertyId,
            preload_file_id: data?.preload_file_id,
        }));

        const filesPreloadData = filesAttributesValues.map(({ data, propertyId }) => ({
            property_id: propertyId,
            preload_file_id: data?.preload_file_id,
        }));

        const requestData = getFormValues(values, imagesPreloadData, filesPreloadData);

        const { attributes, ...fields } = requestData as {
            attributes: ProductAttributeValue[];
        } & typeof requestData;

        const emptyAttributeIds: number[] = [];
        const nonEmptyAttributes: ProductAttributeValue[] = [];

        const propertyMap = new Map<number, Property>();

        (categoriesProperties || []).forEach(property => {
            propertyMap.set(+property.property_id, property);
        });

        const isAttributeEmpty = (property: Property, attribute: ProductAttributeValue) => {
            if (property?.is_required) return false;

            if ('preload_file_id' in attribute && attribute?.preload_file_id) return false;

            if (property?.has_directory) {
                return !attribute?.directory_value_id;
            }

            return attribute?.value === null;
        };

        if (attributes) {
            attributes.forEach(attribute => {
                const property = propertyMap.get(+attribute.property_id)!;

                if (isAttributeEmpty(property, attribute)) {
                    emptyAttributeIds.push(+attribute.property_id);
                } else {
                    nonEmptyAttributes.push(attribute);
                }
            });
        }

        const { deleted: deletedNameplates, added: addedNameplates } = calcOptionsDifference(
            initialValues.nameplates,
            values.nameplates
        );

        let isNameplatesNotificationShowed = false;

        const onSuccessNameplatesUpdate = () => {
            if (!isNameplatesNotificationShowed)
                appendModal({
                    message: DetailedModalMessages.SUCCESS_UPDATE('тега'),
                    theme: 'success',
                });
            isNameplatesNotificationShowed = true;
        };

        const tasks = [
            !!Object.keys(fields).length &&
                productUpdate.mutateAsync({
                    id,
                    // @ts-ignore
                    data: fields,
                }),
            !!nonEmptyAttributes.length && updateAttributes.mutateAsync({ id, attributes: nonEmptyAttributes }),
            !!emptyAttributeIds.length && deleteAttributes.mutateAsync({ id, property_ids: emptyAttributeIds }),
            !!deletedNameplates.length &&
                deleteNameplates.mutateAsync(
                    { productId: id, ids: deletedNameplates },
                    { onSuccess: onSuccessNameplatesUpdate }
                ),
            !!addedNameplates.length &&
                addNameplates.mutateAsync(
                    { productId: id, ids: addedNameplates },
                    { onSuccess: onSuccessNameplatesUpdate }
                ),
        ].filter(Boolean);

        await Promise.allSettled(tasks);
    };

    const isImageLoading = productImagesPut.isPending || isFilesLoading || productImageUpload.isPending;
    const canView = isNewProduct ? canViewDetailPage && canCreate : canViewDetailPage;

    const isNotFound = isPageNotFound({ id, error: isDetailError, isCreation: isNewProduct });

    return (
        <PageWrapper
            title={pageTitle}
            isLoading={
                isDetailLoading ||
                productUpdate?.isPending ||
                productCreate?.isPending ||
                productDelete?.isPending ||
                productImagesPut?.isPending ||
                isFilesLoading
            }
            isNotFound={isNotFound}
            isForbidden={isIDForbidden}
        >
            <FormWrapper
                initialValues={initialValues}
                enableReinitialize
                validationSchema={validationSchema}
                onSubmit={async values => {
                    if (isNewProduct) {
                        const newProduct = await createProduct(values);

                        return {
                            method: RedirectMethods.push,
                            redirectPath: isNeedRedirect
                                ? '/products/catalog'
                                : `/products/catalog/${newProduct.data?.id}`,
                        };
                    }

                    setUpdating(true);

                    try {
                        await updateProduct(values);
                    } finally {
                        setUpdating(false);
                    }

                    return {
                        method: RedirectMethods.push,
                        redirectPath: isNeedRedirect ? '/products/catalog' : `/products/catalog/${product?.data?.id}`,
                    };
                }}
            >
                {({ reset }) => (
                    <>
                        <PageTemplate
                            h1={pageTitle}
                            backlink={{ text: 'Назад', href: '/products/catalog' }}
                            controls={
                                <PageControls
                                    access={{
                                        LIST: {
                                            view: true,
                                        },
                                        ID: {
                                            create: canCreate,
                                            delete: canDeleteProducts,
                                            edit: canEdit,
                                            view: canView,
                                        },
                                    }}
                                >
                                    <PageControls.Delete
                                        onClick={() =>
                                            popupDispatch({
                                                type: ActionType.Edit,
                                                payload: {
                                                    open: true,
                                                    productAction: ProductStatePopup.DELETE,
                                                },
                                            })
                                        }
                                    />
                                    <PageControls.Close onClick={() => push(listLink)} />
                                    <PageControls.Apply
                                        onClick={() => setIsNeedRedirect(false)}
                                        disabled={isUpdating}
                                    />
                                    <PageControls.Save onClick={() => setIsNeedRedirect(true)} disabled={isUpdating} />
                                </PageControls>
                            }
                            aside={
                                <SideBar
                                    id={id}
                                    initialValues={initialValues}
                                    isCreation={isNewProduct}
                                    isLoading={isDetailLoading}
                                />
                            }
                            customChildren
                        >
                            <TabsContent
                                id={id}
                                isNewProduct={isNewProduct}
                                productGroups={productData?.product_groups}
                                productProperties={productData?.categories?.flatMap(category => category.properties)}
                                removedImages={removedImages}
                                setRemovedImages={setRemovedImages}
                                isImageLoading={isImageLoading}
                                isLoading={isDetailLoading}
                            />
                        </PageTemplate>

                        <ProductStatusPopup
                            state={popupState}
                            dispatch={popupDispatch}
                            onDelete={async () => {
                                popupDispatch({ type: ActionType.PreClose });
                                await productDelete.mutateAsync(Number(id), {
                                    onSuccess: () => {
                                        popupDispatch({
                                            type: ActionType.Edit,
                                            payload: {
                                                open: true,
                                                productAction: ProductStatePopup.DELETE_SUCCESS,
                                            },
                                        });
                                        reset();
                                        setTimeout(() => {
                                            replace(listLink);
                                        }, 1000);
                                    },
                                });
                            }}
                        />
                    </>
                )}
            </FormWrapper>
        </PageWrapper>
    );
};

export default Product;
