import { ThemesEnum } from '@ensi-platform/core-components';

import { ProductStatePopup } from '@scripts/enums';

export interface PopupDataItem {
    action: ProductStatePopup;
    title: string;
    description?: string;
    status: `${ThemesEnum}`;
}
