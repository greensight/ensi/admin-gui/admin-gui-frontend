import { ProductDetail } from '@api/catalog';

export type ProductFormData = Omit<ProductDetail, 'product_groups' | 'id' | 'attributes'> & {
    attributes: Record<string, any>;
};

export interface IPreloadData {
    property_id?: string;
    preload_file_id: number;
}
