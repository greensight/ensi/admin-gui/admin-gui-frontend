import { useRouter } from 'next/router';

import { CREATE_PARAM } from '@scripts/constants';
import { defineAccessMatrix, useAccess, useAllowedFields } from '@scripts/hooks';
import { useIDForbidden } from '@scripts/hooks/useIDForbidden';

import { ProductFormData } from './types';

export const accessMatrix = defineAccessMatrix({
    LIST: {
        view: 401,
    },
    ID: {
        view: 402,
        create: 403,
        edit: 404,
        delete: 413,
        editActivity: 408,
        // Пользователю доступна функция "Изменить атрибуты" в табличном списке товаров
        editProperties: 415,
        mainTab: {
            view: 405,
            edit: 411,
        },
        contentTab: {
            view: 406,
            edit: 409,
        },
        propertiesTab: {
            view: 407,
            edit: 410,
        },
        productGroupsTab: {
            view: 414,
        },
        productCatalogMigration: 418, // Пользователю доступна миграция для сервиса Catalog Cache
        productFeedsMigration: 2005, // Пользователю доступна миграция для сервиса Feeds
    },
});

const productsAllowedFields: Record<number, (keyof ProductFormData)[]> = {
    [accessMatrix.ID.editActivity]: ['allow_publish'],
    [accessMatrix.ID.contentTab.edit]: ['description', 'images'],
    [accessMatrix.ID.propertiesTab.edit]: ['attributes'],
    [accessMatrix.ID.mainTab.edit]: [
        'name',
        'vendor_code',
        'barcode',

        'type',
        'category_ids',
        'brand_id',
        'is_adult',
        'weight',
        'weight_gross',
        'length',
        'width',
        'height',
        'code',
        'external_id',
        'status_id',

        'uom',
        'order_step',
        'order_minvol',
        'picking_weight_deviation',
        'tariffing_volume',
    ],
};

export const useProductsAllowedFields = () => {
    const { query } = useRouter();

    return useAllowedFields<ProductFormData>(
        productsAllowedFields,
        query.id === CREATE_PARAM ? accessMatrix.ID.create : accessMatrix.ID.edit,
        ['id', 'created_at', 'updated_at']
    );
};

export const useCatalogAccess = () => {
    const access = useAccess(accessMatrix);

    // Если есть право на редактирование, то можно редактировать все разделы
    if (access.ID.edit) {
        access.ID.editActivity = true;
        access.ID.mainTab.edit = true;
        access.ID.contentTab.edit = true;
        access.ID.propertiesTab.edit = true;
    }

    if (access.ID.view) {
        access.ID.mainTab.view = true;
        access.ID.contentTab.view = true;
        access.ID.propertiesTab.view = true;
        access.ID.productGroupsTab.view = true;
    }

    const canViewAnyDetail =
        access.ID.view ||
        access.ID.mainTab.view ||
        access.ID.contentTab.view ||
        access.ID.propertiesTab.view ||
        access.ID.productGroupsTab.view;

    const isIDForbidden = useIDForbidden(access, canViewAnyDetail);

    return {
        ...access,
        isIDForbidden,
        canViewAnyDetail:
            access.ID.view ||
            access.ID.mainTab.view ||
            access.ID.contentTab.view ||
            access.ID.propertiesTab.view ||
            access.ID.productGroupsTab.view,
        canCreateOnly: access.ID.create && !access.ID.edit,
    };
};
