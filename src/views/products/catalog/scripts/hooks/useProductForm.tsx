import { format } from 'date-fns';
import { isEqual } from 'lodash';
import { useMemo } from 'react';
import * as Yup from 'yup';

import { Property } from '@api/catalog';
import { ProductDetail } from '@api/catalog/types/products';
import { useSearchNameplates } from '@api/content/nameplates';

import {
    AttributeTypeEnum,
    getBaseFilesArraySchema,
    mapAttributesToProperties,
    normalizeNulls,
    populateObjectFrom,
} from '@views/products/scripts';

import { ErrorMessages, FileSizes } from '@scripts/constants';
import { ProductType } from '@scripts/enums';
import { formatISOTimeFromDate } from '@scripts/helpers';
import { MutationExtractor, extractMutations } from '@scripts/mutationExtractor';

import { IPreloadData } from './types';
import { useProductsAllowedFields } from './useCatalogAccess';
import { useMainDataValidation } from './useMainDataValidation';

export const useProductForm = ({
    productData,
    images,
    attributeImages,
    attributeFiles,
}: {
    productData?: ProductDetail;
    images?: File[];
    /** "не привязанный" массив изображений, вхождение искать по полю name === value */
    attributeImages?: File[];
    /** "не привязанный" массив файлов, вхождение искать по полю name === value */
    attributeFiles?: File[];
}) => {
    const categoryPropertiesWithProductValues: Property[] = useMemo(
        () =>
            productData?.categories?.flatMap(category =>
                category?.properties?.map(categoryProp => {
                    const multiplyValues = productData?.attributes?.filter(
                        ({ property_id }) => property_id === categoryProp?.property_id
                    );
                    return {
                        ...categoryProp,
                        attributeValue: multiplyValues,
                    };
                })
            ) || [],
        [productData]
    );

    const { data: apiNameplates } = useSearchNameplates(
        {
            filter: {
                product_id: productData?.id,
            },
        },
        !!productData?.id
    );

    const productNameplates = useMemo(() => apiNameplates?.data.map(e => e.id) || [], [apiNameplates?.data]);

    const attributesInitValues = useMemo(
        () =>
            categoryPropertiesWithProductValues.reduce(
                (acc, cur) =>
                    cur
                        ? {
                              ...acc,

                              // пустой простой атрибут
                              ...(!cur.attributeValue?.length &&
                                  !cur.is_multiple &&
                                  !cur.has_directory && {
                                      [cur.property_id]:
                                          // eslint-disable-next-line no-nested-ternary
                                          cur.type === AttributeTypeEnum.COLOR
                                              ? [
                                                    {
                                                        name: '',
                                                        value: '#000000',
                                                    },
                                                ]
                                              : // eslint-disable-next-line no-nested-ternary
                                                cur.type === AttributeTypeEnum.DATETIME
                                                ? [
                                                      {
                                                          date: '',
                                                          time: '',
                                                      },
                                                  ]
                                                : cur.type === AttributeTypeEnum.FILE
                                                  ? []
                                                  : '',
                                  }),

                              // простое значение - ни справочник - ни множественное
                              ...(cur.attributeValue?.length === 1 &&
                                  !cur.is_multiple &&
                                  !cur.has_directory && {
                                      [cur.property_id]: cur.attributeValue[0]?.value,
                                      ...(cur.type === AttributeTypeEnum.IMAGE && {
                                          [cur.property_id]: attributeImages?.length
                                              ? [
                                                    attributeImages?.find(
                                                        file => file?.name === cur.attributeValue[0]?.value
                                                    ),
                                                ]
                                              : [],
                                      }),
                                      ...(cur.type === AttributeTypeEnum.FILE && {
                                          [cur.property_id]: attributeFiles?.length
                                              ? [
                                                    attributeFiles?.find(
                                                        file => file?.name === cur.attributeValue[0]?.value
                                                    ),
                                                ]
                                              : [],
                                      }),
                                      ...(cur.type === AttributeTypeEnum.COLOR && {
                                          [cur.property_id]: [
                                              {
                                                  name: cur.attributeValue[0]?.name,
                                                  value: cur.attributeValue[0]?.value,
                                              },
                                          ],
                                      }),
                                      ...(cur.type === AttributeTypeEnum.DATETIME && {
                                          [cur.property_id]: [
                                              {
                                                  name: cur.attributeValue[0]?.name,
                                                  value: cur.attributeValue[0]?.value,
                                                  date: `${format(new Date(cur.attributeValue[0]?.value), 'dd.MM.yyyy')}`,
                                                  time: formatISOTimeFromDate(cur.attributeValue[0]?.value),
                                              },
                                          ],
                                      }),
                                  }),

                              // пустой справочник НЕмножественный
                              ...(!cur.attributeValue?.length &&
                                  !cur.is_multiple &&
                                  cur.has_directory && { [cur.property_id]: null }),

                              // пустой НЕсправочник множественный
                              ...(!cur.attributeValue?.length &&
                                  cur.is_multiple &&
                                  !cur.has_directory && {
                                      [cur.property_id]: [null],
                                      ...(cur.type === AttributeTypeEnum.COLOR && {
                                          [cur.property_id]: [{ name: '', value: '' }],
                                      }),
                                      ...(cur.type === AttributeTypeEnum.DATETIME && {
                                          [cur.property_id]: [{ date: '', time: '' }],
                                      }),
                                  }),

                              // НЕсправочник множественное
                              ...(cur.attributeValue?.length &&
                                  cur.is_multiple &&
                                  !cur.has_directory && {
                                      [cur.property_id]: cur.attributeValue?.map((item: any) => {
                                          if (cur.type === AttributeTypeEnum.IMAGE)
                                              return attributeImages?.length
                                                  ? [attributeImages?.find(file => file?.name === item?.value)]
                                                  : [];
                                          if (cur.type === AttributeTypeEnum.FILE)
                                              return attributeFiles?.length
                                                  ? [attributeFiles?.find(file => file?.name === item?.value)]
                                                  : [];
                                          if (cur.type === AttributeTypeEnum.COLOR)
                                              return { name: item?.name, value: item?.value };
                                          if (cur.type === AttributeTypeEnum.DATETIME)
                                              return {
                                                  name: item?.name,
                                                  value: item?.value,
                                                  date: new Date(item?.value).getTime(),
                                                  time: formatISOTimeFromDate(item?.value),
                                              };
                                          return item?.value;
                                      }),
                                  }),

                              // НЕмножественный справочник
                              ...(cur.attributeValue?.length &&
                                  !cur.is_multiple &&
                                  cur.has_directory && {
                                      [cur.property_id]: cur.attributeValue[0]?.directory_value_id,
                                  }),

                              // пустой множественный справочник
                              ...(!cur.attributeValue?.length &&
                                  cur.is_multiple &&
                                  cur.has_directory && { [cur.property_id]: [] }),

                              // множественный справочник
                              ...(cur.attributeValue?.length >= 1 &&
                                  cur.is_multiple &&
                                  cur.has_directory &&
                                  cur.attributeValue?.some((item: any) => item?.directory_value_id !== undefined) && {
                                      [cur.property_id]: cur.attributeValue?.map(
                                          (item: any) => item?.directory_value_id
                                      ),
                                  }),

                              ...(cur.type === AttributeTypeEnum.BOOLEAN && {
                                  [cur.property_id]: cur.attributeValue[0]?.value || false,
                              }),
                          }
                        : acc,
                {} as Record<`${number}` | string, any>
            ),
        [attributeFiles, attributeImages, categoryPropertiesWithProductValues]
    );

    const initialValues = useMemo(
        () => ({
            allow_publish: productData?.allow_publish || false,

            attributes: attributesInitValues,
            nameplates: productNameplates,

            barcode: productData?.barcode || '',
            brand_id: productData?.brand_id || '',
            category_ids: productData?.category_ids || [],
            code: productData?.code || '',
            description: productData?.description || '',
            external_id: productData?.external_id || '',
            height: productData?.height || '',
            images: [...(images || [])],
            is_adult: productData?.is_adult || false,
            status_id: productData?.status_id || '',
            length: productData?.length || '',
            name: productData?.name || '',
            type: productData?.type || null,
            vendor_code: productData?.vendor_code || '',
            weight: productData?.weight || '',
            weight_gross: productData?.weight_gross || '',
            width: productData?.width || '',
            created_at: productData?.created_at || '',
            updated_at: productData?.updated_at || '',
            uom: productData?.uom || '',
            tariffing_volume: productData?.tariffing_volume || '',
            order_step: productData?.order_step || '',
            order_minvol: productData?.order_minvol || '',
            picking_weight_deviation: productData?.picking_weight_deviation || '',
        }),
        [productData, attributesInitValues, productNameplates, images]
    );

    const { extractAllowedValues, canEditField } = useProductsAllowedFields();
    const mutationExtractors = useMemo<MutationExtractor<typeof initialValues>[]>(
        () => [
            {
                keys: (Object.keys(initialValues) as (keyof typeof initialValues)[]).filter(
                    e => !['brand_id', 'order_minvol', 'order_step', 'picking_weight_deviation', 'uom'].includes(e)
                ),
                mapper: val => val,
            },
            {
                keys: ['order_minvol', 'order_step', 'picking_weight_deviation', 'uom'],
                mapper: val => val,
            },
            {
                keys: ['brand_id'],
                mapper: val => +val || null,
            },
        ],
        [initialValues]
    );

    const getFormValues = (
        unnormalizedValues: typeof initialValues,
        imagesPreloadDataUnsafe: IPreloadData[] = [],
        filesPreloadData: IPreloadData[] = []
    ) => {
        const { attributes: initialAttributes, ...normalizedInitialValues } = normalizeNulls({
            ...initialValues,
            images: null,
        }) as unknown as typeof initialValues;
        const normalizedResult = normalizeNulls({ ...unnormalizedValues, images: null });
        if (normalizedResult === null) {
            throw new Error('Нормализованные данные равны null.');
        }
        const { attributes, ...values } = normalizedResult;
        // Data consistency
        if (attributes && initialAttributes) {
            populateObjectFrom(initialAttributes, attributes);
            populateObjectFrom(attributes, initialAttributes);
        }

        populateObjectFrom(normalizedInitialValues, values);
        populateObjectFrom(values, normalizedInitialValues);

        const imagesPreloadData = canEditField('images') ? imagesPreloadDataUnsafe : [];

        const normalizeValues = {
            ...values,
            barcode: `${values.barcode || ''}`,
            ...(values.type === ProductType.WEIGHT
                ? {
                      ...(initialValues.uom !== values.uom && { uom: values.uom }),
                      ...(initialValues.order_step !== values.order_step && { order_step: values.order_step }),
                      ...(initialValues.order_minvol !== values.order_minvol && { order_minvol: values.order_minvol }),
                      ...(initialValues.picking_weight_deviation !== values.picking_weight_deviation && {
                          picking_weight_deviation: values.picking_weight_deviation,
                      }),
                  }
                : {
                      uom: null,
                      order_step: null,
                      order_minvol: null,
                      picking_weight_deviation: null,
                  }),
        };

        const { data: finalValues } = extractMutations(
            normalizedInitialValues as never as typeof initialValues,
            normalizeValues as never as typeof initialValues,
            mutationExtractors
        );

        delete finalValues.nameplates;
        const fields = extractAllowedValues(finalValues as Omit<typeof finalValues, 'nameplates'>);

        // при смене категории нужно не учитывать атрибуты, ведь они меняются
        if (!isEqual(values?.category_ids, normalizedInitialValues?.category_ids)) {
            return fields;
        }

        const newAttrs = Object.keys(attributes || {}).filter(
            item => !isEqual(attributes[item], initialAttributes[item])
        );

        const attributeEntries = newAttrs.map(e => {
            const attribute = categoryPropertiesWithProductValues.find(({ property_id }) => property_id === Number(e))!;

            return {
                attribute: {
                    ...attribute,
                    property_id: Number(e),
                    id: Number(e),
                },
                value: attributes[e],
            };
        });

        const preparedAttrs = [
            ...mapAttributesToProperties(attributeEntries),
            ...imagesPreloadData,
            ...filesPreloadData,
        ];

        return {
            ...fields,
            ...(preparedAttrs.length > 0 && {
                attributes: preparedAttrs,
            }),
        };
    };

    const numberValidationWithLength = useMemo(
        () =>
            Yup.mixed()
                .transform(a => (a === '' || a === null ? undefined : Number(a)))
                .test(`number-length`, ErrorMessages.MAX_SYMBOLS(50), strVal => {
                    const val = strVal as never as number;

                    if (val === undefined) return true;
                    if (val && val?.toString()?.length) {
                        return val?.toLocaleString('fullwide', { useGrouping: false })?.length < 50;
                    }
                    return false;
                })
                .nullable(),
        []
    );

    const shapeWithDynamicFields = useMemo(
        () =>
            categoryPropertiesWithProductValues.reduce(
                (acc: { [x: string]: any }, { property_id, is_required, type, has_directory, is_multiple }: any) => {
                    if (!canEditField('attributes')) {
                        acc[property_id] = Yup.mixed().nullable();
                        return acc;
                    }

                    if (has_directory && is_multiple) {
                        acc[property_id] = is_required
                            ? Yup.array().required(ErrorMessages.REQUIRED).min(1, ErrorMessages.REQUIRED)
                            : Yup.array().nullable();
                    }

                    if (has_directory && !is_multiple) {
                        acc[property_id] = is_required
                            ? Yup.number().required(ErrorMessages.REQUIRED)
                            : Yup.number().nullable();
                    }

                    if (type === AttributeTypeEnum.COLOR && !has_directory) {
                        if (is_required && is_multiple) {
                            acc[property_id] = Yup.array()
                                .of(
                                    Yup.object().shape({
                                        name: Yup.string()
                                            .nullable()
                                            // валидируем на обязательность только первый элемент из всего списка
                                            .test('name', ErrorMessages.REQUIRED, (item, context) => {
                                                const currentIndex = Number(context.path.split('[')[1].split(']')[0]);
                                                return !(currentIndex === 0 && !item);
                                            }),

                                        value: Yup.string()
                                            .nullable()
                                            .test('value', ErrorMessages.REQUIRED, (item, context) => {
                                                const currentIndex = Number(context.path.split('[')[1].split(']')[0]);
                                                return !(currentIndex === 0 && !item);
                                            }),
                                    })
                                )
                                .min(1, ErrorMessages.MIN_ITEMS(1))
                                .required(ErrorMessages.REQUIRED);
                        } else if (!is_required && is_multiple) {
                            acc[property_id] = Yup.array()
                                .of(
                                    Yup.object()
                                        .shape({
                                            name: Yup.string().nullable(),
                                            value: Yup.string().nullable(),
                                        })
                                        .nullable()
                                )
                                .nullable();
                        } else if (is_required && !is_multiple) {
                            acc[property_id] = Yup.array()
                                .of(
                                    Yup.object().shape({
                                        name: Yup.string().required(ErrorMessages.REQUIRED),
                                        value: Yup.string().required(ErrorMessages.REQUIRED),
                                    })
                                )
                                .required(ErrorMessages.REQUIRED);
                        } else
                            acc[property_id] = Yup.array()
                                .of(
                                    Yup.object().shape({
                                        name: Yup.string().nullable(),
                                        value: Yup.string().nullable(),
                                    })
                                )
                                .nullable();
                    }

                    if (type === AttributeTypeEnum.DATETIME && !has_directory) {
                        if (is_required && is_multiple) {
                            acc[property_id] = Yup.array()
                                .of(
                                    Yup.object().shape({
                                        date: Yup.string()
                                            .transform(value => {
                                                const [day, month, year] = value.split('.').map(Number);
                                                const dateValue = new Date(`${month}/${day}/${year}`);
                                                return Number.isNaN(dateValue.getTime()) ? null : value;
                                            })
                                            .nullable()
                                            .test('date', ErrorMessages.REQUIRED, (item, context) => {
                                                const currentIndex = Number(context.path.split('[')[1].split(']')[0]);
                                                return !(
                                                    currentIndex === 0 &&
                                                    (!item ||
                                                        (typeof item === 'string' && item === 'NaN') ||
                                                        Number.isNaN(new Date(item).getTime()))
                                                );
                                            }),
                                        time: Yup.string()
                                            .nullable()
                                            .test('time', ErrorMessages.REQUIRED, (item, context) => {
                                                const currentIndex = Number(context.path.split('[')[1].split(']')[0]);
                                                return !(currentIndex === 0 && !item);
                                            }),
                                    })
                                )
                                .min(1, ErrorMessages.MIN_ITEMS(1))
                                .required(ErrorMessages.REQUIRED);
                        } else if (!is_required && is_multiple) {
                            acc[property_id] = Yup.array()
                                .of(
                                    Yup.object()
                                        .shape({
                                            date: Yup.string()
                                                .transform(value => {
                                                    const [day, month, year] = value.split('.').map(Number);
                                                    const dateValue = new Date(`${month}/${day}/${year}`);
                                                    return Number.isNaN(dateValue.getTime()) ? null : value;
                                                })
                                                .nullable(),
                                            time: Yup.string().nullable(),
                                        })
                                        .nullable()
                                )
                                .nullable();
                        } else if (is_required && !is_multiple) {
                            acc[property_id] = Yup.array()
                                .of(
                                    Yup.object().shape({
                                        date: Yup.string()
                                            .transform(value => {
                                                const [day, month, year] = value.split('.').map(Number);
                                                const dateValue = new Date(`${month}/${day}/${year}`);
                                                return Number.isNaN(dateValue.getTime()) ? null : value;
                                            })
                                            .nullable()
                                            .test('date', ErrorMessages.REQUIRED, val => {
                                                if (!val) return false;
                                                const [day, month, year] = val.split('.').map(Number);
                                                const dateValue = new Date(`${month}/${day}/${year}`);

                                                return (
                                                    dateValue.getDate() === day &&
                                                    dateValue.getMonth() + 1 === month &&
                                                    dateValue.getFullYear() === year
                                                );
                                            }),
                                        time: Yup.string().required(ErrorMessages.REQUIRED),
                                    })
                                )
                                .required(ErrorMessages.REQUIRED);
                        } else {
                            acc[property_id] = Yup.array()
                                .of(
                                    Yup.object().shape({
                                        date: Yup.string()
                                            .transform(value => {
                                                const [day, month, year] = value.split('.').map(Number);
                                                const dateValue = new Date(`${month}/${day}/${year}`);
                                                return Number.isNaN(dateValue.getTime()) ? null : value;
                                            })
                                            .nullable(),
                                        time: Yup.string().nullable(),
                                    })
                                )
                                .nullable();
                        }
                    }

                    if ((type === AttributeTypeEnum.INTEGER || type === AttributeTypeEnum.DOUBLE) && !has_directory) {
                        if (is_required && is_multiple) {
                            acc[property_id] = Yup.array()
                                .of(
                                    numberValidationWithLength.test(
                                        'number-required',
                                        ErrorMessages.REQUIRED,
                                        (item, context) => {
                                            const currentIndex = Number(context.path.split('[')[1].split(']')[0]);
                                            return !(currentIndex === 0 && !item);
                                        }
                                    )
                                )
                                .min(1, ErrorMessages.MIN_ITEMS(1))
                                .required(ErrorMessages.REQUIRED);
                        } else if (!is_required && is_multiple) {
                            acc[property_id] = Yup.array().of(numberValidationWithLength).nullable();
                        } else if (is_required && !is_multiple) {
                            acc[property_id] = numberValidationWithLength.required(ErrorMessages.REQUIRED);
                        } else acc[property_id] = numberValidationWithLength;
                    }

                    if ((type === AttributeTypeEnum.TEXT || type === AttributeTypeEnum.STRING) && !has_directory) {
                        if (is_required && is_multiple) {
                            acc[property_id] = Yup.array()
                                .of(
                                    Yup.string()
                                        .nullable()
                                        .max(200, ErrorMessages.MAX_SYMBOLS(200))
                                        .test('string-required', ErrorMessages.REQUIRED, (item, context) => {
                                            const currentIndex = Number(context.path.split('[')[1].split(']')[0]);
                                            return !(currentIndex === 0 && !item);
                                        })
                                )
                                .min(1, ErrorMessages.MIN_ITEMS(1))
                                .required(ErrorMessages.REQUIRED);
                        } else if (!is_required && is_multiple) {
                            acc[property_id] = Yup.array()
                                .of(Yup.string().nullable().max(200, ErrorMessages.MAX_SYMBOLS(200)))
                                .nullable();
                        } else if (is_required && !is_multiple) {
                            acc[property_id] = Yup.string()
                                .max(200, ErrorMessages.MAX_SYMBOLS(200))
                                .required(ErrorMessages.REQUIRED);
                        } else {
                            acc[property_id] = Yup.string().nullable().max(200, ErrorMessages.MAX_SYMBOLS(200));
                        }
                    }

                    if ((type === AttributeTypeEnum.IMAGE || type === AttributeTypeEnum.FILE) && !has_directory) {
                        const maxFileSize = type === AttributeTypeEnum.IMAGE ? FileSizes.MB10 : FileSizes.MB1;
                        const maxSizeMessage = ErrorMessages.MAX_FILE_SIZE(AttributeTypeEnum.IMAGE ? 10 : 1);

                        const baseFilesArraySchema = getBaseFilesArraySchema(maxFileSize, maxSizeMessage);

                        if (is_required && is_multiple) {
                            acc[property_id] = Yup.array()
                                .nullable()
                                .of(
                                    baseFilesArraySchema
                                        .min(1, ErrorMessages.MIN_FILES(1))
                                        .required(ErrorMessages.REQUIRED)
                                );
                        } else if (!is_required && is_multiple) {
                            acc[property_id] = Yup.array().nullable().of(baseFilesArraySchema);
                        } else if (is_required && !is_multiple) {
                            acc[property_id] = baseFilesArraySchema
                                .min(1, ErrorMessages.MIN_FILES(1))
                                .required(ErrorMessages.REQUIRED);
                        } else if (!is_required && !is_multiple) {
                            acc[property_id] = baseFilesArraySchema.max(1, ErrorMessages.MAX_FILES(1));
                        }
                    }

                    return acc;
                },
                {}
            ),
        [categoryPropertiesWithProductValues, numberValidationWithLength]
    );

    const mainDataValidation = useMainDataValidation();

    const validationSchema = useMemo(
        () =>
            Yup.object().shape({
                ...mainDataValidation,
                // отменяем валидацию атрибутов при смене категории
                attributes: Yup.object().when('category_ids', ([category_ids], schema) =>
                    isEqual(category_ids, initialValues.category_ids)
                        ? schema.shape(shapeWithDynamicFields).nullable()
                        : schema
                ),
            }),
        [initialValues.category_ids, mainDataValidation, shapeWithDynamicFields]
    );

    return { initialValues, validationSchema, getFormValues, categoryPropertiesWithProductValues };
};
