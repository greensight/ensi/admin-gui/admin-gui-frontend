import { CSSObject } from '@emotion/react';
import { useEffect } from 'react';

import { FetchError } from '@api/index';

import { useModalsContext } from '@context/modal';

import { colors, typography } from '@scripts/gds';

const detailMessageCSS: CSSObject = {
    ...typography('bodySmBold'),
    color: colors.white,
    order: -1,
};

export const useDetailedError = (err?: FetchError | null, detailMessage?: string) => {
    const { appendModal } = useModalsContext();

    useEffect(() => {
        if (err?.message || err?.code)
            appendModal({
                title: err.code,
                message: err.message,
                theme: 'error',
                children: <p css={detailMessageCSS}>{detailMessage}</p>,
            });
    }, [err?.message, appendModal, err?.code, detailMessage]);
};
