import { useMemo } from 'react';
import * as Yup from 'yup';

import { getBaseFilesArraySchema } from '@views/products/scripts';

import { ErrorMessages, FileSizes } from '@scripts/constants';
import { ProductType } from '@scripts/enums';

import { useProductsAllowedFields } from './useCatalogAccess';

const maxValidation = {
    name: Yup.string().required(ErrorMessages.REQUIRED),
    vendor_code: Yup.string().required(ErrorMessages.REQUIRED),
    barcode: Yup.string()
        .required(ErrorMessages.REQUIRED)
        .test('barcode-range', 'Числовое значения должно быть 8 или 13 символов', anyVal => {
            const val = Number(anyVal);
            if (val) {
                const value = val.toString();
                return value.length === 8 || value.length === 13;
            }
            return false;
        }),
    type: Yup.number()
        .transform(value => (Number.isNaN(+value) ? null : value))
        .required(ErrorMessages.REQUIRED),
    category_ids: Yup.array()
        .of(
            Yup.number()
                .nullable()
                .transform(value => (Number.isNaN(+value) ? null : value))
                .required(ErrorMessages.REQUIRED)
        )
        .min(1, ErrorMessages.REQUIRED)
        .required(ErrorMessages.REQUIRED),
    uom: Yup.number().when('type', {
        is: (typeId: number | string) => +typeId === ProductType.WEIGHT,
        then: schema => schema.transform(v => (Number.isNaN(+v) ? undefined : v)).required(ErrorMessages.REQUIRED),
        otherwise: schema => schema.transform(v => (Number.isNaN(+v) ? undefined : v)).optional(),
    }),

    order_step: Yup.number().when('type', {
        is: (typeId: number | string) => +typeId === ProductType.WEIGHT,
        then: schema => schema.transform(v => (Number.isNaN(+v) ? undefined : v)).required(ErrorMessages.REQUIRED),
        otherwise: schema => schema.transform(v => (Number.isNaN(+v) ? undefined : v)).optional(),
    }),
    order_minvol: Yup.number().when('type', {
        is: (typeId: number | string) => +typeId === ProductType.WEIGHT,
        then: schema => schema.transform(v => (Number.isNaN(+v) ? undefined : v)).required(ErrorMessages.REQUIRED),
        otherwise: schema => schema.transform(v => (Number.isNaN(+v) ? undefined : v)).optional(),
    }),
    picking_weight_deviation: Yup.number().when('type', {
        is: (typeId: number | string) => +typeId === ProductType.WEIGHT,
        then: schema =>
            schema
                .transform(v => (Number.isNaN(+v) ? undefined : v))
                .min(0)
                .max(100)
                .optional(),
        otherwise: schema => schema.transform(v => (Number.isNaN(+v) ? undefined : v)),
    }),
    images: getBaseFilesArraySchema(FileSizes.MB10, ErrorMessages.MAX_FILE_SIZE(10)).max(
        20,
        ErrorMessages.MAX_FILES(20)
    ),
};

export const useMainDataValidation = () => {
    const { canEditField } = useProductsAllowedFields();

    return useMemo(
        () =>
            (Object.keys(maxValidation) as (keyof typeof maxValidation)[]).reduce(
                (acc, cur) => {
                    if (!canEditField(cur)) return acc;

                    acc[cur] = maxValidation[cur];
                    return acc;
                },
                {} as Record<string, any>
            ) as typeof maxValidation,
        [canEditField]
    );
};
