import { Block, useActionPopup } from '@ensi-platform/core-components';
import { Row } from '@tanstack/react-table';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useMemo, useReducer, useRef, useState } from 'react';

import {
    Product,
    ProductMassPatchAttribute,
    ProductMassPatchFields,
    useMassPatchProducts,
    useMassPatchProductsByQuery,
    useProblemProducts,
    useProducts,
    useProductsCommonAttributes,
    useProductsMeta,
} from '@api/catalog';
import { useCatalogCacheMigration } from '@api/catalog/catalog-cache';
import { useFeedsMigration } from '@api/catalog/feeds';

import { useError, useSuccess } from '@context/modal';

import { initialSort } from '@views/logistic/pickup-points/[id]/scripts/constants';
import { mapAttributesToProperties } from '@views/products/scripts';

import LoadWrapper from '@controls/LoadWrapper';
import Tooltip, { ContentBtn, hideOnEsc } from '@controls/Tooltip';

import AutoFilters from '@components/AutoFilters';
import PageWrapper from '@components/PageWrapper';
import Table, { TablePlugin, TooltipItem, TrProps, useSorting, useTable } from '@components/Table';
import { getSelectColumn, getSettingsColumn } from '@components/Table/columns';
import { RowTooltipWrapper, TableEmpty, TableFooter, TableHeader } from '@components/Table/components';

import { ITEMS_PER_PRODUCTS_PAGE, ModalMessages } from '@scripts/constants';
import { ActionType } from '@scripts/enums';
import { Button, Layout, scale, typography } from '@scripts/gds';
import { declOfNum, getTotal, getTotalPages } from '@scripts/helpers';
import { useDebounce, useRedirectToNotEmptyPage, useTableList } from '@scripts/hooks';
import { useAutoColumns, useAutoFilters, useAutoTableData } from '@scripts/hooks/autoTable';
import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import PlusIcon from '@icons/plus.svg';
import KebabIcon from '@icons/small/kebab.svg';

import ConfirmPopup from './components/ConfirmPopup';
import MassEditPopup, { MassData, isMassAttribute } from './components/MassEditPopup';
import { WarningMessage } from './components/WarningMessage';
import { useCatalogAccess, useFileUpload } from './scripts/hooks';

const isNullish = (value: any): boolean => {
    if (value === null) return true;
    if (value === undefined) return true;
    if (value === '') return true;
    // datetime case
    if (value?.time === '' && value?.date === null) return true;
    // color case
    if (value?.value === '') {
        return true;
    }

    if (Array.isArray(value)) {
        if (value.length === 0) return true;

        return !value.some(e => !isNullish(e));
    }

    return false;
};

const TableWithPopups = ({
    filters,
    columns,
    products,
    sortingPlugin,
    total,
    tooltipContentRow,
}: {
    filters: Record<string, any>;
    columns: any[];
    products: Product[];
    sortingPlugin: TablePlugin<Product>;
    total: number;
    tooltipContentRow: any;
}) => {
    const { pathname, push } = useRouter();
    const {
        canViewAnyDetail: canViewDetailPage,
        ID: {
            create: canCreateProduct,
            editProperties: canEditProperties,
            propertiesTab: { view: canViewProperties },
            productCatalogMigration: canCatalogMigrate,
            productFeedsMigration: canFeedsMigrate,
        },
    } = useCatalogAccess();

    const catalogCacheMigration = useCatalogCacheMigration();
    const feedsMigration = useFeedsMigration();

    interface MassEditingState extends MassData {
        popupOpen: boolean;
    }

    const popupReducerFunc = (state: MassEditingState, action: Partial<MassEditingState>) => ({ ...state, ...action });

    const [massData, setMassData] = useReducer(popupReducerFunc, {
        popupOpen: false,
        attributeOrField: '',
        value: '',
    } as MassEditingState);

    const [isOpenConfirmPopup, setIsOpenConfirmPopup] = useState(false);

    const { ActionPopup, popupState, popupDispatch, ActionEnum } = useActionPopup();

    const runFeedsMigration = useCallback(
        () =>
            popupDispatch({
                type: ActionType.Edit,
                payload: {
                    title: `Запустить миграцию данных для сервиса Feeds?`,
                    popupAction: ActionEnum.CONFIRM,
                    onAction: async () => {
                        try {
                            await feedsMigration.mutateAsync();
                        } catch (err) {
                            console.error(err);
                        }
                    },
                },
            }),
        [feedsMigration, popupDispatch]
    );

    const runCatalogCacheMigration = useCallback(
        () =>
            popupDispatch({
                type: ActionType.Edit,
                payload: {
                    title: `Запустить миграцию данных для сервиса Catalog Cache?`,
                    popupAction: ActionEnum.CONFIRM,
                    onAction: async () => {
                        try {
                            await catalogCacheMigration.mutateAsync();
                        } catch (err) {
                            console.error(err);
                        }
                    },
                },
            }),
        [catalogCacheMigration, popupDispatch]
    );

    const massPatchFiltered = useMassPatchProductsByQuery();
    useError(massPatchFiltered.error);
    useSuccess(massPatchFiltered.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const massPatchSelected = useMassPatchProducts();
    useError(massPatchSelected.error);
    useSuccess(massPatchSelected.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const productIds = useMemo(() => products.map(e => e.id), [products]);

    const { updateAttributesImages, updateAttributesFiles, isLoading: isFilesLoading } = useFileUpload();

    const table = useTable(
        {
            data: products,
            columns,
            meta: {
                tableKey: pathname,
            },
        },
        [sortingPlugin]
    );

    const tooltipInstance = useRef<any>();

    const selectedRowIndices = useMemo(
        () => table.getSelectedRowModel().flatRows.map(e => e.original.id),
        [table.getSelectedRowModel().flatRows]
    );

    const findCommonIds = (productIds: number[], selectedId: number[]): number[] => {
        const selectedIdSet: { [key: number]: boolean } = {};
        selectedId.forEach(id => {
            selectedIdSet[id] = true;
        });
        return productIds.filter(id => selectedIdSet[id]);
    };

    const ids = useMemo(
        () => (selectedRowIndices ? findCommonIds(productIds, selectedRowIndices) : undefined),
        [productIds, selectedRowIndices]
    );

    const debouncedIds = useDebounce(ids, 300);

    const { data: commonAttributesData, error: commonAttributesError } = useProductsCommonAttributes(
        {
            filter: selectedRowIndices.length
                ? {
                      id: debouncedIds,
                  }
                : filters,
            pagination: {
                limit: -1,
                offset: 0,
                type: 'offset',
            },
        },
        canViewDetailPage || canViewProperties
    );

    const commonAttributes = useMemo(
        () => commonAttributesData?.data.filter(e => e.is_active) || [],
        [commonAttributesData]
    );
    useError(commonAttributesError);

    const submitMassEdit = useCallback(async () => {
        setIsOpenConfirmPopup(false);

        const attrOrField = massData.attributeOrField;
        if (!attrOrField) return;

        let mappedAttributes: ProductMassPatchAttribute[] = [];
        let mappedFields: ProductMassPatchFields = {};

        if (isMassAttribute(attrOrField)) {
            if (isNullish(massData.value)) {
                mappedAttributes = [
                    {
                        property_id: attrOrField.id,
                        mark_to_delete: true,
                    },
                ];
            } else if (Array.isArray(massData.value)) {
                mappedAttributes = mapAttributesToProperties(
                    massData.value.map(e => ({
                        attribute: attrOrField,
                        value: e,
                    }))
                ) as ProductMassPatchAttribute[];
            } else {
                mappedAttributes = mapAttributesToProperties([
                    {
                        attribute: attrOrField,
                        value: massData.value,
                    },
                ]) as ProductMassPatchAttribute[];
            }
        } else {
            // Field editing, not attribute
            mappedFields = {
                [attrOrField]: (() => {
                    if (massData.value === '') return null;
                    return massData.value;
                })(),
            };
        }

        const mediaAttributesMap = isMassAttribute(attrOrField)
            ? {
                  [attrOrField.id]: massData.value,
              }
            : {};

        // предзагрузка изображений справочников
        const imagesAttributesValues = await updateAttributesImages(mediaAttributesMap, commonAttributes);
        const imagesPreloadData = imagesAttributesValues.map<ProductMassPatchAttribute>(({ data, propertyId }) => ({
            property_id: +propertyId,
            preload_file_id: data?.preload_file_id,
        }));

        // предзагрузка файлов справочников

        const filesAttributesValues = await updateAttributesFiles(mediaAttributesMap, commonAttributes);
        const filesPreloadData = filesAttributesValues.map<ProductMassPatchAttribute>(({ data, propertyId }) => ({
            property_id: +propertyId!,
            preload_file_id: data?.preload_file_id,
        }));

        const totalAttributes = [...mappedAttributes, ...imagesPreloadData, ...filesPreloadData];

        if (ids?.length) {
            await massPatchSelected.mutateAsync({
                ids: ids!,
                attributes: totalAttributes,
                fields: mappedFields,
            });
        } else {
            await massPatchFiltered.mutateAsync({
                filter: filters,
                attributes: totalAttributes,
                fields: mappedFields,
            });
        }
    }, [
        commonAttributes,
        filters,
        ids,
        massData,
        massPatchFiltered,
        massPatchSelected,
        updateAttributesFiles,
        updateAttributesImages,
    ]);

    const getTooltipForRow = useCallback(() => tooltipContentRow || [], [tooltipContentRow]);
    const renderRow = useCallback(
        ({ children, ...props }: TrProps<any>) => (
            <RowTooltipWrapper
                {...props}
                getTooltipForRow={getTooltipForRow}
                onDoubleClick={() => {
                    if (canViewDetailPage) push(`${pathname}/${props.row?.original.id}`);
                }}
            >
                {children}
            </RowTooltipWrapper>
        ),
        [getTooltipForRow]
    );

    const renderHeader = useCallback(
        (selectedRows: Row<Product>[]) => {
            const count = selectedRows.length;
            const isSelected = count > 0;

            const tooltipContent: TooltipItem[] = [
                {
                    type: 'edit',
                    text: isSelected ? 'Изменить атрибуты выделенных' : 'Изменить атрибуты всех товаров',
                    action: () => {
                        setMassData({ popupOpen: true });
                        tooltipInstance.current.hide();
                    },
                    isDisable: !canEditProperties,
                },
            ];

            return (
                <TableHeader css={{ paddingLeft: 0, paddingRight: 0 }}>
                    <div css={{ display: 'flex', justifyContent: 'space-between', width: '100%', padding: 0 }}>
                        {isSelected ? (
                            <span css={typography('bodyMdBold')}>Выбрано {count}</span>
                        ) : (
                            <p css={{ lineHeight: '32px', ...typography('bodySm') }}>
                                Найдено {`${total} ${declOfNum(total, ['товар', 'товара', 'товаров'])}`}
                            </p>
                        )}
                        <Layout type="flex">
                            {canCatalogMigrate && (
                                <Layout.Item>
                                    <Button
                                        type="button"
                                        onClick={runCatalogCacheMigration}
                                        disabled={!canCatalogMigrate}
                                    >
                                        Миграция данных для Catalog Cache
                                    </Button>
                                </Layout.Item>
                            )}
                            {canFeedsMigrate && (
                                <Layout.Item>
                                    <Button type="button" onClick={runFeedsMigration} disabled={!canFeedsMigrate}>
                                        Миграция данных для Feeds
                                    </Button>
                                </Layout.Item>
                            )}
                            {canCreateProduct && (
                                <Layout.Item>
                                    <Button Icon={PlusIcon} theme="primary" onClick={() => push(`${pathname}/create`)}>
                                        Создать товар
                                    </Button>
                                </Layout.Item>
                            )}
                            <Layout.Item>
                                <Link legacyBehavior href="/products/bulk-history" passHref>
                                    <Button as="a">История изменений</Button>
                                </Link>
                            </Layout.Item>
                            <Layout.Item>
                                <Tooltip
                                    content={
                                        <>
                                            {tooltipContent.map(t => (
                                                <ContentBtn
                                                    key={t.text}
                                                    type={t.type}
                                                    onClick={e => {
                                                        e.stopPropagation();
                                                        t.action(table.getSelectedRowModel().flatRows);
                                                    }}
                                                    disabled={
                                                        typeof t?.isDisable === 'function'
                                                            ? t.isDisable()
                                                            : t?.isDisable
                                                    }
                                                >
                                                    {t.text}
                                                </ContentBtn>
                                            ))}
                                        </>
                                    }
                                    plugins={[hideOnEsc]}
                                    trigger="click"
                                    arrow
                                    theme="light"
                                    placement="bottom"
                                    minWidth={scale(36)}
                                    disabled={tooltipContent.length === 0}
                                    appendTo={() => document.body}
                                    onMount={instance => {
                                        tooltipInstance.current = instance;
                                    }}
                                >
                                    <Button
                                        theme="outline"
                                        Icon={KebabIcon}
                                        css={{ marginLeft: scale(2), marginRight: scale(2) }}
                                        iconAfter
                                    >
                                        Действия
                                    </Button>
                                </Tooltip>
                            </Layout.Item>
                        </Layout>
                    </div>
                </TableHeader>
            );
        },
        [total, canCreateProduct, canEditProperties, push, pathname]
    );

    return (
        <LoadWrapper isLoading={massPatchSelected.isPending || isFilesLoading}>
            {renderHeader(table.getSelectedRowModel().flatRows)}
            <Table instance={table} Tr={renderRow} />
            <MassEditPopup
                title={
                    selectedRowIndices.length
                        ? `Изменение атрибутов выделенных (${selectedRowIndices.length}) товаров`
                        : 'Изменение атрибутов всех товаров'
                }
                onSubmit={data => {
                    setMassData({ popupOpen: false, ...data });
                    setIsOpenConfirmPopup(true);
                }}
                isOpen={massData.popupOpen}
                close={() => setMassData({ popupOpen: false })}
                attributes={commonAttributes}
            />
            <ConfirmPopup
                isOpen={isOpenConfirmPopup}
                onClose={() => setIsOpenConfirmPopup(false)}
                onSave={submitMassEdit}
                isSelected={selectedRowIndices.length > 0}
            />
            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </LoadWrapper>
    );
};

const Catalog = () => {
    const {
        LIST: { view: canViewListingAndFilters },
        canViewAnyDetail: canViewDetailPage,
    } = useCatalogAccess();

    const { data: metaData, error: metaError } = useProductsMeta(canViewListingAndFilters);
    const meta = metaData?.data;

    const {
        metaField,
        values,
        filtersActive,
        URLHelper,
        searchRequestFilter,
        emptyInitialValues,
        reset,
        codesToSortKeys,
        clearInitialValue,
        searchRequestIncludes,
    } = useAutoFilters(meta);

    const { activePage, itemsPerPageCount, setItemsPerPageCount } = useTableList({
        defaultSort: meta?.default_sort,
        defaultPerPage: ITEMS_PER_PRODUCTS_PAGE,
        codesToSortKeys,
    });
    const [{ backendSorting }, sortingPlugin] = useSorting<Product>('catalog', meta?.fields, initialSort);

    const {
        data,
        isFetching: isLoading,
        error,
    } = useProducts(
        {
            include: searchRequestIncludes,
            sort: backendSorting,
            filter: searchRequestFilter,
            pagination: { type: 'offset', limit: itemsPerPageCount, offset: (activePage - 1) * itemsPerPageCount },
        },
        Boolean(meta) && !!backendSorting && canViewListingAndFilters
    );

    const {
        data: problemProducts,
        isFetching: isLoadingProblemProducts,
        error: problemProductsError,
    } = useProblemProducts(
        {
            sort: meta?.default_sort,
            filter: { has_no_filled_required_attributes: true },
            pagination: { type: 'offset', limit: -1, offset: 0 },
        },
        canViewListingAndFilters
    );

    const products = useAutoTableData<Product>(data?.data, metaField);
    const autoGeneratedColumns = useAutoColumns(meta, canViewDetailPage);

    const goToDetailPage = useGoToDetailPage({ extraConditions: canViewDetailPage });

    const tooltipContentRow: TooltipItem[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Редактировать товар',
                action: goToDetailPage,
                isDisable: !canViewDetailPage,
            },
        ],
        [canViewDetailPage, goToDetailPage]
    );

    const columns = useMemo(
        () => [
            getSelectColumn(),
            ...autoGeneratedColumns,
            getSettingsColumn({
                columnsToDisable: [],
                visibleColumns: meta?.default_list,
                tooltipContent: tooltipContentRow,
            }),
        ],
        [autoGeneratedColumns, meta?.default_list]
    );

    const total = getTotal(data);
    const totalPages = getTotalPages(data, itemsPerPageCount);
    useRedirectToNotEmptyPage({ activePage, itemsPerPageCount, total });

    useError(error);
    useError(metaError);
    useError(problemProductsError);

    return (
        <PageWrapper h1={canViewListingAndFilters ? 'Товары' : ''} isLoading={isLoading || isLoadingProblemProducts}>
            {canViewListingAndFilters ? (
                <>
                    <AutoFilters
                        initialValues={values}
                        emptyInitialValues={emptyInitialValues}
                        onSubmit={URLHelper}
                        filtersActive={filtersActive}
                        css={{ marginBottom: scale(2) }}
                        onResetFilters={reset}
                        meta={meta}
                        clearInitialValue={clearInitialValue}
                    />
                    {problemProducts?.data && !!problemProducts.data.length && (
                        <WarningMessage
                            warningProducts={problemProducts?.data}
                            text="У следующих товаров не заполнены все обязательные атрибуты: "
                        />
                    )}
                    <Block>
                        <Block.Body>
                            <TableWithPopups
                                filters={searchRequestFilter}
                                products={products}
                                columns={columns}
                                total={total}
                                sortingPlugin={sortingPlugin}
                                tooltipContentRow={tooltipContentRow}
                            />
                            {products.length === 0 ? (
                                <TableEmpty
                                    filtersActive={filtersActive}
                                    titleWithFilters="Товары с данными фильтрами не найдены"
                                    titleWithoutFilters="Товары отсутствуют"
                                    onResetFilters={reset}
                                />
                            ) : (
                                <TableFooter
                                    pages={totalPages}
                                    itemsPerPageCount={itemsPerPageCount}
                                    setItemsPerPageCount={setItemsPerPageCount}
                                />
                            )}
                        </Block.Body>
                    </Block>
                </>
            ) : (
                <h1 css={{ ...typography('h1'), textAlign: 'center' }}>
                    У вас недостаточно прав для просмотра витрины
                </h1>
            )}
        </PageWrapper>
    );
};

export default Catalog;
