import { FormField, FormFieldWrapper, Select, SelectItem, SelectWithTags } from '@ensi-platform/core-components';

import { Property } from '@api/catalog';

import { scale } from '@scripts/gds';

import { LegendAndErrorOfBlock } from '../LegendAndErrorOfBlock';
import AttributeFieldArray from './Array';

export interface StringFieldProps {
    fieldName: string;
    property: Property;
    directoryOptions: SelectItem[];
    disabled: boolean;
}

export const StringField = ({
    fieldName,
    property: { property_id, has_directory, is_multiple, is_required, name },
    directoryOptions,
    disabled,
}: StringFieldProps) => (
    <>
        {!has_directory && !is_multiple ? (
            <FormField
                name={fieldName}
                label={`${name} ${is_required ? '* ' : ''}`}
                maxLength={50}
                disabled={disabled}
            />
        ) : null}

        {!has_directory && is_multiple ? (
            <>
                <LegendAndErrorOfBlock
                    name={`${name}`}
                    isRequired={Boolean(is_required)}
                    propertyId={property_id}
                    css={{ marginBottom: scale(1) }}
                />
                <AttributeFieldArray name={fieldName} initialValue="">
                    {({ name: ithName }) => <FormField name={ithName} maxLength={50} disabled={disabled} />}
                </AttributeFieldArray>
            </>
        ) : null}

        {has_directory && !is_multiple ? (
            <FormFieldWrapper name={fieldName} label={`${name} ${is_required ? '* ' : ''}`} disabled={disabled}>
                <Select options={directoryOptions} hideClearButton={is_required} allowUnselect={!is_required} />
            </FormFieldWrapper>
        ) : null}

        {has_directory && is_multiple ? (
            <FormFieldWrapper name={fieldName} label={`${name} ${is_required ? '* ' : ''}`} disabled={disabled}>
                <SelectWithTags options={directoryOptions} />
            </FormFieldWrapper>
        ) : null}
    </>
);
