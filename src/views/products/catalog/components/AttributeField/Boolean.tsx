import { FormCheckbox, FormFieldWrapper } from '@ensi-platform/core-components';

import { Property } from '@api/catalog';

import { directoryItemsType } from './types';

export interface BooleanFieldProps {
    fieldName: string;
    property: Property;
    directoryItems?: directoryItemsType;
    disabled: boolean;
}

export const BooleanField = ({
    fieldName,
    property: { is_required, name },
    disabled,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    directoryItems,
}: BooleanFieldProps) => (
    <FormFieldWrapper name={fieldName} disabled={disabled}>
        <FormCheckbox>{`${name} ${is_required ? '* ' : ''}`}</FormCheckbox>
    </FormFieldWrapper>
);
