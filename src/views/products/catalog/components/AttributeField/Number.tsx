import { FormFieldWrapper, FormTypedField, Select, SelectItem, SelectWithTags } from '@ensi-platform/core-components';

import { Property } from '@api/catalog';

import { AttributeTypeEnum } from '@views/products/scripts';

import { scale } from '@scripts/gds';

import { LegendAndErrorOfBlock } from '../LegendAndErrorOfBlock';
import AttributeFieldArray from './Array';

export interface NumberFieldProps {
    fieldName: string;
    property: Property;
    directoryOptions: SelectItem[];
    disabled: boolean;
}

export const NumberField = ({
    fieldName,
    property: { property_id, has_directory, type, is_multiple, is_required, name },
    directoryOptions,
    disabled,
}: NumberFieldProps) => (
    <>
        {!has_directory && !is_multiple ? (
            <FormTypedField
                name={fieldName}
                label={`${name} ${is_required ? '* ' : ''}`}
                fieldType={type === AttributeTypeEnum.INTEGER ? 'positiveInt' : 'positiveFloat'}
                disabled={disabled}
            />
        ) : null}
        {!has_directory && is_multiple ? (
            <>
                <LegendAndErrorOfBlock
                    name={`${name}`}
                    isRequired={Boolean(is_required)}
                    propertyId={property_id}
                    css={{ marginBottom: scale(1) }}
                />
                <AttributeFieldArray name={fieldName} initialValue={null}>
                    {({ name: ithName }) => (
                        <FormTypedField
                            name={ithName}
                            fieldType={type === AttributeTypeEnum.INTEGER ? 'positiveInt' : 'positiveFloat'}
                            disabled={disabled}
                        />
                    )}
                </AttributeFieldArray>
            </>
        ) : null}

        {has_directory && !is_multiple ? (
            <FormFieldWrapper name={fieldName} label={`${name} ${is_required ? '* ' : ''}`} disabled={disabled}>
                <Select options={directoryOptions} hideClearButton={is_required} />
            </FormFieldWrapper>
        ) : null}

        {has_directory && is_multiple ? (
            <FormFieldWrapper name={fieldName} label={`${name} ${is_required ? '* ' : ''}`} disabled={disabled}>
                <SelectWithTags options={directoryOptions} />
            </FormFieldWrapper>
        ) : null}
    </>
);
