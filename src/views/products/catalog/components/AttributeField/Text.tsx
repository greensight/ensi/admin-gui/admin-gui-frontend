import { FormFieldWrapper, Select, SelectItem, SelectWithTags, Textarea } from '@ensi-platform/core-components';

import { Property } from '@api/catalog';

import { scale } from '@scripts/gds';

import { LegendAndErrorOfBlock } from '../LegendAndErrorOfBlock';
import AttributeFieldArray from './Array';

export interface TextFieldProps {
    fieldName: string;
    property: Property;
    directoryOptions: SelectItem[];
    disabled: boolean;
}

export const TextField = ({
    fieldName,
    property: { property_id, has_directory, is_multiple, is_required, name },
    directoryOptions,
    disabled,
}: TextFieldProps) => (
    <>
        {!has_directory && !is_multiple ? (
            <FormFieldWrapper name={fieldName} disabled={disabled} label={`${name}${is_required ? '*' : ''}`}>
                <Textarea maxLength={200} />
            </FormFieldWrapper>
        ) : null}

        {!has_directory && is_multiple ? (
            <>
                <LegendAndErrorOfBlock
                    name={`${name}`}
                    isRequired={Boolean(is_required)}
                    propertyId={property_id}
                    css={{ marginBottom: scale(1) }}
                    showError
                />
                <AttributeFieldArray name={fieldName} initialValue="">
                    {({ name: ithName }) => (
                        <FormFieldWrapper name={ithName} disabled={disabled} key={ithName}>
                            <Textarea maxLength={200} />
                        </FormFieldWrapper>
                    )}
                </AttributeFieldArray>
            </>
        ) : null}

        {has_directory && !is_multiple ? (
            <FormFieldWrapper
                name={fieldName}
                label={`${name} ${is_required ? '* ' : ''}`}
                disabled={disabled}
                css={{
                    maxWidth: scale(90),
                }}
            >
                <Select options={directoryOptions} optionsListWidth="field" hideClearButton={is_required} />
            </FormFieldWrapper>
        ) : null}

        {has_directory && is_multiple ? (
            <FormFieldWrapper
                name={fieldName}
                label={`${name} ${is_required ? '* ' : ''}`}
                disabled={disabled}
                css={{
                    maxWidth: scale(90),
                }}
            >
                <SelectWithTags options={directoryOptions} optionsListWidth="field" />
            </FormFieldWrapper>
        ) : null}
    </>
);
