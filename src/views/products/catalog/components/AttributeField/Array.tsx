import { useFieldArray, useForm } from '@ensi-platform/core-components';
import { Fragment, ReactNode } from 'react';

import { Button, Layout, LayoutProps, scale } from '@scripts/gds';

import IconPlus from '@icons/plus.svg';
import IconTrash from '@icons/small/trash.svg';

interface IAttributeFieldArray<AttributeType> extends Omit<LayoutProps, 'children' | 'wrap' | 'reverse' | 'type'> {
    name: string;
    initialValue: AttributeType | number | string | null;
    children: (args: { name: string; index: number }) => ReactNode | ReactNode[];
}

const AttributeFieldArray = <AttributeType extends Record<string, any>>({
    name,
    initialValue,
    children,
    ...props
}: IAttributeFieldArray<AttributeType>) => {
    const { disabled } = useForm();
    const { fields, append, remove } = useFieldArray({
        name,
    });

    const isOneField = fields.length === 1;

    return (
        <Layout cols={isOneField ? 1 : [1, `${scale(5)}px`]} gap={[scale(2), 0]} align="end" {...props}>
            {(fields as AttributeType[]).map((attr: AttributeType, index) => (
                <Fragment key={attr.id}>
                    <Layout.Item col={1}>{children({ name: `${name}[${index}]`, index })}</Layout.Item>

                    {!isOneField && (
                        <Layout.Item>
                            <Button
                                Icon={IconTrash}
                                theme="outline"
                                disabled={disabled}
                                hidden
                                onClick={() => remove(index)}
                            >
                                Удалить
                            </Button>
                        </Layout.Item>
                    )}
                </Fragment>
            ))}

            <Layout.Item col={1}>
                <Button Icon={IconPlus} disabled={disabled} onClick={() => append(initialValue)}>
                    Добавить значение атрибута
                </Button>
            </Layout.Item>
        </Layout>
    );
};

export default AttributeFieldArray;
