import { FormFieldWrapper, Select, SelectItem, SelectWithTags } from '@ensi-platform/core-components';

import { Property } from '@api/catalog';

import Dropzone from '@controls/Dropzone';

import { FileSizes, ImageAcceptTypes } from '@scripts/constants';
import { scale } from '@scripts/gds';

import { LegendAndErrorOfBlock } from '../LegendAndErrorOfBlock';
import AttributeFieldArray from './Array';

export interface ImageFieldProps {
    fieldName: string;
    property: Property;
    directoryOptions: SelectItem[];
    disabled: boolean;
}

export const ImageField = ({
    fieldName,
    property: { property_id, has_directory, is_multiple, is_required, name },
    directoryOptions,
    disabled,
}: ImageFieldProps) => (
    <>
        {!has_directory && !is_multiple ? (
            <>
                <LegendAndErrorOfBlock
                    name={`${name}`}
                    isRequired={Boolean(is_required)}
                    propertyId={property_id}
                    css={{ marginBottom: scale(1) }}
                />
                <FormFieldWrapper name={fieldName}>
                    <Dropzone
                        maxFiles={1}
                        maxSize={FileSizes.MB10}
                        multiple={false}
                        accept={ImageAcceptTypes}
                        disabled={disabled}
                    />
                </FormFieldWrapper>
            </>
        ) : null}

        {!has_directory && is_multiple ? (
            <>
                <LegendAndErrorOfBlock
                    name={`${name}`}
                    isRequired={Boolean(is_required)}
                    propertyId={property_id}
                    css={{ marginBottom: scale(1) }}
                />
                <AttributeFieldArray name={fieldName} initialValue={[null]}>
                    {({ name: ithName }) => (
                        <FormFieldWrapper name={ithName}>
                            <Dropzone maxSize={FileSizes.MB10} multiple accept={ImageAcceptTypes} disabled={disabled} />
                        </FormFieldWrapper>
                    )}
                </AttributeFieldArray>
            </>
        ) : null}

        {has_directory && !is_multiple ? (
            <FormFieldWrapper name={fieldName} label={`${name} ${is_required ? '* ' : ''}`} disabled={disabled}>
                <Select options={directoryOptions} hideClearButton={is_required} />
            </FormFieldWrapper>
        ) : null}
        {has_directory && is_multiple ? (
            <FormFieldWrapper name={fieldName} label={`${name} ${is_required ? '* ' : ''}`} disabled={disabled}>
                <SelectWithTags options={directoryOptions} />
            </FormFieldWrapper>
        ) : null}
    </>
);
