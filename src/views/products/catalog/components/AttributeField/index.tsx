import { useMemo } from 'react';

import { Property } from '@api/catalog';

import { AttributeTypeEnum } from '@views/products/scripts';

import { useAttributeOptions } from '../../scripts/hooks';
import { BooleanField } from './Boolean';
import { ColorField } from './Color';
import { DateTimeField } from './DateTime';
import { FileField } from './File';
import { ImageField } from './Image';
import { NumberField } from './Number';
import { StringField } from './String';
import { TextField } from './Text';

/**
 * Компонент который отвечает за рендер одного атрибута по переданному property
 * @param param0
 * @returns
 */
const AttributeField = ({ property, disabled }: { property: Property; disabled: boolean }) => {
    const { getDirectoryItems } = useAttributeOptions({
        properties: [property],
    });

    const { type } = property;

    const commonProps = useMemo(
        () => ({
            property,
            fieldName: 'value',
            directoryOptions: getDirectoryItems(property.id).map(e => ({
                label: e.name || '',
                value: e.id,
                content: e.label,
            })),
            disabled,
        }),
        [getDirectoryItems, property, disabled]
    );

    switch (type) {
        case AttributeTypeEnum.STRING:
            return <StringField {...commonProps} />;
        case AttributeTypeEnum.TEXT:
            return <TextField {...commonProps} />;

        case AttributeTypeEnum.INTEGER:
        case AttributeTypeEnum.DOUBLE:
            return <NumberField {...commonProps} />;

        case AttributeTypeEnum.COLOR:
            return <ColorField {...commonProps} />;

        case AttributeTypeEnum.IMAGE:
            return <ImageField {...commonProps} />;

        case AttributeTypeEnum.FILE:
            return <FileField {...commonProps} />;

        case AttributeTypeEnum.DATETIME:
            return <DateTimeField {...commonProps} />;

        case AttributeTypeEnum.BOOLEAN:
            return <BooleanField {...commonProps} />;

        default:
            return null;
    }
};

export default AttributeField;
