import { FormField, FormFieldWrapper, Select, SelectItem, SelectWithTags } from '@ensi-platform/core-components';

import { Property } from '@api/catalog';

import { Layout, scale } from '@scripts/gds';

import { LegendAndErrorOfBlock } from '../LegendAndErrorOfBlock';
import AttributeFieldArray from './Array';

export interface ColorFieldProps {
    fieldName: string;
    property: Property;
    directoryOptions: SelectItem[];
    disabled: boolean;
}

export const ColorField = ({
    fieldName,
    property: { property_id, has_directory, is_multiple, is_required, name },
    directoryOptions,
    disabled,
}: ColorFieldProps) => (
    <>
        {!has_directory && !is_multiple ? (
            <Layout cols={3} gap="8px 16px">
                <Layout.Item col={3}>
                    <LegendAndErrorOfBlock
                        name={`${name}`}
                        isRequired={Boolean(is_required)}
                        propertyId={property_id}
                    />
                </Layout.Item>
                <Layout.Item>
                    <FormField label="Название цвета" name={`${fieldName}[0].name`} disabled={disabled} />
                </Layout.Item>
                <Layout.Item>
                    <FormField label="Код цвета" name={`${fieldName}[0].value`} disabled={disabled} />
                </Layout.Item>
                <Layout.Item>
                    <FormField name={`${fieldName}[0].value`} label="Палитра" type="color" disabled={disabled} />
                </Layout.Item>
            </Layout>
        ) : null}

        {!has_directory && is_multiple ? (
            <>
                <LegendAndErrorOfBlock
                    name={`${name}`}
                    isRequired={Boolean(is_required)}
                    propertyId={property_id}
                    css={{ marginBottom: scale(1) }}
                    showError
                />
                <AttributeFieldArray name={fieldName} initialValue={{ name: '', value: '' }}>
                    {({ name: ithName }) => (
                        <Layout
                            cols={4}
                            css={{
                                gridTemplateColumns: '1fr 1fr 1fr auto !important',
                                alignItems: 'end',
                                marginTop: scale(2),
                            }}
                        >
                            <Layout.Item>
                                <FormField label="Название цвета" name={`${ithName}.name`} disabled={disabled} />
                            </Layout.Item>
                            <Layout.Item>
                                <FormField label="Код цвета" name={`${ithName}.value`} disabled={disabled} />
                            </Layout.Item>
                            <Layout.Item>
                                <FormField name={`${ithName}.value`} label="Палитра" type="color" disabled={disabled} />
                            </Layout.Item>
                        </Layout>
                    )}
                </AttributeFieldArray>
            </>
        ) : null}

        {has_directory && !is_multiple ? (
            <FormFieldWrapper name={fieldName} label={`${name} ${is_required ? '* ' : ''}`} disabled={disabled}>
                <Select options={directoryOptions} hideClearButton={is_required} />
            </FormFieldWrapper>
        ) : null}

        {has_directory && is_multiple ? (
            <FormFieldWrapper name={fieldName} label={`${name} ${is_required ? '* ' : ''}`} disabled={disabled}>
                <SelectWithTags options={directoryOptions} />
            </FormFieldWrapper>
        ) : null}
    </>
);
