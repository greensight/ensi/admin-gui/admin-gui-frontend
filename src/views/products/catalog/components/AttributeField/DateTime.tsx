import {
    CalendarInput,
    FormFieldWrapper,
    Input,
    Select,
    SelectItem,
    SelectWithTags,
} from '@ensi-platform/core-components';

import { Property } from '@api/catalog';

import { scale } from '@scripts/gds';

import { LegendAndErrorOfBlock } from '../LegendAndErrorOfBlock';
import AttributeFieldArray from './Array';

export interface DateTimeFieldProps {
    fieldName: string;
    property: Property;
    directoryOptions: SelectItem[];
    disabled: boolean;
}

export const DateTimeField = ({
    fieldName,
    property: { property_id, has_directory, is_multiple, is_required, name },
    directoryOptions,
    disabled,
}: DateTimeFieldProps) => (
    <>
        {!has_directory && !is_multiple ? (
            <>
                <LegendAndErrorOfBlock
                    name={`${name}`}
                    isRequired={Boolean(is_required)}
                    propertyId={property_id}
                    css={{ marginBottom: scale(1) }}
                />
                <div css={{ display: 'flex', alignItems: 'center' }}>
                    <FormFieldWrapper name={`${fieldName}[0].date`} css={{ marginRight: scale(1) }} label="Дата">
                        <CalendarInput picker placeholder="Дата" disabled={disabled} view="date" platform="desktop" />
                    </FormFieldWrapper>
                    <FormFieldWrapper name={`${fieldName}[0].time`} label="Время" placeholder="Время">
                        <Input type="time" disabled={disabled} />
                    </FormFieldWrapper>
                </div>
            </>
        ) : null}

        {!has_directory && is_multiple ? (
            <>
                <LegendAndErrorOfBlock
                    name={`${name}`}
                    isRequired={Boolean(is_required)}
                    propertyId={property_id}
                    css={{ marginBottom: scale(1) }}
                    showError
                />
                <AttributeFieldArray
                    name={fieldName}
                    initialValue={{
                        date: '',
                        time: '',
                    }}
                >
                    {({ name: ithName }) => (
                        <div
                            css={{
                                display: 'flex',
                                gap: scale(2),
                                alignItems: 'center',
                            }}
                            data-name={ithName}
                        >
                            <FormFieldWrapper name={`${ithName}.date`} css={{ marginRight: scale(1) }}>
                                <CalendarInput
                                    picker
                                    placeholder="Дата"
                                    label="Дата"
                                    disabled={disabled}
                                    view="date"
                                    platform="desktop"
                                />
                            </FormFieldWrapper>
                            <FormFieldWrapper name={`${ithName}.time`} label="Время" placeholder="Время">
                                <Input type="time" disabled={disabled} />
                            </FormFieldWrapper>
                        </div>
                    )}
                </AttributeFieldArray>
            </>
        ) : null}

        {has_directory && !is_multiple ? (
            <FormFieldWrapper name={fieldName} label={`${name} ${is_required ? '*' : ''}`}>
                <Select options={directoryOptions} disabled={disabled} hideClearButton={is_required} />
            </FormFieldWrapper>
        ) : null}

        {has_directory && is_multiple ? (
            <FormFieldWrapper name={fieldName} label={`${name} ${is_required ? '*' : ''}`}>
                <SelectWithTags options={directoryOptions} disabled={disabled} />
            </FormFieldWrapper>
        ) : null}
    </>
);
