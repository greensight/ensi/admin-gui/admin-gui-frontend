import { FormMessage, useFormContext } from '@ensi-platform/core-components';
import { useMemo } from 'react';

import Legend from '@controls/Legend';

import { scale, useTheme } from '@scripts/gds';

export const LegendAndErrorOfBlock = ({
    name,
    isRequired,
    propertyId,
    className,
    showError = false,
}: {
    name: string;
    isRequired: boolean;
    propertyId: string | number;
    className?: string;
    showError?: boolean;
}) => {
    const { formState } = useFormContext();
    const { colors } = useTheme();

    const hasError = useMemo(() => {
        const fieldError = (formState?.errors?.attributes as Record<any, any>)?.[propertyId] as
            | object[]
            | object
            | undefined;

        if (Array.isArray(fieldError)) {
            return fieldError.length > 0;
        }

        return !!fieldError;
    }, [formState, propertyId]);

    return (
        <>
            <Legend
                label={`${name} ${isRequired ? '* ' : ''}`}
                css={{ paddingBottom: 0, color: hasError ? colors?.danger : undefined }}
                className={className}
                error={hasError ? '' : undefined}
            />

            {showError &&
            (formState.errors?.attributes as Record<any, any>)?.[propertyId] &&
            !Array.isArray((formState.errors?.attributes as Record<any, any>)?.[propertyId]) ? (
                <FormMessage
                    message={(formState.errors?.attributes as Record<any, any>)[propertyId]?.message}
                    css={{ marginTop: scale(1, true) }}
                />
            ) : null}
        </>
    );
};
