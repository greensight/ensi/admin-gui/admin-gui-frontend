import { Popup, PopupFooter, PopupHeader } from '@ensi-platform/core-components';
import { FC } from 'react';

import { Button, Layout, scale } from '@scripts/gds';

interface ConfirmPopupProps {
    isOpen: boolean;
    onClose: () => void;
    onSave: () => void;
    isSelected: boolean;
}

const ConfirmPopup: FC<ConfirmPopupProps> = ({ isOpen, onClose, onSave, isSelected }) => (
    <Popup open={isOpen} onClose={onClose} size="lg">
        <PopupHeader
            title={isSelected ? 'Внести изменения во все выбранные товары?' : 'Внести изменения во все товары?'}
        />
        <PopupFooter css={{ border: 'none' }}>
            <Layout gap={scale(2)}>
                <Layout.Item>
                    <Button onClick={onClose} theme="outline">
                        Нет
                    </Button>
                </Layout.Item>
                <Layout.Item>
                    <Button onClick={onSave}>Да</Button>
                </Layout.Item>
            </Layout>
        </PopupFooter>
    </Popup>
);

export default ConfirmPopup;
