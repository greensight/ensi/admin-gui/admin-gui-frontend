import { AdultField } from './AdultField';

import { BrandField } from './BrandField';
import { CategoryField } from './CategoryField';
import { NameplatesField } from './NameplatesField';
import { TypeField } from './TypeField';
import { ProductFieldsComponent } from './types';

const ProductFields: ProductFieldsComponent = ({ children }) => children;

ProductFields.Type = TypeField;
ProductFields.Category = CategoryField;
ProductFields.Brand = BrandField;
ProductFields.Adult = AdultField;

ProductFields.Nameplates = NameplatesField;

export default ProductFields;
