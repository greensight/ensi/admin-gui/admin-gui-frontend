import { FormCheckbox, FormFieldWrapper } from '@ensi-platform/core-components';

export const AdultField = ({ name = 'is_adult', disabled }: { name?: string; disabled?: boolean }) => (
    <FormFieldWrapper name={name}>
        <FormCheckbox disabled={disabled}>Товар 18+</FormCheckbox>
    </FormFieldWrapper>
);
