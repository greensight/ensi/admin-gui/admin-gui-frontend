import { AutocompleteAsync, FormFieldWrapper } from '@ensi-platform/core-components';

import { SearchNameplatesResponse } from '@api/content/types/nameplates';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

const API_URL = 'cms/nameplates';

export const NameplatesField = ({
    name = 'nameplates',
    isRequired = false,
    disabled,
}: {
    name?: string;
    isRequired?: boolean;
    disabled?: boolean;
}) => {
    const apiClient = useAuthApiClient();

    const asyncSearchFn = async (query: string) => {
        const res: SearchNameplatesResponse = await apiClient.post(`${API_URL}:search`, {
            data: {
                filter: {
                    name_like: query,
                    is_active: true,
                },
            },
        });
        return {
            options: res.data.map(e => ({
                label: e.name,
                value: e.id,
            })),
            hasMore: false,
        };
    };

    const asyncOptionsByValuesFn = async (vals: number[] | { value: number }[]) => {
        const actualValues = vals.map(v => {
            if (typeof v === 'object' && 'value' in v) return Number(v.value);
            return Number(v);
        });
        const res: SearchNameplatesResponse = await apiClient.post(`${API_URL}:search`, {
            data: {
                filter: { id: actualValues },
            },
        });
        return res.data.map(e => ({
            label: e.name,
            value: e.id,
            disabled: !e.is_active,
        }));
    };

    return (
        <FormFieldWrapper name={name} label={`Теги${isRequired ? '*' : ''}`}>
            <AutocompleteAsync
                moveInputToNewLine={false}
                multiple
                asyncOptionsByValuesFn={asyncOptionsByValuesFn}
                asyncSearchFn={asyncSearchFn}
                disabled={disabled}
            />
        </FormFieldWrapper>
    );
};
