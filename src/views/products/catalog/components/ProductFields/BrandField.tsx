import { FormFieldWrapper, Select } from '@ensi-platform/core-components';
import { useMemo } from 'react';

import { useBrands } from '@api/catalog';

import { useError } from '@context/modal';

export const BrandField = ({ name = 'brand_id', disabled }: { name?: string; disabled?: boolean }) => {
    const { data: apiDataBrands, error: apiErrorBrands } = useBrands({
        filter: {
            is_active: true,
        },
        pagination: {
            limit: -1,
            offset: 0,
            type: 'offset',
        },
    });
    useError(apiErrorBrands);

    const brandsOptions = useMemo(
        () =>
            apiDataBrands?.data
                ? [
                      { label: 'Нет', value: null },
                      ...(apiDataBrands?.data || []).map(({ id, name }) => ({ label: name, value: id })),
                  ]
                : [],
        [apiDataBrands]
    );

    return (
        <FormFieldWrapper name={name} label="Бренд" disabled={disabled}>
            <Select options={brandsOptions} />
        </FormFieldWrapper>
    );
};
