import { Autocomplete, FormFieldWrapper } from '@ensi-platform/core-components';
import { useMemo } from 'react';

import { useCategoriesTree } from '@api/catalog';

import { useError } from '@context/modal';

import { customFlatWithParents } from '@views/products/scripts';

export const CategoryField = ({
    name = 'category_ids',
    isRequired = true,
    disabled,
}: {
    name?: string;
    isRequired?: boolean;
    disabled?: boolean;
}) => {
    const { data: categoriesTree, error: errorCategoriesTree } = useCategoriesTree();
    useError(errorCategoriesTree);

    const categoriesOptions = useMemo(
        () =>
            customFlatWithParents(categoriesTree?.data).map(e => ({
                value: e.value,
                label: e.label,
                content: e.label,
            })),
        [categoriesTree?.data]
    );

    return (
        <FormFieldWrapper name={name}>
            <Autocomplete
                disabled={disabled}
                label={`Категории товара${isRequired ? '*' : ''}`}
                options={categoriesOptions}
                allowUnselect={false}
                hideClearButton
                optionsListWidth="field"
                multiple
            />
        </FormFieldWrapper>
    );
};
