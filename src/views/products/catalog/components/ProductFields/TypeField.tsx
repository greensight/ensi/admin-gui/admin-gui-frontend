import { FormFieldWrapper } from '@ensi-platform/core-components';
import { useMemo } from 'react';

import { useProductsTypes } from '@api/catalog';

import { useError } from '@context/modal';

import Select from '@controls/Select';

export const TypeField = ({
    name = 'type',
    isRequired = true,
    disabled,
}: {
    name?: string;
    isRequired?: boolean;
    disabled?: boolean;
}) => {
    const { data: apiTypes, error: apiTypesError } = useProductsTypes();
    useError(apiTypesError);

    const productTypes = useMemo(
        () => (apiTypes ? apiTypes.data.map(el => ({ value: el.id, key: el.name })) : []),
        [apiTypes]
    );

    return (
        <FormFieldWrapper name={name} label={`Тип товара${isRequired ? '*' : ''}`}>
            <Select disabled={disabled} options={productTypes} hideClearButton optionsListWidth="field" />
        </FormFieldWrapper>
    );
};
