import { ReactNode } from 'react';

import { AdultField } from './AdultField';
import { BrandField } from './BrandField';
import { CategoryField } from './CategoryField';
import { NameplatesField } from './NameplatesField';
import { TypeField } from './TypeField';

export type ProductFieldsProps = {
    children?: ReactNode | ReactNode[];
};

export type ProductFieldsComponent = ((props: ProductFieldsProps) => ReactNode) & {
    Type: typeof TypeField;
    Category: typeof CategoryField;
    Brand: typeof BrandField;
    Adult: typeof AdultField;
    Nameplates: typeof NameplatesField;
};
