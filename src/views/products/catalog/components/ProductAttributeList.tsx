import { SelectItem } from '@ensi-platform/core-components';
import Image from 'next/image';
import { useCallback, useMemo } from 'react';

import { useProductDetail, usePropertyDirectories } from '@api/catalog';

import { useError } from '@context/modal';

import { AttributeTypeEnum } from '@views/products/scripts';

import LoadWrapper from '@controls/LoadWrapper';

import { CREATE_PARAM } from '@scripts/constants';
import { Layout, scale } from '@scripts/gds';

import { useProductForm, useProductsAllowedFields } from '../scripts/hooks';
import { BooleanField } from './AttributeField/Boolean';
import { ColorField } from './AttributeField/Color';
import { DateTimeField } from './AttributeField/DateTime';
import { FileField } from './AttributeField/File';
import { ImageField } from './AttributeField/Image';
import { NumberField } from './AttributeField/Number';
import { StringField } from './AttributeField/String';
import { TextField } from './AttributeField/Text';

/**
 * Компонент который отвечает за рендер списка атрибутов по id товара
 */
export const ProductAttributesList = ({
    id,
    isCreationPage,
}: {
    id: typeof CREATE_PARAM | number;
    isCreationPage: boolean;
}) => {
    const { canEditField } = useProductsAllowedFields();

    // деталка товара
    const { data: product } = useProductDetail(
        {
            id: String(id),
            include: [
                'brand',
                'brand_count',
                'categories',
                'categories_count',
                'images',
                'images_count',
                'attributes',
                'categories.properties',
                'categories.hidden_properties',
                'product_groups.products',
            ],
        },
        Boolean(id) && !isCreationPage
    );

    // добавляем значения атрибутов товара в список атрибутов родительской категории товара
    const { categoryPropertiesWithProductValues } = useProductForm({ productData: product?.data });

    // получаем коллекцию всех значений справочников всех атрибутов-справочников
    const {
        data: directories,
        isFetching: isLoadingDirectories,
        error: directoriesError,
    } = usePropertyDirectories(
        {
            sort: [],
            include: [],
            filter: {
                property_id: categoryPropertiesWithProductValues
                    ?.filter(({ has_directory }) => has_directory)
                    ?.map(({ property_id }) => property_id),
            },
            pagination: { type: 'offset', limit: -1, offset: 0 },
        },
        Boolean(product)
    );
    useError(directoriesError);

    const directoriesCollection = useMemo(() => directories?.data, [directories]);

    const optionStyles = useCallback(
        (backgroundColor: string) => ({
            display: 'flex',
            width: scale(10, true),
            height: scale(3, true),
            borderRadius: '50vh',
            background: `${backgroundColor}`,
            marginRight: scale(1),
        }),
        []
    );

    // фильтруем значения справочников для опций селектов
    const getDirectoryItems = useCallback(
        (property_id: number) =>
            (directoriesCollection || [])
                .filter(({ property_id: directoryPropertyId }) => directoryPropertyId === property_id)
                .map<SelectItem>(item => ({
                    label: item?.name || `${item.id}`,
                    value: item.id,

                    ...(item?.type === AttributeTypeEnum.COLOR && {
                        content: (
                            <div css={{ display: 'inline-flex', alignItems: 'center', gap: scale(1) }}>
                                <span style={optionStyles(`${item?.value}`)} />
                                <strong>{item?.name}</strong>
                            </div>
                        ),
                    }),

                    ...(item?.type === AttributeTypeEnum.IMAGE && {
                        content: (
                            <div css={{ display: 'inline-flex', alignItems: 'center', gap: scale(1) }}>
                                <div css={{ width: scale(6), height: scale(6), borderRadius: scale(1, true) }}>
                                    <Image
                                        width={scale(6)}
                                        height={scale(6)}
                                        unoptimized
                                        src={item?.url || '/noimage.png'}
                                        alt=""
                                    />
                                </div>
                                <strong>{item?.name}</strong>
                            </div>
                        ),
                    }),
                })),
        [directoriesCollection, optionStyles]
    );

    const canEdit = canEditField('attributes');

    return (
        <LoadWrapper isLoading={isLoadingDirectories}>
            <Layout cols={2} gap={scale(2)} css={{ marginBottom: scale(2) }}>
                {categoryPropertiesWithProductValues?.map(property => {
                    const { type, property_id } = property;
                    const commonProps = {
                        fieldName: `attributes[${property_id}]`,
                        directoryOptions: getDirectoryItems(property_id),
                        property,
                        disabled: !canEdit,
                    };
                    switch (type) {
                        case AttributeTypeEnum.STRING:
                            return (
                                <Layout.Item col={2} key={property_id}>
                                    <StringField {...commonProps} />
                                </Layout.Item>
                            );
                        case AttributeTypeEnum.TEXT:
                            return (
                                <Layout.Item col={2} key={property_id}>
                                    <TextField {...commonProps} />
                                </Layout.Item>
                            );

                        case AttributeTypeEnum.INTEGER:
                        case AttributeTypeEnum.DOUBLE:
                            return (
                                <Layout.Item col={2} key={property_id}>
                                    <NumberField {...commonProps} />
                                </Layout.Item>
                            );

                        case AttributeTypeEnum.COLOR:
                            return (
                                <Layout.Item col={2} key={property_id}>
                                    <ColorField {...commonProps} />
                                </Layout.Item>
                            );

                        case AttributeTypeEnum.IMAGE:
                            return (
                                <Layout.Item col={2} key={property_id}>
                                    <ImageField {...commonProps} />
                                </Layout.Item>
                            );

                        case AttributeTypeEnum.FILE:
                            return (
                                <Layout.Item col={2} key={property_id}>
                                    <FileField {...commonProps} />
                                </Layout.Item>
                            );

                        case AttributeTypeEnum.DATETIME:
                            return (
                                <Layout.Item col={2} key={property_id}>
                                    <DateTimeField {...commonProps} />
                                </Layout.Item>
                            );
                        case AttributeTypeEnum.BOOLEAN:
                            return (
                                <Layout.Item col={2} key={property_id}>
                                    <BooleanField {...commonProps} />
                                </Layout.Item>
                            );
                        default:
                            return null;
                    }
                })}
            </Layout>
        </LoadWrapper>
    );
};
