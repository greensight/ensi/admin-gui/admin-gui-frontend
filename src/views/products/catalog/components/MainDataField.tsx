import { FormCheckbox, FormField, FormFieldWrapper, Textarea } from '@ensi-platform/core-components';

import { MAIN_DATA_FIELD_NAMES, MassEditableProductField as MainDataFieldType } from '../[id]/scripts/settings';
import ProductFields from './ProductFields';

export const MainDataField = ({ fieldName }: { fieldName: MainDataFieldType }) => {
    switch (fieldName) {
        case 'type':
            return <ProductFields.Type name="value" isRequired={false} />;
        case 'category_id':
            return <ProductFields.Category name="value" isRequired={false} />;
        case 'brand_id':
            return <ProductFields.Brand name="value" />;
        case 'nameplates':
            return <ProductFields.Nameplates name="value" isRequired={false} />;
        case 'description':
            return (
                <FormFieldWrapper name="value" label={MAIN_DATA_FIELD_NAMES[fieldName]}>
                    <Textarea />
                </FormFieldWrapper>
            );
        case 'allow_publish':
        case 'is_adult':
            return (
                <FormFieldWrapper name="value" label={MAIN_DATA_FIELD_NAMES[fieldName]}>
                    <FormCheckbox>Да</FormCheckbox>
                </FormFieldWrapper>
            );
        default:
            return <FormField name="value" label={MAIN_DATA_FIELD_NAMES[fieldName]} />;
    }
};
