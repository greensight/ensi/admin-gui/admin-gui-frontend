import { useAccess } from '@scripts/hooks';

export const useOffersAccess = () => {
    const access = useAccess({
        LIST: {
            view: 2801, // Пользователю доступен табличный список офферов с фильтрами и поиском
        },
        ID: {
            view: 2807, // Пользователю доступна детальная страница оффера со всеми данными для просмотра

            edit: 2806, // Пользователю доступна функция редактирование любых данных оффера и совершения любых действий с ним

            mainData: {
                edit: 2802, // Пользователю доступно полное редактирование на вкладке "Основные данные"
            },
            stocks: {
                edit: 2803, // Пользователю доступно полное редактирование на вкладке "Стоки"
            },
            granularPublish: { edit: 2804 }, // Пользователю доступно управление чекбоксом "Публикация"

            granularPrice: { edit: 2805 }, // Пользователю доступно управлением полем "Стоимость"

            offersEntitiesSync: 2808, // Пользователю доступно обновление данных офферов

            offersMigration: 2809, // Пользователю доступна миграция состояния офферов
        },
    });

    if (access.ID.edit) {
        access.ID.granularPrice.edit = true;
        access.ID.granularPublish.edit = true;
        access.ID.mainData.edit = true;
        access.ID.stocks.edit = true;
    }

    if (access.ID.mainData.edit) {
        access.ID.granularPrice.edit = true;
        access.ID.granularPublish.edit = true;
    }

    return {
        ...access,
        canEditAny:
            access.ID.granularPrice.edit ||
            access.ID.granularPublish.edit ||
            access.ID.mainData.edit ||
            access.ID.stocks.edit,
    };
};
