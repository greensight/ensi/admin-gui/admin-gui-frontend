import { useActionPopup } from '@ensi-platform/core-components';
import { useCallback, useMemo } from 'react';

import { Offer, useOffersMeta, useOffersSynchronization, useSearchOffers, useSyncEntities } from '@api/catalog';

import { useError, useSuccess } from '@context/modal';

import ListBuilder from '@components/ListBuilder';
import { TooltipItem } from '@components/Table';

import { ModalMessages } from '@scripts/constants';
import { ActionType } from '@scripts/enums';
import { Button, scale } from '@scripts/gds';
import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import { useOffersAccess } from './useOffersAccess';

export default function OffersList() {
    const access = useOffersAccess();

    const { popupState, popupDispatch, ActionPopup, ActionEnum } = useActionPopup();
    const offersSynchronization = useOffersSynchronization();
    const updateOffers = useSyncEntities();

    useError(offersSynchronization.error);
    useSuccess(offersSynchronization.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    useError(updateOffers.error);
    useSuccess(updateOffers.isSuccess && 'Процесс обновления запущен');

    const goToDetailPage = useGoToDetailPage();

    const tooltipContent = useMemo<TooltipItem[]>(
        () => [
            {
                type: 'edit',
                text: 'Редактировать оффер',
                action: goToDetailPage,
                isDisable: !access.ID.view,
            },
        ],
        [access.ID.view, goToDetailPage]
    );

    const renderHeader = useCallback(
        () => (
            <div
                css={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    width: '100%',
                    alignItems: 'center',
                    gap: scale(2),
                    margin: `0 0 ${scale(2)}px 0`,
                }}
            >
                {access.LIST.view && access.ID.offersMigration && (
                    <Button
                        type="button"
                        onClick={() => {
                            popupDispatch({
                                type: ActionType.Edit,
                                payload: {
                                    title: `Запустить синхронизацию состояния офферов относительно PIM и складов?`,
                                    popupAction: ActionEnum.COPY, // TODO Вернуть в npm пакет ActionEnum.CONFIRM,
                                    onAction: async () => {
                                        try {
                                            await offersSynchronization.mutateAsync();
                                        } catch (err) {
                                            console.error(err);
                                        }
                                    },
                                },
                            });
                        }}
                        disabled={!access.ID.offersMigration}
                    >
                        Синхронизировать состояния офферов
                    </Button>
                )}
                {access.LIST.view && access.ID.offersEntitiesSync && (
                    <Button
                        type="button"
                        onClick={() => {
                            popupDispatch({
                                type: ActionType.Edit,
                                payload: {
                                    title: `Запустить обновление данных офферов из мастер систем?`,
                                    popupAction: ActionEnum.COPY, // TODO Вернуть в npm пакет ActionEnum.CONFIRM,
                                    onAction: async () => {
                                        try {
                                            await updateOffers.mutate(null);
                                        } catch (err) {
                                            console.error(err);
                                        }
                                    },
                                },
                            });
                        }}
                        disabled={!access.ID.offersEntitiesSync}
                    >
                        Запустить обновление данных офферов
                    </Button>
                )}
            </div>
        ),
        [access.ID.view]
    );

    return (
        <ListBuilder<Offer>
            access={access}
            searchHook={useSearchOffers}
            metaHook={useOffersMeta}
            headerInner={renderHeader}
            tooltipItems={tooltipContent}
            hiddenRowSelect
            title="Список Офферов"
        >
            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </ListBuilder>
    );
}
