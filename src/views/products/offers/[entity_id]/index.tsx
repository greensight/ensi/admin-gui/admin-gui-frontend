import {
    DescriptionList,
    DescriptionListItem,
    FormFieldWrapper,
    Tabs,
    getDefaultDates,
} from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { useMemo, useRef } from 'react';
import * as Yup from 'yup';

import { useOffer, usePatchOffer } from '@api/catalog';

import { useError, useSuccess } from '@context/modal';

import Switcher from '@controls/Switcher';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { ErrorMessages, ModalMessages } from '@scripts/constants';
import { RedirectMethods } from '@scripts/enums';
import { scale, typography } from '@scripts/gds';
import { fromKopecksToRouble, fromRoubleToKopecks, isPageNotFound } from '@scripts/helpers';
import { useTabs } from '@scripts/hooks/useTabs';

import { useOffersAccess } from '../useOffersAccess';
import { MainData } from './MainData';
import { StocksTable } from './StocksTable';

const LISTING_URL = '/products/offers';

export default function Offer() {
    const access = useOffersAccess();

    const { query, push } = useRouter();
    const entityId = +`${query?.entity_id}`;

    const { data: apiOffer, isFetching: isLoading, error } = useOffer({ id: entityId }, {}, access.ID.view);
    useError(error);

    const offer = apiOffer?.data;

    const title = `Оффер #${entityId || ''}`;
    const redirectAfterSave = useRef(false);

    const initialValues = useMemo(
        () => ({
            is_active: offer?.is_active || false,
            allow_publish: offer?.allow_publish || false,
            product_id: offer?.product_id || '',
            price: fromKopecksToRouble(offer?.price || 0),
        }),
        [offer]
    );

    const patch = usePatchOffer();
    useError(patch.error);
    useSuccess(patch.isSuccess && ModalMessages.SUCCESS_UPDATE);

    const { getTabsProps } = useTabs();

    const isNotFound = isPageNotFound({ id: entityId, error });

    return (
        <PageWrapper
            title={title}
            isLoading={isLoading || patch.isPending}
            isForbidden={!access.ID.view}
            isNotFound={isNotFound}
        >
            <FormWrapper
                disabled={!access.canEditAny}
                initialValues={initialValues}
                enableReinitialize
                validationSchema={Yup.object().shape({
                    price: Yup.number()
                        .transform(value => (Number.isNaN(+value) ? undefined : value))
                        .min(0, `${ErrorMessages.GREATER_OR_EQUAL} 0`)
                        .nullable(),
                })}
                onSubmit={async vals => {
                    await patch.mutateAsync({
                        id: entityId,
                        ...(+initialValues.price !== +vals.price && {
                            price: fromRoubleToKopecks(+vals.price),
                        }),
                        ...(initialValues.allow_publish !== vals.allow_publish && {
                            allow_publish: vals.allow_publish || false,
                        }),
                    });

                    return {
                        redirectPath: redirectAfterSave.current ? LISTING_URL : `${LISTING_URL}/${entityId}`,
                        method: RedirectMethods.push,
                    };
                }}
            >
                <PageTemplate
                    backlink={{
                        text: 'Назад',
                        href: LISTING_URL,
                    }}
                    aside={
                        <DescriptionList>
                            <DescriptionListItem
                                name="Активность"
                                value={
                                    <FormFieldWrapper name="is_active" label="Активность" disabled>
                                        <Switcher>Активность</Switcher>
                                    </FormFieldWrapper>
                                }
                            />
                            <DescriptionListItem name="ID" value={entityId} />
                            {getDefaultDates({ ...offer }).map(item => (
                                <DescriptionListItem {...item} key={item.name} />
                            ))}
                        </DescriptionList>
                    }
                    controls={
                        <PageControls access={access} gap={scale(1)}>
                            <PageControls.Close
                                onClick={() => {
                                    push(LISTING_URL);
                                }}
                            />
                            <PageControls.Apply
                                disabled={!access.canEditAny}
                                onClick={() => {
                                    redirectAfterSave.current = false;
                                }}
                            />
                            <PageControls.Save
                                disabled={!access.canEditAny}
                                onClick={() => {
                                    redirectAfterSave.current = true;
                                }}
                            />
                        </PageControls>
                    }
                    h1={title}
                    customChildren
                >
                    <Tabs {...getTabsProps()} css={{ width: '100%' }}>
                        <Tabs.Tab id="0" title="Основные данные">
                            <MainData offer={offer} />
                        </Tabs.Tab>
                    </Tabs>
                </PageTemplate>
            </FormWrapper>
            <h4 css={{ marginTop: scale(4), marginBottom: scale(2), ...typography('h3') }}>Стоки</h4>
            <StocksTable offerId={entityId} />
        </PageWrapper>
    );
}
