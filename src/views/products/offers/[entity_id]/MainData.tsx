import { Block, FormFieldWrapper, FormTypedField, Picture, useFormContext } from '@ensi-platform/core-components';
import Link from 'next/link';

import { Offer, useProductDetail } from '@api/catalog';

import LoadingSkeleton from '@controls/LoadingSkeleton';
import Switcher from '@controls/Switcher';

import { Layout, scale, typography } from '@scripts/gds';

import { useOffersAccess } from '../useOffersAccess';

export const MainData = ({ offer }: { offer?: Offer }) => {
    const access = useOffersAccess();

    const { data: apiProduct, isFetched } = useProductDetail(
        {
            id: offer?.product_id.toString()!,
            include: ['images'],
        },
        !!offer?.product_id
    );

    const product = apiProduct?.data;
    const { setValue, getValues } = useFormContext();

    return (
        <Block>
            <Block.Body>
                <Layout cols={2} gap={scale(2)}>
                    <Layout.Item col={2}>
                        {!isFetched && <LoadingSkeleton height={scale(8)} css={{ width: '100%' }} />}

                        {isFetched && product ? (
                            <Link
                                href={`/products/catalog/${product.id}`}
                                passHref
                                css={{
                                    ':hover': {
                                        opacity: 0.5,
                                    },
                                }}
                                target="_blank"
                            >
                                <p
                                    css={{
                                        ...typography('captionBold'),
                                        marginBottom: scale(1),
                                    }}
                                >
                                    {product.name} (#{product.id})
                                </p>
                                <Picture
                                    alt="изображение"
                                    height={scale(6)}
                                    width={scale(6)}
                                    sources={[
                                        {
                                            image: (product.images || [])[0]?.url || '/noimage.png',
                                            media: '(min-width: 0px)',
                                        },
                                    ]}
                                />
                            </Link>
                        ) : (
                            <small>Товар не существует</small>
                        )}
                    </Layout.Item>

                    <Layout.Item col={2}>
                        <FormFieldWrapper
                            name="allow_publish"
                            label="Разрешить публикацию"
                            disabled={!access.ID.granularPublish.edit}
                        >
                            <Switcher description="Оффер публикуется в публичной части сайта, если активность оффера выставлена и статус публикации оффера выставлен.">
                                Разрешить публикацию
                            </Switcher>
                        </FormFieldWrapper>
                    </Layout.Item>
                    <Layout.Item>
                        <FormTypedField
                            name="price"
                            label="Цена"
                            fieldType="positiveFloat"
                            disabled={!access.ID.granularPrice.edit}
                            onBlur={() => {
                                const currentValue = getValues('price');
                                if (currentValue) setValue('price', +(+getValues('price')).toFixed(2));
                            }}
                        />
                    </Layout.Item>
                </Layout>
            </Block.Body>
        </Block>
    );
};
