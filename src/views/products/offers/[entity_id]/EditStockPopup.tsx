import {
    Form,
    FormTypedField,
    Loader,
    Popup,
    PopupContent,
    PopupFooter,
    PopupHeader,
} from '@ensi-platform/core-components';
import { Dispatch, useCallback, useMemo } from 'react';
import * as Yup from 'yup';

import { usePatchStock } from '@api/catalog';

import { ErrorMessages } from '@scripts/constants';
import { ActionType } from '@scripts/enums';
import { Button, Layout } from '@scripts/gds';
import { Action } from '@scripts/hooks/usePopupState';

type StockData = {
    store_id: number;
    stock_id: number;
    qty: number;
};

type EditableData = Pick<StockData, 'qty'>;

export type EditStockPopupState = {
    data?: StockData | null;
    open: boolean;
    action: ActionType;
};

export interface EditStockPopupProps {
    state: Partial<EditStockPopupState>;
    dispatch: Dispatch<Action<Partial<EditStockPopupState>>>;
}

export const EditStockPopup = ({ state, dispatch }: EditStockPopupProps) => {
    const { data } = state;

    const initialValues = useMemo<EditableData>(
        () => ({
            qty: data?.qty || 0,
        }),
        [data]
    );

    const patchStock = usePatchStock();

    const onSubmit = useCallback(
        async (values: EditableData) => {
            await patchStock.mutateAsync({
                id: data?.stock_id!,
                qty: values.qty,
            });

            dispatch({ type: ActionType.Close });
        },
        [data?.stock_id, dispatch, patchStock]
    );

    return (
        <Popup
            open={state.open!}
            onClose={() => dispatch({ type: ActionType.PreClose })}
            onUnmount={() => dispatch({ type: ActionType.Close })}
            size="minMd"
        >
            <Form
                initialValues={initialValues}
                onSubmit={onSubmit}
                validationSchema={Yup.object({
                    qty: Yup.number()
                        .transform(val => (Number.isNaN(+val) ? undefined : val))
                        .min(0, `${ErrorMessages.GREATER_OR_EQUAL} 0`)
                        .nullable(),
                })}
            >
                {patchStock.isPending && <Loader />}
                <PopupHeader title={`Редактирование стока #${data?.stock_id}`} />
                <PopupContent>
                    <p>Склад #{data?.store_id}</p>
                    <Layout cols={1}>
                        <FormTypedField fieldType="positiveFloat" name="qty" label="Количество" />
                    </Layout>
                </PopupContent>
                <PopupFooter>
                    <Button theme="outline" onClick={() => dispatch({ type: ActionType.Close })}>
                        Отменить
                    </Button>
                    <Button type="submit">Сохранить</Button>
                </PopupFooter>
            </Form>
        </Popup>
    );
};
