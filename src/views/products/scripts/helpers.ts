import * as Yup from 'yup';

import { CategoryTreeItem, Property } from '@api/catalog';

import { ErrorMessages } from '@scripts/constants';
import { prepareDateTime } from '@scripts/helpers';

import { AttributePropertyEnum, AttributeTypeEnum } from './types';

export const dataToAttrProps = (data: Property) => {
    const attrsList: AttributePropertyEnum[] = [];

    const {
        is_multiple: isMultiple,
        is_color: isColor,
        is_filterable: isFilterable,
        is_common: isCommon,
        is_public: isPublic,
        is_system: isSystem,
        has_directory: hasDirectory,
    } = data;

    const propsToAttrsMap = new Map([
        [AttributePropertyEnum.FEW_VALUES, isMultiple],
        [AttributePropertyEnum.COLOR, isColor],
        [AttributePropertyEnum.FILTER, isFilterable],
        [AttributePropertyEnum.COMMON, isCommon],
        [AttributePropertyEnum.PUBLIC, isPublic],
        [AttributePropertyEnum.SYSTEM, isSystem],
        [AttributePropertyEnum.DIRECTORY, hasDirectory],
    ]);

    propsToAttrsMap.forEach((attrActive: boolean | undefined, attr: AttributePropertyEnum) => {
        if (attrActive) {
            attrsList.push(attr);
        }
    });

    return attrsList;
};

export const getValidationProperties = (type?: string) =>
    Yup.object().shape({
        productNameForPublic: Yup.string().required(ErrorMessages.REQUIRED),
        productNameForAdmin: Yup.string().required(ErrorMessages.REQUIRED),
        attrType: Yup.string().required(ErrorMessages.REQUIRED),
        additionalAttributes: Yup.array().when('attrProps', {
            is: (attrProps: string[]) => {
                if (!Array.isArray(attrProps)) return false;
                return attrProps.includes(AttributePropertyEnum.DIRECTORY);
            },
            then: schema =>
                schema
                    .of(
                        Yup.object().shape({
                            ...(type !== AttributeTypeEnum.IMAGE &&
                                type !== AttributeTypeEnum.FILE &&
                                type !== AttributeTypeEnum.DATETIME && {
                                    name: Yup.string().nullable(),
                                    value: Yup.string().required(ErrorMessages.REQUIRED),
                                }),
                            ...(type === AttributeTypeEnum.IMAGE && {
                                image: Yup.array()
                                    .transform(e => {
                                        if (e === '' || e === undefined || e === null) return [];
                                        return e;
                                    })
                                    .min(1, ErrorMessages.REQUIRED)
                                    .required(ErrorMessages.REQUIRED),
                            }),
                            ...(type === AttributeTypeEnum.FILE && {
                                file: Yup.array()
                                    .transform(e => {
                                        if (e === '' || e === undefined || e === null) return [];
                                        return e;
                                    })
                                    .min(1, ErrorMessages.REQUIRED)
                                    .required(ErrorMessages.REQUIRED),
                            }),
                            ...(type === AttributeTypeEnum.DATETIME && {
                                date: Yup.string()
                                    .transform(value => {
                                        const [day, month, year] = value.split('.').map(Number);
                                        const dateValue = new Date(`${month}/${day}/${year}`);
                                        return Number.isNaN(dateValue.getTime()) ? null : value;
                                    })
                                    .required(ErrorMessages.REQUIRED),
                                time: Yup.string().nullable(),
                            }),
                            ...(type === AttributeTypeEnum.COLOR && {
                                value: Yup.string().required(ErrorMessages.REQUIRED),
                                code: Yup.string().required(ErrorMessages.REQUIRED),
                            }),
                        })
                    )
                    .min(2, 'Введите минимум 2 атрибута')
                    .required(ErrorMessages.REQUIRED),
            otherwise: schema => schema,
        }),
    });

export const customFlat = (category: any, nesting?: string) => {
    if (category)
        return category.reduce((acc: CategoryTreeItem[], cur: CategoryTreeItem) => {
            const curNesting = nesting || '';
            if (cur.children.length > 0)
                return [
                    ...acc,
                    { value: cur.id, label: `${curNesting} ${cur.name}`, code: cur.code },
                    ...customFlat(cur.children, `${curNesting} -`),
                ];

            return [...acc, { value: cur.id, label: `${curNesting} ${cur.name}`, code: cur.code }];
        }, []);
    return [];
};

// eslint-disable-next-line default-param-last
export const customFlatWithParents = (category: any, name = '', selfId?: number): { value: any; label: string }[] => {
    if (category)
        return category.reduce((acc: any[], cur: any) => {
            if (selfId === Number(cur?.id)) return acc;

            if (cur.children.length > 0)
                return [
                    ...acc,
                    { value: cur?.id, label: `${name?.length ? `${name} \u2192 ` : ''}${cur.name}` },
                    ...customFlatWithParents(
                        cur.children,
                        `${name?.length ? `${name} \u2192 ` : ''}${cur.name}`,
                        selfId
                    ),
                ];

            if (cur.children.length === 0)
                return [...acc, { value: cur.id, label: `${name?.length ? `${name} \u2192 ` : ''}${cur.name}` }];

            return acc;
        }, []);
    return [];
};

export function populateObjectFrom(objectA: Record<string, any>, objectB: Record<string, any>) {
    Object.keys(objectB).forEach(key => {
        if (!(key in objectA)) {
            objectA[key] = null;
        }
    });
}

export function normalizeNulls(obj: any): any {
    if (obj === '' || obj === null || obj === undefined) return null;

    if (Array.isArray(obj)) {
        const filteredArray = obj.map(element => normalizeNulls(element)).filter(e => e !== null);
        return filteredArray.length > 0 ? filteredArray : null;
    }

    if (typeof obj === 'object') {
        if (obj instanceof File) return obj;

        const normalizedObj: Record<string, any> = {};

        // eslint-disable-next-line no-restricted-syntax
        for (const key in obj) {
            if (Object.hasOwn(obj, key)) {
                normalizedObj[key] = normalizeNulls(obj[key]);
            }
        }

        if (
            'date' in normalizedObj &&
            'time' in normalizedObj &&
            normalizedObj.date === null &&
            normalizedObj.time === null
        )
            return null;
        if (
            'name' in normalizedObj &&
            'value' in normalizedObj &&
            normalizedObj.name === null &&
            normalizedObj.value === null
        )
            return null;

        return normalizedObj;
    }

    return obj;
}

export const mapAttributesToProperties = (
    entries: {
        attribute: Property;
        value: any;
    }[]
) =>
    entries
        .flatMap(({ attribute, value }) => {
            const { has_directory, is_multiple, type, id } = attribute;

            if (value === null) {
                return {
                    property_id: id,
                    value: null,
                };
            }

            if (type === AttributeTypeEnum.IMAGE || type === AttributeTypeEnum.FILE) {
                const isSetSingular = !is_multiple && !has_directory && value;
                const isSetMultiple = is_multiple && !has_directory && Array.isArray(value) && value.length;

                if (isSetSingular || isSetMultiple) return null;
            }

            if (Array.isArray(value)) {
                type Entry = {
                    property_id: number;
                    directory_value_id?: number;
                    value?: string | number | null;
                    name?: string;
                };

                const result = value.map(element => {
                    const common: Entry = {
                        property_id: id,
                        ...(is_multiple && {
                            ...(has_directory && { directory_value_id: element }),
                            ...(!has_directory && { value: element }),
                        }),
                    };

                    if (!has_directory && type === AttributeTypeEnum.COLOR) {
                        return {
                            ...common,
                            value: element?.value,
                            name: element?.name || '',
                        };
                    }

                    if (!has_directory && type === AttributeTypeEnum.DATETIME) {
                        return {
                            ...common,
                            name: element?.date && element?.time && prepareDateTime(element.date, element?.time),
                            value: element?.date && element?.time && prepareDateTime(element.date, element?.time),
                        };
                    }

                    if (
                        !has_directory &&
                        is_multiple &&
                        (element === undefined || element === null || element === '')
                    ) {
                        return {
                            ...common,
                            value: null,
                        };
                    }

                    return common;
                }) as Entry[];

                return result;
            }

            return [
                {
                    property_id: id,
                    ...(has_directory && { directory_value_id: value }),
                    ...(!has_directory && type !== AttributeTypeEnum.COLOR && { value }),
                    ...(!has_directory &&
                        type === AttributeTypeEnum.COLOR && {
                            value: value?.value,
                            name: value?.name || '',
                        }),
                    ...(!has_directory &&
                        type === AttributeTypeEnum.DATETIME && {
                            name: prepareDateTime(value?.date, value?.time),
                            value: prepareDateTime(value?.date, value?.time),
                        }),
                },
            ];
        })
        .filter(Boolean);

export const getBaseFilesArraySchema = (maxFileSize: number, maxSizeMessage: string) =>
    Yup.array()
        .nullable()
        .transform(e => {
            if (e === '' || e === undefined || e === null) return [];
            return e;
        })
        .test('max-file-size', maxSizeMessage, value => {
            if (!value) return true;
            if (Array.isArray(value)) {
                return value.every(file => file?.size <= maxFileSize);
            }
            return false;
        });
