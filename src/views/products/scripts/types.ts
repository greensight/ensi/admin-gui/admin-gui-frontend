export enum AttributeTypeEnum {
    STRING = 'string',
    TEXT = 'text',
    INTEGER = 'integer',
    DOUBLE = 'double',
    BOOLEAN = 'boolean',
    DATETIME = 'datetime',
    IMAGE = 'image',
    FILE = 'file',
    COLOR = 'color',
    DIRECTORY = 'directory',
}

export enum AttributePropertyEnum {
    COLOR = 'color',
    FILTER = 'filter',
    FEW_VALUES = 'fewValues',
    PUBLIC = 'public',
    REQUIRED = 'required',
    DIRECTORY = 'directory',
    COMMON = 'common',
    SYSTEM = 'system',
}
