import {
    FormFieldWrapper,
    Popup,
    PopupContent,
    PopupFooter,
    PopupHeader,
    Select,
    SelectItem,
    SimpleSelect,
    useFormContext,
} from '@ensi-platform/core-components';
import { Fragment, useEffect, useMemo, useState } from 'react';

import { useProductEventOperations, useProductEvents } from '@api/catalog/product-statuses';

import Legend from '@controls/Legend';

import { Button, Layout, colors, scale, typography } from '@scripts/gds';
import { toSelectItems } from '@scripts/helpers';

import TrashIcon from '@icons/small/trash.svg';

const SingleEvent = ({ name, onRemove }: { name: string; onRemove: () => void }) => (
    <div
        css={{
            border: `1px solid ${colors.grey600}`,
            padding: scale(2),
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
        }}
    >
        {name}
        <Button onClick={onRemove} type="button" Icon={TrashIcon} hidden>
            удалить
        </Button>
    </div>
);

const AddPopup = ({
    isOpen,
    onClose,
    onAppend,
    newOptions,
}: {
    isOpen: boolean;
    onClose: () => void;
    onAppend: (eventId: number) => void;
    newOptions: SelectItem[];
}) => {
    const [selectedOption, setSelectedOption] = useState<SelectItem>();

    useEffect(() => {
        if (!isOpen) {
            setSelectedOption(undefined);
        }

        return () => setSelectedOption(undefined);
    }, [isOpen]);

    return (
        <Popup open={isOpen} onClose={onClose} size="minMd">
            <PopupHeader title="Добавление события" />
            <PopupContent>
                <SimpleSelect
                    zIndexPopover={101}
                    options={newOptions}
                    selected={selectedOption}
                    onChange={(_, payload) => {
                        if (payload?.actionItem) {
                            setSelectedOption(payload.actionItem);
                        }
                    }}
                />
            </PopupContent>
            <PopupFooter>
                <Button onClick={onClose} theme="outline">
                    Отменить
                </Button>
                <Button
                    disabled={!selectedOption}
                    onClick={() => {
                        onAppend(Number(selectedOption?.value!));
                    }}
                >
                    Добавить
                </Button>
            </PopupFooter>
        </Popup>
    );
};

const EventsPicker = ({ disabled }: { disabled: boolean }) => {
    const { watch, setValue } = useFormContext<{
        events: {
            events: number[];
            operation: number;
        };
    }>();

    const currentEventEvents = watch('events.events');
    const currentEventIds = useMemo(
        () => (Array.isArray(currentEventEvents) ? currentEventEvents : [currentEventEvents]),
        [currentEventEvents]
    );

    const { data: apiEvents } = useProductEvents();
    const eventOptions = useMemo(() => toSelectItems(apiEvents?.data) || [], [apiEvents?.data]);

    const data = useMemo(() => {
        const currentEvents = [] as SelectItem[];
        const newEvents = [] as SelectItem[];

        eventOptions.forEach(option => {
            if (currentEventIds.includes(option.value)) {
                currentEvents.push(option);
            } else {
                newEvents.push(option);
            }
        });

        return { currentEvents, newEvents };
    }, [currentEventIds, eventOptions]);

    const currentOperationId = watch('events.operation');

    const { data: apiOperations } = useProductEventOperations();
    const operationOptions = useMemo(() => {
        const items: SelectItem[] = [];

        if (apiOperations?.data) {
            items.push({
                label: 'Одно событие',
                value: 0,
            });
            items.push(...toSelectItems(apiOperations.data));
        }

        return items;
    }, [apiOperations?.data]);

    const currentOperation = useMemo(
        () => operationOptions.find(e => e.value === currentOperationId),
        [currentOperationId, operationOptions]
    );

    const [isOpen, setOpen] = useState(false);

    return (
        <div>
            <Legend label="События" />
            <Layout cols={[`${scale(28)}px`, 1]}>
                <Layout.Item>
                    <FormFieldWrapper name="events.operation" label="Логическая функция" disabled={disabled}>
                        <Select
                            options={operationOptions}
                            hideClearButton
                            onChange={() => {
                                setValue('events.events', []);
                            }}
                        />
                    </FormFieldWrapper>
                </Layout.Item>
                <Layout.Item>
                    {currentOperationId !== 0 ? (
                        <>
                            <Legend label="Когда..." />
                            {data.currentEvents.map((item, index) => (
                                <Fragment key={item.value?.toString() || index}>
                                    <SingleEvent
                                        name={item.label}
                                        onRemove={() => {
                                            setValue(
                                                'events.events',
                                                currentEventIds.filter(e => e !== item.value)
                                            );
                                        }}
                                    />
                                    {index !== data.currentEvents.length - 1 && (
                                        <p
                                            css={{
                                                color: colors.black,
                                                ...typography('buttonBold'),
                                                marginTop: scale(1),
                                                marginBottom: scale(1),
                                                marginLeft: scale(1),
                                            }}
                                        >
                                            {currentOperation?.label}
                                        </p>
                                    )}
                                </Fragment>
                            ))}
                            <Button
                                onClick={() => setOpen(true)}
                                css={{
                                    marginTop: data.currentEvents.length ? scale(2) : 0,
                                }}
                                disabled={disabled}
                            >
                                Добавить
                            </Button>
                        </>
                    ) : (
                        <FormFieldWrapper name="events.events" label="Когда...">
                            <Select options={eventOptions} disabled={disabled} hideClearButton />
                        </FormFieldWrapper>
                    )}
                </Layout.Item>
            </Layout>
            <AddPopup
                isOpen={isOpen}
                onClose={() => {
                    setOpen(false);
                }}
                newOptions={data.newEvents}
                onAppend={id => {
                    setOpen(false);
                    setValue('events.events', [...currentEventIds, id], {
                        shouldDirty: true,
                        shouldTouch: true,
                    });
                }}
            />
        </div>
    );
};

export default EventsPicker;
