import {
    CopyButton,
    DescriptionList,
    DescriptionListItem,
    FormCheckbox,
    FormField,
    FormFieldWrapper,
    Input,
    Select,
    SelectWithTags,
    getDefaultDates,
    useActionPopup,
    useFormContext,
} from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { useMemo, useRef } from 'react';
import * as Yup from 'yup';

import {
    useCreateProductStatus,
    useDeleteProductStatus,
    usePatchProductStatus,
    useProductStatus,
    useProductStatusType,
    useSearchProductStatuses,
    useSetPreviousProductStatuses,
} from '@api/catalog/product-statuses';
import {
    CreateStatusSettingRequest,
    PatchStatusSettingRequest,
    ProductStatusTypeEnum,
} from '@api/catalog/types/product-statuses';

import { useError, useSuccess } from '@context/modal';

import LoadingSkeleton from '@controls/LoadingSkeleton';
import Mask from '@controls/Mask';
import Switcher from '@controls/Switcher';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { CREATE_PARAM, ErrorMessages, ModalMessages } from '@scripts/constants';
import { RedirectMethods } from '@scripts/enums';
import { Layout, colors, scale } from '@scripts/gds';
import { isPageNotFound, toSelectItems } from '@scripts/helpers';
import { useDeferredLoading } from '@scripts/hooks/useDeferredLoading';

import { useStatusesAccess } from '../useStatusesAccess';
import EventsPicker from './components/EventsPicker';

const LISTING_URL = '/products/statuses';

const StatusInner = ({ isLoading, id, canEditForm }: { isLoading: boolean; id: number; canEditForm: boolean }) => {
    const { watch } = useFormContext();
    const statusType = watch('type');

    const { data: apiTypes } = useProductStatusType();
    const types = useMemo(() => toSelectItems(apiTypes?.data), [apiTypes?.data]);

    const { data: apiStatuses } = useSearchProductStatuses({
        pagination: {
            limit: -1,
            offset: 0,
            type: 'offset',
        },
    });

    const statuses = useMemo(
        () =>
            apiStatuses?.data
                .filter(e => e.id !== id)
                .map(status => ({
                    label: status.name,
                    value: status.id,
                })) || [],
        [apiStatuses?.data, id]
    );

    const deferredIsLoading = useDeferredLoading(isLoading, 150);

    if (deferredIsLoading) {
        return (
            <LoadingSkeleton
                count={5}
                css={{
                    marginBottom: scale(2),
                }}
                height={scale(6)}
            />
        );
    }

    return (
        <Layout cols={8} gap={scale(2)} css={{ marginBottom: scale(2) }}>
            <Layout.Item col={8}>
                <FormField name="name" label="Название*" />
            </Layout.Item>
            <Layout.Item col={8}>
                <FormFieldWrapper name="code" label="Код">
                    <Mask mask={/^[a-z\d/.-_\\-]+$/i} />
                </FormFieldWrapper>
            </Layout.Item>
            <Layout.Item col={8}>
                <FormFieldWrapper name="color" label="Цвет*">
                    <Input type="color" />
                </FormFieldWrapper>
            </Layout.Item>
            <Layout.Item col={8}>
                <FormFieldWrapper name="type" label="Тип*">
                    <Select options={types} hideClearButton />
                </FormFieldWrapper>
            </Layout.Item>
            <Layout.Item col={8}>
                <FormFieldWrapper name="is_publication">
                    <FormCheckbox>Статус публикации</FormCheckbox>
                </FormFieldWrapper>
            </Layout.Item>
            <Layout.Item col={8}>
                <FormFieldWrapper name="from_statuses" label="Переходы">
                    <SelectWithTags
                        options={statuses}
                        hideClearButton
                        {...(statuses.length === 0 && { disabled: true })}
                    />
                </FormFieldWrapper>
            </Layout.Item>
            {statusType === ProductStatusTypeEnum.AUTO && (
                <Layout.Item col={8}>
                    <EventsPicker disabled={!canEditForm} />
                </Layout.Item>
            )}
        </Layout>
    );
};

const ContentStatus = () => {
    const { access, isIDForbidden } = useStatusesAccess();

    const {
        query: { entity_id },
        push,
    } = useRouter();

    const parsedId = +`${entity_id}`;
    const isCreation = entity_id === CREATE_PARAM;

    const { data: apiData, error, isFetching: isLoading } = useProductStatus({ id: parsedId });
    useError(error);

    const status = apiData?.data;

    const setPreviousStatuses = useSetPreviousProductStatuses();
    const updateStatus = usePatchProductStatus();
    const deleteStatus = useDeleteProductStatus();
    const createStatus = useCreateProductStatus();

    const { popupState, popupDispatch, ActionPopup, ActionEnum, ActionType } = useActionPopup();

    useSuccess(updateStatus.isSuccess && ModalMessages.SUCCESS_UPDATE);
    useSuccess(deleteStatus.isSuccess && ModalMessages.SUCCESS_DELETE);
    useSuccess(createStatus.isSuccess && ModalMessages.SUCCESS_CREATE);

    useError(setPreviousStatuses.error);
    useError(updateStatus.error);
    useError(deleteStatus.error);
    useError(createStatus.error);

    const initialValues = useMemo(
        () => ({
            name: status?.name || '',
            code: status?.code || '',
            color: status?.color || colors.grey500,
            type: status?.type || '',
            is_active: status?.is_active || false,
            is_publication: status?.is_publication || false,
            from_statuses: status?.previous_statuses?.map(e => e.id),
            events: {
                events: status?.events?.events || [],
                operation: (status?.events?.operation || 0) as number | null,
            },
        }),
        [status]
    );

    const pageTitle = isCreation ? 'Создать статус' : `Редактировать статус ${parsedId}`;
    const shouldReturnBack = useRef(false);

    const canEditForm = isCreation ? access.ID.create : access.ID.edit;

    const isNotFound = isPageNotFound({ id: parsedId, error, isCreation });

    return (
        <PageWrapper
            title={pageTitle}
            isLoading={isLoading || updateStatus.isPending || createStatus.isPending || deleteStatus.isPending}
            isNotFound={isNotFound}
            isForbidden={isIDForbidden}
        >
            <FormWrapper
                disabled={!canEditForm}
                initialValues={initialValues}
                validationSchema={Yup.object().shape({
                    name: Yup.string().required(ErrorMessages.REQUIRED),
                    code: Yup.string(),
                    backgroundColor: Yup.string().min(1, ErrorMessages.REQUIRED),
                    color: Yup.string().min(1, ErrorMessages.REQUIRED),
                    type: Yup.number()
                        .transform(val => (Number.isNaN(val) ? undefined : val))
                        .required(ErrorMessages.REQUIRED),
                    from_statuses: Yup.array().of(
                        Yup.number()
                            .transform(val => (Number.isNaN(val) ? undefined : val))
                            .required(ErrorMessages.REQUIRED)
                    ),
                    events: Yup.object().when('type', {
                        is: ProductStatusTypeEnum.AUTO,
                        then: schema =>
                            schema.shape({
                                operation: Yup.number()
                                    .transform(val => (Number.isNaN(val) ? undefined : val))
                                    .required(ErrorMessages.REQUIRED),
                                events: Yup.array()
                                    .of(
                                        Yup.number()
                                            .transform(val => (Number.isNaN(val) ? undefined : val))
                                            .required(ErrorMessages.REQUIRED)
                                    )
                                    .min(1, ErrorMessages.REQUIRED),
                            }),
                        otherwise: schema => schema.nullable(),
                    }),
                })}
                enableReinitialize
                onSubmit={async (allValues, { reset }) => {
                    const { from_statuses = [], ...values } = allValues as CreateStatusSettingRequest & {
                        from_statuses: number[];
                    };

                    if (values.type !== ProductStatusTypeEnum.AUTO) {
                        delete values.events;
                    } else {
                        values.events!.operation = +values.events!.operation! || null;
                    }

                    if (isCreation) {
                        const result = await createStatus.mutateAsync(values as unknown as CreateStatusSettingRequest);

                        if (from_statuses.length) {
                            await setPreviousStatuses.mutateAsync({
                                id: result.data.id,
                                ids: from_statuses as number[],
                            });
                        }

                        if (shouldReturnBack.current) {
                            return {
                                method: RedirectMethods.push,
                                redirectPath: LISTING_URL,
                            };
                        }

                        return {
                            method: RedirectMethods.replace,
                            redirectPath: `${LISTING_URL}/${result.data.id}`,
                        };
                    }

                    await updateStatus.mutateAsync({
                        ...(values as unknown as PatchStatusSettingRequest),
                        id: parsedId,
                    });

                    if (from_statuses.length) {
                        await setPreviousStatuses.mutateAsync({
                            id: parsedId,
                            ids: from_statuses as number[],
                        });
                    }

                    if (shouldReturnBack.current) {
                        reset(values);
                        return {
                            method: RedirectMethods.push,
                            redirectPath: LISTING_URL,
                        };
                    }
                    return {
                        method: RedirectMethods.replace,
                        redirectPath: `${LISTING_URL}/${parsedId}`,
                    };
                }}
            >
                {({ reset }) => (
                    <PageTemplate
                        h1={pageTitle}
                        controls={
                            <PageControls access={access}>
                                <PageControls.Delete
                                    onClick={() => {
                                        popupDispatch({
                                            type: ActionType.Delete,
                                            payload: {
                                                title: `Вы уверены, что хотите удалить статус?`,
                                                popupAction: ActionEnum.DELETE,
                                                onAction: async () => {
                                                    reset();
                                                    await deleteStatus.mutateAsync({
                                                        id: parsedId,
                                                    });

                                                    push(LISTING_URL);
                                                },
                                            },
                                        });
                                    }}
                                />
                                <PageControls.Close onClick={() => push(LISTING_URL)} />
                                <PageControls.Apply
                                    onClick={() => {
                                        shouldReturnBack.current = false;
                                    }}
                                />
                                <PageControls.Save
                                    onClick={() => {
                                        shouldReturnBack.current = true;
                                    }}
                                />
                            </PageControls>
                        }
                        backlink={{ text: 'К списку статусов', href: LISTING_URL }}
                        aside={
                            <>
                                <FormFieldWrapper name="is_active" label="Активность" disabled={!canEditForm}>
                                    <Switcher>Активность</Switcher>
                                </FormFieldWrapper>

                                <div css={{ marginBottom: scale(1), marginTop: scale(3) }}>
                                    <span css={{ marginRight: scale(1, true), color: colors?.grey800 }}>ID:</span>
                                    <CopyButton>{`${entity_id}`}</CopyButton>
                                </div>

                                <DescriptionList css={{ marginBottom: scale(5) }}>
                                    {getDefaultDates({ ...status }).map(item => (
                                        <DescriptionListItem {...item} key={item.name} />
                                    ))}
                                </DescriptionList>
                            </>
                        }
                    >
                        <StatusInner id={parsedId} isLoading={isLoading} canEditForm={canEditForm} />
                    </PageTemplate>
                )}
            </FormWrapper>

            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </PageWrapper>
    );
};

export default ContentStatus;
