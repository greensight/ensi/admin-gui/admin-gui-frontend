import { useAccess } from '@scripts/hooks';
import { useIDForbidden } from '@scripts/hooks/useIDForbidden';

export const accessMatrix = {
    LIST: {
        view: 2301,
    },
    ID: {
        view: 2302,
        create: 2303,
        edit: 2304,
        delete: 2305,
    },
};

export const useStatusesAccess = () => {
    const access = useAccess(accessMatrix);
    const isIDForbidden = useIDForbidden(access);

    return { access, isIDForbidden };
};
