import { useMemo } from 'react';

import { useProductStatusesMeta, useSearchProductStatuses } from '@api/catalog/product-statuses';
import { ProductStatusSettings } from '@api/catalog/types/product-statuses';

import ListBuilder from '@components/ListBuilder';
import { TooltipItem } from '@components/Table';

import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import { useStatusesAccess } from './useStatusesAccess';

const ProductStatuses = () => {
    const { access } = useStatusesAccess();

    const canViewDetailPage = access.ID.view;

    const goToDetailPage = useGoToDetailPage({ extraConditions: canViewDetailPage });

    const tooltipContent = useMemo<TooltipItem[]>(
        () => [
            {
                type: 'edit',
                text: 'Редактировать статус',
                action: goToDetailPage,
                isDisable: !canViewDetailPage,
            },
        ],
        [canViewDetailPage, goToDetailPage]
    );

    return (
        <ListBuilder<ProductStatusSettings>
            access={access}
            searchHook={useSearchProductStatuses}
            metaHook={useProductStatusesMeta}
            tooltipItems={tooltipContent}
            title="Статусная модель"
        />
    );
};

export default ProductStatuses;
