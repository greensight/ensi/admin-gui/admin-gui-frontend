/* eslint-disable react/no-unstable-nested-components */
import { Checkbox, Popup, PopupContent, PopupHeader } from '@ensi-platform/core-components';
import { createColumnHelper } from '@tanstack/react-table';
import { isEmpty } from 'lodash';
import { useRouter } from 'next/router';
import { ChangeEvent, useCallback, useEffect, useMemo, useState } from 'react';
import type { FieldValues } from 'react-hook-form';

import { Property, useCategoryBindProperties, useCategoryDetail, useProperties, usePropertiesMeta } from '@api/catalog';
import { StorePickupTime } from '@api/units';

import { useError, useSuccess } from '@context/modal';

import LoadWrapper from '@controls/LoadWrapper';
import Tooltip from '@controls/Tooltip';

import AutoFilters from '@components/AutoFilters';
import Table, { useSorting, useTable } from '@components/Table';
import { getSettingsColumn } from '@components/Table/columns';
import { TableEmpty, TableFooter, TableHeader } from '@components/Table/components';

import { CREATE_PARAM } from '@scripts/constants';
import { Button, scale, typography } from '@scripts/gds';
import { declOfNum, getTotal, getTotalPages, isEmptyArray } from '@scripts/helpers';
import { useEditableTableRows, useRedirectToNotEmptyPage, useTableList } from '@scripts/hooks';
import { useAutoColumns, useAutoFilters, useAutoTableData } from '@scripts/hooks/autoTable';

import PlusIcon from '@icons/plus.svg';
import TipIcon from '@icons/small/status/tip.svg';

interface RowData {
    id: number;
    isSelected: boolean;
    isRequired: boolean;
    isGluing: boolean;
}

const getInitialRowData = (id: number): RowData => ({ id, isGluing: false, isRequired: false, isSelected: false });

const AddAttributePopup = ({ isOpen = false, onRequestClose }: { isOpen: boolean; onRequestClose: () => void }) => {
    const { push, pathname } = useRouter();

    const { extraRowsData, setExtraRowsData, onRowChange } = useEditableTableRows(getInitialRowData, (old, payload) => {
        switch (payload.column) {
            case 'isSelected': {
                if (!payload.value) {
                    old.isRequired = false;
                    old.isGluing = false;
                }

                old.isSelected = payload.value;

                break;
            }
            case 'isRequired': {
                old.isRequired = payload.value;
                break;
            }
            case 'isGluing': {
                old.isGluing = payload.value;
                break;
            }
            default:
                break;
        }

        return old;
    });

    useEffect(() => {
        if (!isOpen) setExtraRowsData({});
    }, [isOpen, setExtraRowsData]);

    const { query } = useRouter();

    const id = Array.isArray(query.id) ? query.id[0] : query.id;
    const isCreatePage = id === CREATE_PARAM;

    const { data: meta, isFetching: isMetaLoading, error: metaError } = usePropertiesMeta();

    useError(metaError);

    const metaData = useMemo(() => meta?.data, [meta]);
    const { metaField, emptyInitialValues, reset, codesToSortKeys, clearInitialValue } = useAutoFilters(metaData);

    const pageKey = 'attributes-page';

    const { activePage, itemsPerPageCount, setItemsPerPageCount } = useTableList({
        defaultSort: metaData?.default_sort,
        codesToSortKeys,
        pageKey,
    });

    const [selectedFilter, setSelectedFilter] = useState({});

    useEffect(() => {
        if (isOpen) return;

        setSelectedFilter(old => {
            if (!isEmpty(selectedFilter)) return {};
            return old;
        });
    }, [isOpen, selectedFilter]);

    const [{ backendSorting }, sortingPlugin] = useSorting<StorePickupTime>('add-attribute-popup', metaData?.fields);

    const {
        data: propertiesListData,
        error: propertiesListError,
        isFetching: propertiesListLoading,
    } = useProperties(
        {
            sort: backendSorting,
            filter: selectedFilter,
            pagination: { type: 'offset', limit: itemsPerPageCount, offset: (activePage - 1) * itemsPerPageCount },
        },
        isOpen && !!backendSorting
    );

    const resetFilters = () => {
        reset();
        setSelectedFilter({});

        push({ pathname, query: { pid: query.pid } });
    };

    useError(propertiesListError);

    const { data: category } = useCategoryDetail(Number(id), !!id && !isCreatePage);
    const inheritedIdsToFilter = useMemo(
        () => category?.data?.properties?.filter(item => item.is_inherited)?.map(({ property_id }) => property_id),
        [category?.data?.properties]
    );

    const propertiesTableData = useAutoTableData<Property>(propertiesListData?.data, metaField);

    const tableData = useMemo(
        () =>
            propertiesTableData
                ?.filter(item => !inheritedIdsToFilter?.includes(item?.id))
                ?.map(item => ({
                    ...item,
                    id: item?.id,
                    customListLink: '/products/directories/properties',
                    is_required_selection: false,
                    ...(Number(item?.id) in extraRowsData && extraRowsData[Number(item?.id)]),
                })),
        [propertiesTableData, inheritedIdsToFilter, extraRowsData]
    );

    const selectedTableIndices = useMemo(
        () => Object.keys(extraRowsData).map(e => propertiesTableData.findIndex(j => j.id === Number(e))),
        [extraRowsData, propertiesTableData]
    );
    const prepareSelectedIndicesRows = useCallback(() => {
        if (selectedTableIndices.length > 0) {
            const result: Record<string, any> = {};

            selectedTableIndices.forEach((item: number) => {
                const foundIndex = tableData.findIndex(property => property.id === item);
                result[foundIndex] = true;
            });
            return result;
        }
    }, [selectedTableIndices, tableData]);

    const autoGeneratedColumns = useAutoColumns(metaData);

    const columnHelper = createColumnHelper<Record<string, any>>();
    const columns = useMemo(
        () => [
            columnHelper.accessor('select', {
                header: 'Выделение',
                enableSorting: false,
                cell: ({ row }) => (
                    <Checkbox
                        value="true"
                        name={`select-${row?.original?.id}-AddAttributePopup`}
                        id={`select-${row?.original?.id}-AddAttributePopup`}
                        checked={row.original.isSelected}
                        onChange={(e: ChangeEvent<HTMLInputElement>) =>
                            onRowChange({ id: row.original.id, value: e.target.checked, column: 'isSelected' })
                        }
                    />
                ),
            }),
            columnHelper.accessor('is_required_selection', {
                header: () => (
                    <p>
                        Обязательность{' '}
                        <Tooltip
                            content="После установки обязательности атрибута - необходимо заполнить значение в карточке товара"
                            arrow
                            maxWidth={scale(30)}
                            theme="light"
                            css={{ padding: `0px ${scale(2)}px`, ...typography('bodySm') }}
                        >
                            <button type="button" css={{ verticalAlign: 'middle' }}>
                                <TipIcon />
                            </button>
                        </Tooltip>
                    </p>
                ),
                enableSorting: false,
                cell: ({ row }) => (
                    <Checkbox
                        value="true"
                        name={`is_required_selection-${row?.original?.id}`}
                        id={`is_required_selection-${row?.original?.id}`}
                        disabled={!row?.original?.isSelected}
                        checked={row?.original?.isRequired || false}
                        onChange={(e: ChangeEvent<HTMLInputElement>) =>
                            onRowChange({ id: row.original.id, value: e.target.checked, column: 'isRequired' })
                        }
                    />
                ),
            }),
            columnHelper.accessor('is_gluing', {
                header: 'Параметр склеивания',
                enableSorting: false,
                cell: ({ row }) => (
                    <Checkbox
                        value="true"
                        name={`is_gluing-${row?.original?.id}-AddAttributePopup`}
                        id={`is_gluing-${row?.original?.id}-AddAttributePopup`}
                        disabled={
                            !row?.original?.isSelected || row.original.type === 'Файл' || row.original.type === 'Текст'
                        }
                        checked={row?.original?.isGluing || false}
                        onChange={(e: ChangeEvent<HTMLInputElement>) =>
                            onRowChange({ id: row.original.id, value: e.target.checked, column: 'isGluing' })
                        }
                    />
                ),
            }),
            ...autoGeneratedColumns,
            getSettingsColumn({
                columnsToDisable: ['id', 'is_required_selection'],
                visibleColumns: [...(metaData?.default_list || []), 'is_required_selection', 'is_gluing'],
            }),
        ],
        [autoGeneratedColumns, metaData?.default_list, onRowChange]
    );

    const total = getTotal(propertiesListData);
    const totalPages = getTotalPages(propertiesListData, itemsPerPageCount);

    const bindProperties = useCategoryBindProperties();

    const submitBindingProperties = useCallback(
        async (selectedRows: RowData[]) => {
            await bindProperties.mutateAsync({
                id: Number(query?.pid as string),
                replace: false,
                properties: selectedRows.map(row => ({
                    id: row.id,
                    is_required: row.isRequired,
                    is_gluing: row.isGluing,
                })),
            });

            onRequestClose();
        },
        [bindProperties, onRequestClose, query?.pid]
    );

    const selectedRows = useMemo(() => Object.values(extraRowsData).filter(e => e.isSelected), [extraRowsData]);

    useError(bindProperties?.error);
    useSuccess(bindProperties?.isSuccess ? 'Атрибуты успешно добавлены' : '');

    useRedirectToNotEmptyPage({ activePage, itemsPerPageCount, total, shallow: true, pageKey });

    const renderHeader = useCallback(
        () => (
            <TableHeader>
                <div css={{ display: 'flex', justifyContent: 'space-between', width: '100%', padding: 0 }}>
                    <p css={{ lineHeight: '32px', ...typography('bodySm') }}>
                        Найдено {`${total} ${declOfNum(total, ['атрибут', 'атрибута', 'атрибутов'])}`}
                    </p>
                    {selectedRows.length > 0 ? (
                        <Button
                            Icon={PlusIcon}
                            theme="primary"
                            type="submit"
                            onClick={async () => submitBindingProperties(selectedRows)}
                        >
                            Добавить {selectedRows.length}{' '}
                            {declOfNum(selectedRows.length, ['атрибут', 'атрибута', 'атрибутов'])}
                        </Button>
                    ) : null}
                </div>
            </TableHeader>
        ),
        [submitBindingProperties, total, selectedRows]
    );

    const checkValueIsDate = useCallback(
        (key: string) => {
            const code = key.replace(/(_from)|(_to)/, '');

            const type = metaData?.fields?.find(({ code: fieldCode }) => fieldCode === code)?.type;

            return type && ['datetime', 'date'].includes(type);
        },
        [metaData?.fields]
    );

    const submitAddAttributeFilter = useCallback(
        (vals: FieldValues) => {
            if (typeof vals === 'object' && !Array.isArray(vals) && vals !== null) {
                /** add unique form values */
                const filterValues = Object.keys(vals).reduce((acc, cur) => {
                    if (vals[cur] && !isEmptyArray(vals[cur]))
                        return { ...acc, [cur]: checkValueIsDate(cur) ? new Date(vals[cur]).toISOString() : vals[cur] };
                    return acc;
                }, {});

                if (Object.keys(filterValues)?.length) setSelectedFilter(filterValues);
            }
        },
        [checkValueIsDate]
    );
    const table = useTable(
        {
            data: tableData,
            columns,
            meta: {
                tableKey: pageKey,
            },
            ...(selectedTableIndices.length > 0 && {
                initialState: {
                    rowSelection: prepareSelectedIndicesRows(),
                },
            }),
        },
        [sortingPlugin]
    );

    return (
        <Popup open={isOpen} onClose={onRequestClose} innerScroll size="screen_lg">
            <PopupHeader title="Выберите атрибут для добавления" />
            <PopupContent>
                <LoadWrapper isLoading={propertiesListLoading || isMetaLoading || bindProperties.isPending}>
                    <AutoFilters
                        initialValues={emptyInitialValues}
                        emptyInitialValues={emptyInitialValues}
                        onSubmit={submitAddAttributeFilter}
                        filtersActive={!isEmpty(selectedFilter)}
                        css={{ marginBottom: scale(2) }}
                        meta={metaData}
                        onResetFilters={resetFilters}
                        clearInitialValue={clearInitialValue}
                    />

                    {renderHeader()}
                    <Table instance={table} />

                    {tableData?.length === 0 ? (
                        <TableEmpty
                            onResetFilters={() => setSelectedFilter({})}
                            filtersActive={!isEmpty(selectedFilter)}
                            titleWithFilters="Атрибуты не найдены"
                            titleWithoutFilters="Атрибутов нет"
                            addItems={() => setItemsPerPageCount(10)}
                        />
                    ) : (
                        <TableFooter
                            pages={totalPages}
                            itemsPerPageCount={itemsPerPageCount}
                            setItemsPerPageCount={setItemsPerPageCount}
                            pageKey={pageKey}
                        />
                    )}
                </LoadWrapper>
            </PopupContent>
        </Popup>
    );
};

export default AddAttributePopup;
