import {
    Block,
    DescriptionList,
    DescriptionListItem,
    DescriptionListItemType,
    FormCheckbox,
    FormField,
    FormFieldWrapper,
    Input,
    Select,
    Tabs,
    getDefaultDates,
    useActionPopup,
} from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { useEffect, useMemo, useRef, useState } from 'react';
import * as Yup from 'yup';

import {
    useCategoriesTree,
    useCategoryCreate,
    useCategoryDelete,
    useCategoryDetail,
    useCategoryUpdate,
} from '@api/catalog';

import { useError, useSuccess } from '@context/modal';

import { customFlatWithParents } from '@views/products/scripts';

import Switcher from '@controls/Switcher';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { CREATE_PARAM, ErrorMessages, ModalMessages } from '@scripts/constants';
import { RedirectMethods } from '@scripts/enums';
import { Button, Layout, scale } from '@scripts/gds';
import { isPageNotFound } from '@scripts/helpers';
import { useMedia } from '@scripts/hooks';
import { onlyEnglishLettersDigits } from '@scripts/regex';

import PlusIcon from '@icons/small/plus.svg';

import AddAttributePopup from '../components/AddAttributePopup';
import InheritedPropertiesTable from '../components/InheritedPropertiesTable';
import SelfPropertiesTable from '../components/SelfPropertiesTable';
import useCategoriesAccess from '../useCategoriesAccess';

const ProductCategory = () => {
    const { query, push, pathname } = useRouter();
    const { sm, xs } = useMedia();
    const id = Array.isArray(query.pid) ? query.pid[0] : query.pid;
    const isCreation = id === CREATE_PARAM;
    const listLink = pathname.split(`[pid]`)[0];

    const shouldReturnBack = useRef(false);
    const [isAddAttributePopupOpen, setAddAttributePopupOpen] = useState(false);

    const { popupState, popupDispatch, ActionPopup, ActionEnum, ActionType } = useActionPopup();

    const { access, isIDForbidden } = useCategoriesAccess();

    const {
        ID: {
            view: canViewDetailPage,
            edit: canEditAllCategoryData,
            editProperties: canAddAndEditingProperties,
            editDetail: canEditCategoryDataWithoutActive,
            delete: canDeleteCategory,
            editActivity: canEditCategoryActive,
            create: canCreateCategory,
        },
    } = access;

    useEffect(() => {
        if (!isAddAttributePopupOpen && Object.keys(query)?.filter(i => i !== 'pid')?.length)
            push({
                pathname,
                query: { pid: id },
            });
    }, [id, isAddAttributePopupOpen, pathname, push, query]);

    const {
        data: category,
        isFetching: isDetailLoading,
        error: isDetailError,
    } = useCategoryDetail(id, canViewDetailPage);
    useError(isDetailError);

    const categoryData = useMemo(() => category?.data, [category]);

    const pageTitle = useMemo(
        () => (isCreation ? 'Создать новую категорию' : `Категория: ${categoryData?.name}`),
        [categoryData?.name, isCreation]
    );

    const { data: categoriesTree, isFetching: categoriesTreeLoading, error: errorCategoriesTree } = useCategoriesTree();
    useError(errorCategoriesTree);

    const flattedCategoriesTree = customFlatWithParents(categoriesTree?.data, '', Number(id));

    const parentCategories = useMemo(
        () => [{ value: null, label: 'Нет' }, ...flattedCategoriesTree],
        [flattedCategoriesTree]
    );

    const createCategory = useCategoryCreate();
    useError(createCategory.error);
    useSuccess(createCategory.isSuccess ? ModalMessages.SUCCESS_SAVE : '');

    const updateCategory = useCategoryUpdate();
    useError(updateCategory.error);
    useSuccess(updateCategory.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const deleteCategory = useCategoryDelete();
    useError(deleteCategory.error);
    useSuccess(deleteCategory.isSuccess ? ModalMessages.SUCCESS_DELETE : '');

    const canEditForm = isCreation ? canCreateCategory : canEditAllCategoryData || canEditCategoryDataWithoutActive;
    const canEditActivity = isCreation ? canCreateCategory : canEditCategoryActive || canEditAllCategoryData;

    const isNotFound = isPageNotFound({ id, error: isDetailError, isCreation });

    return (
        <PageWrapper
            title={pageTitle}
            isLoading={
                isDetailLoading ||
                categoriesTreeLoading ||
                createCategory?.isPending ||
                updateCategory?.isPending ||
                deleteCategory?.isPending
            }
            isNotFound={isNotFound}
            isForbidden={isIDForbidden}
        >
            <FormWrapper
                onSubmit={async values => {
                    if (isCreation) {
                        const {
                            data: { id: createdId },
                        } = await createCategory.mutateAsync(values);

                        return {
                            method: shouldReturnBack.current ? RedirectMethods.push : RedirectMethods.replace,
                            redirectPath: shouldReturnBack.current ? listLink : `${listLink}${createdId}`,
                        };
                    }

                    await updateCategory?.mutateAsync({ id: Number(id), ...values });

                    return {
                        method: shouldReturnBack.current ? RedirectMethods.push : RedirectMethods.replace,
                        redirectPath: shouldReturnBack.current ? listLink : `${listLink}${id}`,
                    };
                }}
                initialValues={{
                    name: categoryData ? categoryData?.name : '',
                    code: categoryData ? categoryData?.code : '',
                    parent_id: categoryData ? categoryData?.parent_id : null,
                    is_inherits_properties: categoryData ? categoryData?.is_inherits_properties : false,
                    is_self_code: false,
                    is_active: categoryData ? categoryData?.is_active : false,
                }}
                validationSchema={Yup.object().shape({
                    name: Yup.string().required(ErrorMessages.REQUIRED),
                    code: Yup.string().when('is_self_code', ([is_self_code], schema) =>
                        is_self_code ? schema.matches(onlyEnglishLettersDigits, ErrorMessages.WRONG_FORMAT) : schema
                    ),
                })}
                enableReinitialize
                disabled={!canEditForm}
            >
                {({ reset: resetForm }) => (
                    <>
                        <PageTemplate
                            h1={pageTitle}
                            backlink={{ text: 'Назад', href: listLink }}
                            controls={
                                <PageControls
                                    access={{
                                        ID: {
                                            create: true,
                                            edit: true,
                                            view: true,
                                            delete: canDeleteCategory,
                                        },
                                        LIST: {
                                            view: true,
                                        },
                                    }}
                                >
                                    <PageControls.Delete
                                        onClick={() => {
                                            popupDispatch({
                                                type: ActionType.Delete,
                                                payload: {
                                                    title: 'Вы уверены, что хотите удалить категорию?',
                                                    popupAction: ActionEnum.DELETE,
                                                    onAction: async () => {
                                                        resetForm();
                                                        await deleteCategory?.mutateAsync(Number(id));
                                                        push(listLink);
                                                    },
                                                },
                                            });
                                        }}
                                    />
                                    <PageControls.Close onClick={() => push(listLink)} />
                                    <PageControls.Apply
                                        onClick={() => {
                                            shouldReturnBack.current = false;
                                        }}
                                    />
                                    <PageControls.Save
                                        onClick={() => {
                                            shouldReturnBack.current = true;
                                        }}
                                    />
                                </PageControls>
                            }
                            aside={
                                <>
                                    <DescriptionList css={{ marginBottom: scale(5) }}>
                                        <DescriptionListItem
                                            value={
                                                <FormFieldWrapper name="is_active" css={{ marginBottom: scale(3) }}>
                                                    <Switcher disabled={!canEditActivity}>
                                                        Активность категории
                                                    </Switcher>
                                                </FormFieldWrapper>
                                            }
                                        />
                                        {(
                                            [
                                                { name: 'ID', value: categoryData?.id },
                                                {
                                                    name: 'Код',
                                                    value: categoryData?.code,
                                                },
                                            ] as DescriptionListItemType[]
                                        ).map(item => (
                                            <DescriptionListItem {...item} key={item.name} />
                                        ))}
                                    </DescriptionList>

                                    <DescriptionList>
                                        {getDefaultDates({ ...categoryData }).map(item => (
                                            <DescriptionListItem {...item} key={item.name} />
                                        ))}
                                    </DescriptionList>
                                </>
                            }
                            customChildren
                        >
                            <Tabs css={{ flexGrow: 1 }}>
                                <Tabs.Tab title="Характеристики" id="0">
                                    <Block css={{ height: '100%', borderTopLeftRadius: 0 }}>
                                        <Block css={{ height: '100%', borderTopLeftRadius: 0 }}>
                                            <Block.Body>
                                                <Layout cols={3}>
                                                    <Layout.Item col={{ xxxl: 1, xs: 3 }}>
                                                        <FormFieldWrapper name="name" label="Название категории">
                                                            <Input />
                                                        </FormFieldWrapper>
                                                    </Layout.Item>

                                                    <Layout.Item col={{ xxxl: 1, xs: 3 }}>
                                                        <PageTemplate.FormPanel>
                                                            {({ watch }) => (
                                                                <FormField
                                                                    name="code"
                                                                    label="Символьный код"
                                                                    disabled={!watch('is_self_code')}
                                                                />
                                                            )}
                                                        </PageTemplate.FormPanel>
                                                    </Layout.Item>

                                                    <Layout.Item
                                                        col={{ xxxl: 1, xs: 3 }}
                                                        align="end"
                                                        css={{ paddingBottom: scale(1), [sm]: { paddingBottom: 0 } }}
                                                    >
                                                        <FormFieldWrapper name="is_self_code">
                                                            <FormCheckbox>Задать символьный код вручную</FormCheckbox>
                                                        </FormFieldWrapper>
                                                    </Layout.Item>

                                                    <Layout.Item col={3}>
                                                        <FormFieldWrapper
                                                            name="parent_id"
                                                            label="Родительская категория"
                                                        >
                                                            <Select options={parentCategories} />
                                                        </FormFieldWrapper>
                                                    </Layout.Item>

                                                    <Layout.Item col={3}>
                                                        <FormFieldWrapper name="is_inherits_properties">
                                                            <FormCheckbox>
                                                                Категория наследует атрибуты родительской
                                                            </FormCheckbox>
                                                        </FormFieldWrapper>
                                                    </Layout.Item>
                                                </Layout>
                                            </Block.Body>
                                        </Block>
                                    </Block>
                                </Tabs.Tab>

                                <Tabs.Tab disabled={isCreation || !categoryData} title="Атрибуты" id="1">
                                    <Block css={{ height: '100%' }}>
                                        <Block.Header
                                            css={{
                                                borderBottom: 'none',
                                                [xs]: { flexDirection: 'column', alignItems: 'flex-start' },
                                            }}
                                        >
                                            <h2>Атрибуты категории</h2>

                                            <div css={{ button: { marginLeft: scale(2) } }}>
                                                <Button
                                                    Icon={PlusIcon}
                                                    onClick={() => setAddAttributePopupOpen(true)}
                                                    disabled={!(canAddAndEditingProperties || canEditAllCategoryData)}
                                                    css={{ [xs]: { marginTop: scale(1) } }}
                                                >
                                                    Добавить атрибут
                                                </Button>
                                            </div>
                                        </Block.Header>

                                        <Block.Body css={{ padding: 0 }}>
                                            <SelfPropertiesTable
                                                canEditCategoryData={
                                                    canAddAndEditingProperties || canEditAllCategoryData
                                                }
                                            />
                                        </Block.Body>

                                        <Block.Header css={{ borderBottom: 'none', marginTop: scale(2) }}>
                                            <h2>Наследуемые атрибуты категории</h2>
                                        </Block.Header>

                                        <Block.Body css={{ padding: 0 }}>
                                            <InheritedPropertiesTable />
                                        </Block.Body>
                                    </Block>
                                </Tabs.Tab>
                            </Tabs>
                        </PageTemplate>

                        <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />

                        <AddAttributePopup
                            isOpen={isAddAttributePopupOpen}
                            onRequestClose={() => setAddAttributePopupOpen(false)}
                        />
                    </>
                )}
            </FormWrapper>
        </PageWrapper>
    );
};

export default ProductCategory;
