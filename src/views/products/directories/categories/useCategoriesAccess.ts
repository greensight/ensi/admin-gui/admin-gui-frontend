import { defineAccessMatrix, useAccess } from '@scripts/hooks';
import { useIDForbidden } from '@scripts/hooks/useIDForbidden';

const accessMatrix = defineAccessMatrix({
    ID: {
        view: 502,
        edit: 503,
        create: 504,
        editProperties: 505,
        editDetail: 506,
        delete: 507,
        editActivity: 508,
        editGluing: 509,
    },
    LIST: {
        view: 501,
    },
});

const useCategoriesAccess = () => {
    const access = useAccess(accessMatrix);
    const isIDForbidden = useIDForbidden(access);

    return { access, isIDForbidden };
};

export default useCategoriesAccess;
