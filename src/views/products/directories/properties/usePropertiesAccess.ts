import { defineAccessMatrix, useAccess } from '@scripts/hooks';
import { useIDForbidden } from '@scripts/hooks/useIDForbidden';

export const accessMatrix = defineAccessMatrix({
    LIST: {
        view: 601,
    },
    ID: {
        view: 602,
        edit: 603,
        create: 604,
        delete: 605,
        editActive: 606,
        editPublish: 607,
    },
});

export const usePropertiesAccess = () => {
    const access = useAccess(accessMatrix);
    const isIDForbidden = useIDForbidden(access);

    return { access, isIDForbidden };
};
