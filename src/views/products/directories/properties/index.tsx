import { useMemo } from 'react';

import { Property, useProperties, usePropertiesMeta } from '@api/catalog';

import ListBuilder from '@components/ListBuilder';
import { TooltipItem } from '@components/Table';

import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import { usePropertiesAccess } from './usePropertiesAccess';

const ProductProperties = () => {
    const { access } = usePropertiesAccess();

    const goToDetailPage = useGoToDetailPage();

    const tooltipContent: TooltipItem[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Редактировать атрибут',
                action: goToDetailPage,
                isDisable: !access.ID.view,
            },
        ],
        [access.ID.view, goToDetailPage]
    );

    return (
        <ListBuilder<Property>
            access={access}
            searchHook={useProperties}
            metaHook={usePropertiesMeta}
            tooltipItems={tooltipContent}
            title="Атрибуты"
        />
    );
};

export default ProductProperties;
