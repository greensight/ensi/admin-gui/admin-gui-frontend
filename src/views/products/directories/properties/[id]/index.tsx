import {
    DescriptionList,
    DescriptionListItem,
    DescriptionListItemType,
    FormFieldWrapper,
    getDefaultDates,
    useActionPopup,
} from '@ensi-platform/core-components';
import { format, parseISO } from 'date-fns';
import { useRouter } from 'next/router';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';

import {
    DirectoryData,
    useDirectoryPreloadFile,
    useDirectoryPreloadImage,
    useProperty,
    usePropertyChange,
    usePropertyCreate,
    usePropertyDirectoryChange,
    usePropertyDirectoryMassCreate,
    usePropertyDirectoryRemove,
    usePropertyRemove,
} from '@api/catalog';

import { useError, useSuccess } from '@context/modal';

import { downloadFile } from '@controls/Dropzone/utils';
import Switcher from '@controls/Switcher';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import PageLeaveGuard from '@components/PageLeaveGuard';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { CREATE_PARAM, ModalMessages } from '@scripts/constants';
import { ButtonNameEnum, RedirectMethods } from '@scripts/enums';
import { scale } from '@scripts/gds';
import { formatISOTimeFromDate, isPageNotFound, prepareDateTime } from '@scripts/helpers';

import { AttributePropertyEnum, AttributeTypeEnum, dataToAttrProps, getValidationProperties } from '../../../scripts';
import { usePropertiesAccess } from '../usePropertiesAccess';
import FormChildren from './components/FormChildren';
import { generateEmptyValueForAttribute } from './scripts/helpers';
import { DirectoryValueItem } from './scripts/types';

const ProductProperty = () => {
    const { query, push, pathname } = useRouter();
    let id = Array.isArray(query.id) ? query.id[0] : query.id;
    const isCreation = id === CREATE_PARAM;

    if (Number.isNaN(id)) id = undefined;

    const listLink = pathname.split(`[id]`)[0];
    const [filesState, setFilesState] = useState<(File | undefined)[]>([]);
    const buttonNameRef = useRef<ButtonNameEnum | null>(null);

    const { access, isIDForbidden } = usePropertiesAccess();

    const {
        edit: canEdit,
        create: canCreate,
        delete: canDelete,
        editActive: canEditActive,
        editPublish: canEditPublish,
    } = access.ID;

    const {
        mutateAsync: addProperty,
        isPending: isLoadingCreate,
        error: createError,
        isSuccess: isCreateSuccess,
    } = usePropertyCreate();
    useError(createError);
    useSuccess(isCreateSuccess ? ModalMessages.SUCCESS_SAVE : '');

    const { mutateAsync: preloadDirectoryImage } = useDirectoryPreloadImage();
    const { mutateAsync: preloadDirectoryFile } = useDirectoryPreloadFile();

    const { popupState, popupDispatch, ActionPopup, ActionEnum, ActionType } = useActionPopup();

    const { data: apiData, isFetching: isDetailLoading, error: isDetailError } = useProperty(id);
    useError(isDetailError);

    const { mutateAsync: changeProperty, isPending: isLoadingChange, error: changeError } = usePropertyChange();
    useError(changeError);

    const { mutateAsync: removeProperty, error: removeError } = usePropertyRemove();
    useError(removeError);

    const {
        mutateAsync: massAddDirectories,
        isPending: isLoadingMassAddDirectories,
        error: massAddDirectoriesError,
        isSuccess: isMassAddSuccess,
    } = usePropertyDirectoryMassCreate();
    useError(massAddDirectoriesError);
    useSuccess(isMassAddSuccess ? ModalMessages.SUCCESS_SAVE : '');

    const {
        mutateAsync: changeDirectory,
        isPending: isLoadingDirectoryChange,
        error: changeDirectoryError,
        isSuccess: isChangeSuccess,
    } = usePropertyDirectoryChange();
    useError(changeDirectoryError);
    useSuccess(isChangeSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const {
        mutateAsync: removePropertyDirectory,
        isPending: isLoadingDirectoryRemove,
        error: removeDirectoryError,
        isSuccess: isRemoveSuccess,
    } = usePropertyDirectoryRemove();
    useError(removeDirectoryError);
    useSuccess(isRemoveSuccess ? ModalMessages.SUCCESS_DELETE : '');

    const propertyData = apiData ? apiData.data : null;

    const existingFiles = useCallback(async () => {
        if (propertyData?.directory?.length) {
            await Promise.all(
                propertyData?.directory.map(item => (item.url ? downloadFile(item.url, item.name) : undefined))
            ).then(res => {
                setFilesState(res);
            });
        }
    }, [propertyData?.directory]);

    useEffect(() => {
        existingFiles();
    }, [existingFiles]);

    const preparedAdditionalAttributes = useMemo(
        () =>
            propertyData?.directory?.length
                ? propertyData.directory
                      ?.sort((a, b) => Number(a.id) - Number(b.id))
                      ?.map(({ name, code, type, url, value, id: attrId }, index) => ({
                          value: name,
                          name: code,
                          code,
                          attrId,
                          date: null,
                          time: null,
                          ...(type === AttributeTypeEnum.IMAGE &&
                              url &&
                              filesState.length && {
                                  image: [filesState.find(el => el!.name === name)],
                              }),
                          ...(type === AttributeTypeEnum.FILE &&
                              url &&
                              filesState.length && {
                                  file: [filesState[index]],
                              }),
                          ...(type === AttributeTypeEnum.DATETIME &&
                              value && {
                                  date: format(parseISO(value), 'dd.MM.yyyy'),
                                  time: formatISOTimeFromDate(value),
                              }),
                      }))
                : generateEmptyValueForAttribute(),
        [filesState]
    );

    const [attrType, setAttrType] = useState(propertyData ? propertyData.type : '');

    useEffect(() => {
        if (!attrType && propertyData?.type) setAttrType(propertyData.type);
    }, [attrType, propertyData?.type]);

    const validationSchema = useMemo(() => getValidationProperties(attrType), [attrType]);

    const pageTitle = isCreation ? 'Создание атрибута' : 'Редактирование товарного атрибута';

    const initialValues = useMemo(
        () => ({
            attrType: propertyData ? propertyData.type : '',
            is_active: propertyData?.is_active || false,
            attrProps: propertyData ? dataToAttrProps(propertyData) : [],
            productNameForAdmin: propertyData ? propertyData.name : '',
            productNameForPublic: propertyData ? propertyData.display_name : '',
            additionalAttributes: preparedAdditionalAttributes,
        }),
        [preparedAdditionalAttributes, propertyData]
    );

    const isDisabled = isCreation ? !canCreate : !canEdit;

    const isNotFound = isPageNotFound({ id, error: isDetailError, isCreation });

    return (
        <PageWrapper
            title={pageTitle}
            isLoading={
                isLoadingCreate ||
                isDetailLoading ||
                isLoadingChange ||
                isLoadingDirectoryChange ||
                isLoadingDirectoryRemove ||
                isLoadingMassAddDirectories
            }
            isNotFound={isNotFound}
            isForbidden={isIDForbidden}
        >
            <FormWrapper
                initialValues={initialValues}
                validationSchema={validationSchema}
                enableReinitialize
                disabled={isDisabled}
                onSubmit={async values => {
                    const addPropertyValues = {
                        name: values.productNameForAdmin || values.productNameForPublic,
                        display_name: values.productNameForPublic,
                        type: values.attrType,
                        is_multiple: values.attrProps.includes(AttributePropertyEnum.FEW_VALUES),
                        is_active: values.is_active,
                        is_public: values.attrProps.includes(AttributePropertyEnum.PUBLIC),
                        has_directory: values.attrProps.includes(AttributePropertyEnum.DIRECTORY),
                        ...(!Number.isNaN(id || NaN) && { id: Number(id) }),
                    };

                    const additionalAttributes = (values.additionalAttributes || []) as DirectoryValueItem[];

                    if (isCreation) {
                        const { data: property } = await addProperty(addPropertyValues);

                        const tasks = additionalAttributes
                            .filter(item => {
                                const { value, image, file, date, time, code } = item;
                                return !!(value || image || file || date || time || code);
                            })
                            .map<Promise<DirectoryData>>(async item => {
                                const { value, image, file, date, time, code } = item;

                                const formData = new FormData();
                                let fileId;

                                if (image?.length) {
                                    if (image[0]) formData.append('file', image[0]);

                                    const { data: resData } = await preloadDirectoryImage({ formData });
                                    fileId = resData.preload_file_id;
                                }

                                if (file?.length) {
                                    if (file[0]) formData.append('file', file[0]);

                                    const { data: resData } = await preloadDirectoryFile({ formData });
                                    fileId = resData.preload_file_id;
                                }

                                const name = `${value || (date && time && prepareDateTime(date, time))}`;

                                return {
                                    name,
                                    value:
                                        values.attrType === AttributeTypeEnum.DATETIME && date && time
                                            ? prepareDateTime(date, time)
                                            : value,
                                    ...(code && values.attrType === AttributeTypeEnum.COLOR && { code, value: code }),
                                    ...(fileId && { name: `${fileId}`, preload_file_id: fileId }),
                                };
                            });

                        const directoriesForAdd = await Promise.all(tasks);

                        if (directoriesForAdd.length)
                            await massAddDirectories({
                                propertyId: property.id,
                                items: directoriesForAdd,
                            });

                        return {
                            method:
                                buttonNameRef.current === ButtonNameEnum.SAVE
                                    ? RedirectMethods.push
                                    : RedirectMethods.replace,
                            redirectPath:
                                buttonNameRef.current === ButtonNameEnum.SAVE ? listLink : `${listLink}${property.id}`,
                        };
                    }

                    if (!propertyData)
                        return {
                            method: RedirectMethods.replace,
                            redirectPath: `${listLink}${id}`,
                        };

                    if (propertyData.type !== addPropertyValues.type)
                        await changeProperty({ id: Number(id), type: addPropertyValues.type });

                    const computedAdditionalAttributes = additionalAttributes?.map(item => ({
                        ...item,
                        ...(!item?.name && { name: item?.value }),
                        ...(!item?.code && { code: item?.value }),
                    }));

                    const directoryPromises: Promise<any>[] = [];

                    const additionalAttributesWithValues: DirectoryValueItem[] = computedAdditionalAttributes.filter(
                        ({ value, image, file, date }: DirectoryValueItem) =>
                            Boolean(value || image?.length || file?.length || date)
                    );

                    if (!propertyData.has_directory) {
                        await changeProperty(addPropertyValues);
                    }

                    if (additionalAttributesWithValues.length) {
                        if (addPropertyValues.has_directory) {
                            const deletedValues =
                                propertyData.directory?.filter(value => {
                                    if (
                                        value.type === AttributeTypeEnum.FILE ||
                                        value.type === AttributeTypeEnum.IMAGE
                                    ) {
                                        return (
                                            (value.type === AttributeTypeEnum.IMAGE &&
                                                !additionalAttributesWithValues.find(item => item.value === value.name)
                                                    ?.image?.length) ||
                                            (value.type === AttributeTypeEnum.FILE &&
                                                !additionalAttributesWithValues.find(item => item.value === value.name)
                                                    ?.file?.length)
                                        );
                                    }
                                    return !additionalAttributesWithValues?.find(
                                        ({ attrId }) => Number(attrId) === Number(value?.id)
                                    );
                                }) || [];

                            const directoriesForAdd: DirectoryData[] = [];

                            await Promise.allSettled(
                                additionalAttributesWithValues.map(
                                    async (
                                        { value, attrId, code, image, file, date, time }: DirectoryValueItem,
                                        index
                                    ) => {
                                        const attr = propertyData.directory?.find(
                                            directoryItem => directoryItem?.id === attrId
                                        );

                                        let fileId;

                                        if (
                                            addPropertyValues.type === AttributeTypeEnum.FILE ||
                                            addPropertyValues.type === AttributeTypeEnum.IMAGE
                                        ) {
                                            if (image?.length) {
                                                const formData = new FormData();
                                                formData.append('file', image[0]);

                                                const { data: resData } = await preloadDirectoryImage({ formData });
                                                fileId = resData.preload_file_id;
                                            }

                                            if (file?.length) {
                                                const formData = new FormData();
                                                formData.append('file', file[0]);

                                                const { data: resData } = await preloadDirectoryFile({ formData });
                                                fileId = resData.preload_file_id;
                                            }
                                        }

                                        if (
                                            attr &&
                                            (attr.name !== value ||
                                                attr.code !== code ||
                                                propertyData.type !== addPropertyValues.type) &&
                                            !attr.url
                                        ) {
                                            let preparedValue;

                                            if (values.attrType === AttributeTypeEnum.COLOR) {
                                                preparedValue = code;
                                            } else if (values.attrType === AttributeTypeEnum.DATETIME && date && time) {
                                                preparedValue = prepareDateTime(date, time);
                                            } else {
                                                preparedValue = value;
                                            }

                                            directoryPromises.push(
                                                changeDirectory({
                                                    directoryId: attr.id,
                                                    name: `${value || (date && time && prepareDateTime(date, time))}`,
                                                    value: preparedValue,
                                                    ...(code &&
                                                        values.attrType === AttributeTypeEnum.COLOR && {
                                                            code,
                                                        }),
                                                })
                                            );
                                        } else if (
                                            attr &&
                                            fileId &&
                                            !deletedValues.map(i => i.name).includes(attr.name)
                                        ) {
                                            directoryPromises.push(
                                                changeDirectory({
                                                    directoryId: attr.id,
                                                    name: value,
                                                    value: `${value}`,
                                                    ...(fileId && {
                                                        name: `${fileId}`,
                                                        preload_file_id: Number(fileId),
                                                    }),
                                                })
                                            );
                                        } else if (!attr && (code || fileId || date)) {
                                            let preparedValue;

                                            if (values.attrType === AttributeTypeEnum.COLOR) {
                                                preparedValue = code;
                                            } else if (values.attrType === AttributeTypeEnum.DATETIME && date && time) {
                                                preparedValue = prepareDateTime(date, time);
                                            } else {
                                                preparedValue = value;
                                            }

                                            directoriesForAdd.push({
                                                name: `${value || (date && time && prepareDateTime(date, time))}`,
                                                value: preparedValue,
                                                ...(code && values.attrType === AttributeTypeEnum.COLOR && { code }),
                                                ...(fileId && { name: `${fileId}`, preload_file_id: fileId }),
                                            });
                                        }

                                        if (
                                            index === additionalAttributesWithValues.length - 1 &&
                                            directoriesForAdd.length
                                        ) {
                                            await massAddDirectories({
                                                propertyId: Number(id),
                                                items: directoriesForAdd,
                                            });
                                        }
                                    }
                                )
                            );

                            deletedValues.forEach(value => {
                                directoryPromises.push(removePropertyDirectory(value.id));
                            });
                        } else {
                            additionalAttributesWithValues.forEach(value => {
                                directoryPromises.push(removePropertyDirectory(Number(value.attrId)));
                            });
                        }
                        await Promise.allSettled(directoryPromises);

                        if (propertyData.has_directory) {
                            await changeProperty(addPropertyValues);
                        }
                    } else {
                        await changeProperty(addPropertyValues);
                    }

                    return {
                        method:
                            buttonNameRef.current === ButtonNameEnum.SAVE
                                ? RedirectMethods.push
                                : RedirectMethods.replace,
                        redirectPath: buttonNameRef.current === ButtonNameEnum.SAVE ? listLink : `${listLink}${id}`,
                    };
                }}
            >
                {({ reset: resetForm }) => (
                    <>
                        <PageTemplate
                            h1={pageTitle}
                            backlink={{ text: 'Назад', href: '/products/directories/properties' }}
                            controls={
                                <PageControls
                                    access={{
                                        ID: {
                                            create: true,
                                            edit: true,
                                            view: true,
                                            delete: canDelete,
                                        },
                                        LIST: {
                                            view: true,
                                        },
                                    }}
                                >
                                    <PageControls.Delete
                                        onClick={() => {
                                            popupDispatch({
                                                type: ActionType.Delete,
                                                payload: {
                                                    title: 'Вы уверены, что хотите удалить атрибут?',
                                                    popupAction: ActionEnum.DELETE,
                                                    onAction: async () => {
                                                        if (id) {
                                                            resetForm();
                                                            await removeProperty(Number(id));
                                                            push(listLink);
                                                        }
                                                    },
                                                },
                                            });
                                        }}
                                    />
                                    <PageControls.Close onClick={() => push(listLink)} />
                                    <PageControls.Apply
                                        onClick={() => {
                                            buttonNameRef.current = ButtonNameEnum.APPLY;
                                        }}
                                    />
                                    <PageControls.Save
                                        onClick={() => {
                                            buttonNameRef.current = ButtonNameEnum.SAVE;
                                        }}
                                    />
                                </PageControls>
                            }
                            aside={
                                <>
                                    <DescriptionList css={{ marginBottom: scale(5) }}>
                                        <DescriptionListItem
                                            value={
                                                <FormFieldWrapper name="is_active">
                                                    <Switcher
                                                        disabled={isCreation ? !canCreate : !(canEditActive || canEdit)}
                                                    >
                                                        Активность атрибута
                                                    </Switcher>
                                                </FormFieldWrapper>
                                            }
                                        />
                                        {(
                                            [
                                                { name: 'ID', value: propertyData?.id },
                                                {
                                                    name: 'Код',
                                                    value: propertyData?.code,
                                                },
                                            ] as DescriptionListItemType[]
                                        ).map(item => (
                                            <DescriptionListItem {...item} key={item.name} />
                                        ))}
                                    </DescriptionList>

                                    <DescriptionList css={{ marginBottom: scale(5) }}>
                                        {getDefaultDates({ ...propertyData }).map(item => (
                                            <DescriptionListItem {...item} key={item.name} />
                                        ))}
                                    </DescriptionList>
                                </>
                            }
                        >
                            <PageLeaveGuard />
                            <FormChildren
                                onAttrTypeChange={e => {
                                    setAttrType(e?.selected?.value);
                                }}
                                canEditPropertyViewOnShowcase={isCreation ? canCreate : canEditPublish || canEdit}
                            />
                        </PageTemplate>

                        <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
                    </>
                )}
            </FormWrapper>
        </PageWrapper>
    );
};

export default ProductProperty;
