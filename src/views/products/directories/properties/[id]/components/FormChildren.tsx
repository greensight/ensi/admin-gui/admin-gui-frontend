import { FormCheckbox, FormFieldWrapper, Input, useForm, useFormContext } from '@ensi-platform/core-components';
import { useEffect, useMemo } from 'react';

import { usePropertiesTypes } from '@api/catalog';

import { useError } from '@context/modal';

import { AttributePropertyEnum, AttributeTypeEnum } from '@views/products/scripts';

import Select from '@controls/Select';

import { Layout, colors, scale, typography } from '@scripts/gds';

import { FormValuesTypes } from '../scripts/types';
import AdditionalAttributes from './AdditionalAttributes';

const FormChildren = ({
    onAttrTypeChange,
    canEditPropertyViewOnShowcase,
}: {
    onAttrTypeChange?: (e: any) => void;
    canEditPropertyViewOnShowcase: boolean;
}) => {
    const { disabled } = useForm();
    const {
        watch,
        formState: { errors },
        setValue: setFieldValue,
    } = useFormContext<FormValuesTypes>();

    const [attrType, attrProps] = watch(['attrType', 'attrProps']);

    useEffect(() => {
        if (attrType === AttributeTypeEnum.BOOLEAN && attrProps.includes(AttributePropertyEnum.DIRECTORY)) {
            setFieldValue(
                'attrProps',
                attrProps.filter(
                    prop => prop !== AttributePropertyEnum.DIRECTORY && prop !== AttributePropertyEnum.FEW_VALUES
                )
            );
        }
    }, [attrType, setFieldValue, attrProps]);

    const { data: propertiesTypes, error: propertiesTypesError } = usePropertiesTypes();
    useError(propertiesTypesError);

    const propertiesTypesOptions = useMemo(
        () => propertiesTypes?.data?.map(item => ({ value: item.id, key: item.name })),
        [propertiesTypes]
    );

    return (
        <Layout cols={2}>
            <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                <FormFieldWrapper name="productNameForPublic" label="Публичное название" disabled={disabled}>
                    <Input />
                </FormFieldWrapper>
            </Layout.Item>

            <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                <FormFieldWrapper name="productNameForAdmin" label="Рабочее название" disabled={disabled}>
                    <Input />
                </FormFieldWrapper>
            </Layout.Item>

            <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                {propertiesTypesOptions && (
                    <FormFieldWrapper name="attrType" disabled={disabled}>
                        <Select
                            label="Тип"
                            options={propertiesTypesOptions}
                            onChange={onAttrTypeChange}
                            hideClearButton
                            visibleOptions={7}
                        />
                    </FormFieldWrapper>
                )}
            </Layout.Item>

            <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                <fieldset>
                    <FormFieldWrapper name="attrProps">
                        <FormCheckbox value={AttributePropertyEnum.PUBLIC} disabled={!canEditPropertyViewOnShowcase}>
                            Отображение на витрине
                        </FormCheckbox>
                    </FormFieldWrapper>

                    <FormFieldWrapper name="attrProps">
                        <FormCheckbox
                            value={AttributePropertyEnum.FEW_VALUES}
                            disabled={disabled || attrType === AttributeTypeEnum.BOOLEAN}
                        >
                            Атрибут хранит несколько значений
                        </FormCheckbox>
                    </FormFieldWrapper>

                    <FormFieldWrapper name="attrProps">
                        <FormCheckbox
                            value={AttributePropertyEnum.DIRECTORY}
                            disabled={disabled || attrType === AttributeTypeEnum.BOOLEAN}
                        >
                            Атрибут является справочником
                        </FormCheckbox>
                    </FormFieldWrapper>
                </fieldset>
            </Layout.Item>

            <Layout.Item col={2}>
                <h2 css={{ ...typography('h3'), marginBottom: scale(2) }}>Атрибут может принимать значения:</h2>

                {attrProps.includes(AttributePropertyEnum.DIRECTORY) ? (
                    <>
                        {attrType === AttributeTypeEnum.IMAGE && (
                            <Layout cols={1} gap={scale(1)} css={{ marginBottom: scale(1) }}>
                                <Layout.Item>
                                    <h5
                                        css={{
                                            ...typography('bodySmBold'),
                                        }}
                                    >
                                        Изображения:
                                    </h5>
                                </Layout.Item>
                                <Layout.Item>
                                    <ul
                                        css={{
                                            ...typography('bodySm'),
                                        }}
                                    >
                                        <li>1. Формат &#8212; JPEG или PNG;</li>
                                        <li>2. Размер &#8212; не больше 1 МБ;</li>
                                    </ul>
                                </Layout.Item>
                            </Layout>
                        )}

                        <p
                            css={{
                                marginBottom: scale(2),
                                ...(errors?.additionalAttributes &&
                                    !Array.isArray(errors?.additionalAttributes) && { color: colors?.danger }),
                            }}
                        >
                            Укажите не менее двух возможных значений для атрибута:
                        </p>

                        <AdditionalAttributes attrType={attrType} />
                    </>
                ) : (
                    <>
                        <p css={{ marginBottom: scale(2), color: colors?.success }}>
                            Атрибут может принимать любые значения
                        </p>
                        <p css={{ marginBottom: scale(2) }}>
                            Чтобы установить жестко заданные варианты, выберите тип &ldquo;Справочник&rdquo;
                        </p>
                    </>
                )}
            </Layout.Item>
        </Layout>
    );
};

export default FormChildren;
