import { CalendarInput, FormFieldWrapper, Input, useForm, useFormContext } from '@ensi-platform/core-components';
import { ChangeEvent, useCallback, useEffect, useMemo, useState } from 'react';

import { download } from '@api/common';

import { AttributeTypeEnum } from '@views/products/scripts';

import Dropzone from '@controls/Dropzone';

import { ImageAcceptTypes } from '@scripts/constants';
import { Layout, scale } from '@scripts/gds';
import { debounce } from '@scripts/helpers';
import { usePrevious } from '@scripts/hooks';

import { FormValuesTypes } from '../scripts/types';

interface AdditionalAttributeItemProps {
    attrType: AttributeTypeEnum;
    name: `additionalAttributes.${number}`;
    index: number;
}

const AdditionalAttributeItem = ({ attrType, index, name }: AdditionalAttributeItemProps) => {
    const {
        setValue: setFieldValue,
        watch,
        formState: { errors },
    } = useFormContext<FormValuesTypes>();
    const { disabled } = useForm()!;

    const error = errors?.additionalAttributes?.[index];
    const additionalAttributes = watch('additionalAttributes');

    const { attrId, code: currentColorValue } = additionalAttributes[index];

    const prevCurrentValue = usePrevious(currentColorValue);
    const [color, setColor] = useState('#000000');

    useEffect(() => {
        if (prevCurrentValue !== currentColorValue && currentColorValue) {
            setColor(currentColorValue);
        }
    }, [prevCurrentValue, currentColorValue]);

    const changeColor = useCallback(
        debounce((e: ChangeEvent<HTMLInputElement>) => {
            const colorValue = e?.target?.value;
            setFieldValue(`${name}.code`, colorValue);
        }, 300),
        [index]
    );

    const isSpecialField = useMemo(
        () =>
            attrType === AttributeTypeEnum.COLOR ||
            attrType === AttributeTypeEnum.FILE ||
            attrType === AttributeTypeEnum.IMAGE ||
            attrType === AttributeTypeEnum.DATETIME,
        [attrType]
    );

    useEffect(() => {
        if (attrType === AttributeTypeEnum.COLOR && !currentColorValue) setFieldValue(`${name}.code`, '#000000');
    }, [attrType, currentColorValue, index, name, setFieldValue]);

    return (
        <Layout cols={4}>
            {attrType === AttributeTypeEnum.COLOR && (
                <>
                    <Layout.Item col={2}>
                        <FormFieldWrapper
                            label={`Возможное значение ${index + 1}`}
                            name={`${name}.value`}
                            disabled={disabled}
                        >
                            <Input error={error?.value?.message} />
                        </FormFieldWrapper>
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <FormFieldWrapper label="Код цвета" name={`${name}.code`} disabled={disabled}>
                            <Input error={error?.code?.message} />
                        </FormFieldWrapper>
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <input
                            name={`${name}.code`}
                            type="color"
                            value={color}
                            onChange={e => {
                                changeColor(e);
                                setColor(e.target.value);
                            }}
                            css={{ width: '100%', height: scale(7, true), marginTop: scale(3) }}
                            disabled={disabled}
                        />
                    </Layout.Item>
                </>
            )}

            {attrType === AttributeTypeEnum.IMAGE && (
                <Layout.Item col={4}>
                    <FormFieldWrapper label="Изображение" name={`${name}.image`}>
                        <Dropzone accept={ImageAcceptTypes} disabled={disabled} error={error?.image?.message} />
                    </FormFieldWrapper>
                </Layout.Item>
            )}

            {attrType === AttributeTypeEnum.FILE && (
                <Layout.Item col={4}>
                    <FormFieldWrapper label="Файл" name={`${name}.file`}>
                        <Dropzone
                            onFileClick={f => {
                                const url = URL.createObjectURL(f);
                                download(url, f.name);
                                URL.revokeObjectURL(url);
                            }}
                            enableFileClick={Boolean(attrId)}
                            disabled={disabled}
                            error={error?.file?.message}
                        />
                    </FormFieldWrapper>
                </Layout.Item>
            )}

            {attrType === AttributeTypeEnum.DATETIME && (
                <Layout.Item col={4} css={{ display: 'flex', alignItems: 'end' }}>
                    <FormFieldWrapper name={`${name}.date`} css={{ marginRight: scale(1) }}>
                        <CalendarInput
                            picker
                            placeholder="Дата"
                            label="Дата"
                            view="date"
                            platform="desktop"
                            error={error?.date?.message}
                        />
                    </FormFieldWrapper>
                    <FormFieldWrapper name={`${name}.time`} label="Время" placeholder="Время">
                        <Input type="time" error={error?.time?.message} />
                    </FormFieldWrapper>
                </Layout.Item>
            )}

            {!isSpecialField && (
                <Layout.Item col={4}>
                    <FormFieldWrapper
                        label={`Возможное значение ${index + 1}`}
                        name={`${name}.value`}
                        type={
                            attrType === AttributeTypeEnum.INTEGER || attrType === AttributeTypeEnum.DOUBLE
                                ? 'number'
                                : 'text'
                        }
                        disabled={disabled}
                    >
                        <Input error={error?.value?.message} />
                    </FormFieldWrapper>
                </Layout.Item>
            )}
        </Layout>
    );
};

export default AdditionalAttributeItem;
