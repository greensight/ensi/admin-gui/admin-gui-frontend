import { useFieldArray, useForm } from '@ensi-platform/core-components';
import { Fragment } from 'react';

import { AttributeTypeEnum } from '@views/products/scripts';

import { Button, Layout, scale } from '@scripts/gds';

import IconPlus from '@icons/plus.svg';
import IconTrash from '@icons/small/trash.svg';

import { getEmptyValue } from '../scripts/helpers';
import AdditionalAttributeItem from './AdditionalAttributeItem';

interface IAdditionalAttributeProps {
    attrType: AttributeTypeEnum;
}

/**
 * Field array of additional attributes for DIRECTORY property
 */
const AdditionalAttributes = ({ attrType }: IAdditionalAttributeProps) => {
    const { disabled } = useForm()!;
    const { fields, append, remove } = useFieldArray({
        name: 'additionalAttributes',
    });

    const isOneField = fields.length === 1;

    return (
        <Layout
            cols={isOneField ? 3 : 5}
            gap={scale(2)}
            align="end"
            css={{
                alignItems: 'end',
                marginBottom: scale(2),
            }}
        >
            {fields.map((attr, index) => (
                <Fragment key={attr.id}>
                    <Layout.Item col={3}>
                        <AdditionalAttributeItem
                            name={`additionalAttributes.${index}`}
                            index={index}
                            attrType={attrType}
                        />
                    </Layout.Item>

                    {!isOneField && (
                        <Layout.Item>
                            <Button
                                Icon={IconTrash}
                                theme="outline"
                                disabled={disabled}
                                hidden
                                onClick={() => remove(index)}
                            >
                                Удалить
                            </Button>
                        </Layout.Item>
                    )}
                </Fragment>
            ))}

            <Layout.Item col={5}>
                <Button Icon={IconPlus} disabled={disabled} onClick={() => append(getEmptyValue())}>
                    Добавить
                </Button>
            </Layout.Item>
        </Layout>
    );
};

export default AdditionalAttributes;
