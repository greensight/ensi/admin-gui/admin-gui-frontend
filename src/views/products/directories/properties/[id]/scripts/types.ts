import { AttributeTypeEnum } from '@views/products/scripts';

export interface DirectoryValueItem {
    value?: string;
    name: string;
    code?: string;
    image?: File[] | string | undefined;
    file?: File[] | string | undefined;
    attrId?: number;
    date?: string;
    time?: string;
}

export interface FormValuesTypes {
    attrType: AttributeTypeEnum;
    attrProps: string[];
    productNameForAdmin: string;
    productNameForPublic: string;
    additionalAttributes: DirectoryValueItem[];
}
