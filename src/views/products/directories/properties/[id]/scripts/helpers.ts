export const getEmptyValue = () => ({ value: '', code: '', name: '', image: '', file: '', date: '', time: '' });

export const generateEmptyValueForAttribute = () => Array.from({ length: 2 }, () => getEmptyValue());
