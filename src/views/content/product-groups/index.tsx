// import { FieldValues } from 'formik';
// import Link from 'next/link';
// import { useRouter } from 'next/router';
// import { useEffect, useMemo, useState } from 'react';
//
// import { useDeleteProductGroup, useProductGroupTypes, useProductGroups } from '@api/catalog';
//
// import { Block } from '@ensi-platform/core-components';
// import OldTable from '@components/OldTable';
// import PageWrapper from '@components/PageWrapper';
// import {Form, FormFieldWrapper} from '@ensi-platform/core-components';
// import Pagination from '@controls/Pagination';
// import Select from '@controls/Select';
//
// import { Button, Layout, scale } from '@scripts/gds';
// import { useFiltersHelper } from '@scripts/hooks';
// import { useProductTypesSelectable } from '@scripts/hooks/useProductTypesSelectable';
//
// import PlusIcon from '@icons/small/plus.svg';
// import { useActionPopup } from "@hooks/useActionPopup";
//
// type FilterForm = {
//     id: string;
//     type_id: string;
// };
//
// const columns = [
//     {
//         Header: 'ID',
//         accessor: 'id',
//     },
//     {
//         Header: 'Видимость',
//         accessor: 'visibility',
//     },
//     {
//         Header: 'Изображение',
//         accessor: 'photo',
//         getProps: () => ({ type: 'photo' }),
//     },
//     {
//         Header: 'Название',
//         accessor: 'title',
//     },
//     {
//         Header: 'Тип',
//         accessor: 'type',
//     },
// ];
//
// const Filter = ({
//     onSubmit,
//     onReset,
//     initialValues,
//     emptyValues,
// }: {
//     onSubmit: (vals: FieldValues) => void;
//     onReset: (vals: FieldValues) => void;
//     initialValues: FieldValues;
//     emptyValues: FieldValues;
// }) => {
//     const preparedTypes = useProductTypesSelectable();
//
//     return (
//         <Form onSubmit={onSubmit} initialValues={initialValues} onReset={onReset}>
//             <Layout cols={4} css={{ marginBottom: scale(2) }}>
//                 <Layout.Item col={1}>
//                     <FormField name="id" label="ID" autoComplete="off" />
//                 </Layout.Item>
//                 <Layout.Item col={1}>
//                     <FormFieldWrapper name="type_id" label="Тип">
//                         <Select options={preparedTypes} />
//                     </FormFieldWrapper>
//                 </Layout.Item>
//             </Layout>
//             <FormReset theme="fill" css={{ marginRight: scale(2) }} initialValues={emptyValues}>
//                 Очистить
//             </FormReset>
//             <Button type="submit">Применить</Button>
//         </Form>
//     );
// };
//
// const ContentProductGroups = () => {
//     const initFilter: FilterForm = {
//         id: '',
//         type_id: '',
//     };
//
//     const { initialValues, URLHelper } = useFiltersHelper(initFilter);
//
//     const [filter, setFilter] = useState({});
//
//     useEffect(() => {
//         setFilter({
//             id: initialValues.id.length ? initialValues.id : undefined,
//             type_id: initialValues.type_id.length ? initialValues.type_id : undefined,
//         });
//     }, [initialValues.id, initialValues.type_id]);
//
//     const { data: apiProductGroups } = useProductGroups({
//         sort: ['id'],
//         include: [],
//         pagination: {
//             limit: 10,
//             offset: 0,
//             type: 'offset',
//         },
//         filter,
//     });
//
//     const { data: apiGroupTypes } = useProductGroupTypes({
//         include: [],
//         pagination: {
//             limit: 10,
//             type: 'offset',
//             offset: 0,
//         },
//         sort: ['id'],
//     });
//
//     const { popupState, popupDispatch, ActionPopup, ActionEnum, ActionType } = useActionPopup();
//
//     const data = useMemo(
//         () =>
//             apiProductGroups?.data?.map(e => ({
//                 id: `${e.id}`,
//                 visibility: e.is_shown ? 'Видим' : 'Не видим',
//                 photo: e.preview_photo || '',
//                 title: e.name,
//                 type: apiGroupTypes?.data?.find(t => t.id === e.type_id)?.name || '',
//             })) || [],
//         [apiProductGroups?.data, apiGroupTypes?.data]
//     );
//
//     const pages = useMemo(
//         () =>
//             apiProductGroups?.meta.pagination
//                 ? Math.floor(apiProductGroups.meta.pagination.total / apiProductGroups.meta.pagination.limit) + 1
//                 : 0,
//         [apiProductGroups?.meta.pagination]
//     );
//
//     const deleteProductGroup = useDeleteProductGroup();
//
//     const { push } = useRouter();
//
//     return (
//         <PageWrapper h1="Подборки">
//             <Block css={{ marginBottom: scale(2) }}>
//                 <Block.Body>
//                     <Filter
//                         initialValues={initialValues}
//                         emptyValues={initFilter}
//                         onReset={val => URLHelper(val)}
//                         onSubmit={val => URLHelper(val)}
//                     />
//                 </Block.Body>
//             </Block>
//
//             <Link legacyBehavior href="/content/product-groups/create" passHref>
//                 <Button css={{ marginBottom: scale(2) }} Icon={PlusIcon}>
//                     Создать
//                 </Button>
//             </Link>
//             <Block>
//                 <Block.Body>
//                     <OldTable
//                         needCheckboxesCol={false}
//                         editRow={val => (val ? push(`/content/product-groups/${val.id}`) : '')}
//                         deleteRow={val => {
//                             popupDispatch({
//                                 type: ActionType.Delete,
//                                 payload: {
//                                     title: `Вы уверены, что хотите удалить подборку?`,
//                                     popupAction: ActionEnum.DELETE,
//                                     onAction: () => {
//                                         // TODO: проверить что удаление работает (баг бэка)
//                                         if (val) {
//                                             deleteProductGroup.mutate({
//                                                 id: val.id,
//                                             });
//                                         }
//                                     },
//                                     children: val ? (
//                                         <p css={{ marginBottom: scale(2) }}>
//                                             #{val.id} – {val.title}
//                                         </p>
//                                     ) : (
//                                         ''
//                                     ),
//                                 },
//                             });
//                         }}
//                         columns={columns}
//                         data={data}
//                         css={{ marginBottom: scale(2) }}
//                     />
//                     <Pagination pages={pages} />
//                 </Block.Body>
//             </Block>
//
//             <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
//         </PageWrapper>
//     );
// };
//
// export default ContentProductGroups;
