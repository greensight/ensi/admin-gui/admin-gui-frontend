import { Block, useActionPopup } from '@ensi-platform/core-components';
import { Table as TableType } from '@tanstack/table-core/build/lib/types';
import { useRouter } from 'next/router';
import { useCallback, useMemo } from 'react';

import {
    useNameplateProductsMeta,
    useSearchNameplateProducts,
    useUnbindNameplateProducts,
} from '@api/content/nameplates';
import { NameplateProduct } from '@api/content/types/nameplates';
import { DiscountProduct } from '@api/marketing';

import { useError, useSuccess } from '@context/modal';

import { initialSort } from '@views/logistic/pickup-points/[id]/scripts/constants';
import { useCatalogAccess } from '@views/products/catalog/scripts/hooks';

import LoadingSkeleton from '@controls/LoadingSkeleton';

import AutoFilters from '@components/AutoFilters';
import Table, { ExtendedRow, TooltipItem, TrProps, useSorting, useTable } from '@components/Table';
import { getSelectColumn, getSettingsColumn } from '@components/Table/columns';
import { RowTooltipWrapper, TableEmpty, TableFooter, TableHeader } from '@components/Table/components';

import { ModalMessages } from '@scripts/constants';
import { Button, Layout, scale, typography } from '@scripts/gds';
import { declOfNum, getTotal, getTotalPages } from '@scripts/helpers';
import { useRedirectToNotEmptyPage, useTableList } from '@scripts/hooks';
import { useAutoColumns, useAutoFilters, useAutoTableData } from '@scripts/hooks/autoTable';
import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import { useNameplatesAccess } from '../useNameplatesAccess';

const BindedProducts = ({
    canSubmit,
    onAdd,
    nameplateId,
    className,
}: {
    canSubmit: boolean;
    nameplateId: number;
    className?: string;
    onAdd: () => void;
}) => {
    const { query, push, pathname } = useRouter();

    const { data: metaData, error: metaError, isFetching: isMetaLoading } = useNameplateProductsMeta();
    const meta = metaData?.data;

    const {
        metaField,
        values,
        filtersActive,
        URLHelper,
        searchRequestFilter,
        searchRequestIncludes,
        emptyInitialValues,
        reset,
        codesToSortKeys,
        clearInitialValue,
    } = useAutoFilters(meta);

    const { activePage, itemsPerPageCount, setItemsPerPageCount } = useTableList({
        defaultSort: meta?.default_sort,
        codesToSortKeys,
    });
    const [{ backendSorting }, sortingPlugin] = useSorting<DiscountProduct>(pathname, meta?.fields, initialSort);

    const isEnabled = Boolean(meta) && !!backendSorting && !!nameplateId;

    const {
        data,
        isFetching: isLoading,
        isInitialLoading: isIdle,
        error,
    } = useSearchNameplateProducts(
        {
            sort: backendSorting,
            filter: {
                ...searchRequestFilter,
                nameplate_id: nameplateId,
            },
            pagination: { type: 'offset', limit: itemsPerPageCount, offset: (activePage - 1) * itemsPerPageCount },
        },
        searchRequestIncludes,
        isEnabled
    );

    const isIdleYetEnabled = isIdle && isEnabled;

    const convertedTableData = useMemo(
        () =>
            data?.data.map(e => ({
                ...e,
                customListLink: '/products/catalog',
                customIdResolver: (original: NameplateProduct) => original.product?.id,
            })),
        [data?.data]
    );

    const tableData = useAutoTableData<NameplateProduct>(convertedTableData, metaField, undefined);

    const { popupState, popupDispatch, ActionPopup, ActionEnum, ActionType } = useActionPopup();

    const total = getTotal(data);
    const totalPages = getTotalPages(data, itemsPerPageCount);

    useRedirectToNotEmptyPage({ activePage, itemsPerPageCount, total });

    const unbindProducts = useUnbindNameplateProducts(nameplateId);

    useSuccess(unbindProducts.isSuccess && ModalMessages.SUCCESS_UPDATE);
    useError(error || metaError || unbindProducts.error);

    const onDelete = useCallback(
        async (ids: number[]) => {
            await unbindProducts.mutateAsync({
                ids,
            });

            push({
                pathname,
                query: {
                    entity_id: query?.entity_id,
                },
            });
        },
        [unbindProducts, push, pathname, query?.entity_id]
    );

    const { access } = useNameplatesAccess();
    const catalogAccess = useCatalogAccess();

    const renderHeader = useCallback(
        (table: TableType<NameplateProduct>) => {
            const selected = table.getSelectedRowModel().flatRows;
            return (
                <TableHeader css={{ justifyItems: 'space-between' }}>
                    <Layout type="flex" gap={scale(4)} align="center" css={{ width: '100%', minHeight: scale(9) }}>
                        <Layout.Item>
                            <p css={{ lineHeight: '32px', ...typography('bodySm') }}>
                                <b>Всего привязано:</b> {`${total} ${declOfNum(total, ['товар', 'товара', 'товаров'])}`}
                            </p>
                        </Layout.Item>
                        {!!selected.length && (
                            <>
                                <Layout.Item>
                                    <b>Выбрано:</b> {selected.length}
                                </Layout.Item>
                                <Layout.Item>
                                    <Button type="button" onClick={reset}>
                                        Снять выделение
                                    </Button>
                                </Layout.Item>
                                <Layout.Item>
                                    <Button
                                        disabled={!access.ID.edit}
                                        type="button"
                                        theme="dangerous"
                                        onClick={() => {
                                            popupDispatch({
                                                type: ActionType.Delete,
                                                payload: {
                                                    title: `Вы уверены, что хотите отвязать выбранные товары?`,
                                                    popupAction: ActionEnum.UNTIE,
                                                    onAction: () => {
                                                        const ids = selected
                                                            .filter(e => e.getIsSelected())
                                                            .map(e => Number(e?.original.product.id));
                                                        return onDelete(ids);
                                                    },
                                                    children: <p>Выбрано товаров: {selected.length}</p>,
                                                },
                                            });
                                        }}
                                    >
                                        Отвязать выбранные товары
                                    </Button>
                                </Layout.Item>
                            </>
                        )}
                    </Layout>
                    <Button onClick={onAdd} disabled={!canSubmit}>
                        Добавить
                    </Button>
                </TableHeader>
            );
        },
        [total, access.ID.edit, onAdd, canSubmit, popupDispatch, ActionType.Delete, ActionEnum.UNTIE, onDelete]
    );

    const goToDetailPage = useGoToDetailPage({
        pathname: '/products/catalog',
    });

    const goToDeleteRow = useCallback(
        (originalRows: ExtendedRow['original'][] | undefined) => {
            if (originalRows) {
                popupDispatch({
                    type: ActionType.Delete,
                    payload: {
                        title: `Вы уверены, что хотите отвязать выбранные товары?`,
                        popupAction: ActionEnum.UNTIE,
                        onAction: () => onDelete([Number(originalRows[0].original.product.id)]),
                        children: <p>Выбрано товаров: {[Number(originalRows[0].original.product.id)].length}</p>,
                    },
                });
            }
        },
        [ActionEnum.UNTIE, ActionType.Delete, onDelete, popupDispatch]
    );

    const tooltipContent = useMemo<TooltipItem[]>(
        () => [
            {
                type: 'edit',
                text: 'Перейти в карточку товара',
                action: goToDetailPage,
                isDisable: !catalogAccess.canViewAnyDetail,
            },
            {
                type: 'delete',
                text: 'Отвязать',
                action: goToDeleteRow,
                isDisable: !access.ID.edit,
            },
        ],
        [goToDetailPage, goToDeleteRow, access.ID.edit, catalogAccess.canViewAnyDetail]
    );

    const autoGeneratedColumns = useAutoColumns(meta);
    const columns = useMemo(
        () => [
            getSelectColumn(),
            ...autoGeneratedColumns,
            getSettingsColumn({ columnsToDisable: [], visibleColumns: meta?.default_list, tooltipContent }),
        ],
        [autoGeneratedColumns, meta?.default_list]
    );

    const getTooltipForRow = useCallback(() => tooltipContent || [], [tooltipContent]);
    const renderRow = useCallback(
        ({ children, ...props }: TrProps<any>) => (
            <RowTooltipWrapper
                {...props}
                getTooltipForRow={getTooltipForRow}
                onDoubleClick={() => {
                    push(
                        `${props.row?.original.customListLink}/${props.row?.original.customIdResolver(
                            props.row?.original
                        )}`
                    );
                }}
            >
                {children}
            </RowTooltipWrapper>
        ),
        [getTooltipForRow]
    );
    const table = useTable(
        {
            data: tableData,
            columns,
            meta: {
                tableKey: pathname,
            },
        },
        [sortingPlugin]
    );

    if (isLoading || isIdleYetEnabled)
        return (
            <>
                <div css={{ marginBottom: scale(3) }}>
                    <LoadingSkeleton height={200} />
                </div>
                <LoadingSkeleton
                    count={10}
                    height={40}
                    css={{
                        marginBottom: scale(1),
                    }}
                />
            </>
        );

    return (
        <div className={className}>
            <h4 css={{ ...typography('h3'), marginBottom: scale(2) }}>Привязанные товары</h4>

            <Block css={{ marginBottom: scale(3) }}>
                <AutoFilters
                    initialValues={values}
                    emptyInitialValues={emptyInitialValues}
                    onSubmit={URLHelper}
                    filtersActive={filtersActive}
                    meta={meta}
                    queryPart={{ entity_id: nameplateId }}
                    isLoading={isMetaLoading}
                    onResetFilters={reset}
                    clearInitialValue={clearInitialValue}
                />
            </Block>
            <Block>
                <Block.Body>
                    {renderHeader(table)}
                    <Table instance={table} Tr={renderRow} />

                    {tableData.length === 0 && !isLoading ? (
                        <TableEmpty
                            filtersActive={filtersActive}
                            titleWithFilters="Привязанные товары не найдены. Попробуйте сбросить фильтры."
                            titleWithoutFilters="Нет привязанных товаров"
                            addItems={() => setItemsPerPageCount(10)}
                            onResetFilters={() => {
                                reset();
                                push({
                                    pathname,
                                    query: {
                                        entity_id: nameplateId,
                                    },
                                });
                            }}
                        />
                    ) : (
                        <TableFooter
                            pages={totalPages}
                            itemsPerPageCount={itemsPerPageCount}
                            setItemsPerPageCount={setItemsPerPageCount}
                        />
                    )}
                </Block.Body>
            </Block>

            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </div>
    );
};

export default BindedProducts;
