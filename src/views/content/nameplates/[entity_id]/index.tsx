import {
    CopyButton,
    DescriptionList,
    DescriptionListItem,
    FormField,
    FormFieldWrapper,
    Input,
    getDefaultDates,
    useActionPopup,
} from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { useEffect, useMemo, useRef, useState } from 'react';
import * as Yup from 'yup';

import { useCreateNameplate, useDeleteNameplate, useNameplate, useUpdateNameplate } from '@api/content/nameplates';

import { useError, useSuccess } from '@context/modal';

import LoadingSkeleton from '@controls/LoadingSkeleton';
import Switcher from '@controls/Switcher';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { CREATE_PARAM, ErrorMessages, ModalMessages } from '@scripts/constants';
import { RedirectMethods } from '@scripts/enums';
import { Layout, colors, scale } from '@scripts/gds';
import { isPageNotFound } from '@scripts/helpers';

import { useNameplatesAccess } from '../useNameplatesAccess';
import { AddProductsPopup } from './AddProductsPopup';
import BindedProducts from './BindedProducts';

const NAMEPLATES_URL = '/content/nameplates';

const NameplateInner = ({ isLoading }: { isLoading: boolean }) => {
    const [deferredIsLoading, setDeferredIsLoading] = useState(isLoading);

    useEffect(() => {
        if (!isLoading) {
            setTimeout(() => {
                setDeferredIsLoading(false);
            }, 150);
        }
    }, [isLoading]);

    if (deferredIsLoading) {
        return (
            <LoadingSkeleton
                count={4}
                css={{
                    marginBottom: scale(2),
                }}
                height={scale(6)}
            />
        );
    }

    return (
        <Layout cols={8} gap={scale(2)} css={{ marginBottom: scale(2) }}>
            <Layout.Item col={8}>
                <FormField name="name" label="Название*" />
            </Layout.Item>
            <Layout.Item col={8}>
                <FormField name="code" label="Код*" />
            </Layout.Item>
            <Layout.Item col={8}>
                <FormFieldWrapper name="backgroundColor" label="Цвет фона*">
                    <Input type="color" />
                </FormFieldWrapper>
            </Layout.Item>
            <Layout.Item col={8}>
                <FormFieldWrapper name="textColor" label="Цвет текста*">
                    <Input type="color" />
                </FormFieldWrapper>
            </Layout.Item>
            {/* Пока что не требуется */}
            {/* <Layout.Item col={6}>
                    <FormFieldWrapper name="productIds" label="Товары">
                        <AutocompleteAsync
                            multiple
                            asyncOptionsByValuesFn={async () => []}
                            asyncSearchFn={async () => ({
                                hasMore: false,
                                options: [],
                            })}
                        />
                    </FormFieldWrapper>
                </Layout.Item> */}
            {/* <Layout.Item col={2} align="end">
                    <Button type="button" onClick={() => setAddingProducts(true)}>
                        Добавить товары-склейки
                    </Button>
                </Layout.Item> */}
        </Layout>
    );
};

const ContentNameplate = () => {
    const {
        query: { entity_id },
        push,
        pathname,
    } = useRouter();

    const parsedId = +`${entity_id}`;
    const isCreation = entity_id === CREATE_PARAM;
    const listLink = pathname.split(`[entity_id]`)[0];

    const { data: apiData, error, isFetching: isLoading } = useNameplate(parsedId);
    useError(error);
    const nameplate = apiData?.data;

    const updateNameplate = useUpdateNameplate();
    const deleteNameplate = useDeleteNameplate();
    const createNameplate = useCreateNameplate();

    const { popupState, popupDispatch, ActionPopup, ActionEnum, ActionType } = useActionPopup();

    useSuccess(updateNameplate.isSuccess && ModalMessages.SUCCESS_UPDATE);
    useSuccess(deleteNameplate.isSuccess && ModalMessages.SUCCESS_DELETE);
    useSuccess(createNameplate.isSuccess && ModalMessages.SUCCESS_CREATE);

    useError(updateNameplate.error);
    useError(deleteNameplate.error);
    useError(createNameplate.error);

    const initialValues = useMemo(
        () =>
            isCreation
                ? {
                      name: '',
                      code: '',
                      backgroundColor: '',
                      textColor: '',
                      active: false,
                  }
                : {
                      name: nameplate?.name || '',
                      code: nameplate?.code || '',
                      backgroundColor: nameplate?.background_color || '',
                      textColor: nameplate?.text_color || '',
                      active: nameplate?.is_active || false,
                  },
        [nameplate, isCreation]
    );

    const pageTitle = isCreation ? 'Создать тег' : `Редактировать тег ${parsedId}`;

    const shouldReturnBack = useRef(false);
    const [isAddingProducts, setAddingProducts] = useState(false);

    const { access, isIDForbidden } = useNameplatesAccess();

    const canSubmit = isCreation ? access.ID.create : access.ID.edit;

    const isNotFound = isPageNotFound({ id: parsedId, error, isCreation });

    return (
        <PageWrapper
            title={pageTitle}
            isLoading={isLoading || updateNameplate.isPending || createNameplate.isPending || deleteNameplate.isPending}
            isNotFound={isNotFound}
            isForbidden={isIDForbidden}
        >
            <FormWrapper
                disabled={!canSubmit}
                onSubmit={async (values, { reset }) => {
                    if (isCreation) {
                        const result = await createNameplate.mutateAsync({
                            background_color: values.backgroundColor!,
                            code: values.code!,
                            is_active: values.active!,
                            name: values.name!,
                            text_color: values.textColor!,
                        });

                        if (shouldReturnBack.current) {
                            return {
                                method: RedirectMethods.push,
                                redirectPath: NAMEPLATES_URL,
                            };
                        }

                        return {
                            method: RedirectMethods.push,
                            redirectPath: `${NAMEPLATES_URL}/${result.data.id}`,
                        };
                    }

                    await updateNameplate.mutateAsync({
                        id: parsedId,
                        background_color: values.backgroundColor!,
                        code: values.code!,
                        is_active: values.active!,
                        name: values.name!,
                        text_color: values.textColor!,
                    });

                    if (shouldReturnBack.current) {
                        reset(values);
                        return {
                            method: RedirectMethods.push,
                            redirectPath: NAMEPLATES_URL,
                        };
                    }
                    return {
                        method: RedirectMethods.replace,
                        redirectPath: `${NAMEPLATES_URL}/${parsedId}`,
                    };
                }}
                initialValues={initialValues}
                validationSchema={Yup.object().shape({
                    name: Yup.string().required(ErrorMessages.REQUIRED),
                    code: Yup.string().required(ErrorMessages.REQUIRED),
                    backgroundColor: Yup.string().min(1, ErrorMessages.REQUIRED),
                    textColor: Yup.string().min(1, ErrorMessages.REQUIRED),
                    // productIds: Yup.array().of(Yup.mixed()).min(1, ErrorMessages.MIN_ITEMS(1)),
                })}
                enableReinitialize
            >
                {({ reset }) => (
                    <PageTemplate
                        h1={pageTitle}
                        controls={
                            <PageControls access={access}>
                                <PageControls.Delete
                                    onClick={() => {
                                        popupDispatch({
                                            type: ActionType.Delete,
                                            payload: {
                                                title: `Вы уверены, что хотите удалить тег?`,
                                                popupAction: ActionEnum.DELETE,
                                                onAction: async () => {
                                                    reset();
                                                    await deleteNameplate.mutateAsync({
                                                        id: parsedId,
                                                    });

                                                    push(`/content/nameplates`);
                                                },
                                            },
                                        });
                                    }}
                                />
                                <PageControls.Close
                                    onClick={() =>
                                        push({
                                            pathname: listLink,
                                        })
                                    }
                                />
                                <PageControls.Apply
                                    onClick={() => {
                                        shouldReturnBack.current = false;
                                    }}
                                />
                                <PageControls.Save
                                    onClick={() => {
                                        shouldReturnBack.current = true;
                                    }}
                                />
                            </PageControls>
                        }
                        backlink={{ text: 'К списку тегов', href: '/content/nameplates' }}
                        aside={
                            <>
                                <FormFieldWrapper name="active" label="Активность" disabled={!canSubmit}>
                                    <Switcher>Активность</Switcher>
                                </FormFieldWrapper>

                                <div css={{ marginBottom: scale(1), marginTop: scale(3) }}>
                                    <span css={{ marginRight: scale(1, true), color: colors?.grey800 }}>ID:</span>
                                    <CopyButton>{`${entity_id}`}</CopyButton>
                                </div>

                                <DescriptionList css={{ marginBottom: scale(5) }}>
                                    {getDefaultDates({ ...nameplate }).map(item => (
                                        <DescriptionListItem {...item} key={item.name} />
                                    ))}
                                </DescriptionList>
                            </>
                        }
                    >
                        <NameplateInner isLoading={isLoading} />
                    </PageTemplate>
                )}
            </FormWrapper>
            {!!parsedId && (
                <BindedProducts
                    canSubmit={canSubmit}
                    nameplateId={parsedId}
                    css={{ marginTop: scale(2) }}
                    onAdd={() => setAddingProducts(true)}
                />
            )}
            <AddProductsPopup
                nameplateId={parsedId}
                isOpen={isAddingProducts}
                onClose={() => setAddingProducts(false)}
            />
            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </PageWrapper>
    );
};

export default ContentNameplate;
