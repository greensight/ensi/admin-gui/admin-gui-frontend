import { useActionPopup } from '@ensi-platform/core-components';
import { useCallback, useMemo } from 'react';

import { useDeleteNameplate, useNameplatesMeta, useSearchNameplates } from '@api/content/nameplates';
import { Nameplate } from '@api/content/types/nameplates';

import { useError, useSuccess } from '@context/modal';

import ListBuilder from '@components/ListBuilder';
import { ExtendedRow, TooltipItem } from '@components/Table';

import { ModalMessages } from '@scripts/constants';
import { ActionType } from '@scripts/enums';
import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import { useNameplatesAccess } from './useNameplatesAccess';

const NameplateList = () => {
    const { access } = useNameplatesAccess();
    const { popupState, popupDispatch, ActionPopup, ActionEnum } = useActionPopup();

    const deleteNameplate = useDeleteNameplate();
    useSuccess(deleteNameplate.isSuccess && ModalMessages.SUCCESS_DELETE);
    useError(deleteNameplate.error);

    const goToDetailPage = useGoToDetailPage({ extraConditions: access.ID.view });

    const goToDeleteRow = useCallback(
        (originalRows: ExtendedRow['original'][] | undefined) => {
            if (originalRows) {
                popupDispatch({
                    type: ActionType.Delete,
                    payload: {
                        title: `Вы уверены, что хотите удалить тег #${originalRows[0].original.id}?`,
                        popupAction: ActionEnum.DELETE,
                        onAction: async () => {
                            try {
                                await deleteNameplate.mutateAsync({
                                    id: Number(originalRows[0].original.id)!,
                                });
                            } catch (err) {
                                console.error(err);
                            }
                        },
                    },
                });
            }
        },
        [ActionEnum.DELETE, deleteNameplate, popupDispatch]
    );

    const tooltipContent: TooltipItem[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Редактировать тег',
                action: goToDetailPage,
                isDisable: !access.ID.view,
            },
            {
                type: 'delete',
                text: 'Удалить тег',
                action: goToDeleteRow,
                isDisable: !access.ID.delete,
            },
        ],
        [access.ID.delete, access.ID.view, goToDeleteRow, goToDetailPage]
    );

    return (
        <ListBuilder<Nameplate>
            access={access}
            searchHook={useSearchNameplates}
            metaHook={useNameplatesMeta}
            isLoading={deleteNameplate.isPending}
            tooltipItems={tooltipContent}
            title="Список товарных тегов"
        >
            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </ListBuilder>
    );
};

export default NameplateList;
