import { Block, Form, FormField, FormReset } from '@ensi-platform/core-components';
import { createColumnHelper } from '@tanstack/react-table';
import { useRouter } from 'next/router';
import { useMemo } from 'react';
import type { FieldValues } from 'react-hook-form';

import { useMenus } from '@api/content/menu';
import { MenuFilter, Menu as MenuType } from '@api/content/types/menus';

import Pagination from '@controls/Pagination';

import PageWrapper from '@components/PageWrapper';
import Table, { TableColumnDefAny, useTable } from '@components/Table';

import { LIMIT_PAGE } from '@scripts/constants';
import { Button, Layout, scale, typography } from '@scripts/gds';
import { getTotal, getTotalPages } from '@scripts/helpers';
import { useActivePage, useFiltersHelper } from '@scripts/hooks';

interface TableData extends Pick<MenuType, 'id' | 'code'> {
    title: string[];
}

const columnHelper = createColumnHelper<MenuType>();

export async function getServerSideProps() {
    return {
        props: {},
    };
}

type FilterForm = {
    [key in keyof MenuFilter]?: string;
};

const Filter = ({
    onSubmit,
    onReset,
    initialValues,
    emptyValues,
}: {
    onSubmit: (vals: FieldValues) => void;
    onReset: (vals: FieldValues) => any;
    initialValues: FieldValues;
    emptyValues: FieldValues;
}) => (
    <Form onSubmit={onSubmit} initialValues={initialValues} onReset={onReset} css={{ marginBottom: scale(2) }}>
        <Layout cols={4} css={{ marginBottom: scale(2) }}>
            <Layout.Item col={1}>
                <FormField name="id" label="ID" autoComplete="off" />
            </Layout.Item>
            <Layout.Item col={1}>
                <FormField name="code" label="Код" autoComplete="off" />
            </Layout.Item>
        </Layout>
        <FormReset theme="fill" css={{ marginRight: scale(2) }} initialValues={emptyValues}>
            Очистить
        </FormReset>
        <Button type="submit">Применить</Button>
    </Form>
);

const Menu = () => {
    const initFilter: FilterForm = {
        id: '',
        code: '',
    };

    const { push, pathname } = useRouter();
    const { initialValues, URLHelper } = useFiltersHelper(initFilter);

    const activePage = useActivePage();

    const {
        data: apiMenus,
        isInitialLoading: isIdle,
        isFetching: isLoading,
    } = useMenus({
        sort: ['id'],
        include: ['items'],
        filter: {
            id: initialValues.id.length ? +initialValues.id : undefined,
            code: initialValues.code.length ? initialValues.code : undefined,
        },
        pagination: { type: 'offset', limit: LIMIT_PAGE, offset: (activePage - 1) * LIMIT_PAGE },
    });

    const totalPages = getTotalPages(apiMenus);
    const total = getTotal(apiMenus);

    const data: TableData[] = useMemo(
        () =>
            apiMenus?.data?.map(e => ({
                id: e.id,
                code: e.code,
                title: [e.name, `/content/menu/${e.id}`],
            })) || [],
        [apiMenus?.data]
    );

    const columns: TableColumnDefAny[] = useMemo(
        () => [
            columnHelper.accessor('id', {
                header: 'ID',
                cell: props => props.getValue(),
            }),
            columnHelper.accessor('code', {
                header: 'Код',
                cell: props => props.getValue(),
            }),
            // columnHelper.accessor('title', {
            //     header: 'Название',
            //     cell: props => props.getValue(),
            //     // getProps: () => ({ type: 'link' }),
            // }),
        ],
        []
    );

    const table = useTable<TableData>(
        {
            data,
            columns,
            meta: {
                tableKey: `menu`,
            },
        },
        []
    );

    return (
        <PageWrapper h1="Меню" isLoading={isLoading || isIdle}>
            <Block css={{ marginTop: scale(2) }}>
                <Block.Body>
                    <Filter
                        initialValues={initialValues}
                        emptyValues={initFilter}
                        onReset={() => push(pathname)}
                        onSubmit={val => URLHelper(val)}
                    />
                    <div css={{ ...typography('bodySm'), marginBottom: scale(2) }}>Всего найдено меню: {total}.</div>
                    {data.length < 1 ? (
                        <p css={typography('bodyMd')}>Ни одного меню не найдено.</p>
                    ) : (
                        <Table instance={table} />
                    )}
                    <Pagination pages={totalPages} css={{ marginTop: scale(2) }} />
                </Block.Body>
            </Block>
        </PageWrapper>
    );
};

export default Menu;
