import {
    CopyButton,
    DescriptionList,
    DescriptionListItem,
    FormField,
    FormFieldWrapper,
    Input,
    getDefaultDates,
    useActionPopup,
} from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { useMemo, useRef } from 'react';
import * as Yup from 'yup';

import { useCreatePage, usePageDelete, usePageDetail, usePageUpdate } from '@api/content';
import { PageMutate } from '@api/content/types/pages';

import { useError, useSuccess } from '@context/modal';

import CalendarRange from '@controls/CalendarRange';
import Switcher from '@controls/Switcher';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';
import TextEditor from '@components/TextEditor';

import { CREATE_PARAM, ErrorMessages, ModalMessages } from '@scripts/constants';
import { RedirectMethods } from '@scripts/enums';
import { Layout, scale } from '@scripts/gds';
import { isPageNotFound, toISOString } from '@scripts/helpers';

import { useContentPagesAccess } from '../useContentPagesAccess';

const PageDetail = () => {
    const { push, pathname, query } = useRouter();
    const id = Array.isArray(query?.id) ? query.id[0]! : query.id!;
    const isCreation = id === CREATE_PARAM;
    const listLink = pathname.split(`[id]`)[0];

    const redirectAfterSave = useRef(false);

    const {
        canViewDetailPage,
        canEditContentPage,
        canEditContentPageActive,
        canDeleteContentPage,
        canCreateContentPage,
        isIDForbidden,
    } = useContentPagesAccess();

    const { popupState, popupDispatch, ActionPopup, ActionEnum, ActionType } = useActionPopup();

    const { data, isFetching: isLoading, error } = usePageDetail(!isCreation ? id : undefined);
    const page = useMemo(() => data?.data, [data?.data]);

    const createPage = useCreatePage();
    const updatePage = usePageUpdate();
    const deletePage = usePageDelete();

    useError(error);
    useError(createPage.error);
    useError(updatePage.error);
    useError(deletePage.error);

    useSuccess(createPage.status === 'success' ? ModalMessages.SUCCESS_SAVE : '');
    useSuccess(updatePage.status === 'success' ? ModalMessages.SUCCESS_UPDATE : '');
    useSuccess(deletePage.status === 'success' ? ModalMessages.SUCCESS_DELETE : '');

    const initialValues = useMemo<PageMutate>(
        () => ({
            name: page?.name || '',
            is_active: typeof page?.is_active === 'boolean' ? page.is_active : false,
            slug: page?.slug || '',
            active_from: page?.active_from ? new Date(page.active_from).getTime() : null,
            active_to: page?.active_to ? new Date(page.active_to).getTime() : null,
            content: page?.content || '',
        }),
        [page]
    );

    const canEditForm = isCreation ? canCreateContentPage : canEditContentPage;
    const canEditActivity = isCreation ? canCreateContentPage : canEditContentPageActive || canEditContentPage;

    const isNotFound = !isCreation && isPageNotFound({ id, error, isCreation });

    return (
        <PageWrapper
            isLoading={isLoading || createPage.isPending || updatePage.isPending || deletePage.isPending}
            isNotFound={isNotFound}
            isForbidden={isIDForbidden}
        >
            <FormWrapper
                initialValues={initialValues}
                onSubmit={async values => {
                    if (isCreation) {
                        const {
                            data: { id: createdId },
                        } = await createPage.mutateAsync({
                            ...values,
                            active_from: values.active_from ? toISOString(new Date(values.active_from)) : null,
                            active_to: values.active_to ? toISOString(new Date(values.active_to)) : null,
                        });

                        return {
                            method: RedirectMethods.push,
                            redirectPath: redirectAfterSave.current ? listLink : `${listLink}${createdId}`,
                        };
                    }

                    await updatePage.mutateAsync({
                        ...values,
                        active_from: values.active_from ? toISOString(new Date(values.active_from)) : null,
                        active_to: values.active_to ? toISOString(new Date(values.active_to)) : null,
                        id: +id,
                    });

                    return {
                        method: RedirectMethods.push,
                        redirectPath: redirectAfterSave.current ? listLink : `${listLink}${id}`,
                    };
                }}
                validationSchema={Yup.object().shape({
                    name: Yup.string().required(ErrorMessages.REQUIRED),
                    slug: Yup.string().required(ErrorMessages.REQUIRED),
                    content: Yup.string().required(ErrorMessages.REQUIRED),
                    active_from: Yup.number()
                        .transform(val => (Number.isNaN(val) ? undefined : val))
                        .nullable(),
                    active_to: Yup.number()
                        .transform(val => (Number.isNaN(val) ? undefined : val))
                        .nullable(),
                })}
                enableReinitialize
                disabled={!canEditForm}
            >
                {({ reset }) => (
                    <PageTemplate
                        h1={isCreation ? 'Новая страница' : page?.name}
                        backlink={{ text: 'Назад', href: listLink }}
                        controls={
                            <PageControls
                                access={{
                                    ID: {
                                        view: canViewDetailPage,
                                        delete: canDeleteContentPage,
                                        edit: canEditContentPage,
                                        create: true,
                                    },
                                    LIST: {
                                        view: true,
                                    },
                                }}
                                gap={scale(1)}
                            >
                                <PageControls.Delete
                                    onClick={() => {
                                        popupDispatch({
                                            type: ActionType.Delete,
                                            payload: {
                                                title: `Вы уверены, что хотите удалить страницу ${page?.name}?`,
                                                popupAction: ActionEnum.DELETE,
                                                onAction: async () => {
                                                    if (!id) return;
                                                    reset();
                                                    await deletePage.mutateAsync(+id);
                                                    push({ pathname: listLink });
                                                },
                                            },
                                        });
                                    }}
                                />

                                <PageControls.Close
                                    onClick={() =>
                                        push({
                                            pathname: listLink,
                                        })
                                    }
                                />
                                <PageControls.Apply
                                    onClick={() => {
                                        redirectAfterSave.current = false;
                                    }}
                                />
                                <PageControls.Save
                                    onClick={() => {
                                        redirectAfterSave.current = true;
                                    }}
                                />
                            </PageControls>
                        }
                        aside={
                            <>
                                <FormFieldWrapper
                                    name="is_active"
                                    disabled={!canEditActivity}
                                    css={{ marginBottom: scale(3) }}
                                >
                                    <Switcher>Активность</Switcher>
                                </FormFieldWrapper>
                                <DescriptionList css={{ marginBottom: scale(5) }}>
                                    <DescriptionListItem
                                        name="ID:"
                                        value={!isCreation ? <CopyButton>{`${id}`}</CopyButton> : '-'}
                                    />
                                    {getDefaultDates({ ...page }).map(item => (
                                        <DescriptionListItem {...item} key={item.name} />
                                    ))}
                                </DescriptionList>
                            </>
                        }
                    >
                        <Layout cols={2} gap={scale(3)} align="end">
                            <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                <FormFieldWrapper name="name" label="Наименование*">
                                    <Input />
                                </FormFieldWrapper>
                            </Layout.Item>
                            <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                <FormField name="slug" label="Ссылка*" />
                            </Layout.Item>
                            <Layout.Item col={2}>
                                <CalendarRange
                                    label="Период публикации"
                                    nameFrom="active_from"
                                    nameTo="active_to"
                                    disabled={!canEditForm}
                                />
                            </Layout.Item>
                            <Layout.Item col={2}>
                                <FormFieldWrapper label="Контент*" name="content" css={{ marginBottom: scale(2) }}>
                                    <TextEditor />
                                </FormFieldWrapper>
                            </Layout.Item>
                        </Layout>
                    </PageTemplate>
                )}
            </FormWrapper>

            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </PageWrapper>
    );
};

export default PageDetail;
