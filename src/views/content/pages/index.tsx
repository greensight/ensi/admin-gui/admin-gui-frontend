import { useMemo } from 'react';

import { usePages, usePagesMeta } from '@api/content';
import { Page } from '@api/content/types/pages';

import ListBuilder from '@components/ListBuilder';
import { TooltipItem } from '@components/Table';

import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import { useContentPagesAccess } from './useContentPagesAccess';

const Pages = () => {
    const { access } = useContentPagesAccess();
    const goToDetailPage = useGoToDetailPage();

    const tooltipContent: TooltipItem[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Редактировать страницу',
                action: goToDetailPage,
                isDisable: !access.ID.view,
            },
        ],
        [access.ID.view, goToDetailPage]
    );

    return (
        <ListBuilder<Page>
            access={access}
            searchHook={usePages}
            metaHook={usePagesMeta}
            tooltipItems={tooltipContent}
            title="Список Страниц"
        />
    );
};

export default Pages;
