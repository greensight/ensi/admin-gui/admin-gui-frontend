import { useMemo } from 'react';

import { useCurrentUser } from '@api/auth';

import { useAccess } from '@scripts/hooks';
import { useIDForbidden } from '@scripts/hooks/useIDForbidden';

export const accessMatrix = {
    LIST: {
        view: 901,
    },
    ID: {
        view: 902,
        edit: 903,
        editActive: 904,
        create: 905,
        delete: 906,
    },
};

export const useContentPagesAccess = () => {
    const { data: userData } = useCurrentUser();

    const viewListingAndFilters = 901;
    const viewDetailPage = 902;
    const editContentPageData = 903;
    const editContentPageActive = 904;
    const createContentPage = 905;
    const deleteContentPage = 906;

    const access = useAccess(accessMatrix);
    const isIDForbidden = useIDForbidden(access);

    const canViewListingAndFilters = useMemo(
        () => !!userData?.data.rights_access.includes(viewListingAndFilters),
        [userData?.data.rights_access]
    );
    const canViewDetailPage = useMemo(
        () => !!userData?.data.rights_access.includes(viewDetailPage),
        [userData?.data.rights_access]
    );
    const canEditContentPage = useMemo(
        () => !!userData?.data.rights_access.includes(editContentPageData),
        [userData?.data.rights_access]
    );
    const canEditContentPageActive = useMemo(
        () => !!userData?.data.rights_access.includes(editContentPageActive),
        [userData?.data.rights_access]
    );
    const canCreateContentPage = useMemo(
        () => !!userData?.data.rights_access.includes(createContentPage),
        [userData?.data.rights_access]
    );
    const canDeleteContentPage = useMemo(
        () => !!userData?.data.rights_access.includes(deleteContentPage),
        [userData?.data.rights_access]
    );

    return {
        canViewListingAndFilters,
        canViewDetailPage,
        canEditContentPage,
        canEditContentPageActive,
        canCreateContentPage,
        canDeleteContentPage,
        access,
        isIDForbidden,
    };
};
