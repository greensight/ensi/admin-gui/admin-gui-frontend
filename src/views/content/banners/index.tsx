import { useMemo } from 'react';

import { useCmsBannersMeta, useCmsBannersSearch, useDeleteCmsBanner } from '@api/content';
import { Banner } from '@api/content/types/banners';

import { useError } from '@context/modal';

import ListBuilder from '@components/ListBuilder';
import { TooltipItem } from '@components/Table';

import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import { useBannersAccess } from './useBannersAccess';

const Banners = () => {
    const { access } = useBannersAccess();

    const deleteBanner = useDeleteCmsBanner();
    useError(deleteBanner.error);

    const goToDetailPage = useGoToDetailPage();

    const tooltipContent = useMemo<TooltipItem[]>(
        () => [
            {
                type: 'edit',
                text: 'Редактировать баннер',
                action: goToDetailPage,
                isDisable: !access.ID.view,
            },
        ],
        [access.ID.view, goToDetailPage]
    );

    return (
        <ListBuilder<Banner>
            access={access}
            searchHook={useCmsBannersSearch}
            metaHook={useCmsBannersMeta}
            isLoading={deleteBanner.isPending}
            tooltipItems={tooltipContent}
            title="Список баннеров"
        />
    );
};

export default Banners;
