import { useAccess } from '@scripts/hooks';
import { useIDForbidden } from '@scripts/hooks/useIDForbidden';

export const AccessMatrix = {
    LIST: {
        view: 1401,
    },
    ID: {
        view: 1402,
        edit: 1403,
        create: 1404,
        delete: 1405,
    },
};

export const useBannersAccess = () => {
    const access = useAccess(AccessMatrix);
    const isIDForbidden = useIDForbidden(access);

    return { access, isIDForbidden };
};
