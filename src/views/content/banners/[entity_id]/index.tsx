import {
    CopyButton,
    DescriptionList,
    DescriptionListItem,
    FormField,
    FormFieldWrapper,
    Select,
    SelectItem,
    useActionPopup,
    useFormContext,
} from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { useMemo, useRef } from 'react';
import * as Yup from 'yup';

import {
    useCmsBannerTypesSearch,
    useCmsBannersSearchOne,
    useCreateCmsBanner,
    useDeleteCmsBanner,
    usePutCmsBanner,
} from '@api/content/banners';

import { useError, useSuccess } from '@context/modal';

import { Controls } from '@views/content/seo/[entity_id]/components/Controls';

import Dropzone from '@controls/Dropzone';
import LoadingSkeleton from '@controls/LoadingSkeleton';
import Switcher from '@controls/Switcher';

import FormWrapper from '@components/FormWrapper';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { CREATE_PARAM, ErrorMessages, FileSizes, ImageAcceptTypes, ModalMessages } from '@scripts/constants';
import { BannerButtonLocation, BannerButtonType, RedirectMethods } from '@scripts/enums';
import { Layout, scale } from '@scripts/gds';
import { isPageNotFound } from '@scripts/helpers';
import { useDeferredLoading } from '@scripts/hooks/useDeferredLoading';

import { useBannersAccess } from '../useBannersAccess';
import { preparedButtonLocations, preparedButtonTypes } from './scripts/constants';
import { useImages } from './scripts/hooks/useImages';

interface BannerValues {
    title: string;
    url: string;
    active: boolean;
    sort: string | number;
    type: string | number;
    link: string;
    hasBtn: boolean;
    btnText: string;
    btnType: string;
    btnPosition: string;

    mobileImg: File[];
    desktopImg: File[];
}

const BannerInner = ({ isLoading, isImagesLoading }: { isLoading: boolean; isImagesLoading: boolean }) => {
    const { watch } = useFormContext();
    const hasBtn = watch('hasBtn');

    const { data: apiTypes } = useCmsBannerTypesSearch({});
    const preparedTypes = useMemo(
        () =>
            apiTypes?.data.reduce<SelectItem[]>(
                (acc, type) => (type.active ? [...acc, { label: type.name!, value: type.id }] : acc),
                []
            ) || [],
        [apiTypes]
    );

    const deferredIsLoading = useDeferredLoading(isLoading);

    if (deferredIsLoading) {
        return (
            <LoadingSkeleton
                count={5}
                css={{
                    marginBottom: scale(2),
                }}
                height={scale(6)}
            />
        );
    }

    return (
        <Layout cols={1} gap={scale(2)} css={{ marginBottom: scale(2) }}>
            <FormField name="title" label="Наимeнование" />
            <FormFieldWrapper label="Изображение" name="desktopImg">
                <Dropzone
                    accept={ImageAcceptTypes}
                    maxFiles={1}
                    maxSize={FileSizes.KB256}
                    multiple={false}
                    isLoading={isImagesLoading}
                />
            </FormFieldWrapper>
            <FormFieldWrapper label="Изображение для адаптива" name="mobileImg">
                <Dropzone
                    accept={ImageAcceptTypes}
                    maxFiles={1}
                    maxSize={FileSizes.KB256}
                    multiple={false}
                    isLoading={isImagesLoading}
                />
            </FormFieldWrapper>
            <FormFieldWrapper label="Тип" name="type">
                <Select options={preparedTypes} hideClearButton />
            </FormFieldWrapper>
            <FormField name="url" label="Ссылка" />
            <FormField name="sort" label="Сортировка" type="number" inputMode="numeric" />
            <FormFieldWrapper name="hasBtn" css={{ marginBottom: scale(2) }}>
                <Switcher>С кнопкой</Switcher>
            </FormFieldWrapper>
            {hasBtn ? (
                <>
                    <FormField name="link" label="Ссылка" />
                    <FormField name="btnText" label="Текст кнопки" />
                    <FormFieldWrapper name="btnType" label="Тип кнопки">
                        <Select options={preparedButtonTypes} hideClearButton />
                    </FormFieldWrapper>
                    <FormFieldWrapper name="btnPosition" label="Местоположение кнопки">
                        <Select options={preparedButtonLocations} hideClearButton />
                    </FormFieldWrapper>
                </>
            ) : null}
        </Layout>
    );
};

const Banner = () => {
    const {
        query: { entity_id },
        push,
    } = useRouter();

    const isCreation = entity_id === CREATE_PARAM;
    const parsedId = isCreation ? 0 : +`${entity_id}`;

    const { popupState, popupDispatch, ActionPopup, ActionEnum, ActionType } = useActionPopup();

    const {
        data: apiData,
        error,
        isFetching: isLoading,
    } = useCmsBannersSearchOne({
        filter: {
            id: parsedId,
        },
    });

    useError(error);

    const banner = apiData?.data;

    const updateBanner = usePutCmsBanner();
    const deleteBanner = useDeleteCmsBanner();
    const createBanner = useCreateCmsBanner();

    useSuccess(updateBanner.isSuccess && ModalMessages.SUCCESS_UPDATE);
    useSuccess(deleteBanner.isSuccess && ModalMessages.SUCCESS_DELETE);
    useSuccess(createBanner.isSuccess && ModalMessages.SUCCESS_CREATE);

    const {
        initialImages,
        processImages,
        isDownloading,
        isLoading: isImagesLoading,
    } = useImages({ banner, isCreation });

    const initialValues = useMemo<BannerValues>(
        () =>
            isCreation
                ? {
                      title: '',
                      url: '',
                      active: false,
                      type: '',
                      link: '',
                      mobileUrl: '',
                      desktopUrl: '',
                      mobileImg: [],
                      desktopImg: [],
                      hasBtn: false,
                      btnText: '',
                      btnType: '',
                      btnPosition: '',
                      sort: '',
                  }
                : {
                      url: banner?.url || '',
                      title: banner?.name || '',
                      active: banner?.is_active || false,
                      type: banner?.type_id || '',
                      link: banner?.button?.url || '',
                      mobileUrl: banner?.mobile_image || '',
                      desktopUrl: banner?.desktop_image || '',
                      desktopImg: initialImages.desktop ? [initialImages.desktop] : [],
                      mobileImg: initialImages.mobile ? [initialImages.mobile] : [],
                      hasBtn: !!banner?.button,
                      btnText: banner?.button?.text || '',
                      btnType: banner?.button?.type || '',
                      btnPosition: banner?.button?.location || '',
                      sort: banner?.sort || '',
                  },
        [banner, initialImages.desktop, initialImages.mobile, isCreation]
    );

    const pageTitle = isCreation ? 'Создать баннер' : `Редактировать баннер ${parsedId}`;

    const shouldReturnBack = useRef(false);

    const { access, isIDForbidden } = useBannersAccess();
    const canEdit = isCreation ? access.ID.create : access.ID.edit;

    const isNotFound = isPageNotFound({ id: parsedId, error, isCreation });

    return (
        <PageWrapper
            title={pageTitle}
            isLoading={isLoading || updateBanner.isPending || createBanner.isPending || deleteBanner.isPending}
            isNotFound={isNotFound}
            isForbidden={isIDForbidden}
        >
            <FormWrapper
                initialValues={initialValues}
                enableReinitialize
                disabled={!canEdit}
                onSubmit={async values => {
                    if (isCreation) {
                        const result = await createBanner.mutateAsync({
                            name: values.title,
                            url: values.url,
                            sort: +values.sort,
                            is_active: values.active,
                            type_id: +values.type,
                            ...(values.hasBtn && {
                                button: {
                                    url: values.link,
                                    text: values.btnText,
                                    location: values.btnPosition as BannerButtonLocation,
                                    type: values.btnType as BannerButtonType,
                                },
                            }),
                        });

                        await processImages(result.data.id, values);

                        return {
                            redirectPath: shouldReturnBack.current
                                ? `/content/banners`
                                : `/content/banners/${result.data.id}`,
                            method: RedirectMethods.push,
                        };
                    }

                    await updateBanner.mutateAsync({
                        id: parsedId,
                        url: values.url,
                        sort: +values.sort,
                        name: values.title,
                        is_active: values.active,
                        type_id: +values.type,
                        ...(values.hasBtn && {
                            button: {
                                url: values.link,
                                text: values.btnText,
                                location: values.btnPosition as BannerButtonLocation,
                                type: values.btnType as BannerButtonType,
                            },
                        }),
                    });

                    await processImages(parsedId, values);

                    return {
                        redirectPath: shouldReturnBack.current ? `/content/banners` : `/content/banners/${parsedId}`,
                        method: RedirectMethods.replace,
                        valuesToReset: { ...values, desktopImage: [], mobileImage: [] },
                    };
                }}
                validationSchema={Yup.object().shape({
                    title: Yup.string().required(ErrorMessages.REQUIRED),
                    desktopImg: Yup.array().of(Yup.mixed()).min(1, ErrorMessages.REQUIRED),
                    sort: Yup.number()
                        .transform(val => (Number.isNaN(val) ? undefined : val))
                        .required(ErrorMessages.REQUIRED),
                    type: Yup.string().required(ErrorMessages.REQUIRED),
                    link: Yup.string().when('hasBtn', ([hasBtn], schema) =>
                        hasBtn ? schema.required(ErrorMessages.REQUIRED) : schema
                    ),
                    btnText: Yup.string().when('hasBtn', ([hasBtn], schema) =>
                        hasBtn ? schema.required(ErrorMessages.REQUIRED) : schema
                    ),
                    btnType: Yup.string().when('hasBtn', ([hasBtn], schema) =>
                        hasBtn ? schema.required(ErrorMessages.REQUIRED) : schema
                    ),
                    btnPosition: Yup.string().when('hasBtn', ([hasBtn], schema) =>
                        hasBtn ? schema.required(ErrorMessages.REQUIRED) : schema
                    ),
                })}
            >
                {({ reset }) => (
                    <PageTemplate
                        h1={pageTitle}
                        controls={
                            <Controls
                                access={access}
                                onDelete={() =>
                                    popupDispatch({
                                        type: ActionType.Delete,
                                        payload: {
                                            title: `Вы уверены, что хотите удалить баннер?`,
                                            popupAction: ActionEnum.DELETE,
                                            onAction: async () => {
                                                reset();
                                                await deleteBanner.mutateAsync({
                                                    id: parsedId,
                                                });

                                                push(`/content/banners`);
                                            },
                                        },
                                    })
                                }
                                onApply={() => {
                                    shouldReturnBack.current = false;
                                }}
                                onSave={() => {
                                    shouldReturnBack.current = true;
                                }}
                            />
                        }
                        backlink={{ text: 'К списку баннеров', href: '/content/banners' }}
                        aside={
                            <DescriptionList>
                                <DescriptionListItem
                                    name="Активность"
                                    value={
                                        <FormFieldWrapper name="active" label="Активность" disabled={!canEdit}>
                                            <Switcher>Активность</Switcher>
                                        </FormFieldWrapper>
                                    }
                                />
                                {!isCreation && (
                                    <DescriptionListItem name="ID" value={<CopyButton>{`${entity_id}`}</CopyButton>} />
                                )}
                                {!isCreation && (
                                    <DescriptionListItem
                                        name="Код"
                                        value={<CopyButton>{`${banner?.code}`}</CopyButton>}
                                    />
                                )}
                                <DescriptionListItem name="Дата создания" value={banner?.created_at} type="date" />
                                <DescriptionListItem name="Дата обновления" value={banner?.updated_at} type="date" />
                            </DescriptionList>
                        }
                    >
                        <BannerInner isLoading={isLoading} isImagesLoading={isDownloading || isImagesLoading} />
                    </PageTemplate>
                )}
            </FormWrapper>
            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </PageWrapper>
    );
};

export default Banner;
