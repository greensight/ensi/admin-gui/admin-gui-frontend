import { BannerButtonLocation, BannerButtonType } from '@scripts/enums';
import { prepareEnumForSelect } from '@scripts/helpers';

export const BUTTON_TYPE_NAMES: {
    [key in BannerButtonType]: string;
} = {
    [BannerButtonType.BLACK]: 'Черная',
    [BannerButtonType.OUTLINE_BLACK]: 'Черная обводка',
    [BannerButtonType.OUTLINE_WHITE]: 'Белая обводка',
    [BannerButtonType.WHITE]: 'Белая',
};

export const BUTTON_LOCATION_NAMES: {
    [key in BannerButtonLocation]: string;
} = {
    [BannerButtonLocation.BOTTOM]: 'Снизу',
    [BannerButtonLocation.BOTTOM_LEFT]: 'Снизу слева',
    [BannerButtonLocation.BOTTOM_RIGHT]: 'Снизу справа',
    [BannerButtonLocation.LEFT]: 'Слева',
    [BannerButtonLocation.RIGHT]: 'Справа',
    [BannerButtonLocation.TOP]: 'Сверху',
    [BannerButtonLocation.TOP_LEFT]: 'Сверху слева',
    [BannerButtonLocation.TOP_RIGHT]: 'Сверху справа',
};

export const preparedButtonTypes = prepareEnumForSelect(BUTTON_TYPE_NAMES);
export const preparedButtonLocations = prepareEnumForSelect(BUTTON_LOCATION_NAMES);
