import { useCallback, useEffect, useMemo, useState } from 'react';

import { CommonResponse } from '@api/common/types';
import { useCmsBannerDeleteFile, useCmsBannerUploadFile } from '@api/content';
import { Banner } from '@api/content/types/banners';

import { downloadFile } from '@controls/Dropzone/utils';

interface InitialImageState {
    desktop?: File;
    mobile?: File;
}

interface useImagesProps {
    banner?: Banner;
    isCreation: boolean;
}

interface ValuesWithImages {
    mobileImg: File[];
    desktopImg: File[];
}

type ImageAction = 'upload' | 'delete' | 'noop';

const bannerImageTypes = ['desktop', 'mobile'] as const;
type ImageType = (typeof bannerImageTypes)[number];

export const useImages = ({ isCreation, banner }: useImagesProps) => {
    const [initialImages, setInitialImages] = useState<InitialImageState>({});
    const [isDownloading, setDownloading] = useState(false);

    const download = useCallback((path: string, reduce: (old: InitialImageState, image: File) => InitialImageState) => {
        setDownloading(true);
        downloadFile(path, path)
            .then(image => {
                setInitialImages(old => {
                    const result = reduce(old, image!);
                    if (result === old) return { ...result };
                    return result;
                });
            })
            .finally(() => setDownloading(false));
    }, []);

    useEffect(() => {
        if (!banner?.desktop_image) return;

        setInitialImages(old => {
            old.desktop = undefined;

            return { ...old };
        });

        download(banner?.desktop_image, (imageState, img) => {
            imageState.desktop = img;
            return imageState;
        });
    }, [banner?.desktop_image, download]);

    useEffect(() => {
        setInitialImages(old => ({ ...old, mobile: undefined }));
        if (!banner?.mobile_image) return;

        download(banner?.mobile_image, (imageState, img) => {
            imageState.mobile = img;
            return imageState;
        });
    }, [banner?.mobile_image, download]);

    const processImageType = useCallback(
        (values: ValuesWithImages, image: ImageType): [ImageAction, File | undefined] => {
            const imageArr = values[`${image}Img`];
            const img = imageArr[0];

            // При создании всегда нужно загружать
            if (isCreation) return ['upload', img];

            // Нужно загружать, если еще не было картинки.
            if (!initialImages[image]) return ['upload', img];

            // Если картинка не выбрана, подразумеваем удаление
            if (!imageArr.length) return ['delete', img];

            // Картинка не изменилась
            if (initialImages[image]!.name === imageArr[0].name) {
                return ['noop', img];
            }

            // Картинка свежая => замена
            if ('path' in imageArr[0]) return ['upload', img];

            return ['noop', undefined];
        },
        [initialImages, isCreation]
    );

    const upload = useCmsBannerUploadFile();
    const remove = useCmsBannerDeleteFile();

    const uploadFile = useCallback(
        async (bannerId: number, type: ImageType, file?: File) => {
            if (!file) return;

            const formData = new FormData();
            formData.append('type', type);
            formData.append('file', file);
            return upload.mutateAsync({
                id: bannerId,
                formData,
            });
        },
        [upload]
    );

    const removeFile = useCallback(
        async (bannerId: number, type: ImageType) => remove.mutateAsync({ id: bannerId, type }),
        [remove]
    );

    const actionHandlers: Record<ImageAction, (bannerId: number, type: ImageType, file?: File) => Promise<any>> =
        useMemo(
            () => ({
                delete: removeFile,
                noop: () => Promise.resolve(),
                upload: uploadFile,
            }),
            [removeFile, uploadFile]
        );

    const processImages = useCallback(
        async (bannerId: number, values: ValuesWithImages) => {
            const tasks = bannerImageTypes.map(async e => {
                const [action, file] = processImageType(values, e);

                return {
                    type: e,
                    result: (await actionHandlers[action](bannerId, e, file)) as undefined | CommonResponse<any>,
                };
            });

            return Promise.all(tasks);
        },
        [actionHandlers, processImageType]
    );

    return { initialImages, processImages, isLoading: upload.isPending || remove.isPending, isDownloading };
};
