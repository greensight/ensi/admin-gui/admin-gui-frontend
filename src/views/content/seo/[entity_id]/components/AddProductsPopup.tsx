import { Layout, Popup, PopupContent, PopupFooter, PopupHeader } from '@ensi-platform/core-components';
import { Row } from '@tanstack/react-table';
import { useRouter } from 'next/router';
import { useCallback, useMemo } from 'react';

import { Product, useProducts, useProductsMeta } from '@api/catalog';
import { useAddSeoTemplateProducts, useSearchSeoTemplateProducts } from '@api/content/seo';
import { SeoTemplate } from '@api/content/types/seo';

import { useError, useSuccess } from '@context/modal';

import { useCatalogAccess } from '@views/products/catalog/scripts/hooks';

import LoadWrapper from '@controls/LoadWrapper';

import AutoFilters from '@components/AutoFilters';
import Table, { useSorting, useTable } from '@components/Table';
import { getSelectColumn, getSettingsColumn } from '@components/Table/columns';
import { TableEmpty, TableFooter, TableHeader } from '@components/Table/components';

import { ITEMS_PER_PRODUCTS_PAGE, ModalMessages } from '@scripts/constants';
import { Button, scale } from '@scripts/gds';
import { declOfNum, getPagination, getTotal, getTotalPages } from '@scripts/helpers';
import { useRedirectToNotEmptyPage, useTableList } from '@scripts/hooks';
import { useAutoColumns, useAutoFilters, useAutoTableData } from '@scripts/hooks/autoTable';

export const AddProductsPopup = ({
    isOpen,
    onClose,
    template,
}: {
    isOpen: boolean;
    onClose: () => void;
    template?: SeoTemplate;
}) => {
    const { query, push, pathname } = useRouter();

    const { data: apiTemplateProducts } = useSearchSeoTemplateProducts(
        {
            filter: {
                template_id: template?.id!,
            },
            pagination: getPagination(1, -1),
        },
        isOpen && !!template?.id!
    );

    const { data: metaData, error: metaError } = useProductsMeta(isOpen);
    const meta = metaData?.data;

    const { canViewAnyDetail: canViewDetailPage } = useCatalogAccess();

    const {
        metaField,
        values,
        filtersActive,
        URLHelper,
        reset,
        searchRequestFilter,
        searchRequestIncludes,
        emptyInitialValues,
        codesToSortKeys,
        clearInitialValue,
    } = useAutoFilters(meta, {
        isSaveToUrl: false,
    });

    const pageKey = 'products-page';

    const [{ backendSorting }, sortingPlugin] = useSorting<Product>(pageKey, meta?.fields);

    const { activePage, itemsPerPageCount, setItemsPerPageCount } = useTableList({
        defaultSort: meta?.default_sort,
        defaultPerPage: ITEMS_PER_PRODUCTS_PAGE,
        codesToSortKeys,
        pageKey,
    });

    const resetFilters = () => {
        reset();

        push({ pathname, query: { entity_id: query.entity_id } });
    };

    const uniqueExistingProductIds = useMemo(() => {
        const result: number[] = [];

        apiTemplateProducts?.data?.forEach(e => {
            if (!result.includes(e.product_id!)) {
                result.push(e.product_id!);
            }
        });

        return result;
    }, [apiTemplateProducts?.data]);

    const {
        data,
        isFetching: isLoading,
        error,
    } = useProducts(
        {
            sort: backendSorting,
            include: searchRequestIncludes,
            filter: {
                ...searchRequestFilter,
                ignore_id: uniqueExistingProductIds,
            },
            pagination: getPagination(activePage, itemsPerPageCount),
        },
        Boolean(meta) && !!backendSorting && !!apiTemplateProducts?.data && isOpen
    );

    const convertedTableData = useMemo(
        () =>
            data?.data.map(e => ({
                ...e,
                customListLink: '/products/catalog',
            })),
        [data?.data]
    );

    const products = useAutoTableData<Product>(convertedTableData, metaField);
    const autoGeneratedColumns = useAutoColumns(meta, canViewDetailPage);

    const columns = useMemo(
        () => [
            getSelectColumn(0, 'add-products-popup'),
            ...autoGeneratedColumns,
            getSettingsColumn({ columnsToDisable: [], visibleColumns: meta?.default_list }),
        ],
        [autoGeneratedColumns, meta?.default_list]
    );

    const total = getTotal(data);
    const totalPages = getTotalPages(data, itemsPerPageCount);

    useRedirectToNotEmptyPage({ activePage, itemsPerPageCount, shallow: true, total, pageKey });

    useError(error);
    useError(metaError);

    const bindProducts = useAddSeoTemplateProducts();
    useSuccess(bindProducts.isSuccess && ModalMessages.SUCCESS_UPDATE);
    useError(bindProducts.error);

    const renderHeader = useCallback(
        (selected: Row<Product>[]) => (
            <TableHeader css={{ paddingLeft: 0, paddingRight: 0 }}>
                <Layout type="flex" gap={scale(4)} align="center" css={{ width: '100%', minHeight: scale(9) }}>
                    <Layout.Item>
                        <p>Найдено {`${total} ${declOfNum(total, ['товар', 'товара', 'товаров'])}`}</p>
                    </Layout.Item>
                    {!!selected.length && (
                        <>
                            <Layout.Item>
                                <p>
                                    <b>Выбрано:</b> {selected.length}
                                </p>
                            </Layout.Item>
                            <Layout.Item>
                                <Button
                                    type="button"
                                    onClick={async () => {
                                        onClose();
                                        try {
                                            await bindProducts.mutateAsync({
                                                id: template?.id!,
                                                ids: selected.map(e => Number(e.original.id)),
                                            });
                                        } catch (e) {
                                            console.error(e);
                                        }
                                    }}
                                >
                                    Привязать к SEO-шаблону
                                </Button>
                            </Layout.Item>
                        </>
                    )}
                </Layout>
            </TableHeader>
        ),
        [bindProducts, onClose, template?.id, total]
    );

    const table = useTable(
        {
            data: products,
            columns,
            meta: {
                tableKey: pageKey,
            },
        },
        [sortingPlugin]
    );

    return (
        <Popup open={isOpen} onClose={onClose} size="screen_lg" innerScroll>
            <PopupHeader>
                <h4>Выберите товары для добавления</h4>
            </PopupHeader>
            <PopupContent>
                <LoadWrapper isLoading={isLoading}>
                    <AutoFilters
                        initialValues={values}
                        filtersSettingsName="AddingTemplatesProductsFilterSettings"
                        emptyInitialValues={emptyInitialValues}
                        onSubmit={v => {
                            URLHelper(v);
                            push({ pathname, query: { entity_id: query.entity_id, page: 1 } }, undefined, {
                                shallow: true,
                            });
                        }}
                        onResetFilters={resetFilters}
                        clearInitialValue={clearInitialValue}
                        filtersActive={filtersActive}
                        css={{ padding: 0, boxShadow: 'none', margin: 0 }}
                        meta={meta}
                        queryPart={{ entity_id: template?.id! }}
                    />
                    {renderHeader(table.getSelectedRowModel().flatRows)}
                    <Table instance={table} />
                    {/* <Table renderHeader={renderHeader} columns={columns} data={products} onSortingChange={setSort} /> */}
                </LoadWrapper>
            </PopupContent>
            <PopupFooter justify="end" cols={1}>
                <Layout.Item>
                    {products.length === 0 ? (
                        <TableEmpty
                            filtersActive={filtersActive}
                            titleWithFilters="Товары с данными фильтрами не найдены"
                            titleWithoutFilters="Товары отсутствуют"
                            onResetFilters={resetFilters}
                        />
                    ) : (
                        <TableFooter
                            pages={totalPages}
                            itemsPerPageCount={itemsPerPageCount}
                            setItemsPerPageCount={setItemsPerPageCount}
                            pageKey={pageKey}
                            css={{ border: 'none' }}
                        />
                    )}
                </Layout.Item>
            </PopupFooter>
        </Popup>
    );
};
