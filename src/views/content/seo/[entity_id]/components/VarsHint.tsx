import { useMemo } from 'react';

import { useSeoTemplateVariables } from '@api/content/seo';

import VariablesHint from '@components/VariablesHint';

export const VarsHint = () => {
    const { data: apiVars } = useSeoTemplateVariables();

    const items = useMemo(() => apiVars?.data?.map(e => ({
        id: e.id,
        title: e.name,
        items: []
    })) || [], [apiVars?.data]);

    return <VariablesHint items={items} label="Доступны теги:" />;
};
