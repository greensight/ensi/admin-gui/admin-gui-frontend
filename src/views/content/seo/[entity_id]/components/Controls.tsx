import { useRouter } from 'next/router';

import PageControls from '@components/PageControls';

import { AccessMatrix, ReturnedMatrix } from '@scripts/hooks/useAccess';

export const Controls = ({
    access,
    onSave,
    onApply,
    onDelete,
}: {
    access: ReturnedMatrix<AccessMatrix>;
    onDelete: () => void;
    onSave: () => void;
    onApply: () => void;
}) => {
    const { push, pathname } = useRouter();
    const listLink = pathname.split(`[entity_id]`)[0];

    return (
        <PageControls access={access}>
            <PageControls.Delete onClick={onDelete} />
            <PageControls.Close
                onClick={() =>
                    push({
                        pathname: listLink,
                    })
                }
            />
            <PageControls.Apply onClick={onApply} />
            <PageControls.Save onClick={onSave} />
        </PageControls>
    );
};
