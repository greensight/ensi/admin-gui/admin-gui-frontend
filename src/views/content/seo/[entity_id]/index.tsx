import {
    CopyButton,
    DescriptionList,
    DescriptionListItem,
    FormField,
    FormFieldWrapper,
    Layout,
    Select,
    Textarea,
    useActionPopup,
} from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { useEffect, useMemo, useRef, useState } from 'react';
import * as Yup from 'yup';

import {
    useCreateSeoTemplate,
    useDeleteSeoTemplate,
    usePatchSeoTemplate,
    useSeoTemplate,
    useSeoTemplateTypes,
} from '@api/content/seo';
import { CmsSeoTemplateTypeEnum } from '@api/content/types/seo';

import { useError, useSuccess } from '@context/modal';

import Legend from '@controls/Legend';
import LoadingSkeleton from '@controls/LoadingSkeleton';
import Switcher from '@controls/Switcher';

import FormWrapper from '@components/FormWrapper';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';
import TextEditor from '@components/TextEditor';

import { CREATE_PARAM, ErrorMessages, ModalMessages } from '@scripts/constants';
import { RedirectMethods } from '@scripts/enums';
import { scale } from '@scripts/gds';
import { isPageNotFound, toSelectItems } from '@scripts/helpers';
import { useDeferredLoading } from '@scripts/hooks/useDeferredLoading';

import { useSeoAccess } from '../useSeoAccess';
import { AddProductsPopup } from './components/AddProductsPopup';
import { BindedProducts } from './components/BindedProducts';
import { Controls } from './components/Controls';
import { VarsHint } from './components/VarsHint';

interface SeoValues {
    name: string;
    type: CmsSeoTemplateTypeEnum;
    header: string;
    title: string;
    description: string;
    seo_text: string;
    is_active: boolean;
}

const SeoInner = ({
    isLoading,
    setCurrentType,
}: {
    isLoading: boolean;
    setCurrentType: (type: CmsSeoTemplateTypeEnum) => void;
}) => {
    const { data: apiTypes, isFetching: isTypesLoading } = useSeoTemplateTypes();
    const templateTypes = useMemo(() => toSelectItems(apiTypes?.data || []), [apiTypes?.data]);

    const deferredIsLoading = useDeferredLoading(isLoading || isTypesLoading);
    if (deferredIsLoading) {
        return (
            <LoadingSkeleton
                count={5}
                css={{
                    marginBottom: scale(2),
                }}
                height={scale(6)}
            />
        );
    }

    return (
        <Layout cols={1} gap={scale(2)} css={{ marginBottom: scale(2) }}>
            <FormField name="name" label="Название*" />
            <FormFieldWrapper name="type" label="Тип*">
                <Select
                    allowUnselect={false}
                    options={templateTypes}
                    hideClearButton
                    onChange={(_, payload) => {
                        if (payload?.actionItem?.value) {
                            setCurrentType(Number(payload.actionItem.value));
                        }
                    }}
                />
            </FormFieldWrapper>
            <FormField name="header" label="Заголовок h1*" rightAddons={<VarsHint />} />
            <FormField name="title" label="Заголовок окна браузера" rightAddons={<VarsHint />} />
            <FormFieldWrapper name="description" label="Описание страницы">
                <Textarea minRows={5} rightAddons={<VarsHint />} />
            </FormFieldWrapper>
            <FormFieldWrapper name="seo_text" label="" css={{ minHeight: scale(20) }}>
                <div css={{ display: 'flex', alignItems: 'baseline', gap: scale(1) }}>
                    <Legend label="SEO-текст" />
                    <VarsHint />
                </div>
                <TextEditor />
            </FormFieldWrapper>
        </Layout>
    );
};

const Seo = () => {
    const {
        query: { entity_id },
        push,
    } = useRouter();

    const isCreation = entity_id === CREATE_PARAM;
    const parsedId = isCreation ? 0 : +`${entity_id}`;

    const { data: apiData, error, isFetching: isLoading } = useSeoTemplate({ id: parsedId });
    const template = apiData?.data;
    useError(error);

    const updateSeo = usePatchSeoTemplate();
    const deleteSeo = useDeleteSeoTemplate();
    const createSeo = useCreateSeoTemplate();

    useError(updateSeo.error);
    useError(deleteSeo.error);
    useError(createSeo.error);

    useSuccess(updateSeo.isSuccess && ModalMessages.SUCCESS_UPDATE);
    useSuccess(deleteSeo.isSuccess && ModalMessages.SUCCESS_DELETE);
    useSuccess(createSeo.isSuccess && ModalMessages.SUCCESS_CREATE);

    const initialValues = useMemo<SeoValues>(
        () => ({
            description: template?.description || '',
            header: template?.header || '',
            is_active: template?.is_active || false,
            name: template?.name || '',
            seo_text: template?.seo_text || '',
            title: template?.title || '',
            type: template?.type || ('' as never as number),
        }),
        [template]
    );

    const pageTitle = isCreation ? 'Создать SEO-шаблон' : `SEO-шаблон: ${template?.name || ''}`;

    const shouldReturnBack = useRef(false);

    const { access, isIDForbidden } = useSeoAccess();

    const { popupState, popupDispatch, ActionPopup, ActionEnum, ActionType } = useActionPopup();

    const [isAddingItems, setAddingItems] = useState(false);
    const [currentType, setCurrentType] = useState<CmsSeoTemplateTypeEnum>();

    useEffect(() => {
        setCurrentType(template?.type);
    }, [template?.type]);

    const canEdit = isCreation ? access.ID.create : access.ID.edit;

    const isNotFound = isPageNotFound({ id: parsedId, error, isCreation });

    return (
        <PageWrapper
            title={pageTitle}
            isLoading={isLoading || updateSeo.isPending || createSeo.isPending || deleteSeo.isPending}
            isNotFound={isNotFound}
            isForbidden={isIDForbidden}
        >
            <FormWrapper
                disabled={!canEdit}
                initialValues={initialValues}
                enableReinitialize
                onSubmit={async values => {
                    if (isCreation) {
                        const result = await createSeo.mutateAsync(values);

                        if (shouldReturnBack.current) {
                            return {
                                redirectPath: `/content/seo`,
                                method: RedirectMethods.push,
                            };
                        }

                        return {
                            redirectPath: `/content/seo/${result.data.id}`,
                            method: RedirectMethods.replace,
                        };
                    }

                    await updateSeo.mutateAsync({
                        id: parsedId,
                        ...values,
                    });

                    if (shouldReturnBack.current) {
                        return {
                            redirectPath: `/content/seo`,
                            method: RedirectMethods.push,
                        };
                    }

                    return {
                        redirectPath: `/content/seo/${parsedId}`,
                        method: RedirectMethods.replace,
                    };
                }}
                validationSchema={Yup.object().shape({
                    name: Yup.string().required(ErrorMessages.REQUIRED),
                    type: Yup.number()
                        .transform(val => (Number.isNaN(val) ? undefined : val))
                        .required(ErrorMessages.REQUIRED),
                    header: Yup.string().required(ErrorMessages.REQUIRED),
                    title: Yup.string(),
                    description: Yup.string(),
                    seo_text: Yup.string(),
                    is_active: Yup.boolean(),
                })}
            >
                {({ reset }) => (
                    <PageTemplate
                        h1={pageTitle}
                        controls={
                            <Controls
                                access={access}
                                onDelete={() =>
                                    popupDispatch({
                                        type: ActionType.Delete,
                                        payload: {
                                            title: `Вы уверены, что хотите удалить шаблон?`,
                                            popupAction: ActionEnum.DELETE,
                                            onAction: async () => {
                                                reset();
                                                await deleteSeo.mutateAsync({
                                                    id: parsedId,
                                                });

                                                push('/content/seo');
                                            },
                                        },
                                    })
                                }
                                onApply={() => {
                                    shouldReturnBack.current = false;
                                }}
                                onSave={() => {
                                    shouldReturnBack.current = true;
                                }}
                            />
                        }
                        backlink={{ text: 'К списку SEO-шаблонов', href: '/content/seo' }}
                        aside={
                            <DescriptionList>
                                <DescriptionListItem
                                    name="Активность"
                                    value={
                                        <FormFieldWrapper name="is_active" label="Активность">
                                            <Switcher>Активность</Switcher>
                                        </FormFieldWrapper>
                                    }
                                />
                                {!isCreation && (
                                    <DescriptionListItem name="ID" value={<CopyButton>{`${entity_id}`}</CopyButton>} />
                                )}
                                <DescriptionListItem name="Дата создания" value={template?.created_at} type="date" />
                                <DescriptionListItem name="Дата обновления" value={template?.updated_at} type="date" />
                            </DescriptionList>
                        }
                    >
                        <SeoInner isLoading={isLoading} setCurrentType={setCurrentType} />
                    </PageTemplate>
                )}
            </FormWrapper>
            {currentType === CmsSeoTemplateTypeEnum.PRODUCT && (
                <BindedProducts template={template} onAdd={() => setAddingItems(true)} canEdit={access.ID.edit} />
            )}
            <AddProductsPopup template={template} isOpen={isAddingItems} onClose={() => setAddingItems(false)} />
            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </PageWrapper>
    );
};

export default Seo;
