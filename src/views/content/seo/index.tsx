import { useMemo } from 'react';

import { useSearchSeoTemplates, useSeoTemplatesMeta } from '@api/content/seo';
import { SeoTemplate } from '@api/content/types/seo';

import { useSeoAccess } from '@views/content/seo/useSeoAccess';

import ListBuilder from '@components/ListBuilder';
import { TooltipItem } from '@components/Table';

import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

const SeoList = () => {
    const { access } = useSeoAccess();

    const goToDetailPage = useGoToDetailPage();

    const tooltipContent: TooltipItem[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Редактировать SEO-шаблон',
                action: goToDetailPage,
                isDisable: !access.ID.view,
            },
        ],
        [access.ID.view, goToDetailPage]
    );

    return (
        <ListBuilder<SeoTemplate>
            access={access}
            searchHook={useSearchSeoTemplates}
            metaHook={useSeoTemplatesMeta}
            tooltipItems={tooltipContent}
            title="Список SEO-шаблонов"
        />
    );
};

export default SeoList;
