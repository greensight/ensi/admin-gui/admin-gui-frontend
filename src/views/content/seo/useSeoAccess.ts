import { useAccess } from '@scripts/hooks/useAccess';
import { useIDForbidden } from '@scripts/hooks/useIDForbidden';

export const AccessMatrix = {
    LIST: {
        // Пользователю доступен табличный список SEO-шаблонов с фильтрами и поиском
        view: 1701,
    },
    ID: {
        // Пользователю доступна детальная страница SEO-шаблонов со всеми данными для просмотра
        view: 1702,
        // Пользователю доступно редактирование SEO-шаблонов
        edit: 1703,
        // Пользователю доступно создание SEO-шаблонов
        create: 1704,
        // Пользователю доступно удаление SEO-шаблонов
        delete: 1705,
    },
};

export const useSeoAccess = () => {
    const access = useAccess(AccessMatrix);
    const isIDForbidden = useIDForbidden(access);

    return { access, isIDForbidden };
};
