import { defineAccessMatrix, useAccess } from '@scripts/hooks';
import { useIDForbidden } from '@scripts/hooks/useIDForbidden';

export const accessMatrix = defineAccessMatrix({
    LIST: {
        view: 1901,
    },
    ID: {
        view: 1902,
        edit: 1903,
        activityEdit: 1907,
        create: 1908,
        delete: 1909,
        workingEdit: 1904,
        contactsEdit: 1905,
        pickupTimeEdit: 1906,
    },
});

export const useStoresAccess = () => {
    const access = useAccess(accessMatrix);
    const isIDForbidden = useIDForbidden(access);

    return { access, isIDForbidden };
};
