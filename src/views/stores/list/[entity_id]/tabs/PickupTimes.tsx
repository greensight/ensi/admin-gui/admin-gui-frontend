import { ActionEnum, Block, useActionPopup } from '@ensi-platform/core-components';
import { CellContext, createColumnHelper } from '@tanstack/react-table';
import { useCallback, useMemo, useRef } from 'react';

import { useDeliveryServices } from '@api/logistic';
import { Store, StorePickupTime, useDeleteStorePickupTime } from '@api/units';

import LoadWrapper from '@controls/LoadWrapper';

import Table, { useSorting, useTable } from '@components/Table';

import { ActionType, daysValues } from '@scripts/enums';
import { Button, Layout, scale } from '@scripts/gds';
import { formatDate } from '@scripts/helpers';

import EditIcon from '@icons/small/edit.svg';
import AddIcon from '@icons/small/plus.svg';
import DeleteIcon from '@icons/small/trash.svg';

import { EditPickupTimePopupProps } from '../components/EditPickupTimePopup';

const columnHelper = createColumnHelper<StorePickupTime>();

export const PickupTimes = ({
    store,
    dispatch,
    disabled,
}: {
    disabled: boolean;
    store?: Store;
    dispatch: EditPickupTimePopupProps['dispatch'];
}) => {
    const { data: apiDeliveryServices, isFetching: isLoading } = useDeliveryServices({
        pagination: {
            limit: -1,
            offset: 0,
            type: 'offset',
        },
    });

    const deliveryServicesMap = useMemo(() => {
        const map = new Map<number, string>();

        apiDeliveryServices?.data.forEach(service => {
            map.set(service.id, service.name);
        });

        return map;
    }, [apiDeliveryServices?.data]);

    const deletePickupTime = useDeleteStorePickupTime();

    const deletePickupTimeAsync = useRef(deletePickupTime.mutateAsync);
    deletePickupTimeAsync.current = deletePickupTime.mutateAsync;

    const { popupState, popupDispatch, ActionPopup } = useActionPopup();

    const SettingsCell = useCallback(
        ({ row: { original } }: CellContext<StorePickupTime, unknown>) => (
            <Layout type="flex">
                <Layout.Item>
                    <Button
                        hidden
                        disabled={disabled}
                        Icon={EditIcon}
                        onClick={() => {
                            dispatch({
                                type: ActionType.Edit,
                                payload: {
                                    data: {
                                        id: original.id,
                                        day: original.day,
                                        delivery_service: original.delivery_service,
                                        pickup_time_end: original.pickup_time_end,
                                        pickup_time_start: original.pickup_time_start,
                                    },
                                    open: true,
                                    storeId: store?.id!,
                                },
                            });
                        }}
                    >
                        изменить
                    </Button>
                </Layout.Item>
                <Layout.Item>
                    <Button
                        hidden
                        Icon={DeleteIcon}
                        disabled={disabled}
                        theme="dangerous"
                        onClick={() => {
                            popupDispatch({
                                type: ActionType.Delete,
                                payload: {
                                    title: `Вы уверены, что хотите удалить время отгрузки #${original.id}?`,
                                    popupAction: ActionEnum.DELETE,
                                    onAction: async () => {
                                        try {
                                            await deletePickupTimeAsync.current({
                                                id: original.id,
                                            });
                                        } catch (e) {
                                            console.error(e);
                                        }
                                    },
                                },
                            });
                        }}
                    >
                        удалить
                    </Button>
                </Layout.Item>
            </Layout>
        ),
        [disabled, dispatch, popupDispatch, store?.id]
    );

    const columns = useMemo(
        () => [
            columnHelper.accessor('id', {
                header: 'ID',
                cell: props => props.getValue(),
            }),
            columnHelper.accessor('day', {
                header: 'День недели',
                cell: props => daysValues[String(props.getValue()) as keyof typeof daysValues] || '-',
            }),
            columnHelper.accessor('pickup_time_start', {
                header: 'Время начала отгрузки',
                cell: props => props.getValue(),
            }),
            columnHelper.accessor('pickup_time_end', {
                header: 'Время окончания отгрузки',
                cell: props => props.getValue(),
            }),
            columnHelper.accessor('delivery_service', {
                header: 'Служба доставки',
                cell: props => deliveryServicesMap.get(props.getValue()!) || '-',
            }),
            columnHelper.accessor('created_at', {
                header: 'Дата создания',
                cell: props => formatDate(new Date(props.getValue())),
            }),
            columnHelper.accessor('updated_at', {
                header: 'Дата изменения',
                cell: props => formatDate(new Date(props.getValue())),
            }),
            columnHelper.display({
                id: 'settings',
                header: '',
                cell: SettingsCell,
            }),
        ],
        [SettingsCell, deliveryServicesMap]
    );

    const data = useMemo(() => store?.pickup_times || [], [store?.pickup_times]);

    const [{ sorting }, sortingPlugin] = useSorting<any>('store_pickup_times', [], undefined, false);

    const table = useTable(
        {
            data,
            columns,
            meta: {
                tableKey: `store_pickup_times`,
            },
            state: {
                sorting,
            },
        },
        [sortingPlugin]
    );

    return (
        <LoadWrapper isLoading={isLoading}>
            <Block css={{ width: '100%', borderTopLeftRadius: 0, borderTopRightRadius: 0 }}>
                <Block.Body>
                    <div css={{ width: '100%', display: 'flex', marginBottom: scale(2) }}>
                        <Button
                            disabled={disabled}
                            Icon={AddIcon}
                            onClick={() =>
                                dispatch({
                                    type: ActionType.Add,
                                })
                            }
                            css={{ marginLeft: 'auto' }}
                        >
                            Добавить время отгрузки
                        </Button>
                    </div>
                    <Table instance={table} />
                    <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
                </Block.Body>
            </Block>
        </LoadWrapper>
    );
};
