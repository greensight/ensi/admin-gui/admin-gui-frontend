import { Autocomplete, AutocompleteAsync, Block, FormField, FormFieldWrapper } from '@ensi-platform/core-components';

import { useSellerAutocomplete } from '@api/units';

import { Layout } from '@scripts/gds';
import { useTimezones } from '@scripts/hooks';

export const MainData = () => {
    const timezoneOptions = useTimezones().map(e => ({ label: e.label, value: e.value }));

    const sellerAutocompleteProps = useSellerAutocomplete();

    return (
        <Block css={{ width: '100%', borderTopLeftRadius: 0, borderTopRightRadius: 0 }}>
            <Block.Body>
                <Layout cols={1}>
                    <Layout.Item>
                        <FormField name="name" label="Название склада" />
                    </Layout.Item>
                    <Layout.Item>
                        <FormFieldWrapper name="seller_id" label="Продавец">
                            <AutocompleteAsync {...sellerAutocompleteProps} />
                        </FormFieldWrapper>
                    </Layout.Item>
                    <Layout.Item>
                        <FormField label="Адрес" name="address_string" />
                    </Layout.Item>
                    <Layout.Item>
                        <FormField label="Почтовый индекс" name="postal_index" />
                    </Layout.Item>
                    <Layout.Item>
                        <FormFieldWrapper label="Часовой пояс склада" name="timezone">
                            <Autocomplete options={timezoneOptions} />
                        </FormFieldWrapper>
                    </Layout.Item>
                </Layout>
            </Block.Body>
        </Block>
    );
};
