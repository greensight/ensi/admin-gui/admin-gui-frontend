import {
    CopyButton,
    DescriptionList,
    DescriptionListItem,
    FormFieldWrapper,
    Tabs,
    getDefaultDates,
    useActionPopup,
} from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { useMemo, useRef } from 'react';
import * as Yup from 'yup';

import {
    StoreAddress,
    StoreForCreate,
    StoreForPatch,
    StoresSearchRequestInclude,
    useCreateStore,
    useDeleteStore,
    usePatchStore,
    useStore,
} from '@api/units';

import { useError, useSuccess } from '@context/modal';

import { useStoresAccess } from '@views/stores/useStoresAccess';

import Switcher from '@controls/Switcher';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { CREATE_PARAM, ErrorMessages, ModalMessages } from '@scripts/constants';
import { ButtonNameEnum, RedirectMethods } from '@scripts/enums';
import { scale } from '@scripts/gds';
import { isPageNotFound, pickChangedFormValues } from '@scripts/helpers';
import { usePopupState } from '@scripts/hooks';
import { useTabs } from '@scripts/hooks/useTabs';

import { EditContactPopup, EditContactPopupState } from './components/EditContactPopup';
import { EditPickupTimePopup, EditPickupTimePopupState } from './components/EditPickupTimePopup';
import { EditWorkingPopup, EditWorkingPopupState } from './components/EditWorkingPopup';
import { Contacts } from './tabs/Contacts';
import { MainData } from './tabs/MainData';
import { PickupTimes } from './tabs/PickupTimes';
import { Workings } from './tabs/Workings';

const Store = () => {
    const { query, pathname, push } = useRouter();
    const listLink = pathname.split(`[entity_id]`)[0];

    const { getTabsProps } = useTabs();
    const { access, isIDForbidden } = useStoresAccess();

    const {
        ID: { activityEdit, create, edit, contactsEdit, pickupTimeEdit, workingEdit },
    } = access;

    const id = +(query.entity_id?.toString() || '');
    const isCreation = query.entity_id === CREATE_PARAM;

    const canEdit = isCreation ? create : edit;

    const {
        data: apiStore,
        isFetching: isLoading,
        error,
    } = useStore(
        { id },
        {
            include: Object.values(StoresSearchRequestInclude),
        }
    );

    useError(error);

    const store = apiStore?.data;

    const createStore = useCreateStore();
    const patchStore = usePatchStore();
    const deleteStore = useDeleteStore();

    useError(createStore.error || patchStore.error || deleteStore.error);

    const { popupState, popupDispatch, ActionPopup, ActionEnum, ActionType } = useActionPopup();

    const [workingState, workingDispatch] = usePopupState<Partial<EditWorkingPopupState>>({ open: false, storeId: id });
    const [contactState, contactDispatch] = usePopupState<Partial<EditContactPopupState>>({ open: false, storeId: id });
    const [pickupState, pickupDispatch] = usePopupState<Partial<EditPickupTimePopupState>>({
        open: false,
        storeId: id,
    });

    useSuccess(createStore.isSuccess ? ModalMessages.SUCCESS_CREATE : '');
    useSuccess(patchStore.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const buttonNameRef = useRef<ButtonNameEnum | null>(null);

    const pageTitle = isCreation ? 'Создание склада' : `Редактирование склада #${id}`;

    const initialValues = useMemo(
        () => ({
            name: store?.name || '',
            seller_id: store?.seller_id || null,
            active: store?.active || false,
            address_string: store?.address.address_string || '',
            postal_index: store?.address.post_index || '',
            timezone: store?.timezone || '',
        }),
        [store]
    );

    const validationSchema = useMemo(
        () =>
            Yup.object().shape({
                name: Yup.string().required(ErrorMessages.REQUIRED),
                seller_id: Yup.number()
                    .transform(val => (Number.isNaN(val) ? undefined : val))
                    .optional()
                    .nullable(),
                timezone: Yup.string().optional().nullable(),
                address_string: Yup.string().optional().nullable(),
            }),
        []
    );

    const isNotFound = isPageNotFound({ id, error, isCreation });

    return (
        <>
            <PageWrapper
                title={pageTitle}
                isLoading={isLoading || createStore.isPending || patchStore.isPending}
                isNotFound={isNotFound}
                isForbidden={isIDForbidden}
            >
                <FormWrapper
                    initialValues={initialValues}
                    disabled={!canEdit}
                    enableReinitialize
                    validationSchema={validationSchema}
                    css={{ width: '100%', marginBottom: scale(3) }}
                    onSubmit={async values => {
                        const addressObj: StoreAddress = {
                            address_string: values.address_string,
                            post_index: values.postal_index,
                            country_code: '-',
                            region: '-',
                            region_guid: '-',
                            city: '-',
                            city_guid: '-',
                            geo_lat: '-',
                            geo_lon: '-',
                        };

                        if (isCreation) {
                            const createRequest: StoreForCreate = {
                                xml_id: null,
                                seller_id: values.seller_id,
                                active: values.active || false,
                                name: values.name || '',
                                timezone: values.timezone,
                                address: addressObj,
                            };

                            const response = await createStore.mutateAsync(createRequest);

                            if (buttonNameRef.current === ButtonNameEnum.APPLY) {
                                workingDispatch({
                                    type: ActionType.PreClose,
                                    payload: { storeId: response.data.id },
                                });
                                contactDispatch({
                                    type: ActionType.PreClose,
                                    payload: { storeId: response.data.id },
                                });
                                pickupDispatch({
                                    type: ActionType.PreClose,
                                    payload: { storeId: response.data.id },
                                });

                                return {
                                    method: RedirectMethods.replace,
                                    redirectPath: `/stores/list/${response.data.id}`,
                                };
                            }

                            return {
                                method: RedirectMethods.push,
                                redirectPath: '/stores/list',
                            };
                        }

                        const initialValuesPicked: StoreForPatch = {
                            name: initialValues.name,
                            active: initialValues.active,
                            seller_id: initialValues.seller_id,
                            address: {
                                address_string: initialValues.address_string,
                                post_index: initialValues.postal_index,
                                country_code: '-',
                                region: '-',
                                region_guid: '-',
                                city: '-',
                                city_guid: '-',
                                geo_lat: '-',
                                geo_lon: '-',
                            },
                            timezone: initialValues.timezone,
                        };

                        const newPatchData: StoreForPatch = {
                            name: values.name || '',
                            active: values.active || false,
                            timezone: values.timezone,
                            address: addressObj,
                            seller_id: values.seller_id,
                        };

                        const patchRequest = pickChangedFormValues(initialValuesPicked, newPatchData);
                        await patchStore.mutateAsync({ id, ...patchRequest });

                        if (buttonNameRef.current === ButtonNameEnum.APPLY) {
                            return {
                                method: RedirectMethods.replace,
                                redirectPath: `/stores/list/${id}`,
                            };
                        }

                        return {
                            method: RedirectMethods.push,
                            redirectPath: '/stores/list',
                        };
                    }}
                >
                    {({ reset }) => (
                        <PageTemplate
                            h1={pageTitle}
                            backlink={{ text: 'К списку складов', href: '/stores/list' }}
                            customChildren
                            controls={
                                <PageControls access={access}>
                                    <PageControls.Delete
                                        onClick={() => {
                                            popupDispatch({
                                                type: ActionType.Delete,
                                                payload: {
                                                    title: `Вы уверены, что хотите удалить склад №${id}?`,
                                                    popupAction: ActionEnum.DELETE,
                                                    onAction: async () => {
                                                        if (!id) return;
                                                        reset();
                                                        await deleteStore.mutateAsync({ id });

                                                        push({ pathname: listLink });
                                                    },
                                                },
                                            });
                                        }}
                                    />
                                    <PageControls.Close onClick={() => push(listLink)} />
                                    <PageControls.Apply
                                        onClick={() => {
                                            buttonNameRef.current = ButtonNameEnum.APPLY;
                                        }}
                                    />
                                    <PageControls.Save
                                        onClick={() => {
                                            buttonNameRef.current = ButtonNameEnum.SAVE;
                                        }}
                                    />
                                </PageControls>
                            }
                            aside={
                                <DescriptionList>
                                    <DescriptionListItem
                                        value={
                                            <FormFieldWrapper name="active" label="Активность" disabled={!activityEdit}>
                                                <Switcher>Активность</Switcher>
                                            </FormFieldWrapper>
                                        }
                                    />
                                    <DescriptionListItem
                                        name="ID:"
                                        value={!isCreation ? <CopyButton>{`${id}`}</CopyButton> : '-'}
                                    />
                                    {getDefaultDates(store || {}).map(item => (
                                        <DescriptionListItem key={item.name} {...item} />
                                    ))}
                                </DescriptionList>
                            }
                        >
                            <div css={{ display: 'flex', width: '100%' }}>
                                <Tabs {...getTabsProps()} css={{ width: '100%' }}>
                                    <Tabs.Tab title="Основные данные" id="0">
                                        <MainData />
                                    </Tabs.Tab>
                                    <Tabs.Tab title="Время работы" id="1" hidden={isCreation}>
                                        <Workings store={store} dispatch={workingDispatch} disabled={!workingEdit} />
                                    </Tabs.Tab>
                                    <Tabs.Tab title="Контакты" id="2" hidden={isCreation}>
                                        <Contacts store={store} dispatch={contactDispatch} disabled={!contactsEdit} />
                                    </Tabs.Tab>
                                    <Tabs.Tab title="График отгрузки" id="3" hidden={isCreation}>
                                        <PickupTimes
                                            store={store}
                                            dispatch={pickupDispatch}
                                            disabled={!pickupTimeEdit}
                                        />
                                    </Tabs.Tab>
                                </Tabs>
                            </div>
                        </PageTemplate>
                    )}
                </FormWrapper>
            </PageWrapper>
            <EditWorkingPopup state={workingState} dispatch={workingDispatch} />
            <EditContactPopup state={contactState} dispatch={contactDispatch} />
            <EditPickupTimePopup state={pickupState} dispatch={pickupDispatch} />
            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </>
    );
};

export default Store;
