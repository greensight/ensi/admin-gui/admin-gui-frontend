import {
    Form,
    FormField,
    FormFieldWrapper,
    Loader,
    Popup,
    PopupContent,
    PopupFooter,
    PopupHeader,
} from '@ensi-platform/core-components';
import { Dispatch, useCallback, useMemo } from 'react';
import * as Yup from 'yup';

import { useCreateStoreContact, usePatchStoreContact } from '@api/units';

import Mask from '@controls/Mask';

import { ErrorMessages } from '@scripts/constants';
import { ActionType } from '@scripts/enums';
import { Button, Layout } from '@scripts/gds';
import { Action } from '@scripts/hooks/usePopupState';
import { maskPhone } from '@scripts/mask';

type ContactData = {
    id: number;
    name: string;
    phone: string;
    email: string;
};

export type EditContactPopupState = {
    data?: ContactData | null;
    storeId: number;
    open: boolean;
    action: ActionType;
};

export interface EditContactPopupProps {
    state: Partial<EditContactPopupState>;
    dispatch: Dispatch<Action<Partial<EditContactPopupState>>>;
}

export const EditContactPopup = ({ state, dispatch }: EditContactPopupProps) => {
    const { data, storeId, action } = state;
    const isCreation = action === ActionType.Add;

    const initialValues = useMemo<ContactData>(
        () => ({
            id: data?.id!,
            email: data?.email || '',
            name: data?.name || '',
            phone: data?.phone || '',
        }),
        [data]
    );

    const createContact = useCreateStoreContact();
    const updateContact = usePatchStoreContact();

    const onSubmit = useCallback(
        async (values: ContactData) => {
            if (isCreation) {
                await createContact.mutateAsync({
                    email: values.email,
                    name: values.name,
                    phone: values.phone,
                    store_id: storeId!,
                });

                dispatch({ type: ActionType.Close });
                return;
            }

            await updateContact.mutateAsync({
                ...values,
                store_id: storeId!,
            });

            dispatch({ type: ActionType.Close });
        },
        [createContact, data?.id, dispatch, isCreation, storeId, updateContact]
    );

    const isLoading = createContact.isPending || updateContact.isPending;

    return (
        <Popup open={state.open!} onClose={() => dispatch({ type: ActionType.Close })} size="minMd">
            {isLoading ? (
                <Loader />
            ) : (
                <Form
                    initialValues={initialValues}
                    onSubmit={onSubmit}
                    validationSchema={Yup.object({
                        name: Yup.string().required(ErrorMessages.REQUIRED),
                        phone: Yup.string().required(ErrorMessages.REQUIRED).min(17, ErrorMessages.REQUIRED),
                        email: Yup.string().email(ErrorMessages.EMAIL).required(ErrorMessages.REQUIRED),
                    })}
                >
                    <PopupHeader title={`${isCreation ? 'Создание' : 'Редактирование'} контактного лица`} />
                    <PopupContent>
                        <Layout cols={1}>
                            <FormField name="name" label="ФИО" />
                            <FormFieldWrapper name="phone" label="Телефон" type="tel">
                                <Mask mask={maskPhone} />
                            </FormFieldWrapper>
                            <FormField name="email" label="Email" />
                        </Layout>
                    </PopupContent>
                    <PopupFooter>
                        <Button theme="outline" onClick={() => dispatch({ type: ActionType.Close })}>
                            Отменить
                        </Button>
                        <Button type="submit">Сохранить</Button>
                    </PopupFooter>
                </Form>
            )}
        </Popup>
    );
};
