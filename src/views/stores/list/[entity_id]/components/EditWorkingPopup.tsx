import {
    Form,
    FormFieldWrapper,
    Loader,
    Popup,
    PopupContent,
    PopupFooter,
    PopupHeader,
} from '@ensi-platform/core-components';
import { Dispatch, useCallback, useMemo } from 'react';
import * as Yup from 'yup';

import { useCreateStoreWorking, usePatchStoreWorking } from '@api/units';

import Legend from '@controls/Legend';
import Mask from '@controls/Mask';
import Select from '@controls/Select';
import Switcher from '@controls/Switcher';

import { ErrorMessages } from '@scripts/constants';
import { ActionType, Days, daysValues } from '@scripts/enums';
import { Button, Layout } from '@scripts/gds';
import { prepareEnumForFutureSelect } from '@scripts/helpers';
import { Action } from '@scripts/hooks/usePopupState';
import { TIME_WITHOUT_SECONDS_MASK } from '@scripts/mask';

type WorkingData = {
    id: number;
    active: boolean;
    day: number | '';
    working_start_time?: string | null;
    working_end_time?: string | null;
};

export type EditWorkingPopupState = {
    data?: WorkingData | null;
    storeId: number;
    open: boolean;
    action: ActionType;
};

const dayOptions = prepareEnumForFutureSelect(daysValues).map(e => ({ key: e.key, value: +e.value }));

export interface EditWorkingPopupProps {
    state: Partial<EditWorkingPopupState>;
    dispatch: Dispatch<Action<Partial<EditWorkingPopupState>>>;
}

export const EditWorkingPopup = ({ state, dispatch }: EditWorkingPopupProps) => {
    const { data, storeId, action } = state;
    const isCreation = action === ActionType.Add;

    const initialValues = useMemo<WorkingData>(
        () => ({
            id: data?.id!,
            active: data?.active || false,
            day: data?.day ? +`${data?.day}` : Days.MONDAY,
            working_end_time: data?.working_end_time || null,
            working_start_time: data?.working_start_time || null,
        }),
        [data]
    );

    const createWorking = useCreateStoreWorking();
    const updateWorking = usePatchStoreWorking();

    const onSubmit = useCallback(
        async (values: WorkingData) => {
            if (!storeId || (!isCreation && !initialValues.id)) {
                console.error('ID and storeId are required for submit form');
                return;
            }

            const sanitizedValues = {
                ...values,
                day: +values.day,
                working_start_time: values.working_start_time || '00:00',
                working_end_time: values.working_end_time || '00:00',
                store_id: storeId,
            };

            if (isCreation) {
                await createWorking.mutateAsync({
                    ...sanitizedValues,
                });

                dispatch({ type: ActionType.Close });
                return;
            }

            await updateWorking.mutateAsync({
                ...sanitizedValues,
                id: initialValues.id,
            });

            dispatch({ type: ActionType.Close });
        },
        [createWorking, initialValues.id, dispatch, isCreation, storeId, updateWorking]
    );

    const isLoading = createWorking.isPending || updateWorking.isPending;

    return (
        <Popup open={state.open!} onClose={() => dispatch({ type: ActionType.Close })} size="minMd">
            <Form
                initialValues={initialValues}
                onSubmit={onSubmit}
                validationSchema={Yup.object({
                    active: Yup.boolean(),
                    day: Yup.number()
                        .transform(val => (Number.isNaN(+val) ? undefined : val))
                        .required(ErrorMessages.REQUIRED),
                })}
            >
                {isLoading && <Loader />}
                <PopupHeader title={`${isCreation ? 'Создание' : 'Редактирование'} времени работы`} />
                <PopupContent>
                    <Layout cols={1}>
                        <FormFieldWrapper name="active" label="Активность">
                            <Legend label="Активность" />
                            <Switcher css={{ width: 'fit-content' }}>Да</Switcher>
                        </FormFieldWrapper>
                        <FormFieldWrapper name="day" label="День недели">
                            <Select options={dayOptions} hideClearButton />
                        </FormFieldWrapper>
                        <FormFieldWrapper
                            name="working_start_time"
                            label="Время начала работы склада"
                            placeholder="00:00"
                        >
                            <Mask {...TIME_WITHOUT_SECONDS_MASK} />
                        </FormFieldWrapper>
                        <FormFieldWrapper name="working_end_time" label="Время конца работы склада" placeholder="00:00">
                            <Mask {...TIME_WITHOUT_SECONDS_MASK} />
                        </FormFieldWrapper>
                    </Layout>
                </PopupContent>
                <PopupFooter>
                    <Button theme="outline" onClick={() => dispatch({ type: ActionType.Close })}>
                        Отменить
                    </Button>
                    <Button type="submit">Сохранить</Button>
                </PopupFooter>
            </Form>
        </Popup>
    );
};
