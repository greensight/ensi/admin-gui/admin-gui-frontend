import { useMemo } from 'react';

import { Store, useSearchStores, useStoresMeta } from '@api/units';

import { useStoresAccess } from '@views/stores/useStoresAccess';

import ListBuilder from '@components/ListBuilder';
import { TooltipItem } from '@components/Table';

import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

const StoresList = () => {
    const { access } = useStoresAccess();

    const goToDetailPage = useGoToDetailPage({
        extraConditions: access.LIST.view,
    });

    const tooltipContent = useMemo<TooltipItem[]>(
        () => [
            {
                type: 'edit',
                text: 'Редактировать магазин',
                action: goToDetailPage,
                isDisable: !access.LIST.view,
            },
        ],
        [goToDetailPage, access.LIST.view]
    );

    return (
        <ListBuilder<Store>
            access={access}
            searchHook={useSearchStores}
            metaHook={useStoresMeta}
            tooltipItems={tooltipContent}
            title="Список складов"
        />
    );
};

export default StoresList;
