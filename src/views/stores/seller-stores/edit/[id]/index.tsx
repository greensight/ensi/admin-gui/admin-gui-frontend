// import { CSSObject } from '@emotion/react';
// import { useRouter } from 'next/router';
// import { useMemo } from 'react';
// import * as Yup from 'yup';

// import { useDeliveryServices } from '@api/logistic';
// import { Address } from '@api/orders/types';
// import {
//     StoreContact,
//     useCreateStoreContact,
//     useCreateStorePickupTime,
//     useCreateStoreWorking,
//     useDeleteStoreContact,
//     usePatchStoreContact,
//     usePatchStorePickupTime,
//     usePatchStoreWorking,
//     useStore,
// } from '@api/units';
// import { StorePickupTimeForCreate, StorePickupTimeForPatch } from '@api/units/types/storePickupTimes';

// import { useError, useSuccess } from '@context/modal';

// import PageWrapper from '@components/PageWrapper';
// import {Form, FormFieldWrapper} from '@ensi-platform/core-components';
// import Tabs from '@controls/Tabs';

// import { ErrorMessages } from '@scripts/constants';
// import { daysValues } from '@scripts/enums';
// import { Button, scale, useTheme } from '@scripts/gds';
// import { useTabs } from '@scripts/hooks';
// import { regPhone } from '@scripts/regex';

// import Contacts from './Contacts';
// import EditStore from './EditStore';
// import PickupTimes from './PickupTimes';
// import Workings from './Workings';
// import { WorkingItem } from './types';

// interface StoresFormValues {
//     seller?: number;
//     name?: string;
//     code?: string;
//     address?: Partial<Address>;
//     porch?: number | null;
//     floor?: number | null;
//     intercome?: string | null;
//     timezone?: { label?: string; value?: string };
//     active?: boolean;
//     comment?: string | null | undefined;
//     workings?: WorkingItem[];
//     pickupTimes?: {
//         delivery_service_name: string;
//         delivery_service: number | undefined;
//         pickupTimes: {
//             day: number;
//             pickup_time_id?: number | null | string;
//             pickup_time_code?: string;
//             pickup_time_start?: string;
//             pickup_time_end?: string;
//             cargo_export_time?: string;
//         }[];
//     }[];
//     contacts?: StoreContact[];
// }

// const SellerStoresEdit = () => {
//     const { colors } = useTheme();
//     const { query } = useRouter();
//     const storeID = Array.isArray(query.id) ? query.id[0] : query.id || '';

//     const { data, error } = useStore(
//         {
//             id: storeID,
//         },
//         { include: ['contacts'] }
//     );
//     const store = data?.data;

//     const createStoreWorkings = useCreateStoreWorking();
//     const updateStoreWorkings = usePatchStoreWorking();
//     const createPickupTime = useCreateStorePickupTime();
//     const updatePickupTime = usePatchStorePickupTime();
//     const createContact = useCreateStoreContact();
//     const updateContact = usePatchStoreContact();
//     const deleteContact = useDeleteStoreContact();

//     const { data: deliveryServiceData, error: deliveryServiceError } = useDeliveryServices({
//         pagination: { type: 'offset', limit: -1, offset: 0 },
//     });
//     const deliveryServices = useMemo(() => deliveryServiceData?.data || [], [deliveryServiceData?.data]);

//     const { getTabsProps } = useTabs();
//     useError(
//         error ||
//             deliveryServiceError ||
//             createStoreWorkings.error ||
//             updateStoreWorkings.error ||
//             createPickupTime.error ||
//             updatePickupTime.error ||
//             createContact.error ||
//             updateContact.error ||
//             deleteContact.error
//     );

//     useSuccess(
//         createStoreWorkings.status === 'success' || updateStoreWorkings.status === 'success'
//             ? 'Данные о графике работы обновлены успешно'
//             : ''
//     );
//     useSuccess(
//         createPickupTime.status === 'success' || updatePickupTime.status === 'success'
//             ? 'Данные о графике отгрузки обновлены успешно'
//             : ''
//     );
//     useSuccess(
//         createContact.status === 'success' || updateContact.status === 'success' || deleteContact.status === 'success'
//             ? 'Данные о контактных лицах обновлены успешно'
//             : ''
//     );

//     const lineCSS: CSSObject = {
//         padding: `${scale(2)}px`,
//         borderBottom: `1px solid ${colors?.grey400}`,
//         verticalAlign: 'top',
//     };

//     const workings = useMemo(
//         () =>
//             Object.keys(daysValues).map(d => {
//                 const workingForDay = store?.workings.find(w => w.day === +d);
//                 return {
//                     id: workingForDay?.id || '',
//                     working_start_time: workingForDay?.working_start_time || '',
//                     working_end_time: workingForDay?.working_end_time || '',
//                     day: +d,
//                     active: workingForDay?.active || false,
//                 };
//             }),
//         [store?.workings]
//     );

//     const pickupTimes = useMemo(
//         () => [
//             {
//                 delivery_service_name: 'Все логистические операторы',
//                 delivery_service: undefined,
//                 pickupTimes: Object.keys(daysValues).map(dayCode => {
//                     const foundPT = store?.pickup_times.find(p => p.day === +dayCode && p.delivery_service === null);
//                     return {
//                         day: +dayCode,
//                         pickup_time_id: foundPT ? foundPT.id : undefined,
//                         pickup_time_code: foundPT ? foundPT.pickup_time_code : '',
//                         pickup_time_start: foundPT ? foundPT.pickup_time_start : '',
//                         pickup_time_end: foundPT ? foundPT.pickup_time_end : '',
//                         cargo_export_time: foundPT ? foundPT.cargo_export_time : '',
//                     };
//                 }),
//             },
//             ...deliveryServices.map(ds => ({
//                 delivery_service_name: ds.name || '-',
//                 delivery_service: ds.id,
//                 pickupTimes: Object.keys(daysValues).map(dayCode => {
//                     const foundPT = store?.pickup_times.find(p => p.day === +dayCode && p.delivery_service === ds.id);
//                     const commonData = {
//                         day: +dayCode,
//                         delivery_service: ds.id,
//                     };
//                     return {
//                         ...commonData,
//                         pickup_time_id: foundPT ? foundPT.id : null,
//                         pickup_time_code: foundPT ? foundPT.pickup_time_code : '',
//                         pickup_time_start: foundPT ? foundPT.pickup_time_start : '',
//                         pickup_time_end: foundPT ? foundPT.pickup_time_end : '',
//                         cargo_export_time: foundPT ? foundPT.cargo_export_time : '',
//                     };
//                 }),
//             })),
//         ],
//         [deliveryServices, store?.pickup_times]
//     );

//     const initialValues: StoresFormValues = useMemo(
//         () => ({
//             seller_id: store?.seller_id,
//             name: store?.name,
//             code: store?.xml_id,
//             address: store?.address as any,
//             porch: store?.address?.porch as any,
//             floor: store?.address.floor as any,
//             intercome: store?.address?.intercom,
//             timezone: { label: store?.timezone, value: store?.timezone },
//             active: store?.active,
//             comment: store?.address?.comment,
//             workings,
//             pickupTimes,
//             contacts: store?.contacts,
//         }),
//         [
//             pickupTimes,
//             store?.active,
//             store?.address,
//             store?.contacts,
//             store?.name,
//             store?.seller_id,
//             store?.timezone,
//             store?.xml_id,
//             workings,
//         ]
//     );

//     return (
//         <PageWrapper h1={`Редактирование склада #${storeID}`}>
//             <>
//                 <Form
//                     initialValues={initialValues}
//                     validationSchema={Yup.object().shape({
//                         seller_id: Yup.object().required(ErrorMessages.REQUIRED),
//                         name: Yup.string().required(ErrorMessages.REQUIRED),
//                         address: Yup.object().required(ErrorMessages.REQUIRED),
//                         timezone: Yup.object().required(ErrorMessages.REQUIRED),
//                         contacts: Yup.array()
//                             .nullable()
//                             .of(
//                                 Yup.object().shape({
//                                     email: Yup.string().email(ErrorMessages.EMAIL).required(ErrorMessages.REQUIRED),
//                                     phone: Yup.string()
//                                         .matches(regPhone, ErrorMessages.PHONE)
//                                         .required(ErrorMessages.REQUIRED),
//                                     name: Yup.string().required(ErrorMessages.REQUIRED),
//                                 })
//                             ),
//                     })}
//                     onSubmit={async (values: StoresFormValues) => {
//                         /** обновим workings */
//                         await Promise.all(
//                             values?.workings?.map(async w => {
//                                 const workingsData = { ...w, store_id: +storeID };
//                                 if (workingsData.id) {
//                                     await updateStoreWorkings.mutateAsync({ ...workingsData, id: workingsData.id });
//                                 } else if (workingsData.active) {
//                                     delete workingsData.id;
//                                     await createStoreWorkings.mutateAsync(workingsData);
//                                 }
//                             }) || []
//                         );

//                         /** обновим pickuptimes */
//                         await Promise.all(
//                             (values?.pickupTimes || []).flatMap(async (p, index) =>
//                                 p.pickupTimes.map(async (item, ptIndex) => {
//                                     const oldPickupTime = initialValues.pickupTimes
//                                         ? initialValues.pickupTimes[index].pickupTimes[ptIndex]
//                                         : null;

//                                     if (oldPickupTime) {
//                                         const updatedPickupTime = {
//                                             store_id: +storeID,
//                                             day: item.day,
//                                             pickup_time_code: item.pickup_time_code,
//                                             pickup_time_start: item.pickup_time_start,
//                                             pickup_time_end: item.pickup_time_end,
//                                             cargo_export_time: item.cargo_export_time,
//                                             delivery_service: p.delivery_service,
//                                         };
//                                         if (
//                                             oldPickupTime.cargo_export_time !== updatedPickupTime.cargo_export_time ||
//                                             oldPickupTime.pickup_time_code !== updatedPickupTime.pickup_time_code ||
//                                             oldPickupTime.pickup_time_start !== updatedPickupTime.pickup_time_start ||
//                                             oldPickupTime.pickup_time_end !== updatedPickupTime.pickup_time_end
//                                         ) {
//                                             if (item.pickup_time_id) {
//                                                 updatePickupTime.mutateAsync({
//                                                     id: item.pickup_time_id,
//                                                     ...(updatedPickupTime as StorePickupTimeForPatch),
//                                                 });
//                                             } else {
//                                                 createPickupTime.mutateAsync(
//                                                     updatedPickupTime as StorePickupTimeForCreate
//                                                 );
//                                             }
//                                         }
//                                     }
//                                 })
//                             )
//                         );

//                         /** Обновим контакты */
//                         values?.contacts?.forEach(contact => {
//                             const updatedContact = {
//                                 store_id: +storeID,
//                                 name: contact.name,
//                                 email: contact.email,
//                                 phone: contact.phone,
//                             };
//                             const oldContact = initialValues.contacts?.find(c => c.id === contact.id);
//                             if (
//                                 oldContact?.email !== updatedContact.email ||
//                                 oldContact?.phone !== updatedContact.phone ||
//                                 oldContact?.name !== updatedContact.name
//                             ) {
//                                 if (contact.id) {
//                                     updateContact.mutate({ ...updatedContact, id: contact.id });
//                                 } else {
//                                     createContact.mutate(updatedContact);
//                                 }
//                             }
//                         });

//                         /** удалим лишние контакты */
//                         const contactsToDelete = initialValues?.contacts?.filter(
//                             c => !values.contacts?.find(v => v.id === c.id)
//                         );

//                         if (contactsToDelete && contactsToDelete.length > 0) {
//                             await Promise.all(
//                                 contactsToDelete.map(async c => {
//                                     deleteContact.mutateAsync({
//                                         id: c.id,
//                                     });
//                                 })
//                             );
//                         }
//                     }}
//                     enableReinitialize
//                 >
//                     <div css={{ display: 'flex' }}>
//                         <EditStore
//                             initialValues={initialValues}
//                             needBtns={false}
//                             css={{ maxWidth: scale(128), marginBottom: scale(4) }}
//                         />
//                         <div
//                             css={{
//                                 position: 'relative',
//                                 marginLeft: scale(2),
//                                 flexShrink: 0,
//                                 flexGrow: 1,
//                                 textAlign: 'center',
//                             }}
//                         >
//                             <div css={{ position: 'sticky', top: scale(3) }}>
//                                 <FormReset theme="secondary">Отменить</FormReset>
//                                 <Button theme="primary" css={{ marginLeft: scale(2) }} type="submit">
//                                     Сохранить
//                                 </Button>
//                             </div>
//                         </div>
//                     </div>
//                     <Tabs {...getTabsProps()}>
//                         <Tabs.List>
//                             <Tabs.Tab>График работы</Tabs.Tab>
//                             <Tabs.Tab>График отгрузки</Tabs.Tab>
//                             <Tabs.Tab>Контактные лица</Tabs.Tab>
//                         </Tabs.List>
//                         <Tabs.Panel>
//                             <Workings lineCSS={lineCSS} />
//                         </Tabs.Panel>
//                         <Tabs.Panel>
//                             <PickupTimes lineCSS={lineCSS} />
//                         </Tabs.Panel>
//                         <Tabs.Panel>
//                             <Contacts lineCSS={lineCSS} />
//                         </Tabs.Panel>
//                     </Tabs>
//                 </Form>
//             </>
//         </PageWrapper>
//     );
// };

// export default SellerStoresEdit;

export default function SellerStoresEdit() {}
