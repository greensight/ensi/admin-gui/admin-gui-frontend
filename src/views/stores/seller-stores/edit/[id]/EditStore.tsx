// import { useCallback } from 'react';

// import { CommonResponse } from '@api/common/types';
// import { loadAddresses } from '@api/dadata';
// import { useAuthApiClient } from '@api/hooks/useAuthApiClient';
// import { Seller, SellerFilter } from '@api/units';

// import Autocomplete from '@controls/Autocomplete';
// import { Block } from '@ensi-platform/core-components';

// import {Form, FormFieldWrapper} from '@ensi-platform/core-components';
// import Select from '@controls/Select';
// import Switcher from '@controls/Switcher';

// import { Button, Layout, scale } from '@scripts/gds';
// import { useTimezones } from '@scripts/hooks';

// interface EditStoreProps {
//     className?: string;
//     needBtns?: boolean;
//     initialValues: FormikValues;
// }

// const EditStore = ({ className, needBtns = true, initialValues }: EditStoreProps) => {
//     const timezones = useTimezones();

//     const apiClient = useAuthApiClient();

//     const loadSellers = useCallback(async (inputValue?: string) => {
//         try {
//             const data: { filter: Partial<SellerFilter> } = { filter: { legal_name: inputValue || '' } };
//             const apiSellers: CommonResponse<Seller[]> = await apiClient.post('units/sellers:search', { data });
//             return apiSellers.data?.map(s => ({ label: s.legal_name || +s.id, value: s.id })) || [];
//         } catch (err) {
//             return [];
//         }
//     }, []);
//     return (
//         <Block className={className}>
//             <Block.Body>
//                 <Layout cols={3} css={{ maxWidth: scale(128) }}>
//                     <Layout.Item col={3}>
//                         <FormField name="seller_id" label="Продавец">
//                             <Autocomplete searchAsyncFunc={loadSellers} />
//                         </FormField>
//                     </Layout.Item>
//                     <Layout.Item col={2}>
//                         <FormField name="name" label="Название склада" />
//                     </Layout.Item>
//                     <Layout.Item col={1}>
//                         <FormField name="code" label="Внешний код" />
//                     </Layout.Item>
//                     <Layout.Item col={3}>
//                         <FormField name="address" label="Адрес" hint="Начните вводить адрес">
//                             <Autocomplete searchAsyncFunc={loadAddresses} />
//                         </FormField>
//                     </Layout.Item>
//                     <Layout.Item col={1}>
//                         <FormField name="porch" label="Подъезд" />
//                     </Layout.Item>
//                     <Layout.Item col={1}>
//                         <FormField name="floor" label="Этаж" />
//                     </Layout.Item>
//                     <Layout.Item col={1}>
//                         <FormField name="intercom" label="Домофон" />
//                     </Layout.Item>
//                     <Layout.Item col={3}>
//                         <FormField name="comment" label="Комментарий к адресу" />
//                     </Layout.Item>
//                     <Layout.Item col={3}>
//                         <FormField name="timezone" label="Таймзона">
//                             <Select items={timezones} />
//                         </FormField>
//                     </Layout.Item>
//                     <Layout.Item col={1}>
//                         <FormField name="active">
//                             <Switcher>Активность</Switcher>
//                         </FormField>
//                     </Layout.Item>
//                 </Layout>
//             </Block.Body>
//             {needBtns ? (
//                 <Block.Footer css={{ justifyContent: 'flex-end' }}>
//                     <FormReset theme="secondary" css={{ marginRight: scale(2) }} initialValues={initialValues}>
//                         Отменить
//                     </FormReset>
//                     <Button theme="primary" type="submit">
//                         Добавить
//                     </Button>
//                 </Block.Footer>
//             ) : null}
//         </Block>
//     );
// };

// export default EditStore;
export {};
