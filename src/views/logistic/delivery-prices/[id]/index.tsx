import {
    AutocompleteAsync,
    CopyButton,
    DescriptionList,
    DescriptionListItem,
    FormField,
    FormFieldWrapper,
    FormTypedField,
    Select,
    SelectItem,
    getDefaultDates,
    useActionPopup,
    useFormContext,
} from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { useMemo, useRef, useState } from 'react';
import * as Yup from 'yup';

import { useAuthApiClient } from '@api/hooks/useAuthApiClient';
import {
    CreateDeliveryPriceRequest,
    DeliveryPrice,
    LogisticDeliveryMethodEnum,
    LogisticDeliveryServiceEnum,
    getRegion,
    useCreateDeliveryPrice,
    useDeleteDeliveryPrice,
    useDeliveryMethods,
    useDeliveryPrice,
    useDeliveryServiceAutocomplete,
    usePatchDeliveryPrice,
} from '@api/logistic';
import { useFederalDistrictAutocomplete } from '@api/logistic/federal-districts';
import { useRegionAutocomplete } from '@api/logistic/regions';

import { useError } from '@context/modal';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import PageLeaveGuard from '@components/PageLeaveGuard';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { CREATE_PARAM, ErrorMessages } from '@scripts/constants';
import { ActionType, RedirectMethods } from '@scripts/enums';
import { Layout, MEDIA_QUERIES, scale } from '@scripts/gds';
import { fromKopecksToRouble, fromRoubleToKopecks, isPageNotFound, toSelectItems } from '@scripts/helpers';

import PreloaderIcon from '@icons/preloader.svg';

import { useDeliveryPricesAccess } from '../useDeliveryPricesAccess';

const LISTING_URL = '/logistic/delivery-prices';

interface FormFields {
    federal_district_id: number;
    region_id: number;
    region_guid: string;
    price: number;
    delivery_method: number;
    delivery_service: number;
}

const FormInner = ({
    onDelete,
    onApply,
    onSubmit,
    deliveryPrice,
}: {
    deliveryPrice?: DeliveryPrice;
    onSubmit: () => void;
    onApply: () => void;
    onDelete: () => void;
}) => {
    const { query, push } = useRouter();
    const deliveryPriceId = Array.isArray(query.id) ? undefined : query.id;

    const isCreation = deliveryPriceId === CREATE_PARAM;

    const { access } = useDeliveryPricesAccess();

    const { federalDistrictOptionsByValuesFn, federalDistrictSearchFn } = useFederalDistrictAutocomplete();
    const { deliveryServiceSearchFn, deliveryServiceOptionsByValuesFn } = useDeliveryServiceAutocomplete();
    const { data: apiMethods } = useDeliveryMethods();
    const methods = useMemo(() => toSelectItems(apiMethods?.data), [apiMethods]);

    const apiClient = useAuthApiClient();

    const { setValue, watch } = useFormContext<FormFields>();
    const [isGuidLoading, setGuidLoading] = useState(false);

    const federalDistrictId = watch('federal_district_id');

    const { regionSearchFn, regionOptionsByValuesFn } = useRegionAutocomplete({
        federalDistrictId,
    });

    return (
        <PageTemplate
            h1={isCreation ? `Создание стоимости доставки` : `Редактирование стоимости доставки #${deliveryPriceId}`}
            backlink={{ text: 'Назад', href: LISTING_URL }}
            controls={
                <PageControls access={access}>
                    <PageControls.Delete onClick={onDelete} />
                    <PageControls.Close onClick={() => push(LISTING_URL)} />
                    <PageControls.Apply onClick={onApply} />
                    <PageControls.Save onClick={onSubmit} />
                </PageControls>
            }
            aside={
                <DescriptionList>
                    <DescriptionListItem
                        name="ID"
                        value={!isCreation ? <CopyButton>{`${deliveryPriceId}`}</CopyButton> : '-'}
                    />
                    {getDefaultDates({ ...deliveryPrice }).map(item => (
                        <DescriptionListItem key={item.name} {...item} />
                    ))}
                </DescriptionList>
            }
        >
            <Layout
                cols={2}
                gap={scale(2)}
                css={{
                    ...(isCreation && {
                        [MEDIA_QUERIES.mdMin]: {
                            maxWidth: '50%',
                        },
                    }),
                }}
            >
                <Layout.Item col={2}>
                    <FormFieldWrapper name="federal_district_id" label="Федеральный округ">
                        <AutocompleteAsync
                            asyncOptionsByValuesFn={federalDistrictOptionsByValuesFn}
                            asyncSearchFn={federalDistrictSearchFn}
                        />
                    </FormFieldWrapper>
                </Layout.Item>
                <Layout.Item col={1}>
                    <FormFieldWrapper name="region_id" label="Регион">
                        <AutocompleteAsync
                            asyncOptionsByValuesFn={regionOptionsByValuesFn}
                            asyncSearchFn={regionSearchFn}
                            onChange={(payload: {
                                selected: SelectItem[] | null;
                                actionItem?: SelectItem | null;
                                name?: string;
                            }) => {
                                if (!payload.actionItem?.value) return;
                                setGuidLoading(true);

                                getRegion(apiClient, Number(payload.actionItem.value)).then(val => {
                                    setValue('region_guid', val.data.guid);
                                    setGuidLoading(false);
                                });
                            }}
                        />
                    </FormFieldWrapper>
                </Layout.Item>
                <Layout.Item col={1}>
                    <FormField
                        name="region_guid"
                        label="ФИАС региона"
                        rightAddons={isGuidLoading ? <PreloaderIcon width={scale(2)} /> : null}
                    />
                </Layout.Item>
                <Layout.Item col={2}>
                    <FormFieldWrapper name="delivery_method" label="Способ доставки">
                        <Select options={methods} hideClearButton />
                    </FormFieldWrapper>
                </Layout.Item>
                <Layout.Item col={2}>
                    <FormFieldWrapper name="delivery_service" label="Служба доставки">
                        <AutocompleteAsync
                            asyncOptionsByValuesFn={deliveryServiceOptionsByValuesFn}
                            asyncSearchFn={deliveryServiceSearchFn}
                        />
                    </FormFieldWrapper>
                </Layout.Item>
                <Layout.Item col={2}>
                    <FormTypedField fieldType="positiveFloat" name="price" label="Стоимость" />
                </Layout.Item>
            </Layout>
        </PageTemplate>
    );
};

const DeliveryPriceCreate = () => {
    const { access, isIDForbidden } = useDeliveryPricesAccess();
    const { query, push } = useRouter();

    const deliveryPriceId = Array.isArray(query.id) ? NaN : Number(query.id);
    const isCreation = query.id === CREATE_PARAM;

    const { data, isFetching: isLoading, error } = useDeliveryPrice({ id: deliveryPriceId }, undefined, access.ID.view);
    const deliveryPrice = data?.data;

    useError(error);

    const initialValues = useMemo(
        () => ({
            federal_district_id: deliveryPrice?.federal_district_id || '',
            region_id: deliveryPrice?.region_id || '',
            region_guid: deliveryPrice?.region_guid || '',
            price: typeof deliveryPrice?.price === 'number' ? fromKopecksToRouble(deliveryPrice.price) : '',
            delivery_method: deliveryPrice?.delivery_method || '',
            delivery_service: deliveryPrice?.delivery_service || '',
        }),
        [deliveryPrice]
    );
    const createDeliveryPrice = useCreateDeliveryPrice();
    const updateDeliveryPrice = usePatchDeliveryPrice();
    const deleteDeliveryPrice = useDeleteDeliveryPrice();

    const { popupState, popupDispatch, ActionPopup, ActionEnum } = useActionPopup();

    useError(createDeliveryPrice.error || updateDeliveryPrice.error);
    const returnToListing = useRef(false);

    const canEdit = isCreation ? access.ID.create : access.ID.edit;

    const isNotFound = isPageNotFound({ id: deliveryPriceId, error, isCreation });

    return (
        <PageWrapper
            title="Редактирование стоимость доставки"
            isForbidden={isIDForbidden}
            isLoading={
                createDeliveryPrice.isPending ||
                updateDeliveryPrice.isPending ||
                deleteDeliveryPrice.isPending ||
                isLoading
            }
            isNotFound={isNotFound}
        >
            <FormWrapper
                disabled={!canEdit}
                initialValues={initialValues}
                validationSchema={Yup.object().shape({
                    federal_district_id: Yup.number()
                        .transform(value => (Number.isNaN(+value) ? undefined : value))
                        .required(ErrorMessages.REQUIRED),
                    delivery_method: Yup.number()
                        .transform(value => (Number.isNaN(+value) ? undefined : value))
                        .required(ErrorMessages.REQUIRED),
                })}
                enableReinitialize
                onSubmit={async values => {
                    const newData: CreateDeliveryPriceRequest = {
                        delivery_method: values.delivery_method as LogisticDeliveryMethodEnum,
                        federal_district_id: values.federal_district_id as number,
                        price: fromRoubleToKopecks(values.price as number),
                        delivery_service: (values.delivery_service as LogisticDeliveryServiceEnum) || null,
                        region_id: (values.region_id as number) || undefined,
                        region_guid: values.region_guid || null,
                        // region_guid: (await getRegion(apiClient, values.region_id as number)).data.guid,
                    };

                    if (isCreation) {
                        const result = await createDeliveryPrice.mutateAsync(newData);

                        if (returnToListing.current) {
                            return {
                                method: RedirectMethods.push,
                                redirectPath: LISTING_URL,
                            };
                        }

                        return {
                            method: RedirectMethods.replace,
                            redirectPath: `${LISTING_URL}/${result.data.id}`,
                        };
                    }

                    await updateDeliveryPrice.mutateAsync({
                        id: deliveryPriceId,
                        ...newData,
                    });

                    if (returnToListing.current) {
                        return {
                            method: RedirectMethods.push,
                            redirectPath: LISTING_URL,
                        };
                    }

                    return {
                        method: RedirectMethods.replace,
                        redirectPath: `${LISTING_URL}/${deliveryPriceId}`,
                    };
                }}
            >
                {({ reset }) => (
                    <>
                        <PageLeaveGuard />
                        <FormInner
                            deliveryPrice={deliveryPrice}
                            onDelete={() =>
                                popupDispatch({
                                    type: ActionType.Delete,
                                    payload: {
                                        title: `Вы уверены, что хотите удалить стоимость доставки ${deliveryPrice?.id}?`,
                                        popupAction: ActionEnum.DELETE,
                                        onAction: async () => {
                                            if (deliveryPrice?.id) {
                                                reset();
                                                await deleteDeliveryPrice.mutateAsync({ id: deliveryPrice.id });
                                                push(LISTING_URL);
                                            }
                                        },
                                    },
                                })
                            }
                            onApply={() => {
                                returnToListing.current = false;
                            }}
                            onSubmit={() => {
                                returnToListing.current = true;
                            }}
                        />
                    </>
                )}
            </FormWrapper>
            <ActionPopup popupDispatch={popupDispatch} popupState={popupState} />
        </PageWrapper>
    );
};

export default DeliveryPriceCreate;
