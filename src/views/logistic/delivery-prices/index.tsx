import { useActionPopup } from '@ensi-platform/core-components';
import { useCallback, useMemo } from 'react';

import { DeliveryPrice, useDeleteDeliveryPrice, useDeliveryPriceMeta, useSearchDeliveryPrices } from '@api/logistic';

import { useError } from '@context/modal';

import { useDeliveryPricesAccess } from '@views/logistic/delivery-prices/useDeliveryPricesAccess';

import ListBuilder from '@components/ListBuilder';
import { ExtendedRow, TooltipItem } from '@components/Table';

import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

const DeliveryPrices = () => {
    const { access } = useDeliveryPricesAccess();

    const deleteNameplate = useDeleteDeliveryPrice();
    useError(deleteNameplate.error);
    const { popupState, popupDispatch, ActionPopup, ActionEnum, ActionType } = useActionPopup();

    const goToDetailPage = useGoToDetailPage({ extraConditions: access.ID.view });

    const goToDeleteRow = useCallback(
        (originalRows: ExtendedRow['original'][] | undefined) => {
            if (originalRows) {
                popupDispatch({
                    type: ActionType.Delete,
                    payload: {
                        title: `Вы уверены, что хотите удалить стоимость доставки #${originalRows[0].original.id}?`,
                        popupAction: ActionEnum.DELETE,
                        onAction: async () => {
                            try {
                                await deleteNameplate.mutateAsync({
                                    id: Number(originalRows[0].original.id)!,
                                });
                            } catch (err) {
                                console.error(err);
                            }
                        },
                    },
                });
            }
        },
        [ActionEnum.DELETE, ActionType.Delete, deleteNameplate, popupDispatch]
    );

    const tooltipContent = useMemo<TooltipItem[]>(
        () => [
            {
                type: 'edit',
                text: 'Редактировать стоимость доставки',
                action: goToDetailPage,
                isDisable: !access.ID.view,
            },
            {
                type: 'delete',
                text: 'Удалить стоимость доставки',
                action: goToDeleteRow,
                isDisable: !access.ID.delete,
            },
        ],
        [access.ID, goToDeleteRow, goToDetailPage]
    );

    return (
        <ListBuilder<DeliveryPrice>
            access={access}
            searchHook={useSearchDeliveryPrices}
            metaHook={useDeliveryPriceMeta}
            isLoading={deleteNameplate.isPending}
            tooltipItems={tooltipContent}
            title="Стоимости доставки"
        >
            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </ListBuilder>
    );
};

export default DeliveryPrices;
