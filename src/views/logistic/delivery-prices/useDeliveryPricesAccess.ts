import { defineAccessMatrix, useAccess } from '@scripts/hooks';
import { useIDForbidden } from '@scripts/hooks/useIDForbidden';

export const accessMatrix = defineAccessMatrix({
    LIST: {
        view: 2101,
    },
    ID: {
        view: 2102,
        create: 2103,
        edit: 2104,
        delete: 2105,
    },
});

export const useDeliveryPricesAccess = () => {
    const access = useAccess(accessMatrix);
    const isIDForbidden = useIDForbidden(access);

    return { access, isIDForbidden };
};
