// FIXME Логистические операторы не используется, но возможно потребуется в будущем

// import { useRouter } from 'next/router';
// import { useMemo } from 'react';
//
// import { useDeliveryServices, useDeliveryStatuses } from '@api/logistic';
//
// import { useError } from '@context/modal';
//
// import { Block } from '@ensi-platform/core-components';
// import OldTable from '@components/OldTable';
// import PageWrapper from '@components/PageWrapper';
// import {Form, FormFieldWrapper} from '@ensi-platform/core-components';
// import Pagination from '@controls/Pagination';
// import Select from '@controls/Select';
//
// import { LIMIT_PAGE } from '@scripts/constants';
// import { Button, Layout, scale } from '@scripts/gds';
// import { getTotalPages } from '@scripts/helpers';
// import { useActivePage, useFiltersHelper } from '@scripts/hooks';
//
// const COLUMNS = [
//     {
//         Header: 'ID',
//         accessor: 'id',
//         getProps: () => ({ type: 'linkedID' }),
//     },
//     {
//         Header: 'Название',
//         accessor: 'name',
//     },
//     {
//         Header: 'Статус',
//         accessor: 'statusName',
//     },
//     {
//         Header: 'Приоритет',
//         accessor: 'priority',
//     },
// ];
//
// const emptyInitialValues = {
//     id: '',
//     name: '',
//     status: '',
// };
//
// const DeliveryServicesFilter = ({
//     initialValues,
//     statuses,
//     onSubmit,
//     onReset,
// }: {
//     initialValues: FieldValues;
//     statuses: { label: string; value: string }[];
//     onSubmit: (filters: FieldValues) => void;
//     onReset: () => void;
// }) => (
//     <Block css={{ marginBottom: scale(3) }}>
//         <Form initialValues={initialValues} onSubmit={onSubmit} onReset={onReset}>
//             <Block.Body>
//                 <Layout cols={3}>
//                     <Layout.Item col={1}>
//                         <FormField name="id" label="ID" type="number" />
//                     </Layout.Item>
//                     <Layout.Item col={1}>
//                         <FormField name="name" label="Название" />
//                     </Layout.Item>
//                     <Layout.Item col={1}>
//                         <FormFieldWrapper name="status" label="Статус">
//                             <Select items={statuses} />
//                         </FormFieldWrapper>
//                     </Layout.Item>
//                 </Layout>
//             </Block.Body>
//             <Block.Footer css={{ justifyContent: 'flex-end' }}>
//                 <div>
//                     <Button theme="primary" type="submit">
//                         Применить
//                     </Button>
//                     <FormReset
//                         css={{ marginLeft: scale(2) }}
//                         theme="secondary"
//                         type="button"
//                         initialValues={emptyInitialValues}
//                     >
//                         Сбросить
//                     </FormReset>
//                 </div>
//             </Block.Footer>
//         </Form>
//     </Block>
// );
//
// const DeliveryServices = () => {
//     const { pathname, push } = useRouter();
//     const activePage = useActivePage();
//     const { initialValues, URLHelper } = useFiltersHelper(emptyInitialValues);
//
//     const {
//         data: apiData,
//         isFetching: isLoading,
//         error,
//     } = useDeliveryServices({
//         filter: {
//             id: initialValues.id || undefined,
//             name: initialValues.name || undefined,
//             status: initialValues.status || undefined,
//         },
//         pagination: { type: 'offset', limit: LIMIT_PAGE, offset: (activePage - 1) * LIMIT_PAGE },
//     });
//
//     const { data: apiStatuses, error: apiStatusesError } = useDeliveryStatuses();
//     useError(error || apiStatusesError);
//
//     const statuses = useMemo(
//         () =>
//             apiStatuses && apiStatuses.data && apiStatuses.data.length > 0
//                 ? apiStatuses.data.map(i => ({ label: i.name, value: `${i.id}` }))
//                 : [],
//         [apiStatuses]
//     );
//
//     const columnsData = useMemo(
//         () =>
//             apiData?.data?.map(item => ({
//                 ...item,
//                 statusName: statuses.find(status => +status.value === item.status)?.label,
//             })) || [],
//         [apiData, statuses]
//     );
//
//     const totalPages = getTotalPages(apiData);
//
//     return (
//         <PageWrapper
//             h1="Логистические операторы"
//             isLoading={isLoading}
//             error={error ? JSON.stringify(error) : undefined}
//         >
//             <DeliveryServicesFilter
//                 initialValues={initialValues}
//                 statuses={statuses}
//                 onSubmit={URLHelper}
//                 onReset={() => push(pathname)}
//             />
//
//             <Block>
//                 <Block.Body>
//                     {columnsData.length > 0 ? (
//                         <OldTable
//                             columns={COLUMNS}
//                             data={columnsData}
//                             needCheckboxesCol={false}
//                             needSettingsColumn={false}
//                         />
//                     ) : (
//                         <p>Логистические операторы не найдены</p>
//                     )}
//                     <Pagination pages={totalPages} />
//                 </Block.Body>
//             </Block>
//         </PageWrapper>
//     );
// };
//
// export default DeliveryServices;
