import {
    CopyButton,
    DescriptionList,
    DescriptionListItem,
    FormFieldWrapper,
    Tabs,
    getDefaultDates,
} from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { useCallback, useMemo, useRef } from 'react';

import { useAuthApiClient } from '@api/hooks/useAuthApiClient';
import { Point, searchOneRegion, usePatchPoint, usePoint } from '@api/logistic';

import { useError } from '@context/modal';

import Switcher from '@controls/Switcher';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { RedirectMethods } from '@scripts/enums';
import { isPageNotFound } from '@scripts/helpers';
import { useTabs } from '@scripts/hooks/useTabs';

import { usePickupPointsAccess } from '../usePickupPointsAccess';
import { AddressTab } from './components/AddressTab';
import { MainTab } from './components/MainTab';
import { WorkingsTab } from './components/WorkingsTab';

const LISTING_URL = '/logistic/pickup-points';

const FormInner = ({
    onApply,
    onSubmit,
    pickupPoint,
}: {
    pickupPoint?: Point;
    onApply: () => void;
    onSubmit: () => void;
}) => {
    const { push } = useRouter();

    const { getTabsProps } = useTabs();

    const access = usePickupPointsAccess();

    return (
        <PageTemplate
            customChildren
            h1={`Редактирование ПВЗ #${pickupPoint?.id}`}
            backlink={{ text: 'Назад', href: LISTING_URL }}
            controls={
                <PageControls access={access}>
                    <PageControls.Close onClick={() => push(LISTING_URL)} />
                    <PageControls.Apply onClick={onApply} />
                    <PageControls.Save onClick={onSubmit} />
                </PageControls>
            }
            aside={
                <DescriptionList>
                    <DescriptionListItem name="ID" value={<CopyButton>{`${pickupPoint?.id}`}</CopyButton>} />
                    <DescriptionListItem
                        name="Внешний идентификатор"
                        value={<CopyButton>{`${pickupPoint?.external_id}`}</CopyButton>}
                    />
                    <DescriptionListItem name="Название" value={`${pickupPoint?.name}`} />
                    <DescriptionListItem
                        name="Номер телефона"
                        value={<CopyButton>{`${pickupPoint?.phone}`}</CopyButton>}
                    />
                    <DescriptionListItem
                        name="Активность"
                        value={
                            <FormFieldWrapper name="is_active" label="Активность" disabled={!access.ID.editMainData}>
                                <Switcher>Активен</Switcher>
                            </FormFieldWrapper>
                        }
                    />
                    {getDefaultDates({ ...pickupPoint }).map(item => (
                        <DescriptionListItem key={item.name} {...item} />
                    ))}
                </DescriptionList>
            }
        >
            <Tabs {...getTabsProps()} css={{ flexShrink: 0, flexGrow: 1 }}>
                <Tabs.Tab id="0" title="Основные данные">
                    <MainTab pickupPoint={pickupPoint} />
                </Tabs.Tab>
                <Tabs.Tab id="1" title="Адрес">
                    <AddressTab />
                </Tabs.Tab>
                <Tabs.Tab id="2" title="Время работы">
                    <WorkingsTab pickupPoint={pickupPoint} />
                </Tabs.Tab>
            </Tabs>
        </PageTemplate>
    );
};

const PickupPointCreate = () => {
    const access = usePickupPointsAccess();
    const { query } = useRouter();

    const pointId = Array.isArray(query.id) ? NaN : Number(query.id);

    const {
        data,
        isFetching: isLoading,
        error,
    } = usePoint(
        { id: pointId },
        {
            include: 'point_workings',
        },
        access.ID.view
    );

    useError(error);

    const pickupPoint = data?.data;

    const fieldsOrDefault = useCallback(
        <TObject extends Record<string, any>, TKeys extends keyof TObject>(
            obj: TObject,
            keys: TKeys[],
            defaultValue: any = ''
        ) => {
            const result = {} as Record<string, any>;

            keys.forEach(key => {
                result[key as string] = (obj || {})[key] || defaultValue;
            });

            return result as Required<{
                [K in TKeys]: Exclude<TObject[K], undefined>;
            }>;
        },
        []
    );

    const initialValues = useMemo(
        () => ({
            ...fieldsOrDefault(pickupPoint!, [
                'delivery_service_id',
                'name',
                'external_id',
                'description',
                'address_reduce',
                'city_guid',
                'phone',
                'geo_lat',
                'geo_lon',
                'timezone',
            ]),
            is_active: pickupPoint?.is_active || false,
            address: fieldsOrDefault(pickupPoint?.address!, [
                'address_string',
                'country_code',
                'post_index',
                'region',
                'region_guid',
                'area',
                'area_guid',
                'city',
                'city_guid',
                'street',
                'house',
                'block',
                'flat',
                // 'company_name',
                // 'contact_name',
                // 'email',
                // 'phone',
                // 'comment',
                'floor',
                'porch',
                'intercom',
                'geo_lat',
                'geo_lon',
            ]),
        }),
        [fieldsOrDefault, pickupPoint]
    );
    const updatePickupPoint = usePatchPoint();

    useError(updatePickupPoint.error);
    const returnToListing = useRef(false);

    const apiClient = useAuthApiClient();

    const isNotFound = isPageNotFound({ id: pointId, error });

    return (
        <PageWrapper
            title="Редактирование стоимости доставки"
            isForbidden={!access.ID.view}
            isLoading={updatePickupPoint.isPending || isLoading}
            isNotFound={isNotFound}
        >
            <FormWrapper
                initialValues={initialValues}
                enableReinitialize
                onSubmit={async values => {
                    const request = values.address.region
                        ? searchOneRegion(apiClient, {
                              filter: {
                                  name_like: values.address.region,
                              },
                              pagination: {
                                  type: 'offset',
                                  limit: -1,
                                  offset: 0,
                              },
                          })
                        : Promise.resolve({ data: null });

                    const { data: region } = await request;

                    await updatePickupPoint.mutateAsync({
                        id: pointId,
                        address: {
                            ...values.address,
                            region: region?.name,
                            region_guid: region?.guid,
                        },
                        city_guid: values.city_guid || null,
                        delivery_service_id: values.delivery_service_id || null,
                        geo_lat: values.geo_lat || null,
                        geo_lon: values.geo_lon || null,
                        is_active: values.is_active || false,
                        timezone: values.timezone || null,
                    });

                    if (returnToListing.current) {
                        return {
                            method: RedirectMethods.push,
                            redirectPath: LISTING_URL,
                        };
                    }

                    return {
                        method: RedirectMethods.replace,
                        redirectPath: `${LISTING_URL}/${pointId}`,
                    };
                }}
            >
                <FormInner
                    pickupPoint={pickupPoint}
                    onApply={() => {
                        returnToListing.current = false;
                    }}
                    onSubmit={() => {
                        returnToListing.current = true;
                    }}
                />
            </FormWrapper>
        </PageWrapper>
    );
};

export default PickupPointCreate;
