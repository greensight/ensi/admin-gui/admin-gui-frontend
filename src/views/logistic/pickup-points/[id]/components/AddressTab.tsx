import { AutocompleteAsync, Block, FormField, FormFieldWrapper, useFormContext } from '@ensi-platform/core-components';

import { useRegionAutocomplete } from '@api/logistic/regions';

import { Layout } from '@scripts/gds';

import { usePickupPointsAccess } from '../../usePickupPointsAccess';

export const AddressTab = () => {
    const { regionSearchNameFn, regionOptionsByNameFn } = useRegionAutocomplete();

    const { setValue } = useFormContext();

    const access = usePickupPointsAccess();

    return (
        <Block>
            <Block.Body>
                <Layout cols={{ xxxl: 2, xs: 2 }}>
                    <Layout.Item col={{ xxxl: 2, xs: 2 }}>
                        <FormField
                            label="Полный адрес"
                            name="address.address_string"
                            disabled={!access.ID.editAddress}
                        />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                        <FormField label="Код страны" name="address.country_code" disabled={!access.ID.editAddress} />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                        <FormField
                            label="Почтовый индекс"
                            name="address.post_index"
                            disabled={!access.ID.editAddress}
                        />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                        <FormFieldWrapper label="Регион" name="address.region" disabled={!access.ID.editAddress}>
                            <AutocompleteAsync
                                asyncOptionsByValuesFn={regionOptionsByNameFn}
                                asyncSearchFn={regionSearchNameFn}
                                onClear={() => {
                                    setValue('address.region_guid', null);
                                }}
                            />
                        </FormFieldWrapper>
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                        <FormField label="Область" name="address.area" disabled={!access.ID.editAddress} />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                        <FormField label="GUID области" name="address.area_guid" disabled={!access.ID.editAddress} />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                        <FormField label="Город" name="address.city" disabled={!access.ID.editAddress} />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                        <FormField label="GUID города" name="address.city_guid" disabled={!access.ID.editAddress} />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                        <FormField label="Улица" name="address.street" disabled={!access.ID.editAddress} />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                        <FormField label="Дом" name="address.house" disabled={!access.ID.editAddress} />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                        <FormField label="Корпус/строение" name="address.block" disabled={!access.ID.editAddress} />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                        <FormField label="Квартира/офис" name="address.flat" disabled={!access.ID.editAddress} />
                    </Layout.Item>
                    {/* <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                        <FormField label="Компания" name="address.company_name"  disabled={!access.ID.editAddress} />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                        <FormField label="Контактное лицо" name="address.contact_name"  disabled={!access.ID.editAddress} />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                        <FormField label="Контактный email" name="address.email"  disabled={!access.ID.editAddress} />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                        <FormField label="Контактный телефон" name="address.phone"  disabled={!access.ID.editAddress} />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                        <FormField label="Комментарий" name="address.comment"  disabled={!access.ID.editAddress} />
                    </Layout.Item> */}
                    <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                        <FormField label="Этаж" name="address.floor" disabled={!access.ID.editAddress} />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                        <FormField label="Подъезд" name="address.porch" disabled={!access.ID.editAddress} />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                        <FormField label="Код домофона" name="address.intercom" disabled={!access.ID.editAddress} />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                        <FormField label="Широта" name="address.geo_lat" disabled={!access.ID.editAddress} />
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                        <FormField label="Долгота" name="address.geo_lon" disabled={!access.ID.editAddress} />
                    </Layout.Item>
                </Layout>
            </Block.Body>
        </Block>
    );
};
