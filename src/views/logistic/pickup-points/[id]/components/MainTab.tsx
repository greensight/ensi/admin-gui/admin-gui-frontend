import { Autocomplete, AutocompleteAsync, Block, FormField, FormFieldWrapper } from '@ensi-platform/core-components';
import { useMemo } from 'react';

import { Point, useDeliveryServiceAutocomplete } from '@api/logistic';

import { Layout, colors, scale, typography } from '@scripts/gds';
import { useTimezones } from '@scripts/hooks';

import { usePickupPointsAccess } from '../../usePickupPointsAccess';

export const MainTab = ({ pickupPoint }: { pickupPoint?: Point }) => {
    const access = usePickupPointsAccess();

    const { deliveryServiceOptionsByValuesFn, deliveryServiceSearchFn } = useDeliveryServiceAutocomplete();
    const timezones = useTimezones();
    const timezoneOptions = useMemo(() => timezones.map(e => ({ label: e.label, value: e.value })), [timezones]);

    return (
        <Block>
            <Block.Body>
                <Layout cols={2} gap={scale(2)}>
                    <Layout.Item col={2}>
                        <FormFieldWrapper
                            name="delivery_service_id"
                            label="Сервис доставки"
                            disabled={!access.ID.editMainData}
                        >
                            <AutocompleteAsync
                                asyncOptionsByValuesFn={deliveryServiceOptionsByValuesFn}
                                asyncSearchFn={deliveryServiceSearchFn}
                            />
                        </FormFieldWrapper>
                    </Layout.Item>
                    <Layout.Item col={2}>
                        <FormField name="city_guid" label="Идентификатор города" disabled={!access.ID.editMainData} />
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <FormField name="geo_lat" label="Широта" disabled={!access.ID.editMainData} />
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <FormField name="geo_lon" label="Долгота" disabled={!access.ID.editMainData} />
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <FormFieldWrapper name="timezone" label="Часовой пояс" disabled={!access.ID.editMainData}>
                            <Autocomplete options={timezoneOptions} />
                        </FormFieldWrapper>
                    </Layout.Item>
                    <Layout.Item col={2}>
                        <h4
                            css={{
                                ...typography('bodyMdBold'),
                                color: colors.grey700,
                            }}
                        >
                            Информация
                        </h4>
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <b>Описание проезда</b>
                        <p>{pickupPoint?.description || '-'}</p>
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <b>Адрес ПВЗ, сокращенный</b>
                        <p>{pickupPoint?.address_reduce || '-'}</p>
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <b>Выдача только полностью оплаченных посылок:</b>
                        <p>{pickupPoint?.only_online_payment ? 'да' : 'нет'}</p>
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <b>Возможность оплаты банковской картой:</b>
                        <p>{pickupPoint?.has_payment_card ? 'да' : 'нет'}</p>
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <b>Отделение осуществляет курьерскую доставку :</b>
                        <p>{pickupPoint?.has_courier ? 'да' : 'нет'}</p>
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <b>Станция метро:</b>
                        <p>{pickupPoint?.metro_station || '-'}</p>
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <b>Максимальный объем, м3:</b>
                        <p>{pickupPoint?.max_value || '-'}</p>
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <b>Максимальный вес, кг:</b>
                        <p>{pickupPoint?.max_weight || '-'}</p>
                    </Layout.Item>
                </Layout>
            </Block.Body>
        </Block>
    );
};
