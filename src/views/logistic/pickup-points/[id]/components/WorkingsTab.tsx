import { Block } from '@ensi-platform/core-components';
import { useMemo } from 'react';

import { Point, PointIncludesPointWorkings } from '@api/logistic';

import Table, { useSorting, useTable } from '@components/Table';

import { initialSort, workingsColumns } from '../scripts/constants';

export const WorkingsTab = ({ pickupPoint }: { pickupPoint?: Point }) => {
    const data = useMemo(() => pickupPoint?.point_workings || [], [pickupPoint]);

    const [{ invertedSorting }, sortingPlugin] = useSorting<PointIncludesPointWorkings>(
        'store_workings',
        [],
        initialSort,
        false
    );

    const table = useTable(
        {
            data,
            columns: workingsColumns,
            meta: {
                tableKey: `store_workings`,
            },
            state: {
                sorting: invertedSorting,
            },
        },
        [sortingPlugin]
    );

    return (
        <Block>
            <Block.Body>
                <Table instance={table} />
            </Block.Body>
        </Block>
    );
};
