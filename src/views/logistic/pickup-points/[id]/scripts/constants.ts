import { createColumnHelper } from '@tanstack/react-table';

import { PointIncludesPointWorkings } from '@api/logistic';

import { daysValues } from '@scripts/enums';
import { formatDate } from '@scripts/helpers';

export const columnHelper = createColumnHelper<PointIncludesPointWorkings>();
export const initialSort = { id: 'id', desc: true };

export const workingsColumns = [
    columnHelper.accessor('id', {
        header: 'ID',
        cell: props => props.getValue(),
    }),
    columnHelper.accessor('is_active', {
        header: 'Активность',
        cell: props => (props.getValue() ? 'да' : 'нет'),
    }),
    columnHelper.accessor('day', {
        header: 'День недели',
        cell: props => daysValues[String(props.getValue()) as keyof typeof daysValues] || '-',
    }),
    columnHelper.accessor('working_start_time', {
        header: 'Время начала работы',
        cell: props => props.getValue(),
    }),
    columnHelper.accessor('working_end_time', {
        header: 'Время конца работы',
        cell: props => props.getValue(),
    }),
    columnHelper.accessor('created_at', {
        header: 'Дата создания',
        cell: props => formatDate(new Date(props.getValue())),
    }),
    columnHelper.accessor('updated_at', {
        header: 'Дата изменения',
        cell: props => formatDate(new Date(props.getValue())),
    }),
];
