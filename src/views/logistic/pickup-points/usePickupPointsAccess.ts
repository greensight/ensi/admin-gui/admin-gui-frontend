import { useAccess } from '@scripts/hooks';

export const usePickupPointsAccess = () =>
    useAccess({
        LIST: {
            view: 2901,
        },
        ID: {
            view: 2902,
            editMainData: 2903,
            editAddress: 2904,
        },
    });
