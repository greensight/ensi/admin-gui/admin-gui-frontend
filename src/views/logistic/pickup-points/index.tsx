import { useMemo } from 'react';

import { Point, usePointsMeta, useSearchPoints } from '@api/logistic';

import ListBuilder from '@components/ListBuilder';
import { TooltipItem } from '@components/Table';

import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import { usePickupPointsAccess } from './usePickupPointsAccess';

const DeliveryPrices = () => {
    const access = usePickupPointsAccess();

    const goToDetailPage = useGoToDetailPage({ extraConditions: access.ID.view });

    const tooltipContent = useMemo<TooltipItem[]>(
        () => [
            {
                type: 'edit',
                text: 'Редактировать ПВЗ',
                action: goToDetailPage,
                isDisable: !access.ID.view,
            },
        ],
        [access.ID.view, goToDetailPage]
    );

    return (
        <ListBuilder<Point>
            access={access}
            searchHook={useSearchPoints}
            metaHook={usePointsMeta}
            tooltipItems={tooltipContent}
            title="ПВЗ"
        />
    );
};

export default DeliveryPrices;
