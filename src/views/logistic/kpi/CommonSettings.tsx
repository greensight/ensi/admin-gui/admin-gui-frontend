import { Form, FormField, FormReset, useFormState } from '@ensi-platform/core-components';
import * as Yup from 'yup';

import { ErrorMessages } from '@scripts/constants';
import { Button, Layout, scale } from '@scripts/gds';

const InnerForm = ({ disabled }: { disabled: boolean }) => {
    const { isDirty: dirty } = useFormState();

    return (
        <Layout
            cols={{
                xxxl: 3,
                sm: 1,
            }}
        >
            <Layout.Item
                col={{
                    xxxl: 2,
                    sm: 1,
                }}
            >
                <FormField
                    label="RTG (мин)"
                    hint="Ready-To-Go time - среднее время подтверждения заказа"
                    name="rtg"
                    type="number"
                    min={0}
                />
            </Layout.Item>
            <Layout.Item
                col={{
                    xxxl: 2,
                    sm: 1,
                }}
            >
                <FormField
                    label="CT (мин)"
                    hint="Confirmation Time - среднее время начала сборки с момента создания заказа"
                    name="ct"
                    type="number"
                    min={0}
                />
            </Layout.Item>
            <Layout.Item
                col={{
                    xxxl: 2,
                    sm: 1,
                }}
            >
                <FormField
                    label="PPT (мин)"
                    hint="Planned Processing Time - среднее время сборки заказа"
                    name="ppt"
                    type="number"
                    min={0}
                />
            </Layout.Item>
            <Layout.Item
                col={{
                    xxxl: 3,
                    sm: 1,
                }}
            >
                <FormReset theme="fill" css={{ marginRight: scale(2) }} disabled={!dirty || disabled}>
                    Сбросить
                </FormReset>
                <Button type="submit" disabled={!dirty || disabled}>
                    Сохранить
                </Button>
            </Layout.Item>
        </Layout>
    );
};

const CommonSettings = ({
    disabled,
    initialValues,
    onSubmit,
}: {
    disabled: boolean;
    initialValues: Record<string, any>;
    onSubmit: (values: Record<string, any>) => void;
}) => (
    <Form
        disabled={disabled}
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={Yup.object().shape({
            rtg: Yup.number()
                .transform(val => (Number.isNaN(val) ? undefined : val))
                .min(0, `${ErrorMessages.GREATER_OR_EQUAL} 0`)
                .integer(ErrorMessages.INTEGER)
                .required(ErrorMessages.REQUIRED),
            ct: Yup.number()
                .transform(val => (Number.isNaN(val) ? undefined : val))
                .min(0, `${ErrorMessages.GREATER_OR_EQUAL} 0`)
                .integer(ErrorMessages.INTEGER)
                .required(ErrorMessages.REQUIRED),
            ppt: Yup.number()
                .transform(val => (Number.isNaN(val) ? undefined : val))
                .min(0, `${ErrorMessages.GREATER_OR_EQUAL} 0`)
                .integer(ErrorMessages.INTEGER)
                .required(ErrorMessages.REQUIRED),
        })}
        enableReinitialize
    >
        <InnerForm disabled={disabled} />
    </Form>
);

export default CommonSettings;
