import { useAccess } from '@scripts/hooks';

export const AccessMatrix = {
    LIST: {
        view: 0,
    },
    ID: {
        edit: 2701,
        view: 2702,
    },
};
export const useKpiAccess = () =>
    useAccess({
        LIST: {
            view: 0,
        },
        ID: {
            edit: 2701,
            view: 2702,
        },
    });
