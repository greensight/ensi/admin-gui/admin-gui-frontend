import { useActionPopup } from '@ensi-platform/core-components';
import { Row, Table, createColumnHelper } from '@tanstack/react-table';
import Link from 'next/link';
import { useCallback } from 'react';

import { useDeleteReview, useReviews, useReviewsMeta } from '@api/catalog/reviews';
import { Review } from '@api/catalog/types/reviews';

import { useError, useSuccess } from '@context/modal';

import ListBuilder from '@components/ListBuilder';
import { TableColumnDefAny, TooltipItem } from '@components/Table';

import { ModalMessages } from '@scripts/constants';
import { ActionType } from '@scripts/enums';
import { Button, scale } from '@scripts/gds';
import { declOfNum } from '@scripts/helpers';
import { useAccess, useLinkCSS } from '@scripts/hooks';
import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

const tooltipAction = (rows?: Row<any>[]) => {
    if (rows) {
        alert(`You want to make multi action with ${rows.length} selected row(s)`);
    } else {
        alert(`You want to make action without selecting strings`);
    }
};

const AccessMatrix = {
    LIST: {
        // Пользователю доступен табличный список с отзывами с фильтрами и поиском
        view: 1501,
    },
    ID: {
        // Пользователю доступна детальная страница отзывами со всеми данными для просмотра
        view: 1502,
        // Пользователю доступно удаление отзывов
        delete: 1503,
        // Пользователю доступно изменение статуса отзыва
        edit: 1504,
        create: 0,
    },
};

export default function ExamplePage() {
    const linkStyles = useLinkCSS();
    const access = useAccess(AccessMatrix);
    const deleteReview = useDeleteReview();
    useSuccess(deleteReview.isSuccess ? ModalMessages.SUCCESS_DELETE : '');
    useError(deleteReview.error);

    const isExtraLoading = deleteReview.isPending;
    const { popupState, popupDispatch, ActionPopup, ActionEnum } = useActionPopup();

    const deleteRow = useCallback(
        (ids: number[], method?: () => void) => {
            popupDispatch({
                type: ActionType.Delete,
                payload: {
                    title: `Вы уверены, что хотите удалить ${ids.length} ${declOfNum(ids.length, [
                        'отзыв',
                        'отзыва',
                        'отзывов',
                    ])}`,
                    popupAction: ActionEnum.DELETE,
                    onAction: async () => {
                        try {
                            if (method) method();
                        } catch (err) {
                            console.error(err);
                        }
                    },
                },
            });
        },
        [ActionEnum.DELETE, ActionType.Delete, popupDispatch]
    );

    const goToDetailPage = useGoToDetailPage();

    const tooltipContent: TooltipItem[] = [
        {
            type: 'edit',
            text: `Перейти в деталку`,
            action: goToDetailPage,
            isDisable: !access.ID.view,
        },
        {
            type: 'delete',
            text: 'Удалить запись',
            action: row => {
                if (row) {
                    deleteRow([row[0].original.id]);
                }
            },
            isDisable: !access.ID.delete,
        },
    ];
    const headerInner = (table: Table<Review>) => (
        <Button
            theme="dangerous"
            type="button"
            css={{ marginRight: scale(1) }}
            onClick={() => {
                deleteRow(
                    table.getSelectedRowModel().flatRows.map(row => row.original.id),
                    () => table.toggleAllRowsSelected(false)
                );
            }}
            disabled={table.getSelectedRowModel().flatRows.length === 0}
        >
            Удалить записи
        </Button>
    );

    const tooltipForAdditionActions: TooltipItem[] = [
        {
            type: 'delete',
            text: 'Удалить',
            action: async (rowsParam?: Row<any>[]) => {
                popupDispatch({
                    payload: {
                        title: 'Сущность будет удалена вместе со всеми данными и настройками, удалить?',
                        popupAction: ActionEnum.DELETE,
                        onAction: () => tooltipAction(rowsParam),
                    },
                    type: ActionType.Edit,
                });
            },
            isDisable: false,
        },
    ];

    const columnHelper = createColumnHelper<Review>();
    const extraColumns: TableColumnDefAny[] = [
        columnHelper.accessor('id', {
            header: 'Наименование',
            // eslint-disable-next-line react/no-unstable-nested-components
            cell: ({ getValue }) => (
                <Link legacyBehavior href="test link" passHref>
                    <a css={linkStyles}>{getValue()}</a>
                </Link>
            ),
            enableHiding: true,
        }),
    ];

    return (
        <ListBuilder<Review>
            access={access}
            searchHook={useReviews}
            metaHook={useReviewsMeta}
            isLoading={isExtraLoading}
            extraColumns={extraColumns}
            tooltipItems={tooltipContent}
            headerInner={headerInner}
            tooltipForAdditionalActions={tooltipForAdditionActions}
            title="Список отзывов"
        >
            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </ListBuilder>
    );
}

// You need this function to allow  filters work on first render
export async function getServerSideProps() {
    return {
        props: {},
    };
}
