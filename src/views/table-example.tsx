/* eslint-disable react/no-unstable-nested-components */
import {
    Block,
    CalendarInput,
    Drawer,
    DrawerContent,
    DrawerFooter,
    DrawerHeader,
    Form,
    FormField,
    FormFieldWrapper,
    Input,
} from '@ensi-platform/core-components';
import { Row, createColumnHelper } from '@tanstack/react-table';
import { useRouter } from 'next/router';
import { useCallback, useMemo, useState } from 'react';

import Tooltip, { ContentBtn, hideOnEsc } from '@controls/Tooltip';

import PageWrapper from '@components/PageWrapper';
import Table, { TooltipItem, useSorting, useTable } from '@components/Table';
import { getSelectColumn, getSettingsColumn } from '@components/Table/columns';
import { Cell, TableEmpty, TableFooter, TableHeader } from '@components/Table/components';

import { Button, Layout, scale, typography, useTheme } from '@scripts/gds';
import { useFiltersHelper } from '@scripts/hooks';
import { getNumberId, getRandomItem } from '@scripts/mock';

import FilterActiveIcon from '@icons/filter-active.svg';
import KebabIcon from '@icons/small/kebab.svg';
import PlusIcon from '@icons/small/plus.svg';
import ResetIcon from '@icons/small/reset.svg';
import FilterIcon from '@icons/small/sliders.svg';

const getTableItem = () => ({
    id: getNumberId(),
    photo: '',
    titleAndCode: getRandomItem([
        ['Наушники QCY, белые', 172957172, 721183],
        ['Беспроводные наушники Apple  Airpods Pro белый', 172957172, 721183],
    ]),
    brand: getRandomItem(['Apple', 'Xiaomi', 'Sumsung', 'Huawei']),
    category: 'Беспроводные наушники',
    status: getRandomItem([
        { name: 'Проверен (опубликован)', value: 1 },
        { name: 'Требует дозаполнения', value: 2 },
        { name: 'Отклонено', value: 3 },
    ]),
    features: [
        getRandomItem(['На модерации', 'Наполнение мастер-данными']),
        getRandomItem(['На производстве контента', '']),
    ],
    created_at: new Date(Date.now() - 60 * 60 * 24 * getRandomItem([1, 2, 3, 4])),
    updated_at: new Date(),
});

function makeRandomData<T>(len: number, cb: (index: number) => T) {
    return [...Array(len).keys()].map(el => cb(el));
}

const emptyInitialValues = {
    id: '',
    name: '',
    code: '',
    priceFrom: '',
    priceTo: '',
    date: null,
};

const tooltipAction = (rows?: Row<any>[]) => {
    if (rows) {
        alert(`You want to make multi action with ${rows.length} selected row(s)`);
    } else {
        alert(`You want to make action without selecting strings`);
    }
};

export default function ExamplePage() {
    const { push, pathname } = useRouter();
    const [filtersOpen, setFiltersOpen] = useState(false);
    const [itemsPerPageCount, setItemsPerPageCount] = useState(10);
    const { colors } = useTheme();

    const pageKey = 'example-key';

    const [{ sorting }, sortingPlugin] = useSorting<Record<string, any>>(pageKey, [], undefined, false);

    const resetFilters = useCallback(() => {
        push(pathname);
    }, [pathname, push]);
    const data = useMemo<Record<string, any>[]>(
        () => makeRandomData(itemsPerPageCount, getTableItem),
        [itemsPerPageCount]
    );

    const closeFilters = useCallback(() => {
        setFiltersOpen(false);
    }, []);

    const { initialValues, URLHelper, filtersActive } = useFiltersHelper(emptyInitialValues);

    console.warn('You need initialValues to pass in @tanstack/react-query hook', typeof initialValues);

    const tooltipContent: TooltipItem[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Изменить статус',
                action: tooltipAction,
            },
            {
                type: 'edit',
                text: 'Изменить атрибут',
                action: tooltipAction,
            },
            {
                type: 'edit',
                text: 'Изменить категоризацию',
                action: tooltipAction,
            },
            {
                type: 'edit',
                text: 'Задать признаки',
                action: tooltipAction,
            },
            {
                type: 'edit',
                text: 'Отправить на модерацию',
                action: tooltipAction,
            },
            {
                type: 'copy',
                text: 'Копировать ID',
                action: tooltipAction,
            },
            {
                type: 'copy',
                text: 'Копировать артикул',
                action: tooltipAction,
            },
            {
                type: 'export',
                text: 'Экспорт товаров',
                action: tooltipAction,
            },
            {
                type: 'delete',
                text: 'Удалить',
                action: tooltipAction,
            },
        ],
        []
    );

    const renderHeader = useCallback(
        (selectedRows: Row<any>[]) => {
            const count = selectedRows.length;
            const isSelected = count > 0;

            return (
                <TableHeader>
                    {isSelected ? (
                        <span css={typography('bodyMdBold')}>Выбрано {count}</span>
                    ) : (
                        <>
                            <span>Товаров {data.length} </span>
                            <Input
                                placeholder="Поиск по названию, артикулу или штрихкоду"
                                css={{ width: scale(42), marginLeft: scale(2) }}
                            />
                            <Button
                                theme="secondary"
                                Icon={filtersActive ? FilterActiveIcon : FilterIcon}
                                css={{ marginLeft: scale(2) }}
                                onClick={() => setFiltersOpen(true)}
                            >
                                Фильтры
                            </Button>
                            {filtersActive && (
                                <Button
                                    theme="fill"
                                    Icon={ResetIcon}
                                    css={{ marginLeft: scale(2) }}
                                    onClick={resetFilters}
                                >
                                    Сбросить
                                </Button>
                            )}
                        </>
                    )}
                    <Button theme="secondary" css={{ marginLeft: 'auto' }}>
                        Экспорт товаров
                    </Button>
                    {isSelected ? (
                        <Tooltip
                            content={
                                <>
                                    {tooltipContent.map(t => (
                                        <ContentBtn
                                            key={t.text}
                                            type={t.type}
                                            onClick={e => {
                                                e.stopPropagation();
                                                t.action(selectedRows);
                                            }}
                                            disabled={typeof t?.isDisable === 'function' ? t.isDisable() : t?.isDisable}
                                        >
                                            {t.text}
                                        </ContentBtn>
                                    ))}
                                </>
                            }
                            plugins={[hideOnEsc]}
                            trigger="click"
                            arrow
                            theme="light"
                            placement="bottom"
                            minWidth={scale(36)}
                            disabled={tooltipContent.length === 0}
                            appendTo={() => document.body}
                        >
                            <Button
                                theme="outline"
                                Icon={KebabIcon}
                                css={{ marginLeft: scale(2), marginRight: scale(2) }}
                                iconAfter
                            >
                                Действия
                            </Button>
                        </Tooltip>
                    ) : (
                        <Button Icon={PlusIcon} css={{ marginLeft: scale(2) }}>
                            Добавить товар
                        </Button>
                    )}
                </TableHeader>
            );
        },
        [data.length, filtersActive, resetFilters, tooltipContent]
    );

    const columnHelper = createColumnHelper<Record<string, any>>();
    const columns = useMemo(
        () => [
            getSelectColumn<Record<string, any>>(undefined, 'Example'),
            columnHelper.accessor('id', {
                header: 'ID',
                cell: props => props.getValue(),
            }),
            columnHelper.accessor('photo', {
                header: '',
                cell: props => <Cell type="photo" value={props.getValue()} {...props} />,
                enableSorting: false,
            }),
            columnHelper.accessor('titleAndCode', {
                header: 'Название, артикул',
                cell: ({ getValue }) =>
                    getValue().map((item: string, i: number) => (
                        <p css={i !== 0 && { color: colors?.grey800 }} key={item}>
                            {i === 1 && 'Артикул: '}
                            {i === 2 && 'Штрихкод: '}
                            {item}
                        </p>
                    )),
            }),
            columnHelper.accessor('brand', {
                header: 'Бренд',
                cell: props => props.getValue(),
            }),
            columnHelper.accessor('category', {
                header: 'Категория',
                cell: props => props.getValue(),
            }),
            columnHelper.accessor('status', {
                header: 'Статус',
                cell: ({
                    getValue,
                }: {
                    getValue: () => {
                        value: number;
                        name: string;
                    };
                }) => {
                    const getBgColor = (val: number) => {
                        switch (val) {
                            case 1:
                                return colors?.success;
                            case 2:
                                return colors?.warning;
                            case 3:
                            default:
                                return colors?.danger;
                        }
                    };

                    return (
                        <p>
                            <span
                                css={{
                                    width: 6,
                                    height: 6,
                                    borderRadius: '50%',
                                    display: 'inline-block',
                                    verticalAlign: 'middle',
                                    marginRight: scale(1),
                                    background: getBgColor(getValue().value),
                                }}
                            />
                            {getValue().name}
                        </p>
                    );
                },
            }),
            columnHelper.accessor('features', {
                header: 'Признаки',
                cell: props => <Cell type="photo" value={props.getValue()} {...props} />,
                enableSorting: false,
            }),
            columnHelper.accessor('created_at', {
                header: 'Создан',
                cell: props => <Cell type="photo" value={props.getValue()} {...props} />,
                enableSorting: false,
            }),
            columnHelper.accessor('updated_at', {
                header: 'Изменен',
                cell: props => <Cell type="photo" value={props.getValue()} {...props} />,
                enableSorting: false,
            }),
            getSettingsColumn({ tooltipContent }),
        ],
        []
    );

    const table = useTable(
        {
            data,
            columns,
            meta: {
                tableKey: pageKey,
            },
            state: {
                sorting,
            },
        },
        [sortingPlugin]
    );

    return (
        <PageWrapper title="Пример страницы с таблицей" css={{ padding: 0 }}>
            <Block>
                <Block.Body>
                    {sorting.length > 0 && <p>Сортировка: {sorting[0].id}</p>}
                    {renderHeader(table.getSelectedRowModel().flatRows)}
                    <Table instance={table} />

                    {data.length === 0 ? (
                        <TableEmpty
                            filtersActive={filtersActive}
                            titleWithFilters="Товары не найдены"
                            titleWithoutFilters="Товаров нет"
                            addItems={() => setItemsPerPageCount(10)}
                            addItemsText="Добавьте товары"
                        />
                    ) : (
                        <TableFooter
                            pages={7}
                            itemsPerPageCount={itemsPerPageCount}
                            setItemsPerPageCount={setItemsPerPageCount}
                        />
                    )}
                </Block.Body>
            </Block>

            <Drawer open={filtersOpen} onClose={closeFilters}>
                <DrawerHeader title="Фильтры" onClose={closeFilters} hasCloseButton />
                <Form
                    onSubmit={vals => {
                        URLHelper(vals);
                        closeFilters();
                    }}
                    initialValues={initialValues}
                    css={{ display: 'flex', flexDirection: 'column', height: '100%' }}
                >
                    <DrawerContent>
                        <Layout cols={1} css={{ width: '100%' }}>
                            <Layout.Item col={1}>
                                <FormField name="name" label="Название" />
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <FormField name="code" label="Артикул" />
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <FormField name="id" label="ID" />
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <FormField name="priceFrom" label="Цена" type="number" />
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <FormFieldWrapper name="date" label="Создан">
                                    <CalendarInput picker view="date" platform="desktop" />
                                </FormFieldWrapper>
                            </Layout.Item>
                        </Layout>
                    </DrawerContent>
                    <DrawerFooter>
                        <Layout gap={scale(2)} cols={2}>
                            <Layout.Item col={1}>
                                <Button
                                    theme="fill"
                                    block
                                    onClick={() => {
                                        push(pathname);
                                        closeFilters();
                                    }}
                                >
                                    Сбросить
                                </Button>
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Button block type="submit">
                                    Показать
                                </Button>
                            </Layout.Item>
                        </Layout>
                    </DrawerFooter>
                </Form>
            </Drawer>
        </PageWrapper>
    );
}

// You need this function to allow  filters work on first render
export async function getServerSideProps() {
    return {
        props: {},
    };
}
