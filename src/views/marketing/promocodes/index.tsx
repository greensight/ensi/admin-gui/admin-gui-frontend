import { Popup, PopupContent, PopupFooter, PopupHeader } from '@ensi-platform/core-components';
import { createColumnHelper } from '@tanstack/react-table';
import { useCallback, useMemo } from 'react';

import { PromoCode, useDeletePromoCode, usePromoCodesMeta, useSearchPromoCodes } from '@api/marketing';

import { useError, useSuccess } from '@context/modal';

import LoadWrapper from '@controls/LoadWrapper';

import ListBuilder from '@components/ListBuilder';
import Table, { ExtendedRow, TableColumnDefAny, TooltipItem, useSorting, useTable } from '@components/Table';

import { ModalMessages } from '@scripts/constants';
import { ActionType } from '@scripts/enums';
import { Button, scale } from '@scripts/gds';
import { usePopupState } from '@scripts/hooks';
import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import { usePromocodesAccess } from './usePromocodesAccess';

interface State {
    selected_rows: { id: number; name: string }[];
    action?: ActionType;
    open: boolean;
}

const initialState: State = { selected_rows: [], action: ActionType.Close, open: false };

const MarketingPromoCodes = () => {
    const { access } = usePromocodesAccess();

    const goToDetailPage = useGoToDetailPage();

    const [popupState, popupDispatch] = usePopupState(initialState);

    const deletePromoCode = useDeletePromoCode();
    useSuccess(deletePromoCode.isSuccess ? ModalMessages.SUCCESS_DELETE : '');
    useError(deletePromoCode.error);

    const onDeletePopup = useCallback(
        (originalRows: ExtendedRow['original'][]) => {
            popupDispatch({
                type: ActionType.Delete,
                payload: {
                    selected_rows: originalRows.map(row => ({
                        id: Number(row!.original.id),
                        name: row!.original.name,
                    })),
                    action: ActionType.Delete,
                    open: true,
                },
            });
        },
        [popupDispatch]
    );

    const tooltipContent = useMemo<TooltipItem[]>(
        () => [
            {
                type: 'edit',
                text: 'Редактировать промокод',
                action: goToDetailPage,
                isDisable: !access.ID.view,
            },
            {
                type: 'delete',
                text: 'Удалить промокод',
                action: rows => {
                    if (rows) onDeletePopup(rows);
                },
                isDisable: !access.ID.delete,
            },
        ],
        [access.ID.delete, access.ID.view, goToDetailPage, onDeletePopup]
    );

    const [{ sorting }, sortingPlugin] = useSorting<Record<string, any>>(
        'delete-promocode-table',
        [],
        undefined,
        false
    );

    const columnHelper = createColumnHelper<Record<string, any>>();
    const columns: TableColumnDefAny[] = [
        columnHelper.accessor('id', {
            header: 'ID',
            cell: props => props.getValue(),
        }),
        columnHelper.accessor('name', {
            header: 'Название',
            cell: props => props.getValue(),
        }),
    ];

    const table = useTable(
        {
            data: popupState.selected_rows,
            columns,
            meta: {
                tableKey: `delete-promocode-table`,
            },
            state: {
                sorting,
            },
        },
        [sortingPlugin]
    );

    return (
        <ListBuilder<PromoCode>
            access={access}
            searchHook={useSearchPromoCodes}
            metaHook={usePromoCodesMeta}
            isLoading={false}
            tooltipItems={tooltipContent}
            title="Список промокодов"
        >
            <Popup
                open={popupState.open && popupState.action === ActionType.Delete}
                onClose={() =>
                    popupDispatch({
                        type: ActionType.Close,
                    })
                }
                css={{ minWidth: scale(100) }}
            >
                <LoadWrapper isLoading={deletePromoCode.isPending}>
                    <PopupHeader title="Вы уверены, что хотите удалить следующие промокоды?" />
                    <PopupContent>
                        <Table instance={table} css={{ marginBottom: scale(2) }} />
                    </PopupContent>
                    <PopupFooter>
                        <Button
                            type="button"
                            theme="secondary"
                            onClick={() => {
                                popupDispatch({
                                    type: ActionType.Close,
                                });
                            }}
                        >
                            Не удалять
                        </Button>
                        <Button
                            type="button"
                            theme="primary"
                            onClick={async () => {
                                await deletePromoCode.mutateAsync({ id: popupState.selected_rows[0].id });
                                popupDispatch({
                                    type: ActionType.Close,
                                });
                            }}
                        >
                            Удалить
                        </Button>
                    </PopupFooter>
                </LoadWrapper>
            </Popup>
        </ListBuilder>
    );
};

export default MarketingPromoCodes;
