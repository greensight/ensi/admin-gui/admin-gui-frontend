import { useAccess } from '@scripts/hooks';
import { useIDForbidden } from '@scripts/hooks/useIDForbidden';

export const AccessMatrix = {
    LIST: {
        view: 1311,
    },
    ID: {
        view: 1312,
        create: 1313,
        edit: 1314,
        delete: 1315,
    },
};

export const usePromocodesAccess = () => {
    const access = useAccess(AccessMatrix);
    const isIDForbidden = useIDForbidden(access);

    return { access, isIDForbidden };
};
