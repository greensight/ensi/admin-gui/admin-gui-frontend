import {
    AutocompleteAsync,
    CopyButton,
    DescriptionList,
    DescriptionListItem,
    FormCheckbox,
    FormField,
    FormFieldWrapper,
    getDefaultDates,
    useActionPopup,
    useController,
    useFormContext,
} from '@ensi-platform/core-components';
import { parseISO } from 'date-fns';
import { nanoid } from 'nanoid';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useMemo, useRef } from 'react';
import * as Yup from 'yup';

import {
    NewPromoCode,
    PromoCode,
    useCreatePromoCode,
    useDeletePromoCode,
    useDiscountsAutocomplete,
    usePatchPromoCode,
    usePromoCode,
    usePromoCodeStatuses,
} from '@api/marketing';

import { useError } from '@context/modal';

import CalendarRange from '@controls/CalendarRange';
import Legend from '@controls/Legend';
import Select from '@controls/Select';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import PageLeaveGuard from '@components/PageLeaveGuard';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { CREATE_PARAM, ErrorMessages } from '@scripts/constants';
import { ActionType, PromoCodeStatus, RedirectMethods } from '@scripts/enums';
import { Button, Layout, scale } from '@scripts/gds';
import { isPageNotFound, toISOString } from '@scripts/helpers';
import { useLinkCSS } from '@scripts/hooks';

import { prepareValuesForFutureSelect } from '../helpers';
import { usePromocodesAccess } from './usePromocodesAccess';

const PROMOCODES_URL = '/marketing/promocodes';

const emptyValues = {
    name: '',
    code: '',
    status: '',
    start_date: '',
    end_date: '',
    counter: '',
    currentCounter: '',
    discount_id: '',
    infinite: false,
};

const GenerateCodeBtn = ({ disabled }: { disabled?: boolean }) => {
    const { field } = useController({
        name: 'code',
    });

    return (
        <Button disabled={disabled} block onClick={() => field.onChange(nanoid(10).toLowerCase())}>
            Сгенерировать&nbsp;случаный&nbsp;промокод
        </Button>
    );
};

const DiscountField = () => {
    const { watch } = useFormContext();
    const discountID = watch('discount_id');

    const linkCSS = useLinkCSS('blue');

    const { discountsOptionsByValuesFn, discountsSearchFn } = useDiscountsAutocomplete();

    return (
        <>
            <FormFieldWrapper name="discount_id" label="Название скидки">
                <AutocompleteAsync
                    asyncSearchFn={discountsSearchFn}
                    asyncOptionsByValuesFn={discountsOptionsByValuesFn}
                    hideClearButton
                />
            </FormFieldWrapper>
            {discountID && (
                <Link legacyBehavior href={`/marketing/discounts/${discountID}`} passHref>
                    <a css={{ marginTop: scale(1), ...linkCSS }} target="_blank">
                        Перейти к скидке
                    </a>
                </Link>
            )}
        </>
    );
};

const FormInner = ({
    promo,
    onDelete,
    onApply,
    onSubmit,
}: {
    promo?: PromoCode;
    onSubmit: () => void;
    onApply: () => void;
    onDelete: () => void;
}) => {
    const { query, pathname, push } = useRouter();
    const listLink = pathname.split(`[id]`)[0];

    const { access } = usePromocodesAccess();
    const promoId = Array.isArray(query.id) ? undefined : query.id;
    const isCreation = promoId === CREATE_PARAM;

    const canSubmit = isCreation ? access.ID.create : access.ID.edit;

    const { watch } = useFormContext();
    const [code, currentCounter, infinite] = watch(['code', 'currentCounter', 'infinite']);

    const { data: apiStatuses, error: errorStatuses } = usePromoCodeStatuses();
    const statuses = useMemo(
        () => (apiStatuses?.data ? prepareValuesForFutureSelect(apiStatuses.data) : []),
        [apiStatuses]
    );

    useError(errorStatuses);

    return (
        <PageTemplate
            h1={isCreation ? `Создание промокода` : `Редактирование промокода #${promoId}`}
            backlink={{ text: 'Назад', href: PROMOCODES_URL }}
            controls={
                <PageControls access={access}>
                    <PageControls.Delete onClick={onDelete} />
                    <PageControls.Close
                        onClick={() =>
                            push({
                                pathname: listLink,
                            })
                        }
                    />
                    <PageControls.Save onClick={onSubmit} />
                    <PageControls.Apply onClick={onApply} />
                </PageControls>
            }
            aside={
                <Layout cols={1} gap={scale(3)}>
                    <Layout.Item>
                        <DescriptionList>
                            <DescriptionListItem
                                name="ID"
                                value={!isCreation ? <CopyButton>{String(promoId)}</CopyButton> : '-'}
                            />
                            <DescriptionListItem name="Код" value={code ? <CopyButton>{code}</CopyButton> : null} />
                            {getDefaultDates({ ...promo }).map(item => (
                                <DescriptionListItem {...item} key={item.name} />
                            ))}
                        </DescriptionList>
                    </Layout.Item>
                </Layout>
            }
        >
            <Layout cols={2} gap={scale(2)}>
                <Layout.Item col={2}>
                    <FormField name="name" label="Название" />
                </Layout.Item>
                <Layout.Item col={2}>
                    <Layout cols={3}>
                        <Layout.Item col={2}>
                            <FormField name="code" label="Код" hint="Код должен быть уникальным" />
                        </Layout.Item>
                        <Layout.Item col={1} align="center">
                            <GenerateCodeBtn disabled={!canSubmit} />
                        </Layout.Item>
                    </Layout>
                </Layout.Item>
                <Layout.Item col={1}>
                    <DiscountField />
                </Layout.Item>
                <Layout.Item col={1}>
                    <FormFieldWrapper name="status" label="Статус">
                        <Select options={statuses} hideClearButton />
                    </FormFieldWrapper>
                </Layout.Item>
                <Layout.Item col={2}>
                    {!infinite && (
                        <CalendarRange
                            label="Введите дату"
                            nameFrom="start_date"
                            nameTo="end_date"
                            disabled={!canSubmit}
                        />
                    )}
                    <FormFieldWrapper name="infinite" label={infinite ? 'Введите дату' : undefined}>
                        <FormCheckbox css={{ marginTop: infinite ? 0 : scale(2) }} disabled={!canSubmit}>
                            Бессрочный
                        </FormCheckbox>
                    </FormFieldWrapper>
                </Layout.Item>
                <Layout.Item col={1}>
                    <FormField
                        name="counter"
                        label="Максимальное количество применений"
                        hint="Не более N раз"
                        type="number"
                    />
                </Layout.Item>
                <Layout.Item col={1}>
                    <Legend label="Текущее количество применений" />
                    {currentCounter || '-'}
                </Layout.Item>
            </Layout>
        </PageTemplate>
    );
};

const PromocodeCreate = () => {
    const { query, push } = useRouter();
    const { access, isIDForbidden } = usePromocodesAccess();

    const promoId = Array.isArray(query.id) ? NaN : Number(query.id);
    const isCreation = query.id === CREATE_PARAM;

    const { data, isFetching: isLoading, error } = usePromoCode({ id: promoId }, access.ID.view);
    const promo = data?.data;

    useError(error);

    const initialValues = useMemo(() => {
        if (promo)
            return {
                code: promo.code || '',
                counter: promo.counter || '',
                name: promo.name || '',
                status: promo.status || '',
                discount_id: promo.discount_id || '',
                currentCounter: promo.current_counter || '',
                start_date: promo.start_date ? parseISO(promo.start_date).getTime() : '',
                end_date: promo.end_date ? parseISO(promo.end_date).getTime() : '',
                infinite: !promo.start_date && !promo.end_date,
            };
        return emptyValues;
    }, [promo]);
    const createPromocode = useCreatePromoCode();
    const updatePromocode = usePatchPromoCode();
    const deletePromocode = useDeletePromoCode();

    const { popupState, popupDispatch, ActionPopup, ActionEnum } = useActionPopup();

    useError(createPromocode.error || updatePromocode.error);
    const returnToListing = useRef(false);

    const canSubmit = isCreation ? access.ID.create : access.ID.edit;

    const isNotFound = isPageNotFound({ id: promoId, error, isCreation });

    return (
        <PageWrapper
            title={isCreation ? 'Создание промокода' : `Редактирование промокода #${promoId}`}
            isLoading={createPromocode.isPending || updatePromocode.isPending || isLoading}
            isNotFound={isNotFound}
            isForbidden={isIDForbidden}
        >
            <FormWrapper
                initialValues={initialValues}
                validationSchema={Yup.object().shape({
                    status: Yup.number()
                        .transform(value => (Number.isNaN(+value) ? undefined : value))
                        .required(ErrorMessages.REQUIRED),
                    name: Yup.string().required(ErrorMessages.REQUIRED),
                    code: Yup.string().required(ErrorMessages.REQUIRED),
                    counter: Yup.number()
                        .transform(value => (Number.isNaN(+value) ? undefined : value))
                        .optional(),
                    discount_id: Yup.number()
                        .transform(value => (Number.isNaN(+value) ? undefined : value))
                        .required(ErrorMessages.REQUIRED),
                    infinite: Yup.boolean().test(
                        `can-uncheck`,
                        'Введите дату или выберите "бессрочный"',
                        (val, ctx) => {
                            const dates = [ctx.parent.start_date, ctx.parent.end_date];
                            const check = dates.every(e => e !== -1 && e !== undefined);
                            const isValid = check || val || false;
                            return isValid;
                        }
                    ),
                })}
                enableReinitialize
                disabled={!canSubmit}
                onSubmit={async values => {
                    const newData: NewPromoCode = {
                        status: values.status as PromoCodeStatus,
                        name: values.name,
                        code: values.code,
                        counter: +values.counter,
                        discount_id: +values.discount_id,
                        ...(!values.infinite
                            ? {
                                  start_date: (values.start_date && toISOString(new Date(values.start_date))) || null,
                                  end_date: (values.end_date && toISOString(new Date(values.end_date))) || null,
                              }
                            : {
                                  start_date: null,
                                  end_date: null,
                              }),
                    };

                    if (isCreation) {
                        const result = await createPromocode.mutateAsync(newData);

                        if (returnToListing.current) {
                            return {
                                method: RedirectMethods.push,
                                redirectPath: PROMOCODES_URL,
                            };
                        }

                        return {
                            method: RedirectMethods.replace,
                            redirectPath: `${PROMOCODES_URL}/${result.data.id}`,
                        };
                    }

                    await updatePromocode.mutateAsync({ ...newData, id: Number(promoId) });

                    if (returnToListing.current) {
                        return {
                            method: RedirectMethods.push,
                            redirectPath: PROMOCODES_URL,
                        };
                    }

                    return {
                        method: RedirectMethods.replace,
                        redirectPath: `${PROMOCODES_URL}/${promoId}`,
                    };
                }}
            >
                {({ reset }) => (
                    <>
                        <PageLeaveGuard />
                        <FormInner
                            promo={promo}
                            onDelete={() =>
                                popupDispatch({
                                    type: ActionType.Delete,
                                    payload: {
                                        title: `Вы уверены, что хотите удалить промокод ${promo?.name}?`,
                                        popupAction: ActionEnum.DELETE,
                                        onAction: async () => {
                                            if (promo?.id) {
                                                reset();
                                                await deletePromocode.mutateAsync({ id: promo.id });
                                                push(PROMOCODES_URL);
                                            }
                                        },
                                    },
                                })
                            }
                            onApply={() => {
                                returnToListing.current = false;
                            }}
                            onSubmit={() => {
                                returnToListing.current = true;
                            }}
                        />
                    </>
                )}
            </FormWrapper>
            <ActionPopup popupDispatch={popupDispatch} popupState={popupState} />
        </PageWrapper>
    );
};

export default PromocodeCreate;
