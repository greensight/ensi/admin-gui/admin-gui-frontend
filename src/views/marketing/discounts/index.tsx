import { useActionPopup } from '@ensi-platform/core-components';
import { Table as TableType } from '@tanstack/react-table';
import { useCallback, useMemo, useState } from 'react';

import { Discount, useDeleteDiscount, useDiscounts, useDiscountsMeta, useDiscountsStatusChange } from '@api/marketing';

import { useError, useSuccess } from '@context/modal';

import ChangeDiscountStatusPopup from '@views/marketing/discounts/[entity_id]/components/ChangeDiscountStatusPopup';

import ListBuilder from '@components/ListBuilder';
import { ExtendedRow, TooltipItem } from '@components/Table';

import { ModalMessages } from '@scripts/constants';
import { ActionType } from '@scripts/enums';
import { Button, scale } from '@scripts/gds';
import { usePopupState } from '@scripts/hooks';
import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import { useDiscountAccess } from './useDiscountAccess';

export interface State {
    selected_rows: { id: number; name: string }[];
    action?: ActionType;
    open: boolean;
}

const initialState: State = { selected_rows: [], action: ActionType.Close, open: false };

const MarketingDiscounts = () => {
    const { access } = useDiscountAccess();
    const [changeStatusesOpen, setChangeStatusesOpen] = useState(false);
    const [discountsPopupState, discountsPopupDispatch] = usePopupState(initialState);
    const { popupState, popupDispatch, ActionPopup, ActionEnum } = useActionPopup();

    const changeDiscountsStatus = useDiscountsStatusChange();
    const deleteDiscount = useDeleteDiscount();
    useError(changeDiscountsStatus.error);
    useSuccess(changeDiscountsStatus.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const goToDetailPage = useGoToDetailPage({ extraConditions: access.ID.view });

    const onDeletePopup = useCallback(
        (originalRows: ExtendedRow['original'][]) => {
            discountsPopupDispatch({
                type: ActionType.Delete,
                payload: {
                    selected_rows: originalRows.map(row => ({
                        id: Number(row!.original.id),
                        name: row!.original.name,
                    })),
                    action: ActionType.Delete,
                    open: true,
                },
            });

            const tableRows = originalRows.map(row => ({
                id: Number(row!.original.id),
                name: row!.original.name,
            }));

            popupDispatch({
                type: ActionType.Delete,
                payload: {
                    title: `Вы уверены, что хотите удалить следующие скидки?`,
                    popupAction: ActionEnum.DELETE,
                    onAction: async () => {
                        await deleteDiscount.mutateAsync(tableRows[0].id);
                    },
                    children: (
                        <ChangeDiscountStatusPopup
                            state={discountsPopupState}
                            isOpen={changeStatusesOpen}
                            onClose={() => setChangeStatusesOpen(false)}
                            changeDiscountsStatus={changeDiscountsStatus}
                        />
                    ),
                },
            });
        },
        [ActionEnum.DELETE, deleteDiscount, discountsPopupDispatch, popupDispatch]
    );

    const tooltipContent = useMemo<TooltipItem[]>(
        () => [
            {
                type: 'edit',
                text: 'Редактировать скидку',
                action: goToDetailPage,
                isDisable: !access.ID.view,
            },
            {
                type: 'delete',
                text: 'Удалить скидку',
                action: rows => {
                    if (rows) onDeletePopup(rows);
                },
                isDisable: !access.ID.delete,
            },
        ],
        [access.ID.delete, access.ID.view, goToDetailPage, onDeletePopup]
    );

    const renderHeader = useCallback(
        (table: TableType<Discount>) => (
            <div
                css={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    width: '100%',
                    alignItems: 'center',

                    margin: `0 ${scale(2)}px ${scale(2)}px 0`,
                }}
            >
                <div
                    css={{
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}
                >
                    <div
                        css={{
                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}
                    >
                        {access.LIST.massStatusUpdate && !!table.getSelectedRowModel().flatRows.length && (
                            <Button
                                type="button"
                                onClick={() => {
                                    discountsPopupDispatch({
                                        type: ActionType.Edit,
                                        payload: {
                                            selected_rows: table.getSelectedRowModel().flatRows.map(row => ({
                                                id: Number(row.original.id),
                                                name: row.original.name,
                                            })),
                                            action: ActionType.Edit,
                                            open: true,
                                        },
                                    });

                                    setChangeStatusesOpen(true);
                                }}
                            >
                                Изменить статус
                            </Button>
                        )}
                    </div>
                </div>
            </div>
        ),
        [access.ID.create, access.LIST.massStatusUpdate, discountsPopupDispatch]
    );

    return (
        <ListBuilder<Discount>
            access={access}
            searchHook={useDiscounts}
            metaHook={useDiscountsMeta}
            isLoading={deleteDiscount.isPending || changeDiscountsStatus.isPending}
            tooltipItems={tooltipContent}
            headerInner={renderHeader}
            title="Скидки"
        >
            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
            <ChangeDiscountStatusPopup
                state={discountsPopupState}
                isOpen={changeStatusesOpen}
                onClose={() => setChangeStatusesOpen(false)}
                changeDiscountsStatus={changeDiscountsStatus}
            />
        </ListBuilder>
    );
};

export default MarketingDiscounts;
