import {
    CopyButton,
    DescriptionList,
    DescriptionListItem,
    FormCheckbox,
    FormField,
    FormFieldWrapper,
    getDefaultDates,
    useActionPopup,
    useFormContext,
} from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import * as Yup from 'yup';

import {
    Discount,
    useDeleteDiscount,
    useDiscount,
    useDiscountCreate,
    useDiscountEdit,
    useDiscountStatuses,
    useDiscountTypes,
    useDiscountValueTypes,
} from '@api/marketing';

import { useError, useSuccess } from '@context/modal';

import CalendarRange from '@controls/CalendarRange';
import LoadingSkeleton from '@controls/LoadingSkeleton';
import Select from '@controls/Select';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { CREATE_PARAM, ErrorMessages, ModalMessages } from '@scripts/constants';
import { RedirectMethods } from '@scripts/enums';
import { Layout, colors, scale } from '@scripts/gds';
import { isPageNotFound, toISOString } from '@scripts/helpers';
import { useDeferredLoading } from '@scripts/hooks/useDeferredLoading';

import { prepareValuesForFutureSelect } from '../../helpers';
import { useDiscountAccess } from '../useDiscountAccess';
import BindedProducts from './BindedProducts';
import { AddProductsPopup } from './components/AddProductsPopup';
import { formatDiscountFromKopecks, formatDiscountFromRub } from './scripts/helpers';
import { DiscountStatusEnum, DiscountTypeEnum, DiscountValueTypeEnum } from './types';

const FormInner = ({
    discount,
    title,
    onDelete,
    onApply,
    onSubmit,
}: {
    discount?: Discount;
    title: string;
    onDelete: () => void;
    onApply: () => void;
    onSubmit: () => void;
}) => {
    const { query, push } = useRouter();
    const discountId = (query?.entity_id && +query.entity_id) || 0;
    const isCreation = query?.entity_id === CREATE_PARAM;

    const { watch, setValue } = useFormContext();
    const infinite = watch('infinite');

    const { data: apiStatuses, error: errorStatuses, isFetching: isStatusesLoading } = useDiscountStatuses();
    const discountStatuses = useMemo(
        () => (apiStatuses?.data ? prepareValuesForFutureSelect(apiStatuses.data) : []),
        [apiStatuses]
    );
    useError(errorStatuses);

    const { data: apiTypes, error: errorTypes, isFetching: isTypesLoading } = useDiscountTypes();
    const discountTypes = useMemo(
        () => (apiTypes?.data ? prepareValuesForFutureSelect(apiTypes.data) : []),
        [apiTypes]
    );
    useError(errorTypes);

    const { data: apiValueTypes, error: errorValueTypes, isFetching: isValueTypesLoading } = useDiscountValueTypes();
    const discountValueTypes = useMemo(
        () => (apiValueTypes?.data ? prepareValuesForFutureSelect(apiValueTypes.data) : []),
        [apiValueTypes]
    );
    useError(errorValueTypes);

    const isLoading = isStatusesLoading || isTypesLoading || isValueTypesLoading;
    const deferredIsLoading = useDeferredLoading(isLoading);

    const { access } = useDiscountAccess();

    const canEdit = isCreation ? access.ID.create : access.ID.edit;

    return (
        <PageTemplate
            backlink={{
                href: '/marketing/discounts',
                text: 'Назад к списку скидок',
            }}
            h1={title}
            controls={
                <PageControls access={access}>
                    <PageControls.Delete onClick={onDelete} />
                    <PageControls.Close
                        onClick={() => {
                            push({
                                pathname: '/marketing/discounts',
                            });
                        }}
                    />
                    <PageControls.Apply onClick={onApply} />
                    <PageControls.Save onClick={onSubmit} />
                </PageControls>
            }
            aside={
                <DescriptionList css={{ marginBottom: scale(5) }}>
                    <DescriptionListItem
                        name="ID:"
                        value={!isCreation ? <CopyButton>{`${discountId}`}</CopyButton> : '-'}
                    />
                    {getDefaultDates({ ...discount }).map(item => (
                        <DescriptionListItem {...item} key={item.name} />
                    ))}
                </DescriptionList>
            }
        >
            {deferredIsLoading ? (
                <LoadingSkeleton
                    count={5}
                    css={{
                        marginBottom: scale(2),
                    }}
                    height={scale(6)}
                />
            ) : (
                <Layout cols={4}>
                    <Layout.Item col={4}>
                        <FormField name="name" label="Название" />
                    </Layout.Item>
                    <Layout.Item col={2}>
                        <FormFieldWrapper name="type" label="Тип">
                            <Select hideClearButton options={discountTypes} />
                        </FormFieldWrapper>
                    </Layout.Item>
                    <Layout.Item col={2}>
                        <Layout cols={2}>
                            <Layout.Item>
                                <FormFieldWrapper name="value_type" label="Тип значения">
                                    <Select hideClearButton options={discountValueTypes} />
                                </FormFieldWrapper>
                            </Layout.Item>
                            <Layout.Item>
                                <FormField
                                    name="value"
                                    label={
                                        watch('value_type') === DiscountValueTypeEnum.PERCENT
                                            ? 'Значение в процентах'
                                            : 'Значение в рублях'
                                    }
                                    type="number"
                                />
                                <p css={{ color: colors.grey600, marginTop: scale(1, true) }}>
                                    {watch('value_type') === DiscountValueTypeEnum.PERCENT
                                        ? 'Значение от 0 до 100'
                                        : 'Значение больше 0'}
                                </p>
                            </Layout.Item>
                        </Layout>
                    </Layout.Item>
                    <Layout.Item col={2}>
                        <CalendarRange
                            label="Введите дату"
                            nameFrom="start_date"
                            nameTo="end_date"
                            css={{ grow: 2 }}
                            disabled={infinite || !canEdit}
                        />
                        <FormFieldWrapper name="infinite">
                            <FormCheckbox
                                css={{ marginTop: scale(1) }}
                                onChange={e => {
                                    const newInfinite = e.currentTarget.checked;
                                    if (newInfinite) {
                                        setValue('start_date', null);
                                        setValue('end_date', null);
                                    }
                                }}
                            >
                                Бессрочный
                            </FormCheckbox>
                        </FormFieldWrapper>
                    </Layout.Item>
                    <Layout.Item col={1}>
                        <FormFieldWrapper name="status" label="Статус">
                            <Select hideClearButton options={discountStatuses} />
                        </FormFieldWrapper>
                    </Layout.Item>
                    <Layout.Item col={4}>
                        <FormFieldWrapper name="promo_code_only" css={{ width: 'fit-content' }}>
                            <FormCheckbox>Скидка действительна только по промокоду</FormCheckbox>
                        </FormFieldWrapper>
                    </Layout.Item>
                </Layout>
            )}
        </PageTemplate>
    );
};

const MarketingDiscounts = () => {
    const { query, push } = useRouter();

    const { access, isIDForbidden } = useDiscountAccess();

    const discountId = (query?.entity_id && +query.entity_id) || 0;

    const {
        data: apiDiscount,
        isFetching: isLoading,
        error,
    } = useDiscount(discountId, ['products', 'products.product'].join(','), access.ID.view);

    const discount = apiDiscount?.data;

    const createDiscount = useDiscountCreate();
    const editDiscount = useDiscountEdit();

    useError(createDiscount.error || editDiscount.error);
    useSuccess(createDiscount.isSuccess || editDiscount.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const isCreation = query?.entity_id === CREATE_PARAM;

    useError(error);

    const initialValues = useMemo(
        () => ({
            infinite: discount ? !discount.start_date && !discount.end_date : false,
            type: discount ? discount?.type : '',
            name: discount?.name || '',
            value_type: discount ? discount.value_type : '',
            value: formatDiscountFromKopecks(discount?.value_type, discount?.value),
            status: discount ? discount.status : '',
            start_date: discount?.start_date ? new Date(discount.start_date).getTime() : null,
            end_date: discount?.end_date ? new Date(discount.end_date).getTime() : null,
            promo_code_only: discount ? discount.promo_code_only : false,
        }),
        [discount]
    );

    const [isOffer, setIsOffer] = useState(false);
    const [isAddingProducts, setAddingProducts] = useState(false);

    useEffect(() => {
        if (!discount) return;

        setIsOffer(discount.type === DiscountTypeEnum.OFFER);
    }, [discount]);

    const deleteDiscount = useDeleteDiscount();

    const { popupState, popupDispatch, ActionPopup, ActionEnum, ActionType } = useActionPopup();

    const onDeleteDiscount = useCallback(async () => {
        await deleteDiscount.mutateAsync(discount!.id);
        push('/marketing/discounts');
    }, [discount, deleteDiscount, push]);

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const canEditStatus = (_: Pick<typeof initialValues, 'type'>) => true;

    const title = isCreation ? 'Создание скидки' : `Редактировать скидку ${discountId}`;
    const returnToListing = useRef(false);

    const isNotFound = isPageNotFound({ id: discountId, error, isCreation });

    return (
        <PageWrapper
            title={title}
            isLoading={isLoading || createDiscount.isPending || editDiscount.isPending}
            isNotFound={isNotFound}
            isForbidden={isIDForbidden}
        >
            <FormWrapper
                disabled={isCreation ? !access.ID.create : !access.ID.edit}
                enableReinitialize
                initialValues={initialValues}
                validationSchema={Yup.object().shape({
                    type: Yup.string().required(ErrorMessages.REQUIRED),
                    name: Yup.string().required(ErrorMessages.REQUIRED),
                    value_type: Yup.string().required(ErrorMessages.REQUIRED),
                    value: Yup.number()
                        .when('value_type', {
                            is: (val: any) => Number(val) === Number(DiscountValueTypeEnum.PERCENT),
                            then: schema =>
                                schema
                                    .transform(val => (Number.isNaN(val) ? undefined : val))
                                    .min(0, `${ErrorMessages.GREATER_OR_EQUAL} 0`)
                                    .max(100, `${ErrorMessages.LESS_OR_EQUAL} 100`),
                            otherwise: schema =>
                                schema
                                    .transform(val => (Number.isNaN(val) ? undefined : val))
                                    .moreThan(0, `${ErrorMessages.GREATER} 0`),
                        })
                        .required(ErrorMessages.REQUIRED),
                    status: Yup.string().when('type', ([type], schema) =>
                        canEditStatus({ type }) ? schema.required(ErrorMessages.REQUIRED) : schema.nullable()
                    ),
                    infinite: Yup.boolean().test(
                        `can-uncheck`,
                        'Введите дату или выберите "бессрочный"',
                        (val, ctx) => {
                            const dates = [ctx.parent.start_date, ctx.parent.end_date];
                            const check = dates.every(e => e !== -1 && e !== undefined);
                            const isValid = check || val || false;
                            return isValid;
                        }
                    ),
                    promo_code_only: Yup.boolean().required(ErrorMessages.REQUIRED),
                })}
                onChange={vals => {
                    const newIsOffer = vals.type === DiscountTypeEnum.OFFER;
                    if (isOffer === newIsOffer) return;

                    setIsOffer(newIsOffer);
                }}
                onSubmit={async values => {
                    const data = {
                        ...values,
                        type: +values.type,
                        value_type: +values.value_type,
                        status: +values.status,
                        value: formatDiscountFromRub(+values.value_type, +values.value),
                        ...(!values.infinite
                            ? {
                                  start_date: (values.start_date && toISOString(new Date(values.start_date))) || null,
                                  end_date: (values.end_date && toISOString(new Date(values.end_date))) || null,
                              }
                            : {
                                  start_date: null,
                                  end_date: null,
                              }),
                        ...(!canEditStatus(values) && {
                            status: DiscountStatusEnum.SUSPENDED,
                        }),
                    };

                    if (isCreation) {
                        const result = await createDiscount.mutateAsync(data);

                        if (returnToListing.current) {
                            return {
                                method: RedirectMethods.push,
                                redirectPath: `/marketing/discounts`,
                            };
                        }

                        return {
                            method: RedirectMethods.replace,
                            redirectPath: `/marketing/discounts/${result.data.id}`,
                        };
                    }

                    await editDiscount.mutateAsync({ ...data, id: discountId });

                    if (returnToListing.current) {
                        return {
                            method: RedirectMethods.push,
                            redirectPath: `/marketing/discounts`,
                        };
                    }

                    return {
                        method: RedirectMethods.replace,
                        redirectPath: `/marketing/discounts/${discountId}`,
                    };
                }}
            >
                {({ reset }) => (
                    <FormInner
                        discount={discount}
                        title={title}
                        onDelete={() => {
                            popupDispatch({
                                type: ActionType.Delete,
                                payload: {
                                    title: `Вы уверены, что хотите удалить скидку?`,
                                    popupAction: ActionEnum.DELETE,
                                    onAction: () => {
                                        reset();
                                        onDeleteDiscount();
                                    },
                                },
                            });
                        }}
                        onApply={() => {
                            returnToListing.current = false;
                        }}
                        onSubmit={() => {
                            returnToListing.current = true;
                        }}
                    />
                )}
            </FormWrapper>
            {!isCreation && (
                <div
                    css={{
                        ...(!isOffer
                            ? {
                                  visibility: 'hidden',
                                  pointerEvents: 'none',
                              }
                            : {
                                  marginTop: scale(2),
                              }),
                    }}
                >
                    <BindedProducts discountId={discountId} onAdd={() => setAddingProducts(true)} />
                </div>
            )}

            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />

            <AddProductsPopup
                discountId={discountId}
                isOpen={isAddingProducts}
                onClose={() => setAddingProducts(false)}
            />
        </PageWrapper>
    );
};

export default MarketingDiscounts;
