import { Block, useActionPopup } from '@ensi-platform/core-components';
import { Table as TableType } from '@tanstack/react-table';
import { useRouter } from 'next/router';
import { useCallback, useMemo } from 'react';

import {
    DiscountProduct,
    useDiscountProducts,
    useDiscountProductsMeta,
    useDiscountUnbindProducts,
} from '@api/marketing';

import { useError, useSuccess } from '@context/modal';

import { initialSort } from '@views/logistic/pickup-points/[id]/scripts/constants';
import { useCatalogAccess } from '@views/products/catalog/scripts/hooks';

import LoadingSkeleton from '@controls/LoadingSkeleton';

import AutoFilters from '@components/AutoFilters';
import Table, { ExtendedRow, TooltipItem, TrProps, useSorting, useTable } from '@components/Table';
import { getSelectColumn, getSettingsColumn } from '@components/Table/columns';
import { RowTooltipWrapper, TableEmpty, TableFooter, TableHeader } from '@components/Table/components';

import { ITEMS_PER_PRODUCTS_PAGE, ModalMessages } from '@scripts/constants';
import { ActionType } from '@scripts/enums';
import { Button, Layout, scale, typography } from '@scripts/gds';
import { declOfNum, getPagination, getTotal, getTotalPages } from '@scripts/helpers';
import { useRedirectToNotEmptyPage, useTableList } from '@scripts/hooks';
import { useAutoColumns, useAutoFilters, useAutoTableData } from '@scripts/hooks/autoTable';
import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import { useDiscountAccess } from '../useDiscountAccess';

const BindedProducts = ({ onAdd, discountId }: { discountId: number; onAdd: () => void }) => {
    const { query, push, pathname } = useRouter();

    const pageKey = 'products-page';

    const { access } = useDiscountAccess();

    const { data: metaData, error: metaError, isFetching: isMetaLoading } = useDiscountProductsMeta();

    const { popupState, popupDispatch, ActionPopup, ActionEnum } = useActionPopup();

    const meta = metaData?.data;

    const {
        metaField,
        values,
        filtersActive,
        URLHelper,
        searchRequestFilter,
        searchRequestIncludes,
        emptyInitialValues,
        reset,
        clearInitialValue,
        codesToSortKeys,
    } = useAutoFilters(meta);

    const { activePage, itemsPerPageCount, setItemsPerPageCount } = useTableList({
        defaultSort: meta?.default_sort,
        defaultPerPage: ITEMS_PER_PRODUCTS_PAGE,
        codesToSortKeys,
        pageKey,
    });

    const [{ backendSorting }, sortingPlugin] = useSorting<DiscountProduct>(pathname, meta?.fields, initialSort);

    const {
        data,
        isFetching: isLoading,
        isInitialLoading: isIdle,
        error,
    } = useDiscountProducts(
        {
            sort: backendSorting,
            filter: {
                ...searchRequestFilter,
                discount_id: discountId,
            },
            pagination: getPagination(activePage, itemsPerPageCount),
        },
        searchRequestIncludes,
        Boolean(meta) && !!backendSorting && !!discountId
    );

    const convertedTableData = useMemo(
        () =>
            data?.data.map(e => ({
                ...e,
                customListLink: '/products/catalog',
                customIdResolver: (original: DiscountProduct) => original?.product?.id,
            })),
        [data?.data]
    );

    const total = getTotal(data);
    const totalPages = getTotalPages(data, itemsPerPageCount);

    useRedirectToNotEmptyPage({ activePage, itemsPerPageCount, total, pageKey });

    const unbindProducts = useDiscountUnbindProducts(discountId);

    useSuccess(unbindProducts.isSuccess && ModalMessages.SUCCESS_UPDATE);
    useError(error || metaError || unbindProducts.error);

    const renderHeader = useCallback(
        (table: TableType<DiscountProduct>) => {
            const selected = table.getSelectedRowModel().flatRows;
            return (
                <TableHeader css={{ justifyItems: 'space-between' }}>
                    <Layout type="flex" gap={scale(4)} align="center" css={{ width: '100%', minHeight: scale(9) }}>
                        <Layout.Item>
                            <p css={{ lineHeight: '32px', ...typography('bodySm') }}>
                                <b>Всего привязано:</b> {`${total} ${declOfNum(total, ['товар', 'товара', 'товаров'])}`}
                            </p>
                        </Layout.Item>
                        {!!selected.length && (
                            <>
                                <Layout.Item>
                                    <b>Выбрано:</b> {selected.length}
                                </Layout.Item>
                                <Layout.Item>
                                    <Button type="button" onClick={table.reset}>
                                        Снять выделение
                                    </Button>
                                </Layout.Item>
                                <Layout.Item>
                                    <Button
                                        disabled={!access.ID.edit}
                                        type="button"
                                        theme="dangerous"
                                        onClick={() => {
                                            popupDispatch({
                                                type: ActionType.Delete,
                                                payload: {
                                                    title: `Вы уверены, что хотите отвязать выбранные товары?`,
                                                    popupAction: ActionEnum.UNTIE,
                                                    onAction: async () => {
                                                        await unbindProducts.mutateAsync({
                                                            ids: selected
                                                                .filter(e => e.getIsSelected())
                                                                .map(e => Number(e?.original.id)),
                                                        });

                                                        push({
                                                            pathname,
                                                            query: {
                                                                entity_id: query?.entity_id,
                                                            },
                                                        });
                                                        table.reset();
                                                    },
                                                    children: <p>Выбрано товаров: {selected.length}</p>,
                                                },
                                            });
                                        }}
                                    >
                                        Отвязать выбранные товары
                                    </Button>
                                </Layout.Item>
                            </>
                        )}
                    </Layout>
                    <Button onClick={onAdd} disabled={!access.ID.edit}>
                        Добавить
                    </Button>
                </TableHeader>
            );
        },
        [
            total,
            access.ID.edit,
            onAdd,
            popupDispatch,
            ActionEnum.UNTIE,
            unbindProducts,
            push,
            pathname,
            query?.entity_id,
        ]
    );

    const goToDetailPage = useGoToDetailPage({ pathname: '/products/catalog' });

    const goToDeleteRow = useCallback(
        (originalRows: ExtendedRow['original'][] | undefined) => {
            if (originalRows) {
                popupDispatch({
                    type: ActionType.Delete,
                    payload: {
                        title: `Вы уверены, что хотите отвязать выбранные товары?`,
                        popupAction: ActionEnum.UNTIE,
                        onAction: async () => {
                            await unbindProducts.mutateAsync({
                                ids: [Number(originalRows[0].original.id)],
                            });

                            push({
                                pathname,
                                query: {
                                    entity_id: query?.entity_id,
                                },
                            });
                        },
                        children: <p>Выбрано товаров: {[Number(originalRows[0].original.id)].length}</p>,
                    },
                });
            }
        },
        [ActionEnum.UNTIE, pathname, popupDispatch, push, query?.entity_id, unbindProducts]
    );

    const productsAccess = useCatalogAccess();

    const tooltipContent = useMemo<TooltipItem[]>(
        () => [
            {
                type: 'edit',
                text: 'Перейти в карточку товара',
                action: goToDetailPage,
                isDisable: () => !productsAccess.canViewAnyDetail,
            },
            {
                type: 'delete',
                text: 'Отвязать',
                action: goToDeleteRow,
                isDisable: () => !access.ID.edit,
            },
        ],
        [goToDetailPage, goToDeleteRow, productsAccess.canViewAnyDetail, access.ID.edit]
    );

    const autoGeneratedColumns = useAutoColumns(meta);

    const rows = useAutoTableData<DiscountProduct>(convertedTableData, metaField);

    const columns = useMemo(
        () => [
            getSelectColumn(),
            ...autoGeneratedColumns,
            getSettingsColumn({
                columnsToDisable: [],
                visibleColumns: meta?.default_list,
                tooltipContent,
            }),
        ],
        [autoGeneratedColumns, meta?.default_list]
    );

    const getTooltipForRow = useCallback(() => tooltipContent || [], [tooltipContent]);
    const renderRow = useCallback(
        ({ children, ...props }: TrProps<any>) => (
            <RowTooltipWrapper
                {...props}
                getTooltipForRow={getTooltipForRow}
                onDoubleClick={() => {
                    push(
                        `${props.row?.original.customListLink}/${props.row?.original.customIdResolver(
                            props.row?.original
                        )}`
                    );
                }}
            >
                {children}
            </RowTooltipWrapper>
        ),
        [getTooltipForRow]
    );

    const table = useTable(
        {
            data: rows,
            columns,
            meta: {
                tableKey: pathname,
            },
        },
        [sortingPlugin]
    );

    if (isLoading || isIdle)
        return (
            <>
                <div css={{ marginBottom: scale(3) }}>
                    <LoadingSkeleton height={200} />
                </div>
                <LoadingSkeleton
                    count={10}
                    height={40}
                    css={{
                        marginBottom: scale(1),
                    }}
                />
            </>
        );

    return (
        <>
            <h4 css={{ ...typography('h3'), marginBottom: scale(2) }}>Привязанные товары</h4>
            <Block css={{ marginBottom: scale(3) }}>
                <AutoFilters
                    initialValues={values}
                    emptyInitialValues={emptyInitialValues}
                    onSubmit={URLHelper}
                    filtersActive={filtersActive}
                    meta={meta}
                    queryPart={{ entity_id: discountId }}
                    isLoading={isMetaLoading}
                    onResetFilters={reset}
                    clearInitialValue={clearInitialValue}
                />
            </Block>
            <Block>
                <Block.Body>
                    {renderHeader(table)}
                    <Table instance={table} Tr={renderRow} />
                    {rows.length === 0 && !isLoading ? (
                        <TableEmpty
                            filtersActive={filtersActive}
                            titleWithFilters="Привязанные товары не найдены. Попробуйте сбросить фильтры."
                            titleWithoutFilters="Нет привязанных товаров"
                            addItems={() => setItemsPerPageCount(10)}
                            onResetFilters={() => {
                                reset();
                                push({
                                    pathname,
                                    query: {
                                        entity_id: discountId,
                                    },
                                });
                            }}
                        />
                    ) : (
                        <TableFooter
                            pages={totalPages}
                            itemsPerPageCount={itemsPerPageCount}
                            setItemsPerPageCount={setItemsPerPageCount}
                            pageKey={pageKey}
                        />
                    )}
                </Block.Body>
            </Block>

            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </>
    );
};

export default BindedProducts;
