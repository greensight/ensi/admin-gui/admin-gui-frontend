import { Form, FormFieldWrapper, Popup, PopupContent, PopupFooter, PopupHeader } from '@ensi-platform/core-components';
import { UseMutationResult } from '@tanstack/react-query';
import { createColumnHelper } from '@tanstack/react-table';
import { useMemo } from 'react';
import * as Yup from 'yup';

import { CommonResponse, FetchError } from '@api/common/types';
import { DiscountsStatusFormData, useDiscountStatuses } from '@api/marketing';

import { useError } from '@context/modal';

import { State } from '@views/marketing/discounts';
import { prepareValuesForFutureSelect } from '@views/marketing/helpers';

import Select from '@controls/Select';

import Table, { TableColumnDefAny, useSorting, useTable } from '@components/Table';

import { ErrorMessages } from '@scripts/constants';
import { Button, scale } from '@scripts/gds';

const ChangeDiscountStatusPopup = ({
    state,
    isOpen,
    onClose,
    changeDiscountsStatus,
}: {
    state: State;
    isOpen: boolean;
    onClose: () => void;
    changeDiscountsStatus: UseMutationResult<CommonResponse<null>, FetchError, DiscountsStatusFormData, unknown>;
}) => {
    const { data: apiStatuses, error: errorStatuses } = useDiscountStatuses();
    const discountStatuses = useMemo(
        () => (apiStatuses?.data ? prepareValuesForFutureSelect(apiStatuses.data) : []),
        [apiStatuses]
    );
    useError(errorStatuses);

    const [{ sorting }, sortingPlugin] = useSorting<Record<string, any>>(
        'change-discount-status',
        [],
        undefined,
        false
    );

    const columnHelper = createColumnHelper<Record<string, any>>();
    const columns: TableColumnDefAny[] = [
        columnHelper.accessor('id', {
            header: 'ID',
            cell: props => props.getValue(),
        }),
        columnHelper.accessor('name', {
            header: 'Название',
            cell: props => props.getValue(),
        }),
    ];
    const table = useTable(
        {
            data: state.selected_rows,
            columns,
            meta: {
                tableKey: `change-discount-status`,
            },
            state: {
                sorting,
            },
        },
        [sortingPlugin]
    );

    return (
        <Popup open={isOpen} onClose={onClose} size="minMd">
            <Form
                onSubmit={async ({ changedStatus }) => {
                    const idsToUpdate = state.selected_rows.map(i => i.id);
                    if (idsToUpdate.length > 0) {
                        onClose();

                        await changeDiscountsStatus.mutateAsync({
                            ids: idsToUpdate,
                            status: +changedStatus!,
                        });
                    }
                }}
                initialValues={
                    {
                        changedStatus: null,
                    } as {
                        changedStatus: number | null;
                    }
                }
                validationSchema={Yup.object().shape({
                    changedStatus: Yup.number()
                        .transform(val => (Number.isNaN(val) ? undefined : val))
                        .required(ErrorMessages.REQUIRED),
                })}
            >
                <PopupHeader title="Изменение статуса" />
                <PopupContent>
                    <Table instance={table} css={{ marginBottom: scale(2) }} />
                    <FormFieldWrapper name="changedStatus" label="Статус" css={{ marginBottom: scale(2) }}>
                        <Select options={discountStatuses} hideClearButton />
                    </FormFieldWrapper>
                </PopupContent>
                <PopupFooter>
                    <Button type="submit" theme="primary">
                        Изменить статус
                    </Button>
                </PopupFooter>
            </Form>
        </Popup>
    );
};

export default ChangeDiscountStatusPopup;
