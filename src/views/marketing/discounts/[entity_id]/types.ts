export const enum DiscountValueTypeEnum {
    PERCENT = 1,
    RUBLES = 2,
}

export const enum DiscountTypeEnum {
    OFFER = 1,
    BASKET = 2,
}

export const enum DiscountStatusEnum {
    ACTIVE = 1,
    SUSPENDED = 2,
}
