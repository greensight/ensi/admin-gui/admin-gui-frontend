import { fromKopecksToRouble, fromRoubleToKopecks } from "@scripts/helpers";
import { DiscountValueTypeEnum } from "../types";

export const formatDiscountFromKopecks = (valueType?: number, value?: number) => {
    if (valueType === undefined || value === undefined) return 0;
    if (valueType === DiscountValueTypeEnum.PERCENT) return value;
    return fromKopecksToRouble(value);
};

export const formatDiscountFromRub = (valueType?: number, value?: number) => {
    if (valueType === undefined || value === undefined) return 0;
    if (valueType === DiscountValueTypeEnum.PERCENT) return value;
    return fromRoubleToKopecks(value);
};