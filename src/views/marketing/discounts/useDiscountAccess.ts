import { useAccess } from '@scripts/hooks';
import { useIDForbidden } from '@scripts/hooks/useIDForbidden';

export const AccessMatrix = {
    LIST: {
        /** Пользователю доступен табличный список со скидками */
        view: 1301,
        massStatusUpdate: 1306,
    },
    ID: {
        /** Пользователю доступна детальная страница скидки со всеми данными для просмотра и без возможности редактирования полей */
        view: 1302,
        create: 1303,
        edit: 1304,
        delete: 1305,
    },
};

export const useDiscountAccess = () => {
    const access = useAccess(AccessMatrix);
    const isIDForbidden = useIDForbidden(access);

    return { access, isIDForbidden };
};
