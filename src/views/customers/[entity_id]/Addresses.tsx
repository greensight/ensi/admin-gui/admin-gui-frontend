import {
    Block,
    Form,
    FormField,
    FormFieldWrapper,
    Popup,
    PopupContent,
    PopupHeader,
    Textarea,
    useActionPopup,
} from '@ensi-platform/core-components';
import { Row, createColumnHelper } from '@tanstack/react-table';
import { useRouter } from 'next/router';
import { useMemo } from 'react';
import type { FieldValues } from 'react-hook-form';
import * as Yup from 'yup';

import {
    Address,
    useAddresses,
    useCreateAddress,
    useCustomer,
    useDeleteAddress,
    useUpdateAddress,
} from '@api/customers';
import { AddressMutate } from '@api/customers/types';

import Switcher from '@controls/Switcher';

import Table, { TableColumnDefAny, TooltipItem, useSorting, useTable } from '@components/Table';
import { getSelectColumn, getSettingsColumn } from '@components/Table/columns';

import { ErrorMessages } from '@scripts/constants';
import { ActionType } from '@scripts/enums';
import { Button, Layout, scale } from '@scripts/gds';
import { usePopupState } from '@scripts/hooks';

import PlusIcon from '@icons/small/plus.svg';

type State = {
    id?: string;
    address?: string;
    default?: boolean;
    porch?: string;
    intercom?: string;
    floor?: string;
    flat?: string;
    comment?: string;
    action?: ActionType;
    open?: boolean;
};
const columnHelper = createColumnHelper<Address>();

const Addresses = () => {
    const { query } = useRouter();
    const id = +(query.entity_id?.toString() || '');

    const { data: customerData } = useCustomer(id);
    const customerId = customerData?.data?.id;
    const { data: addresses, refetch } = useAddresses({ filter: { customer_id: customerId }, enabled: !!customerId });

    const tableData = useMemo(
        () =>
            addresses?.data?.map(address => ({
                id: address.id,
                address: address.address_string,
                default: address.default ? 'Да' : 'Нет',
                porch: address.porch,
                intercom: address.intercom,
                floor: address.floor,
                flat: address.flat,
                comment: address.comment,
            })) || [],
        [addresses]
    );

    const createAddress = useCreateAddress();
    const updateAddress = useUpdateAddress();
    // const setDefaultAddress = useSetDefaultAddress();
    const deleteAddress = useDeleteAddress();

    const initialValues = {
        id: '',
        address: '',
        default: false,
        porch: '',
        intercom: '',
        floor: '',
        flat: '',
        comment: '',
    };
    const initialState = {
        ...initialValues,
        action: ActionType.Close,
        open: false,
    };

    const [addressesPopupState, addressesPopupDispatch] = usePopupState<State>(initialState);
    const { popupState, popupDispatch, ActionPopup, ActionEnum } = useActionPopup();

    const close = () => addressesPopupDispatch({ type: ActionType.Close });

    const onRowEdit = (row: Row<any>) => {
        if (row) {
            addressesPopupDispatch({
                type: ActionType.Edit,
                payload: {
                    id: row.original.id,
                    address: row.original.address,
                    default: row.original.default,
                    porch: row.original.porch,
                    intercom: row.original.intercom,
                    floor: row.original.floor,
                    flat: row.original.flat,
                    comment: row.original.comment,
                },
            });
        }
    };

    const tooltipContent: TooltipItem[] = [
        {
            type: 'delete',
            text: 'Редактировать адрес',
            action: rows => {
                if (rows) onRowEdit(rows[0]);
            },
        },
        {
            type: 'delete',
            text: 'Удалить',
            action: async (rows?: Row<any>[]) => {
                if (rows) {
                    popupDispatch({
                        type: ActionType.Delete,
                        payload: {
                            title: `Вы уверены, что хотите удалить адрес?`,
                            popupAction: ActionEnum.DELETE,
                            onAction: async () => {
                                if (rows[0].original.id) {
                                    await deleteAddress.mutateAsync(+rows[0].original.id);
                                }
                            },
                            children: (
                                <p css={{ marginBottom: scale(2) }}>
                                    {rows[0].original.id}# {rows[0].original.address}
                                </p>
                            ),
                        },
                    });
                }
            },
        },
    ];

    const onSubmit = async (vals: FieldValues) => {
        if (customerId) {
            const suggestion = vals.address;
            const params: AddressMutate = {
                customer_id: customerId,
                address_string: suggestion.value,
                default: vals.default,
                post_index: suggestion.data.postal_code,
                country_code: suggestion.data.country_iso_code,
                region: suggestion.data.region,
                region_guid: suggestion.data.region_fias_id,
                area: suggestion.data.area,
                area_guid: suggestion.data.area_fias_id,
                city: suggestion.data.city,
                city_guid: suggestion.data.city_fias_id,
                street: suggestion.data.street,
                house: suggestion.data.house,
                block: suggestion.data.block,
                porch: vals.porsh,
                intercom: vals.intercom,
                floor: vals.floor,
                flat: vals.flat,
                comment: vals.comment,
                geo_lat: vals.geo_lat,
                geo_lon: vals.geo_lon,
            };
            if (addressesPopupState.id) {
                await updateAddress.mutateAsync({ id: +addressesPopupState.id, ...params });
                refetch();
                close();
            } else {
                await createAddress.mutateAsync(params);
                close();
            }
        }
    };

    const [{ sorting }, sortingPlugin] = useSorting<Address>('customers_addresses', [], undefined, false);

    const columns: TableColumnDefAny[] = [
        getSelectColumn<Address>(undefined, 'customers_addresses'),
        columnHelper.accessor('id', {
            header: 'ID',
            cell: props => props.getValue(),
        }),
        columnHelper.accessor('address', {
            header: 'Полный адрес',
            cell: props => props.getValue(),
        }),
        columnHelper.accessor('porch', {
            header: 'Подъезд',
            cell: props => props.getValue(),
        }),
        columnHelper.accessor('floor', {
            header: 'Этаж',
            cell: props => props.getValue(),
        }),
        columnHelper.accessor('flat', {
            header: 'Квартира',
            cell: props => props.getValue(),
        }),
        columnHelper.accessor('intercom', {
            header: 'Домофон',
            cell: props => props.getValue(),
        }),
        columnHelper.accessor('comment', {
            header: 'Комментарий',
            cell: props => props.getValue(),
        }),
        columnHelper.accessor('default', {
            header: 'По-умолчанию',
            cell: props => props.getValue(),
        }),
        getSettingsColumn({
            tooltipContent,
        }),
    ];

    const table = useTable(
        {
            data: tableData,
            columns,
            meta: {
                tableKey: `customers_addresses`,
            },
            state: {
                sorting,
            },
        },
        [sortingPlugin]
    );

    return (
        <Block css={{ borderTopLeftRadius: 0, borderTopRightRadius: 0 }}>
            <Block.Header>
                <div>
                    <Button onClick={() => addressesPopupDispatch({ type: ActionType.Add })} Icon={PlusIcon}>
                        Добавить адрес
                    </Button>
                </div>
            </Block.Header>
            <Block.Body>{tableData.length ? <Table instance={table} /> : <p>Адресов не найдено</p>}</Block.Body>
            <Popup
                open={Boolean(addressesPopupState?.open && addressesPopupState.action !== ActionType.Delete)}
                onClose={close}
                size="sm"
            >
                <PopupHeader
                    title={`${addressesPopupState.action === ActionType.Add ? 'Создание' : 'Редактирование'} адреса`}
                />
                <PopupContent>
                    <Form
                        initialValues={addressesPopupState}
                        onSubmit={onSubmit}
                        validationSchema={Yup.object().shape({
                            address: Yup.object().required(ErrorMessages.REQUIRED),
                        })}
                    >
                        <Layout cols={4}>
                            <Layout.Item col={4}>
                                <FormField name="address" label="Адрес" />
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <FormFieldWrapper name="default">
                                    <Switcher>Адрес по-умолчанию</Switcher>
                                </FormFieldWrapper>
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <FormField name="porch" label="Подъезд" type="number" />
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <FormField name="intercom" label="Домофон" />
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <FormField name="floor" label="Этаж" type="number" />
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <FormField name="flat" label="Квартира" type="number" />
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <FormFieldWrapper name="comment" label="Комментарий">
                                    <Textarea rows={3} />
                                </FormFieldWrapper>
                            </Layout.Item>
                            <Layout.Item col={4} justify="end">
                                <Button theme="secondary" css={{ marginRight: scale(2) }} onClick={close}>
                                    Отменить
                                </Button>
                                <Button type="submit">
                                    {addressesPopupState.action === ActionType.Add ? 'Создать' : 'Сохранить'}
                                </Button>
                            </Layout.Item>
                        </Layout>
                    </Form>
                </PopupContent>
            </Popup>

            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </Block>
    );
};

export default Addresses;
