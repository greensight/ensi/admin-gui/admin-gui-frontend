import {
    Block,
    CopyButton,
    DescriptionList,
    DescriptionListItem,
    DescriptionListItemType,
    Form,
    FormField,
    FormFieldWrapper,
    FormReset,
    Popup,
    PopupContent,
    PopupHeader,
    Tabs,
    Textarea,
    getDefaultDates,
    useActionPopup,
} from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { useEffect, useMemo, useRef, useState } from 'react';
import type { FieldValues } from 'react-hook-form';
import * as Yup from 'yup';

import {
    useCustomer,
    useCustomerDeletePersonalData,
    useDeleteCustomerAvatar,
    usePatchCustomer,
    useUploadCustomerAvatar,
} from '@api/customers';

import { useError, useSuccess } from '@context/modal';

import Favorites from '@views/customers/[entity_id]/Favorites';
import useOrdersAccess from '@views/orders/list/useOrdersAccess';

import Dropzone from '@controls/Dropzone';
import { downloadFile } from '@controls/Dropzone/utils';
import Legend from '@controls/Legend';
import Select from '@controls/Select';

import PageControls from '@components/PageControls';
import PageLeaveGuard from '@components/PageLeaveGuard';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';

import { CREATE_PARAM, ErrorMessages, FileSizes, ImageAcceptTypes, ModalMessages } from '@scripts/constants';
import { ButtonNameEnum, CustomerGender, customerGenderValues, customerStatusValues } from '@scripts/enums';
import { Button, Layout, MEDIA_QUERIES, colors, scale } from '@scripts/gds';
import { isPageNotFound } from '@scripts/helpers';
import { useTabs } from '@scripts/hooks/useTabs';

import { useCustomersAccess } from '../useCustomersAccess';
import Addresses from './Addresses';
import Orders from './Orders';

const Customer = () => {
    const access = useCustomersAccess();
    const customerAccess = useCustomersAccess();
    const { query, pathname, push } = useRouter();
    const listLink = pathname.split(`[entity_id]`)[0];

    const { getTabsProps } = useTabs();
    const [isChangeStatusOpen, setIsChangeStatusOpen] = useState(false);

    const statuses = useMemo(
        () =>
            Object.keys(customerStatusValues).map(k => ({
                value: +k,
                key: customerStatusValues[k as keyof typeof customerStatusValues],
            })),
        []
    );

    const genderOptions = useMemo(
        () =>
            Object.keys(customerGenderValues).map(k => ({
                value: +k,
                key: customerGenderValues[k as keyof typeof customerGenderValues],
            })),
        []
    );

    const id = +(query.entity_id?.toString() || '');
    const isCreation = query.entity_id === CREATE_PARAM;

    const { data, isFetching: isLoading, isInitialLoading: isIdle, error } = useCustomer(id);
    const customer = data?.data;
    const patchCustomer = usePatchCustomer();
    const deletePersonalData = useCustomerDeletePersonalData();
    const updateAvatar = useUploadCustomerAvatar();
    const deleteAvatar = useDeleteCustomerAvatar();

    const { popupState, popupDispatch, ActionPopup, ActionEnum, ActionType } = useActionPopup();

    useError(patchCustomer.error);
    useError(deletePersonalData.error);

    useSuccess(patchCustomer.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');

    const onStatusUpdate = async (vals: FieldValues) => {
        setIsChangeStatusOpen(false);

        if (customer) {
            await patchCustomer.mutateAsync({
                id: +id,
                comment_status: vals?.comment,
            });
        }
    };

    const buttonNameRef = useRef<ButtonNameEnum | null>(null);

    const clientName = customer?.is_deleted ? '(удален)' : customer?.first_name;
    const pageTitle = `Клиент ${clientName}`;

    const canDeletePd = !isLoading && !customer?.is_deleted && access.ID.delete;

    const [initialAvatarFile, setInitialAvatarFile] = useState<File[]>([]);

    useEffect(() => {
        if (!customer?.avatar) {
            setInitialAvatarFile([]);
            return;
        }
        downloadFile(customer.avatar).then(res => {
            if (res) setInitialAvatarFile([res]);
        });
    }, [customer?.avatar]);

    const ordersAccess = useOrdersAccess();

    const isNotFound = isPageNotFound({ id, error });

    return (
        <>
            <PageWrapper
                title={pageTitle}
                isLoading={
                    isLoading || isIdle || patchCustomer.isPending || updateAvatar.isPending || deleteAvatar.isPending
                }
                isForbidden={!access.ID.view}
                isNotFound={isNotFound}
            >
                <Form
                    initialValues={{
                        name: customer?.first_name || '',
                        middleName: customer?.middle_name || '',
                        lastName: customer?.last_name || '',
                        gender: customer?.gender || CustomerGender.MALE,
                        comment: customer?.comment_status || '',
                        city: customer?.city || '',
                        file: initialAvatarFile,
                    }}
                    disabled={!access.ID.edit}
                    enableReinitialize
                    validationSchema={Yup.object().shape({
                        name: Yup.string().required(ErrorMessages.REQUIRED),
                        middleName: Yup.string().nullable(),
                        lastName: Yup.string().required(ErrorMessages.REQUIRED),
                        gender: Yup.number()
                            .transform(val => (Number.isNaN(val) ? undefined : val))
                            .required(ErrorMessages.REQUIRED),
                        comment: Yup.string().nullable(),
                        city: Yup.string().nullable(),
                    })}
                    css={{ width: '100%', marginBottom: scale(3) }}
                    onSubmit={async (values, { reset }) => {
                        if (!isCreation) {
                            await patchCustomer.mutateAsync({
                                id: +id,
                                first_name: values.name,
                                middle_name: values.middleName,
                                last_name: values.lastName,
                                gender: values.gender,
                                comment_status: values.comment,
                                city: values.city,
                            });

                            if (
                                values.file.length > 0 &&
                                values.file[0] &&
                                values.file[0]?.name !== initialAvatarFile[0]?.name
                            ) {
                                const formData = new FormData();
                                formData.append('file', values.file[0]);
                                await updateAvatar.mutateAsync({ id: customer!.id, file: formData });
                            } else if (!values.file.length && initialAvatarFile.length) {
                                await deleteAvatar.mutateAsync(customer!.id);
                            }

                            reset(values);

                            if (buttonNameRef.current === ButtonNameEnum.SAVE) push({ pathname: listLink });
                        }
                    }}
                >
                    <PageLeaveGuard />
                    <PageTemplate
                        h1={pageTitle}
                        backlink={{ text: 'К списку клиентов', href: '/customers' }}
                        customChildren
                        controls={
                            <PageControls
                                access={{
                                    ID: {
                                        view: true,
                                        edit: true,
                                        create: true,
                                    },
                                    LIST: {
                                        view: true,
                                    },
                                }}
                            >
                                {canDeletePd && (
                                    <PageControls.Button
                                        theme="dangerous"
                                        onClick={() => {
                                            popupDispatch({
                                                type: ActionType.Delete,
                                                payload: {
                                                    title: `Вы уверены, что хотите удалить персональные данные клиента?`,
                                                    popupAction: ActionEnum.DELETE,
                                                    onAction: async () => {
                                                        try {
                                                            await deletePersonalData.mutateAsync(+id);
                                                        } catch (err) {
                                                            console.error(err);
                                                        }
                                                    },
                                                },
                                            });
                                        }}
                                    >
                                        Удалить персональные данные
                                    </PageControls.Button>
                                )}
                                <PageControls.Close
                                    onClick={() =>
                                        push({
                                            pathname: listLink,
                                        })
                                    }
                                />
                                <PageControls.Apply
                                    onClick={() => {
                                        buttonNameRef.current = ButtonNameEnum.APPLY;
                                    }}
                                />
                                <PageControls.Save
                                    onClick={() => {
                                        buttonNameRef.current = ButtonNameEnum.SAVE;
                                    }}
                                />
                            </PageControls>
                        }
                        aside={
                            !isCreation && (
                                <>
                                    <div css={{ marginBottom: scale(1) }}>
                                        <span css={{ marginRight: scale(1, true), color: colors?.grey800 }}>ID:</span>
                                        <CopyButton>{`${id}`}</CopyButton>
                                    </div>
                                    <DescriptionList>
                                        {(
                                            [
                                                {
                                                    name: 'Активность',
                                                    value: customer?.active ? 'Активный' : `Неактивный`,
                                                },
                                                {
                                                    name: 'Удален?',
                                                    value: customer?.is_deleted,
                                                    type: 'boolean',
                                                },
                                                ...(customer?.is_deleted
                                                    ? [
                                                          {
                                                              name: 'Дата удаления',
                                                              value: customer.delete_request,
                                                              type: 'date',
                                                          },
                                                      ]
                                                    : []),
                                                ...getDefaultDates({ ...customer }),
                                                {
                                                    name: 'Дата последнего посещения клиента',
                                                    value: customer?.last_visit_date,
                                                    type: 'date',
                                                },
                                                {
                                                    name: 'Часовой пояс клиента',
                                                    value: customer?.timezone,
                                                },
                                            ] as DescriptionListItemType[]
                                        ).map(item => (
                                            <DescriptionListItem key={item.name} {...item} />
                                        ))}
                                    </DescriptionList>
                                </>
                            )
                        }
                    >
                        <Layout cols={1} css={{ flexGrow: 2 }}>
                            <Block>
                                <Block.Body>
                                    <Layout cols={1}>
                                        <FormFieldWrapper label="Аватар" name="file" css={{ marginTop: scale(1) }}>
                                            <Dropzone maxFiles={1} accept={ImageAcceptTypes} maxSize={FileSizes.MB1} />
                                        </FormFieldWrapper>
                                        <Layout.Item>
                                            <Layout cols={3}>
                                                <FormField name="lastName" label="Фамилия" />
                                                <FormField name="name" label="Имя" />
                                                <FormField name="middleName" label="Отчество" />
                                            </Layout>
                                        </Layout.Item>
                                        <Layout.Item>
                                            <Legend label="Телефон" />
                                            {customer?.phone}
                                        </Layout.Item>
                                        <Layout.Item>
                                            <Legend label="E-mail" />
                                            {customer?.email}
                                        </Layout.Item>
                                        <FormFieldWrapper
                                            name="gender"
                                            label="Пол"
                                            css={{
                                                [MEDIA_QUERIES.mdMin]: {
                                                    maxWidth: '50%',
                                                },
                                            }}
                                        >
                                            <Select options={genderOptions} hideClearButton />
                                        </FormFieldWrapper>
                                        <FormField
                                            name="city"
                                            label="Город"
                                            css={{
                                                [MEDIA_QUERIES.mdMin]: {
                                                    maxWidth: '50%',
                                                },
                                            }}
                                        />
                                        <FormFieldWrapper
                                            name="comment"
                                            label="Комментарий"
                                            css={{
                                                [MEDIA_QUERIES.mdMin]: {
                                                    maxWidth: '50%',
                                                },
                                            }}
                                        >
                                            <Textarea minRows={5} />
                                        </FormFieldWrapper>
                                    </Layout>
                                </Block.Body>
                            </Block>
                        </Layout>
                    </PageTemplate>
                </Form>
                <Tabs {...getTabsProps()}>
                    <Tabs.Tab title="Адреса" id="0">
                        <Addresses />
                    </Tabs.Tab>
                    {ordersAccess.canViewListingAndFilters && (
                        <Tabs.Tab title="Заказы" id="1">
                            <Orders customerId={customer?.id!} />
                        </Tabs.Tab>
                    )}
                    {customerAccess.ID.view && (
                        <Tabs.Tab title="Избранные товары" id="2">
                            <Favorites />
                        </Tabs.Tab>
                    )}
                </Tabs>
            </PageWrapper>
            <Popup open={isChangeStatusOpen} onClose={() => setIsChangeStatusOpen(false)} size="minSm">
                <PopupHeader title="Изменить статус" />
                <PopupContent>
                    <Form
                        onSubmit={onStatusUpdate}
                        initialValues={{ status: '', comment: '' }}
                        validationSchema={Yup.object().shape({
                            status: Yup.number()
                                .transform(old => {
                                    if (!+old) return undefined;
                                    return +old;
                                })
                                .required(ErrorMessages.REQUIRED),
                            comment: Yup.string(),
                        })}
                    >
                        <FormFieldWrapper name="status" label="Статус проверки" css={{ marginBottom: scale(2) }}>
                            <Select options={statuses} />
                        </FormFieldWrapper>
                        <FormFieldWrapper name="comment" label="Комментарий к статусу" css={{ marginBottom: scale(4) }}>
                            <Textarea rows={3} />
                        </FormFieldWrapper>
                        <div css={{ display: 'flex', justifyContent: 'flex-end' }}>
                            <FormReset
                                theme="outline"
                                onClick={() => setIsChangeStatusOpen(false)}
                                css={{ marginRight: scale(2) }}
                            >
                                Отменить
                            </FormReset>
                            <Button type="submit" theme="primary">
                                Сохранить
                            </Button>
                        </div>
                    </Form>
                </PopupContent>
            </Popup>

            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </>
    );
};

export default Customer;
