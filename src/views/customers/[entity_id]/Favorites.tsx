import {
    AutocompleteAsync,
    Block,
    Form,
    FormFieldWrapper,
    ModalMessages,
    Popup,
    PopupContent,
    PopupHeader,
    useActionPopup,
} from '@ensi-platform/core-components';
import { Row, createColumnHelper } from '@tanstack/react-table';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useMemo } from 'react';
import type { FieldValues } from 'react-hook-form';
import * as Yup from 'yup';

import { Product } from '@api/catalog';
import { CommonResponse } from '@api/common/types';
import {
    Favorite,
    useCreateMassFavorite,
    useCustomer,
    useDeleteAllFavorites,
    useDeleteMassFavorite,
    useFavorites,
    useSearchFavorites,
} from '@api/customers';
import { useAuthApiClient } from '@api/hooks/useAuthApiClient';

import { useError, useSuccess } from '@context/modal';

import { useCustomersAccess } from '@views/customers/useCustomersAccess';

import Table, { TableColumnDefAny, TooltipItem, useSorting, useTable } from '@components/Table';
import { getSettingsColumn } from '@components/Table/columns';
import { TableFooter } from '@components/Table/components/TableFooter';

import { ErrorMessages, ITEMS_PER_PRODUCTS_PAGE } from '@scripts/constants';
import { ActionType } from '@scripts/enums';
import { Button, Layout, scale, typography } from '@scripts/gds';
import { declOfNum, getPagination, getTotal, getTotalPages } from '@scripts/helpers';
import { useLinkCSS, usePopupState, useRedirectToNotEmptyPage, useTableList } from '@scripts/hooks';

import PlusIcon from '@icons/small/plus.svg';

type State = {
    id?: string;
    productId?: string;
    action?: ActionType;
    open?: boolean;
};

const pageKey = 'favorites';

const Favorites = () => {
    const { query } = useRouter();
    const access = useCustomersAccess();

    const id = query.entity_id?.toString() || '';

    const { activePage, itemsPerPageCount, setItemsPerPageCount } = useTableList({
        defaultSort: 'id',
        codesToSortKeys: { id: 'id' },
        defaultPerPage: ITEMS_PER_PRODUCTS_PAGE,
        pageKey,
    });

    const { data: customerData } = useCustomer(id);
    const [{ backendSorting }, sortingPlugin] = useSorting<Favorite>('customers_favorites', []);

    const customerId = customerData?.data?.id;
    const { data: favorites } = useFavorites({
        filter: { customer_id: customerId },
        include: ['products'],
        sort: backendSorting,
        pagination: getPagination(activePage, itemsPerPageCount),
        enabled: !!customerId,
    });

    const totalPages = getTotalPages(favorites, itemsPerPageCount);
    const total = getTotal(favorites);

    const tableData = useMemo(
        () =>
            favorites?.data?.map(favorite => ({
                id: favorite.id,
                product: {
                    id: favorite.product?.id,
                    name: favorite.product?.name,
                },
            })) || [],
        [favorites]
    );

    const initialState = {
        action: ActionType.Close,
        open: false,
    };

    const checkFavoriteProduct = useSearchFavorites();

    const checkFavoriteExists = useCallback(
        async (value: number) => {
            const { data } = await checkFavoriteProduct.mutateAsync({
                filter: { customer_id: customerId, product_id: value },
            });

            return data.length === 0;
        },
        [checkFavoriteProduct, customerId]
    );

    const {
        mutateAsync: createMassFavorite,
        error: isCreateError,
        isSuccess: isCreateSuccess,
    } = useCreateMassFavorite();
    const {
        mutateAsync: deleteMassFavorite,
        error: isDeleteError,
        isSuccess: isDeleteSuccess,
    } = useDeleteMassFavorite();
    const {
        mutateAsync: deleteAllFavorites,
        error: isDeleteMassError,
        isSuccess: isDeleteMassSuccess,
    } = useDeleteAllFavorites();

    useSuccess(isCreateSuccess ? ModalMessages.SUCCESS_CREATE : '');
    useSuccess(isDeleteSuccess || isDeleteMassSuccess ? ModalMessages.SUCCESS_DELETE : '');
    useError(isCreateError);
    useError(isDeleteError);
    useError(isDeleteMassError);

    const [favoritesPopupState, favoritesPopupDispatch] = usePopupState<State>(initialState);
    const { popupState, popupDispatch, ActionPopup, ActionEnum } = useActionPopup();

    const close = () => favoritesPopupDispatch({ type: ActionType.Close });

    const onRowsDelete = (row?: Row<any>) => {
        popupDispatch({
            type: ActionType.Delete,
            payload: {
                title: row
                    ? `Вы уверены, что хотите удалить избранный товар?`
                    : 'Вы уверены, что хотите удалить все избранные товары клиента?',
                popupAction: ActionEnum.DELETE,
                onAction: async () => {
                    if (customerId) {
                        if (row && row?.id) {
                            await deleteMassFavorite({
                                customer_id: customerId,
                                product_ids: [row.original.product.id],
                            });
                            return;
                        }

                        await deleteAllFavorites({
                            customer_id: customerId,
                        });
                    }
                },
                ...(row && { children: <p css={{ marginBottom: scale(2) }}>id товара: {row?.original.product.id}</p> }),
            },
        });
    };

    const tooltipContent: TooltipItem[] = [
        {
            type: 'delete',
            text: 'Удаление избранного',
            action: row => {
                if (row) onRowsDelete(row[0]);
            },
        },
    ];

    const onSubmit = async (vals: FieldValues) => {
        if (customerId) {
            await createMassFavorite({ product_ids: [+vals.productId], customer_id: customerId });
            close();
        }
    };

    const columnHelper = createColumnHelper<Favorite>();

    const linkStyles = useLinkCSS();

    useRedirectToNotEmptyPage({ activePage, itemsPerPageCount, total, pageKey });

    const columns: TableColumnDefAny[] = [
        columnHelper.accessor('id', {
            header: 'id',
            cell: props => props.getValue(),
        }),
        columnHelper.accessor('product.name', {
            header: 'Товар',
            enableSorting: false,
            /* eslint-disable-next-line react/no-unstable-nested-components */
            cell: ({ row, getValue }) => (
                <p css={{ marginBottom: scale(1) }}>
                    <Link css={linkStyles} passHref href={`/products/catalog/${row?.original.product.id}`}>
                        {getValue()}
                    </Link>
                </p>
            ),
        }),
        ...(access.ID.edit
            ? [
                  getSettingsColumn({
                      tooltipContent,
                  }),
              ]
            : []),
    ];

    const table = useTable(
        {
            data: tableData,
            columns,
            meta: {
                tableKey: pageKey,
            },
        },
        [sortingPlugin]
    );

    const apiClient = useAuthApiClient();
    const asyncLoadProducts = useCallback(
        async (value: string) => {
            const filter = {
                name: value || '',
            };

            const res: CommonResponse<Product[]> = await apiClient.post('catalog/products/drafts:search', {
                data: { filter },
            });

            return {
                options: res.data.map(p => ({
                    label: p.name,
                    value: p.id,
                })),
                hasMore: false,
            };
        },
        [apiClient]
    );

    const asyncOptionsByValuesFn = useCallback(async (vals: Product[]) => {
        if (!vals?.[0]) return [];

        return [
            {
                label: vals[0].name,
                value: vals[0].id,
            },
        ];
    }, []);

    return (
        <Block>
            {access.ID.edit && (
                <Block.Header>
                    <div css={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
                        <div>
                            <p css={typography('bodySm')}>
                                <b>Всего избранных:</b> {`${total} ${declOfNum(total, ['товар', 'товара', 'товаров'])}`}
                            </p>
                        </div>
                        <Layout type="flex">
                            <Layout.Item>
                                <Button
                                    onClick={() => favoritesPopupDispatch({ type: ActionType.Add })}
                                    Icon={PlusIcon}
                                >
                                    Создать избранный товар
                                </Button>
                            </Layout.Item>

                            <Layout.Item>
                                <Button
                                    onClick={() => {
                                        onRowsDelete();
                                    }}
                                >
                                    Удалить все избранные товары
                                </Button>
                            </Layout.Item>
                        </Layout>
                    </div>
                </Block.Header>
            )}
            <Block.Body>
                {tableData.length ? <Table instance={table} /> : <p>Товаров в избранном не найдено</p>}

                <TableFooter
                    pages={totalPages}
                    itemsPerPageCount={itemsPerPageCount}
                    setItemsPerPageCount={setItemsPerPageCount}
                    pageKey={pageKey}
                />
            </Block.Body>
            <Popup
                open={Boolean(favoritesPopupState?.open && favoritesPopupState.action !== ActionType.Delete)}
                onClose={close}
                size="sm"
            >
                <PopupHeader title="Создание избранного товара" />
                <PopupContent>
                    <Form
                        initialValues={{ productId: null }}
                        onSubmit={onSubmit}
                        validationSchema={Yup.object().shape({
                            productId: Yup.number()
                                .required(ErrorMessages.REQUIRED)
                                .test(
                                    'is-unique',
                                    'Этот товар уже в избранном',
                                    value => !value || checkFavoriteExists(value)
                                )
                                .nullable(),
                        })}
                    >
                        <Layout cols={4}>
                            <Layout.Item col={4}>
                                <FormFieldWrapper name="productId" label="Товар">
                                    <AutocompleteAsync
                                        asyncSearchFn={asyncLoadProducts}
                                        asyncOptionsByValuesFn={asyncOptionsByValuesFn}
                                    />
                                </FormFieldWrapper>
                            </Layout.Item>
                            <Layout.Item col={4} justify="end">
                                <Button theme="secondary" css={{ marginRight: scale(2) }} onClick={close}>
                                    Отменить
                                </Button>
                                <Button type="submit">Создать</Button>
                            </Layout.Item>
                        </Layout>
                    </Form>
                </PopupContent>
            </Popup>

            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </Block>
    );
};

export default Favorites;
