import { useAccess } from '@scripts/hooks';

export const useCustomersAccess = () =>
    useAccess({
        LIST: {
            view: 1202,
        },
        ID: {
            view: 1203,
            edit: 1204,
            delete: 1201,
        },
    });
