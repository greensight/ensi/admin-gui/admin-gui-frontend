import { useMemo } from 'react';

import { Customer, useCustomers, useCustomersMeta } from '@api/customers';

import ListBuilder from '@components/ListBuilder';
import { TooltipItem } from '@components/Table';

import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import { useCustomersAccess } from './useCustomersAccess';

const Customers = () => {
    const access = useCustomersAccess();
    const goToDetailPage = useGoToDetailPage({ extraConditions: access.ID.view });

    const tooltipContent: TooltipItem[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Редактировать клиента',
                action: goToDetailPage,
                isDisable: !access.ID.view,
            },
        ],
        [access.ID.view, goToDetailPage]
    );

    return (
        <ListBuilder<Customer>
            access={access}
            searchHook={useCustomers}
            metaHook={useCustomersMeta}
            tooltipItems={tooltipContent}
            title="Список клиентов"
        />
    );
};

export default Customers;
