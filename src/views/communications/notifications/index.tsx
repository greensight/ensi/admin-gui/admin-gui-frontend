import { useActionPopup } from '@ensi-platform/core-components';
import { useCallback, useMemo } from 'react';

import {
    NotificationSetting,
    useDeleteNotificationSetting,
    useNotificationSettingsMeta,
    useSearchNotificationSettings,
} from '@api/communications/notificationSettings';

import { useError, useSuccess } from '@context/modal';

import ListBuilder from '@components/ListBuilder';
import { ExtendedRow, TooltipItem } from '@components/Table';

import { ModalMessages } from '@scripts/constants';
import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import { useNotificationsAccess } from './useNotificationsAccess';

const NotificationList = () => {
    const { access } = useNotificationsAccess();
    const { popupState, popupDispatch, ActionPopup, ActionEnum, ActionType } = useActionPopup();

    const deleteNotification = useDeleteNotificationSetting();
    useSuccess(deleteNotification.isSuccess && ModalMessages.SUCCESS_DELETE);
    useError(deleteNotification.error);

    const goToDetailPage = useGoToDetailPage({ extraConditions: access.ID.view });

    const goToDeleteRow = useCallback(
        (originalRows: ExtendedRow['original'][] | undefined) => {
            if (originalRows) {
                popupDispatch({
                    type: ActionType.Delete,
                    payload: {
                        title: `Вы уверены, что хотите удалить настройку #${originalRows[0].original.id}?`,
                        popupAction: ActionEnum.DELETE,
                        onAction: async () => {
                            try {
                                await deleteNotification.mutateAsync({
                                    id: Number(originalRows[0].original.id)!,
                                });
                            } catch (err) {
                                console.error(err);
                            }
                        },
                    },
                });
            }
        },
        [ActionEnum.DELETE, ActionType.Delete, deleteNotification, popupDispatch]
    );

    const tooltipContent: TooltipItem[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Редактировать настройку',
                action: goToDetailPage,
                isDisable: !access.ID.view,
            },
            {
                type: 'delete',
                text: 'Удалить настройку',
                action: goToDeleteRow,
                isDisable: !access.ID.delete,
            },
        ],
        [access.ID.delete, access.ID.view, goToDeleteRow, goToDetailPage]
    );

    return (
        <ListBuilder<NotificationSetting>
            access={access}
            searchHook={useSearchNotificationSettings}
            metaHook={useNotificationSettingsMeta}
            isLoading={deleteNotification.isPending}
            tooltipItems={tooltipContent}
            title="Сервисные уведомления"
        >
            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </ListBuilder>
    );
};

export default NotificationList;
