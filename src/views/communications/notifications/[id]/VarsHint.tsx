import { useFormContext } from '@ensi-platform/core-components';
import { useMemo } from 'react';

import { useNotificationSettingVariables } from '@api/communications/notificationSettings';

import VariablesHint from '@components/VariablesHint';

export const VarsHint = () => {
    const { watch } = useFormContext<{ event: number }>();
    const event = watch('event');

    const { data: apiVars } = useNotificationSettingVariables();

    const items = useMemo(() => {
        if (!apiVars?.data || !event) return [];

        const itemsForEvent = apiVars?.data.find(e => e.event === event)?.variables || [];

        console.log('itemsForEvent=', itemsForEvent);
        return itemsForEvent;
    }, [apiVars?.data, event]);

    return <VariablesHint items={items} label="Доступны теги:" />;
};
