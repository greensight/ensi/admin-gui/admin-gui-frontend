import {
    CopyButton,
    DescriptionList,
    DescriptionListItem,
    FormField,
    FormFieldWrapper,
    Select,
    SelectWithTags,
    getDefaultDates,
    useActionPopup,
    useFormContext,
} from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { useMemo, useRef } from 'react';
import * as Yup from 'yup';

import {
    useCreateNotificationSetting,
    useDeleteNotificationSetting,
    useNotificationSetting,
    useNotificationSettingChannels,
    useNotificationSettingEvents,
    usePatchNotificationSetting,
} from '@api/communications/notificationSettings';

import { useError, useSuccess } from '@context/modal';

import Legend from '@controls/Legend';
import LoadingSkeleton from '@controls/LoadingSkeleton';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';
import TextEditor from '@components/TextEditor';

import { CREATE_PARAM, ErrorMessages, ModalMessages } from '@scripts/constants';
import { RedirectMethods } from '@scripts/enums';
import { Layout, scale } from '@scripts/gds';
import { isPageNotFound, toSelectItems } from '@scripts/helpers';
import { useDeferredLoading } from '@scripts/hooks/useDeferredLoading';

import { useNotificationsAccess } from '../useNotificationsAccess';
import { VarsHint } from './VarsHint';

const LISTING_URL = '/communications/notifications';

const FormInner = ({ isLoading }: { isLoading: boolean }) => {
    const { watch } = useFormContext();
    const event = watch('event');

    const { data: apiChannels } = useNotificationSettingChannels();
    const { data: apiEvents } = useNotificationSettingEvents();

    const channels = useMemo(() => toSelectItems(apiChannels?.data), [apiChannels?.data]);

    const events = useMemo(
        () =>
            toSelectItems(apiEvents?.data).map(e => ({
                ...e,
                label: e.label.replace('&#39;', '«').replace('&#39;', '»'),
            })),
        [apiEvents?.data]
    );

    const deferredIsLoading = useDeferredLoading(isLoading);

    if (deferredIsLoading) {
        return (
            <LoadingSkeleton
                count={5}
                css={{
                    marginBottom: scale(2),
                }}
                height={scale(6)}
            />
        );
    }

    return (
        <Layout cols={8} gap={scale(2)} css={{ marginBottom: scale(2) }}>
            <Layout.Item col={8}>
                <FormField name="name" label="Название уведомления*" />
            </Layout.Item>
            {event && (
                <Layout.Item col={8}>
                    <FormField name="theme" label="Тема*" rightAddons={<VarsHint />} />
                </Layout.Item>
            )}
            <Layout.Item col={8}>
                <FormFieldWrapper name="event" label="Идентификатор события*">
                    <Select options={events} hideClearButton />
                </FormFieldWrapper>
            </Layout.Item>
            <Layout.Item col={8}>
                <FormFieldWrapper name="channels" label="Каналы*">
                    <SelectWithTags options={channels} hideClearButton />
                </FormFieldWrapper>
            </Layout.Item>
            {event && (
                <Layout.Item col={8}>
                    <div css={{ display: 'flex', alignItems: 'baseline', gap: scale(1) }}>
                        <Legend label="Текст уведомления" />
                        <VarsHint />
                    </div>
                    <FormFieldWrapper name="text" label="" css={{ minHeight: scale(20) }}>
                        <TextEditor />
                    </FormFieldWrapper>
                </Layout.Item>
            )}
        </Layout>
    );
};

const Details = () => {
    const {
        query: { id },
        push,
    } = useRouter();

    const parsedId = +`${id}`;
    const isCreation = id === CREATE_PARAM;

    const update = usePatchNotificationSetting();
    const create = useCreateNotificationSetting();
    const remove = useDeleteNotificationSetting();

    const { data: apiData, error, isFetching: isLoading } = useNotificationSetting({ id: parsedId });
    useError(error);

    const notificationSetting = apiData?.data;

    const { popupState, popupDispatch, ActionPopup, ActionEnum, ActionType } = useActionPopup();

    useSuccess(update.isSuccess && ModalMessages.SUCCESS_UPDATE);
    useSuccess(remove.isSuccess && ModalMessages.SUCCESS_DELETE);
    useSuccess(create.isSuccess && ModalMessages.SUCCESS_CREATE);

    useError(update.error);
    useError(remove.error);
    useError(create.error);

    const initialValues = useMemo(
        () => ({
            name: notificationSetting?.name || '',
            theme: notificationSetting?.theme || '',
            event: (notificationSetting?.event || '') as number,
            channels: notificationSetting?.channels || [],
            text: notificationSetting?.text || '',
        }),
        [notificationSetting]
    );

    const pageTitle = isCreation ? 'Создать шаблон уведомления' : `Редактировать шаблон уведомления ${parsedId}`;

    const shouldReturnBack = useRef(false);

    const { access, isIDForbidden } = useNotificationsAccess();

    const canSubmit = isCreation ? access.ID.create : access.ID.edit;

    const isNotFound = isPageNotFound({ id: parsedId, error, isCreation });

    return (
        <PageWrapper
            title={pageTitle}
            isLoading={isLoading || update.isPending || create.isPending || remove.isPending}
            isNotFound={isNotFound}
            isForbidden={isIDForbidden}
        >
            <FormWrapper
                disabled={!canSubmit}
                onSubmit={async (values, { reset }) => {
                    if (isCreation) {
                        const result = await create.mutateAsync(values);

                        if (shouldReturnBack.current) {
                            return {
                                method: RedirectMethods.push,
                                redirectPath: LISTING_URL,
                            };
                        }

                        return {
                            method: RedirectMethods.push,
                            redirectPath: `${LISTING_URL}/${result.data.id}`,
                        };
                    }
                    await update.mutateAsync({
                        id: parsedId,
                        ...values,
                    });

                    if (shouldReturnBack.current) {
                        reset(values);
                        return {
                            method: RedirectMethods.push,
                            redirectPath: LISTING_URL,
                        };
                    }
                    return {
                        method: RedirectMethods.replace,
                        redirectPath: `${LISTING_URL}/${parsedId}`,
                    };
                }}
                initialValues={initialValues}
                validationSchema={Yup.object().shape({
                    name: Yup.string().required(ErrorMessages.REQUIRED),
                    theme: Yup.string().required(ErrorMessages.REQUIRED),
                    text: Yup.string().required(ErrorMessages.REQUIRED),
                    event: Yup.number()
                        .transform(val => (Number.isNaN(val) ? undefined : val))
                        .required(ErrorMessages.REQUIRED),
                    channels: Yup.array().of(Yup.number()).min(1, ErrorMessages.MIN_ITEMS(1)),
                })}
                enableReinitialize
            >
                {({ reset }) => (
                    <PageTemplate
                        h1={pageTitle}
                        controls={
                            <PageControls access={access}>
                                <PageControls.Delete
                                    onClick={() => {
                                        popupDispatch({
                                            type: ActionType.Delete,
                                            payload: {
                                                title: `Вы уверены, что хотите удалить тег?`,
                                                popupAction: ActionEnum.DELETE,
                                                onAction: async () => {
                                                    reset();
                                                    await remove.mutateAsync({
                                                        id: parsedId,
                                                    });

                                                    push(LISTING_URL);
                                                },
                                            },
                                        });
                                    }}
                                />
                                <PageControls.Close
                                    onClick={() =>
                                        push({
                                            pathname: LISTING_URL,
                                        })
                                    }
                                />
                                <PageControls.Apply
                                    onClick={() => {
                                        shouldReturnBack.current = false;
                                    }}
                                />
                                <PageControls.Save
                                    onClick={() => {
                                        shouldReturnBack.current = true;
                                    }}
                                />
                            </PageControls>
                        }
                        backlink={{ text: 'К списку шаблонов', href: '/communications/notifications' }}
                        aside={
                            <DescriptionList css={{ marginBottom: scale(5) }}>
                                <DescriptionListItem name="ID:" value={<CopyButton>{`${id || ''}`}</CopyButton>} />
                                {getDefaultDates({ ...notificationSetting }).map(item => (
                                    <DescriptionListItem {...item} key={item.name} />
                                ))}
                            </DescriptionList>
                        }
                    >
                        <FormInner isLoading={isLoading} />
                    </PageTemplate>
                )}
            </FormWrapper>
            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </PageWrapper>
    );
};

export default Details;
