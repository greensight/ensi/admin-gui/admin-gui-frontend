import { format, parseISO } from 'date-fns';

import { OrderDelivery, OrderStatus } from '@api/orders';

import { colors } from '@scripts/gds';

export const getStatusColor = (status: OrderStatus) => {
    switch (status) {
        case OrderStatus.WAITING:
            return colors.warning;
        case OrderStatus.NEW:
            return colors.primary;
        case OrderStatus.CONFIRMED:
            return colors.success;
        case OrderStatus.FINISHED:
            return colors.grey900;
        case OrderStatus.CANCELED:
            return colors.danger;
        default:
            return colors.primary;
    }
};

export const getOrderDeliveryDateTime = (deliveries?: OrderDelivery[]) => {
    const { id, date, timeslot, status } = deliveries?.[0] || {};

    if (id && date && timeslot)
        return {
            id,
            date: format(parseISO(date), 'dd.MM.yyyy'),
            timeslotId: timeslot.id,
            timeslot: !timeslot?.from || !timeslot?.to ? '' : `${timeslot?.from} - ${timeslot?.to}`,
            status,
        };

    return null;
};
