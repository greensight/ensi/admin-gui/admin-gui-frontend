import {
    Block,
    CalendarInput,
    FormField,
    FormFieldWrapper,
    Input,
    Textarea,
    useFormContext,
} from '@ensi-platform/core-components';
import { Fragment, useMemo } from 'react';

import { useDeliveryMethods } from '@api/logistic';
import { Order } from '@api/orders';

import { useError } from '@context/modal';

import Select from '@controls/Select';

import Mask from '@components/controls/Mask';

import { DeliveryMethods } from '@scripts/enums';
import { Layout, scale } from '@scripts/gds';
import { toFutureSelectItems } from '@scripts/helpers';
import { timeslotMask } from '@scripts/mask';

export const Delivery = ({
    order,
    canEditCommentariesInDeliveryTab,
    canEditDeliveryMethod,
    canEditOrderData,
}: {
    order: Order | undefined;
    canEditCommentariesInDeliveryTab: boolean;
    canEditDeliveryMethod: boolean;
    canEditOrderData: boolean;
}) => {
    const { data, error } = useDeliveryMethods();
    useError(error);
    const items = useMemo(() => toFutureSelectItems(data?.data), [data?.data]);
    const { watch } = useFormContext<{
        delivery_method: DeliveryMethods;
        delivery_date: { date: number; timeslot: string };
    }>();
    const delivery_method = watch('delivery_method');
    const delivery_date = watch('delivery_date');

    return (
        <Block css={{ padding: scale(3) }}>
            <Layout cols={4}>
                <Layout.Item col={2}>
                    <FormFieldWrapper label="Способ доставки" name="delivery_method">
                        <Select options={items} disabled={!canEditDeliveryMethod} hideClearButton />
                    </FormFieldWrapper>
                </Layout.Item>
                {delivery_method === DeliveryMethods.DELIVERY && (
                    <>
                        <Layout.Item col={4}>
                            <FormField
                                label="Адрес"
                                name="delivery_address.address_string"
                                disabled={!(canEditOrderData || canEditDeliveryMethod)}
                            />
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                            <FormField
                                label="Код страны"
                                name="delivery_address.country_code"
                                disabled={!(canEditOrderData || canEditDeliveryMethod)}
                            />
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                            <FormField
                                label="Почтовый индекс"
                                name="delivery_address.post_index"
                                disabled={!(canEditOrderData || canEditDeliveryMethod)}
                            />
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                            <FormField
                                label="Код региона"
                                name="delivery_address.region"
                                disabled={!(canEditOrderData || canEditDeliveryMethod)}
                            />
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                            <FormField
                                label="Идентификатор региона"
                                name="delivery_address.region_guid"
                                disabled={!(canEditOrderData || canEditDeliveryMethod)}
                            />
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                            <FormField
                                label="Подъезд"
                                name="delivery_address.porch"
                                disabled={!(canEditOrderData || canEditDeliveryMethod)}
                            />
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                            <FormField
                                label="Широта"
                                name="delivery_address.geo_lat"
                                disabled={!(canEditOrderData || canEditDeliveryMethod)}
                            />
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                            <FormField
                                label="Долгота"
                                name="delivery_address.geo_lon"
                                disabled={!(canEditOrderData || canEditDeliveryMethod)}
                            />
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                            <FormFieldWrapper
                                label="Этаж"
                                name="delivery_address.floor"
                                type="number"
                                disabled={!(canEditOrderData || canEditDeliveryMethod)}
                            >
                                <Input />
                            </FormFieldWrapper>
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                            <FormFieldWrapper
                                label="Кв/Офис"
                                name="delivery_address.flat"
                                disabled={!(canEditOrderData || canEditDeliveryMethod)}
                            >
                                <Input />
                            </FormFieldWrapper>
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, xs: 2 }}>
                            <FormFieldWrapper
                                label="Домофон"
                                name="delivery_address.intercom"
                                disabled={!(canEditOrderData || canEditDeliveryMethod)}
                            >
                                <Input />
                            </FormFieldWrapper>
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, xs: 2 }} />
                        <Layout.Item col={{ xxxl: 1, xs: 2 }} />

                        {delivery_date && (
                            <>
                                <Layout.Item col={4}>
                                    <p>Дата и время доставки для №{order?.deliveries?.[0]?.number}</p>
                                </Layout.Item>

                                <Layout.Item col={2} css={{ display: 'flex' }}>
                                    <FormFieldWrapper
                                        css={{ marginRight: scale(3) }}
                                        name="delivery_date.date"
                                        disabled={!(canEditOrderData || canEditDeliveryMethod)}
                                    >
                                        <CalendarInput label="Дата доставки" platform="desktop" view="date" picker />
                                    </FormFieldWrapper>

                                    <FormFieldWrapper
                                        label="Время доставки"
                                        name="delivery_date.timeslot"
                                        disabled={!(canEditOrderData || canEditDeliveryMethod)}
                                    >
                                        <Mask {...timeslotMask} />
                                    </FormFieldWrapper>
                                </Layout.Item>
                            </>
                        )}
                    </>
                )}

                <Layout.Item col={4}>
                    <FormFieldWrapper label="Комментарий к доставке" name="delivery_comment">
                        <Textarea minRows={5} disabled={!canEditCommentariesInDeliveryTab} block />
                    </FormFieldWrapper>
                </Layout.Item>
            </Layout>
        </Block>
    );
};
