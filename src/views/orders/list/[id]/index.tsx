import { Form, Tabs } from '@ensi-platform/core-components';
import { useRouter } from 'next/router';
import { Dispatch, SetStateAction, useCallback, useMemo, useRef, useState } from 'react';
import deepEqual from 'react-fast-compare';
import * as Yup from 'yup';

import {
    Order,
    useDeliveryChange,
    useOrderChange,
    useOrderDeliveryChange,
    useOrderDetail,
    useOrdersAddFile,
    useOrdersDeleteFiles,
} from '@api/orders';

import { useError, useSuccess } from '@context/modal';

import { getOrderDeliveryDateTime } from '@views/orders/list/[id]/helpers';

import { FileType } from '@controls/Dropzone/DropzoneFile';
import Tooltip, { ContentBtn, hideOnEsc } from '@controls/Tooltip';

import PageControls from '@components/PageControls';
import PageLeaveGuard from '@components/PageLeaveGuard';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';
import { TooltipItem } from '@components/Table';

import { ErrorMessages, ModalMessages } from '@scripts/constants';
import { DeliveryMethods } from '@scripts/enums';
import { Button, Layout, scale } from '@scripts/gds';
import { cleanPhoneValue, isPageNotFound, prepareIsoDate } from '@scripts/helpers';
import { preFormatPhone } from '@scripts/mask';
import { MutationExtractor, extractMutations } from '@scripts/mutationExtractor';

import KebabIcon from '@icons/small/kebab.svg';

import AddOffersPopup from '../components/AddOffersPopup';
import ChangeStatusPopup from '../components/ChangeStatusPopup';
import ChooseProductsPopup from '../components/SelectProductsPopup';
import useOrdersAccess, { useOrdersAllowedFields } from '../useOrdersAccess';
import { Comments } from './Comments';
import { Customer } from './Customer';
import { Delivery } from './Delivery';
import { Files } from './Files';
import { Main } from './Main';
import { Products } from './Products';
import { Aside } from './components/Aside';

const LISTING_URL = '/orders/list';

const FormInner = ({
    order,
    refetch,
    setSelectProducts,
}: {
    order?: Order;
    refetch: () => void;

    setSelectProducts: Dispatch<SetStateAction<boolean>>;
}) => {
    const {
        canViewMainTab,
        canViewClientTab,
        canViewProductsTab,
        canViewDeliveryTab,
        canViewCommentsTab,
        canViewAttachmentsTab,
        canEditOrderData,
        canEditDeliveryMethod,
        canEditCommentariesInDeliveryTab,
        canLoadAttachmentsData,
        canDeleteAttachmentsData,
        canEditShipmentStatus,
    } = useOrdersAccess();

    return (
        <div css={{ display: 'flex', gap: scale(2), flexWrap: 'wrap', width: '100%' }}>
            <div css={{ flexGrow: 1, flexShrink: 1, maxWidth: '100%' }}>
                <Tabs css={{ width: '100%' }}>
                    {canViewMainTab && (
                        <Tabs.Tab title="Главное" id="main">
                            <Main order={order} />
                        </Tabs.Tab>
                    )}

                    {canViewClientTab && (
                        <Tabs.Tab title="Клиент" id="client">
                            <Customer isLoading={!order} />
                        </Tabs.Tab>
                    )}

                    {canViewProductsTab && (
                        <Tabs.Tab title="Товары" id="products">
                            <Products
                                order={order}
                                canEditShipmentStatus={canEditShipmentStatus}
                                refetch={refetch}
                                setSelectProducts={setSelectProducts}
                            />
                        </Tabs.Tab>
                    )}

                    {canViewDeliveryTab && (
                        <Tabs.Tab title="Доставка" id="delivery">
                            <Delivery
                                order={order}
                                canEditCommentariesInDeliveryTab={canEditOrderData || canEditCommentariesInDeliveryTab}
                                canEditDeliveryMethod={canEditOrderData || canEditDeliveryMethod}
                                canEditOrderData={canEditOrderData}
                            />
                        </Tabs.Tab>
                    )}

                    {canViewCommentsTab && (
                        <Tabs.Tab title="Комментарии" id="comments">
                            <Comments />
                        </Tabs.Tab>
                    )}

                    {canViewAttachmentsTab && (
                        <Tabs.Tab title="Вложения" id="attachments">
                            <Files
                                canLoadAttachmentsData={canEditOrderData || canLoadAttachmentsData}
                                canDeleteAttachmentsData={canEditOrderData || canDeleteAttachmentsData}
                            />
                        </Tabs.Tab>
                    )}
                </Tabs>
            </div>
        </div>
    );
};

const OrderPage = () => {
    const { query } = useRouter();
    const orderId = (query && query.id && +query.id) || 0;
    const [tippyInstance, setTippyInstance] = useState<any>();
    const { push, pathname } = useRouter();

    const {
        canEditOrderData,
        canViewDetailPage,
        canEditStatusOrder,
        canCreateRequestForRefund,
        canViewAnyPartOfDetailPage,
        isIDForbidden,
    } = useOrdersAccess();

    const [isStatusOpen, setIsStatusOpen] = useState(false);

    const isReturnBack = useRef(false);

    const changeOrder = useOrderChange();
    const changeOrderDelivery = useOrderDeliveryChange();
    const changeDeliveryDate = useDeliveryChange();

    const {
        data,
        isInitialLoading: isIdle,
        isFetching: isLoading,
        error,
        refetch,
        isRefetching,
    } = useOrderDetail(orderId, [
        'deliveries',
        'deliveries.shipments',
        'deliveries.shipments.orderItems',
        'customer',
        'customer.user',
        'responsible',
        'files',
        'orderItems.product',
        'items',
    ]);

    const order = data?.data;

    const addFile = useOrdersAddFile();
    const deleteFiles = useOrdersDeleteFiles();
    useError(addFile.error);
    useError(deleteFiles.error);
    useSuccess(addFile.status === 'success' ? ModalMessages.SUCCESS_UPDATE : '');
    useSuccess(deleteFiles.status === 'success' ? ModalMessages.SUCCESS_DELETE : '');

    useError(error);

    useError(changeOrder.error);
    useError(changeOrderDelivery.error);
    useError(changeDeliveryDate.error);
    useSuccess(changeOrder.status === 'success' ? ModalMessages.SUCCESS_UPDATE : '');
    useSuccess(changeOrderDelivery.status === 'success' ? ModalMessages.SUCCESS_UPDATE : '');

    const fieldsOrDefault = useCallback(
        <TObject extends Record<string, any>, TKeys extends keyof TObject>(obj: TObject, keys: TKeys[]) => {
            const result = {} as Record<string, any>;

            keys.forEach(key => {
                result[key as string] = (obj || {})[key] || '';
            });

            return result as {
                [K in TKeys]: Exclude<TObject, undefined>[K];
            };
        },
        []
    );

    const { extractAllowedValues } = useOrdersAllowedFields();

    const initialValues = useMemo(
        () => ({
            ...fieldsOrDefault(order!, [
                'receiver_name',
                'receiver_email',
                'delivery_method',
                'delivery_comment',
                'client_comment',
                'problem_comment',
            ]),
            delivery_address: fieldsOrDefault(order?.delivery_address!, [
                'address_string',
                'country_code',
                'post_index',
                'porch',
                'floor',
                'flat',
                'intercom',
                'region',
                'region_guid',
                'geo_lat',
                'geo_lon',
            ]),
            responsible_id: { label: order?.responsible?.full_name || '', value: order?.responsible?.id || null },
            receiver_phone: order?.receiver_phone ? preFormatPhone(order.receiver_phone) : '',
            delivery_date: getOrderDeliveryDateTime(order?.deliveries),
            delivery_point_id: { label: order?.delivery_point_id, value: order?.delivery_point_id },
            is_problem: order?.is_problem || false,
            files: (order?.files?.map(f => ({ name: f.original_name, id: f.id, file: f.file })) || []) as FileType[],
            files_to_delete: [],
        }),
        [fieldsOrDefault, order]
    );

    const title = canViewAnyPartOfDetailPage ? `Заказ №${order?.number || ''}` : '';

    const [isAddingOffers, setAddingOffers] = useState(false);
    const [isSelectProducts, setSelectProducts] = useState(false);
    const [selectedProducts, setSelectedProducts] = useState<number[]>([]);

    const mutationExtractors = useMemo<MutationExtractor<typeof initialValues>[]>(
        () => [
            {
                keys: [
                    'receiver_name',
                    'receiver_email',
                    'client_comment',
                    'is_problem',
                    'problem_comment',
                    'client_comment',
                ],
                mapper: val => val,
            },
            {
                keys: ['receiver_phone'],
                mapper: cleanPhoneValue,
            },
            {
                keys: ['responsible_id'],
                mapper: e => e?.value,
            },
        ],
        []
    );

    const tooltipContent: TooltipItem[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Изменить статус заказа',
                action: () => {
                    setIsStatusOpen(true);
                    tippyInstance?.hide();
                },
                isDisable: !canEditStatusOrder,
            },
            {
                type: 'edit',
                text: 'Создать заявку на возврат',
                action: () => {
                    push({
                        pathname: `${pathname}/create-refund`,
                        query: { id: orderId },
                    });
                },
                isDisable: !canCreateRequestForRefund,
            },
        ],
        [canCreateRequestForRefund, canEditStatusOrder, orderId, pathname, push, tippyInstance]
    );

    const validationSchema = useMemo(
        () =>
            Yup.object().shape({
                delivery_method: Yup.number()
                    .transform(val => (Number.isNaN(val) ? undefined : val))
                    .required(ErrorMessages.REQUIRED),
                receiver_name: Yup.string().min(3, ErrorMessages.MIN_SYMBOLS(3)).required(ErrorMessages.REQUIRED),
                receiver_email: Yup.string().email(ErrorMessages.EMAIL).required(ErrorMessages.REQUIRED),
                receiver_phone: Yup.string().min(3, ErrorMessages.MIN_SYMBOLS(3)),
                problem_comment: Yup.string().when('is_problem', ([is_problem], schema) =>
                    is_problem ? schema.required(ErrorMessages.REQUIRED) : schema
                ),
                delivery_address: Yup.object().when('delivery_method', {
                    is: (value: DeliveryMethods) => value === DeliveryMethods.DELIVERY,
                    then: schema =>
                        schema.shape({
                            address_string: Yup.string().required(ErrorMessages.REQUIRED),
                            country_code: Yup.string().required(ErrorMessages.REQUIRED),
                            post_index: Yup.string().required(ErrorMessages.REQUIRED),
                            region: Yup.string().required(ErrorMessages.REQUIRED),
                            region_guid: Yup.string().required(ErrorMessages.REQUIRED),
                            geo_lat: Yup.string().required(ErrorMessages.REQUIRED),
                            geo_lon: Yup.string().required(ErrorMessages.REQUIRED),
                        }),
                    otherwise: schema => schema,
                }),
            }),
        []
    );

    const onSubmit = useCallback(
        async (vals: typeof initialValues) => {
            if (!order) return;

            const promises: Promise<any>[] = [];

            const { data: changeOrderData, hasDefinedValues } = extractMutations(
                initialValues,
                vals,
                mutationExtractors
            );

            if (hasDefinedValues) {
                promises.push(
                    changeOrder.mutateAsync(
                        extractAllowedValues({
                            id: orderId,
                            ...changeOrderData,
                        })
                    )
                );
            }

            const readonlyData = {
                id: orderId,
                delivery_service: order?.delivery_service,
                delivery_cost: order?.delivery_cost,
                delivery_price: order?.delivery_price,
                delivery_tariff_id: order?.delivery_tariff_id,
            };

            const newDeliveryData = {
                delivery_method: +vals.delivery_method,
                delivery_point_id: vals.delivery_point_id.value || null,
                delivery_address: vals.delivery_address,
                delivery_comment: vals.delivery_comment,
            };

            const initialDeliveryData = {
                delivery_method: +initialValues.delivery_method,
                delivery_point_id: initialValues.delivery_point_id.value || null,
                delivery_address: initialValues.delivery_address,
                delivery_comment: initialValues.delivery_comment,
            };

            // если данные поменялись
            if (!deepEqual(initialDeliveryData, newDeliveryData)) {
                promises.push(
                    // @ts-ignore
                    changeOrderDelivery.mutateAsync({
                        ...readonlyData,
                        ...newDeliveryData,
                    })
                );
            }

            if (vals.delivery_date && !deepEqual(initialValues.delivery_date, vals.delivery_date)) {
                const { timeslotId, timeslot, date, ...rest } = vals.delivery_date;
                const [from, to] = timeslot.split(' - ');

                promises.push(
                    changeDeliveryDate.mutateAsync({
                        date: prepareIsoDate(date),
                        timeslot: { id: timeslotId, from, to },
                        ...rest,
                    })
                );
            }

            if (vals.files_to_delete.length > 0) {
                promises.push(deleteFiles.mutateAsync({ id: orderId, file_ids: vals.files_to_delete }));
            }

            if (vals.files.length > 0) {
                promises.push(
                    ...vals.files.map(async file => {
                        // Если файл еще не был загружен
                        if (!file.id) {
                            await addFile.mutateAsync({ id: orderId, file });
                        }
                    })
                );
            }

            const results = await Promise.allSettled(promises);
            results.forEach(e => {
                if (e.status === 'rejected') {
                    console.error(e.reason);
                }
            });
        },
        [
            addFile,
            changeOrder,
            changeOrderDelivery,
            deleteFiles,
            extractAllowedValues,
            initialValues,
            mutationExtractors,
            order,
            orderId,
        ]
    );

    const isNotFound = isPageNotFound({ id: orderId, error });

    return (
        <PageWrapper
            title={title}
            isLoading={isIdle || isLoading || isRefetching}
            isForbidden={isIDForbidden}
            isNotFound={isNotFound}
        >
            <Form
                css={{ position: 'relative' }}
                initialValues={initialValues}
                validationSchema={validationSchema}
                enableReinitialize
                onSubmit={onSubmit}
            >
                <PageLeaveGuard />
                <PageTemplate
                    h1={title}
                    backlink={{
                        text: 'К списку заказов',
                        href: LISTING_URL,
                    }}
                    aside={<Aside order={order} />}
                    controls={
                        <PageControls
                            access={{
                                ID: {
                                    view: canViewDetailPage,
                                    delete: false,
                                    edit: canEditOrderData,
                                    create: false,
                                },
                                LIST: {
                                    view: false,
                                },
                            }}
                        >
                            <Layout.Item>
                                <Tooltip
                                    content={
                                        <>
                                            {tooltipContent.map(t => (
                                                <ContentBtn
                                                    key={t.text}
                                                    type={t.type}
                                                    onClick={e => {
                                                        e.stopPropagation();
                                                        t.action();
                                                    }}
                                                    disabled={
                                                        typeof t?.isDisable === 'function'
                                                            ? t.isDisable()
                                                            : t?.isDisable
                                                    }
                                                >
                                                    {t.text}
                                                </ContentBtn>
                                            ))}
                                        </>
                                    }
                                    plugins={[hideOnEsc]}
                                    trigger="click"
                                    arrow
                                    theme="light"
                                    placement="bottom"
                                    minWidth={scale(36)}
                                    onCreate={inst => setTippyInstance(inst)}
                                    appendTo={() => document.body}
                                >
                                    <Button
                                        theme="outline"
                                        Icon={KebabIcon}
                                        css={{ marginLeft: scale(2), marginRight: scale(2) }}
                                        iconAfter
                                        disabled={
                                            !(canEditOrderData || canEditStatusOrder || canCreateRequestForRefund)
                                        }
                                    >
                                        Действия
                                    </Button>
                                </Tooltip>
                            </Layout.Item>
                            <PageControls.Close
                                onClick={() => {
                                    push(LISTING_URL);
                                }}
                            />
                            <PageControls.Apply
                                onClick={() => {
                                    isReturnBack.current = false;
                                }}
                            />
                            <PageControls.Save
                                onClick={() => {
                                    isReturnBack.current = true;
                                }}
                            />
                        </PageControls>
                    }
                    customChildren
                >
                    <FormInner order={order} refetch={refetch} setSelectProducts={setSelectProducts} />
                </PageTemplate>
            </Form>
            <AddOffersPopup
                isOpen={isAddingOffers}
                onClose={() => setAddingOffers(false)}
                orderId={orderId}
                selectedProducts={selectedProducts}
            />
            <ChooseProductsPopup
                isOpen={isSelectProducts}
                onRequestClose={(productIds: number[]) => {
                    setSelectProducts(false);
                    if (productIds.length) {
                        setSelectedProducts(productIds);
                        setAddingOffers(true);
                    }
                }}
            />
            <ChangeStatusPopup
                isOpen={isStatusOpen}
                setIsOpen={setIsStatusOpen}
                orderId={orderId}
                oldStatus={order?.status}
            />
        </PageWrapper>
    );
};

export default OrderPage;
