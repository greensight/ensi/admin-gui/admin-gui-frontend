/* eslint-disable react/no-unstable-nested-components */
import {
    Block,
    Form,
    FormField,
    FormFieldWrapper,
    FormMessage,
    Textarea,
    useFormContext,
} from '@ensi-platform/core-components';
import { createColumnHelper } from '@tanstack/react-table';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useEffect, useMemo } from 'react';
import * as Yup from 'yup';

import { useCurrentUser } from '@api/auth';
import {
    BasketItem,
    OrderSources,
    RefundStatuses,
    useCreateRefund,
    useOrderDetail,
    useRefundAddFile,
    useRefundReasons,
} from '@api/orders';

import { useError, useModalsContext, useSuccess } from '@context/modal';

import Dropzone from '@controls/Dropzone';
import Select from '@controls/Select';

import PageWrapper from '@components/PageWrapper';
import Table, { TableColumnDefAny, useSorting, useTable } from '@components/Table';
import { getSelectColumn } from '@components/Table/columns';
import { Cell } from '@components/Table/components';

import { ErrorMessages, ModalMessages } from '@scripts/constants';
import { Button, Layout, scale, useTheme } from '@scripts/gds';
import { isPageNotFound, toFutureSelectItems } from '@scripts/helpers';
import { useLinkCSS, usePrevious } from '@scripts/hooks';

import CheckIcon from '@icons/small/check.svg';

import { Aside } from '../components/Aside';

interface Product {
    isReturn: boolean;
    id: number;
    qty: number;
}

const ProductsTable = ({ products: initialProducts }: { products: BasketItem[] }) => {
    const linkStyles = useLinkCSS();
    const { colors } = useTheme();

    const tableData = useMemo(
        () =>
            initialProducts.map(item => ({
                id: item.id,
                photo: item.product?.main_image_url || item.product?.main_image_file,
                name: {
                    name: item.name,
                    code: item.product_data?.barcode,
                    link: item?.product?.id ? `/products/catalog/${item?.product?.id}` : null,
                },
                price: [item.price_per_one, item.cost_per_one],
                qty: item.qty,
                cost: item.price,
                refundQty: item.qty,
            })),
        [initialProducts]
    );

    const tableKey = 'create-refund';
    const columnHelper = createColumnHelper<Record<string, any>>();
    const columns: TableColumnDefAny[] = [
        getSelectColumn<Product>(undefined, tableKey),
        columnHelper.accessor('id', {
            header: 'ID',
            cell: props => props.getValue(),
        }),
        columnHelper.accessor('photo', {
            header: 'Фото',
            cell: ({ getValue }) => <Cell value={getValue()} type="photo" />,
            enableHiding: true,
        }),
        columnHelper.accessor('name', {
            header: 'Название и артикул',
            cell: ({ getValue }) => {
                const value = getValue();

                return (
                    <>
                        {value?.link ? (
                            <p css={{ marginBottom: scale(1) }}>
                                <Link legacyBehavior passHref href={value.link}>
                                    <a css={linkStyles}>{value.name}</a>
                                </Link>
                            </p>
                        ) : (
                            <p css={{ marginBottom: scale(1) }}>{value.name}</p>
                        )}
                        <p css={{ color: colors?.grey700 }}>{value.barcode}</p>
                    </>
                );
            },
            enableHiding: true,
        }),
        columnHelper.accessor('price', {
            header: 'Цена и цена без скидки,  ₽',
            cell: ({ getValue }) =>
                getValue().map((v: number, index: number) => (
                    <div key={Number(index)} css={index > 0 && { color: colors?.grey700 }}>
                        <Cell value={v} type="price" />
                    </div>
                )),
            enableHiding: true,
        }),
        columnHelper.accessor('quantity', {
            header: 'Количество',
            cell: props => props.getValue() || '-',
        }),
        columnHelper.accessor('cost', {
            header: 'Стоимость,  ₽',
            cell: ({ getValue }) => <Cell value={getValue()} type="price" />,
        }),
        columnHelper.accessor('refund_qty', {
            header: 'Количество к возврату',
            cell: ({ getValue, row }) => (
                <FormField type="number" min={1} max={getValue()} name={`products.${row.index}.qty`} />
            ),
        }),
    ];
    const [{ sorting }, sortingPlugin] = useSorting<typeof tableData>(tableKey, [], undefined, false);

    const table = useTable(
        {
            data: tableData,
            columns,
            meta: {
                tableKey,
            },
            state: {
                sorting,
            },
            getRowId: (row: (typeof tableData)[0]) => String(row.id),
        },
        [sortingPlugin]
    );

    const {
        setValue: setFieldValue,
        watch,
        formState: { errors },
    } = useFormContext<{ products: Product[] }>();

    const products = watch('products');

    const selectedRowIds = table.getSelectedRowModel().flatRows.map(r => r.id);
    const selectedQt = selectedRowIds.length;
    const prevSelectedQt = usePrevious(selectedQt);

    useEffect(() => {
        if (prevSelectedQt !== null && prevSelectedQt !== selectedQt) {
            const updatedProducts = [...products];
            updatedProducts.forEach(p => {
                p.isReturn = selectedRowIds.includes(String(p.id));
            });
            setFieldValue('products', updatedProducts, { shouldValidate: true });
        }
    }, [selectedRowIds, setFieldValue, products, prevSelectedQt, selectedQt]);

    return (
        <>
            {errors.products?.message && (
                <FormMessage message={errors.products.message} css={{ marginBottom: scale(1) }} type="error" />
            )}
            <Table instance={table} />
        </>
    );
};

const SaveButton = () => {
    const {
        formState: { isDirty },
    } = useFormContext<{
        products: Product[];
    }>();

    return (
        <Button type="submit" Icon={CheckIcon} iconAfter disabled={!isDirty}>
            Создать заявку
        </Button>
    );
};

export default function CreateRefundPage() {
    const { query } = useRouter();
    const orderId = (query && query.id && +query.id) || 0;

    const {
        data,
        isInitialLoading: isIdle,
        isFetching: isLoading,
        error,
    } = useOrderDetail(orderId, [
        'deliveries',
        'deliveries.shipments',
        'deliveries.shipments.orderItems',
        'customer',
        'customer.user',
        'responsible',
        'orderItems.product',
    ]);

    const createRefund = useCreateRefund();
    const addFile = useRefundAddFile();
    const { appendModal } = useModalsContext();

    const order = useMemo(() => data?.data, [data?.data]);

    const products = useMemo(
        () =>
            order?.deliveries?.reduce((acc, val) => {
                const items = val.shipments.reduce((acc2, val2) => [...acc, ...val2.order_items], [] as BasketItem[]);
                return [...acc, ...items];
            }, [] as BasketItem[]) || [],
        [order?.deliveries]
    );

    const { data: refundReasonsData, error: refundReasonsError } = useRefundReasons();
    useError(error);
    useError(addFile.error);
    useError(refundReasonsError);
    useError(createRefund.error);

    useSuccess(createRefund.status === 'success' ? ModalMessages.SUCCESS_SAVE : '');

    const initialProducts = useMemo(() => products.map(p => ({ isReturn: false, id: p.id, qty: p.qty })), [products]);

    const validateProductsQt = useCallback(
        (products: Product[]) =>
            products.some(p => {
                if (p.isReturn) {
                    const maxQt = initialProducts.find(i => i.id === p.id)?.qty || 0;
                    if (maxQt && (p.qty > maxQt || p.qty < 1)) {
                        return true;
                    }
                }
                return false;
            }),
        [initialProducts]
    );

    const { data: userData } = useCurrentUser();

    const isNotFound = isPageNotFound({ id: orderId, error });

    return (
        <PageWrapper
            title={`Создание заявки на возврат для заказа №${order?.number || ''}`}
            isLoading={isIdle || isLoading}
            isNotFound={isNotFound}
        >
            <Form
                initialValues={{
                    products: initialProducts,
                    reasons: [],
                    comment: '',
                    files: [],
                }}
                validationSchema={Yup.object().shape({
                    reasons: Yup.array().min(1, ErrorMessages.MIN_ITEMS(1)),
                    comment: Yup.string().required(ErrorMessages.REQUIRED),
                    products: Yup.array().test({
                        message: 'Выберите товары и заполните их количество подлежащее к возврату',
                        test: arr => !!arr?.some(p => p.isReturn),
                    }),
                })}
                enableReinitialize
                onSubmit={vals => {
                    if (validateProductsQt(vals.products as Product[])) {
                        appendModal({
                            message:
                                'Количество товара к возврату не может быть больше приобретенного количества или меньше 1',
                            theme: 'error',
                        });
                        return;
                    }
                    const productsToRefund = vals.products as Product[];

                    const order_items = productsToRefund.reduce(
                        (acc, product) => {
                            if (product.isReturn) {
                                acc.push({ id: product.id, qty: product.qty });
                            }
                            return acc;
                        },
                        [] as {
                            id: number;
                            qty: number;
                        }[]
                    );

                    createRefund
                        .mutateAsync({
                            order_id: orderId,
                            source: OrderSources.ADMIN,
                            user_comment: vals.comment,
                            refund_reason_ids: vals.reasons,
                            order_items,
                            manager_id: userData?.data.id || null,
                            responsible_id: null,
                            rejection_comment: null,
                            status: RefundStatuses.NEW,
                        })
                        .then(refund => {
                            const { id } = refund.data;
                            vals.files.forEach((file: File) => {
                                addFile.mutate({ id, file });
                            });
                        });
                }}
                css={{ position: 'relative' }}
            >
                <>
                    <div css={{ display: 'flex', position: 'absolute', top: '-40px', right: 0 }}>
                        <SaveButton />
                    </div>

                    <div css={{ display: 'flex', gap: scale(2) }}>
                        <div css={{ flexGrow: 1, flexShrink: 1 }}>
                            <Block css={{ padding: scale(3) }}>
                                <Layout cols={2}>
                                    <Layout.Item col={2}>
                                        <FormFieldWrapper name="products" label="Товары к возврату*">
                                            <ProductsTable products={products} />
                                        </FormFieldWrapper>
                                    </Layout.Item>

                                    <Layout.Item col={2}>
                                        <FormFieldWrapper name="reasons" label="Причина возврата*">
                                            <Select options={toFutureSelectItems(refundReasonsData?.data)} multiple />
                                        </FormFieldWrapper>
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <FormFieldWrapper name="comment" label="Комментарий*">
                                            <Textarea minRows={3} />
                                        </FormFieldWrapper>
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <FormFieldWrapper name="files" label="Файл">
                                            <Dropzone />
                                        </FormFieldWrapper>
                                    </Layout.Item>
                                </Layout>
                            </Block>
                        </div>
                        <Aside order={order} />
                    </div>
                </>
            </Form>
        </PageWrapper>
    );
}
