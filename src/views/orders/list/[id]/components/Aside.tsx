import { CopyButton, DescriptionList, DescriptionListItem, getDefaultDates } from '@ensi-platform/core-components';
import { FC } from 'react';

import { Order, OrderStatus, useOrderStatuses } from '@api/orders';

import Circle from '@components/Circle';

import { scale } from '@scripts/gds';
import { getOptionName } from '@scripts/helpers';

import { getStatusColor } from '../helpers';

export const Aside: FC<{ order: Order | undefined }> = ({ order }) => {
    const { data: statuses } = useOrderStatuses();
    return (
        <DescriptionList>
            <DescriptionListItem
                name="Статус"
                value={
                    <div>
                        <Circle
                            css={{
                                marginRight: scale(1, true),
                                marginLeft: scale(1),
                                background: getStatusColor(order?.status as OrderStatus),
                            }}
                        />
                        {getOptionName(statuses?.data, order?.status)}
                    </div>
                }
            />
            <DescriptionListItem
                name="ID:"
                value={<CopyButton linkStyle={order?.is_changed ? 'red' : undefined}>{`${order?.id}`}</CopyButton>}
            />
            <DescriptionListItem name="Номер заказа:" value={<CopyButton>{`${order?.number}`}</CopyButton>} />
            {getDefaultDates({ ...order }).map(item => (
                <DescriptionListItem {...item} key={item.name} />
            ))}
        </DescriptionList>
    );
};
