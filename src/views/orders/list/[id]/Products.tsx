/* eslint-disable react/no-unstable-nested-components */
import { CSSObject } from '@emotion/react';
import { Block, useActionPopup } from '@ensi-platform/core-components';
import { createColumnHelper } from '@tanstack/react-table';
import Link from 'next/link';
import { Dispatch, SetStateAction, useEffect, useMemo, useRef, useState } from 'react';

import {
    Order,
    OrderShipment,
    useChangeOrderItemQty,
    useDeleteOrderItems,
    useDeliveryStatuses,
    useShipmentStatuses,
} from '@api/orders';
import { useShipmentChange } from '@api/orders/shipments';
import { StorePickupTime } from '@api/units';

import { useError, useSuccess } from '@context/modal';

import Counter from '@controls/Counter';
import LoadingSkeleton from '@controls/LoadingSkeleton';
import Tooltip, { ContentBtn } from '@controls/Tooltip';

import Table, { TableColumnDefAny, useSorting, useTable } from '@components/Table';
import { Cell, TableEmpty } from '@components/Table/components';

import { DateFormatters, ModalMessages } from '@scripts/constants';
import { ActionType } from '@scripts/enums';
import { Button, Layout, scale, typography, useTheme } from '@scripts/gds';
import { formatDate, formatPrice, fromKopecksToRouble, getOptionName } from '@scripts/helpers';
import { useDebounce, useLinkCSS } from '@scripts/hooks';

import PlusIcon from '@icons/small/plus.svg';

import useOrdersAccess from '../useOrdersAccess';

const itemCSS: CSSObject = {
    display: 'flex',
    ':not(:last-of-type)': {
        marginBottom: scale(1, true),
    },
};

const itemTitleCSS: CSSObject = {
    ...typography('bodySmBold'),
    marginRight: scale(1, true),
};

const QuantityCell = ({
    getValue,
    row: {
        original: { id, orderId },
    },
}: {
    getValue: () => number;
    row: {
        original: Record<string, any>;
    };
}) => {
    const value = getValue();
    const changeOrderItemQty = useChangeOrderItemQty();
    const changeOrderItemQtyAsync = useRef(changeOrderItemQty.mutateAsync);
    changeOrderItemQtyAsync.current = changeOrderItemQty.mutateAsync;

    const [targetValue, setTargetValue] = useState(value);
    const debouncedTarget = useDebounce(targetValue, 1000);

    const valueRef = useRef(value);
    valueRef.current = value;

    useEffect(() => {
        if (valueRef.current === debouncedTarget) return;

        changeOrderItemQtyAsync.current({
            id: orderId,
            order_items: [
                {
                    item_id: id,
                    qty: debouncedTarget,
                },
            ],
        });
    }, [debouncedTarget, id, orderId]);

    if (changeOrderItemQty.isPending) return <LoadingSkeleton height={scale(5)} width={scale(18)} />;

    return <Counter label="label" name="quantity" value={targetValue} onChange={e => setTargetValue(e)} />;
};

export const Shipment = ({
    orderId,
    shipment,
    canEditShipmentStatus,
    onChangeStatus,
    disabledOffersEdit,
    disabledProductRemove,
}: {
    orderId?: number;
    shipment: OrderShipment;
    canEditShipmentStatus: boolean;
    onChangeStatus: (status: number) => void;
    disabledOffersEdit: boolean;
    disabledProductRemove: boolean;
}) => {
    const [tippyInstance, setTippyInstance] = useState<any>();
    const { colors } = useTheme();
    const linkStyles = useLinkCSS();
    const data = useMemo(
        () =>
            shipment.order_items.map(item => ({
                id: item.id,
                orderId,
                photo: item.product?.main_image_url || item.product?.main_image_file,
                name: {
                    name: item.name,
                    code: item.product?.vendor_code,
                    link: item?.product?.id ? `/products/catalog/${item?.product?.id}` : null,
                },
                vendor_code: item.product?.vendor_code,
                price: [item.price_per_one, item.cost_per_one],
                quantity: item.qty,
                cost: item.price,
                is_added: item.is_added,
                is_deleted: item.is_deleted,
                offer_id: item.offer_id,
                old_qty: item.old_qty,
            })),
        [orderId, shipment.order_items]
    );

    const { data: apiShipmentStatuses } = useShipmentStatuses();
    const shipmentStatus = useMemo(
        () => getOptionName(apiShipmentStatuses?.data, shipment.status),
        [apiShipmentStatuses?.data, shipment.status]
    );

    const deleteOrderItems = useDeleteOrderItems();
    useSuccess(deleteOrderItems.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');
    useError(deleteOrderItems.error);

    const { popupState, popupDispatch, ActionPopup, ActionEnum } = useActionPopup();

    const columnHelper = createColumnHelper<Record<string, any>>();
    const columns: TableColumnDefAny[] = [
        columnHelper.accessor('id', {
            header: 'ID',
            cell: props => props.getValue(),
        }),
        columnHelper.accessor('photo', {
            header: 'Фото',
            cell: ({ getValue }) => <Cell value={getValue()} type="photo" />,
            enableHiding: true,
        }),
        columnHelper.accessor('name', {
            header: 'Наименование',
            cell: ({ getValue }) => {
                const value = getValue();

                return value?.link ? (
                    <p css={{ marginBottom: scale(1) }}>
                        <Link legacyBehavior passHref href={value.link}>
                            <a css={linkStyles}>{value.name}</a>
                        </Link>
                    </p>
                ) : (
                    <p css={{ marginBottom: scale(1) }}>{value.name}</p>
                );
            },
            enableHiding: true,
        }),
        columnHelper.accessor('vendor_code', {
            header: 'Артикул',
            cell: props => props.getValue(),
        }),
        columnHelper.accessor('price', {
            header: 'Цена и цена без скидки,  ₽',
            cell: ({ getValue }) =>
                getValue().map((v: number, index: number) => (
                    <div key={Number(index)} css={index > 0 && { color: colors?.grey700 }}>
                        <Cell value={v} type="price" />
                    </div>
                )),
            enableHiding: true,
        }),
        columnHelper.accessor('quantity', {
            header: 'Количество',
            cell: !disabledOffersEdit
                ? ({ ...props }) => <QuantityCell key={props.row.original.id} {...props} />
                : ({ getValue }) => <Cell value={getValue()} type="int" />,
        }),
        columnHelper.accessor('old_qty', {
            header: 'Старое количество',
            cell: props => props.getValue() || '-',
        }),
        columnHelper.accessor('is_changed', {
            header: 'Изменен',
            cell: ({ row }) => <Cell value={!!row.original.old_qty} type="bool" />,
        }),
        columnHelper.accessor('is_added', {
            header: 'Добавлен',
            cell: ({ getValue }) => <Cell value={getValue()} type="bool" />,
        }),
        columnHelper.accessor('is_deleted', {
            header: 'Удален',
            cell: ({ getValue }) => <Cell value={getValue()} type="bool" />,
        }),
        columnHelper.accessor('cost', {
            header: 'Стоимость,  ₽',
            cell: ({ getValue }) => <Cell value={getValue()} type="price" />,
        }),
        columnHelper.accessor('action', {
            header: 'Действие',
            cell: ({ row }) => (
                <Layout type="flex" gap={scale(1)}>
                    <Layout.Item>
                        <Button
                            onClick={() => {
                                popupDispatch({
                                    type: ActionType.Delete,
                                    payload: {
                                        title: `Вы уверены, что хотите удалить товар из заказа?`,
                                        popupAction: ActionEnum.DELETE,
                                        onAction: async () => {
                                            try {
                                                await deleteOrderItems.mutateAsync({
                                                    id: orderId!,
                                                    offer_ids: [Number(row.original.offer_id)],
                                                });
                                            } catch (err) {
                                                console.error(err);
                                            }
                                        },
                                    },
                                });
                            }}
                            disabled={disabledOffersEdit || disabledProductRemove}
                        >
                            Удалить
                        </Button>
                    </Layout.Item>
                </Layout>
            ),
            enableHiding: true,
        }),
    ];

    const [{ sorting }, sortingPlugin] = useSorting<StorePickupTime>('order-detail_products', [], undefined, false);

    const table = useTable(
        {
            data,
            columns,
            meta: {
                tableKey: `order-detail_products`,
            },
            state: {
                sorting,
            },
        },
        [sortingPlugin]
    );

    return (
        <div>
            <ul css={{ marginBottom: scale(2) }}>
                <li css={itemCSS}>
                    <p css={itemTitleCSS}>Номер отгрузки:</p>
                    {shipment.number}
                </li>
                <li css={itemCSS}>
                    <p css={itemTitleCSS}>Идентификатор склада:</p>
                    {shipment.store_id}
                </li>
                <li css={itemCSS}>
                    <p css={itemTitleCSS}>Статус отгрузки:</p>
                    {canEditShipmentStatus ? (
                        <Tooltip
                            trigger="click"
                            theme="light"
                            arrow
                            placement="bottom-start"
                            minWidth={scale(15)}
                            onCreate={inst => setTippyInstance(inst)}
                            content={
                                <ul>
                                    {apiShipmentStatuses?.data.map(status => (
                                        <li key={status.id}>
                                            <ContentBtn
                                                onClick={() => {
                                                    tippyInstance?.hide();
                                                    onChangeStatus(status.id);
                                                }}
                                            >
                                                {status.name}
                                            </ContentBtn>
                                        </li>
                                    ))}
                                </ul>
                            }
                        >
                            <button
                                type="button"
                                css={linkStyles}
                                aria-label="Изменить статус отгрузки"
                                title="Изменить статус отгрузки"
                            >
                                {shipmentStatus}
                            </button>
                        </Tooltip>
                    ) : (
                        shipmentStatus
                    )}
                </li>
                <li css={itemCSS}>
                    <p css={itemTitleCSS}>Дата/время изменения статуса:</p>
                    {shipment.status_at
                        ? formatDate(new Date(shipment.status_at), DateFormatters.DATE_AND_TIME, true)
                        : '-'}
                </li>
            </ul>

            {data.length !== 0 ? (
                <Table instance={table} />
            ) : (
                <TableEmpty
                    filtersActive={false}
                    titleWithFilters="Офферы не найдены"
                    titleWithoutFilters="Офферы отсутствуют"
                />
            )}

            <ActionPopup popupState={popupState} popupDispatch={popupDispatch} />
        </div>
    );
};

export const Products = ({
    order,
    refetch,
    canEditShipmentStatus,
    setSelectProducts,
}: {
    order: Order | undefined;
    refetch: () => void;
    canEditShipmentStatus: boolean;
    setSelectProducts: Dispatch<SetStateAction<boolean>>;
}) => {
    const { data: apiDeliveryStatuses } = useDeliveryStatuses();

    const { canEditProductsTab } = useOrdersAccess();

    const changeShipmentStatus = useShipmentChange();
    useSuccess(changeShipmentStatus.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');
    useError(changeShipmentStatus.error);

    const deliveries = useMemo(() => order?.deliveries || [], [order?.deliveries]);
    const disabledOffersEdit = useMemo(
        () => !order?.is_editable || !canEditProductsTab,
        [order?.is_editable, canEditProductsTab]
    );
    const disabledProductRemove = useMemo(
        () =>
            order?.deliveries &&
            order?.deliveries[0]?.shipments.flatMap(({ order_items }) => order_items)?.length === 1,
        [order?.deliveries]
    );

    return (
        <>
            <Layout cols={1} gap={scale(2)}>
                {deliveries.map(delivery => (
                    <Layout.Item>
                        <Block>
                            <Block.Header>
                                <li css={itemCSS}>
                                    <p css={itemTitleCSS}>Номер отправления:</p>
                                    {delivery.number}
                                </li>
                            </Block.Header>
                            <Block.Body>
                                <ul css={{ marginBottom: scale(4) }}>
                                    <li css={itemCSS}>
                                        <p css={itemTitleCSS}>Дата доставки отправления:</p>
                                        {delivery.date
                                            ? formatDate(new Date(delivery.date), 'dd MMMM yyyy')
                                            : 'Дата не выбрана'}
                                    </li>
                                    <li css={itemCSS}>
                                        <p css={itemTitleCSS}>Статус отправления:</p>
                                        {getOptionName(apiDeliveryStatuses?.data, delivery?.status)}
                                    </li>
                                    <li css={itemCSS}>
                                        <p css={itemTitleCSS}>Дата/время изменения статуса: </p>
                                        {delivery.status_at
                                            ? formatDate(
                                                  new Date(delivery.status_at),
                                                  DateFormatters.DATE_AND_TIME,
                                                  true
                                              )
                                            : '-'}
                                    </li>
                                </ul>
                                <p
                                    css={{
                                        ...typography('h3'),
                                        marginBottom: scale(1),
                                    }}
                                >
                                    Отгрузки:
                                </p>
                                <ul>
                                    {delivery.shipments.map((shipment, j) => (
                                        <>
                                            {j > 0 && <hr css={{ margin: `${scale(2)}px 0` }} />}
                                            <li key={shipment.id} css={j > 0 && { marginTop: scale(3) }}>
                                                <Shipment
                                                    orderId={order?.id}
                                                    shipment={shipment}
                                                    canEditShipmentStatus={canEditShipmentStatus}
                                                    onChangeStatus={async (status: number) => {
                                                        try {
                                                            await changeShipmentStatus.mutateAsync(
                                                                {
                                                                    id: shipment.id,
                                                                    status,
                                                                },
                                                                {
                                                                    onSuccess: () => {
                                                                        refetch();
                                                                    },
                                                                }
                                                            );
                                                        } catch (err) {
                                                            console.error(err);
                                                        }
                                                    }}
                                                    disabledOffersEdit={disabledOffersEdit}
                                                    disabledProductRemove={disabledProductRemove || false}
                                                />
                                                <div css={{ marginTop: scale(2) }}>
                                                    <p css={itemCSS}>
                                                        <span css={itemTitleCSS}>Количество позиций:</span>
                                                        {shipment.order_items.length || 0}
                                                    </p>
                                                    <p css={itemCSS}>
                                                        <span css={itemTitleCSS}>Итого:</span>
                                                        {formatPrice(fromKopecksToRouble(+shipment.cost))} ₽
                                                    </p>
                                                </div>
                                            </li>
                                        </>
                                    ))}
                                </ul>
                            </Block.Body>
                        </Block>
                    </Layout.Item>
                ))}
            </Layout>
            <Button
                css={{ marginTop: scale(2) }}
                Icon={PlusIcon}
                onClick={() => setSelectProducts(true)}
                disabled={disabledOffersEdit}
            >
                Добавить товары в заказ
            </Button>
        </>
    );
};
