import { Block, FormCheckbox, FormFieldWrapper, Textarea, useFormContext } from '@ensi-platform/core-components';

import { Layout, scale } from '@scripts/gds';

import { useOrdersAllowedFields } from '../useOrdersAccess';

export const Comments = () => {
    const { watch } = useFormContext<{ is_problem: boolean }>();
    const isProblem = watch('is_problem');

    const { canEditField } = useOrdersAllowedFields();

    return (
        <Block css={{ padding: scale(3) }}>
            <Layout cols={1}>
                <Layout.Item col={1}>
                    <FormFieldWrapper label="Комментарий клиента" name="client_comment">
                        <Textarea minRows={5} disabled={!canEditField('client_comment')} />
                    </FormFieldWrapper>
                </Layout.Item>
                <Layout.Item col={1}>
                    <FormFieldWrapper name="is_problem" disabled={!canEditField('is_problem')}>
                        <FormCheckbox>Есть проблема</FormCheckbox>
                    </FormFieldWrapper>
                </Layout.Item>
                {isProblem && (
                    <Layout.Item col={1}>
                        <FormFieldWrapper
                            label="Проблемы"
                            name="problem_comment"
                            disabled={!canEditField('problem_comment')}
                        >
                            <Textarea minRows={5} />
                        </FormFieldWrapper>
                    </Layout.Item>
                )}
            </Layout>
        </Block>
    );
};
