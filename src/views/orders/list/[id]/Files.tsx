import { Block, FormFieldWrapper, useFormContext } from '@ensi-platform/core-components';

import { useDownloadFile } from '@api/common';

import Dropzone from '@controls/Dropzone';

import { scale } from '@scripts/gds';

export const Files = ({
    canDeleteAttachmentsData,
    canLoadAttachmentsData,
}: {
    canDeleteAttachmentsData: boolean;
    canLoadAttachmentsData: boolean;
}) => {
    const { watch, setValue: setFieldValue } = useFormContext<{ files_to_delete: number[] }>();
    const files_to_delete = watch('files_to_delete');

    const { downloadFile } = useDownloadFile();

    return (
        <Block css={{ padding: scale(3) }}>
            <FormFieldWrapper label="Прикрепите вложения" name="files">
                <Dropzone
                    onFileRemove={(index, file) => {
                        if (file?.id) {
                            setFieldValue('files_to_delete', [...files_to_delete, file?.id]);
                        }
                    }}
                    onFileClick={f => downloadFile(f.file, f.name)}
                    isDragDisabled={!canLoadAttachmentsData}
                    isDisableRemove={!canDeleteAttachmentsData}
                />
            </FormFieldWrapper>
        </Block>
    );
};
