import { Block, FormField, FormFieldWrapper } from '@ensi-platform/core-components';

import LoadingSkeleton from '@controls/LoadingSkeleton';
import Mask from '@controls/Mask';

import { Layout, scale } from '@scripts/gds';
import { maskPhone } from '@scripts/mask';

import { useOrdersAllowedFields } from '../useOrdersAccess';

export const Customer = ({ isLoading }: { isLoading: boolean }) => {
    const { canEditField } = useOrdersAllowedFields();

    return (
        <Block css={{ padding: scale(3) }}>
            <Layout cols={1}>
                {isLoading ? (
                    [1, 2, 3].map(e => (
                        <Layout.Item col={1} key={e}>
                            <LoadingSkeleton />
                        </Layout.Item>
                    ))
                ) : (
                    <>
                        <Layout.Item col={1}>
                            <FormField name="receiver_name" label="ФИО" disabled={!canEditField('receiver_name')} />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <FormFieldWrapper
                                name="receiver_phone"
                                label="Телефон"
                                type="tel"
                                disabled={!canEditField('receiver_phone')}
                            >
                                <Mask mask={maskPhone} />
                            </FormFieldWrapper>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <FormField
                                name="receiver_email"
                                label="E-mail"
                                disabled={!canEditField('receiver_email')}
                            />
                        </Layout.Item>
                    </>
                )}
            </Layout>
        </Block>
    );
};
