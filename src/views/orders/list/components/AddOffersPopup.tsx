/* eslint-disable react/no-unstable-nested-components */
import { Loader, Popup, PopupContent, PopupHeader } from '@ensi-platform/core-components';
import { Row, createColumnHelper } from '@tanstack/react-table';
import { useCallback, useEffect, useMemo, useState } from 'react';

import { Offer, useSearchOffers } from '@api/catalog';
import { useAddOrderItems } from '@api/orders';

import { useError, useSuccess } from '@context/modal';

import Counter from '@controls/Counter';

import Table, { useSorting, useTable } from '@components/Table';
import { getSelectColumn } from '@components/Table/columns';
import { TableEmpty, TableFooter, TableHeader } from '@components/Table/components';

import { ModalMessages } from '@scripts/constants';
import { Button, typography } from '@scripts/gds';
import { declOfNum, formatDate, getTotal, getTotalPages } from '@scripts/helpers';
import { useEditableTableRows, useRedirectToNotEmptyPage, useTableList } from '@scripts/hooks';

import PlusIcon from '@icons/plus.svg';

export interface OfferItem extends Offer {
    qty: number;
}

interface RowData {
    id: number;
    isSelected: boolean;
    qty: number;
}

const getInitialRowData = (id: number): RowData => ({ id, isSelected: false, qty: 1 });

export interface AddOffersPopupProps {
    orderId: number;
    selectedProducts: number[];
    isOpen: boolean;
    onClose: () => void;
}

const AddOffersPopup = ({ isOpen, onClose, orderId, selectedProducts }: AddOffersPopupProps) => {
    const addOrderItems = useAddOrderItems();
    useSuccess(addOrderItems.isSuccess ? ModalMessages.SUCCESS_UPDATE : '');
    useError(addOrderItems.error);

    const isAdding = addOrderItems.isPending;

    const onSubmit = useCallback(
        async (items: OfferItem[]) => {
            const postItems = items.map(({ id, qty }) => ({
                offer_id: id,
                qty,
            }));
            await addOrderItems.mutateAsync({
                id: orderId,
                order_items: postItems,
            });
            onClose();
        },
        [addOrderItems, orderId, onClose]
    );

    const { extraRowsData, setExtraRowsData, onRowChange } = useEditableTableRows(getInitialRowData, (old, payload) => {
        switch (payload.column) {
            case 'isSelected': {
                if (!payload.value) {
                    old.qty = 1;
                }

                old.isSelected = payload.value;

                break;
            }
            case 'qty': {
                old.qty = payload.value;
                break;
            }
            default:
                break;
        }

        return old;
    });

    useEffect(() => {
        if (!isOpen) setExtraRowsData({});
    }, [isOpen, setExtraRowsData]);

    const [activePage, setActivePage] = useState(1);

    const pageKey = 'offers-page';

    const [{ backendSorting }, sortingPlugin] = useSorting<Offer>(pageKey, []);

    const { itemsPerPageCount, setItemsPerPageCount } = useTableList({
        defaultSort: 'id',
        codesToSortKeys: { id: 'id' },
        pageKey,
    });

    const { data: apiOffers } = useSearchOffers(
        {
            sort: backendSorting,
            filter: { product_id: selectedProducts },
            pagination: { type: 'offset', limit: itemsPerPageCount, offset: (activePage - 1) * itemsPerPageCount },
        },
        isOpen
    );

    const offers = useMemo(
        () =>
            apiOffers?.data.map(e => {
                const extraData = extraRowsData[Number(e?.id)];
                return {
                    ...e,
                    qty: extraData?.qty || 1,
                    isSelected: extraData?.isSelected,
                };
            }) || [],
        [apiOffers?.data, extraRowsData]
    );

    const columnHelper = createColumnHelper<any>();
    const columns = useMemo(
        () => [
            getSelectColumn<Offer>(undefined, 'add_offers_popup'),
            columnHelper.accessor('id', {
                header: 'ID оффера',
                cell: props => props.getValue(),
            }),
            columnHelper.accessor('created_at', {
                header: 'Дата создания',
                cell: props => formatDate(new Date(props.getValue())),
            }),
            columnHelper.accessor('updated_at', {
                header: 'Дата изменения',
                cell: props => formatDate(new Date(props.getValue())),
            }),
            columnHelper.accessor('quantity', {
                header: 'Количество',
                cell: ({ row }) => (
                    <Counter
                        label=""
                        value={row.original.qty}
                        name="qty"
                        onChange={newQty => onRowChange({ id: row.original.id, column: 'qty', value: newQty })}
                    />
                ),
            }),
        ],
        []
    );

    const total = getTotal(apiOffers);
    const totalPages = getTotalPages(apiOffers, itemsPerPageCount);

    useRedirectToNotEmptyPage({ activePage, itemsPerPageCount, total, shallow: true, pageKey });

    const renderHeader = useCallback(
        (selectedRows: Row<OfferItem>[]) => (
            <TableHeader>
                <div css={{ display: 'flex', justifyContent: 'space-between', width: '100%', padding: 0 }}>
                    <p css={{ lineHeight: '32px', ...typography('bodySm') }}>
                        Найдено {`${total} ${declOfNum(total, ['оффер', 'оффера', 'офферов'])}`}
                    </p>
                    {selectedRows?.length > 0 ? (
                        <Button
                            Icon={PlusIcon}
                            theme="primary"
                            type="submit"
                            onClick={async () => {
                                await onSubmit(selectedRows.map((row: Row<OfferItem>) => row.original));
                            }}
                        >
                            Добавить оффер(ы)
                        </Button>
                    ) : null}
                </div>
            </TableHeader>
        ),
        [onSubmit, total]
    );
    const selectedTableIndices = useMemo(
        () => Object.keys(extraRowsData).map(e => offers.findIndex(j => j.id === Number(e))),
        [extraRowsData, offers]
    );

    const prepareSelectedIndicesRows = useCallback(() => {
        if (selectedTableIndices.length > 0) {
            const result: Record<string, any> = {};

            selectedTableIndices.forEach((item: number) => {
                const foundIndex = offers.findIndex(offer => offer.id === item);
                result[foundIndex] = true;
            });
            return result;
        }
    }, [selectedTableIndices, offers]);

    const table = useTable(
        {
            data: offers,
            columns,
            meta: {
                tableKey: pageKey,
            },
            ...(selectedTableIndices.length > 0 && {
                initialState: {
                    rowSelection: prepareSelectedIndicesRows(),
                },
            }),
        },
        [sortingPlugin]
    );

    return (
        <Popup open={isOpen} onClose={onClose} size="screen_lg">
            {isAdding ? (
                <Loader />
            ) : (
                <>
                    <PopupHeader title="Добавление офферов в заказ" />
                    <PopupContent>
                        {renderHeader(table.getSelectedRowModel().flatRows)}
                        <Table instance={table} />

                        {offers.length === 0 ? (
                            <TableEmpty
                                filtersActive={false}
                                titleWithFilters="Офферы с данными фильтрами не найдены"
                                titleWithoutFilters="Офферы отсутствуют"
                            />
                        ) : (
                            <TableFooter
                                pages={totalPages}
                                itemsPerPageCount={itemsPerPageCount}
                                setItemsPerPageCount={setItemsPerPageCount}
                                setPage={setActivePage}
                                pageKey={pageKey}
                                controlledPage={activePage}
                            />
                        )}
                    </PopupContent>
                </>
            )}
        </Popup>
    );
};

export default AddOffersPopup;
