import { Form, FormFieldWrapper, Popup, PopupContent, PopupFooter, PopupHeader } from '@ensi-platform/core-components';
import { Dispatch, SetStateAction, useCallback, useMemo } from 'react';
import * as Yup from 'yup';

import { OrderStatus, useOrderChange } from '@api/orders';

import { useError, useSuccess } from '@context/modal';

import Select from '@controls/Select';

import { ErrorMessages, ModalMessages } from '@scripts/constants';
import { Button } from '@scripts/gds';

import { getStatusReplacements } from '../helpers';

const orderStatusesOptions = [
    {
        key: 'Отменен',
        value: OrderStatus.CANCELED,
    },
    { key: 'Подтвержден', value: OrderStatus.CONFIRMED },
];

interface ChangeStatusPopupProps {
    isOpen: boolean;
    setIsOpen: Dispatch<SetStateAction<boolean>>;
    orderId: number;
    oldStatus?: OrderStatus;
}

const ChangeStatusPopup = ({ isOpen, setIsOpen, orderId, oldStatus }: ChangeStatusPopupProps) => {
    const closeStatusPopup = useCallback(() => setIsOpen(false), [setIsOpen]);

    const changeOrder = useOrderChange();

    useError(changeOrder.error);
    useSuccess(changeOrder.status === 'success' ? ModalMessages.SUCCESS_UPDATE : '');

    const statusReplacements = useMemo(() => {
        const statusList = getStatusReplacements(oldStatus);
        return orderStatusesOptions.filter(status => statusList.includes(status.value));
    }, [oldStatus]);

    return (
        <Popup open={isOpen} onClose={closeStatusPopup} size="minSm">
            <PopupHeader title="Изменить статус заказа" />
            <Form
                onSubmit={async ({ status }) => {
                    if (status)
                        changeOrder.mutate({
                            id: orderId,
                            status,
                        });
                    closeStatusPopup();
                }}
                initialValues={{ status: null }}
                validationSchema={Yup.object().shape({
                    status: Yup.number().required(ErrorMessages.REQUIRED),
                })}
            >
                <PopupContent>
                    <FormFieldWrapper name="status" label="Статус">
                        <Select options={statusReplacements} />
                    </FormFieldWrapper>
                </PopupContent>
                <PopupFooter>
                    <Button type="button" theme="secondary" onClick={closeStatusPopup}>
                        Отмена
                    </Button>
                    <Button type="submit">Сохранить</Button>
                </PopupFooter>
            </Form>
        </Popup>
    );
};

export default ChangeStatusPopup;
