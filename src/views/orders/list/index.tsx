import { useCallback, useMemo, useState } from 'react';

import { useOrderStatuses, useOrdersMeta, useOrdersSearch } from '@api/orders';
import { Order } from '@api/orders/types';

import ListBuilder from '@components/ListBuilder';
import { ExtendedRow, TooltipItem } from '@components/Table';

import { useAccess } from '@scripts/hooks';
import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import ChangeStatusPopup from './components/ChangeStatusPopup';
import useOrdersAccess, { AccessMatrix } from './useOrdersAccess';

const OrderList = () => {
    const access = useAccess(AccessMatrix);

    const [isStatusOpen, setIsStatusOpen] = useState(false);
    const [chosenOrderId, setChosenOrderId] = useState(0);
    const [chosenStatus, setChosenStatus] = useState<number>();

    const { canViewAnyPartOfDetailPage, canCreateRequestForRefund, canEditStatusOrder } = useOrdersAccess();

    const goToDetailPage = useGoToDetailPage({ extraConditions: canViewAnyPartOfDetailPage });
    const goToRefundPage = useGoToDetailPage({ rightPathnameAddon: '/create-refund' });

    const { data: statuses, isLoading: isLoadingStatuses } = useOrderStatuses();
    const openChangeStatus = useCallback(
        (originalRows: ExtendedRow['original'][] | undefined) => {
            if (originalRows && originalRows[0].original.id) {
                const oldStatus = statuses?.data.find(status => status.name === originalRows[0].original.status);
                setChosenOrderId(+originalRows[0].original.id);
                if (oldStatus?.id) setChosenStatus(oldStatus.id);
                setIsStatusOpen(true);
            }
        },
        [setChosenOrderId, setIsStatusOpen, setChosenStatus, statuses]
    );

    const tooltipContent: TooltipItem[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Редактировать заказ',
                action: goToDetailPage,
                isDisable: !canViewAnyPartOfDetailPage,
            },
            {
                type: 'edit',
                text: 'Создать заявку на возврат',
                action: goToRefundPage,
                isDisable: !canCreateRequestForRefund,
            },
            {
                type: 'edit',
                text: 'Изменить статус заказа',
                action: openChangeStatus,
                isDisable: !canEditStatusOrder,
            },
        ],
        [
            canViewAnyPartOfDetailPage,
            canCreateRequestForRefund,
            canEditStatusOrder,
            goToDetailPage,
            goToRefundPage,
            openChangeStatus,
        ]
    );

    const enrichOrdersSearchRequest = () => ({
        include: ['deliveries'],
    });

    return (
        <ListBuilder<Order>
            access={access}
            searchHook={useOrdersSearch}
            enrichSearchHookRequest={enrichOrdersSearchRequest}
            metaHook={useOrdersMeta}
            isLoading={isLoadingStatuses}
            tooltipItems={tooltipContent}
            title="Список заказов"
            canViewDetailPage={canViewAnyPartOfDetailPage}
        >
            <ChangeStatusPopup
                isOpen={isStatusOpen}
                setIsOpen={setIsStatusOpen}
                orderId={chosenOrderId}
                oldStatus={chosenStatus}
            />
        </ListBuilder>
    );
};

export default OrderList;
