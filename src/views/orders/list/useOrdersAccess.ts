import { OrderChangeData } from '@api/orders';

import { useAccess, useAllowedFields } from '@scripts/hooks';
import { useIDForbidden } from '@scripts/hooks/useIDForbidden';

export const AccessMatrix = {
    LIST: {
        view: 101,
    },
    ID: {
        create: -1,
        view: 102,
        edit: 109,

        ACTION: {
            EDIT_STATUS: {
                edit: 117,
            },
            REFUND: {
                edit: 118,
            },
        },

        MAIN_TAB: {
            view: 103,
        },
        CLIENT_TAB: {
            view: 104,
            edit: 114,
        },
        PRODUCTS_TAB: {
            view: 105,
            edit: 119,
        },
        DELIVERY_TAB: {
            view: 106,
            DELIVERY: {
                edit: 112,
            },
            COMMENTS: {
                edit: 113,
            },
            edit_status: 120,
        },
        COMMENT_TAB: {
            view: 107,
            COMMENTS: {
                edit: 110,
            },
            PROBLEMS: {
                edit: 111,
            },
        },
        ATTACHMENTS_TAB: {
            view: 108,
            ATTACHMENTS: {
                edit: 115,
                delete: 116,
            },
        },
    },
};
export const useOrdersAllowedFields = () =>
    useAllowedFields<OrderChangeData>(
        {
            110: ['client_comment'],
            111: ['is_problem', 'problem_comment'],
            114: ['receiver_name', 'receiver_phone', 'receiver_email'],
            117: ['status'],
        },
        109
    );

const useOrdersAccess = () => {
    const access = useAccess(AccessMatrix);

    const isIDForbidden = useIDForbidden(access);

    const canViewListingAndFilters = access.LIST.view;
    const canViewDetailPage = access.ID.view;
    const canViewMainTab = access.ID.MAIN_TAB.view || canViewDetailPage;
    const canViewClientTab = access.ID.CLIENT_TAB.view || canViewDetailPage;
    const canViewProductsTab = access.ID.PRODUCTS_TAB.view || canViewDetailPage;
    const canEditProductsTab = access.ID.PRODUCTS_TAB.edit;
    const canViewDeliveryTab = access.ID.DELIVERY_TAB.view || canViewDetailPage;
    const canViewCommentsTab = access.ID.COMMENT_TAB.view || canViewDetailPage;
    const canViewAttachmentsTab = access.ID.ATTACHMENTS_TAB.view || canViewDetailPage;
    const canEditOrderData = access.ID.edit;

    const canEditCommentariesInCommentTab = access.ID.COMMENT_TAB.COMMENTS.edit;
    const canEditOrderProblem = access.ID.COMMENT_TAB.PROBLEMS.edit;

    const canEditDeliveryMethod = access.ID.DELIVERY_TAB.DELIVERY.edit;
    const canEditCommentariesInDeliveryTab = access.ID.DELIVERY_TAB.COMMENTS.edit;
    const canEditDataInClientTab = access.ID.CLIENT_TAB.edit;
    const canLoadAttachmentsData = access.ID.ATTACHMENTS_TAB.ATTACHMENTS.edit;
    const canDeleteAttachmentsData = access.ID.ATTACHMENTS_TAB.ATTACHMENTS.delete;
    const canEditStatusOrder = access.ID.ACTION.EDIT_STATUS.edit;
    const canCreateRequestForRefund = access.ID.ACTION.REFUND.edit;
    const canEditShipmentStatus = access.ID.DELIVERY_TAB.edit_status;

    const canViewAnyPartOfDetailPage =
        canViewDetailPage ||
        canViewMainTab ||
        canViewClientTab ||
        canViewProductsTab ||
        canViewDeliveryTab ||
        canViewCommentsTab ||
        canViewAttachmentsTab;

    return {
        canViewAnyPartOfDetailPage,
        canEditShipmentStatus,
        canViewListingAndFilters,
        canViewDetailPage,
        canViewMainTab,
        canViewClientTab,
        canViewProductsTab,
        canViewDeliveryTab,
        canViewCommentsTab,
        canViewAttachmentsTab,
        canEditOrderData,
        canEditCommentariesInCommentTab,
        canEditOrderProblem,
        canEditDeliveryMethod,
        canEditCommentariesInDeliveryTab,
        canEditDataInClientTab,
        canLoadAttachmentsData,
        canDeleteAttachmentsData,
        canEditStatusOrder,
        canCreateRequestForRefund,
        canEditProductsTab,
        isIDForbidden,
    };
};

export default useOrdersAccess;
