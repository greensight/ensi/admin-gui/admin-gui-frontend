/* eslint-disable react/no-unstable-nested-components */
import {
    Block,
    Form,
    FormField,
    FormFieldWrapper,
    Popup,
    PopupContent,
    PopupFooter,
    PopupHeader,
    Textarea,
} from '@ensi-platform/core-components';
import { Row, createColumnHelper } from '@tanstack/react-table';
import { useCallback, useMemo, useState } from 'react';
import * as Yup from 'yup';

import { RefundReason, useCreateRefundReasons, usePatchRefundReasons, useRefundReasons } from '@api/orders';

import { useError, useSuccess } from '@context/modal';

import PageWrapper from '@components/PageWrapper';
import Table, { TooltipItem, useSorting, useTable } from '@components/Table';
import { getSelectColumn, getSettingsColumn } from '@components/Table/columns';
import { Cell, TableHeader } from '@components/Table/components';

import { ErrorMessages, ModalMessages } from '@scripts/constants';
import { Button, scale } from '@scripts/gds';

import PlusIcon from '@icons/plus.svg';

import { useRefundsAccess } from '../useRefundsAccess';

const columnHelper = createColumnHelper<RefundReason>();

export default function RefundSettings() {
    const [isOpen, setIsOpen] = useState(false);
    const [activeRow, setActiveRow] = useState<RefundReason | null>(null);
    const closePopup = useCallback(() => {
        setIsOpen(false);
        // чтобы не портить анимацию
        setTimeout(() => setActiveRow(null), 300);
    }, []);

    const {
        access: {
            ID: {
                settings: { view: canView, edit: canEdit, create: canCreate },
            },
        },
    } = useRefundsAccess();

    const [{ sorting }, sortingPlugin] = useSorting<RefundReason>('refund_settings', [], undefined, false);

    const { data, error } = useRefundReasons(canView);
    const createReason = useCreateRefundReasons();
    const updateReason = usePatchRefundReasons();
    useError(error);
    useError(createReason.error);
    useError(updateReason.error);
    useError(createReason.error);
    useSuccess(createReason.status === 'success' ? ModalMessages.SUCCESS_UPDATE : '');
    useSuccess(updateReason.status === 'success' ? ModalMessages.SUCCESS_UPDATE : '');

    const editRow = useCallback((row: Row<any>[] | undefined) => {
        if (row) {
            setActiveRow(row[0].original as RefundReason);
            setIsOpen(true);
        }
    }, []);

    const tooltipContent: TooltipItem[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Изменить причину',
                action: editRow,
                isDisable: !canEdit,
            },
        ],
        [canEdit, editRow]
    );

    const initialValues = useMemo(
        () => ({
            code: activeRow?.code || '',
            name: activeRow?.name || '',
            description: activeRow?.description || '',
        }),
        [activeRow]
    );

    const columns = useMemo(
        () => [
            getSelectColumn<RefundReason>(undefined, 'refund_settings'),
            columnHelper.accessor('id', {
                header: 'ID',
                cell: props => props.getValue(),
            }),
            columnHelper.accessor('code', {
                header: 'Символьный код',
                cell: props => props.getValue(),
            }),
            columnHelper.accessor('name', {
                header: 'Название',
                cell: props => props.getValue(),
            }),
            columnHelper.accessor('description', {
                header: 'Описание',
                cell: props => props.getValue(),
            }),
            columnHelper.accessor('created_at', {
                header: 'Дата создания',
                cell: ({ getValue }) => <Cell value={getValue()} type="datetime" />,
            }),
            columnHelper.accessor('updated_at', {
                header: 'Дата изменения',
                cell: ({ getValue }) => <Cell value={getValue()} type="datetime" />,
            }),
            getSettingsColumn({ tooltipContent }),
        ],
        []
    );

    const table = useTable(
        {
            data: data?.data || [],
            columns,
            meta: {
                tableKey: `refund_settings`,
            },
            state: {
                sorting,
            },
        },
        [sortingPlugin]
    );

    return (
        <PageWrapper h1={canView ? 'Настройка причин возврата' : ''} isForbidden={!canView}>
            <Block>
                <Block.Body>
                    <TableHeader css={{ justifyContent: 'space-between' }}>
                        <span>Всего причин: {data?.data.length}</span>
                        {canCreate && (
                            <Button Icon={PlusIcon} onClick={() => setIsOpen(true)}>
                                Добавить причину
                            </Button>
                        )}
                    </TableHeader>
                    <Table instance={table} />
                </Block.Body>
            </Block>
            <Popup onClose={closePopup} open={isOpen}>
                <PopupHeader title={`${activeRow ? 'Редактирование' : 'Создание'} причины возврата`} />
                <Form
                    initialValues={initialValues}
                    validationSchema={Yup.object().shape({
                        code: Yup.string().required(ErrorMessages.REQUIRED),
                        name: Yup.string().required(ErrorMessages.REQUIRED),
                    })}
                    onSubmit={({ code, name, description }) => {
                        const newData = {
                            code: (code as string).toUpperCase(),
                            name,
                            description: description || null,
                        };
                        if (activeRow) {
                            updateReason.mutate({ id: activeRow.id, ...newData });
                        } else {
                            createReason.mutate(newData);
                        }
                        closePopup();
                    }}
                >
                    <PopupContent>
                        <FormField name="name" label="Название причины возврата" css={{ marginBottom: scale(2) }} />
                        <FormField
                            name="code"
                            label="Символьный код причины возврата"
                            css={{ marginBottom: scale(2) }}
                        />
                        <FormFieldWrapper name="description" label="Детальное описание причины возврата">
                            <Textarea minRows={2} />
                        </FormFieldWrapper>
                    </PopupContent>
                    <PopupFooter>
                        <Button theme="secondary" onClick={closePopup}>
                            Отменить
                        </Button>
                        <Button type="submit">{activeRow ? 'Обновить' : 'Создать'}</Button>
                    </PopupFooter>
                </Form>
            </Popup>
        </PageWrapper>
    );
}
