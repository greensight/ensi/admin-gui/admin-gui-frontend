/* eslint-disable react/no-unstable-nested-components */
import { AutocompleteAsync, Block, FormFieldWrapper, Price, Tabs } from '@ensi-platform/core-components';
import { createColumnHelper } from '@tanstack/react-table';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import * as Yup from 'yup';

import { useDownloadFile } from '@api/common';
import { useCustomer } from '@api/customers';
import {
    RefundStatuses,
    useOrderSources,
    usePatchRefund,
    useRefund,
    useRefundAddFile,
    useRefundDeleteFiles,
} from '@api/orders';
import { useAdminUser, useGetAdminUsersOptionsByValuesFn, useGetAdminUsersSearchFn, useGetUserById } from '@api/units';

import { useError, useSuccess } from '@context/modal';

import Dropzone from '@controls/Dropzone';
import { FileType } from '@controls/Dropzone/DropzoneFile';
import Tooltip, { ContentBtn, TippyInstance, hideOnEsc } from '@controls/Tooltip';

import FormWrapper from '@components/FormWrapper';
import PageControls from '@components/PageControls';
import { PageTemplate } from '@components/PageTemplate';
import PageWrapper from '@components/PageWrapper';
import Table, { TableColumnDefAny, TooltipItem, useSorting, useTable } from '@components/Table';
import { Cell } from '@components/Table/components';

import { ErrorMessages, ModalMessages } from '@scripts/constants';
import { RedirectMethods } from '@scripts/enums';
import { Button, Layout, scale, typography, useTheme } from '@scripts/gds';
import { fromKopecksToRouble, getOptionName, isPageNotFound, prepareTelValue } from '@scripts/helpers';
import { useLinkCSS, useMedia } from '@scripts/hooks';
import { useTabs } from '@scripts/hooks/useTabs';

import KebabIcon from '@icons/small/kebab.svg';

import { useRefundsAccess } from '../useRefundsAccess';
import { Aside } from './Aside';
import { ChangeStatusPopup } from './ChangeStatusPopup';

export default function RefundsPage() {
    const { pathname, push, query } = useRouter();
    const { sm } = useMedia();
    const { colors } = useTheme();
    const linkStyles = useLinkCSS();
    const refundId = (query && query.id && +query.id) || 0;

    const { adminUsersSearchFn } = useGetAdminUsersSearchFn();
    const { adminUsersOptionsByValuesFn } = useGetAdminUsersOptionsByValuesFn();

    const listLink = pathname.split(`[id]`)[0];

    const {
        access: {
            ID: {
                view: canViewFullDetailPage,
                edit: canEditOnlyRefundsData,
                main: { view: canViewMain, editResponsibleForRefund: canEditResponsibleForRefund },
                products: { view: canViewProducts },
                attachments: {
                    view: canViewAttachments,
                    create: canLoadAttachmentsData,
                    delete: canDeleteAttachmentsData,
                },
                settings: { edit: canEditRefund },
            },
        },
        canViewAnyPartOfDetailPage,
    } = useRefundsAccess();

    const canViewMainTab = canViewMain || canViewFullDetailPage;
    const canViewProductsTab = canViewProducts || canViewFullDetailPage;
    const canViewAttachmentsTab = canViewAttachments || canViewFullDetailPage;
    const canViewAny = canViewFullDetailPage || canViewAnyPartOfDetailPage;

    const { getTabsProps, tabIds } = useTabs({
        tabsVisibility: {
            '0': canViewMainTab,
            '1': canViewProductsTab,
            '2': canViewAttachmentsTab,
        },
    });

    const [isStatusOpen, setIsStatusOpen] = useState(false);
    const [responsibleUserData, setResponsibleUserData] = useState<{
        id: number;
        title: string;
    } | null>(null);
    const closeStatusPopup = useCallback(() => setIsStatusOpen(false), []);

    const {
        data,
        isFetching: isLoading,
        error,
    } = useRefund(refundId, ['files', 'items', 'items.product', 'order', 'reasons'], canViewAny);
    const { data: sourcesData } = useOrderSources(canViewAny);

    const refund = useMemo(() => data?.data, [data?.data]);
    const { data: customerData, isFetching: isCustomerFetching } = useCustomer(refund?.order.customer_id, canViewAny);

    const disabledEditResponsible = !(canEditResponsibleForRefund || canEditOnlyRefundsData);

    const { data: author } = useAdminUser(String(refund?.manager_id), undefined, canViewAny && !!refund?.manager_id);

    const patchRefund = usePatchRefund();
    const addFile = useRefundAddFile();
    const deleteFiles = useRefundDeleteFiles();

    useError(error);
    useError(patchRefund.error);
    useSuccess(patchRefund.status === 'success' ? ModalMessages.SUCCESS_UPDATE : '');
    useError(addFile.error);
    useSuccess(addFile.status === 'success' ? ModalMessages.SUCCESS_SAVE : '');
    useError(deleteFiles.error);
    useSuccess(deleteFiles.status === 'success' ? ModalMessages.SUCCESS_DELETE : '');

    const pageTitle = `Заявка на возврат ${refund?.id || ''}`;
    const filesToDelete = useRef<FileType[]>([]);
    const filesToUpload = useRef<FileType[]>([]);
    const initialValues = useMemo(
        () => ({
            files_to_delete: [] as number[],
            files: (refund?.files?.map(f => ({ id: f.id, name: f.original_name, file: f.file })) || []) as FileType[],
            responsible_id: responsibleUserData ? responsibleUserData.id : refund?.responsible_id,
        }),
        [refund?.files, refund?.responsible_id, responsibleUserData]
    );

    const products = useMemo(
        () =>
            refund?.items?.map(item => ({
                id: item.id,
                photo: item.product?.main_image_file,
                name: {
                    name: item.name,
                    code: item.product_data?.barcode,
                    link: item?.product?.id ? `/products/catalog/${item?.product?.id}` : null,
                },
                price: [item.price_per_one, item.cost_per_one],
                quantity: item.qty,
                cost: item.price,
                refund_qty: item.refund_qty,
            })) || [],
        [refund?.items]
    );

    const { getUserById } = useGetUserById();

    useEffect(() => {
        const fetchUser = async (id: number) => {
            const res = await getUserById(id);
            setResponsibleUserData(res[0]);
        };

        if (disabledEditResponsible && refund?.responsible_id && !responsibleUserData)
            fetchUser(refund?.responsible_id);
    }, [disabledEditResponsible, getUserById, refund?.responsible_id, responsibleUserData]);

    const { downloadFile } = useDownloadFile();
    const shouldReturnBack = useRef(false);
    const canSubmit = canEditResponsibleForRefund || canEditOnlyRefundsData || canEditRefund;

    const tippyInstance = useRef<TippyInstance>();

    const tooltipContent: TooltipItem[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Изменить статус заявки',
                action: () => {
                    setIsStatusOpen(true);
                    tippyInstance.current?.hide();
                },
                isDisable: !(canEditRefund || canEditOnlyRefundsData),
            },
        ],
        []
    );

    const columnHelper = createColumnHelper<Record<string, any>>();
    const columns: TableColumnDefAny[] = [
        columnHelper.accessor('id', {
            header: 'ID',
            cell: props => props.getValue(),
        }),
        columnHelper.accessor('photo', {
            header: 'Фото',
            cell: ({ getValue }) => <Cell value={getValue()} type="photo" />,
            enableHiding: true,
        }),
        columnHelper.accessor('name', {
            header: 'Наименование',
            cell: ({ getValue }) => {
                const value = getValue();

                return value?.link ? (
                    <p css={{ marginBottom: scale(1) }}>
                        <Link legacyBehavior passHref href={value.link}>
                            <a css={linkStyles}>{value.name}</a>
                        </Link>
                    </p>
                ) : (
                    <p css={{ marginBottom: scale(1) }}>{value.name}</p>
                );
            },
            enableHiding: true,
        }),
        columnHelper.accessor('price', {
            header: 'Цена и цена без скидки,  ₽',
            cell: ({ getValue }) =>
                getValue().map((v: number, index: number) => (
                    <div key={Number(index)} css={index > 0 && { color: colors?.grey700 }}>
                        <Cell value={v} type="price" />
                    </div>
                )),
            enableHiding: true,
        }),
        columnHelper.accessor('quantity', {
            header: 'Количество',
            cell: props => props.getValue() || '-',
        }),
        columnHelper.accessor('cost', {
            header: 'Стоимость,  ₽',
            cell: ({ getValue }) => <Cell value={getValue()} type="price" />,
        }),
        columnHelper.accessor('refund_qty', {
            header: 'Количество к возврату',
            cell: props => props.getValue() || '-',
        }),
    ];

    const [{ sorting }, sortingPlugin] = useSorting<typeof products>('refund-detail_products', [], undefined, false);

    const table = useTable(
        {
            data: products,
            columns,
            meta: {
                tableKey: `refund-detail_products`,
            },
            state: {
                sorting,
            },
        },
        [sortingPlugin]
    );

    const isNotFound = isPageNotFound({ id: refundId, error });

    return (
        <PageWrapper isLoading={isLoading} isForbidden={!canViewAny} isNotFound={isNotFound}>
            <FormWrapper
                disabled={!canSubmit}
                initialValues={initialValues}
                enableReinitialize
                validationSchema={Yup.object().shape({
                    responsible_id: Yup.number()
                        .transform(val => (Number.isNaN(val) ? undefined : val))
                        .required(ErrorMessages.REQUIRED),
                })}
                onSubmit={async vals => {
                    if (initialValues.responsible_id !== vals.responsible_id) {
                        await patchRefund.mutateAsync({
                            id: refundId,
                            responsible_id: vals.responsible_id,
                        });
                    }
                    if (filesToDelete.current.length > 0) {
                        await deleteFiles.mutateAsync({
                            id: refundId,
                            file_ids: filesToDelete.current.map(e => e.id!),
                        });
                        filesToDelete.current = [];
                    }

                    filesToUpload.current = vals.files.filter(file => !file?.id);

                    await Promise.all(filesToUpload.current.map(file => addFile.mutateAsync({ id: refundId, file })));

                    return {
                        method: RedirectMethods.push,
                        redirectPath: shouldReturnBack.current ? listLink : `/orders/refunds/${refundId}`,
                    };
                }}
                css={{ position: 'relative' }}
            >
                <PageTemplate
                    h1={pageTitle}
                    backlink={{ text: 'Назад', href: '/orders/refunds' }}
                    controls={
                        <PageControls
                            access={{
                                LIST: {
                                    view: canViewAny,
                                },
                                ID: {
                                    delete: false,
                                    create: false,
                                    view: canViewAny,
                                    edit: canSubmit,
                                },
                            }}
                        >
                            {(refund?.status === RefundStatuses.NEW || refund?.status === RefundStatuses.CONFIRMED) && (
                                <Layout.Item>
                                    <Tooltip
                                        content={
                                            <>
                                                {tooltipContent.map(t => (
                                                    <ContentBtn
                                                        key={t.text}
                                                        type={t.type}
                                                        onClick={e => {
                                                            e.stopPropagation();
                                                            t.action(table.getSelectedRowModel().flatRows);
                                                        }}
                                                        disabled={
                                                            typeof t?.isDisable === 'function'
                                                                ? t.isDisable()
                                                                : t?.isDisable
                                                        }
                                                    >
                                                        {t.text}
                                                    </ContentBtn>
                                                ))}
                                            </>
                                        }
                                        plugins={[hideOnEsc]}
                                        trigger="click"
                                        arrow
                                        theme="light"
                                        placement="bottom"
                                        minWidth={scale(36)}
                                        disabled={tooltipContent.length === 0}
                                        appendTo={() => document.body}
                                    >
                                        <Button
                                            theme="outline"
                                            Icon={KebabIcon}
                                            css={{ marginLeft: scale(2), marginRight: scale(2) }}
                                            iconAfter
                                        >
                                            Действия
                                        </Button>
                                    </Tooltip>
                                </Layout.Item>
                            )}
                            <PageControls.Close
                                onClick={() =>
                                    push({
                                        pathname: listLink,
                                    })
                                }
                            />
                            <PageControls.Apply
                                onClick={() => {
                                    shouldReturnBack.current = false;
                                }}
                            />
                            <PageControls.Save
                                onClick={() => {
                                    shouldReturnBack.current = true;
                                }}
                            />
                        </PageControls>
                    }
                    aside={<Aside refund={refund} css={{ marginTop: scale(6), [sm]: { marginTop: 0 } }} />}
                    customChildren
                >
                    <Tabs {...getTabsProps()} css={{ flexGrow: 1 }}>
                        <Tabs.Tab id={tabIds![0]} title="Главное" hidden={!canViewMainTab}>
                            <Block css={{ borderTopLeftRadius: 0 }}>
                                <Block.Body>
                                    <Layout cols={2}>
                                        <Layout.Item col={2}>
                                            <FormFieldWrapper label="Ответственный" name="responsible_id">
                                                <AutocompleteAsync
                                                    asyncSearchFn={adminUsersSearchFn}
                                                    asyncOptionsByValuesFn={adminUsersOptionsByValuesFn}
                                                    disabled={disabledEditResponsible}
                                                />
                                            </FormFieldWrapper>
                                        </Layout.Item>
                                        <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                            <p css={{ ...typography('bodySmBold'), marginBottom: scale(1) }}>Клиент</p>
                                            {!isCustomerFetching && !customerData ? (
                                                <p>-</p>
                                            ) : (
                                                <>
                                                    <p>
                                                        {`${customerData?.data.last_name || ''} ${
                                                            customerData?.data.first_name || ''
                                                        } ${customerData?.data.middle_name || ''}`}
                                                    </p>

                                                    <p>
                                                        <a href={`tel:${customerData?.data.phone}`}>
                                                            {prepareTelValue(customerData?.data.phone || '')}
                                                        </a>
                                                    </p>
                                                    <p>
                                                        <a href={`mailto:${customerData?.data.email}`}>
                                                            {customerData?.data.email}
                                                        </a>
                                                    </p>
                                                </>
                                            )}
                                        </Layout.Item>
                                        {refund?.manager_id && (
                                            <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                                <p css={{ ...typography('bodySmBold'), marginBottom: scale(1) }}>
                                                    Автор
                                                </p>
                                                {author?.data.full_name || '-'}
                                            </Layout.Item>
                                        )}
                                        <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                            <p css={{ ...typography('bodySmBold'), marginBottom: scale(1) }}>Канал</p>
                                            {getOptionName(sourcesData?.data, refund?.source)}
                                        </Layout.Item>

                                        <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                            <p css={{ ...typography('bodySmBold'), marginBottom: scale(1) }}>
                                                Сумма возврата
                                            </p>
                                            <Price value={fromKopecksToRouble(refund?.price || 0)} />
                                        </Layout.Item>

                                        <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                            <p css={{ ...typography('bodySmBold'), marginBottom: scale(1) }}>
                                                Причина возврата
                                            </p>
                                            {new Intl.ListFormat('ru').format(refund?.reasons.map(r => r.name) || [])}
                                        </Layout.Item>
                                        <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                            <p css={{ ...typography('bodySmBold'), marginBottom: scale(1) }}>
                                                Комментарий
                                            </p>
                                            {refund?.user_comment || '-'}
                                        </Layout.Item>
                                        {refund?.rejection_comment && (
                                            <Layout.Item col={{ xxxl: 1, xxs: 2 }}>
                                                <p css={{ ...typography('bodySmBold'), marginBottom: scale(1) }}>
                                                    Причина отклонения
                                                </p>
                                                {refund?.rejection_comment || '-'}
                                            </Layout.Item>
                                        )}
                                        {/* Способ возврата Не реализовано на бэкенде */}
                                        {/* Иноформация о возврате Не реализовано на бэкенде */}
                                    </Layout>
                                </Block.Body>
                            </Block>
                        </Tabs.Tab>
                        <Tabs.Tab id={tabIds![1]} title="Товары" hidden={!canViewProductsTab}>
                            <Block>
                                <Block.Body>
                                    <Table instance={table} />
                                </Block.Body>
                            </Block>
                        </Tabs.Tab>
                        <Tabs.Tab id={tabIds![2]} title="Вложения" hidden={!canViewAttachmentsTab}>
                            <Block>
                                <Block.Body>
                                    <FormFieldWrapper label="Прикрепите вложения" name="files">
                                        <Dropzone
                                            onFileRemove={(index, file) => {
                                                if (file?.id) {
                                                    filesToDelete.current.push(file);
                                                }
                                            }}
                                            onFileClick={f => downloadFile(f.file, f.name)}
                                            isDragDisabled={!(canLoadAttachmentsData || canEditOnlyRefundsData)}
                                            isDisableRemove={!(canDeleteAttachmentsData || canEditOnlyRefundsData)}
                                        />
                                    </FormFieldWrapper>
                                </Block.Body>
                            </Block>
                        </Tabs.Tab>
                    </Tabs>
                </PageTemplate>
            </FormWrapper>
            <ChangeStatusPopup
                isOpen={isStatusOpen}
                onClose={closeStatusPopup}
                refundId={refundId}
                initialStatus={refund?.status as RefundStatuses}
            />
        </PageWrapper>
    );
}
