import {
    Form,
    FormFieldWrapper,
    Popup,
    PopupContent,
    PopupFooter,
    PopupHeader,
    Textarea,
    useFormContext,
} from '@ensi-platform/core-components';
import { FC, useMemo } from 'react';
import * as Yup from 'yup';

import { RefundStatuses, usePatchRefund, useRefundStatuses } from '@api/orders';

import Select from '@controls/Select';

import { ErrorMessages } from '@scripts/constants';
import { Button, scale } from '@scripts/gds';
import { toFutureSelectItems } from '@scripts/helpers';

interface IChangeStatusPopupProps {
    refundId: number;
    isOpen: boolean;
    onClose: () => void;
    initialStatus?: RefundStatuses;
}

interface IFormInnerProps extends Pick<IChangeStatusPopupProps, 'onClose' | 'initialStatus'> {}

const FormInner: FC<IFormInnerProps> = ({ onClose, initialStatus = RefundStatuses.NEW }) => {
    const { watch } = useFormContext();
    const { data: statusData } = useRefundStatuses();

    const status = +watch('status');

    const statuses = useMemo(
        () =>
            toFutureSelectItems(
                statusData?.data.filter(s => {
                    if (initialStatus === RefundStatuses.CONFIRMED) {
                        return s.id === RefundStatuses.CANCELED;
                    }

                    return s.id === RefundStatuses.CONFIRMED || s.id === RefundStatuses.REJECTED;
                })
            ),
        [initialStatus, statusData?.data]
    );

    return (
        <>
            <PopupContent>
                <FormFieldWrapper label="Выберите статус" name="status" css={{ marginBottom: scale(2) }}>
                    <Select options={statuses} />
                </FormFieldWrapper>
                {status === RefundStatuses.REJECTED && (
                    <FormFieldWrapper label="Причина отклонения" name="rejection_comment">
                        <Textarea minRows={3} />
                    </FormFieldWrapper>
                )}
            </PopupContent>
            <PopupFooter>
                <Button theme="secondary" onClick={onClose}>
                    Закрыть
                </Button>
                {status === RefundStatuses.REJECTED && (
                    <Button theme="dangerous" type="submit">
                        Отклонить заявку
                    </Button>
                )}
                {status === RefundStatuses.CONFIRMED && <Button type="submit">Подтвердить заявку</Button>}
                {status === RefundStatuses.CANCELED && (
                    <Button theme="dangerous" type="submit">
                        Отменить заявку
                    </Button>
                )}
            </PopupFooter>
        </>
    );
};

export const ChangeStatusPopup: FC<IChangeStatusPopupProps> = ({ isOpen, onClose, refundId, initialStatus }) => {
    const patchRefund = usePatchRefund();

    return (
        <Popup open={isOpen} onClose={onClose} size="sm">
            <PopupHeader title="Изменить статус заявки" />

            <Form
                initialValues={{ status: initialStatus || RefundStatuses.NEW, rejection_comment: '' }}
                onSubmit={async vals => {
                    await patchRefund.mutateAsync({
                        id: refundId,
                        status: +vals.status,
                        rejection_comment: vals.rejection_comment || undefined,
                    });

                    onClose();
                }}
                validationSchema={Yup.object().shape({
                    status: Yup.number()
                        .transform(value => (Number.isNaN(+value) ? undefined : value))
                        .required(ErrorMessages.REQUIRED),
                    rejection_comment: Yup.string().when('status', ([status], schema) =>
                        status === RefundStatuses.REJECTED ? schema.required(ErrorMessages.REQUIRED) : schema
                    ),
                })}
            >
                <FormInner onClose={onClose} initialStatus={initialStatus} />
            </Form>
        </Popup>
    );
};
