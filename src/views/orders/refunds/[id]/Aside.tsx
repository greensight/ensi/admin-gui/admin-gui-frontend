import { CopyButton, DescriptionList, DescriptionListItem, getDefaultDates } from '@ensi-platform/core-components';
import { FC } from 'react';

import { Refund, useRefundStatuses } from '@api/orders';

import Circle from '@components/Circle';

import { scale } from '@scripts/gds';
import { getOptionName } from '@scripts/helpers';

import { getRefundStatusColor } from '../helpers';

export const Aside: FC<{ refund: Refund | undefined; className?: string }> = ({ refund }) => {
    const { data: statusData } = useRefundStatuses();

    return (
        <DescriptionList>
            <DescriptionListItem
                name="Статус:"
                value={
                    <>
                        <Circle
                            css={{
                                marginLeft: scale(1),
                                marginRight: scale(1, true),
                                background: getRefundStatusColor(refund?.status || 0),
                            }}
                        />
                        {getOptionName(statusData?.data, refund?.status)}
                    </>
                }
            />
            <DescriptionListItem name="ID:" value={<CopyButton>{`${refund?.id}`}</CopyButton>} />
            <DescriptionListItem name="Номер заказа:" value={<CopyButton>{`${refund?.order?.number}`}</CopyButton>} />
            {getDefaultDates({ ...refund }).map(item => (
                <DescriptionListItem {...item} key={item.name} />
            ))}
        </DescriptionList>
    );
};
