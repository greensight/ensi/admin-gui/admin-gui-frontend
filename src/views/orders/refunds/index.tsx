import Link from 'next/link';
import { useCallback, useMemo } from 'react';

import { Refund, useRefundsMeta, useRefundsSearch } from '@api/orders';

import ListBuilder from '@components/ListBuilder';
import { TooltipItem } from '@components/Table';

import { Button, scale } from '@scripts/gds';
import { useGoToDetailPage } from '@scripts/hooks/useGoToDetailPage';

import SettingIcon from '@icons/small/settings.svg';

import { useRefundsAccess } from './useRefundsAccess';

export default function Refunds() {
    const { access, canViewAnyPartOfDetailPage } = useRefundsAccess();

    const enrichRefundsSearchRequest = () => ({
        include: ['reasons', 'order'],
    });

    const goToDetailPage = useGoToDetailPage({ extraConditions: access.ID.view });

    const tooltipContent: TooltipItem[] = useMemo(
        () => [
            {
                type: 'edit',
                text: 'Редактировать заявку',
                action: goToDetailPage,
                isDisable: !access.ID.view,
            },
        ],
        [access.ID.view, goToDetailPage]
    );

    const renderHeader = useCallback(
        () =>
            access.LIST.view ? (
                <Link legacyBehavior href="/orders/refunds/settings" passHref>
                    <Button as="a" theme="fill" Icon={SettingIcon} css={{ marginRight: scale(2) }}>
                        Настройка причин возврата
                    </Button>
                </Link>
            ) : null,
        [access.LIST.view]
    );

    return (
        <ListBuilder<Refund>
            access={access}
            searchHook={useRefundsSearch}
            enrichSearchHookRequest={enrichRefundsSearchRequest}
            metaHook={useRefundsMeta}
            tooltipItems={tooltipContent}
            headerInner={renderHeader}
            canViewDetailPage={canViewAnyPartOfDetailPage}
            title="Список Возвратов"
        />
    );
}
