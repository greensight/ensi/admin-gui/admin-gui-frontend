import getAuthData, { AuthProps } from '@scripts/getAuthData';

export { default } from '@views/content/menu';

export async function getServerSideProps(data: AuthProps) {
    const { ...totalData } = await getAuthData({ ...data });

    return {
        props: {
            ...totalData,
        },
    };
}
