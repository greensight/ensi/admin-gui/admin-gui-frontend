import getAuthData, { AuthProps } from '@scripts/getAuthData';

export { default } from '@views/products/offers/[entity_id]';

export async function getServerSideProps(data: AuthProps) {
    const { ...totalData } = await getAuthData({ ...data });

    return {
        props: {
            ...totalData,
        },
    };
}
