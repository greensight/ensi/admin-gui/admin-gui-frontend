import getAuthData, { AuthProps } from '@scripts/getAuthData';

export { default } from '@views/products/product-groups/[pid]';

export async function getServerSideProps(data: AuthProps) {
    const { ...totalData } = await getAuthData({ ...data });

    return {
        props: {
            ...totalData,
        },
    };
}
