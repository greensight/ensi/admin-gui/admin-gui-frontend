import getAuthData, { AuthProps } from '@scripts/getAuthData';

export { default } from '@views/orders/list/[id]/create-refund';

export async function getServerSideProps(data: AuthProps) {
    const { ...totalData } = await getAuthData({ ...data });

    return {
        props: {
            ...totalData,
        },
    };
}
