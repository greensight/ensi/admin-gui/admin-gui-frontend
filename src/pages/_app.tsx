import type { AppProps } from 'next/app';
import Head from 'next/head';
import { FC, useState } from 'react';

import { useCurrentUser } from '@api/auth';
import { apiFront } from '@api/index';

import { useAuth } from '@context/auth';
import { useModalsContext } from '@context/modal';

import Header from '@containers/Header';
import SidebarContainer from '@containers/Sidebar';

import AppProviders from '@components/AppProviders';
import Auth from '@components/Auth';
import Footer from '@components/Footer';

import { ModalMessages } from '@scripts/constants';
import { scale } from '@scripts/gds';
import { useMedia } from '@scripts/hooks';

const AppContent: FC<AppProps> = ({ Component, pageProps }) => {
    const { tokenData, setTokenData } = useAuth();
    const { appendModal } = useModalsContext();
    const { md } = useMedia();

    const [isAuthLoading, setAuthLoading] = useState(false);
    const { data: userData } = useCurrentUser(Boolean(tokenData.accessToken));

    return (
        <>
            <Head>
                <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
                <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
                <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
                <link rel="manifest" href="/site.webmanifest" />
                <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5" />
                <meta name="msapplication-TileColor" content="#da532c" />
                <meta name="theme-color" content="#ffffff" />
                <meta name="robots" content="noindex" />
                {/* this string is required according to Ensi license */}
                <meta name="generator" content="Ensi Platform" />
            </Head>

            {tokenData.accessToken ? (
                <div css={{ [md]: { paddingBottom: scale(6) } }}>
                    <Header
                        user={userData?.data}
                        onLogout={async () => {
                            try {
                                await apiFront.logOut();
                            } catch (error: unknown) {
                                if (error instanceof Error) {
                                    appendModal({ message: error.message, theme: 'error' });
                                } else {
                                    appendModal({ message: 'An unknown error occurred', theme: 'error' });
                                }
                            }
                            setTokenData({ accessToken: '', hasRefreshToken: false, expiresAt: '' });
                        }}
                    />
                    <SidebarContainer>
                        <div css={{ height: 'calc(100% - 44px)' }}>
                            {userData?.data && <Component {...pageProps} />}
                        </div>
                        <Footer />
                    </SidebarContainer>
                </div>
            ) : (
                <Auth
                    isLoading={isAuthLoading}
                    logIn={async vals => {
                        setAuthLoading(true);

                        try {
                            const promise = apiFront.post('login', { data: vals });

                            appendModal({
                                promise,
                                loading: {
                                    title: ModalMessages.LOADING,
                                    message: '',
                                },
                                error: {
                                    title: 'Ошибка авторизации',
                                    message: ModalMessages.ERROR_UPDATE,
                                },
                                success: {
                                    title: 'Успешно авторизован',
                                    message: '',
                                },
                            });

                            const { data } = await promise;

                            setTokenData({
                                accessToken: data.accessToken,
                                hasRefreshToken: data.hasRefreshToken,
                                expiresAt: data.expiresAt,
                            });
                        } catch (error) {
                            console.error(error);
                        } finally {
                            setAuthLoading(false);
                        }
                    }}
                />
            )}
        </>
    );
};

function MyApp(props: AppProps) {
    return (
        <AppProviders {...props}>
            <AppContent {...props} />
        </AppProviders>
    );
}

export default MyApp;
