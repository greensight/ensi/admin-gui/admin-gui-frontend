import NotFoundPage from '@views/404';

import getAuthData, { AuthProps } from '@scripts/getAuthData';

export default NotFoundPage;

export async function getServerSideProps(data: AuthProps) {
    const { ...totalData } = await getAuthData({ ...data });

    return {
        props: {
            ...totalData,
        },
    };
}
