#!groovy
// -*- coding: utf-8; mode: Groovy; -*-

@Library('ru.greensight@v1.0.8')_

import ru.greensight.HelmParams
import ru.greensight.Options

def options = new Options(script:this)
def helm = new HelmParams(script:this)

def configVarsList = [
    "K8S_NAMESPACE",             // name of k8s namespace, eg. 'stage'
    "HELM_RELEASE",              // name of app helm release
    "GIT_CREDENTIALS_ID",        // credentials id for gitlab login/password
    "VALUES_REPO",               // address of git repository with app chart and values
    "VALUES_BRANCH",             // git branch
    "VALUES_PATH",               // name of directory, eg. 'stage'
    "MULTISTAGE_VALUES_REPO",    // address of git repository with multistage settings file
    "MULTISTAGE_VALUES_BRANCH",  //
    "MULTISTAGE_VALUES_PATH",    //
    "DOCKER_IMAGE_ADDRESS",      // name of application docker image with registry domain (harbor.gs.ru/project/service)
    "DOCKER_IMAGE_NAME",         // name of application docker image without registry domain (project/service)
    "HARBOR_ADDRESS",            // docker registry address (https://harbor.gs.ru)
    "REGISTRY_CREDS",            // credentials id for user/password for docker registry
    "GITLAB_TOKEN_CREDS",        // credentials id for gitlab token
    "HELM_IMAGE",                // docker image with kubectl/helm
    "NEW_SOPS_IMAGE",            // docker image with sops
    "NEW_SOPS_URL",              // address of sops keyservice, eg tcp://1.2.3.4:7777
    "SOPS_KEY_CREDS",            // credentials id for jenkins gpg key
    "K8S_CREDS",                 // credentials id for kubeconfig
    "AUTODEPLOY_BRANCHES",       // list of git branches, which must deploy automatically
    "DTRACK_CREDS",              // credentials id for dependency track token
    "DTRACK_FOLDER"              // name of folder in dependency track UI, where show current project
]

properties([
    gitLabConnection('public-gitlab'),
    parameters([
       booleanParam(name: 'DEPLOY_K8S', defaultValue: false, description: 'Deploy in kubernetes'),
       booleanParam(name: 'PAUSE_BEFORE_DEPLOY', defaultValue: false, description: 'Ask user approvement before deploy'),
       string(name: 'VALUES_BRANCH', defaultValue: env.BRANCH_NAME, description: 'config-store branch'),
       string(name: 'RELEASE_NAME', defaultValue: env.BRANCH_NAME, description: 'name release branch'),
       booleanParam(name: 'FORCE_REBUILD', defaultValue: false, description: 'Build image even if it already exists in registry'),
   ]),
    buildDiscarder(logRotator (artifactDaysToKeepStr: '', artifactNumToKeepStr: '10', daysToKeepStr: '', numToKeepStr: '10')),
    disableConcurrentBuilds(),
])

def doDeploy = ''
def gitCommit = ''
def dockerTag = ''
def releaseName = ''
def valuesBranchRelease = ''
def ciImage = ''
def imageExists = false

node('docker-agent'){
    lock(label: 'docker', quantity: 1) {
        stage('Checkout') {
            gitlabCommitStatus("checkout") {
                cleanWs()

                // We check that the parameters are filled in. If not, we'll fill it in with default values later.
                // It is necessary for a situation when the branch is being assembled for the first time and the parameters are not filled in.
                def paramsDefined = false
                if (options.get("VALUES_BRANCH") != null) {
                    paramsDefined = true
                }

                options.loadConfigFile("env-folder")
                options.loadConfigFile("env-service")

                if (!paramsDefined) {
                    options.vars["DEPLOY_K8S"] = false
                    options.vars["PAUSE_BEFORE_DEPLOY"] = false
                    options.vars["VALUES_BRANCH"] = env.BRANCH_NAME
                }

                options.checkDefined(configVarsList)

                withCredentials([usernamePassword(credentialsId: options.get("REGISTRY_CREDS"), usernameVariable: 'username', passwordVariable: 'password')]) {
                    sh "docker login ${options.get("HARBOR_ADDRESS")} --username ${username} --password '${password}'"
                }

                releaseName = "${options.get('HELM_RELEASE')}-${params.RELEASE_NAME}".replace("_", "-")

                def releaseExists = false
                docker.image(options.get("HELM_IMAGE")).inside('--entrypoint=""') {
                    withCredentials([file(credentialsId: options.get("K8S_CREDS"), variable: 'kubecfg')]) {
                        def status = sh(
                            script: "KUBECONFIG=${kubecfg} helm --namespace ${options.get('K8S_NAMESPACE')} status ${releaseName}",
                            returnStatus: true
                        )
                        releaseExists = status == 0

                        if (releaseExists){
                            valuesBranchRelease = sh(
                                script: "KUBECONFIG=${kubecfg} kubectl get configmap ${releaseName} --namespace ${options.get('K8S_NAMESPACE')} --output=json | jq -r '.metadata.labels.valuesBranchRelease' ",
                            returnStdout: true)?.trim()
                            if (valuesBranchRelease == "" || valuesBranchRelease == "null") {
                                options.vars["VALUES_BRANCH_DEPLOY"] = params.VALUES_BRANCH
                            } else {
                                if(params.DEPLOY_K8S) {
                                    options.vars["VALUES_BRANCH_DEPLOY"] = params.VALUES_BRANCH
                                }else{
                                    options.vars["VALUES_BRANCH_DEPLOY"] = valuesBranchRelease
                                }
                            }
                        } else {
                            options.vars["VALUES_BRANCH_DEPLOY"] = params.VALUES_BRANCH
                        }
                    }
                }
                doDeploy = params.DEPLOY_K8S || options.getAsList("AUTODEPLOY_BRANCHES").contains(BRANCH_NAME) || releaseExists

                echo "Debug: Name of the config branch ${options.get('VALUES_BRANCH_DEPLOY')}"
                echo "Debug: Release Name ${releaseName}"

                if (doDeploy) {
                    cloneToFolder('ms-helm-values', options.get("VALUES_REPO"), options.get("VALUES_BRANCH_DEPLOY"), options.get("GIT_CREDENTIALS_ID"))

                    cloneToFolder('multistage-values', options.get("MULTISTAGE_VALUES_REPO"), options.get("MULTISTAGE_VALUES_BRANCH"), options.get("GIT_CREDENTIALS_ID"))

                    helm.addFirstExistingOptional([
                        "multistage-values/${options.get("MULTISTAGE_VALUES_PATH")}/multistage.yaml",
                    ])

                    helm.addFirstExistingOptional([
                        "ms-helm-values/${options.get("COMMON_VALUES_PATH")}/common-env.yaml",
                    ])
                    helm.addFirstExistingOptional([
                        "ms-helm-values/${options.get("COMMON_VALUES_PATH")}/common-env.sops.yaml",
                    ])

                    def branchFolder = "ms-helm-values/${options.get("VALUES_PATH")}/${env.BRANCH_NAME}/${options.get("HELM_RELEASE")}"
                    def masterFolder = "ms-helm-values/${options.get("VALUES_PATH")}/master/${options.get("HELM_RELEASE")}"
                    helm.addFirstExisting([
                        "${branchFolder}/${options.get("HELM_RELEASE")}.yaml",
                        "${masterFolder}/${options.get("HELM_RELEASE")}.yaml"
                    ])
                    helm.addFirstExistingOptional([
                        "${branchFolder}/${options.get("HELM_RELEASE")}.sops.yaml",
                        "${masterFolder}/${options.get("HELM_RELEASE")}.sops.yaml"
                    ])
                }
                dir('src') {
                    checkout scm
                    gitCommit = sh(returnStdout: true, script: 'git log -1 --format=%h').trim();
                    dockerTag = "${env.BRANCH_NAME}-${gitCommit}"
                    ciImage = sh(returnStdout: true, script: "awk '/# CI:/ {print \$3}' Dockerfile").trim();
                }

                if (params.FORCE_REBUILD) {
                    echo "Debug: test/build is forced"
                } else {
                    imageExists = imageExistsInRegistry(
                        options.get("REGISTRY_CREDS"),
                        options.get("HARBOR_ADDRESS"),
                        options.get('DOCKER_IMAGE_NAME'),
                        dockerTag
                    )

                    echo "Debug: Image exists: ${imageExists}"
                }
            }
        }

        stage('Security') {
            if (!options.get("PACKAGES_SKIP_CHECK") && !imageExists) {
                def dtrackCredentialsId = options.get("DTRACK_CREDS") ?: 'dt-token'
                dir('src') {
                    withCredentials([string(credentialsId: dtrackCredentialsId, variable: 'dtrack_api_key')]) {
                         docker.image(ciImage).inside("--entrypoint=''") {
                             sh """
                                 npm install -g @cyclonedx/cdxgen
                                 cdxgen -o bom.xml
                             """
                            dependencyTrackPublisher(
                                artifact: 'bom.xml',
                                projectName: options.get("HELM_RELEASE"),
                                projectVersion: "master",
                                synchronous: true,
                                dependencyTrackApiKey: dtrack_api_key,
                                projectProperties: [parentId: options.get('DTRACK_FOLDER')],
                                failedTotalCritical: options.get("PACKAGES_FAIL_WHEN_CRITICAL") ?: 1,
                                failedTotalHigh: options.get("PACKAGES_FAIL_WHEN_HIGH") ?: 1,
                                failedTotalMedium: options.get("PACKAGES_FAIL_WHEN_MEDIUM") ?: 1,
                            )
                         }
                    }
                }
            }
        }

        stage('Build') {
            gitlabCommitStatus("Build") {
                if (!imageExists) {
                    dir ('src') {
                        def fullImageNameWithTag = "${options.get('DOCKER_IMAGE_ADDRESS')}:${dockerTag}"
                        image = docker.build(fullImageNameWithTag, "--build-arg BASE_IMAGE=${options.get("BASE_IMAGE")} .")

                        if (doDeploy) {
                            docker.withRegistry(options.get("HARBOR_ADDRESS"), options.get("REGISTRY_CREDS")) {
                                image.push(dockerTag)
                            }
                        }
                    }
                }
            }
        }

        stage('Cleaning Docker images') {
            gitlabCommitStatus("deploy") {
                if (!options.get("DISABLE_DOCKER_CLEAN")) {
                    echo "Debug: Deleting the collected docker images of the service - ${options.get('DOCKER_IMAGE_ADDRESS')}:${dockerTag}"
                    def delDockerImages = sh(
                        script: "docker rmi ${options.get('DOCKER_IMAGE_ADDRESS')}:${dockerTag}",
                        returnStatus: true
                    )
                }
            }
        }

        if (doDeploy) {
            stage('Deploy') {
                gitlabCommitStatus("deploy") {
                    def continueDeploy = false
                    if (params.PAUSE_BEFORE_DEPLOY) {
                        continueDeploy = input(
                                id: 'userInput',
                                message: 'Continue deploy?',
                                parameters: [
                                        [$class: 'BooleanParameterDefinition', defaultValue: true, name: 'Deploy in k8s']
                                ]
                        )
                    } else {
                        continueDeploy = true
                    }

                    if (continueDeploy) {
                        def svcName = ""
                        def ingressHost = ""
                        def helmParamsStr = ""

                        helm
                            .setValue("app.image.repository", options.get('DOCKER_IMAGE_ADDRESS'))
                            .setValue("app.image.tag", dockerTag)
                            .setValue("web.service.name", releaseName)
                            .setValue("app.env.GIT_BRANCH.value", env.BRANCH_NAME)
                            .setValue("multiStage.serviceRelease", params.RELEASE_NAME)

                        withCredentials([file(credentialsId: options.get("SOPS_KEY_CREDS"), variable: 'gpgKeyPath')]) {
                            helmParamsStr = helm.buildParams(options.get('NEW_SOPS_IMAGE'), options.get('NEW_SOPS_URL'), gpgKeyPath)
                        }

                        docker.image(options.get("HELM_IMAGE")).inside('--entrypoint=""') {
                            withCredentials([file(credentialsId: options.get("K8S_CREDS"), variable: 'kubecfg')]) {
                                sh """KUBECONFIG=${kubecfg} \
                                      helm upgrade --install --timeout=30m \
                                      ${helmParamsStr} \
                                      --namespace ${options.get('K8S_NAMESPACE')} \
                                      ${releaseName} ms-helm-values/charts/app"""

                                if (!options.getAsList('NOT_AUTODELETE').contains(params.RELEASE_NAME)){
                                    sh(script:"""
                                        VERSIONRELEASE=`KUBECONFIG=${kubecfg} helm status ${releaseName} --namespace ${options.get('K8S_NAMESPACE')} --output json | jq -r '.version'`
                                        KUBECONFIG=${kubecfg} kubectl patch --namespace ${options.get('K8S_NAMESPACE')} --patch '{"metadata":{"labels":{"autodelete":"true"}}}' secret/sh.helm.release.v1.${releaseName}.v\$VERSIONRELEASE
                                    """)
                                }

                                if (params.RELEASE_NAME != "master"){
                                    sh(script:"""
                                        KUBECONFIG=${kubecfg} kubectl patch --namespace ${options.get('K8S_NAMESPACE')} --patch '{"metadata":{"labels":{"valuesBranchRelease": "${options.get('VALUES_BRANCH_DEPLOY')}"}}}' configmap/${releaseName}
                                    """)
                                }

                                svcName = sh(returnStdout: true, script: """helm template -s templates/web-svc.yaml \
                                    ${helmParamsStr} \
                                    ${releaseName} ms-helm-values/charts/app \
                                    |  awk '/^\\s+name:/ {print \$2}' | head -1
                                    """).trim()

                                ingressHost = sh(returnStdout: true, script: """helm template -s templates/web-ing.yaml \
                                    ${helmParamsStr} \
                                    ${releaseName} ms-helm-values/charts/app \
                                    | awk '/host:/ {print \$3}' | sed 's/"//g'
                                    """).trim()
                            }
                        }

                        currentBuild.displayName = "#${BUILD_NUMBER} - Release : $params.RELEASE_NAME | Values : ${options.get('VALUES_BRANCH_DEPLOY')}"

                        currentBuild.description = [
                            "Release Branch : $params.RELEASE_NAME",
                            "Values Branch : ${options.get('VALUES_BRANCH_DEPLOY')}",
                            "Docker image: ${options.get('DOCKER_IMAGE_ADDRESS')}:${dockerTag}",
                            "Internal host: http://${svcName}.${options.get('K8S_NAMESPACE')}.svc.cluster.local",
                            "Public host: https://${ingressHost}"
                        ].join("\n")

                    }
                }
            }
        }
    }
}
